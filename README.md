# www-api

www-api is a set of microservice APIs to provide support to my personal website, [www.jvt.me](https://www.jvt.me).

This API is licensed under the GPU Affero General Public License v3, for more details I would recommend [a read of the license's terms on TL;DR Legal](https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0)).

## micropub

An implementation of [Micropub](https://indieweb.org/Micropub) for use with my website.

## postdeploy

An API to receive a webhook from GitLab to note that the site has been deployed.

## notifications

An API to handle sending notifications to me.
