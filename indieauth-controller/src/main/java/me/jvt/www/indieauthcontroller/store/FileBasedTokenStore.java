package me.jvt.www.indieauthcontroller.store;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class FileBasedTokenStore implements TokenStore {

  private final String filename;

  public FileBasedTokenStore(String filename) {
    this.filename = filename;
  }

  @Override
  public String get() {
    File file = new File(filename);
    if (!file.exists()) {
      throw new IllegalStateException("File does not exist");
    }

    try {
      List<String> lines = Files.readAllLines(file.toPath());
      return lines.get(0);
    } catch (IOException e) {
      throw new IllegalStateException();
    }
  }

  @Override
  public void put(String token) {
    try {
      BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
      writer.write(token);
      writer.close();
    } catch (IOException e) {
      throw new IllegalStateException("Could not write", e);
    }
  }
}
