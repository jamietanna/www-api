package me.jvt.www.indieauthcontroller.store;

public class InMemoryTokenStore implements TokenStore {

  private String token;

  @Override
  public String get() {
    return token;
  }

  @Override
  public void put(String token) {
    this.token = token;
  }
}
