package me.jvt.www.indieauthcontroller.store.kubernetes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.kubernetes.client.custom.V1Patch;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class PatchBodyFactory {

  private final ObjectMapper objectMapper;

  public PatchBodyFactory(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  public PatchBody create(String path, String rawValue) {
    return new PatchBody(path, rawValue);
  }

  public V1Patch toPatch(PatchBody body) {
    return new V1Patch(serialize(body));
  }

  public String serialize(PatchBody patchBody) {
    try {
      return objectMapper.writeValueAsString(List.of(patchBody));
    } catch (JsonProcessingException e) {
      throw new IllegalStateException(e);
    }
  }
}
