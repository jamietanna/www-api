package me.jvt.www.indieauthcontroller.controller;

import java.util.List;
import me.jvt.www.indieauthclient.IndieAuthService;
import me.jvt.www.indieauthclient.IndieAuthService.TokenEndpointResponse;
import me.jvt.www.indieauthcontroller.store.TokenStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("${indieauthcontroller.prefix:}/indieauth")
public class IndieAuthController {

  private final IndieAuthService service;
  private final String me;
  private final RandomFacade facade;
  private final List<String> scopes;
  private final TokenStore store;

  @Autowired
  public IndieAuthController(
      IndieAuthService service,
      @Value("${indieauthcontroller.me}") String me,
      @Value("#{'${indieauthcontroller.scope}'.split(',')}") List<String> scopes,
      TokenStore tokenStore) {
    this.service = service;
    this.me = me;
    this.facade = new RandomFacade();
    this.scopes = scopes;
    this.store = tokenStore;
  }

  IndieAuthController(
      IndieAuthService service,
      String me,
      RandomFacade facade,
      List<String> scopes,
      TokenStore tokenStore) {
    this.service = service;
    this.me = me;
    this.facade = facade;
    this.scopes = scopes;
    this.store = tokenStore;
  }

  /**
   * Begin the IndieAuth flow.
   *
   * @return a redirect to the authorization server
   */
  @GetMapping("/start")
  public String start() {
    return String.format(
        "redirect:%s",
        service.constructAuthorizationUrl(facade.random(32), facade.random(32), me, scopes));
  }

  /**
   * Callback from the IndieAuth server.
   *
   * @param code the authorization coed
   * @param state the state for that request
   * @return OK / Bad Request HTTP response
   */
  @GetMapping("/callback")
  public ResponseEntity<Void> callback(String code, String state) {
    TokenEndpointResponse response = service.exchangeAuthorizationCode(code, state, null, me);
    if (!me.equals(response.getMe())) {
      return ResponseEntity.badRequest().build();
    }
    if (null != response.getRefreshToken()) {
      store.put(response.getRefreshToken());
    } else {
      store.put(response.getAccessToken());
    }
    return ResponseEntity.ok().build();
  }
}
