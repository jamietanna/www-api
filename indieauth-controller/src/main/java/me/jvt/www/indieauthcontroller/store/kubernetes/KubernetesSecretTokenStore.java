package me.jvt.www.indieauthcontroller.store.kubernetes;

import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.V1Secret;
import java.util.Map;
import me.jvt.www.indieauthcontroller.store.TokenStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KubernetesSecretTokenStore implements TokenStore {
  private static final Logger LOGGER = LoggerFactory.getLogger(KubernetesSecretTokenStore.class);
  private final String secretName;
  private final String namespace;
  private final String secretPath;
  private final PatchBodyFactory patchBodyFactory;
  private final CoreV1Api api;

  public KubernetesSecretTokenStore(
      String secretName,
      String namespace,
      String secretPath,
      PatchBodyFactory patchBodyFactory,
      ApiClient apiClient) {
    this(secretName, namespace, secretPath, patchBodyFactory, new CoreV1Api(apiClient));
  }

  KubernetesSecretTokenStore(
      String name,
      String namespace,
      String secretPath,
      PatchBodyFactory patchBodyFactory,
      CoreV1Api api) {
    this.secretName = name;
    this.namespace = namespace;
    this.secretPath = secretPath;
    this.patchBodyFactory = patchBodyFactory;
    this.api = api;
  }

  @Override
  public String get() {
    V1Secret secret;
    try {
      secret = api.readNamespacedSecret(secretName, namespace, null);
    } catch (ApiException e) {
      LOGGER.error(
          "Received an HTTP {} when retrieving the secret, with body `{}`, and response headers `{}`",
          e.getCode(),
          e.getResponseBody(),
          e.getResponseHeaders());
      throw new IllegalStateException(e);
    }

    Map<String, byte[]> data = secret.getData();
    if (data == null || !data.containsKey(secretPath)) {
      throw new IllegalStateException(
          String.format(
              "No data could be found in V1Secret{name=%s, namespace=%s}", secretName, namespace));
    }
    byte[] encoded = data.get(secretPath);
    return new String(encoded);
  }

  @Override
  public void put(String token) {
    try {
      api.patchNamespacedSecret(
          secretName,
          namespace,
          patchBodyFactory.toPatch(patchBodyFactory.create(secretPath, token)),
          null,
          null,
          KubernetesSecretTokenStore.class.getCanonicalName(),
          null);
    } catch (ApiException e) {
      LOGGER.error(
          "Received an HTTP {} when storing the secret, with body `{}`, and response headers `{}`",
          e.getCode(),
          e.getResponseBody(),
          e.getResponseHeaders());
      throw new IllegalStateException(e);
    }
  }
}
