package me.jvt.www.indieauthcontroller.controller;

import org.apache.commons.lang3.RandomStringUtils;

public class RandomFacade {
  public String random(int length) {
    return RandomStringUtils.randomAlphanumeric(length);
  }
}
