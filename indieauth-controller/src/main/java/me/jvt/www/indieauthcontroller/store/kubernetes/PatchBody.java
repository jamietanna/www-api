package me.jvt.www.indieauthcontroller.store.kubernetes;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class PatchBody {

  private final String path;
  private final String value;

  public PatchBody(String path, String rawValue) {
    this.path = "/data/" + path;
    this.value = Base64.getEncoder().encodeToString(rawValue.getBytes(StandardCharsets.UTF_8));
  }

  @JsonProperty("op")
  public String getOperation() {
    return "replace";
  }

  public String getPath() {
    return path;
  }

  public String getValue() {
    return value;
  }
}
