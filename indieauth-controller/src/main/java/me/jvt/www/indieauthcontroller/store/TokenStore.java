package me.jvt.www.indieauthcontroller.store;

public interface TokenStore {
  String get();

  void put(String token);
}
