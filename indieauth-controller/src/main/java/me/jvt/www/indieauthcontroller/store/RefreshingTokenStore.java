package me.jvt.www.indieauthcontroller.store;

import me.jvt.www.indieauthclient.IndieAuthService;
import me.jvt.www.indieauthclient.IndieAuthService.TokenEndpointResponse;

public class RefreshingTokenStore implements TokenStore {

  private final IndieAuthService service;
  private final TokenStore store;

  public RefreshingTokenStore(IndieAuthService service, TokenStore store) {
    this.service = service;
    this.store = store;
  }

  @Override
  public String get() {
    String refreshToken = store.get();
    if (null == refreshToken) {
      throw new IllegalStateException("The store does not have a token");
    }
    TokenEndpointResponse response = service.refreshRefreshToken(refreshToken);
    if (null != response.getRefreshToken()) {
      store.put(response.getRefreshToken());
    }
    return response.getAccessToken();
  }

  @Override
  public void put(String token) {
    store.put(token);
  }
}
