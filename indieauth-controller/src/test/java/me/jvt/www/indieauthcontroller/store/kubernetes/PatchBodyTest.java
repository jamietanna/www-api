package me.jvt.www.indieauthcontroller.store.kubernetes;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import me.jvt.www.indieauthcontroller.store.kubernetes.support.Application;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.test.context.ContextConfiguration;

@JsonTest
@ContextConfiguration(classes = Application.class)
class PatchBodyTest {

  @Autowired private JacksonTester<PatchBody> jackson;

  private JsonContent<PatchBody> asJson;

  @Nested
  class Serialisation {
    @BeforeEach
    void setup() throws IOException {
      PatchBody body = new PatchBody("client_secret", "raw-value");
      asJson = jackson.write(body);
    }

    @Test
    void setsOperation() {
      assertThat(asJson).extractingJsonPathStringValue("op").isEqualTo("replace");
    }

    @Test
    void setsPath() {
      assertThat(asJson).extractingJsonPathStringValue("path").isEqualTo("/data/client_secret");
    }

    @Test
    void setsEncodedValue() {
      assertThat(asJson).extractingJsonPathStringValue("value").isEqualTo("cmF3LXZhbHVl");
    }
  }
}
