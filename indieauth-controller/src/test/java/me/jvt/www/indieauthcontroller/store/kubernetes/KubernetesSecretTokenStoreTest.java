package me.jvt.www.indieauthcontroller.store.kubernetes;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.kubernetes.client.custom.V1Patch;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.V1Secret;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class KubernetesSecretTokenStoreTest {
  @Mock private PatchBodyFactory factory;
  @Mock private CoreV1Api api;
  private KubernetesSecretTokenStore store;

  @BeforeEach
  void setup() {
    store = new KubernetesSecretTokenStore("name", "namespace", "/the/path", factory, api);
  }

  @Nested
  class Get {
    private final String rawSecret = "the-secret";

    @Mock private V1Secret secret;

    void happyPath() throws ApiException {
      when(api.readNamespacedSecret(any(), any(), any())).thenReturn(secret);
      when(secret.getData())
          .thenReturn(
              Collections.singletonMap("/the/path", rawSecret.getBytes(StandardCharsets.UTF_8)));
    }

    @Test
    void delegatesToApiToRead() throws ApiException {
      happyPath();

      store.get();

      verify(api).readNamespacedSecret("name", "namespace", null);
    }

    @Test
    void retrievesSecretAndDecodes() throws ApiException {
      happyPath();

      String actual = store.get();

      assertThat(actual).isEqualTo(rawSecret);
    }

    @Test
    void throwsIllegalStateExceptionIfNoData() throws ApiException {
      when(api.readNamespacedSecret(any(), any(), any())).thenReturn(secret);

      assertThatThrownBy(() -> store.get())
          .isInstanceOf(IllegalStateException.class)
          .hasMessage("No data could be found in V1Secret{name=name, namespace=namespace}");
    }

    @Test
    void throwsIllegalStateExceptionIfDoesNotContainKey() throws ApiException {
      when(api.readNamespacedSecret(any(), any(), any())).thenReturn(secret);
      when(secret.getData()).thenReturn(Collections.emptyMap());

      assertThatThrownBy(() -> store.get())
          .isInstanceOf(IllegalStateException.class)
          .hasMessage("No data could be found in V1Secret{name=name, namespace=namespace}");
    }

    @Test
    void throwsIllegalStateExceptionIfApiError() throws ApiException {
      ApiException exception = new ApiException();
      when(api.readNamespacedSecret(any(), any(), any())).thenThrow(exception);

      assertThatThrownBy(() -> store.get())
          .isInstanceOf(IllegalStateException.class)
          .hasCause(exception);
    }
  }

  @Nested
  class Put {

    @Test
    void delegatesToApiToPatch(@Mock V1Patch patch) throws ApiException {
      when(factory.toPatch(any())).thenReturn(patch);

      store.put("foo");

      verify(api)
          .patchNamespacedSecret(
              "name",
              "namespace",
              patch,
              null,
              null,
              KubernetesSecretTokenStore.class.getCanonicalName(),
              null);
    }

    @Test
    void throwsIllegalStateExceptionIfApiError() throws ApiException {
      ApiException exception = new ApiException();
      when(api.patchNamespacedSecret(any(), any(), any(), any(), any(), any(), any()))
          .thenThrow(exception);

      assertThatThrownBy(() -> store.put(null))
          .isInstanceOf(IllegalStateException.class)
          .hasCause(exception);
    }
  }
}
