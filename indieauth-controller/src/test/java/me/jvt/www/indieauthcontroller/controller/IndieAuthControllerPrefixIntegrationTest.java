package me.jvt.www.indieauthcontroller.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import me.jvt.www.indieauthclient.IndieAuthService;
import me.jvt.www.indieauthclient.IndieAuthService.TokenEndpointResponse;
import me.jvt.www.indieauthcontroller.support.Application;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.MOCK,
    classes = Application.class,
    properties = {"indieauthcontroller.prefix=/foo"})
@AutoConfigureMockMvc
class IndieAuthControllerPrefixIntegrationTest {
  @Autowired private MockMvc mockMvc;
  @MockBean private IndieAuthService service;

  @Test
  void prefixIsPrependedToStart() throws Exception {
    when(service.constructAuthorizationUrl(any(), any(), any(), any())).thenReturn("url");

    this.mockMvc
        .perform(get("/foo/indieauth/start"))
        .andExpect(MockMvcResultMatchers.status().isFound());
  }

  @Test
  void prefixIsPrependedToCallback() throws Exception {
    TokenEndpointResponse response = new TokenEndpointResponse();
    response.setMe("https://me");
    when(service.exchangeAuthorizationCode(any(), any(), any(), any())).thenReturn(response);

    this.mockMvc
        .perform(
            get("/foo/indieauth/callback").param("code", "the-code").param("state", "random-value"))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }
}
