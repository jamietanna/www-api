package me.jvt.www.indieauthcontroller.store.kubernetes;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.kubernetes.client.custom.V1Patch;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PatchBodyFactoryTest {
  @Mock private ObjectMapper objectMapper;

  @InjectMocks private PatchBodyFactory factory;

  @Nested
  class Create {
    private PatchBody patchBody;

    @Test
    void itDelegatesPath() {
      patchBody = factory.create("foo", "");

      assertThat(patchBody.getPath()).isEqualTo("/data/foo");
    }

    @Test
    void itDelegatesValue() {
      patchBody = factory.create("", "the-raw-value");

      assertThat(patchBody.getValue())
          .isEqualTo(
              Base64.getEncoder().encodeToString("the-raw-value".getBytes(StandardCharsets.UTF_8)));
    }
  }

  @Nested
  class ToPatch {
    private final PatchBody patchBody = new PatchBody("foo", "bar");

    @Test
    void itReturnsValueFromObjectMapper() throws JsonProcessingException {
      when(objectMapper.writeValueAsString(any())).thenReturn("the-encoded");

      V1Patch patch = factory.toPatch(patchBody);

      assertThat(patch.getValue()).isEqualTo("the-encoded");
    }
  }

  @Nested
  class Serialize {
    private final PatchBody patchBody = new PatchBody("foo", "bar");

    @Test
    void itReturnsValueFromObjectMapper() throws JsonProcessingException {
      when(objectMapper.writeValueAsString(any())).thenReturn("the-encoded");

      String actual = factory.serialize(patchBody);

      assertThat(actual).isEqualTo("the-encoded");
    }

    @Test
    void throwsIllegalStateExceptionWhenJsonProcessingException(
        @Mock JsonProcessingException exception) throws JsonProcessingException {
      when(objectMapper.writeValueAsString(any())).thenThrow(exception);

      assertThatThrownBy(() -> factory.serialize(new PatchBody(null, "")))
          .isInstanceOf(IllegalStateException.class)
          .hasCause(exception);
    }
  }
}
