package me.jvt.www.indieauthcontroller.support;

import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.indieauthcontroller.store.InMemoryTokenStore;
import me.jvt.www.indieauthcontroller.store.TokenStore;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@ComponentScan("me.jvt.www.indieauthcontroller")
public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class);
  }

  @Configuration
  public static class Config {
    @Bean
    public RequestSpecificationFactory factory() {
      return new RequestSpecificationFactory();
    }

    @Bean
    public TokenStore tokenStore() {
      return new InMemoryTokenStore();
    }
  }
}
