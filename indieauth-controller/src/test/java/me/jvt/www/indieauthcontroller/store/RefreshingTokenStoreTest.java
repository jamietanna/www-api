package me.jvt.www.indieauthcontroller.store;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import me.jvt.www.indieauthclient.IndieAuthService;
import me.jvt.www.indieauthclient.IndieAuthService.TokenEndpointResponse;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RefreshingTokenStoreTest {
  @Mock private IndieAuthService service;
  @Mock private TokenStore delegate;

  @InjectMocks private RefreshingTokenStore store;

  @Nested
  class Get {
    private String accessToken;

    @Test
    void gets() {
      execute(true);

      verify(delegate).get();
    }

    @Test
    void passesDelegateTokenToService() {
      execute(true);

      verify(service).refreshRefreshToken("rt");
    }

    @Test
    void returnsAccessTokenFromService() {
      execute(true);

      assertThat(accessToken).isEqualTo("at");
    }

    @Test
    void storesRefreshTokenInInDelegateIfReturned() {
      execute(true);

      verify(delegate).put("rt.2");
    }

    @Test
    void doesNotStoreRefreshTokenInInDelegateIfNotReturned() {
      execute(false);

      verify(delegate, never()).put(any());
    }

    @Test
    void throwsIllegalStateExceptionWhenNoRefreshTokenFromStore() {
      when(delegate.get()).thenReturn(null);

      assertThatThrownBy(() -> store.get())
          .isInstanceOf(IllegalStateException.class)
          .hasMessage("The store does not have a token");
    }

    private void execute(boolean withRefresh) {
      when(delegate.get()).thenReturn("rt");
      TokenEndpointResponse response = new TokenEndpointResponse();
      response.setAccessToken("at");
      if (withRefresh) {
        response.setRefreshToken("rt.2");
      }
      when(service.refreshRefreshToken(any())).thenReturn(response);

      accessToken = store.get();
    }
  }

  @Nested
  class Put {
    @Test
    void delegates() {
      store.put("foo");

      verify(delegate).put("foo");
    }
  }
}
