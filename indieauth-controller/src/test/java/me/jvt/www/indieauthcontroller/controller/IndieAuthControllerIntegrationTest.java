package me.jvt.www.indieauthcontroller.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import me.jvt.www.indieauthclient.IndieAuthService;
import me.jvt.www.indieauthclient.IndieAuthService.TokenEndpointResponse;
import me.jvt.www.indieauthcontroller.support.Application;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = Application.class)
@AutoConfigureMockMvc
class IndieAuthControllerIntegrationTest {
  @Autowired private MockMvc mockMvc;
  @MockBean private IndieAuthService service;

  @Test
  void startReturnsRedirectToAuthorizationUrl() throws Exception {
    when(service.constructAuthorizationUrl(any(), any(), any(), any())).thenReturn("url");

    this.mockMvc
        .perform(get("/indieauth/start"))
        .andExpect(MockMvcResultMatchers.status().isFound())
        .andExpect(redirectedUrl("url"));
  }

  @Test
  void callbackMapsCode() throws Exception {
    when(service.exchangeAuthorizationCode(any(), any(), any(), any()))
        .thenReturn(new TokenEndpointResponse());

    this.mockMvc.perform(
        get("/indieauth/callback").param("code", "the-code").param("state", "random-value"));

    verify(service).exchangeAuthorizationCode(eq("the-code"), any(), any(), any());
  }

  @Test
  void callbackMapsState() throws Exception {
    when(service.exchangeAuthorizationCode(any(), any(), any(), any()))
        .thenReturn(new TokenEndpointResponse());

    this.mockMvc.perform(
        get("/indieauth/callback").param("code", "the-code").param("state", "random-value"));

    verify(service).exchangeAuthorizationCode(any(), eq("random-value"), any(), any());
  }
}
