package me.jvt.www.indieauthcontroller.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import me.jvt.www.indieauthclient.IndieAuthService;
import me.jvt.www.indieauthclient.IndieAuthService.TokenEndpointResponse;
import me.jvt.www.indieauthcontroller.store.TokenStore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
class IndieAuthControllerTest {

  @Mock private IndieAuthService service;

  @Mock private RandomFacade facade;
  @Mock private TokenStore store;

  private IndieAuthController controller;

  @BeforeEach
  void setup() {
    controller =
        new IndieAuthController(
            service, "https://me", facade, Collections.singletonList("woo"), store);
  }

  @Nested
  class Start {
    private String actual;

    @BeforeEach
    void setup() {
      when(facade.random(anyInt())).thenReturn("first", "second");
      when(service.constructAuthorizationUrl(any(), any(), any(), any()))
          .thenReturn("/authorize/..");
      actual = controller.start();
    }

    @Test
    void delegatesGeneratedState() {
      verify(service).constructAuthorizationUrl(eq("first"), any(), any(), any());
    }

    @Test
    void usesSufficientRandomnessForState() {
      verify(facade, atLeastOnce()).random(32);
    }

    @Test
    void delegatesGeneratedCodeVerifier() {
      verify(service).constructAuthorizationUrl(any(), eq("second"), any(), any());
    }

    @Test
    void usesSufficientRandomnessForCodeVerifier() {
      verify(facade, atLeastOnce()).random(32);
    }

    @Test
    void delegatesConfiguredMe() {
      verify(service).constructAuthorizationUrl(any(), any(), eq("https://me"), any());
    }

    @Test
    void delegatesConfiguredScopes() {
      List<String> expected = Collections.singletonList("woo");
      verify(service).constructAuthorizationUrl(any(), any(), any(), eq(expected));
    }

    @Test
    void itReturnsUrlFromClient() {
      assertThat(actual).isEqualTo("redirect:/authorize/..");
    }
  }

  @Nested
  class Callback {
    @Nested
    class HappyPath {

      private ResponseEntity<Void> actual;

      @BeforeEach
      void setup() {
        TokenEndpointResponse response = new TokenEndpointResponse();
        response.setMe("https://me");
        response.setAccessToken("token");
        when(service.exchangeAuthorizationCode(any(), any(), any(), any())).thenReturn(response);

        actual = controller.callback("code", "state");
      }

      @Test
      void itDelegatesCode() {
        verify(service).exchangeAuthorizationCode(eq("code"), any(), any(), any());
      }

      @Test
      void itDelegatesState() {
        verify(service).exchangeAuthorizationCode(any(), eq("state"), any(), any());
      }

      @Test
      void itDoesNotProvideCodeVerifier() {
        verify(service).exchangeAuthorizationCode(any(), any(), isNull(), any());
      }

      @Test
      void itProvidesConfiguredMe() {
        verify(service).exchangeAuthorizationCode(any(), any(), any(), eq("https://me"));
      }

      @Test
      void itSavesInStore() {
        verify(store).put("token");
      }

      @Test
      void itReturnsOk() {
        ResponseEntity<Void> expected = ResponseEntity.ok().build();

        assertThat(actual).isEqualTo(expected);
      }
    }

    @Nested
    class WhenRefreshToken {

      private ResponseEntity<Void> actual;

      @BeforeEach
      void setup() {
        TokenEndpointResponse response = new TokenEndpointResponse();
        response.setMe("https://me");
        response.setAccessToken("token");
        response.setRefreshToken("refresh_token");
        when(service.exchangeAuthorizationCode(any(), any(), any(), any())).thenReturn(response);

        actual = controller.callback("code", "state");
      }

      @Test
      void itSavesInStore() {
        verify(store).put("refresh_token");
      }
    }

    @Nested
    class SadPath {
      @Test
      void itReturns400WhenInvalidMeFromClient() {
        TokenEndpointResponse response = new TokenEndpointResponse();
        response.setMe("https://not-me");
        response.setAccessToken("token");
        when(service.exchangeAuthorizationCode(any(), any(), any(), any())).thenReturn(response);

        ResponseEntity<Void> expected = ResponseEntity.badRequest().build();

        ResponseEntity<Void> actual = controller.callback("code", "state");

        assertThat(actual).isEqualTo(expected);
      }
    }
  }
}
