package me.jvt.www.api.pushover.model;

public class Message {

  private final String title;
  private final String message;
  private final String url;

  public Message(String title, String message, String url) {
    this.title = title;
    this.message = message;
    this.url = url;
  }

  public String getTitle() {
    return title;
  }

  public String getMessage() {
    return message;
  }

  public String getUrl() {
    return url;
  }
}
