package me.jvt.www.api.pushover;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.pushover.model.Message;
import org.apache.http.entity.ContentType;

public class PushoverClient {

  private static final String PUSHOVER_BASE_URI = "https://api.pushover.net";
  private static final String MESSAGES_API_PATH = "/1/messages.json";

  private final RequestSpecificationFactory requestSpecificationFactory;
  private final String appToken;
  private final String userKey;

  public PushoverClient(
      RequestSpecificationFactory requestSpecificationFactory, String appToken, String userKey) {
    this.requestSpecificationFactory = requestSpecificationFactory;
    this.appToken = appToken;
    this.userKey = userKey;
  }

  public void sendMessage(Message message) throws IllegalArgumentException {
    RequestSpecification requestSpecification =
        requestSpecificationFactory.newRequestSpecification();

    Response pushoverResponse =
        requestSpecification
            .baseUri(PUSHOVER_BASE_URI)
            .contentType(ContentType.MULTIPART_FORM_DATA.toString())
            .multiPart("token", appToken)
            .multiPart("user", userKey)
            .multiPart("title", message.getTitle())
            .multiPart("message", message.getMessage())
            .multiPart("url", message.getUrl())
            .post(MESSAGES_API_PATH);

    int responseCodeFamily = pushoverResponse.getStatusCode() / 100;

    switch (responseCodeFamily) {
      case 4:
        throw new IllegalArgumentException(
            String.format(
                "Received an HTTP %d from Pushover with body %s",
                pushoverResponse.getStatusCode(), pushoverResponse.getBody().toString()));
      case 5:
        throw new RuntimeException(
            String.format("Received an HTTP %d from Pushover", pushoverResponse.getStatusCode()));
      default:
        // do nothing
    }
  }
}
