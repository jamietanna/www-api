package me.jvt.www.api.pushover;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.RETURNS_SELF;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.pushover.model.Message;
import org.apache.http.entity.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PushoverClientTest {

  RequestSpecification mockRequestSpecification;
  Response mockResponse;

  Message message;

  PushoverClient sut;

  @BeforeEach
  void setup() {
    mockRequestSpecification = mock(RequestSpecification.class, RETURNS_SELF);
    mockResponse = mock(Response.class);

    when(mockRequestSpecification.post(anyString())).thenReturn(mockResponse);

    RequestSpecificationFactory mockRequestSpecificationFactory =
        mock(RequestSpecificationFactory.class);
    when(mockRequestSpecificationFactory.newRequestSpecification())
        .thenReturn(mockRequestSpecification);

    sut = new PushoverClient(mockRequestSpecificationFactory, "app_token_here", "user_key_here");

    message = new Message("title_here", "Long winded message here", "https://www.jvt.me");
  }

  @Test
  void itSendsTheConfiguredAppToken() {
    sut.sendMessage(message);

    verify(mockRequestSpecification).multiPart("token", "app_token_here");
  }

  @Test
  void itSendsTheConfiguredUserKey() {
    sut.sendMessage(message);

    verify(mockRequestSpecification).multiPart("user", "user_key_here");
  }

  @Test
  void itSendsTheTitle() {
    sut.sendMessage(message);

    verify(mockRequestSpecification).multiPart("title", "title_here");
  }

  @Test
  void itSendsTheMessage() {
    sut.sendMessage(message);

    verify(mockRequestSpecification).multiPart("message", "Long winded message here");
  }

  @Test
  void itSendsTheUrl() {
    sut.sendMessage(message);

    verify(mockRequestSpecification).multiPart("url", "https://www.jvt.me");
  }

  @Test
  void itSendsAsMultipartForm() {
    sut.sendMessage(message);

    verify(mockRequestSpecification).contentType(ContentType.MULTIPART_FORM_DATA.toString());
  }

  @Test
  void itUsesBaseUri() {
    sut.sendMessage(message);

    verify(mockRequestSpecification).baseUri("https://api.pushover.net");
  }

  @Test
  void itSendsPostRequest() {
    sut.sendMessage(message);

    verify(mockRequestSpecification).post("/1/messages.json");
  }

  @Test
  void itDoesNotErrorOnSuccess() {
    when(mockResponse.getStatusCode()).thenReturn(200);

    sut.sendMessage(message);

    // no error
  }

  @Test
  void itThrowsIllegalArgumentExceptionIf4xxResponse() {
    when(mockResponse.getStatusCode()).thenReturn(400);
    ResponseBody responseBody = mock(ResponseBody.class);
    when(mockResponse.getBody()).thenReturn(responseBody);
    when(responseBody.toString()).thenReturn("{}");

    assertThatThrownBy(() -> sut.sendMessage(message))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Received an HTTP 400 from Pushover with body {}");
  }

  @Test
  void itThrowsRuntimeErrorIf5xxResponse() {
    when(mockResponse.getStatusCode()).thenReturn(503);

    assertThatThrownBy(() -> sut.sendMessage(message))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Received an HTTP 503 from Pushover");
  }
}
