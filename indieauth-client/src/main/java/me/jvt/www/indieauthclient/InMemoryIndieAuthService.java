package me.jvt.www.indieauthclient;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

public class InMemoryIndieAuthService implements IndieAuthService {

  private static final int MAX_ITEMS_IN_CACHE = 20;

  private final IndieAuthClient delegate;
  private final LRUCache stateMap;

  /**
   * Create a new {@link IndieAuthClient} that performs in-memory state validation.
   *
   * @param delegate the {@link IndieAuthClient} to perform the core operations
   */
  public InMemoryIndieAuthService(IndieAuthClient delegate) {
    this.delegate = delegate;
    stateMap = new LRUCache(MAX_ITEMS_IN_CACHE);
  }

  @Override
  public String constructAuthorizationUrl(
      String state, String codeVerifier, String me, List<String> scopes) {
    if (stateMap.containsKey(state)) {
      throw new IllegalStateException("Duplicate state used for requests, which is not allowed");
    }
    stateMap.put(state, new Request(me, codeVerifier));
    return delegate.constructAuthorizationUrl(state, codeVerifier, me, scopes);
  }

  @Override
  public TokenEndpointResponse exchangeAuthorizationCode(
      String code, String state, String codeVerifier, String me) throws IllegalArgumentException {
    if (!stateMap.containsKey(state)) {
      throw new IllegalArgumentException("State is invalid");
    }

    Request request = stateMap.get(state);
    if (!request.getMe().equals(me)) {
      throw new IllegalArgumentException("State is invalid");
    }

    TokenEndpointResponse tokenEndpointResponse =
        delegate.exchangeAuthorizationCode(code, state, request.getCodeVerifier(), me);
    stateMap.remove(state);
    return tokenEndpointResponse;
  }

  @Override
  public String verifyAuthorizationCode(String code, String state, String codeVerifier)
      throws IllegalArgumentException {
    if (!stateMap.containsKey(state)) {
      throw new IllegalArgumentException("State is invalid");
    }
    Request request = stateMap.remove(state);
    return delegate.verifyAuthorizationCode(code, state, request.getCodeVerifier());
  }

  @Override
  public String retrieveMeForState(String state) {
    Request request = stateMap.get(state);

    if (null == request) {
      throw new IllegalArgumentException("State is invalid");
    }
    return request.getMe();
  }

  @Override
  public TokenEndpointResponse refreshRefreshToken(String refreshToken)
      throws IllegalArgumentException {
    return delegate.refreshRefreshToken(refreshToken);
  }

  private static class LRUCache extends LinkedHashMap<String, Request> {

    private final int size;

    private LRUCache(int size) {
      super(size, 0.75f, true);
      this.size = size;
    }

    @Override
    protected boolean removeEldestEntry(Entry<String, Request> eldest) {
      return size() > size;
    }
  }

  private static class Request {
    private final String me;
    private final String codeVerifier;

    private Request(String me, String codeVerifier) {
      this.me = me;
      this.codeVerifier = codeVerifier;
    }

    public String getMe() {
      return me;
    }

    public String getCodeVerifier() {
      return codeVerifier;
    }
  }
}
