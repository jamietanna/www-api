package me.jvt.www.indieauthclient;

import io.restassured.response.Response;
import me.jvt.www.api.core.RequestSpecificationFactory;

public class DynamicIndieAuthConfiguration implements IndieAuthConfiguration {

  private final String profileUrl;
  private final RequestSpecificationFactory factory;

  public DynamicIndieAuthConfiguration(String profileUrl, RequestSpecificationFactory factory) {
    this.profileUrl = profileUrl;
    this.factory = factory;
  }

  @Override
  public String getAuthorizationEndpoint() {
    Response response = request();
    if (response.getStatusCode() != 200) {
      throw new IllegalStateException(
          String.format(
              "rel=authorization_endpoint could not be found for %s. HTTP %d was returned",
              profileUrl, response.getStatusCode()));
    }
    String authorizationEndpoint = response.path("rels.authorization_endpoint[0]");
    if (authorizationEndpoint == null) {
      throw new IllegalStateException(
          String.format("rel=authorization_endpoint could not be found for %s", profileUrl));
    }
    return authorizationEndpoint;
  }

  @Override
  public String getTokenEndpoint() {
    Response response = request();
    if (response.getStatusCode() != 200) {
      throw new IllegalStateException(
          String.format(
              "rel=token_endpoint could not be found for %s. HTTP %d was returned",
              profileUrl, response.getStatusCode()));
    }
    String authorizationEndpoint = response.path("rels.token_endpoint[0]");
    if (authorizationEndpoint == null) {
      throw new IllegalStateException(
          String.format("rel=token_endpoint could not be found for %s", profileUrl));
    }
    return authorizationEndpoint;
  }

  private Response request() {
    return factory
        .newRequestSpecification()
        .baseUri("https://php.microformats.io/")
        .queryParam("url", profileUrl)
        .get();
  }
}
