package me.jvt.www.indieauthclient;

public interface IndieAuthConfiguration {
  /**
   * Get the user's IndieAuth authorization endpoint.
   *
   * @return the authorization endpoint
   */
  String getAuthorizationEndpoint();

  /**
   * Get the user's IndieAuth token endpoint.
   *
   * @return the authorization endpoint
   */
  String getTokenEndpoint();
}
