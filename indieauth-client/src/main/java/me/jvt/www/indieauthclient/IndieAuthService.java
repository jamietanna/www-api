package me.jvt.www.indieauthclient;

import java.util.HashMap;
import java.util.List;

public interface IndieAuthService {
  /**
   * Provide the authorization URL for a given request, using the pre-configured IndieAuth
   * Authorization Server's URLs.
   *
   * <p>https://indieauth.spec.indieweb.org/#authorization-request
   *
   * @param state A parameter set by the client which will be included when the user is redirected
   *     back to the client. This is used to prevent CSRF attacks. The authorization server MUST
   *     return the unmodified state value back to the client.
   * @param codeVerifier the code verifier to use
   * @param scopes a list of scopes that the client is requesting
   * @return the URL that the user will be redirected to, with a generated <code>state</code>
   *     parameter
   */
  String constructAuthorizationUrl(
      String state, String codeVerifier, String me, List<String> scopes);
  /**
   * Provide the authorization URL for a given request, using the pre-configured IndieAuth
   * Authorization Server's URLs.
   *
   * <p>Performs validation of the incoming <code>state</code> parameter before exchanging.
   *
   * <p>https://indieauth.spec.indieweb.org/#access-token-verification
   *
   * @param code the authorization_code provided on the redirect
   * @param state the state that was provided on the redirect
   * @param codeVerifier
   * @param me the user's profile URL
   * @return the response from the access token endpoint
   * @throws IllegalArgumentException if the code is invalid
   * @throws IllegalArgumentException if the state is invalid
   * @throws IllegalArgumentException if the codeVerifier is invalid
   */
  TokenEndpointResponse exchangeAuthorizationCode(
      String code, String state, String codeVerifier, String me) throws IllegalArgumentException;

  /**
   * Validate the authorization code, and return the <code>me</code>.
   *
   * <p>https://indieauth.spec.indieweb.org/#authorization-code-verification-0
   *
   * @param code authorization_code returned from the redirect
   * @param state the state that was provided on the redirect
   * @param codeVerifier
   * @return the profile URL of the user (<code>me</code>)
   * @throws IllegalArgumentException if the state is invalid
   * @throws IllegalArgumentException if the codeVerifier is invalid
   */
  String verifyAuthorizationCode(String code, String state, String codeVerifier)
      throws IllegalArgumentException;

  /**
   * Return the profile URL (<code>me</code> value) for the principal going through the IndieAuth
   * journey.
   *
   * @param state the state value to map to a profile URL
   * @return the profile URL currently going through an IndieAuth flow
   * @throws IllegalArgumentException if the state is invalid
   */
  String retrieveMeForState(String state);

  /**
   * Refresh a refresh token, to retrieve new access token, and optionally a new referesh token.
   *
   * @param refreshToken the refresh token
   * @return the response from the access token endpoint
   * @throws IllegalArgumentException if the refreshToken is invalid
   */
  TokenEndpointResponse refreshRefreshToken(String refreshToken) throws IllegalArgumentException;

  class TokenEndpointResponse extends HashMap<String, String> {
    public String getAccessToken() {
      return get("access_token");
    }

    public void setAccessToken(String accessToken) {
      put("access_token", accessToken);
    }

    public String getMe() {
      return get("me");
    }

    public void setMe(String me) {
      put("me", me);
    }

    public String getRefreshToken() {
      return get("refresh_token");
    }

    public void setRefreshToken(String refreshToken) {
      put("refresh_token", refreshToken);
    }
  }
}
