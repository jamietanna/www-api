package me.jvt.www.indieauthclient;

import io.restassured.response.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.List;
import me.jvt.www.api.core.RequestSpecificationFactory;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.utils.URIBuilder;

public class IndieAuthClient implements IndieAuthService {

  private final URI authorizationEndpoint;
  private final URI tokenEndpoint;
  private final URI clientId;
  private final URI redirectUri;
  private final RequestSpecificationFactory requestSpecificationFactory;

  /**
   * Construct a new IndieAuth client.
   *
   * @param indieAuthConfiguration the configuration for the current user's IndieAuth server
   * @param clientId The client URL
   * @param redirectUri The redirect URL indicating where the user should be redirected to after
   */
  public IndieAuthClient(
      IndieAuthConfiguration indieAuthConfiguration,
      String clientId,
      String redirectUri,
      RequestSpecificationFactory requestSpecificationFactory) {
    try {
      this.authorizationEndpoint = new URI(indieAuthConfiguration.getAuthorizationEndpoint());
    } catch (URISyntaxException e) {
      throw new IllegalArgumentException("authorizationEndpoint was not a valid URI");
    }
    try {
      this.tokenEndpoint = new URI(indieAuthConfiguration.getTokenEndpoint());
    } catch (URISyntaxException e) {
      throw new IllegalArgumentException("tokenEndpoint was not a valid URI");
    }
    try {
      this.clientId = new URI(clientId);
    } catch (URISyntaxException e) {
      throw new IllegalArgumentException("clientId was not a valid URI");
    }
    try {
      this.redirectUri = new URI(redirectUri);
    } catch (URISyntaxException e) {
      throw new IllegalArgumentException("redirectUri was not a valid URI");
    }
    this.requestSpecificationFactory = requestSpecificationFactory;
  }

  /** {@inheritDoc} */
  @Override
  public String constructAuthorizationUrl(
      String state, String codeVerifier, String me, List<String> scopes) {
    if (null == state || state.isEmpty()) {
      throw new IllegalArgumentException("state parameter is required");
    }
    if (null == codeVerifier || codeVerifier.isEmpty()) {
      throw new IllegalArgumentException("codeVerifier parameter is required");
    }

    URIBuilder builder = new URIBuilder(authorizationEndpoint);

    builder.addParameter("redirect_uri", redirectUri.toString());
    builder.addParameter("client_id", clientId.toString());
    builder.addParameter("me", me);
    builder.addParameter("state", state);
    appendProofOfKeyCodeExchange(builder, codeVerifier);
    if (null != scopes && !scopes.isEmpty()) {
      builder.addParameter("scope", String.join(" ", scopes));
    }
    builder.addParameter("response_type", "code");

    try {
      return builder.build().toString();
    } catch (URISyntaxException e) {
      throw new RuntimeException(e);
    }
  }

  /** {@inheritDoc} */
  @Override
  public TokenEndpointResponse exchangeAuthorizationCode(
      String code, String state, String codeVerifier, String me) throws IllegalArgumentException {
    Response response =
        requestSpecificationFactory
            .newRequestSpecification()
            .header("accept", "application/json")
            .formParam("grant_type", "authorization_code")
            .formParam("code", code)
            .formParam("code_verifier", codeVerifier)
            .formParam("client_id", clientId.toString())
            .formParam("redirect_uri", redirectUri.toString())
            .formParam("me", me)
            .post(tokenEndpoint.toString());
    if (HttpStatus.SC_OK != response.getStatusCode()) {
      throw new IllegalArgumentException(
          String.format(
              "Token Endpoint returned %d for that authorization code", response.getStatusCode()));
    }
    TokenEndpointResponse tokenEndpointResponse = new TokenEndpointResponse();
    tokenEndpointResponse.setAccessToken(response.path("access_token"));
    tokenEndpointResponse.setRefreshToken(response.path("refresh_token"));
    tokenEndpointResponse.setMe(response.path("me"));
    return tokenEndpointResponse;
  }

  /** {@inheritDoc} */
  @Override
  public String verifyAuthorizationCode(String code, String state, String codeVerifier)
      throws IllegalArgumentException {
    Response response =
        requestSpecificationFactory
            .newRequestSpecification()
            .header("accept", "application/json")
            .formParam("code", code)
            .formParam("code_verifier", codeVerifier)
            .formParam("client_id", clientId.toString())
            .formParam("redirect_uri", redirectUri.toString())
            .post(authorizationEndpoint.toString());
    if (HttpStatus.SC_OK != response.getStatusCode()) {
      throw new IllegalArgumentException(
          String.format(
              "Authorization Endpoint returned %d for that authorization code",
              response.getStatusCode()));
    }

    return response.path("me");
  }

  /**
   * {@inheritDoc}
   *
   * @throws UnsupportedOperationException as this is not supported
   */
  @Override
  public String retrieveMeForState(String state) {
    throw new UnsupportedOperationException();
  }

  @Override
  public TokenEndpointResponse refreshRefreshToken(String refreshToken)
      throws IllegalArgumentException {
    Response response =
        requestSpecificationFactory
            .newRequestSpecification()
            .header("accept", "application/json")
            .formParam("grant_type", "refresh_token")
            .formParam("client_id", clientId.toString())
            .formParam("refresh_token", refreshToken)
            .post(tokenEndpoint.toString());
    if (HttpStatus.SC_OK != response.getStatusCode()) {
      throw new IllegalArgumentException(
          String.format(
              "Token Endpoint returned %d for that refresh token", response.getStatusCode()));
    }
    TokenEndpointResponse tokenEndpointResponse = new TokenEndpointResponse();
    tokenEndpointResponse.setAccessToken(response.path("access_token"));
    tokenEndpointResponse.setRefreshToken(response.path("refresh_token"));
    return tokenEndpointResponse;
  }

  private void appendProofOfKeyCodeExchange(URIBuilder builder, String codeVerifier) {
    String computedChallenge =
        Base64.getUrlEncoder().encodeToString(DigestUtils.sha256(codeVerifier));
    builder.addParameter("code_challenge", computedChallenge.replace("=", ""));
    builder.addParameter("code_challenge_method", "S256");
  }
}
