package me.jvt.www.indieauthclient;

public class StaticIndieAuthConfiguration implements IndieAuthConfiguration {
  private final String authorizationEndpoint;
  private final String tokenEndpoint;

  /**
   * Construct a {@link IndieAuthConfiguration} based on static configuration.
   *
   * @param authorizationEndpoint the authorization endpoint to use to authenticate the client
   * @param tokenEndpoint the token endpoint to use for retrieval of tokens
   */
  public StaticIndieAuthConfiguration(String authorizationEndpoint, String tokenEndpoint) {
    this.authorizationEndpoint = authorizationEndpoint;
    this.tokenEndpoint = tokenEndpoint;
  }

  @Override
  public String getAuthorizationEndpoint() {
    return authorizationEndpoint;
  }

  @Override
  public String getTokenEndpoint() {
    return tokenEndpoint;
  }
}
