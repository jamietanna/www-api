package me.jvt.www.indieauthclient;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.indieauthclient.IndieAuthService.TokenEndpointResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class IndieAuthClientTest {

  private static final String AUTHORIZATION_ENDPOINT = "https://authz/abc?page=1";
  private static final String TOKEN_ENDPOINT = "https://token";
  private static final String CLIENT_ID = "https://me.app";
  private static final String REDIRECT_URI = "https://redirect";

  // via https://stackoverflow.com/a/13500078/2257038
  private static final String INVALID_URI =
      "http://mw1.google.com/mw-earth-vectordb/kml-samples/gp/seattle/gigapxl/$[level]/r$[y]_c$[x].jpg";

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification mockRequestSpecification;

  @Mock private RequestSpecificationFactory mockFactory;

  @Mock private Response mockResponse;

  private IndieAuthClient sut;

  @BeforeEach
  void setup() {
    sut =
        new IndieAuthClient(
            new StaticIndieAuthConfiguration(AUTHORIZATION_ENDPOINT, TOKEN_ENDPOINT),
            CLIENT_ID,
            REDIRECT_URI,
            mockFactory);
  }

  @Nested
  class ConstructorThrowsIllegalArgumentExceptionIfInvalidUri {
    @Test
    void authorizationEndpoint() {
      assertThatThrownBy(
              () ->
                  new IndieAuthClient(
                      new StaticIndieAuthConfiguration(INVALID_URI, TOKEN_ENDPOINT),
                      CLIENT_ID,
                      REDIRECT_URI,
                      mockFactory))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("authorizationEndpoint was not a valid URI");
    }

    @Test
    void tokenEndpoint() {
      assertThatThrownBy(
              () ->
                  new IndieAuthClient(
                      new StaticIndieAuthConfiguration(TOKEN_ENDPOINT, INVALID_URI),
                      CLIENT_ID,
                      REDIRECT_URI,
                      mockFactory))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("tokenEndpoint was not a valid URI");
    }

    @Test
    void clientId() {
      assertThatThrownBy(
              () ->
                  new IndieAuthClient(
                      new StaticIndieAuthConfiguration(TOKEN_ENDPOINT, TOKEN_ENDPOINT),
                      INVALID_URI,
                      REDIRECT_URI,
                      mockFactory))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("clientId was not a valid URI");
    }

    @Test
    void redirectUri() {
      assertThatThrownBy(
              () ->
                  new IndieAuthClient(
                      new StaticIndieAuthConfiguration(TOKEN_ENDPOINT, TOKEN_ENDPOINT),
                      CLIENT_ID,
                      INVALID_URI,
                      mockFactory))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("redirectUri was not a valid URI");
    }
  }

  @Nested
  class ConstructAuthorizationUrl {
    private String url;

    @BeforeEach
    void setup() {
      url =
          sut.constructAuthorizationUrl(
              "the_state",
              "a6128783714cfda1d388e2e98b6ae8221ac31aca31959e59512c59f5",
              "https://me.me/",
              Arrays.asList("read", "write"));
    }

    @Test
    void itStartsWithAuthorizationEndpoint() {
      assertThat(url).startsWith("https://authz/abc?page=1&");
    }

    @Test
    void itUrlEncodesTheRedirectUri() {
      urlMatches("redirect_uri=https%3A%2F%2Fredirect");
    }

    @Test
    void itUrlEncodesTheClientId() {
      urlMatches("client_id=https%3A%2F%2Fme.app");
    }

    @Test
    void itUrlEncodesTheMe() {
      urlMatches("me=https%3A%2F%2Fme.me%2F");
    }

    @Test
    void itAddsState() {
      urlMatches("state=the_state");
    }

    @Test
    void itComputesAndAddsCodeChallenge() {
      urlMatches("code_challenge=OfYAxt8zU2dAPDWQxTAUIteRzMsoj9QBdMIVEDOErUo");
    }

    @Test
    void itAddsCodeChallengeMethodAsSha256() {
      urlMatches("code_challenge_method=S256");
    }

    @Test
    void itComputesAndAddsCodeChallengeAndStripsPadding() {
      url =
          sut.constructAuthorizationUrl(
              "another",
              "a4a03231cd563791a1862930fca82efa981159e2ea17300590a4d24757b4e77e",
              "https://me.me/",
              Arrays.asList("read", "write"));

      urlMatches("code_challenge=DwXhJ1p6ieibvO2BI_3Fu-FijjWwp5EKRBugL2oCw1w");
    }

    @ParameterizedTest
    @NullAndEmptySource
    void itThrowsIllegalArgumentExceptionIfStateIsNotAdded(String state) {
      assertThatThrownBy(
              () -> sut.constructAuthorizationUrl(state, "v", "", Collections.emptyList()))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("state parameter is required");
    }

    @ParameterizedTest
    @NullAndEmptySource
    void itThrowsIllegalArgumentExceptionIfCodeVerifierIsNotAdded(String codeVerifier) {
      assertThatThrownBy(
              () ->
                  sut.constructAuthorizationUrl("state", codeVerifier, "", Collections.emptyList()))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("codeVerifier parameter is required");
    }

    @Test
    void itUrlEncodesScopesIfPresent() {
      urlMatches("scope=read\\+write");
    }

    @ParameterizedTest
    @NullAndEmptySource
    void itDoesNotAddScopesIfNotPresent(List<String> scopes) {
      url = sut.constructAuthorizationUrl("the_state", "v", "", scopes);

      assertThat(url).doesNotContain("scope=");
    }

    @Test
    void itAddsResponseType() {
      urlMatches("response_type=code");
    }

    private void urlMatches(String regex) {
      assertThat(url).matches(String.format(".*%s.*", regex));
    }
  }

  @Nested
  class ExchangeAuthorizationCode {

    private TokenEndpointResponse tokenEndpointResponse;

    @BeforeEach
    void setup() {
      when(mockFactory.newRequestSpecification()).thenReturn(mockRequestSpecification);
      when(mockRequestSpecification.post(anyString())).thenReturn(mockResponse);
      when(mockResponse.getStatusCode()).thenReturn(200);
      when(mockResponse.path("access_token")).thenReturn("j.w.t");
      when(mockResponse.path("refresh_token")).thenReturn("r.t");
      when(mockResponse.path("me")).thenReturn("https://me.me/profile/");

      tokenEndpointResponse =
          sut.exchangeAuthorizationCode("authz_me", "state-here", "verifier", "https://foo.me");
    }

    @Test
    void itPostsToTokenEndpoint() {
      verify(mockRequestSpecification).post(TOKEN_ENDPOINT);
    }

    @Test
    void itSendsGrantType() {
      verify(mockRequestSpecification).formParam("grant_type", "authorization_code");
    }

    @Test
    void itSendsCode() {
      verify(mockRequestSpecification).formParam("code", "authz_me");
    }

    @Test
    void itSendsCodeVerifier() {
      verify(mockRequestSpecification).formParam("code_verifier", "verifier");
    }

    @Test
    void itSendsClientId() {
      verify(mockRequestSpecification).formParam("client_id", CLIENT_ID);
    }

    @Test
    void itSendsRedirectUri() {
      verify(mockRequestSpecification).formParam("redirect_uri", REDIRECT_URI);
    }

    @Test
    void itSendsMe() {
      verify(mockRequestSpecification).formParam("me", "https://foo.me");
    }

    @Test
    void throwsIllegalArgumentExceptionIfNot200OkResponse() {
      when(mockResponse.getStatusCode()).thenReturn(444);

      assertThatThrownBy(() -> sut.exchangeAuthorizationCode("authz_me", "", "v", ""))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Token Endpoint returned 444 for that authorization code");
    }

    @Test
    void itExpectsJSONResponse() {
      verify(mockRequestSpecification).header("accept", "application/json");
    }

    @Test
    void itReturnsAccessToken() {
      assertThat(tokenEndpointResponse.getAccessToken()).isEqualTo("j.w.t");
    }

    @Test
    void itReturnsRefreshToken() {
      assertThat(tokenEndpointResponse.getRefreshToken()).isEqualTo("r.t");
    }

    @Test
    void itReturnsMe() {
      assertThat(tokenEndpointResponse.getMe()).isEqualTo("https://me.me/profile/");
    }
  }

  @Nested
  class VerifyAuthorizationCode {

    private String me;

    @BeforeEach
    void setup() {
      when(mockFactory.newRequestSpecification()).thenReturn(mockRequestSpecification);
      when(mockRequestSpecification.post(anyString())).thenReturn(mockResponse);
      when(mockResponse.getStatusCode()).thenReturn(200);
      when(mockResponse.path("me")).thenReturn("https://me.me");

      me = sut.verifyAuthorizationCode("authz_me", null, "verifier");
    }

    @Test
    void itPostsToAuthorizationEndpoint() {
      verify(mockRequestSpecification).post(AUTHORIZATION_ENDPOINT);
    }

    @Test
    void itSendsCode() {
      verify(mockRequestSpecification).formParam("code", "authz_me");
    }

    @Test
    void itSendsCodeVerifier() {
      verify(mockRequestSpecification).formParam("code_verifier", "verifier");
    }

    @Test
    void itSendsClientId() {
      verify(mockRequestSpecification).formParam("client_id", CLIENT_ID);
    }

    @Test
    void itSendsRedirectUri() {
      verify(mockRequestSpecification).formParam("redirect_uri", REDIRECT_URI);
    }

    @Test
    void throwsIllegalArgumentExceptionIfNot200OkResponse() {
      when(mockResponse.getStatusCode()).thenReturn(444);

      assertThatThrownBy(() -> sut.verifyAuthorizationCode("authz_me", null, null))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Authorization Endpoint returned 444 for that authorization code");
    }

    @Test
    void itExpectsJSONResponse() {
      verify(mockRequestSpecification).header("accept", "application/json");
    }

    @Test
    void itReturnsMe() {
      assertThat(me).isEqualTo("https://me.me");
    }
  }

  @Nested
  class RetrieveMeForState {
    @Test
    void isNotSupported() {
      assertThatThrownBy(() -> sut.retrieveMeForState(""))
          .isInstanceOf(UnsupportedOperationException.class);
    }
  }

  @Nested
  class RefreshRefreshToken {

    private TokenEndpointResponse tokenEndpointResponse;

    @BeforeEach
    void setup() {
      when(mockFactory.newRequestSpecification()).thenReturn(mockRequestSpecification);
      when(mockRequestSpecification.post(anyString())).thenReturn(mockResponse);
      when(mockResponse.getStatusCode()).thenReturn(200);
      when(mockResponse.path("access_token")).thenReturn("j.w.t");
      when(mockResponse.path("refresh_token")).thenReturn("r.t.2");

      tokenEndpointResponse = sut.refreshRefreshToken("r.t");
    }

    @Test
    void itPostsToTokenEndpoint() {
      verify(mockRequestSpecification).post(TOKEN_ENDPOINT);
    }

    @Test
    void itSendsGrantType() {
      verify(mockRequestSpecification).formParam("grant_type", "refresh_token");
    }

    @Test
    void itSendsClientId() {
      verify(mockRequestSpecification).formParam("client_id", CLIENT_ID);
    }

    @Test
    void itSendsRefreshToken() {
      verify(mockRequestSpecification).formParam("refresh_token", "r.t");
    }

    @Test
    void throwsIllegalArgumentExceptionIfNot200OkResponse() {
      when(mockResponse.getStatusCode()).thenReturn(444);

      assertThatThrownBy(() -> sut.refreshRefreshToken("r.t"))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Token Endpoint returned 444 for that refresh token");
    }

    @Test
    void itExpectsJSONResponse() {
      verify(mockRequestSpecification).header("accept", "application/json");
    }

    @Test
    void itReturnsAccessToken() {
      assertThat(tokenEndpointResponse.getAccessToken()).isEqualTo("j.w.t");
    }

    @Test
    void itReturnsRefreshToken() {
      assertThat(tokenEndpointResponse.getRefreshToken()).isEqualTo("r.t.2");
    }
  }
}
