package me.jvt.www.indieauthclient;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import me.jvt.www.indieauthclient.IndieAuthService.TokenEndpointResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class InMemoryIndieAuthServiceTest {

  private static final String AUTHORIZATION_ENDPOINT = "https://authz/abc?page=1";
  private static final String TOKEN_ENDPOINT = "https://token";
  private static final String CLIENT_ID = "https://me.app";
  private static final String REDIRECT_URI = "https://redirect";

  @Mock private IndieAuthClient delegate;

  private InMemoryIndieAuthService sut;

  @BeforeEach
  void setup() {
    sut = new InMemoryIndieAuthService(delegate);
  }

  @Nested
  class ConstructAuthorizationUrl {

    private String url;

    @BeforeEach
    void setup() {
      when(delegate.constructAuthorizationUrl(anyString(), anyString(), anyString(), any()))
          .thenReturn("https://something");
      url =
          sut.constructAuthorizationUrl(
              "the_state", "the-verifier", "https://me", Collections.singletonList("abc"));
    }

    @Test
    void itDelegates() {
      verify(delegate)
          .constructAuthorizationUrl(
              "the_state", "the-verifier", "https://me", Collections.singletonList("abc"));
    }

    @Test
    void itReturnsFromDelegate() {
      assertThat(url).isEqualTo("https://something");
    }

    @Test
    void itDoesNotAllowDuplicateState() {
      assertThatThrownBy(
              () ->
                  sut.constructAuthorizationUrl(
                      "the_state", null, "https://me", Collections.singletonList("abc")))
          .isInstanceOf(IllegalStateException.class)
          .hasMessage("Duplicate state used for requests, which is not allowed");
    }
  }

  @Nested
  class ExchangeAuthorizationCode {
    @Mock private TokenEndpointResponse mockResponse;

    private TokenEndpointResponse tokenEndpointResponse;

    @BeforeEach
    void setup() {
      when(delegate.exchangeAuthorizationCode(anyString(), anyString(), anyString(), anyString()))
          .thenReturn(mockResponse);

      sut.constructAuthorizationUrl(
          "the_state", "verifier", "https://foo.me", Collections.singletonList("abc"));
      tokenEndpointResponse =
          sut.exchangeAuthorizationCode(
              "authz_me", "the_state", "ignored-verifier", "https://foo.me");
    }

    @Test
    void itDelegates() {
      // note that codeVerifier is retrieved from the state map
      verify(delegate)
          .exchangeAuthorizationCode("authz_me", "the_state", "verifier", "https://foo.me");
    }

    @Test
    void itReturnsFromDelegate() {
      assertThat(tokenEndpointResponse).isEqualTo(mockResponse);
    }

    @Nested
    class StateValidation {
      @Test
      void itDoesNotFailWhenConstructedFirst() {
        sut.constructAuthorizationUrl(
            "new-state", null, "https://foo.me", Collections.singletonList("abc"));
        sut.exchangeAuthorizationCode("authz_me", "new-state", null, "https://foo.me");
      }

      @Test
      void itThrowsIllegalArgumentExceptionWhenStateHasDifferentMe() {
        sut.constructAuthorizationUrl(
            "new-state", null, "https://me", Collections.singletonList("abc"));

        assertThatThrownBy(
                () ->
                    sut.exchangeAuthorizationCode("authz_me", "new-state", null, "https://foo.me"))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("State is invalid");
      }

      @Test
      void itThrowsIllegalArgumentExceptionWhenStateDoesNotExist() {
        assertThatThrownBy(
                () ->
                    sut.exchangeAuthorizationCode(
                        "authz_me", "unused_state", null, "https://foo.me"))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("State is invalid");
      }

      @Test
      void itThrowsIllegalArgumentExceptionOnExchangeWhenStateAlreadyUsed() {
        sut.constructAuthorizationUrl(
            "new-state", null, "https://foo.me", Collections.singletonList("abc"));
        sut.exchangeAuthorizationCode("authz_me", "new-state", null, "https://foo.me");
        assertThatThrownBy(
                () ->
                    sut.exchangeAuthorizationCode("authz_me", "new-state", null, "https://foo.me"))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("State is invalid");
      }

      @Test
      void itHasALimitOf20Items() {
        for (int i = 0; i < 21; i++) {
          sut.constructAuthorizationUrl(
              createState(i), null, "https://me", Collections.singletonList("abc"));
        }

        assertThatThrownBy(
                () ->
                    sut.exchangeAuthorizationCode(
                        "authz_me", createState(0), "verifier", "https://foo.me"))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("State is invalid");
      }

      private String createState(int i) {
        return String.format("state-%d", i);
      }
    }
  }

  @Nested
  class VerifyAuthorizationCode {
    private String me;

    @BeforeEach
    void setup() {
      sut.constructAuthorizationUrl(
          "new-state", "verifier", "https://me", Collections.singletonList("abc"));
      when(delegate.verifyAuthorizationCode(anyString(), anyString(), anyString()))
          .thenReturn("https://me.me");

      me = sut.verifyAuthorizationCode("authz_me", "new-state", "ignored");
    }

    @Test
    void itDelegates() {
      // note that codeVerifier is retrieved from the state map
      verify(delegate).verifyAuthorizationCode("authz_me", "new-state", "verifier");
    }

    @Test
    void itReturnsFromDelegate() {
      assertThat(me).isEqualTo("https://me.me");
    }
  }

  @Nested
  class RetrieveMeForState {
    @Test
    void itReturnsMeIfAlreadySet() {
      sut.constructAuthorizationUrl(
          "new-state", "verifier", "https://me", Collections.singletonList("abc"));

      String actual = sut.retrieveMeForState("new-state");

      assertThat(actual).isEqualTo("https://me");
    }

    @Test
    void itThrowsIllegalArgumentExceptionWhenStateIsNotSet() {
      assertThatThrownBy(() -> sut.retrieveMeForState("state"))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("State is invalid");
    }

    @Nested
    class StateValidation {
      @Test
      void itDoesNotFailWhenConstructedFirst() {
        sut.constructAuthorizationUrl(
            "new-state", "verifier", "https://foo.me", Collections.singletonList("abc"));
        sut.verifyAuthorizationCode("authz_me", "new-state", "verifier");
      }

      @Test
      void itThrowsIllegalArgumentExceptionWhenStateDoesNotExist() {
        assertThatThrownBy(
                () -> sut.verifyAuthorizationCode("authz_me", "unused_state", "verifier"))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("State is invalid");
      }

      @Test
      void itThrowsIllegalArgumentExceptionOnExchangeWhenStateAlreadyUsed() {
        sut.constructAuthorizationUrl(
            "new-state", "verifier", "https://foo.me", Collections.singletonList("abc"));
        sut.verifyAuthorizationCode("authz_me", "new-state", "verifier");
        assertThatThrownBy(() -> sut.verifyAuthorizationCode("authz_me", "new-state", "verifier"))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("State is invalid");
      }

      @Test
      void itHasALimitOf20Items() {
        for (int i = 0; i < 21; i++) {
          sut.constructAuthorizationUrl(
              createState(i), null, "https://me", Collections.singletonList("abc"));
        }

        assertThatThrownBy(
                () -> sut.verifyAuthorizationCode("authz_me", createState(0), "verifier"))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("State is invalid");
      }

      private String createState(int i) {
        return String.format("state-%d", i);
      }
    }
  }

  @Nested
  class RefreshRefreshToken {
    @Mock TokenEndpointResponse expected;
    private TokenEndpointResponse response;

    @BeforeEach
    void setup() {
      when(delegate.refreshRefreshToken(any())).thenReturn(expected);

      response = sut.refreshRefreshToken("rt");
    }

    @Test
    void delegates() {
      verify(delegate).refreshRefreshToken("rt");
    }

    @Test
    void returnsFromDelegate() {
      assertThat(expected).isEqualTo(response);
    }
  }
}
