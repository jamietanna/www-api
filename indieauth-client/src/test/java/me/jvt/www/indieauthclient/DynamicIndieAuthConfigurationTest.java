package me.jvt.www.indieauthclient;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import me.jvt.www.api.core.RequestSpecificationFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DynamicIndieAuthConfigurationTest {
  @Mock private RequestSpecificationFactory factory;

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification requestSpecification;

  @Mock private Response response;

  private DynamicIndieAuthConfiguration configuration;

  @BeforeEach
  void setup() {
    configuration = new DynamicIndieAuthConfiguration("https://profile", factory);
    when(factory.newRequestSpecification()).thenReturn(requestSpecification);
  }

  @Nested
  class GetAuthorizationEndpoint {
    private String actual;

    @Nested
    class WhenFound {

      @BeforeEach
      void setup() {
        when(requestSpecification.get()).thenReturn(response);
        when(response.getStatusCode()).thenReturn(200);
        when(response.path("rels.authorization_endpoint[0]")).thenReturn("https://auth-endpoint");

        actual = configuration.getAuthorizationEndpoint();
      }

      @Test
      void callsPhpMicroformatsIo() {
        verify(requestSpecification).baseUri("https://php.microformats.io/");
      }

      @Test
      void addsProfileUrl() {
        verify(requestSpecification).queryParam("url", "https://profile");
      }

      @Test
      void sendsGet() {
        verify(requestSpecification).get();
      }

      @Test
      void returnsRelAuthorizationEndpoint() {
        assertThat(actual).isEqualTo("https://auth-endpoint");
      }
    }

    @Nested
    class WhenNotSuccessfulRequest {
      @Test
      void itThrowsIllegalStateException() {
        when(requestSpecification.get()).thenReturn(response);
        when(response.getStatusCode()).thenReturn(499);

        assertThatThrownBy(() -> configuration.getAuthorizationEndpoint())
            .isInstanceOf(IllegalStateException.class)
            .hasMessage(
                "rel=authorization_endpoint could not be found for https://profile. HTTP 499 was returned");
      }
    }

    @Nested
    class WhenNotFound {
      @Test
      void itThrowsIllegalStateException() {
        when(requestSpecification.get()).thenReturn(response);
        when(response.getStatusCode()).thenReturn(200);

        assertThatThrownBy(() -> configuration.getAuthorizationEndpoint())
            .isInstanceOf(IllegalStateException.class)
            .hasMessage("rel=authorization_endpoint could not be found for https://profile");
      }
    }
  }

  @Nested
  class GetTokenEndpoint {
    private String actual;

    @Nested
    class WhenFound {

      @BeforeEach
      void setup() {
        when(requestSpecification.get()).thenReturn(response);
        when(response.getStatusCode()).thenReturn(200);
        when(response.path("rels.token_endpoint[0]")).thenReturn("https://token-endpoint");

        actual = configuration.getTokenEndpoint();
      }

      @Test
      void callsPhpMicroformatsIo() {
        verify(requestSpecification).baseUri("https://php.microformats.io/");
      }

      @Test
      void addsProfileUrl() {
        verify(requestSpecification).queryParam("url", "https://profile");
      }

      @Test
      void sendsGet() {
        verify(requestSpecification).get();
      }

      @Test
      void returnsRelTokenEndpoint() {
        assertThat(actual).isEqualTo("https://token-endpoint");
      }
    }

    @Nested
    class WhenNotSuccessfulRequest {
      @Test
      void itThrowsIllegalStateException() {
        when(requestSpecification.get()).thenReturn(response);
        when(response.getStatusCode()).thenReturn(499);

        assertThatThrownBy(() -> configuration.getTokenEndpoint())
            .isInstanceOf(IllegalStateException.class)
            .hasMessage(
                "rel=token_endpoint could not be found for https://profile. HTTP 499 was returned");
      }
    }

    @Nested
    class WhenNotFound {
      @Test
      void itThrowsIllegalStateException() {
        when(requestSpecification.get()).thenReturn(response);
        when(response.getStatusCode()).thenReturn(200);

        assertThatThrownBy(() -> configuration.getTokenEndpoint())
            .isInstanceOf(IllegalStateException.class)
            .hasMessage("rel=token_endpoint could not be found for https://profile");
      }
    }
  }
}
