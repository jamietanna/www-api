package me.jvt.www.api.analytics.exception;

import me.jvt.www.api.analytics.service.AnalyticsService;

/**
 * An exception to denote that an error occurred when interacting with a {@link AnalyticsService}.
 */
public class AnalyticsServiceException extends Exception {
  public AnalyticsServiceException(String message) {
    super(message);
  }
}
