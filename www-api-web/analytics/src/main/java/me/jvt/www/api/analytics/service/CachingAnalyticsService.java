package me.jvt.www.api.analytics.service;

import java.time.LocalDate;
import java.util.List;
import me.jvt.www.api.analytics.exception.AnalyticsServiceException;
import me.jvt.www.api.analytics.model.Page;
import org.springframework.cache.annotation.Cacheable;

/**
 * A decorator for the {@link AnalyticsService} that adds caching for a delegated AnalyticsService.
 */
public class CachingAnalyticsService implements AnalyticsService {

  private final AnalyticsService delegate;

  public CachingAnalyticsService(AnalyticsService delegate) {
    this.delegate = delegate;
  }

  @Override
  @Cacheable("pages")
  public List<Page> getPages(LocalDate date) throws AnalyticsServiceException {
    return delegate.getPages(date);
  }
}
