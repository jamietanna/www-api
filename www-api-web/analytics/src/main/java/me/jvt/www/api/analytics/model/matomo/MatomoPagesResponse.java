package me.jvt.www.api.analytics.model.matomo;

import java.util.List;

public class MatomoPagesResponse { // TODO: remove this
  private List<MatomoPage> pages;

  public void setPages(List<MatomoPage> pages) {
    this.pages = pages;
  }

  public List<MatomoPage> getPages() {
    return pages;
  }
}
