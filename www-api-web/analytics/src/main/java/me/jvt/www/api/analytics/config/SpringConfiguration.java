package me.jvt.www.api.analytics.config;

import me.jvt.www.api.analytics.service.AnalyticsService;
import me.jvt.www.api.analytics.service.CachingAnalyticsService;
import me.jvt.www.api.analytics.service.MatomoAnalyticsService;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.logging.LoggingConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(LoggingConfiguration.class)
public class SpringConfiguration {

  @Bean
  public RequestSpecificationFactory requestSpecificationFactory() {
    return new RequestSpecificationFactory();
  }

  @Bean
  public AnalyticsService analyticsService(
      RequestSpecificationFactory requestSpecificationFactory,
      @Value("${analytics.baseUrl}") String analyticsBaseUrl,
      @Value("${analytics.token}") String analyticsToken) {
    AnalyticsService service =
        new MatomoAnalyticsService(requestSpecificationFactory, analyticsBaseUrl, analyticsToken);

    return new CachingAnalyticsService(service);
  }
}
