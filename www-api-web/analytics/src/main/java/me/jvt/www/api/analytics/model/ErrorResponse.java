package me.jvt.www.api.analytics.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorResponse {

  public ErrorResponse(String error, String errorDescription) {
    this.error = error;
    this.errorDescription = errorDescription;
  }

  private final String error;
  private final String errorDescription;

  public String getError() {
    return error;
  }

  @JsonProperty("error_description")
  public String getErrorDescription() {
    return errorDescription;
  }
}
