package me.jvt.www.api.analytics.model.matomo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import me.jvt.www.api.analytics.model.Page;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MatomoPage {

  @JsonProperty("label")
  private String label;

  @JsonProperty("nb_visits")
  private int visits;

  @JsonProperty("nb_uniq_visitors")
  private int uniqueVisits;

  public Page asPage() {
    Page page = new Page();
    page.setTitle(label);
    page.setVisits(visits);
    page.setUniqueVisits(uniqueVisits);
    return page;
  }

  public void setLabel(String label) {

    this.label =
        label
            .replace("· Jamie Tanna | Software (Quality) Engineer", "")
            .replace("| Software (Quality) Engineer", "")
            .trim();
  }
}
