package me.jvt.www.api.analytics.config;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableCaching
@EnableScheduling
public class CacheConfiguration {
  @Scheduled(cron = "0 0/30 * * * *")
  @CacheEvict(value = "pages")
  public void evictPagesCache() {}
}
