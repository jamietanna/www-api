package me.jvt.www.api.analytics.service;

import java.time.LocalDate;
import java.util.List;
import me.jvt.www.api.analytics.exception.AnalyticsServiceException;
import me.jvt.www.api.analytics.model.Page;

public interface AnalyticsService {

  /**
   * Get information for the viewed pages, in descending order, on a given {@link LocalDate}.
   *
   * @param date the date to get the information for
   * @return the analytics information for a given collection of {@link Page}s
   * @throws AnalyticsServiceException if there are errors retrieving the data
   */
  List<Page> getPages(LocalDate date) throws AnalyticsServiceException;
}
