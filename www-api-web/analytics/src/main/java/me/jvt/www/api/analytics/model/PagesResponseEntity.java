package me.jvt.www.api.analytics.model;

import java.util.List;

public class PagesResponseEntity {

  public PagesResponseEntity() {}

  public PagesResponseEntity(List<Page> pages) {
    this.pages = pages;
  }

  private List<Page> pages;

  public List<Page> getPages() {
    return pages;
  }

  public void setPages(List<Page> pages) {
    this.pages = pages;
  }
}
