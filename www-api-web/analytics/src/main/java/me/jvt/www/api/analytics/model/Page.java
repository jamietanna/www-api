package me.jvt.www.api.analytics.model;

import com.fasterxml.jackson.annotation.JsonGetter;

public class Page {

  private String title;
  private int visits;
  private int uniqueVisits;

  public Page() {}

  public Page(String title, int visits, int uniqueVisits) {
    this.title = title;
    this.visits = visits;
    this.uniqueVisits = uniqueVisits;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getVisits() {
    return visits;
  }

  public void setVisits(int visits) {
    this.visits = visits;
  }

  @JsonGetter("unique_visits")
  public int getUniqueVisits() {
    return uniqueVisits;
  }

  public void setUniqueVisits(int uniqueVisits) {
    this.uniqueVisits = uniqueVisits;
  }
}
