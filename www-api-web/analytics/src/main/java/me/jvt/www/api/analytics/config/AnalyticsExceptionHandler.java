package me.jvt.www.api.analytics.config;

import me.jvt.www.api.analytics.exception.AnalyticsServiceException;
import me.jvt.www.api.analytics.model.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class AnalyticsExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(AnalyticsServiceException.class)
  public final ResponseEntity<ErrorResponse> handleAnalyticsServiceException(
      AnalyticsServiceException exception, WebRequest req) {
    ErrorResponse errorResponse =
        new ErrorResponse("internal_server_error", exception.getMessage());
    return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
