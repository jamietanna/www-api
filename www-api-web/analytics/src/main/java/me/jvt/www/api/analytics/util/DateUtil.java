package me.jvt.www.api.analytics.util;

import java.time.LocalDate;
import org.springframework.stereotype.Component;

@Component
public class DateUtil {
  public LocalDate today() {
    return LocalDate.now();
  }
}
