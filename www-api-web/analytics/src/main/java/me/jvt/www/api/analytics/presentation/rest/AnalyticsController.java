package me.jvt.www.api.analytics.presentation.rest;

import java.util.List;
import me.jvt.www.api.analytics.exception.AnalyticsServiceException;
import me.jvt.www.api.analytics.model.Page;
import me.jvt.www.api.analytics.model.PagesResponseEntity;
import me.jvt.www.api.analytics.service.AnalyticsService;
import me.jvt.www.api.analytics.util.DateUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AnalyticsController {

  private final AnalyticsService analyticsService;
  private final DateUtil dateUtil;

  public AnalyticsController(AnalyticsService analyticsService, DateUtil dateUtil) {
    this.analyticsService = analyticsService;
    this.dateUtil = dateUtil;
  }

  @GetMapping("/analytics/pages")
  public ResponseEntity<PagesResponseEntity> pages() throws AnalyticsServiceException {
    List<Page> pages = analyticsService.getPages(dateUtil.today());
    return new ResponseEntity<>(new PagesResponseEntity(pages), HttpStatus.OK);
  }
}
