package me.jvt.www.api.analytics.service;

import io.restassured.response.Response;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import me.jvt.www.api.analytics.exception.AnalyticsServiceException;
import me.jvt.www.api.analytics.model.Page;
import me.jvt.www.api.analytics.model.matomo.MatomoPage;
import me.jvt.www.api.core.RequestSpecificationFactory;

/** An implementation of the {@link AnalyticsService} that speaks to a Matomo analytics server. */
public class MatomoAnalyticsService implements AnalyticsService {

  private final RequestSpecificationFactory requestSpecificationFactory;
  private final String baseUrl;
  private final String token;

  public MatomoAnalyticsService(
      RequestSpecificationFactory requestSpecificationFactory, String baseUrl, String token) {
    this.requestSpecificationFactory = requestSpecificationFactory;
    this.baseUrl = baseUrl;
    this.token = token;
  }

  @Override
  public List<Page> getPages(LocalDate date) throws AnalyticsServiceException {
    Response response =
        requestSpecificationFactory
            .newRequestSpecification()
            .queryParam("date", date.format(DateTimeFormatter.ISO_DATE))
            .queryParam("depth", "10")
            .queryParam("expanded", "true")
            .queryParam("format", "JSON")
            .queryParam("idSite", "1")
            .queryParam("module", "API")
            .queryParam("method", "Actions.getPageTitles")
            .queryParam("period", "day")
            .queryParam("token_auth", token)
            .get(baseUrl);
    if (200 != response.getStatusCode()) {
      throw new AnalyticsServiceException(
          String.format("HTTP %d error from Matomo", response.getStatusCode()));
    }
    MatomoPage[] pages = response.as(MatomoPage[].class);

    return Arrays.stream(pages).map(MatomoPage::asPage).collect(Collectors.toList());
  }
}
