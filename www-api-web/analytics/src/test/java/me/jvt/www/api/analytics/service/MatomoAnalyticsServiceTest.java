package me.jvt.www.api.analytics.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.restassured.response.Response;
import io.restassured.specification.AuthenticationSpecification;
import io.restassured.specification.RequestSpecification;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Stream;
import me.jvt.www.api.analytics.exception.AnalyticsServiceException;
import me.jvt.www.api.analytics.model.Page;
import me.jvt.www.api.analytics.model.matomo.MatomoPage;
import me.jvt.www.api.core.RequestSpecificationFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class MatomoAnalyticsServiceTest {

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification mockRequestSpecification;

  @Mock private Response mockResponse;
  @Mock private MatomoPage mockMatomoPage;
  @Mock private Page mockPage;

  private MatomoAnalyticsService sut;
  private LocalDate date;

  static Stream<Arguments> getPagesQuerystring() {
    return Stream.of(
        Arguments.of("depth", "10"),
        Arguments.of("expanded", "true"),
        Arguments.of("format", "JSON"),
        Arguments.of("idSite", "1"),
        Arguments.of("module", "API"),
        Arguments.of("method", "Actions.getPageTitles"),
        Arguments.of("period", "day"));
  }

  @BeforeEach
  void setup() {
    date = LocalDate.now();

    RequestSpecificationFactory mockFactory = mock(RequestSpecificationFactory.class);
    when(mockFactory.newRequestSpecification()).thenReturn(mockRequestSpecification);

    AuthenticationSpecification mockAuthenticationSpecification =
        mock(AuthenticationSpecification.class);
    when(mockRequestSpecification.auth()).thenReturn(mockAuthenticationSpecification);
    when(mockRequestSpecification.get(anyString())).thenReturn(mockResponse);

    when(mockResponse.as(any(Class.class))).thenReturn(new MatomoPage[] {mockMatomoPage});
    when(mockResponse.getStatusCode()).thenReturn(200);
    when(mockMatomoPage.asPage()).thenReturn(mockPage);

    sut = new MatomoAnalyticsService(mockFactory, "https://stats.jvt.me", "fake.token");
  }

  @Test
  void getPagesCallsConfiguredMatomoEndpoint() throws AnalyticsServiceException {
    // given

    // when
    sut.getPages(date);

    // then
    verify(mockRequestSpecification).get("https://stats.jvt.me");
  }

  @ParameterizedTest
  @MethodSource("getPagesQuerystring")
  void getPagesSetsQuerystring(String parameter, String value) throws AnalyticsServiceException {
    // given

    // when
    sut.getPages(date);

    // then
    verify(mockRequestSpecification).queryParam(parameter, value);
  }

  @Test
  void getPagesSendsDate() throws AnalyticsServiceException {
    // given
    LocalDate date = LocalDate.of(2020, 1, 5);

    // when
    sut.getPages(date);

    // then
    verify(mockRequestSpecification).queryParam("date", "2020-01-05");
  }

  @Test
  void getPagesSendsConfiguredTokenAuth() throws AnalyticsServiceException {
    // given

    // when
    sut.getPages(date);

    // then
    verify(mockRequestSpecification).queryParam("token_auth", "fake.token");
  }

  @Test
  void getPagesReturnsResponseFromApi() throws AnalyticsServiceException {
    // given

    // when
    List<Page> actual = sut.getPages(date);

    // then
    assertThat(actual).hasSize(1);
    assertThat(actual.get(0)).isSameAs(mockPage);
  }

  @Test
  void getPagesReturnsResponseFromApiWhenMultiplePages() throws AnalyticsServiceException {
    // given
    MatomoPage mockMatomoPage1 = mock(MatomoPage.class);
    when(mockResponse.as(any(Class.class)))
        .thenReturn(new MatomoPage[] {mockMatomoPage, mockMatomoPage1});
    Page mockPage1 = mock(Page.class);
    when(mockMatomoPage1.asPage()).thenReturn(mockPage1);

    // when
    List<Page> actual = sut.getPages(date);

    // then
    assertThat(actual).containsExactly(mockPage, mockPage1);
  }

  @Test
  void getPagesThrowsAnalyticsServiceExceptionWhenNon2xxResponse() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(400);

    // when
    assertThatThrownBy(() -> sut.getPages(date))
        // then
        .isInstanceOf(AnalyticsServiceException.class)
        .hasMessage("HTTP 400 error from Matomo");
  }
}
