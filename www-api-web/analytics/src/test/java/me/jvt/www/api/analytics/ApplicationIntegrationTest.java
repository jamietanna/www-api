package me.jvt.www.api.analytics;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.RETURNS_SELF;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.restassured.response.Response;
import io.restassured.specification.AuthenticationSpecification;
import io.restassured.specification.RequestSpecification;
import java.time.LocalDate;
import me.jvt.www.api.analytics.exception.AnalyticsServiceException;
import me.jvt.www.api.analytics.model.matomo.MatomoPage;
import me.jvt.www.api.analytics.service.AnalyticsService;
import me.jvt.www.api.core.RequestSpecificationFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = Application.class)
@AutoConfigureMockMvc
class ApplicationIntegrationTest {

  @MockBean RequestSpecificationFactory mockRequestSpecificationFactory;

  @Autowired AnalyticsService cachingAnalyticsService;

  @Test
  void contextLoads(ApplicationContext context) {
    assertThat(context).isNotNull();
  }

  @Test
  void cachingAnalyticsServiceCaches() throws AnalyticsServiceException {
    // given
    RequestSpecification mockRequestSpecification = mock(RequestSpecification.class, RETURNS_SELF);
    when(mockRequestSpecificationFactory.newRequestSpecification())
        .thenReturn(mockRequestSpecification);
    Response mockResponse = mock(io.restassured.response.Response.class);

    AuthenticationSpecification mockAuthenticationSpecification =
        mock(AuthenticationSpecification.class);
    when(mockRequestSpecification.auth()).thenReturn(mockAuthenticationSpecification);
    when(mockRequestSpecification.get(anyString())).thenReturn(mockResponse);

    when(mockResponse.as(any(Class.class))).thenReturn(new MatomoPage[] {});
    when(mockResponse.getStatusCode()).thenReturn(200);

    // when
    cachingAnalyticsService.getPages(LocalDate.now());
    cachingAnalyticsService.getPages(LocalDate.now());

    // then
    verify(mockRequestSpecificationFactory, times(1)).newRequestSpecification();
  }
}
