package me.jvt.www.api.analytics.presentation.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import me.jvt.www.api.analytics.exception.AnalyticsServiceException;
import me.jvt.www.api.analytics.model.Page;
import me.jvt.www.api.analytics.service.AnalyticsService;
import me.jvt.www.api.analytics.util.DateUtil;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AnalyticsController.class)
class AnalyticsControllerTest {

  @MockBean AnalyticsService mockService;
  @MockBean DateUtil dateUtil;

  @Autowired MockMvc mockMvc;

  LocalDate localDate;

  @BeforeEach
  void setup() {
    localDate = LocalDate.now();

    when(dateUtil.today()).thenReturn(localDate);
  }

  @Test
  void getEndpointForPagesDelegatesToServiceForToday() throws Exception {
    // given

    // when
    this.mockMvc.perform(get("/analytics/pages"));

    // then
    verify(mockService).getPages(localDate);
  }

  @Test
  void getEndpointForPagesReturns200Ok() throws Exception {
    // given

    // when
    this.mockMvc
        .perform(get("/analytics/pages"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void getEndpointForPagesReturnsResponseFromService() throws Exception {
    // given
    List<Page> pages = new ArrayList<>();
    pages.add(new Page("Example Page 1", 12, 1));
    pages.add(new Page("Page 2", 20, 2));
    when(mockService.getPages(any())).thenReturn(pages);

    // when
    this.mockMvc
        .perform(get("/analytics/pages"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(
            jsonPath("$.pages[*].title")
                .value(Matchers.containsInAnyOrder("Example Page 1", "Page 2")))
        .andExpect(jsonPath("$.pages[*].visits").value(Matchers.containsInAnyOrder(12, 20)))
        .andExpect(jsonPath("$.pages[*].unique_visits").value(Matchers.containsInAnyOrder(1, 2)));
  }

  @Test
  void getEndpointForPagesReturns500WhenExceptionFromAnalyticsService() throws Exception {
    // given
    when(mockService.getPages(any())).thenThrow(new AnalyticsServiceException("Foo bar"));

    // when
    this.mockMvc
        .perform(get("/analytics/pages"))
        // then
        .andExpect(MockMvcResultMatchers.status().isInternalServerError())
        .andExpect(jsonPath("$.error").value("internal_server_error"))
        .andExpect(jsonPath("$.error_description").value("Foo bar"));
  }
}
