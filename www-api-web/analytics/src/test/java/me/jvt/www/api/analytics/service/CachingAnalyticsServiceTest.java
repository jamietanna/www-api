package me.jvt.www.api.analytics.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.List;
import me.jvt.www.api.analytics.exception.AnalyticsServiceException;
import me.jvt.www.api.analytics.model.Page;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CachingAnalyticsServiceTest {
  AnalyticsService delegate;

  CachingAnalyticsService sut;

  @BeforeEach
  void setup() {
    delegate = mock(AnalyticsService.class);

    sut = new CachingAnalyticsService(delegate);
  }

  @Test
  void getPagesDelegates() throws AnalyticsServiceException {
    // given
    LocalDate date = LocalDate.now();

    // when
    sut.getPages(date);

    // then
    verify(delegate).getPages(date);
  }

  @Test
  void getPagesReturnsValueFromDelegate() throws AnalyticsServiceException {
    // given
    LocalDate date = LocalDate.now();
    List<Page> mockList = mock(List.class);
    when(delegate.getPages(any())).thenReturn(mockList);

    // when
    List<Page> actual = sut.getPages(date);

    // then
    assertThat(actual).isSameAs(mockList);
  }
}
