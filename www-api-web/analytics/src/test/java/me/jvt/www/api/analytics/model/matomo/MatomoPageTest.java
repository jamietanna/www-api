package me.jvt.www.api.analytics.model.matomo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;
import me.jvt.www.api.analytics.model.Page;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class MatomoPageTest {

  private MatomoPage sut;

  @BeforeEach
  void setup() {
    sut = new MatomoPage();
  }

  static Stream<Arguments> titles() {
    return Stream.of(
        Arguments.of("This is a thing with no site string", "This is a thing with no site string"),
        Arguments.of("Title Here · Jamie Tanna | Software (Quality) Engineer", "Title Here"),
        Arguments.of(" Jamie Tanna | Software (Quality) Engineer", "Jamie Tanna"));
  }

  @ParameterizedTest
  @ValueSource(strings = {" this is the post", " this is the post      "})
  void setLabelTrimsWhitespace(String label) {
    // given
    sut.setLabel(label);

    // when
    Page actual = sut.asPage();

    // then
    assertThat(actual.getTitle()).isEqualTo("this is the post");
  }

  @ParameterizedTest
  @MethodSource("titles")
  void stripsSiteTitleInPageTitle(String input, String expectedOutput) {
    // given
    sut.setLabel(input);

    // when
    Page actual = sut.asPage();

    // then
    assertThat(actual.getTitle()).isEqualTo(expectedOutput);
  }
}
