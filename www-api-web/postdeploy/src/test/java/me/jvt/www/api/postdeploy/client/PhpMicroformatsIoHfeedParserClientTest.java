package me.jvt.www.api.postdeploy.client;

import static com.github.valfirst.slf4jtest.LoggingEvent.info;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.github.valfirst.slf4jtest.TestLogger;
import com.github.valfirst.slf4jtest.TestLoggerFactory;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.postdeploy.model.Hentry;
import me.jvt.www.api.postdeploy.model.Hfeed;
import me.jvt.www.api.postdeploy.model.MicroformatsParseResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PhpMicroformatsIoHfeedParserClientTest {

  private Hfeed fakeHfeed;
  private MicroformatsParseResult fakeMicroformatsParseResult;
  private MicroformatsParseResult fakeMicroformatsParseResult2;
  private PhpMicroformatsIoHfeedParserClient sut;

  private final TestLogger logger =
      TestLoggerFactory.getTestLogger(PhpMicroformatsIoHfeedParserClient.class);

  @Mock private Hentry hentry0;

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification mockRequestSpecification;

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification mockRequestSpecification2;

  @Mock private RequestSpecificationFactory mockRequestSpecificationFactory;
  @Mock private Response mockResponse;

  @BeforeEach
  void setup() {
    when(mockRequestSpecificationFactory.newRequestSpecification())
        .thenReturn(mockRequestSpecification, mockRequestSpecification2);
    when(mockRequestSpecification.get(anyString())).thenReturn(mockResponse);
    when(mockResponse.as(MicroformatsParseResult.class)).thenReturn(fakeMicroformatsParseResult);

    sut = new PhpMicroformatsIoHfeedParserClient(mockRequestSpecificationFactory);
    fakeHfeed = new Hfeed();
    fakeHfeed.children = Collections.singletonList(hentry0);
    fakeMicroformatsParseResult = new MicroformatsParseResult();
    fakeMicroformatsParseResult.items = new ArrayList<>();
    fakeMicroformatsParseResult2 = new MicroformatsParseResult();

    when(mockRequestSpecification.get(anyString())).thenReturn(mockResponse);
    when(mockResponse.as(MicroformatsParseResult.class)).thenReturn(fakeMicroformatsParseResult);
  }

  @Test
  void itGetsFirstPageOfUrl() throws ExecutionException, InterruptedException {
    // given

    // when
    Future<Hfeed> actualAsync = sut.resolveAllPosts("https://www.jvt.me");
    Hfeed actual = actualAsync.get();

    // then
    assertThat(actual).isNotNull();
    verify(mockRequestSpecification).get("https://php.microformats.io/?url=https://www.jvt.me");

    assertThat(logger.getLoggingEvents())
        .contains(info("Retrieving h-feed from {}", "https://www.jvt.me"));
  }

  @Test
  void itHandlesEmptyFeed() throws ExecutionException, InterruptedException {
    // given
    fakeHfeed.children = new ArrayList<>();

    // when
    Future<Hfeed> actualAsync = sut.resolveAllPosts("https://www.jvt.me");
    Hfeed actual = actualAsync.get();

    // then
    // no exception
    assertThat(actual.children).hasSize(0);
  }

  @Test
  void itGetsNextPageIfRelNextFound() throws ExecutionException, InterruptedException {
    // given
    fakeHfeed.children = new ArrayList<>();
    Hentry hentry1 = mock(Hentry.class);
    fakeHfeed.children.add(hentry0);
    fakeHfeed.children.add(hentry1);

    Hentry hentry2 = mock(Hentry.class);
    Hfeed fakeHfeed2 = new Hfeed();
    fakeHfeed2.children = Collections.singletonList(hentry2);

    fakeMicroformatsParseResult.rels = new MicroformatsParseResult.Rels();
    fakeMicroformatsParseResult.rels.next = Collections.singletonList("https://www.jvt.me/page/2/");
    fakeMicroformatsParseResult.items = new ArrayList<>();
    fakeMicroformatsParseResult.items.add(fakeHfeed);

    fakeMicroformatsParseResult2.rels = new MicroformatsParseResult.Rels();
    fakeMicroformatsParseResult2.items = new ArrayList<>();
    fakeMicroformatsParseResult2.items.add(fakeHfeed2);
    fakeMicroformatsParseResult2.rels.next = Collections.emptyList();

    Response mockResponse2 = mock(Response.class);
    when(mockRequestSpecification2.get(anyString())).thenReturn(mockResponse2);
    when(mockResponse2.as(MicroformatsParseResult.class)).thenReturn(fakeMicroformatsParseResult2);

    // when
    Future<Hfeed> actualAsync = sut.resolveAllPosts("https://www.jvt.me");
    Hfeed actual = actualAsync.get();

    // then
    assertThat(actual).isNotNull();
    assertThat(actual.children).containsExactly(hentry0, hentry1, hentry2);
    verify(mockRequestSpecification).get("https://php.microformats.io/?url=https://www.jvt.me");
    verify(mockRequestSpecification2)
        .get("https://php.microformats.io/?url=https://www.jvt.me/page/2/");

    assertThat(logger.getLoggingEvents())
        .contains(info("Retrieving h-feed from {}", "https://www.jvt.me/page/2/"));
  }

  @Test
  void handlesRelNextAsNull() throws ExecutionException, InterruptedException {
    // given
    fakeHfeed.children = new ArrayList<>();
    Hentry hentry1 = mock(Hentry.class);
    fakeHfeed.children.add(hentry0);
    fakeHfeed.children.add(hentry1);

    Hentry hentry2 = mock(Hentry.class);
    Hfeed fakeHfeed2 = new Hfeed();
    fakeHfeed2.children = Collections.singletonList(hentry2);

    fakeMicroformatsParseResult.rels = new MicroformatsParseResult.Rels();
    fakeMicroformatsParseResult.rels.next = Collections.singletonList("https://www.jvt.me/page/2/");
    fakeMicroformatsParseResult.items = new ArrayList<>();
    fakeMicroformatsParseResult.items.add(fakeHfeed);

    fakeMicroformatsParseResult2.rels = new MicroformatsParseResult.Rels();
    fakeMicroformatsParseResult2.items = new ArrayList<>();
    fakeMicroformatsParseResult2.items.add(fakeHfeed2);
    fakeMicroformatsParseResult2.rels.next = null;

    Response mockResponse2 = mock(Response.class);
    when(mockRequestSpecification2.get(anyString())).thenReturn(mockResponse2);
    when(mockResponse2.as(MicroformatsParseResult.class)).thenReturn(fakeMicroformatsParseResult2);

    // when
    Future<Hfeed> actualAsync = sut.resolveAllPosts("https://www.jvt.me");
    Hfeed actual = actualAsync.get();

    // then
    assertThat(actual).isNotNull();
    assertThat(actual.children).containsExactly(hentry0, hentry1, hentry2);
    verify(mockRequestSpecification).get("https://php.microformats.io/?url=https://www.jvt.me");
    verify(mockRequestSpecification2)
        .get("https://php.microformats.io/?url=https://www.jvt.me/page/2/");
  }
}
