package me.jvt.www.api.postdeploy.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import me.jvt.www.api.postdeploy.client.SearchEngineClient;
import me.jvt.www.api.postdeploy.client.SitemapParserClient;
import me.jvt.www.api.postdeploy.model.GitLabWebHookEvent;
import me.jvt.www.api.postdeploy.model.GitLabWebHookEvent.ObjectAttributes;
import me.jvt.www.api.postdeploy.validators.GitLabWebHookEventValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.sitemaps.schemas.sitemap._0.TUrl;
import org.sitemaps.schemas.sitemap._0.Urlset;
import org.springframework.scheduling.annotation.AsyncResult;

@ExtendWith(MockitoExtension.class)
class PostDeployServiceImplTest {

  private static long THRESHOLD = 300L;
  private static TemporalUnit THRESHOLD_UNIT = ChronoUnit.MINUTES;

  // I.e. 2020-01-17T18:34:00+00:00
  private static final DateTimeFormatter W3C_DATETIME_FORMATTER =
      DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssX").withZone(ZoneId.of("UTC"));
  private static final SimpleDateFormat W3C_DATETIME_FORMAT =
      new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");

  @Mock private GitLabWebHookEventValidator mockValidator;
  @Mock private SearchEngineClient mockSearchEngineClient1;
  @Mock private SearchEngineClient mockSearchEngineClient2;
  @Mock private SearchEngineClient mockSearchEngineClient3;
  @Mock private SitemapParserClient mockSitemapParserClient;
  @Mock private WebmentionOrchestatorService mockWebmentionOrchestratorService;

  private GitLabWebHookEvent gitLabWebHookEvent;
  private PostDeployServiceImpl sut;

  @BeforeEach
  void setup() {
    List<SearchEngineClient> searchEngineClients = new ArrayList<>();
    searchEngineClients.add(mockSearchEngineClient1);
    searchEngineClients.add(mockSearchEngineClient2);
    searchEngineClients.add(mockSearchEngineClient3);

    gitLabWebHookEvent = new GitLabWebHookEvent();
    gitLabWebHookEvent.objectAttributes = new ObjectAttributes();
    gitLabWebHookEvent.objectAttributes.beforeSha = "0000000000000000000000000000000000000000";
    gitLabWebHookEvent.objectAttributes.sha = "975c4c36baecbd31da111cb97372ad526389553d";

    sut =
        new PostDeployServiceImpl(
            mockValidator,
            searchEngineClients,
            mockSitemapParserClient,
            mockWebmentionOrchestratorService);
  }

  @Test
  void validateDelegatesToGitLabWebHookEventValidatorWhenValid() {
    // given
    GitLabWebHookEvent gitLabWebHookEvent = new GitLabWebHookEvent();
    when(mockValidator.validate(any(GitLabWebHookEvent.class))).thenReturn(true);

    // when
    boolean actual = sut.validate(gitLabWebHookEvent);

    // then
    assertThat(actual).isTrue();
    verify(mockValidator).validate(eq(gitLabWebHookEvent));
  }

  @Test
  void validateDelegatesToGitLabWebHookEventValidatorWhenInvalid() {
    // given
    GitLabWebHookEvent gitLabWebHookEvent = new GitLabWebHookEvent();
    when(mockValidator.validate(any(GitLabWebHookEvent.class))).thenReturn(false);

    // when
    boolean actual = sut.validate(gitLabWebHookEvent);

    // then
    assertThat(actual).isFalse();
    verify(mockValidator).validate(eq(gitLabWebHookEvent));
  }

  @Test
  void sendNotificationsHitsSearchEngineClientsWithSitemap() {
    // given

    // when
    sut.sendNotifications(gitLabWebHookEvent);

    // then
    verify(mockSearchEngineClient1).notifySitemap(eq("https://www.jvt.me/sitemap.xml"));
    verify(mockSearchEngineClient2).notifySitemap(eq("https://www.jvt.me/sitemap.xml"));
    verify(mockSearchEngineClient3).notifySitemap(eq("https://www.jvt.me/sitemap.xml"));
  }

  @Test
  void sendWebmentionsResolvesFromSitemapParserClient() {
    // given
    Instant now = Instant.now();
    String nowStr = W3C_DATETIME_FORMATTER.format(now);
    Urlset sitemap = new Urlset();
    List<TUrl> urls = sitemap.getUrl();
    urls.add(turl("https://www.jvt.me/post-1/", nowStr));
    urls.add(turl("https://www.jvt.me/post-2/", nowStr));
    urls.add(turl("https://www.jvt.me/post-3/", nowStr));
    when(mockSitemapParserClient.parseSitemap(anyString())).thenReturn(new AsyncResult<>(sitemap));

    // when
    sut.sendWebmentions(THRESHOLD, THRESHOLD_UNIT);

    // then
    verify(mockSitemapParserClient).parseSitemap("https://www.jvt.me/sitemap.xml");
    verify(mockWebmentionOrchestratorService).sendWebmentionsForUrl("https://www.jvt.me/post-1/");
    verify(mockWebmentionOrchestratorService).sendWebmentionsForUrl("https://www.jvt.me/post-2/");
    verify(mockWebmentionOrchestratorService).sendWebmentionsForUrl("https://www.jvt.me/post-3/");
  }

  @Test
  void sendWebmentionsResolvesFromSitemapParserClientAndIgnoresWhenDatesAreLessThan300MinutesAgo() {
    // given
    Instant now = Instant.now();
    Instant before = now.minus(299L, ChronoUnit.MINUTES);
    String nowStr = W3C_DATETIME_FORMATTER.format(now);
    Urlset sitemap = new Urlset();
    List<TUrl> urls = sitemap.getUrl();
    urls.add(turl("https://www.jvt.me/post-1/", nowStr));
    urls.add(turl("https://www.jvt.me/post-2/", nowStr));
    urls.add(turl("https://www.jvt.me/post-3/", W3C_DATETIME_FORMATTER.format(before)));
    when(mockSitemapParserClient.parseSitemap(anyString())).thenReturn(new AsyncResult<>(sitemap));

    // when
    sut.sendWebmentions(THRESHOLD, THRESHOLD_UNIT);

    // then
    verify(mockSitemapParserClient).parseSitemap("https://www.jvt.me/sitemap.xml");
    verify(mockWebmentionOrchestratorService).sendWebmentionsForUrl("https://www.jvt.me/post-1/");
    verify(mockWebmentionOrchestratorService).sendWebmentionsForUrl("https://www.jvt.me/post-2/");
    verify(mockWebmentionOrchestratorService).sendWebmentionsForUrl("https://www.jvt.me/post-3/");
  }

  @Test
  void sendWebmentionsResolvesFromSitemapParserClientAndIgnoresWhenDatesAreOver300MinutesAgo() {
    // given
    Instant now = Instant.now();
    Instant before = now.minus(301L, ChronoUnit.MINUTES);
    String nowStr = W3C_DATETIME_FORMATTER.format(now);
    Urlset sitemap = new Urlset();
    List<TUrl> urls = sitemap.getUrl();
    urls.add(turl("https://www.jvt.me/post-1/", nowStr));
    urls.add(turl("https://www.jvt.me/post-2/", nowStr));
    urls.add(turl("https://www.jvt.me/post-3/", W3C_DATETIME_FORMATTER.format(before)));
    when(mockSitemapParserClient.parseSitemap(anyString())).thenReturn(new AsyncResult<>(sitemap));

    // when
    sut.sendWebmentions(THRESHOLD, THRESHOLD_UNIT);

    // then
    verify(mockSitemapParserClient).parseSitemap("https://www.jvt.me/sitemap.xml");
    verify(mockWebmentionOrchestratorService).sendWebmentionsForUrl("https://www.jvt.me/post-1/");
    verify(mockWebmentionOrchestratorService).sendWebmentionsForUrl("https://www.jvt.me/post-2/");
    verify(mockWebmentionOrchestratorService, times(0))
        .sendWebmentionsForUrl("https://www.jvt.me/post-3/");
  }

  @Test
  void sendWebmentionsThrowsRuntimeExceptionCausedByInterruptedException()
      throws ExecutionException, InterruptedException {
    // given
    InterruptedException exceptionToThrow = new InterruptedException();
    Future mockFuture = mock(Future.class);
    when(mockSitemapParserClient.parseSitemap(anyString())).thenReturn(mockFuture);

    when(mockFuture.get()).thenThrow(exceptionToThrow);

    // when
    assertThatThrownBy(
            () -> {
              sut.sendWebmentions(THRESHOLD, THRESHOLD_UNIT);
            })
        // then
        .isExactlyInstanceOf(RuntimeException.class)
        .hasMessage("Internal exception was thrown")
        .hasCause(exceptionToThrow);
  }

  @Test
  void sendWebmentionsThrowsRuntimeExceptionCausedByExecutionException()
      throws ExecutionException, InterruptedException {
    // given
    InterruptedException exceptionToThrow = new InterruptedException();
    Future mockFuture = mock(Future.class);
    when(mockSitemapParserClient.parseSitemap(anyString())).thenReturn(mockFuture);
    when(mockFuture.get()).thenThrow(exceptionToThrow);

    // when
    assertThatThrownBy(
            () -> {
              sut.sendWebmentions(THRESHOLD, THRESHOLD_UNIT);
            })
        // then
        .isExactlyInstanceOf(RuntimeException.class)
        .hasMessage("Internal exception was thrown")
        .hasCause(exceptionToThrow);
  }

  private TUrl turl(String loc, String lastmod) {
    TUrl turl = new TUrl();
    turl.setLoc(loc);
    turl.setLastmod(lastmod);
    return turl;
  }
}
