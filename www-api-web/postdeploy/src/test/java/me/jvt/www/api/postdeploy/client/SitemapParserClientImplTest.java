package me.jvt.www.api.postdeploy.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import me.jvt.www.api.core.RequestSpecificationFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.sitemaps.schemas.sitemap._0.Urlset;

@ExtendWith(MockitoExtension.class)
class SitemapParserClientImplTest {

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification mockRequestSpecification;

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification mockRequestSpecification2;

  @Mock private RequestSpecificationFactory mockRequestSpecificationFactory;
  @Mock private Response mockResponse;

  private Urlset fakeSitemap;
  private SitemapParserClientImpl sut;

  @BeforeEach
  void setup() {
    when(mockRequestSpecificationFactory.newRequestSpecification())
        .thenReturn(mockRequestSpecification, mockRequestSpecification2);
    when(mockRequestSpecification.get(anyString())).thenReturn(mockResponse);
    when(mockResponse.as(Urlset.class)).thenReturn(fakeSitemap);

    sut = new SitemapParserClientImpl(mockRequestSpecificationFactory);
  }

  @Test
  void itGetsTheUrlSpecified() {
    // given

    // when
    sut.parseSitemap("https://site.map/xml");

    // then
    verify(mockRequestSpecification).get("https://site.map/xml");
  }

  @Test
  void itConvertsToSitemapObject() {
    // given

    // when
    sut.parseSitemap("https://site.map/xml");

    // then
    verify(mockResponse).as(Urlset.class);
  }

  @Test
  void itReturnsTheSitemapObject() throws ExecutionException, InterruptedException {
    // given

    // when
    Future<Urlset> async = sut.parseSitemap("https://site.map/xml");

    // then
    Urlset actual = async.get();
    assertThat(actual).isSameAs(fakeSitemap);
  }
}
