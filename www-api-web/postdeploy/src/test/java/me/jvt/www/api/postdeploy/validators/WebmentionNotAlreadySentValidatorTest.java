package me.jvt.www.api.postdeploy.validators;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import me.jvt.www.api.postdeploy.model.WebmentionStatus;
import me.jvt.www.api.postdeploy.persistence.WebmentionRepository;
import me.jvt.www.api.postdeploy.persistence.dao.Webmention;
import me.jvt.www.api.postdeploy.persistence.dao.WebmentionIdentity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

class WebmentionNotAlreadySentValidatorTest {

  private static final String SOURCE = "https://url1";
  private static final String TARGET = "http://url.2";

  private WebmentionNotAlreadySentValidator sut;

  private Webmention mockWebmention;
  private WebmentionRepository mockWebmentionRepository;

  @BeforeEach
  void setUp() throws Exception {
    mockWebmentionRepository = mock(WebmentionRepository.class);
    mockWebmention = mock(Webmention.class);

    sut = new WebmentionNotAlreadySentValidator(mockWebmentionRepository);
  }

  @Test
  void itShouldSendIfNotFoundInRepository() {
    // given
    when(mockWebmentionRepository.findById(any())).thenReturn(Optional.empty());

    // when
    boolean actual = sut.shouldSend(SOURCE, TARGET);

    // then
    assertThat(actual).isTrue();
  }

  @Test
  void itShouldSendIfIsFoundInRepositoryAndIsNotSent() {
    // given
    when(mockWebmentionRepository.findById(any())).thenReturn(Optional.of(mockWebmention));
    when(mockWebmention.getLastWebmentionStatus())
        .thenReturn(WebmentionStatus.SERVER_DOES_NOT_SUPPORT);

    // when
    boolean actual = sut.shouldSend(SOURCE, TARGET);

    // then
    assertThat(actual).isTrue();
  }

  @Test
  void itShouldSendIfIsFoundInRepositoryAndIsNotSupported() {
    // given
    when(mockWebmentionRepository.findById(any())).thenReturn(Optional.of(mockWebmention));
    when(mockWebmention.getLastWebmentionStatus())
        .thenReturn(WebmentionStatus.SERVER_DOES_NOT_SUPPORT);

    // when
    boolean actual = sut.shouldSend(SOURCE, TARGET);

    // then
    assertThat(actual).isTrue();
  }

  @Test
  void itShouldNotSendIfIsFoundInRepositoryAndIsSent() {
    // given
    when(mockWebmentionRepository.findById(any())).thenReturn(Optional.of(mockWebmention));
    when(mockWebmention.getLastWebmentionStatus()).thenReturn(WebmentionStatus.SENT);

    // when
    boolean actual = sut.shouldSend(SOURCE, TARGET);

    // then
    assertThat(actual).isFalse();
  }

  @Test
  void itShouldNotSendIfIsFoundInRepositoryAndIsClientError() {
    // given
    when(mockWebmentionRepository.findById(any())).thenReturn(Optional.of(mockWebmention));
    when(mockWebmention.getLastWebmentionStatus()).thenReturn(WebmentionStatus.CLIENT_ERROR);

    // when
    boolean actual = sut.shouldSend(SOURCE, TARGET);

    // then
    assertThat(actual).isFalse();
  }

  @Test
  void itRetrievesWebmentionFromRepositoryBySourceAndTarget() {
    // given

    // when
    sut.shouldSend(SOURCE, TARGET);

    // then
    ArgumentCaptor<WebmentionIdentity> webmentionIdentityArgumentCaptor =
        ArgumentCaptor.forClass(WebmentionIdentity.class);
    verify(mockWebmentionRepository).findById(webmentionIdentityArgumentCaptor.capture());
    WebmentionIdentity repositoryParameters = webmentionIdentityArgumentCaptor.getValue();
    assertThat(repositoryParameters.getSource()).isEqualTo("https://url1");
    assertThat(repositoryParameters.getTarget()).isEqualTo("http://url.2");
  }
}
