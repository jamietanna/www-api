package me.jvt.www.api.postdeploy.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class TelegraphStatusTest {

  @Test
  void itHandlesAccepted() {
    TelegraphStatus actual = TelegraphStatus.fromString("accepted");
    assertThat(actual).isEqualTo(TelegraphStatus.ACCEPTED);
  }

  @Test
  void itHandlesSuccess() {
    TelegraphStatus actual = TelegraphStatus.fromString("success");
    assertThat(actual).isEqualTo(TelegraphStatus.SUCCESS);
  }

  @Test
  void itHandlesNotSupported() {
    TelegraphStatus actual = TelegraphStatus.fromString("not_supported");
    assertThat(actual).isEqualTo(TelegraphStatus.NOT_SUPPORTED);
  }

  @Test
  void itHandlesNoLinkFound() {
    TelegraphStatus actual = TelegraphStatus.fromString("no_link_found");
    assertThat(actual).isEqualTo(TelegraphStatus.NO_LINK_FOUND);
  }

  @Test
  void itHandlesInvalidData() {
    TelegraphStatus actual = TelegraphStatus.fromString("something_else");
    assertThat(actual).isEqualTo(TelegraphStatus.UNKNOWN);
  }

  @Test
  void itHandlesMixedCase() {
    TelegraphStatus actual = TelegraphStatus.fromString("acCEPted");
    assertThat(actual).isEqualTo(TelegraphStatus.ACCEPTED);
  }

  @Test
  void itHandlesUpperCase() {
    TelegraphStatus actual = TelegraphStatus.fromString("ACCEPTED");
    assertThat(actual).isEqualTo(TelegraphStatus.ACCEPTED);
  }
}
