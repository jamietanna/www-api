package me.jvt.www.api.postdeploy.validators;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import me.jvt.www.api.postdeploy.model.WebmentionStatus;
import me.jvt.www.api.postdeploy.persistence.WebmentionRepository;
import me.jvt.www.api.postdeploy.persistence.dao.Webmention;
import me.jvt.www.api.postdeploy.persistence.dao.WebmentionIdentity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class WebmentionNotSupportedRateLimiterTest {

  private static final String SOURCE = "from";
  private static final String TARGET = "to";

  Webmention mockWebmention;
  WebmentionRepository mockWebmentionRepository;

  WebmentionNotSupportedRateLimiter sut;

  @BeforeEach
  void setup() {
    mockWebmention = mock(Webmention.class);
    mockWebmentionRepository = mock(WebmentionRepository.class);
    sut = new WebmentionNotSupportedRateLimiter(mockWebmentionRepository);

    when(mockWebmentionRepository.findById(any())).thenReturn(Optional.of(mockWebmention));
    when(mockWebmention.getLastSentWebmention()).thenReturn(Instant.now());
  }

  @Test
  void itLooksUpSourceAndTargetInRepository() {
    // given
    when(mockWebmentionRepository.findById(any())).thenReturn(Optional.empty());

    // when
    sut.shouldSend(SOURCE, TARGET);

    // then
    verify(mockWebmentionRepository).findById(new WebmentionIdentity(SOURCE, TARGET));
  }

  @Test
  void itShouldSendIfNotFoundInRepository() {
    // given
    when(mockWebmentionRepository.findById(any())).thenReturn(Optional.empty());

    // when
    boolean actual = sut.shouldSend(SOURCE, TARGET);

    // then
    assertThat(actual).isTrue();
  }

  @Test
  void itShouldSendIfNotServerDoesNotSupport() {
    // given
    when(mockWebmention.getLastWebmentionStatus()).thenReturn(WebmentionStatus.CLIENT_ERROR);

    // when
    boolean actual = sut.shouldSend(SOURCE, TARGET);

    // then
    assertThat(actual).isTrue();
  }

  @Test
  void itShouldSendIfServerDoesNotSupportAndLastWebmentionOverTwoWeeksAgo() {
    // given
    when(mockWebmention.getLastWebmentionStatus())
        .thenReturn(WebmentionStatus.SERVER_DOES_NOT_SUPPORT);
    when(mockWebmention.getLastSentWebmention())
        .thenReturn(Instant.now().minus(15, ChronoUnit.DAYS));

    // when
    boolean actual = sut.shouldSend(SOURCE, TARGET);

    // then
    assertThat(actual).isTrue();
  }

  @Test
  void itShouldNotSendIfServerDoesNotSupportAndLastWebmentionUnderTwoWeeksAgo() {
    // given
    when(mockWebmention.getLastWebmentionStatus())
        .thenReturn(WebmentionStatus.SERVER_DOES_NOT_SUPPORT);
    when(mockWebmention.getLastSentWebmention())
        .thenReturn(Instant.now().minus(12, ChronoUnit.DAYS));

    // when
    boolean actual = sut.shouldSend(SOURCE, TARGET);

    // then
    assertThat(actual).isFalse();
  }
}
