package me.jvt.www.api.postdeploy.presentation.rest;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.temporal.ChronoUnit;
import me.jvt.www.api.postdeploy.model.GitLabWebHookEvent;
import me.jvt.www.api.postdeploy.model.GitLabWebHookEvent.ObjectAttributes;
import me.jvt.www.api.postdeploy.service.PostDeployService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@WebMvcTest(PostDeployController.class)
class PostDeployControllerTest {

  private static final String SHARED_SECRET_HEADER = "X-Example-Test-Header";
  private static final String SHARED_SECRET_VALUE = "FooBar";

  static ObjectMapper objectMapper = new ObjectMapper();
  @MockBean private PostDeployService mockPostDeployService;
  @Autowired private MockMvc mockMvc;

  @Test
  void emptyPostReturnsFoundButBadRequest() throws Exception {
    sendRequest(null).andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @Test
  void postReturnsFoundButBadRequestWhenValidationFails() throws Exception {
    // given
    when(mockPostDeployService.validate(any())).thenReturn(false);
    GitLabWebHookEvent gitLabWebHookEvent = new GitLabWebHookEvent();

    // when
    sendRequestExpectingBadRequest(gitLabWebHookEvent, "Must be `main` pipeline");

    // then
    verify(mockPostDeployService).validate(any());
  }

  @Test
  void postReturnsNoContentWhenValidationPasses() throws Exception {
    // given
    when(mockPostDeployService.validate(any())).thenReturn(true);
    GitLabWebHookEvent gitLabWebHookEvent = new GitLabWebHookEvent();

    // when
    sendRequestExpectingSuccess(gitLabWebHookEvent);

    // then
    verify(mockPostDeployService).validate(any());
  }

  @Test
  void postReturnsForbiddenIfValidButNoSharedSecret() throws Exception {
    // given
    when(mockPostDeployService.validate(any())).thenReturn(true);
    GitLabWebHookEvent gitLabWebHookEvent = new GitLabWebHookEvent();

    // when
    this.mockMvc
        .perform(
            (post("/post-deploy")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writer().writeValueAsString(gitLabWebHookEvent))))
        // then
        .andExpect(MockMvcResultMatchers.status().isForbidden());
  }

  @Test
  void fullPostBodyGetsMappedCorrectly() throws Exception {
    // given
    when(mockPostDeployService.validate(any())).thenReturn(true);
    GitLabWebHookEvent gitLabWebHookEvent = new GitLabWebHookEvent();
    gitLabWebHookEvent.objectAttributes = new ObjectAttributes();
    gitLabWebHookEvent.objectAttributes.ref = "foo";
    gitLabWebHookEvent.objectAttributes.status = "bar";
    gitLabWebHookEvent.objectKind = "blah";

    // when
    sendRequestExpectingSuccess(gitLabWebHookEvent);

    // then
    ArgumentCaptor<GitLabWebHookEvent> gitLabWebHookEventArgumentCaptor =
        ArgumentCaptor.forClass(GitLabWebHookEvent.class);
    verify(mockPostDeployService).validate(gitLabWebHookEventArgumentCaptor.capture());
    GitLabWebHookEvent actual = gitLabWebHookEventArgumentCaptor.getValue();
    assertThat(actual.objectKind).isEqualTo("blah");
    assertThat(actual.objectAttributes).isNotNull();
    assertThat(actual.objectAttributes.ref).isEqualTo("foo");
    assertThat(actual.objectAttributes.status).isEqualTo("bar");
  }

  @Test
  void successfulRequestSendsNotifications() throws Exception {
    // given
    when(mockPostDeployService.validate(any())).thenReturn(true);
    GitLabWebHookEvent gitLabWebHookEvent = new GitLabWebHookEvent();
    gitLabWebHookEvent.objectKind = "foo";

    // when
    sendRequestExpectingSuccess(gitLabWebHookEvent);

    // then
    ArgumentCaptor<GitLabWebHookEvent> gitLabWebHookEventArgumentCaptor =
        ArgumentCaptor.forClass(GitLabWebHookEvent.class);
    verify(mockPostDeployService).sendNotifications(gitLabWebHookEventArgumentCaptor.capture());
    GitLabWebHookEvent serviceWebHookEvent = gitLabWebHookEventArgumentCaptor.getValue();
    assertThat(serviceWebHookEvent).isNotNull();
    assertThat(objectMapper.writer().writeValueAsString(serviceWebHookEvent))
        .isEqualTo(objectMapper.writer().writeValueAsString(gitLabWebHookEvent));
  }

  @Test
  void successfulRequestTriggersWebmentionsForLastHour() throws Exception {
    // given
    when(mockPostDeployService.validate(any())).thenReturn(true);
    GitLabWebHookEvent gitLabWebHookEvent = new GitLabWebHookEvent();

    // when
    sendRequestExpectingSuccess(gitLabWebHookEvent);

    // then
    verify(mockPostDeployService).sendWebmentions(60, ChronoUnit.MINUTES);
  }

  private ResultActions sendRequest(GitLabWebHookEvent gitLabWebHookEvent) throws Exception {
    return this.mockMvc.perform(
        (post("/post-deploy")
            .contentType(MediaType.APPLICATION_JSON)
            .header(SHARED_SECRET_HEADER, SHARED_SECRET_VALUE)
            .content(objectMapper.writer().writeValueAsString(gitLabWebHookEvent))));
  }

  private void sendRequestExpectingSuccess(GitLabWebHookEvent gitLabWebHookEvent) throws Exception {
    sendRequest(gitLabWebHookEvent).andExpect(MockMvcResultMatchers.status().isNoContent());
  }

  private void sendRequestExpectingBadRequest(
      GitLabWebHookEvent gitLabWebHookEvent, String expectedContent) throws Exception {
    sendRequest(gitLabWebHookEvent)
        .andExpect(MockMvcResultMatchers.status().isBadRequest())
        .andExpect(MockMvcResultMatchers.content().string(expectedContent));
  }
}
