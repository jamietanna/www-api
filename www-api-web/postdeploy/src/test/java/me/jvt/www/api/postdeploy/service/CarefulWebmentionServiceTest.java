package me.jvt.www.api.postdeploy.service;

import static com.github.valfirst.slf4jtest.LoggingEvent.info;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.github.valfirst.slf4jtest.TestLogger;
import com.github.valfirst.slf4jtest.TestLoggerFactory;
import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import me.jvt.www.api.postdeploy.client.WebmentionClient;
import me.jvt.www.api.postdeploy.model.Hentry;
import me.jvt.www.api.postdeploy.model.WebmentionResponse;
import me.jvt.www.api.postdeploy.model.WebmentionStatus;
import me.jvt.www.api.postdeploy.persistence.WebmentionRepository;
import me.jvt.www.api.postdeploy.persistence.dao.Webmention;
import me.jvt.www.api.postdeploy.service.CarefulWebmentionService.CarefulWebmentionServiceBuilder;
import me.jvt.www.api.postdeploy.validators.WebmentionNotAlreadySentValidator;
import me.jvt.www.api.postdeploy.validators.WebmentionSourceProfileValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.scheduling.annotation.AsyncResult;

@ExtendWith(MockitoExtension.class)
class CarefulWebmentionServiceTest {

  private static final String SOURCE = "https://source.com";
  private static final String TARGET = "https://outgoing.foo";

  private final TestLogger logger = TestLoggerFactory.getTestLogger(CarefulWebmentionService.class);

  private CarefulWebmentionService sut;

  @Mock private WebmentionNotAlreadySentValidator mockWebmentionNotAlreadySentValidator;
  @Mock private WebmentionSourceProfileValidator mockWebmentionSourceProfileValidator;

  @Mock private Hentry mockHentry;
  @Mock private WebmentionClient mockWebmentionClient;
  @Mock private WebmentionRepository mockWebmentionRepository;
  @Mock private WebmentionResponse mockWebmentionResponse;

  @BeforeEach
  void setup() {
    when(mockWebmentionNotAlreadySentValidator.shouldSend(anyString(), anyString()))
        .thenReturn(true);
    Mockito.lenient()
        .when(mockWebmentionSourceProfileValidator.shouldSend(anyString(), anyString()))
        .thenReturn(true);
    Mockito.lenient()
        .when(mockWebmentionClient.sendWebmention(anyString(), anyString()))
        .thenReturn(new AsyncResult<>(mockWebmentionResponse));
    Mockito.lenient()
        .when(mockWebmentionResponse.getWebmentionStatus())
        .thenReturn(WebmentionStatus.SENT);

    sut =
        CarefulWebmentionServiceBuilder.aCarefulWebmentionService()
            .withWebmentionValidator(mockWebmentionNotAlreadySentValidator)
            .withWebmentionValidator(mockWebmentionSourceProfileValidator)
            .withWebmentionClient(mockWebmentionClient)
            .withWebmentionRepository(mockWebmentionRepository)
            .build();
  }

  @Test
  void itValidatesInOrderWhetherItShouldSendWebmentionBeforeSending()
      throws ExecutionException, InterruptedException {
    // given

    // when
    sendWebmention(SOURCE, TARGET);

    // then
    InOrder inOrder =
        Mockito.inOrder(
            mockWebmentionNotAlreadySentValidator, mockWebmentionSourceProfileValidator);
    inOrder.verify(mockWebmentionNotAlreadySentValidator).shouldSend(SOURCE, TARGET);
    inOrder.verify(mockWebmentionSourceProfileValidator).shouldSend(SOURCE, TARGET);
  }

  @Test
  void itDoesNotSendIfAlreadySent() throws ExecutionException, InterruptedException {
    // given
    when(mockWebmentionNotAlreadySentValidator.shouldSend(anyString(), anyString()))
        .thenReturn(false);

    // when
    sendWebmention(SOURCE, TARGET);

    // then
    verify(mockWebmentionClient, times(0)).sendWebmention(anyString(), anyString());
  }

  @Test
  void itDoesNotSendIfIsSourceUrl() throws ExecutionException, InterruptedException {
    // given
    when(mockWebmentionSourceProfileValidator.shouldSend(anyString(), anyString()))
        .thenReturn(false);

    // when
    Optional<String> actual = sendWebmention(SOURCE, TARGET);

    // then
    assertThat(actual).isNotPresent();
    verify(mockWebmentionClient, times(0)).sendWebmention(anyString(), anyString());
  }

  @Test
  void itSendsIfAllPass() throws ExecutionException, InterruptedException {
    // given

    // when
    sendWebmention(SOURCE, TARGET);

    // then
    verify(mockWebmentionClient).sendWebmention(SOURCE, TARGET);
  }

  @Test
  void itReturnsUrlOfSyndicatedPostIfPresent() throws ExecutionException, InterruptedException {
    // given
    when(mockWebmentionResponse.getSyndicatedLink()).thenReturn(Optional.of("https://foo.bar"));

    // when
    Optional<String> optional = sendWebmention(SOURCE, TARGET);

    // then
    assertThat(optional).isPresent();
    assertThat(optional.get()).isEqualTo("https://foo.bar");
  }

  @Test
  void itSavesWebmentionDetailsWhenSent() throws ExecutionException, InterruptedException {
    // given

    // when
    sendWebmention(SOURCE, TARGET);

    // then
    ArgumentCaptor<Webmention> webmentionArgumentCaptor = ArgumentCaptor.forClass(Webmention.class);
    verify(mockWebmentionRepository).save(webmentionArgumentCaptor.capture());
    Webmention actual = webmentionArgumentCaptor.getValue();
    assertThat(actual).isNotNull();

    assertThat(actual.getWebmentionIdentity().getSource()).isEqualTo("https://source.com");
    assertThat(actual.getWebmentionIdentity().getTarget()).isEqualTo("https://outgoing.foo");

    assertThat(actual.getLastWebmentionStatus()).isEqualTo(WebmentionStatus.SENT);

    assertThat(actual.getLastSentWebmention())
        .isBeforeOrEqualTo(Instant.now()); // TODO: do this better
  }

  @Test
  void itLogsWhenSendingTheWebmention() throws ExecutionException, InterruptedException {
    // given

    // when
    sendWebmention(SOURCE, TARGET);

    // then
    assertThat(logger.getLoggingEvents())
        .contains(
            info("Sending webmention from {} to {}", "https://source.com", "https://outgoing.foo"));
  }

  private Optional<String> sendWebmention(String source, String target)
      throws ExecutionException, InterruptedException {
    return sut.sendWebmention(source, target).get();
  }
}
