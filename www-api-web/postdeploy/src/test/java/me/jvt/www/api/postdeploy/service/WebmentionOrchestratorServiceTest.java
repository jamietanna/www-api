package me.jvt.www.api.postdeploy.service;

import static com.github.valfirst.slf4jtest.LoggingEvent.error;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.github.valfirst.slf4jtest.TestLogger;
import com.github.valfirst.slf4jtest.TestLoggerFactory;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import me.jvt.www.api.postdeploy.client.HentryWebmentionUrlResolver;
import me.jvt.www.api.postdeploy.client.SyndicationRetrievalException;
import me.jvt.www.api.postdeploy.client.SyndicationUpdateClient;
import me.jvt.www.api.postdeploy.model.micropub.SyndicateTo;
import me.jvt.www.api.postdeploy.util.WebmentionPair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.scheduling.annotation.AsyncResult;

@ExtendWith(MockitoExtension.class)
class WebmentionOrchestratorServiceTest {

  private static final String URL = "https://url.foo";

  private final TestLogger logger =
      TestLoggerFactory.getTestLogger(WebmentionOrchestatorService.class);

  private WebmentionPair pair0;
  private WebmentionPair pair1;
  private WebmentionPair pair2;

  @Mock private HentryWebmentionUrlResolver hentryWebmentionUrlResolver;
  @Mock private SyndicationUpdateClient syndicationUpdateClient;
  @Mock private WebmentionService webmentionService;

  private WebmentionOrchestatorService sut;

  @BeforeEach
  void setup() {
    sut =
        new WebmentionOrchestatorService(
            hentryWebmentionUrlResolver, webmentionService, syndicationUpdateClient);

    pair0 = new WebmentionPair("a", "1");
    pair1 = new WebmentionPair("b", "1");
    pair2 = new WebmentionPair("c", "1");

    Set<WebmentionPair> webmentionPairs = new HashSet<>();
    webmentionPairs.add(pair0);
    webmentionPairs.add(pair1);
    webmentionPairs.add(pair2);

    when(hentryWebmentionUrlResolver.resolveAllUrls(anyString())).thenReturn(webmentionPairs);
  }

  @Test
  void sendRetrievesSyndicateToLinks() throws SyndicationRetrievalException {
    // given

    // when
    sut.sendWebmentionsForUrl(URL);

    // then
    verify(syndicationUpdateClient).retrieveSyndication();
  }

  @Test
  void sendProceedsIfSyndicationRetrievalException() throws SyndicationRetrievalException {
    SyndicationRetrievalException exceptionToThrow =
        new SyndicationRetrievalException("Something went wrong");
    when(syndicationUpdateClient.retrieveSyndication()).thenThrow(exceptionToThrow);

    // when
    sut.sendWebmentionsForUrl(URL);

    // then
    verifyNoMoreInteractions(syndicationUpdateClient);
  }

  @Test
  void itResolvesUrlsFromTheResolver() {
    // given

    // when
    sut.sendWebmentionsForUrl(URL);

    // then
    verify(hentryWebmentionUrlResolver).resolveAllUrls(URL);
  }

  @Test
  void itCallsWebmentionServiceForEachWebmentionPair() {
    // given

    // when
    sut.sendWebmentionsForUrl(URL);

    // then
    verify(webmentionService).sendWebmention("a", "1");
    verify(webmentionService).sendWebmention("b", "1");
    verify(webmentionService).sendWebmention("c", "1");
  }

  @Test
  void errorsAreLoggedButNotRethrown() {
    // given
    Exception exception = new RuntimeException("Nope");
    when(hentryWebmentionUrlResolver.resolveAllUrls(anyString()))
        .thenReturn(Collections.singleton(pair0));
    doThrow(exception).when(webmentionService).sendWebmention(anyString(), anyString());

    // when
    sut.sendWebmentionsForUrl(URL);

    // then
    assertThat(logger.getLoggingEvents()).contains(error(exception, "Sending webmention failed"));
  }

  @Test
  void sendUpdatesSyndicationIfTargetUrlIsInRetrievedSyndicationLinks()
      throws SyndicationRetrievalException {
    // given
    Set<WebmentionPair> urls = new HashSet<>();
    urls.add(new WebmentionPair("source", "target"));
    urls.add(new WebmentionPair("source2", "https://bridgy"));
    urls.add(new WebmentionPair("source3", "https://news"));
    when(hentryWebmentionUrlResolver.resolveAllUrls(anyString())).thenReturn(urls);
    when(webmentionService.sendWebmention(anyString(), anyString()))
        .thenReturn(new AsyncResult<>(Optional.empty()));
    when(webmentionService.sendWebmention("source2", "https://bridgy"))
        .thenReturn(new AsyncResult<>(Optional.of("https://silo/post/1")));
    when(webmentionService.sendWebmention("source3", "https://news"))
        .thenReturn(new AsyncResult<>(Optional.of("https://status/1234")));
    when(syndicationUpdateClient.retrieveSyndication())
        .thenReturn(Collections.singleton(new SyndicateTo("https://bridgy", "Bridgy")));

    // when
    sut.sendWebmentionsForUrl(URL);

    // then
    verify(syndicationUpdateClient).updateSyndication("source2", "https://silo/post/1");
    verify(syndicationUpdateClient, times(0)).updateSyndication(eq("source3"), anyString());
  }
}
