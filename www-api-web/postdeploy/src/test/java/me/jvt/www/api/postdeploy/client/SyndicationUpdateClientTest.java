package me.jvt.www.api.postdeploy.client;

import static com.github.valfirst.slf4jtest.LoggingEvent.error;
import static com.github.valfirst.slf4jtest.LoggingEvent.info;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.github.valfirst.slf4jtest.TestLogger;
import com.github.valfirst.slf4jtest.TestLoggerFactory;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.AuthenticationSpecification;
import io.restassured.specification.PreemptiveAuthSpec;
import io.restassured.specification.RequestSpecification;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.postdeploy.model.micropub.AddSyndicationRequest;
import me.jvt.www.api.postdeploy.model.micropub.SyndicateTo;
import me.jvt.www.api.postdeploy.model.micropub.SyndicateToResponse;
import me.jvt.www.indieauthcontroller.store.TokenStore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SyndicationUpdateClientTest {

  @Mock private AuthenticationSpecification mockAuthenticationSpecification;

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification mockRequestSpecification;

  @Mock private RequestSpecificationFactory mockRequestSpecificationFactory;
  @Mock private Response mockResponse;
  @Mock private TokenStore tokenStore;

  private final TestLogger logger = TestLoggerFactory.getTestLogger(SyndicationUpdateClient.class);
  SyndicationUpdateClient sut;

  @BeforeEach
  void setup() {
    when(mockRequestSpecificationFactory.newRequestSpecification())
        .thenReturn(mockRequestSpecification);
    Mockito.lenient().when(mockRequestSpecification.get(anyString())).thenReturn(mockResponse);
    Mockito.lenient().when(mockRequestSpecification.post(anyString())).thenReturn(mockResponse);

    sut = new SyndicationUpdateClient(tokenStore, mockRequestSpecificationFactory);
  }

  @Nested
  class RetrieveSyndication {
    @Test
    void itCallsMicropubSyndicateToEndpoint() throws SyndicationRetrievalException {
      // given
      setupSuccess();

      // when
      sut.retrieveSyndication();

      // then
      verify(mockRequestSpecification).get("https://www-api.jvt.me/micropub");
      verify(mockRequestSpecification).param("q", "syndicate-to");
    }

    @Test
    void itReturnsPojoForResponse() throws SyndicationRetrievalException {
      // given
      setupSuccess();

      // when
      Set<SyndicateTo> actual = sut.retrieveSyndication();

      // then
      SyndicateTo expected0 = new SyndicateTo("https://bridgy", "Bridgy");
      SyndicateTo expected1 = new SyndicateTo("https://news", "News site");
      assertThat(actual).containsExactlyInAnyOrder(expected0, expected1);
    }

    @Test
    void itThrowsSyndicationRetrievalExceptionIfNot200Ok() {
      // given
      when(mockResponse.getStatusCode()).thenReturn(500);

      // when
      assertThatThrownBy(() -> sut.retrieveSyndication())
          // then
          .isInstanceOf(SyndicationRetrievalException.class)
          .hasMessage("Syndication query endpoint returned 500");
    }

    private void setupSuccess() {
      SyndicateToResponse response = new SyndicateToResponse();
      Map<String, String> syndication0 = new HashMap<>();
      syndication0.put("uid", "https://bridgy");
      syndication0.put("name", "Bridgy");
      Map<String, String> syndication1 = new HashMap<>();
      syndication1.put("uid", "https://news");
      syndication1.put("name", "News site");
      Set<Map<String, String>> syndicate = new HashSet<>();
      syndicate.add(syndication0);
      syndicate.add(syndication1);
      response.setSyndicateTo(syndicate);

      when(mockResponse.as(any(Class.class))).thenReturn(response);
      when(mockResponse.getStatusCode()).thenReturn(200);
    }
  }

  @Nested
  class UpdateSyndication {

    private static final String URL = "postUrl";
    private static final String SYNDICATED = "syndicated";
    private PreemptiveAuthSpec mockPreemptiveAuthSpec;

    @BeforeEach
    void setup() {
      when(mockResponse.getStatusCode()).thenReturn(200);
      AuthenticationSpecification mockAuthenticationSpecification =
          mock(AuthenticationSpecification.class);
      when(mockRequestSpecification.auth()).thenReturn(mockAuthenticationSpecification);
      mockPreemptiveAuthSpec = mock(PreemptiveAuthSpec.class);
      when(mockAuthenticationSpecification.preemptive()).thenReturn(mockPreemptiveAuthSpec);
      when(mockPreemptiveAuthSpec.oauth2(anyString())).thenReturn(mockRequestSpecification);
      when(tokenStore.get()).thenReturn("eyJ...");
    }

    @Test
    void itCallsMicropubSyndicateToEndpoint() {
      // given

      // when
      sut.updateSyndication(URL, SYNDICATED);

      // then
      verify(mockRequestSpecification).post("https://www-api.jvt.me/micropub");
    }

    @Test
    void itSendsJson() {
      // given

      // when
      sut.updateSyndication(URL, SYNDICATED);

      // then
      verify(mockRequestSpecification).contentType(ContentType.JSON);
    }

    @Test
    void itProvidesBearerToken() {
      // given

      // when
      sut.updateSyndication(URL, SYNDICATED);

      // then
      verify(mockPreemptiveAuthSpec).oauth2("eyJ...");
    }

    @Test
    void itPostsUpdateToUrl() {
      // given

      // when
      sut.updateSyndication(URL, SYNDICATED);

      // then
      ArgumentCaptor<AddSyndicationRequest> requestBodyArgumentCaptor =
          ArgumentCaptor.forClass(AddSyndicationRequest.class);
      verify(mockRequestSpecification).body(requestBodyArgumentCaptor.capture());

      AddSyndicationRequest body = requestBodyArgumentCaptor.getValue();
      assertThat(body.getAction()).isEqualTo("update");
      assertThat(body.getAdditions()).containsKey("syndication");
      assertThat(body.getAdditions().get("syndication")).containsExactly(SYNDICATED);
      assertThat(body.getUrl()).isEqualTo(URL);
    }

    @ParameterizedTest
    @ValueSource(ints = {200, 201, 204})
    void itLogsTheResponseAsInfoIf2xx(int sc) {
      // given
      when(mockResponse.getStatusCode()).thenReturn(sc);

      // when
      sut.updateSyndication(URL, SYNDICATED);

      // then
      assertThat(logger.getLoggingEvents())
          .contains(info("Post {} updated with syndication link {}", "postUrl", "syndicated"));
    }

    @Test
    void itLogsTheResponseAsErrorIfFailure() {
      // given
      when(mockResponse.getStatusCode()).thenReturn(401);

      // when
      sut.updateSyndication(URL, SYNDICATED);

      // then
      assertThat(logger.getLoggingEvents())
          .contains(
              error(
                  "Post {} failed to update with link {}: HTTP {}", "postUrl", "syndicated", 401));
    }
  }
}
