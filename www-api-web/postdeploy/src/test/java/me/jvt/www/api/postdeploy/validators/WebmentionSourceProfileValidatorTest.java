package me.jvt.www.api.postdeploy.validators;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class WebmentionSourceProfileValidatorTest {

  private WebmentionSourceProfileValidator sut;

  @BeforeEach
  void setup() {
    sut = new WebmentionSourceProfileValidator();
  }

  @Test
  void shouldNotSendIfSourceIsMyProfileUrlWithNoTrailingSlash() {
    // given

    // when
    boolean actual = sut.shouldSend("https://www.jvt.me", "wibble");

    // then
    assertThat(actual).isFalse();
  }

  @Test
  void shouldNotSendIfSourceIsMyProfileUrlWithTrailingSlash() {
    // given

    // when
    boolean actual = sut.shouldSend("https://www.jvt.me/", "wibble");

    // then
    assertThat(actual).isFalse();
  }

  @Test
  void shouldNotSendIfSourceIsNotMyProfileUrl() {
    // given

    // when
    boolean actual = sut.shouldSend("https://www.jvt.me/posts/something/", "wibble");

    // then
    assertThat(actual).isTrue();
  }
}
