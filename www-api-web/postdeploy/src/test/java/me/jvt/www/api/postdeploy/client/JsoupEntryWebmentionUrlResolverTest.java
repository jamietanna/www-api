package me.jvt.www.api.postdeploy.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Set;
import me.jvt.www.api.postdeploy.jsoup.JsoupConnector;
import me.jvt.www.api.postdeploy.util.WebmentionPair;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class JsoupEntryWebmentionUrlResolverTest {

  @Mock private Connection mockConnection;
  @Mock private Document mockDocument;
  @Mock private JsoupConnector mockJsoupConnector;

  private Elements fakeElements;
  private JsoupEntryWebmentionUrlResolver sut;
  private String url = "https://localhost:1234/url";

  @BeforeEach
  void setup() throws IOException {
    fakeElements = new Elements();
    when(mockJsoupConnector.connect(anyString())).thenReturn(mockConnection);
    when(mockConnection.get()).thenReturn(mockDocument);
    Mockito.lenient().when(mockDocument.select(".h-entry")).thenReturn(fakeElements);
    Mockito.lenient().when(mockDocument.select(".h-feed")).thenReturn(new Elements());

    sut = new JsoupEntryWebmentionUrlResolver(mockJsoupConnector);
  }

  @Test
  void retrievesAllLinksForHentry() {
    // given
    fakeElements.add(link("https://foo.local"));
    fakeElements.add(link("https://bar.com"));

    // when
    Set<WebmentionPair> actual = sut.resolveAllUrls(url);

    // then
    verify(mockJsoupConnector).connect(eq("https://localhost:1234/url"));
    assertThat(actual).hasSize(2);
    assertThat(actual)
        .contains(new WebmentionPair("https://localhost:1234/url", "https://foo.local"));
    assertThat(actual)
        .contains(new WebmentionPair("https://localhost:1234/url", "https://bar.com"));

    verify(mockDocument).select(".h-entry");
  }

  @Test
  void doesNotReturnUrlsIfNoLinks() {
    // given
    fakeElements.clear();

    // when
    Set<WebmentionPair> actual = sut.resolveAllUrls(url);

    // then
    verify(mockJsoupConnector).connect(eq("https://localhost:1234/url"));
    assertThat(actual).isEmpty();
  }

  @Test
  void retrievesAllLinksForPageAndReturnsUrlsOnlyForAbsoluteUrls() {
    // given
    fakeElements.add(link("https://foo.local"));
    fakeElements.add(link("/post/something"));

    // when
    Set<WebmentionPair> actual = sut.resolveAllUrls(url);

    // then
    verify(mockJsoupConnector).connect(eq("https://localhost:1234/url"));
    assertThat(actual).hasSize(1);
    assertThat(actual)
        .contains(new WebmentionPair("https://localhost:1234/url", "https://foo.local"));
  }

  @Test
  void itIgnoresMentionsOfMyProfileUrl() {
    // given
    fakeElements.add(link("https://foo.local"));
    fakeElements.add(link("https://www.jvt.me"));
    fakeElements.add(link("http://www.jvt.me"));
    fakeElements.add(link("http://www.jvt.me"));
    fakeElements.add(link("https://www.jvt.me/"));

    // when
    Set<WebmentionPair> actual = sut.resolveAllUrls(url);

    // then
    assertThat(actual)
        .containsExactly(new WebmentionPair("https://localhost:1234/url", "https://foo.local"));
  }

  @Test
  void itThrowsRuntimeExceptionWhenIoExceptionsThrown() throws IOException {
    // given
    IOException exceptionToThrow = new IOException("Something failed");
    when(mockConnection.get()).thenThrow(exceptionToThrow);

    // when
    assertThatThrownBy(
            () -> {
              sut.resolveAllUrls(url);
            })
        // then
        .isInstanceOf(RuntimeException.class)
        .hasMessage("An error occurred when reaching out to https://localhost:1234/url")
        .hasCause(exceptionToThrow);
  }

  @Test
  void itThrowsRuntimeExceptionWhenInvalidUriLinkFound() throws IOException {
    // given
    fakeElements.add(link("http://finance.yahoo.com/q/h?s=^IXIC"));

    // when
    assertThatThrownBy(
            () -> {
              sut.resolveAllUrls(url);
            })
        // then
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid link http://finance.yahoo.com/q/h?s=^IXIC found")
        .hasCauseInstanceOf(URISyntaxException.class);
  }

  @Test
  void itDoesNotResolveLinksFromFeedPage() {
    // given
    Elements hfeeds = mock(Elements.class);
    when(mockDocument.select(".h-feed")).thenReturn(hfeeds);
    when(hfeeds.size()).thenReturn(1);

    // when
    Set<WebmentionPair> actual = sut.resolveAllUrls(url);

    // then
    assertThat(actual).isEmpty();
  }

  private Element link(String url) {
    return new Element(Tag.valueOf("a"), "").attr("href", url);
  }
}
