package me.jvt.www.api.postdeploy.validators;

import static org.assertj.core.api.Assertions.assertThat;

import me.jvt.www.api.postdeploy.model.GitLabWebHookEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GitLabWebHookEventValidatorTest {

  private GitLabWebHookEvent event;
  private GitLabWebHookEventValidator sut;

  @BeforeEach
  void setup() {
    event = new GitLabWebHookEvent();
    sut = new GitLabWebHookEventValidator();
  }

  @Test
  void validateObjectKindWhenPipeline() {
    // given
    event.objectKind = "pipeline";

    // when
    boolean isValid = sut.validateObjectKind(event);

    // then
    assertThat(isValid).isTrue();
  }

  @Test
  void validateObjectKindWhenNotPipeline() {
    // given
    event.objectKind = "issue";

    // when
    boolean isValid = sut.validateObjectKind(event);

    // then
    assertThat(isValid).isFalse();
  }

  @Test
  void validateBranchWhenMain() {
    // given
    event.objectAttributes = new GitLabWebHookEvent.ObjectAttributes();
    event.objectAttributes.ref = "main";

    // when
    boolean isValid = sut.validateBranch(event);

    // then
    assertThat(isValid).isTrue();
  }

  @Test
  void validateBranchWhenNotMain() {
    // given
    event.objectAttributes = new GitLabWebHookEvent.ObjectAttributes();
    event.objectAttributes.ref = "feature/foo";

    // when
    boolean isValid = sut.validateBranch(event);

    // then
    assertThat(isValid).isFalse();
  }

  @Test
  void validateStatusWhenSuccess() {
    // given
    event.objectAttributes = new GitLabWebHookEvent.ObjectAttributes();
    event.objectAttributes.status = "success";

    // when
    boolean isValid = sut.validateStatus(event);

    // then
    assertThat(isValid).isTrue();
  }

  @Test
  void validateStatusWhenNotSuccess() {
    // given
    event.objectAttributes = new GitLabWebHookEvent.ObjectAttributes();
    event.objectAttributes.status = "failure";

    // when
    boolean isValid = sut.validateStatus(event);

    // then
    assertThat(isValid).isFalse();
  }

  @Test
  void validateRequiresAllPassing() {
    // given
    event.objectKind = "pipeline";
    event.objectAttributes = new GitLabWebHookEvent.ObjectAttributes();
    event.objectAttributes.ref = "main";
    event.objectAttributes.status = "success";

    // when
    boolean isValid = sut.validate(event);

    // then
    assertThat(isValid).isTrue();
  }
}
