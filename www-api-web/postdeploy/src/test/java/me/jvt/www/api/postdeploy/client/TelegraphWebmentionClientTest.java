package me.jvt.www.api.postdeploy.client;

import static com.github.valfirst.slf4jtest.LoggingEvent.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.valfirst.slf4jtest.TestLogger;
import com.github.valfirst.slf4jtest.TestLoggerFactory;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.postdeploy.model.WebmentionResponse;
import me.jvt.www.api.postdeploy.model.WebmentionStatus;
import me.jvt.www.api.postdeploy.util.SleepUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TelegraphWebmentionClientTest {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private final TestLogger logger =
      TestLoggerFactory.getTestLogger(TelegraphWebmentionClient.class);

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification mockRequestSpecification;

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification mockStatusRequestSpecification;

  @Mock private RequestSpecificationFactory mockRequestSpecificationFactory;
  @Mock private Response mockResponse;
  @Mock private Response mockStatusResponse;

  @Mock private SleepUtil mockSleepUtil;

  private TelegraphWebmentionClient sut;

  @BeforeEach
  void setup() {
    sut =
        new TelegraphWebmentionClient(
            mockRequestSpecificationFactory, "j.w.t", mockSleepUtil, OBJECT_MAPPER);

    when(mockRequestSpecificationFactory.newRequestSpecification())
        .thenReturn(mockRequestSpecification, mockStatusRequestSpecification);
    Mockito.lenient().when(mockRequestSpecification.post(anyString())).thenReturn(mockResponse);
    Mockito.lenient()
        .when(mockStatusRequestSpecification.get(anyString()))
        .thenReturn(mockStatusResponse);
    Mockito.lenient().when(mockResponse.getStatusCode()).thenReturn(200);
    Mockito.lenient().when(mockStatusResponse.getStatusCode()).thenReturn(200);
    Mockito.lenient().when(mockStatusResponse.path("status")).thenReturn("accepted");
  }

  @Test
  void postsToTelegraph() {
    // given

    // when
    sut.sendWebmention("https://source.foo", "https://bar.bar");

    // then
    verify(mockRequestSpecification).baseUri("https://telegraph.p3k.io");
    verify(mockRequestSpecification).post("/webmention");
  }

  @Test
  void sendsSourceParameter() {
    // given

    // when
    sut.sendWebmention("https://source.foo", "https://bar.bar");

    // then
    verify(mockRequestSpecification).param("source", "https://source.foo");
  }

  @Test
  void sendsTargetParameter() {
    // given

    // when
    sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    verify(mockRequestSpecification).param("target", "https://bar.bar/foo.html");
  }

  @Test
  void sendsApiToken() {
    // given

    // when
    sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    verify(mockRequestSpecification).param("token", "j.w.t");
  }

  @Test
  void itLogsTheResponseCodeAsErrorIfNot201() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(202);

    // when
    sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    assertThat(logger.getLoggingEvents())
        .contains(
            error(
                "Webmention returned invalid unsuccessful code ({}) for {} to {}",
                202,
                "https://source.foo",
                "https://bar.bar/foo.html"));
  }

  @Test
  void itReturnsUnknownErrorIfNot201() throws Exception {
    // given
    when(mockResponse.getStatusCode()).thenReturn(202);

    // when
    Future<WebmentionResponse> actual =
        sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    assertThat(actual.get().getWebmentionStatus()).isEqualTo(WebmentionStatus.UNKNOWN_ERROR);
    assertThat(actual.get().getSyndicatedLink()).isNotPresent();
  }

  @Test
  void itLogsTheResponseCodeAsErrorIfNot2xx() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(499);

    // when
    sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    assertThat(logger.getLoggingEvents())
        .contains(
            error(
                "Webmention failed ({}) for {} to {}",
                499,
                "https://source.foo",
                "https://bar.bar/foo.html"));
  }

  @Test
  void itReturnsClientErrorIfNot2xx() throws Exception {
    // given
    when(mockResponse.getStatusCode()).thenReturn(400);

    // when
    Future<WebmentionResponse> actual =
        sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    assertThat(actual.get().getWebmentionStatus()).isEqualTo(WebmentionStatus.CLIENT_ERROR);
    assertThat(actual.get().getSyndicatedLink()).isNotPresent();
  }

  @Test
  void itLogsErrorTypeIfPresent() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(401);
    when(mockResponse.path("error")).thenReturn("something_wrong");

    // when
    sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    assertThat(logger.getLoggingEvents())
        .contains(
            error(
                "Webmention failed ({}: {}) for {} to {}",
                401,
                "something_wrong",
                "https://source.foo",
                "https://bar.bar/foo.html"));
  }

  @Test
  void itRetrievesTheQueueStatus() throws JsonProcessingException {
    // given
    when(mockResponse.getStatusCode()).thenReturn(201);
    when(mockResponse.getHeader("Location")).thenReturn("https://telegraph.status/queued/1234");
    setupSuccessStatus("");

    // when
    sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    verify(mockStatusRequestSpecification).get("https://telegraph.status/queued/1234");
  }

  @Test
  void itRetriesStatusChecks3MoreTimesAndNoLongerQueued() throws Exception {
    // given
    when(mockResponse.getStatusCode()).thenReturn(201);
    String queued = "https://telegraph.status/queued/1234";
    when(mockResponse.getHeader("Location")).thenReturn(queued);
    setupSuccessStatus("https://silo/status/1234");

    RequestSpecification initiallyQueued = mock(RequestSpecification.class);
    RequestSpecification retry1 = mock(RequestSpecification.class);
    RequestSpecification retry2 = mock(RequestSpecification.class);
    RequestSpecification retry3 = mock(RequestSpecification.class);

    Response queueResponse = mock(Response.class);
    when(queueResponse.getStatusCode()).thenReturn(200);
    when(queueResponse.path("status")).thenReturn("queued");

    when(initiallyQueued.get(anyString())).thenReturn(queueResponse);
    when(retry1.get(anyString())).thenReturn(queueResponse);
    when(retry2.get(anyString())).thenReturn(queueResponse);

    when(retry3.get(anyString())).thenReturn(mockStatusResponse);

    when(mockRequestSpecificationFactory.newRequestSpecification())
        .thenReturn(mockRequestSpecification, initiallyQueued, retry1, retry2, retry3);

    // when
    WebmentionResponse actual =
        sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html").get();

    // then
    assertThat(actual.getWebmentionStatus()).isEqualTo(WebmentionStatus.SENT);
    assertThat(actual.getSyndicatedLink()).isPresent();
    assertThat(actual.getSyndicatedLink().get()).isEqualTo("https://silo/status/1234");

    verify(initiallyQueued).get("https://telegraph.status/queued/1234");
    verify(retry1).get("https://telegraph.status/queued/1234");
    verify(retry2).get("https://telegraph.status/queued/1234");
    verify(retry3).get("https://telegraph.status/queued/1234");

    verify(mockSleepUtil, times(4)).sleep(5000);
  }

  @Test
  void itRetriesStatusChecks3MoreTimesButIsStillQueued() throws Exception {
    // given
    when(mockResponse.getStatusCode()).thenReturn(201);
    when(mockResponse.getHeader("Location")).thenReturn("https://telegraph.status/queued/1234");

    RequestSpecification initiallyQueued = mock(RequestSpecification.class);
    RequestSpecification retry1 = mock(RequestSpecification.class);
    RequestSpecification retry2 = mock(RequestSpecification.class);
    RequestSpecification retry3 = mock(RequestSpecification.class);

    Response queueResponse = mock(Response.class);
    when(queueResponse.getStatusCode()).thenReturn(200);
    when(queueResponse.path("status")).thenReturn("queued");

    when(initiallyQueued.get(anyString())).thenReturn(queueResponse);
    when(retry1.get(anyString())).thenReturn(queueResponse);
    when(retry2.get(anyString())).thenReturn(queueResponse);
    when(retry3.get(anyString())).thenReturn(queueResponse);

    when(mockRequestSpecificationFactory.newRequestSpecification())
        .thenReturn(mockRequestSpecification, initiallyQueued, retry1, retry2, retry3);

    WebmentionResponse actual =
        sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html").get();

    // then
    assertThat(actual.getWebmentionStatus()).isEqualTo(WebmentionStatus.STILL_QUEUED);
    assertThat(actual.getSyndicatedLink()).isNotPresent();

    // then
    verify(initiallyQueued).get("https://telegraph.status/queued/1234");
    verify(retry1).get("https://telegraph.status/queued/1234");
    verify(retry2).get("https://telegraph.status/queued/1234");
    verify(retry3).get("https://telegraph.status/queued/1234");

    verify(mockSleepUtil, times(4)).sleep(5000);
  }

  @Test
  void itThrowsARuntimeExceptionIfNot200Ok() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(201);
    when(mockResponse.getHeader("Location")).thenReturn("https://telegraph.status/queued/1234");
    when(mockStatusResponse.getStatusCode()).thenReturn(502);

    // when
    assertThatThrownBy(() -> sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html"))
        // then
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Telegraph returned an unsuccessful response (502) when checking the"
                + " status for https://telegraph.status/queued/1234");
  }

  @Test
  void itReturnsStatus() throws Exception {
    // given
    when(mockResponse.getStatusCode()).thenReturn(201);
    when(mockResponse.getHeader("Location")).thenReturn("https://telegraph.status/queued/1234");
    when(mockStatusResponse.path("status")).thenReturn("accepted");
    setupSuccessStatus("https://silo/status/1234");

    // when
    Future<WebmentionResponse> actual =
        sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    assertThat(actual.get().getWebmentionStatus()).isEqualTo(WebmentionStatus.SENT);
    assertThat(actual.get().getSyndicatedLink()).isPresent();
    assertThat(actual.get().getSyndicatedLink().get()).isEqualTo("https://silo/status/1234");
  }

  @Test
  void itParsesUrlFromBodyAndReturnsAsSyndicatedLink()
      throws ExecutionException, InterruptedException, JsonProcessingException {
    // given
    when(mockResponse.getStatusCode()).thenReturn(201);
    when(mockResponse.getHeader("Location")).thenReturn("https://telegraph.status/queued/1234");
    setupSuccessStatus("https://foo.bar.syndicated/wibble");

    // when
    Future<WebmentionResponse> actual =
        sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    assertThat(actual.get().getWebmentionStatus()).isEqualTo(WebmentionStatus.SENT);
    assertThat(actual.get().getSyndicatedLink()).isPresent();
    assertThat(actual.get().getSyndicatedLink().get())
        .isEqualTo("https://foo.bar.syndicated/wibble");
  }

  @Test
  void itReturnsEmptyOptionalAsSyndicatedLinkIfNotJson()
      throws ExecutionException, InterruptedException {
    // given
    when(mockResponse.getStatusCode()).thenReturn(201);
    when(mockResponse.getHeader("Location")).thenReturn("https://telegraph.status/queued/1234");
    when(mockStatusResponse.getStatusCode()).thenReturn(200);
    when(mockStatusResponse.path("http_body")).thenReturn("Not JSON response body!");

    // when
    Future<WebmentionResponse> actual =
        sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    assertThat(actual.get().getWebmentionStatus()).isEqualTo(WebmentionStatus.SENT);
    assertThat(actual.get().getSyndicatedLink()).isNotPresent();
  }

  @Test
  void itReturnsEmptyOptionalAsSyndicatedLinkIfNoUrlKey()
      throws ExecutionException, InterruptedException {
    // given
    when(mockResponse.getStatusCode()).thenReturn(201);
    when(mockResponse.getHeader("Location")).thenReturn("https://telegraph.status/queued/1234");
    when(mockStatusResponse.getStatusCode()).thenReturn(200);
    when(mockStatusResponse.path("http_body")).thenReturn("{}");

    // when
    Future<WebmentionResponse> actual =
        sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    assertThat(actual.get().getWebmentionStatus()).isEqualTo(WebmentionStatus.SENT);
    assertThat(actual.get().getSyndicatedLink()).isNotPresent();
  }

  @Test
  void itReturnsEmptyOptionalAsSyndicatedLinkIfUrlIsEmptyString()
      throws ExecutionException, InterruptedException, JsonProcessingException {
    // given
    when(mockResponse.getStatusCode()).thenReturn(201);
    when(mockResponse.getHeader("Location")).thenReturn("https://telegraph.status/queued/1234");
    when(mockStatusResponse.getStatusCode()).thenReturn(200);
    Map<String, String> innerBody = new HashMap<>();
    innerBody.put("url", "");
    when(mockStatusResponse.path("http_body"))
        .thenReturn(OBJECT_MAPPER.writeValueAsString(innerBody));

    // when
    Future<WebmentionResponse> actual =
        sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    assertThat(actual.get().getWebmentionStatus()).isEqualTo(WebmentionStatus.SENT);
    assertThat(actual.get().getSyndicatedLink()).isNotPresent();
  }

  @Test
  void itReturnsEmptyOptionalAsSyndicatedLinkIfUrlIsNotString()
      throws ExecutionException, InterruptedException, JsonProcessingException {
    // given
    when(mockResponse.getStatusCode()).thenReturn(201);
    when(mockResponse.getHeader("Location")).thenReturn("https://telegraph.status/queued/1234");
    when(mockStatusResponse.getStatusCode()).thenReturn(200);
    Map<String, Object> innerBody = new HashMap<>();
    innerBody.put("url", new HashMap<>());
    when(mockStatusResponse.path("http_body"))
        .thenReturn(OBJECT_MAPPER.writeValueAsString(innerBody));

    // when
    Future<WebmentionResponse> actual =
        sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    assertThat(actual.get().getWebmentionStatus()).isEqualTo(WebmentionStatus.SENT);
    assertThat(actual.get().getSyndicatedLink()).isNotPresent();
  }

  @Test
  void itReturnsEmptyOptionalAsSyndicatedLinkIfNoHttpBodyReturned()
      throws ExecutionException, InterruptedException, JsonProcessingException {
    // given
    when(mockResponse.getStatusCode()).thenReturn(201);
    when(mockResponse.getHeader("Location")).thenReturn("https://telegraph.status/queued/1234");
    when(mockStatusResponse.getStatusCode()).thenReturn(200);

    // when
    Future<WebmentionResponse> actual =
        sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    assertThat(actual.get().getWebmentionStatus()).isEqualTo(WebmentionStatus.SENT);
    assertThat(actual.get().getSyndicatedLink()).isNotPresent();
  }

  @Test
  void itLogsTheStatusResponseStatusWhenSuccessful() throws JsonProcessingException {
    // given
    when(mockResponse.getStatusCode()).thenReturn(201);
    when(mockResponse.getHeader("Location")).thenReturn("https://telegraph.status/queued/1234");
    when(mockStatusResponse.path("status")).thenReturn("accepted");
    setupSuccessStatus("");

    // when
    sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    assertThat(logger.getLoggingEvents())
        .contains(
            info(
                "Webmention succeeded ({}) for {} to {}",
                "accepted",
                "https://source.foo",
                "https://bar.bar/foo.html"));
  }

  @Test
  void itLogsTheStatusResponseStatusWhenNotSuccessful() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(201);
    when(mockResponse.getHeader("Location")).thenReturn("https://telegraph.status/queued/1234");
    when(mockStatusResponse.path("status")).thenReturn("no_link_found");

    // when
    sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    assertThat(logger.getLoggingEvents())
        .contains(
            error(
                "Webmention failed ({}) for {} to {}",
                "no_link_found",
                "https://source.foo",
                "https://bar.bar/foo.html"));
  }

  @Test
  void itLogsTheStatusResponseStatusWhenErrorOccurs() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(201);
    when(mockResponse.getHeader("Location")).thenReturn("https://telegraph.status/queued/1234");
    when(mockStatusResponse.path("status")).thenReturn("flooble_crank");

    // when
    sut.sendWebmention("https://source.foo", "https://bar.bar/foo.html");

    // then
    assertThat(logger.getLoggingEvents())
        .contains(
            error(
                "Webmention failed with unknown status ({}) for {} to {}",
                "flooble_crank",
                "https://source.foo",
                "https://bar.bar/foo.html"));
  }

  private void setupSuccessStatus(String url) throws JsonProcessingException {
    when(mockStatusResponse.getStatusCode()).thenReturn(200);
    Map<String, String> innerBody = new HashMap<>();
    innerBody.put("url", url);
    when(mockStatusResponse.path("http_body"))
        .thenReturn(OBJECT_MAPPER.writeValueAsString(innerBody));
  }
}
