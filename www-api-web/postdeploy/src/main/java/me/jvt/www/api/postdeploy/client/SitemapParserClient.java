package me.jvt.www.api.postdeploy.client;

import java.util.concurrent.Future;
import org.sitemaps.schemas.sitemap._0.Urlset;
import org.springframework.scheduling.annotation.Async;

public interface SitemapParserClient {
  @Async
  Future<Urlset> parseSitemap(String sitemapUrl);
}
