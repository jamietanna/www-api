package me.jvt.www.api.postdeploy.jsoup;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

public class JsoupConnector {
  public Connection connect(String url) {
    return Jsoup.connect(url);
  }
}
