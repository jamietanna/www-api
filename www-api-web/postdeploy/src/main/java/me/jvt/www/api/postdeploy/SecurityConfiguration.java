package me.jvt.www.api.postdeploy;

import me.jvt.www.api.postdeploy.security.GitLabSharedSecretAuthFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

@Configuration
@EnableWebSecurity
@Order(1)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Value("${gitlab.sharedSecret.header}")
  private String principalRequestHeader;

  @Value("${gitlab.sharedSecret.value}")
  private String principalRequestValue;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    GitLabSharedSecretAuthFilter gitLabSharedSecretAuthFilter =
        new GitLabSharedSecretAuthFilter(principalRequestHeader);
    gitLabSharedSecretAuthFilter.setAuthenticationManager(
        new AuthenticationManager() {
          public Authentication authenticate(Authentication authentication)
              throws AuthenticationException {
            String principal = (String) authentication.getPrincipal();
            if (!principalRequestValue.equals(principal)) {
              throw new BadCredentialsException(
                  "The shared secret was not found or not the expected value.");
            }
            authentication.setAuthenticated(true);
            return authentication;
          }
        });

    http.antMatcher("/post-deploy")
        .csrf()
        .disable()
        .addFilter(gitLabSharedSecretAuthFilter)
        .authorizeRequests()
        .anyRequest()
        .authenticated();
  }
}
