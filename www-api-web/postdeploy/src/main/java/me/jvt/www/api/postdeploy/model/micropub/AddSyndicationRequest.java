package me.jvt.www.api.postdeploy.model.micropub;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddSyndicationRequest {

  private static final String action = "update";
  private final String url;
  private final Map<String, List<String>> additions;

  public AddSyndicationRequest(String url, String syndicationUrl) {
    this.url = url;
    this.additions = new HashMap<>();
    this.additions.put("syndication", Collections.singletonList(syndicationUrl));
  }

  public String getAction() {
    return action;
  }

  @JsonProperty("add")
  public Map<String, List<String>> getAdditions() {
    return additions;
  }

  public String getUrl() {
    return url;
  }
}
