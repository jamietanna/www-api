package me.jvt.www.api.postdeploy.persistence;

import me.jvt.www.api.postdeploy.persistence.dao.Webmention;
import me.jvt.www.api.postdeploy.persistence.dao.WebmentionIdentity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WebmentionRepository extends CrudRepository<Webmention, WebmentionIdentity> {}
