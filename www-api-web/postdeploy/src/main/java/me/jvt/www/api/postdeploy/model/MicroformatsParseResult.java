package me.jvt.www.api.postdeploy.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MicroformatsParseResult {

  public List<Hfeed> items;
  public Rels rels;

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Rels {

    public List<String> next;
  }
}
