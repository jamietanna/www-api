package me.jvt.www.api.postdeploy.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Hentry {

  public Properties properties;

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Properties {

    public List<String> url;
  }
}
