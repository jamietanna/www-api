package me.jvt.www.api.postdeploy.client;

import static io.restassured.RestAssured.given;

public class BingSearchEngineClient implements SearchEngineClient {

  public void notifySitemap(String sitemapUrl) {
    given()
        .when()
        .post(String.format("https://bing.com/webmaster/ping.aspx?siteMap=%s", sitemapUrl));
  }
}
