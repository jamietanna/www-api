package me.jvt.www.api.postdeploy.persistence.dao;

import java.time.Instant;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import me.jvt.www.api.postdeploy.model.WebmentionStatus;

@Entity
@Table(name = "webmentions")
public class Webmention {

  @EmbeddedId private WebmentionIdentity webmentionIdentity;
  @NotNull private Instant lastSentWebmention;
  @NotNull private WebmentionStatus lastWebmentionStatus;

  public Webmention() {}

  public Webmention(
      WebmentionIdentity webmentionIdentity,
      @NotNull Instant lastSentWebmention,
      @NotNull WebmentionStatus lastWebmentionStatus) {
    this.webmentionIdentity = webmentionIdentity;
    this.lastSentWebmention = lastSentWebmention;
    this.lastWebmentionStatus = lastWebmentionStatus;
  }

  public WebmentionIdentity getWebmentionIdentity() {
    return webmentionIdentity;
  }

  public void setWebmentionIdentity(WebmentionIdentity webmentionIdentity) {
    this.webmentionIdentity = webmentionIdentity;
  }

  public Instant getLastSentWebmention() {
    return lastSentWebmention;
  }

  public void setLastSentWebmention(Instant lastSentWebmention) {
    this.lastSentWebmention = lastSentWebmention;
  }

  public WebmentionStatus getLastWebmentionStatus() {
    return lastWebmentionStatus;
  }

  public void setLastWebmentionStatus(WebmentionStatus lastWebmentionStatus) {
    this.lastWebmentionStatus = lastWebmentionStatus;
  }
}
