package me.jvt.www.api.postdeploy.validators;

import java.util.Optional;
import me.jvt.www.api.postdeploy.model.WebmentionStatus;
import me.jvt.www.api.postdeploy.persistence.WebmentionRepository;
import me.jvt.www.api.postdeploy.persistence.dao.Webmention;
import me.jvt.www.api.postdeploy.persistence.dao.WebmentionIdentity;

public class WebmentionNotAlreadySentValidator implements WebmentionValidator {

  private final WebmentionRepository webmentionRepository;

  public WebmentionNotAlreadySentValidator(WebmentionRepository webmentionRepository) {
    this.webmentionRepository = webmentionRepository;
  }

  @Override
  public boolean shouldSend(String source, String target) {
    Optional<Webmention> webmentionOptional =
        webmentionRepository.findById(new WebmentionIdentity(source, target));
    if (!webmentionOptional.isPresent()) {
      return true;
    }

    Webmention webmention = webmentionOptional.get();

    return !webmention.getLastWebmentionStatus().equals(WebmentionStatus.CLIENT_ERROR)
        && !webmention.getLastWebmentionStatus().equals(WebmentionStatus.SENT);
  }
}
