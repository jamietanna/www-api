package me.jvt.www.api.postdeploy.validators;

public interface WebmentionValidator {

  boolean shouldSend(String source, String target);
}
