package me.jvt.www.api.postdeploy.client;

import java.util.concurrent.Future;
import me.jvt.www.api.core.RequestSpecificationFactory;
import org.sitemaps.schemas.sitemap._0.Urlset;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;

public class SitemapParserClientImpl implements SitemapParserClient {

  private final RequestSpecificationFactory requestSpecificationFactory;

  public SitemapParserClientImpl(RequestSpecificationFactory requestSpecificationFactory) {
    this.requestSpecificationFactory = requestSpecificationFactory;
  }

  @Async
  @Override
  public Future<Urlset> parseSitemap(String sitemapUrl) {
    return new AsyncResult<>(
        requestSpecificationFactory.newRequestSpecification().get(sitemapUrl).as(Urlset.class));
  }
}
