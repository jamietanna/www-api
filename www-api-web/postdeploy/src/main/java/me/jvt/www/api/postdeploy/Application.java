package me.jvt.www.api.postdeploy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(
    scanBasePackages = {"me.jvt.www.api.postdeploy", "me.jvt.www.indieauthcontroller"})
public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class);
  }
}
