package me.jvt.www.api.postdeploy.validators;

import me.jvt.www.api.postdeploy.model.GitLabWebHookEvent;
import org.springframework.stereotype.Component;

@Component
public class GitLabWebHookEventValidator {

  private static final String VALID_BRANCH = "main";
  private static final String VALID_OBJECT_KIND = "pipeline";
  private static final String VALID_STATUS = "success";

  boolean validateBranch(GitLabWebHookEvent event) {
    return event.objectAttributes.ref.equals(VALID_BRANCH);
  }

  boolean validateObjectKind(GitLabWebHookEvent event) {
    return event.objectKind.equals(VALID_OBJECT_KIND);
  }

  boolean validateStatus(GitLabWebHookEvent event) {
    return event.objectAttributes.status.equals(VALID_STATUS);
  }

  public boolean validate(GitLabWebHookEvent event) {
    return validateBranch(event) && validateObjectKind(event) && validateStatus(event);
  }
}
