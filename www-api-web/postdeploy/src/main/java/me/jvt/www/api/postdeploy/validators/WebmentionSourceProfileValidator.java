package me.jvt.www.api.postdeploy.validators;

public class WebmentionSourceProfileValidator implements WebmentionValidator {

  private static final String PROFILE_URL = "https://www.jvt.me";
  private static final String PROFILE_URL_TRAILING_SLASH = "https://www.jvt.me/";

  @Override
  public boolean shouldSend(String source, String target) {
    return !PROFILE_URL.equals(source) && !PROFILE_URL_TRAILING_SLASH.equals(source);
  }
}
