package me.jvt.www.api.postdeploy;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.util.Config;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.postdeploy.client.BingSearchEngineClient;
import me.jvt.www.api.postdeploy.client.GoogleSearchEngineClient;
import me.jvt.www.api.postdeploy.client.HentryWebmentionUrlResolver;
import me.jvt.www.api.postdeploy.client.JsoupEntryWebmentionUrlResolver;
import me.jvt.www.api.postdeploy.client.SearchEngineClient;
import me.jvt.www.api.postdeploy.client.SitemapParserClient;
import me.jvt.www.api.postdeploy.client.SitemapParserClientImpl;
import me.jvt.www.api.postdeploy.client.SyndicationUpdateClient;
import me.jvt.www.api.postdeploy.client.TelegraphWebmentionClient;
import me.jvt.www.api.postdeploy.client.WebmentionClient;
import me.jvt.www.api.postdeploy.jsoup.JsoupConnector;
import me.jvt.www.api.postdeploy.persistence.WebmentionRepository;
import me.jvt.www.api.postdeploy.service.CarefulWebmentionService.CarefulWebmentionServiceBuilder;
import me.jvt.www.api.postdeploy.service.PostDeployService;
import me.jvt.www.api.postdeploy.service.PostDeployServiceImpl;
import me.jvt.www.api.postdeploy.service.WebmentionOrchestatorService;
import me.jvt.www.api.postdeploy.service.WebmentionService;
import me.jvt.www.api.postdeploy.util.SleepUtil;
import me.jvt.www.api.postdeploy.validators.GitLabWebHookEventValidator;
import me.jvt.www.api.postdeploy.validators.WebmentionNotAlreadySentValidator;
import me.jvt.www.api.postdeploy.validators.WebmentionNotSupportedRateLimiter;
import me.jvt.www.api.postdeploy.validators.WebmentionSourceProfileValidator;
import me.jvt.www.indieauthclient.*;
import me.jvt.www.indieauthcontroller.store.RefreshingTokenStore;
import me.jvt.www.indieauthcontroller.store.TokenStore;
import me.jvt.www.indieauthcontroller.store.kubernetes.KubernetesSecretTokenStore;
import me.jvt.www.indieauthcontroller.store.kubernetes.PatchBodyFactory;
import me.jvt.www.logging.LoggingConfiguration;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

@Component
@EnableAsync
@EnableJpaRepositories
@ComponentScan("me.jvt.www.servlet")
@Import(LoggingConfiguration.class)
public class Configuration {

  @Value("${telegraph.apiToken}")
  private String telegraphApiToken;

  @Bean(name = "telegraph.apiToken")
  public String telegraphApiToken() {
    return telegraphApiToken;
  }

  @Bean
  public SyndicationUpdateClient syndicationUpdateClient(
      RequestSpecificationFactory requestSpecificationFactory, TokenStore tokenStore) {
    return new SyndicationUpdateClient(tokenStore, requestSpecificationFactory);
  }

  @Bean
  public WebmentionOrchestatorService webmentionOrchestatorService(
      HentryWebmentionUrlResolver hentryWebmentionUrlResolver,
      WebmentionService webmentionService,
      SyndicationUpdateClient syndicationUpdateClient) {
    return new WebmentionOrchestatorService(
        hentryWebmentionUrlResolver, webmentionService, syndicationUpdateClient);
  }

  @Bean
  public PostDeployService postDeployService(
      GitLabWebHookEventValidator gitLabWebHookEventValidator,
      List<SearchEngineClient> searchEngineClients,
      SitemapParserClient sitemapParserClient,
      WebmentionOrchestatorService webmentionOrchestatorService) {
    return new PostDeployServiceImpl(
        gitLabWebHookEventValidator,
        searchEngineClients,
        sitemapParserClient,
        webmentionOrchestatorService);
  }

  @Bean
  public RequestSpecificationFactory requestSpecificationFactory() {
    return new RequestSpecificationFactory();
  }

  @Bean
  public List<SearchEngineClient> searchEngineClients() {
    List<SearchEngineClient> searchEngineClients = new ArrayList<SearchEngineClient>();
    searchEngineClients.add(new BingSearchEngineClient());
    searchEngineClients.add(new GoogleSearchEngineClient());
    return searchEngineClients;
  }

  @Bean
  public SitemapParserClient sitemapParserClient(
      RequestSpecificationFactory requestSpecificationFactory) {
    return new SitemapParserClientImpl(requestSpecificationFactory);
  }

  @Bean
  public HentryWebmentionUrlResolver hentryWebmentionSender(JsoupConnector jsoupConnector) {
    return new JsoupEntryWebmentionUrlResolver(jsoupConnector);
  }

  @Bean
  public JsoupConnector jsoupConnector() {
    return new JsoupConnector();
  }

  @Bean
  public SleepUtil sleepUtil() {
    return new SleepUtil();
  }

  @Bean
  public WebmentionClient webmentionClient(
      RequestSpecificationFactory requestSpecificationFactory,
      @Qualifier("telegraph.apiToken") String telegraphApiToken,
      SleepUtil sleepUtil,
      ObjectMapper objectMapper) {
    return new TelegraphWebmentionClient(
        requestSpecificationFactory, telegraphApiToken, sleepUtil, objectMapper);
  }

  @Bean
  public WebmentionService webmentionService(
      WebmentionClient webmentionClient, WebmentionRepository webmentionRepository) {
    return CarefulWebmentionServiceBuilder.aCarefulWebmentionService()
        .withWebmentionClient(webmentionClient)
        .withWebmentionValidator(new WebmentionNotAlreadySentValidator(webmentionRepository))
        .withWebmentionValidator(new WebmentionSourceProfileValidator())
        .withWebmentionValidator(new WebmentionNotSupportedRateLimiter(webmentionRepository))
        .withWebmentionRepository(webmentionRepository)
        .build();
  }

  @Bean
  @Qualifier("taskExecutor")
  public Executor taskExecutor(@Value("${async.pools.default.corePoolSize}") int corePoolSize) {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(corePoolSize);
    executor.setThreadNamePrefix("taskExecutor-");
    executor.initialize();
    return executor;
  }

  @Bean
  @Qualifier("webmentionClientThreadPoolExecutor")
  public Executor webmentionClientThreadPoolExecutor(
      @Value("${async.pools.webmentionClient.corePoolSize}") int corePoolSize) {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(corePoolSize);
    executor.setThreadNamePrefix("wm-");
    executor.initialize();
    return executor;
  }

  @Bean
  @Primary
  public IndieAuthService indieAuthService(IndieAuthClient delegate) {
    return new InMemoryIndieAuthService(delegate);
  }

  @Bean
  public IndieAuthClient indieAuthClientDelegate(
      @Value("${indieauth.endpoint.authorization}") String authorizationEndpoint,
      @Value("${indieauth.endpoint.token}") String tokenEndpoint,
      @Value("${postdeploy.indieauth.client-id}") String clientId,
      @Value("${postdeploy.indieauth.redirect-uri}") String redirectUri,
      RequestSpecificationFactory requestSpecificationFactory) {
    return new IndieAuthClient(
        new StaticIndieAuthConfiguration(authorizationEndpoint, tokenEndpoint),
        clientId,
        redirectUri,
        requestSpecificationFactory);
  }

  @Bean
  public ApiClient apiClient() throws IOException {
    return Config.fromCluster();
  }

  @Bean
  public TokenStore tokenStore(
      IndieAuthService service, PatchBodyFactory factory, ApiClient apiClient) {
    TokenStore delegate =
        new KubernetesSecretTokenStore(
            "post-deploy-secrets", "www-api", "postdeploy.tokens.txt", factory, apiClient);
    return new RefreshingTokenStore(service, delegate);
  }
}
