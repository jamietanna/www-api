package me.jvt.www.api.postdeploy.client;

import java.util.concurrent.Future;
import me.jvt.www.api.postdeploy.model.Hfeed;

public interface HfeedParserClient {

  Future<Hfeed> resolveAllPosts(String hfeedUrl);
}
