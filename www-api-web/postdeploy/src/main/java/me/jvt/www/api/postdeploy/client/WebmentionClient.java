package me.jvt.www.api.postdeploy.client;

import java.util.concurrent.Future;
import me.jvt.www.api.postdeploy.model.WebmentionResponse;
import org.springframework.scheduling.annotation.Async;

public interface WebmentionClient {

  @Async("webmentionClientThreadPoolExecutor")
  Future<WebmentionResponse> sendWebmention(String source, String target);
}
