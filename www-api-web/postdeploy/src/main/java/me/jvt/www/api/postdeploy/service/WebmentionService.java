package me.jvt.www.api.postdeploy.service;

import java.util.Optional;
import java.util.concurrent.Future;
import org.springframework.scheduling.annotation.Async;

public interface WebmentionService {

  @Async("webmentionClientThreadPoolExecutor")
  Future<Optional<String>> sendWebmention(String source, String target);
}
