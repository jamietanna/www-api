package me.jvt.www.api.postdeploy.client;

import java.util.Set;
import me.jvt.www.api.postdeploy.util.WebmentionPair;

public interface HentryWebmentionUrlResolver {

  Set<WebmentionPair> resolveAllUrls(String url);
}
