package me.jvt.www.api.postdeploy.client;

public interface SearchEngineClient {

  void notifySitemap(String sitemapUrl);
}
