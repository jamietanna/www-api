package me.jvt.www.api.postdeploy.client;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.postdeploy.model.micropub.AddSyndicationRequest;
import me.jvt.www.api.postdeploy.model.micropub.SyndicateTo;
import me.jvt.www.api.postdeploy.model.micropub.SyndicateToResponse;
import me.jvt.www.indieauthcontroller.store.TokenStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

public class SyndicationUpdateClient {

  private static final Logger LOGGER = LoggerFactory.getLogger(SyndicationUpdateClient.class);
  private static final Set<Integer> SUCCESS_UPDATE_STATUS;

  static {
    SUCCESS_UPDATE_STATUS = new HashSet<>();
    SUCCESS_UPDATE_STATUS.add(HttpStatus.OK.value());
    SUCCESS_UPDATE_STATUS.add(HttpStatus.CREATED.value());
    SUCCESS_UPDATE_STATUS.add(HttpStatus.NO_CONTENT.value());
  }

  private final TokenStore tokenStore;
  private final RequestSpecificationFactory requestSpecificationFactory;

  public SyndicationUpdateClient(
      TokenStore tokenStore, RequestSpecificationFactory requestSpecificationFactory) {
    this.tokenStore = tokenStore;
    this.requestSpecificationFactory = requestSpecificationFactory;
  }

  public Set<SyndicateTo> retrieveSyndication() throws SyndicationRetrievalException {
    Response response =
        requestSpecificationFactory
            .newRequestSpecification()
            .param("q", "syndicate-to")
            .get("https://www-api.jvt.me/micropub");
    if (HttpStatus.OK.value() != response.getStatusCode()) {
      throw new SyndicationRetrievalException(
          String.format("Syndication query endpoint returned %d", response.getStatusCode()));
    }

    SyndicateToResponse syndicateToResponse = response.as(SyndicateToResponse.class);
    Set<SyndicateTo> syndication = new HashSet<>();
    for (Map<String, String> syndicate : syndicateToResponse.getSyndicateTo()) {
      syndication.add(new SyndicateTo(syndicate.get("uid"), syndicate.get("name")));
    }
    return syndication;
  }

  public void updateSyndication(String url, String syndicatedLink) {
    Response response =
        requestSpecificationFactory
            .newRequestSpecification()
            .auth()
            .preemptive()
            .oauth2(tokenStore.get())
            .contentType(ContentType.JSON)
            .body(new AddSyndicationRequest(url, syndicatedLink))
            .post("https://www-api.jvt.me/micropub");

    if (SUCCESS_UPDATE_STATUS.contains(response.getStatusCode())) {
      LOGGER.info("Post {} updated with syndication link {}", url, syndicatedLink);
    } else {
      LOGGER.error(
          "Post {} failed to update with link {}: HTTP {}",
          url,
          syndicatedLink,
          response.getStatusCode());
    }
  }
}
