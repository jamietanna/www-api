package me.jvt.www.api.postdeploy.validators;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import me.jvt.www.api.postdeploy.model.WebmentionStatus;
import me.jvt.www.api.postdeploy.persistence.WebmentionRepository;
import me.jvt.www.api.postdeploy.persistence.dao.Webmention;
import me.jvt.www.api.postdeploy.persistence.dao.WebmentionIdentity;

public class WebmentionNotSupportedRateLimiter implements WebmentionValidator {

  private static final int MAXIMUM_DAYS_BEFORE_RESET = 14;

  private final WebmentionRepository webmentionRepository;

  public WebmentionNotSupportedRateLimiter(WebmentionRepository webmentionRepository) {
    this.webmentionRepository = webmentionRepository;
  }

  @Override
  public boolean shouldSend(String source, String target) {
    Optional<Webmention> optionalWebmention =
        webmentionRepository.findById(new WebmentionIdentity(source, target));
    if (!optionalWebmention.isPresent()) {
      return true;
    }
    Webmention webmention = optionalWebmention.get();

    if (WebmentionStatus.SERVER_DOES_NOT_SUPPORT != webmention.getLastWebmentionStatus()) {
      return true;
    }

    Instant now = Instant.now();
    return MAXIMUM_DAYS_BEFORE_RESET
        <= ChronoUnit.DAYS.between(webmention.getLastSentWebmention(), now);
  }
}
