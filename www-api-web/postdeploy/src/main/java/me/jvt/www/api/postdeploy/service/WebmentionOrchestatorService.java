package me.jvt.www.api.postdeploy.service;

import java.util.Collections;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;
import me.jvt.www.api.postdeploy.client.HentryWebmentionUrlResolver;
import me.jvt.www.api.postdeploy.client.SyndicationRetrievalException;
import me.jvt.www.api.postdeploy.client.SyndicationUpdateClient;
import me.jvt.www.api.postdeploy.model.micropub.SyndicateTo;
import me.jvt.www.api.postdeploy.util.WebmentionPair;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

public class WebmentionOrchestatorService {

  private final HentryWebmentionUrlResolver hentryWebmentionUrlResolver;
  private final WebmentionService webmentionService;
  private final SyndicationUpdateClient syndicationUpdateClient;

  public WebmentionOrchestatorService(
      HentryWebmentionUrlResolver hentryWebmentionUrlResolver,
      WebmentionService webmentionService,
      SyndicationUpdateClient syndicationUpdateClient) {
    this.hentryWebmentionUrlResolver = hentryWebmentionUrlResolver;
    this.webmentionService = webmentionService;
    this.syndicationUpdateClient = syndicationUpdateClient;
  }

  @Async
  public void sendWebmentionsForUrl(String url) {
    Set<SyndicateTo> syndication;
    try {
      syndication = syndicationUpdateClient.retrieveSyndication();
    } catch (SyndicationRetrievalException e) {
      // don't attempt to syndicate
      syndication = Collections.emptySet();
    }
    Set<WebmentionPair> urlsToSendWebmentionsTo = hentryWebmentionUrlResolver.resolveAllUrls(url);

    for (Iterator<WebmentionPair> it = urlsToSendWebmentionsTo.iterator(); it.hasNext(); ) {
      WebmentionPair webmentionPair = it.next();
      Optional<String> maybeSyndicatedLink = Optional.empty();
      try {
        maybeSyndicatedLink =
            webmentionService
                .sendWebmention(webmentionPair.getSource(), webmentionPair.getTarget())
                .get();
      } catch (Exception e) {
        LoggerFactory.getLogger(this.getClass()).error("Sending webmention failed", e);
      }
      if (maybeSyndicatedLink.isPresent()) {
        String syndicationLink = maybeSyndicatedLink.get();
        for (SyndicateTo syndicateTo : syndication) {
          if (syndicateTo.getUid().equals(webmentionPair.getTarget())) {
            syndicationUpdateClient.updateSyndication(webmentionPair.getSource(), syndicationLink);
          }
        }
      }
    }
  }
}
