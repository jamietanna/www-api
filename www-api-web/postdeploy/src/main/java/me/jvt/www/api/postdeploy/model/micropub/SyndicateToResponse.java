package me.jvt.www.api.postdeploy.model.micropub;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SyndicateToResponse {
  @JsonProperty("syndicate-to")
  private Set<Map<String, String>> syndicateTo;

  public Set<Map<String, String>> getSyndicateTo() {
    return syndicateTo;
  }

  public void setSyndicateTo(Set<Map<String, String>> syndicateTo) {
    this.syndicateTo = syndicateTo;
  }
}
