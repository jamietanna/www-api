package me.jvt.www.api.postdeploy.model.micropub;

import java.util.Objects;

public class SyndicateTo {

  private final String uid;

  private final String name;

  public SyndicateTo(String uid, String name) {
    this.uid = uid;
    this.name = name;
  }

  public String getUid() {
    return uid;
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SyndicateTo that = (SyndicateTo) o;
    return Objects.equals(uid, that.uid) && Objects.equals(name, that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(uid, name);
  }
}
