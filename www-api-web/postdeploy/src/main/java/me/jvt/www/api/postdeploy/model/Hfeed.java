package me.jvt.www.api.postdeploy.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Hfeed {

  public List<Hentry> children;
}
