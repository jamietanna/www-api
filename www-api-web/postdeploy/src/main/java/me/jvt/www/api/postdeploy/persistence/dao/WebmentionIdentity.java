package me.jvt.www.api.postdeploy.persistence.dao;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class WebmentionIdentity implements Serializable {

  @NotNull private String source;
  @NotNull private String target;

  public WebmentionIdentity() {}

  public WebmentionIdentity(@NotNull String source, @NotNull String target) {
    this.source = source;
    this.target = target;
  }

  public String getSource() {
    return source;
  }

  public String getTarget() {
    return target;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WebmentionIdentity that = (WebmentionIdentity) o;
    return Objects.equals(source, that.source) && Objects.equals(target, that.target);
  }

  @Override
  public int hashCode() {
    return Objects.hash(source, target);
  }
}
