package me.jvt.www.api.postdeploy.client;

public class SyndicationRetrievalException extends Exception {
  public SyndicationRetrievalException(String message) {
    super(message);
  }
}
