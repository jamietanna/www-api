package me.jvt.www.api.postdeploy.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HentryMicroformatsParseResult {

  public List<Hentry> items;
  public Rels rels;

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Rels {

    public List<String> next;
  }
}
