package me.jvt.www.api.postdeploy.model;

public enum WebmentionStatus {
  SENT,
  SERVER_DOES_NOT_SUPPORT,
  CLIENT_ERROR,
  UNKNOWN_ERROR,
  STILL_QUEUED
}
