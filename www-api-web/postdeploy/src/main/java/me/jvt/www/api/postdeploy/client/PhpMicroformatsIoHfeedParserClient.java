package me.jvt.www.api.postdeploy.client;

import java.util.ArrayList;
import java.util.concurrent.Future;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.postdeploy.model.Hfeed;
import me.jvt.www.api.postdeploy.model.MicroformatsParseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;

public class PhpMicroformatsIoHfeedParserClient implements HfeedParserClient {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(PhpMicroformatsIoHfeedParserClient.class);

  private final RequestSpecificationFactory requestSpecificationFactory;

  public PhpMicroformatsIoHfeedParserClient(
      RequestSpecificationFactory requestSpecificationFactory) {
    this.requestSpecificationFactory = requestSpecificationFactory;
  }

  @Async
  @Override
  public Future<Hfeed> resolveAllPosts(String hfeedUrl) {
    Hfeed hfeed = new Hfeed();
    hfeed.children = new ArrayList<>();

    MicroformatsParseResult microformatsParseResult;
    String url = hfeedUrl;

    do {
      LOGGER.info("Retrieving h-feed from {}", url);
      // get the current page's data
      microformatsParseResult = parseMicroformats(url);

      if (microformatsParseResult.items.isEmpty()) {
        break;
      }

      // grab the entries for that Hfeed
      hfeed.children.addAll(microformatsParseResult.items.get(0).children);

      // then get ready to go to the next one (if it exists)
      if (microformatsParseResult.rels.next == null
          || 0 == microformatsParseResult.rels.next.size()) {
        url = null;
      } else {
        url = microformatsParseResult.rels.next.get(0);
      }
    } while (null != url);

    return new AsyncResult<>(hfeed);
  }

  private MicroformatsParseResult parseMicroformats(String url) {
    return requestSpecificationFactory
        .newRequestSpecification()
        .get(String.format("https://php.microformats.io/?url=%s", url))
        .as(MicroformatsParseResult.class);
  }
}
