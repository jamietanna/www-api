package me.jvt.www.api.postdeploy.model;

import java.util.Optional;

public class WebmentionResponse {

  private final WebmentionStatus webmentionStatus;
  private final Optional<String> maybeSyndicatedLink;

  public WebmentionResponse(
      WebmentionStatus webmentionStatus, Optional<String> maybeSyndicatedLink) {
    this.webmentionStatus = webmentionStatus;
    this.maybeSyndicatedLink = maybeSyndicatedLink;
  }

  public WebmentionResponse(TelegraphStatus telegraphStatus, Optional<String> maybeSyndicatedLink) {
    switch (telegraphStatus) {
      case SUCCESS:
      case ACCEPTED:
        this.webmentionStatus = WebmentionStatus.SENT;
        break;
      case NO_LINK_FOUND:
        this.webmentionStatus = WebmentionStatus.CLIENT_ERROR;
        break;
      case NOT_SUPPORTED:
        this.webmentionStatus = WebmentionStatus.SERVER_DOES_NOT_SUPPORT;
        break;
      case QUEUED:
        this.webmentionStatus = WebmentionStatus.STILL_QUEUED;
        break;
      case UNKNOWN:
      default:
        this.webmentionStatus = WebmentionStatus.UNKNOWN_ERROR;
        break;
    }
    this.maybeSyndicatedLink = maybeSyndicatedLink;
  }

  public Optional<String> getSyndicatedLink() {
    return maybeSyndicatedLink;
  }

  public WebmentionStatus getWebmentionStatus() {
    return webmentionStatus;
  }
}
