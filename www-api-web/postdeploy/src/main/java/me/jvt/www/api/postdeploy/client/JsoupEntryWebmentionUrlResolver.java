package me.jvt.www.api.postdeploy.client;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;
import me.jvt.www.api.postdeploy.jsoup.JsoupConnector;
import me.jvt.www.api.postdeploy.util.WebmentionPair;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class JsoupEntryWebmentionUrlResolver implements HentryWebmentionUrlResolver {

  private final JsoupConnector jsoupConnector;

  public JsoupEntryWebmentionUrlResolver(JsoupConnector jsoupConnector) {
    this.jsoupConnector = jsoupConnector;
  }

  @Override
  public Set<WebmentionPair> resolveAllUrls(String url) {
    Set<WebmentionPair> linksToSendWebmentionsTo = new HashSet<>();

    Document document = null;
    try {
      document = jsoupConnector.connect(url).get();
    } catch (IOException e) {
      throw new RuntimeException(
          String.format("An error occurred when reaching out to %s", url), e);
    }
    Elements hfeeds = document.select(".h-feed");
    // Don't send webmentions on feed pages because although we could let folks know they're listed
    // there, it's really not very useful and causes tonnes of spam, as well as the URLs forever
    // changing as new posts get published
    if (0 != hfeeds.size()) {
      return linksToSendWebmentionsTo;
    }

    Elements hentrys = document.select(".h-entry");
    Elements links = hentrys.select("a[href]");
    for (Element link : links) {
      URI linkUri = null;
      try {
        linkUri = new URI(link.attr("href"));
      } catch (URISyntaxException e) {
        throw new RuntimeException(String.format("Invalid link %s found", link.attr("href")), e);
      }
      if (linkUri.isAbsolute() && !isProfileUrl(linkUri)) {
        linksToSendWebmentionsTo.add(new WebmentionPair(url, link.attr("href")));
      }
    }

    return linksToSendWebmentionsTo;
  }

  private static boolean isProfileUrl(URI uri) {
    return uri.getHost().equals("www.jvt.me")
        && (uri.getPath().equals("") || uri.getPath().equals("/"));
  }
}
