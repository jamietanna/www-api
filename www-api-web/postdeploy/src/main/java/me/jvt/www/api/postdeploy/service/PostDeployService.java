package me.jvt.www.api.postdeploy.service;

import java.time.temporal.TemporalUnit;
import me.jvt.www.api.postdeploy.model.GitLabWebHookEvent;

public interface PostDeployService {

  boolean validate(GitLabWebHookEvent gitLabWebHookEvent);

  void sendNotifications(GitLabWebHookEvent gitLabWebHookEvent);

  void sendWebmentions(long thresholdCount, TemporalUnit thresholdUnit);
}
