package me.jvt.www.api.postdeploy.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Future;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.postdeploy.model.TelegraphStatus;
import me.jvt.www.api.postdeploy.model.WebmentionResponse;
import me.jvt.www.api.postdeploy.model.WebmentionStatus;
import me.jvt.www.api.postdeploy.util.SleepUtil;
import org.apache.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;

public class TelegraphWebmentionClient implements WebmentionClient {

  private static final int MAXIMUM_WEBMENTION_STATUS_CHECK_RETRIES = 4;
  private static final Logger LOGGER = LoggerFactory.getLogger(TelegraphWebmentionClient.class);

  private final RequestSpecificationFactory requestSpecificationFactory;
  private final String telegraphToken;
  private final SleepUtil sleepUtil;
  private final ObjectMapper objectMapper;

  public TelegraphWebmentionClient(
      RequestSpecificationFactory requestSpecificationFactory,
      String telegraphToken,
      SleepUtil sleepUtil,
      ObjectMapper objectMapper) {
    this.requestSpecificationFactory = requestSpecificationFactory;
    this.telegraphToken = telegraphToken;
    this.sleepUtil = sleepUtil;
    this.objectMapper = objectMapper;
  }

  @Async("webmentionClientThreadPoolExecutor")
  @Override
  public Future<WebmentionResponse> sendWebmention(String source, String target) {
    RequestSpecification requestSpecification =
        requestSpecificationFactory.newRequestSpecification();

    Response telegraphResponse =
        requestSpecification
            .baseUri("https://telegraph.p3k.io")
            .param("source", source)
            .param("target", target)
            .param("token", this.telegraphToken)
            .post("/webmention");

    int statusCode = telegraphResponse.getStatusCode();
    if (201 == statusCode) {
      String webmentionStatusResult = telegraphResponse.getHeader(HttpHeaders.LOCATION);
      Response statusResponse = retrieveStatusFromTelegraph(webmentionStatusResult);
      String telegraphStatusStr = statusResponse.path("status");
      TelegraphStatus telegraphStatus = TelegraphStatus.fromString(telegraphStatusStr);
      Optional<String> maybeSyndicatedLink = Optional.empty();
      if (telegraphStatus.isSuccessful()) {
        maybeSyndicatedLink = syndicatedUrl(statusResponse);
        LOGGER.info("Webmention succeeded ({}) for {} to {}", telegraphStatusStr, source, target);
      } else if (telegraphStatus.equals(TelegraphStatus.UNKNOWN)) {
        LOGGER.error(
            "Webmention failed with unknown status ({}) for {} to {}",
            telegraphStatusStr,
            source,
            target);
      } else {
        LOGGER.error("Webmention failed ({}) for {} to {}", telegraphStatusStr, source, target);
      }
      return new AsyncResult<>(new WebmentionResponse(telegraphStatus, maybeSyndicatedLink));
    } else if (200 <= statusCode && 300 > statusCode) {
      LOGGER.error(
          "Webmention returned invalid unsuccessful code ({}) for {} to {}",
          statusCode,
          source,
          target);
      return new AsyncResult<>(
          new WebmentionResponse(WebmentionStatus.UNKNOWN_ERROR, Optional.empty()));
    } else {
      // TODO: what if it's a 5xx?
      // TODO: only do this if there's a JSON response
      String errorMessage = telegraphResponse.path("error");
      if (null != errorMessage) {
        LOGGER.error(
            "Webmention failed ({}: {}) for {} to {}", statusCode, errorMessage, source, target);
      } else {
        LOGGER.error("Webmention failed ({}) for {} to {}", statusCode, source, target);
      }
      return new AsyncResult<>(
          new WebmentionResponse(WebmentionStatus.CLIENT_ERROR, Optional.empty()));
    }
  }

  private Response retrieveStatusFromTelegraph(String webmentionStatusResult) {
    int attempt = 0;
    String telegraphStatusStr;
    Response statusResponse;
    do {
      sleepUtil.sleep(5000);

      statusResponse =
          requestSpecificationFactory.newRequestSpecification().get(webmentionStatusResult);

      if (200 != statusResponse.getStatusCode()) {
        throw new RuntimeException(
            String.format(
                "Telegraph returned an unsuccessful response"
                    + " (%d) when checking the status for %s",
                statusResponse.getStatusCode(), webmentionStatusResult));
      }
      telegraphStatusStr = statusResponse.path("status");

      ++attempt;
    } while ("queued".equals(telegraphStatusStr)
        && MAXIMUM_WEBMENTION_STATUS_CHECK_RETRIES != attempt);

    return statusResponse;
  }

  private Optional<String> syndicatedUrl(Response statusResponse) {
    Map innerBody;
    Object httpBody = statusResponse.path("http_body");
    if (null == httpBody) {
      return Optional.empty();
    }

    try {
      innerBody = objectMapper.readValue(httpBody.toString(), Map.class);
    } catch (JsonProcessingException e) {
      return Optional.empty();
    }
    if (!innerBody.containsKey("url")) {
      return Optional.empty();
    }

    Object url = innerBody.get("url");
    if (!(url instanceof String)) {
      return Optional.empty();
    }
    String theUrl = (String) url;
    if (theUrl.isEmpty()) {
      return Optional.empty();
    }
    return Optional.of(theUrl);
  }
}
