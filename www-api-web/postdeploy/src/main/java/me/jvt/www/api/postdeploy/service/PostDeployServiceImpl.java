package me.jvt.www.api.postdeploy.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.TemporalUnit;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import me.jvt.www.api.postdeploy.client.SearchEngineClient;
import me.jvt.www.api.postdeploy.client.SitemapParserClient;
import me.jvt.www.api.postdeploy.model.GitLabWebHookEvent;
import me.jvt.www.api.postdeploy.validators.GitLabWebHookEventValidator;
import org.sitemaps.schemas.sitemap._0.TUrl;
import org.sitemaps.schemas.sitemap._0.Urlset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

public class PostDeployServiceImpl implements PostDeployService {

  private static final Logger LOGGER = LoggerFactory.getLogger(PostDeployServiceImpl.class);
  // https://stackoverflow.com/a/10614978/2257038
  private static final SimpleDateFormat W3C_DATETIME_FORMAT =
      new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");

  private final GitLabWebHookEventValidator gitLabWebHookEventValidator;
  private final List<SearchEngineClient> searchEngineClients;
  private final SitemapParserClient sitemapParserClient;
  private final WebmentionOrchestatorService webmentionOrchestatorService;

  public PostDeployServiceImpl(
      GitLabWebHookEventValidator gitLabWebHookEventValidator,
      List<SearchEngineClient> searchEngineClients,
      SitemapParserClient sitemapParserClient,
      WebmentionOrchestatorService webmentionOrchestatorService) {
    this.gitLabWebHookEventValidator = gitLabWebHookEventValidator;
    this.searchEngineClients = searchEngineClients;
    this.sitemapParserClient = sitemapParserClient;
    this.webmentionOrchestatorService = webmentionOrchestatorService;
  }

  public boolean validate(GitLabWebHookEvent gitLabWebHookEvent) {
    return gitLabWebHookEventValidator.validate(gitLabWebHookEvent);
  }

  public void sendNotifications(GitLabWebHookEvent gitLabWebHookEvent) {
    for (SearchEngineClient client : searchEngineClients) {
      client.notifySitemap("https://www.jvt.me/sitemap.xml");
    }
  }

  @Async
  @Override
  public void sendWebmentions(long thresholdCount, TemporalUnit thresholdUnit) {
    Future<Urlset> sitemapAsync =
        sitemapParserClient.parseSitemap("https://www.jvt.me/sitemap.xml");
    Urlset sitemap = null;
    try {
      sitemap = sitemapAsync.get();
    } catch (InterruptedException | ExecutionException e) {
      throw new RuntimeException("Internal exception was thrown", e);
    }
    Instant threshold = Instant.now().minus(thresholdCount, thresholdUnit);

    for (TUrl url : sitemap.getUrl()) {
      try {
        Instant lastMod = W3C_DATETIME_FORMAT.parse(url.getLastmod()).toInstant();
        if (threshold.isBefore(lastMod)) {
          webmentionOrchestatorService.sendWebmentionsForUrl(url.getLoc());
        }
      } catch (ParseException e) {
        throw new RuntimeException(e);
      }
    }
  }
}
