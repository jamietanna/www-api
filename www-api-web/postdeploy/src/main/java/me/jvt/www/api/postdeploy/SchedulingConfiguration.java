package me.jvt.www.api.postdeploy;

import java.time.temporal.ChronoUnit;
import me.jvt.www.api.postdeploy.service.PostDeployService;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class SchedulingConfiguration {

  private final PostDeployService postDeployService;

  public SchedulingConfiguration(PostDeployService postDeployService) {
    this.postDeployService = postDeployService;
  }

  @Scheduled(cron = "0 0 0/6 * * *")
  void triggerEvery6Hours() {
    postDeployService.sendWebmentions(1L, ChronoUnit.DAYS);
  }
}
