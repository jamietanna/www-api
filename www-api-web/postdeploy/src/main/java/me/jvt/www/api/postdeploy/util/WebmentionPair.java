package me.jvt.www.api.postdeploy.util;

import java.util.AbstractMap;
import java.util.Map.Entry;

public class WebmentionPair extends AbstractMap.SimpleImmutableEntry<String, String> {

  public WebmentionPair(String s, String s2) {
    super(s, s2);
  }

  public WebmentionPair(Entry<? extends String, ? extends String> entry) {
    super(entry);
  }

  public String getSource() {
    return this.getKey();
  }

  public String getTarget() {
    return this.getValue();
  }
}
