package me.jvt.www.api.postdeploy.service;

import java.time.Instant;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;
import me.jvt.www.api.postdeploy.client.WebmentionClient;
import me.jvt.www.api.postdeploy.model.WebmentionResponse;
import me.jvt.www.api.postdeploy.persistence.WebmentionRepository;
import me.jvt.www.api.postdeploy.persistence.dao.Webmention;
import me.jvt.www.api.postdeploy.persistence.dao.WebmentionIdentity;
import me.jvt.www.api.postdeploy.validators.WebmentionValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;

public class CarefulWebmentionService implements WebmentionService {

  private static final Logger LOGGER = LoggerFactory.getLogger(CarefulWebmentionService.class);

  private final LinkedHashSet<WebmentionValidator> webmentionValidatorSet;
  private final ReentrantLock mutex = new ReentrantLock();
  private final WebmentionClient webmentionClient;
  private final WebmentionRepository webmentionRepository;

  private CarefulWebmentionService(
      LinkedHashSet<WebmentionValidator> webmentionValidatorSet,
      WebmentionClient webmentionClient,
      WebmentionRepository webmentionRepository) {
    this.webmentionValidatorSet = webmentionValidatorSet;
    this.webmentionClient = webmentionClient;
    this.webmentionRepository = webmentionRepository;
  }

  @Async("webmentionClientThreadPoolExecutor")
  @Override
  public Future<Optional<String>> sendWebmention(String source, String target) {
    boolean shouldSend =
        webmentionValidatorSet.stream().allMatch(v -> v.shouldSend(source, target));

    if (!shouldSend) {
      return new AsyncResult<>(Optional.empty());
    }

    LOGGER.info("Sending webmention from {} to {}", source, target);
    Future<WebmentionResponse> webmentionResponseAsync =
        webmentionClient.sendWebmention(source, target);
    WebmentionResponse webmentionResponse = null;
    try {
      webmentionResponse = webmentionResponseAsync.get();
    } catch (InterruptedException | ExecutionException e) {
      throw new RuntimeException(e);
    }
    WebmentionIdentity webmentionIdentity = new WebmentionIdentity(source, target);
    Webmention webmention =
        new Webmention(webmentionIdentity, Instant.now(), webmentionResponse.getWebmentionStatus());

    try {
      mutex.lock();
      saveWebmention(webmention);
    } finally {
      mutex.unlock();
    }
    return new AsyncResult<>(webmentionResponse.getSyndicatedLink());
  }

  private synchronized void saveWebmention(Webmention webmention) {
    webmentionRepository.save(webmention);
  }

  public static final class CarefulWebmentionServiceBuilder {

    private LinkedHashSet<WebmentionValidator> webmentionValidatorSet;
    private WebmentionClient webmentionClient;
    private WebmentionRepository webmentionRepository;

    private CarefulWebmentionServiceBuilder() {
      this.webmentionValidatorSet = new LinkedHashSet<>();
    }

    public static CarefulWebmentionServiceBuilder aCarefulWebmentionService() {
      return new CarefulWebmentionServiceBuilder();
    }

    public CarefulWebmentionServiceBuilder withWebmentionValidator(
        WebmentionValidator webmentionValidator) {
      this.webmentionValidatorSet.add(webmentionValidator);
      return this;
    }

    public CarefulWebmentionServiceBuilder withWebmentionClient(WebmentionClient webmentionClient) {
      this.webmentionClient = webmentionClient;
      return this;
    }

    public CarefulWebmentionServiceBuilder withWebmentionRepository(
        WebmentionRepository webmentionRepository) {
      this.webmentionRepository = webmentionRepository;
      return this;
    }

    public CarefulWebmentionService build() {
      return new CarefulWebmentionService(
          this.webmentionValidatorSet, this.webmentionClient, this.webmentionRepository);
    }
  }
}
