package me.jvt.www.api.postdeploy.presentation.rest;

import java.time.temporal.ChronoUnit;
import me.jvt.www.api.postdeploy.model.GitLabWebHookEvent;
import me.jvt.www.api.postdeploy.service.PostDeployService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostDeployController {

  private static final long WEBMENTION_THRESHOLD = 60L;

  private final PostDeployService postDeployService;

  public PostDeployController(PostDeployService postDeployService) {
    this.postDeployService = postDeployService;
  }

  @PostMapping(path = "/post-deploy")
  public ResponseEntity<String> post(@RequestBody GitLabWebHookEvent gitLabWebHookEvent) {
    if (!postDeployService.validate(gitLabWebHookEvent)) {
      return new ResponseEntity<String>("Must be `main` pipeline", HttpStatus.BAD_REQUEST);
    }
    postDeployService.sendNotifications(gitLabWebHookEvent);
    postDeployService.sendWebmentions(WEBMENTION_THRESHOLD, ChronoUnit.MINUTES);

    return new ResponseEntity<String>("", HttpStatus.NO_CONTENT);
  }
}
