package me.jvt.www.api.postdeploy.client;

import static io.restassured.RestAssured.given;

public class GoogleSearchEngineClient implements SearchEngineClient {

  public void notifySitemap(String sitemapUrl) {
    given()
        .when()
        .post(String.format("https://google.com/webmasters/tools/ping?sitemap=%s", sitemapUrl));
  }
}
