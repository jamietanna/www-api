package me.jvt.www.api.postdeploy.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GitLabWebHookEvent {

  @JsonProperty("object_kind")
  public String objectKind;

  @JsonProperty("object_attributes")
  public ObjectAttributes objectAttributes;

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class ObjectAttributes {

    @JsonProperty("ref")
    public String ref;

    @JsonProperty("status")
    public String status;

    @JsonProperty("before_sha")
    public String beforeSha;

    public String sha;
  }
}
