package me.jvt.www.api.postdeploy.model;

import java.util.HashMap;
import java.util.Map;

public enum TelegraphStatus {
  ACCEPTED("accepted", true),
  SUCCESS("success", true),
  NOT_SUPPORTED("not_supported", false),
  NO_LINK_FOUND("no_link_found", false),
  QUEUED("queued", false),
  UNKNOWN("", false);

  private static final Map<String, TelegraphStatus> lookup = new HashMap<>();

  static {
    for (TelegraphStatus status : TelegraphStatus.values()) {
      lookup.put(status.telegraphResponseText, status);
    }
  }

  private final String telegraphResponseText;
  private final boolean isSuccessful;

  TelegraphStatus(String telegraphResponseText, boolean isSuccessful) {
    this.telegraphResponseText = telegraphResponseText;
    this.isSuccessful = isSuccessful;
  }

  public static TelegraphStatus fromString(String telegraphResponseText) {
    String key = telegraphResponseText.toLowerCase();
    if (!lookup.containsKey(key)) {
      return UNKNOWN;
    }

    return lookup.get(key);
  }

  public boolean isSuccessful() {
    return isSuccessful;
  }
}
