package me.jvt.www.googlefit.scheduled;

import java.time.Instant;
import java.util.Optional;
import me.jvt.www.googlefit.service.StepCountService;
import me.jvt.www.googlefit.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

  private static final Logger LOGGER = LoggerFactory.getLogger(ScheduledTasks.class);
  private final StepCountService stepCountService;
  private final DateUtil dateUtil;

  public ScheduledTasks(StepCountService stepCountService, DateUtil dateUtil) {
    this.stepCountService = stepCountService;
    this.dateUtil = dateUtil;
  }

  @Scheduled(cron = "0 0 8 * * *")
  public void publishYesterdayStepCount() {
    LOGGER.info("Task publishYesterdayStepCount started");

    Instant yesterday = dateUtil.yesterday();
    Optional<String> optionalPublishedMf2 = stepCountService.sendStepsForDateToMicropub(yesterday);

    if (optionalPublishedMf2.isPresent()) {
      LOGGER.info(
          String.format(
              "publishYesterdayStepCount published stats at URL %s", optionalPublishedMf2.get()));
    } else {
      LOGGER.info(
          String.format(
              "publishYesterdayStepCount could not retrieve/publish stats for date %s", yesterday));
    }

    LOGGER.info("Task publishYesterdayStepCount finished");
  }
}
