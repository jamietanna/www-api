package me.jvt.www.googlefit.util;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class DateUtil {

  public Instant yesterday() {
    return Instant.now().minus(1L, ChronoUnit.DAYS);
  }
}
