package me.jvt.www.googlefit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(
    scanBasePackages = {"me.jvt.www.googlefit", "me.jvt.www.indieauthcontroller"})
public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class);
  }
}
