package me.jvt.www.googlefit;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.util.Utils;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.services.fitness.Fitness;
import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.util.Config;
import java.io.IOException;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.googlefit.client.HmeasureMicropubClient;
import me.jvt.www.googlefit.client.StepCountClient;
import me.jvt.www.googlefit.util.DateUtil;
import me.jvt.www.indieauth.security.IndieAuthBearerTokenResolver;
import me.jvt.www.indieauthclient.*;
import me.jvt.www.indieauthcontroller.store.RefreshingTokenStore;
import me.jvt.www.indieauthcontroller.store.TokenStore;
import me.jvt.www.indieauthcontroller.store.kubernetes.KubernetesSecretTokenStore;
import me.jvt.www.indieauthcontroller.store.kubernetes.PatchBodyFactory;
import me.jvt.www.logging.LoggingConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.*;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.oauth2.server.resource.introspection.NimbusOpaqueTokenIntrospector;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;

@Configuration
@EnableScheduling
@Import(LoggingConfiguration.class)
@ComponentScan("me.jvt.www.indieauth.security")
public class SpringConfiguration {

  @Bean
  public HttpTransport httpTransport() {
    return Utils.getDefaultTransport();
  }

  @Bean
  public JsonFactory jsonFactory() {
    return Utils.getDefaultJsonFactory();
  }

  @Bean
  public GoogleCredential googleCredential(
      @Value("${fit.client-id}") String clientId,
      @Value("${fit.client-secret}") String clientSecret,
      @Value("${fit.refresh-token}") String refreshToken,
      HttpTransport httpTransport,
      JsonFactory jsonFactory) {
    GoogleCredential googleCredential =
        new GoogleCredential.Builder()
            .setTransport(httpTransport)
            .setJsonFactory(jsonFactory)
            .setClientSecrets(clientId, clientSecret)
            .build();
    googleCredential.setRefreshToken(refreshToken);
    return googleCredential;
  }

  @Bean
  public Fitness fitness(
      HttpTransport httpTransport, JsonFactory jsonFactory, GoogleCredential googleCredential) {
    return new Fitness.Builder(httpTransport, jsonFactory, googleCredential)
        .setApplicationName("www-api")
        .build();
  }

  @Bean
  public StepCountClient stepCountClient(Fitness fitness) {
    return new StepCountClient(fitness);
  }

  @Bean
  public HmeasureMicropubClient hmeasureMicropubClient(
      RequestSpecificationFactory requestSpecificationFactory,
      TokenStore tokenStore,
      @Value("${fit.micropub.endpoint}") String micropubEndpoint) {
    return new HmeasureMicropubClient(requestSpecificationFactory, tokenStore, micropubEndpoint);
  }

  @Bean
  public RequestSpecificationFactory requestSpecificationFactory() {
    return new RequestSpecificationFactory();
  }

  @Bean
  public DateUtil dateUtil() {
    return new DateUtil();
  }

  @Bean
  public IndieAuthBearerTokenResolver bearerTokenResolver() {
    return new IndieAuthBearerTokenResolver();
  }

  @Bean
  public OpaqueTokenIntrospector indieAuthOpaqueTokenIntrospector(
      @Value("${indieauth.endpoint.token_introspection}")
          String indieAuthTokenIntrospectionEndpoint,
      RestTemplateBuilder restTemplateBuilder) {
    return new NimbusOpaqueTokenIntrospector(
        indieAuthTokenIntrospectionEndpoint, restTemplateBuilder.build());
  }

  @Bean
  @Primary
  public IndieAuthService indieAuthService(IndieAuthClient delegate) {
    return new InMemoryIndieAuthService(delegate);
  }

  @Bean
  public IndieAuthClient indieAuthClientDelegate(
      @Value("${indieauth.endpoint.authorization}") String authorizationEndpoint,
      @Value("${indieauth.endpoint.token}") String tokenEndpoint,
      @Value("${fit.indieauth.client-id}") String clientId,
      @Value("${fit.indieauth.redirect-uri}") String redirectUri,
      RequestSpecificationFactory requestSpecificationFactory) {
    return new IndieAuthClient(
        new StaticIndieAuthConfiguration(authorizationEndpoint, tokenEndpoint),
        clientId,
        redirectUri,
        requestSpecificationFactory);
  }

  @Bean
  public ApiClient apiClient() throws IOException {
    return Config.fromCluster();
  }

  @Bean
  public TokenStore tokenStore(
      IndieAuthService service, PatchBodyFactory factory, ApiClient apiClient) {
    TokenStore delegate =
        new KubernetesSecretTokenStore(
            "google-fit-secrets", "www-api", "google-fit.tokens.txt", factory, apiClient);
    return new RefreshingTokenStore(service, delegate);
  }
}
