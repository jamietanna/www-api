package me.jvt.www.googlefit.client;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.util.Optional;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.googlefit.model.Hmeasure;
import me.jvt.www.indieauthcontroller.store.TokenStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HmeasureMicropubClient {

  private static final Logger LOGGER = LoggerFactory.getLogger(HmeasureMicropubClient.class);

  private final RequestSpecificationFactory requestSpecificationFactory;
  private final TokenStore tokenStore;
  private final String micropubEndpoint;

  public HmeasureMicropubClient(
      RequestSpecificationFactory requestSpecificationFactory,
      TokenStore tokenStore,
      String micropubEndpoint) {
    this.requestSpecificationFactory = requestSpecificationFactory;
    this.tokenStore = tokenStore;
    this.micropubEndpoint = micropubEndpoint;
  }

  public Optional<String> publishHmeasure(Hmeasure hmeasure) {
    Response response =
        requestSpecificationFactory
            .newRequestSpecification()
            .contentType(ContentType.URLENC)
            .param("h", hmeasure.type.get(0))
            .param("num", hmeasure.properties.num.get(0))
            .param("unit", hmeasure.properties.unit.get(0))
            .param("start", hmeasure.properties.start.get(0))
            .param("end", hmeasure.properties.end.get(0))
            .auth()
            .preemptive()
            .oauth2(this.tokenStore.get())
            .post(this.micropubEndpoint);
    if (201 == response.getStatusCode() || 202 == response.getStatusCode()) {
      return Optional.of(response.getHeader("Location"));
    } else {
      LOGGER.error(
          String.format(
              "Received a %d response code when publishing to Micropub", response.getStatusCode()));
      return Optional.empty();
    }
  }
}
