package me.jvt.www.googlefit.presentation.rest;

import java.time.Instant;
import java.util.Optional;
import me.jvt.www.googlefit.service.StepCountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StepCountController {

  private static final Logger LOGGER = LoggerFactory.getLogger(StepCountController.class);

  private final StepCountService stepCountService;

  public StepCountController(StepCountService stepCountService) {
    this.stepCountService = stepCountService;
  }

  @PostMapping(path = "/fit/step-count", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  @PreAuthorize(
      "@profileUrlValidator.isValidProfileUrl(authentication.name) && ("
          + " hasAuthority('SCOPE_create'))")
  public ResponseEntity<Void> stepCount(@RequestParam("date") String date) {
    Instant i;
    try {
      i = Instant.parse(date);
    } catch (Exception e) {
      LOGGER.error(String.format("Invalid date submitted (%s) which threw exception: ", date), e);
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    Optional<String> optionalPublishedMf2Url = stepCountService.sendStepsForDateToMicropub(i);
    if (!optionalPublishedMf2Url.isPresent()) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    } else {
      return ResponseEntity.status(HttpStatus.ACCEPTED)
          .header("Location", optionalPublishedMf2Url.get())
          .build();
    }
  }
}
