package me.jvt.www.googlefit;

import me.jvt.www.indieauth.security.IndieAuthBearerTokenResolver;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  private final OpaqueTokenIntrospector tokenIntrospector;
  private final IndieAuthBearerTokenResolver bearerTokenResolver;

  public SecurityConfiguration(
      OpaqueTokenIntrospector tokenIntrospector, IndieAuthBearerTokenResolver bearerTokenResolver) {
    this.tokenIntrospector = tokenIntrospector;
    this.bearerTokenResolver = bearerTokenResolver;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.httpBasic()
        .disable()
        .csrf()
        .disable()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .authorizeRequests()
        .antMatchers("/fit/indieauth/**")
        .permitAll()
        .antMatchers("/actuator/health")
        .permitAll()
        .antMatchers("/actuator/health/*")
        .permitAll()
        .anyRequest()
        .authenticated()
        .and()
        .oauth2ResourceServer()
        .bearerTokenResolver(bearerTokenResolver)
        .opaqueToken()
        .introspector(tokenIntrospector);
  }
}
