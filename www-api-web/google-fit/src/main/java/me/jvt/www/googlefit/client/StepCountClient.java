package me.jvt.www.googlefit.client;

import com.google.api.services.fitness.Fitness;
import com.google.api.services.fitness.model.AggregateBy;
import com.google.api.services.fitness.model.AggregateRequest;
import com.google.api.services.fitness.model.AggregateResponse;
import com.google.api.services.fitness.model.BucketByTime;
import com.google.api.services.fitness.model.DataPoint;
import java.io.IOException;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Optional;
import me.jvt.www.googlefit.model.Hmeasure;
import me.jvt.www.googlefit.model.Hmeasure.Properties;

public class StepCountClient {

  private final Fitness fitness;

  public StepCountClient(Fitness fitness) {
    this.fitness = fitness;
  }

  public Optional<Hmeasure> retrieveStepsForDate(Instant dateOfSteps) throws IOException {
    AggregateRequest aggregateRequest = new AggregateRequest();
    aggregateRequest.setStartTimeMillis(dateOfSteps.getEpochSecond() * 1000);
    Instant dateOfStepsEnd = dateOfSteps.plus(1, ChronoUnit.DAYS);
    aggregateRequest.setEndTimeMillis(dateOfStepsEnd.getEpochSecond() * 1000);

    BucketByTime bucketByTime = new BucketByTime();
    bucketByTime.setDurationMillis(86400000L);
    aggregateRequest.setBucketByTime(bucketByTime);

    AggregateBy aggregateBy = new AggregateBy();
    aggregateBy.setDataTypeName("com.google.step_count.delta");
    aggregateRequest.setAggregateBy(Collections.singletonList(aggregateBy));

    AggregateResponse aggregateData =
        fitness.users().dataset().aggregate("me", aggregateRequest).execute();
    DataPoint dataPoints = aggregateData.getBucket().get(0).getDataset().get(0).getPoint().get(0);
    if (dataPoints.getValue().isEmpty()) {
      return Optional.empty();
    }
    int stepCount = dataPoints.getValue().get(0).getIntVal();

    Hmeasure hmeasure = new Hmeasure();
    hmeasure.type = Collections.singletonList("measure");
    hmeasure.properties = new Properties();
    hmeasure.properties.num = Collections.singletonList(stepCount);
    hmeasure.properties.unit = Collections.singletonList("steps");
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_INSTANT;

    hmeasure.properties.start = Collections.singletonList(dateTimeFormatter.format(dateOfSteps));
    hmeasure.properties.end = Collections.singletonList(dateTimeFormatter.format(dateOfStepsEnd));

    return Optional.of(hmeasure);
  }
}
