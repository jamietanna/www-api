package me.jvt.www.googlefit.model;

import java.util.List;

public class Hmeasure {

  public List<String> type;
  public Properties properties;

  public static class Properties {

    public List<Integer> num;
    public List<String> unit;
    public List<String> start;
    public List<String> end;
  }
}
