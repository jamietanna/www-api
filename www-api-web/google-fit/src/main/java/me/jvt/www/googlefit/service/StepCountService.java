package me.jvt.www.googlefit.service;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import me.jvt.www.googlefit.client.HmeasureMicropubClient;
import me.jvt.www.googlefit.client.StepCountClient;
import me.jvt.www.googlefit.model.Hmeasure;
import org.springframework.stereotype.Service;

@Service
public class StepCountService {

  private final StepCountClient stepCountClient;
  private final HmeasureMicropubClient hmeasureMicropubClient;

  public StepCountService(
      StepCountClient stepCountClient, HmeasureMicropubClient hmeasureMicropubClient) {
    this.stepCountClient = stepCountClient;
    this.hmeasureMicropubClient = hmeasureMicropubClient;
  }

  public Optional<String> sendStepsForDateToMicropub(Instant datetimeOfSteps) {
    Instant dateOfSteps = Instant.from(datetimeOfSteps).truncatedTo(ChronoUnit.DAYS);
    Optional<Hmeasure> optionalHmeasure;
    try {
      optionalHmeasure = stepCountClient.retrieveStepsForDate(dateOfSteps);
    } catch (IOException e) {
      return Optional.empty();
    }
    if (!optionalHmeasure.isPresent()) {
      return Optional.empty();
    }

    return hmeasureMicropubClient.publishHmeasure(optionalHmeasure.get());
  }
}
