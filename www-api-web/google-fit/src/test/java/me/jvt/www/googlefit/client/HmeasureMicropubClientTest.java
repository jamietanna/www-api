package me.jvt.www.googlefit.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.AuthenticationSpecification;
import io.restassured.specification.PreemptiveAuthSpec;
import io.restassured.specification.RequestSpecification;
import java.util.Collections;
import java.util.Optional;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.googlefit.model.Hmeasure;
import me.jvt.www.googlefit.model.Hmeasure.Properties;
import me.jvt.www.indieauthcontroller.store.TokenStore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HmeasureMicropubClientTest {

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification mockRequestSpecification;

  @Mock private Response mockResponse;
  @Mock private TokenStore store;
  private PreemptiveAuthSpec mockPreemptiveAuthSpec;

  private Hmeasure hmeasure;
  private HmeasureMicropubClient sut;

  @BeforeEach
  void setup() {
    RequestSpecificationFactory mockFactory = mock(RequestSpecificationFactory.class);
    when(mockFactory.newRequestSpecification()).thenReturn(mockRequestSpecification);

    AuthenticationSpecification mockAuthenticationSpecification =
        mock(AuthenticationSpecification.class);
    when(mockRequestSpecification.auth()).thenReturn(mockAuthenticationSpecification);
    mockPreemptiveAuthSpec = mock(PreemptiveAuthSpec.class);
    when(mockAuthenticationSpecification.preemptive()).thenReturn(mockPreemptiveAuthSpec);
    when(mockPreemptiveAuthSpec.oauth2(anyString())).thenReturn(mockRequestSpecification);
    when(mockRequestSpecification.post(anyString())).thenReturn(mockResponse);
    Mockito.lenient().when(mockResponse.getHeader(anyString())).thenReturn("http://location");

    hmeasure = new Hmeasure();
    hmeasure.type = Collections.singletonList("measure");
    hmeasure.properties = new Properties();
    hmeasure.properties.start = Collections.singletonList("this-is-the-start");
    hmeasure.properties.end = Collections.singletonList("end-date");
    hmeasure.properties.num = Collections.singletonList(2345);
    hmeasure.properties.unit = Collections.singletonList("steps");
    when(store.get()).thenReturn("fake.token");
    sut = new HmeasureMicropubClient(mockFactory, store, "https://wibble.jvt.micropub/endpoint");
  }

  @Test
  void itIsAFormPost() {
    // given

    // when
    sut.publishHmeasure(hmeasure);

    // then
    verify(mockRequestSpecification).contentType(ContentType.URLENC);
  }

  @Test
  void itSendsH() {
    // given

    // when
    sut.publishHmeasure(hmeasure);

    // then
    verify(mockRequestSpecification).param("h", "measure");
  }

  @Test
  void itSendsStart() {
    // given

    // when
    sut.publishHmeasure(hmeasure);

    // then
    verify(mockRequestSpecification).param("start", "this-is-the-start");
  }

  @Test
  void itSendsEnd() {
    // given

    // when
    sut.publishHmeasure(hmeasure);

    // then
    verify(mockRequestSpecification).param("end", "end-date");
  }

  @Test
  void itSendsNum() {
    // given

    // when
    sut.publishHmeasure(hmeasure);

    // then
    verify(mockRequestSpecification).param("num", 2345);
  }

  @Test
  void itSendsUnit() {
    // given

    // when
    sut.publishHmeasure(hmeasure);

    // then
    verify(mockRequestSpecification).param("unit", "steps");
  }

  @Test
  void itSendsAccessTokenFromStore() {
    // given

    // when
    sut.publishHmeasure(hmeasure);

    // then
    verify(mockPreemptiveAuthSpec).oauth2("fake.token");
  }

  @Test
  void itPostsToConfiguredUrl() {
    // given

    // when
    sut.publishHmeasure(hmeasure);

    // then
    verify(mockRequestSpecification).post("https://wibble.jvt.micropub/endpoint");
  }

  @Test
  void itReturnsCreatedUrlInOptionalWhen201() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(201);
    when(mockResponse.getHeader("Location")).thenReturn("https://localhost.foo");

    // when
    Optional<String> optUrl = sut.publishHmeasure(hmeasure);

    // then
    assertThat(optUrl).isPresent();
    assertThat(optUrl.get()).isEqualTo("https://localhost.foo");
  }

  @Test
  void itReturnsCreatedUrlInOptionalWhen202() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(202);
    when(mockResponse.getHeader("Location")).thenReturn("https://localhost.foo");

    // when
    Optional<String> optUrl = sut.publishHmeasure(hmeasure);

    // then
    assertThat(optUrl).isPresent();
    assertThat(optUrl.get()).isEqualTo("https://localhost.foo");
  }

  @Test
  void itReturnsEmptyOptionalIfNotAnyOtherResponse() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(500);

    // when
    Optional<String> optUrl = sut.publishHmeasure(hmeasure);

    // then
    assertThat(optUrl).isNotPresent();
  }

  // TODO: catch log messages
}
