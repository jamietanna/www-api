package me.jvt.www.googlefit.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.Instant;
import java.util.Optional;
import me.jvt.www.googlefit.client.HmeasureMicropubClient;
import me.jvt.www.googlefit.client.StepCountClient;
import me.jvt.www.googlefit.model.Hmeasure;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class StepCountServiceTest {

  @Mock private Hmeasure mockHmeasure;
  @Mock private HmeasureMicropubClient mockHmeasureMicropubClient;
  @Mock private StepCountClient mockStepCountClient;

  private Instant date;
  private StepCountService sut;

  @BeforeEach
  void setup() throws IOException {
    date = Instant.parse("2019-09-01T00:00:00Z");
    when(mockStepCountClient.retrieveStepsForDate(any())).thenReturn(Optional.of(mockHmeasure));
    Mockito.lenient()
        .when(mockHmeasureMicropubClient.publishHmeasure(any()))
        .thenReturn(Optional.of("https://published-url.example/foo"));

    sut = new StepCountService(mockStepCountClient, mockHmeasureMicropubClient);
  }

  @Test
  void sendStepsViaMicropubPassesDateToStepCountClient() throws IOException {
    // given

    // when
    sut.sendStepsForDateToMicropub(date);

    // then
    verify(mockStepCountClient).retrieveStepsForDate(date);
  }

  @Test
  void sendStepsViaMicropubSendsHmeasureToMicropubIfExists() {
    // given

    // when
    sut.sendStepsForDateToMicropub(date);

    // then
    verify(mockHmeasureMicropubClient).publishHmeasure(mockHmeasure);
  }

  @Test
  void sendStepsViaMicropubDoesNotSendHmeasureToMicropubIfStepsNotReturned() throws IOException {
    // given
    when(mockStepCountClient.retrieveStepsForDate(any())).thenReturn(Optional.empty());

    // when
    Optional<String> optionalPublishedMf2 = sut.sendStepsForDateToMicropub(date);

    // then
    assertThat(optionalPublishedMf2).isNotPresent();
    verify(mockHmeasureMicropubClient, times(0)).publishHmeasure(mockHmeasure);
  }

  @Test
  void sendStepsViaMicropubReturnsOptionalFromMicropubRequest() {
    // given

    // when
    Optional<String> optionalPublishedMf2 = sut.sendStepsForDateToMicropub(date);

    // then
    assertThat(optionalPublishedMf2).isPresent();
    assertThat(optionalPublishedMf2.get()).isEqualTo("https://published-url.example/foo");
  }

  @Test
  void sendStepsViaMicropubReturnsEmptyOptionalWhenIOException() throws IOException {
    // given
    when(mockStepCountClient.retrieveStepsForDate(any()))
        .thenThrow(new IOException("Something went wrong"));

    // when
    Optional<String> optionalPublishedMf2 = sut.sendStepsForDateToMicropub(date);

    // then
    assertThat(optionalPublishedMf2).isNotPresent();
  }
}
