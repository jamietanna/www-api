package me.jvt.www.googlefit.presentation.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import me.jvt.www.googlefit.service.StepCountService;
import me.jvt.www.indieauth.security.IndieAuthBearerTokenResolver;
import me.jvt.www.indieauth.security.ProfileUrlValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@WebMvcTest(StepCountController.class)
class StepCountControllerTest {

  @Autowired private MockMvc mockMvc;

  @MockBean private IndieAuthBearerTokenResolver mockBearerTokenResolver;

  @MockBean private OpaqueTokenIntrospector mockOpaqueTokenIntrospector;

  @MockBean private StepCountService mockStepCountService;

  @MockBean(name = "profileUrlValidator")
  private ProfileUrlValidator profileUrlValidator;

  private OAuth2AuthenticatedPrincipal fakeAuthenticatedPrincipal;

  void happyPath() {
    when(mockBearerTokenResolver.resolve(any())).thenReturn("some token");
    Collection<GrantedAuthority> grantedAuthorities =
        AuthorityUtils.commaSeparatedStringToAuthorityList("SCOPE_create");
    fakeAuthenticatedPrincipal =
        new OAuth2IntrospectionAuthenticatedPrincipal(
            Map.of("client_id", "https://wibble.foo", "sub", "https://www.jvt.me/"),
            grantedAuthorities);
    when(mockOpaqueTokenIntrospector.introspect(anyString()))
        .thenReturn(fakeAuthenticatedPrincipal);
    when(profileUrlValidator.isValidProfileUrl(any())).thenReturn(true);

    when(mockStepCountService.sendStepsForDateToMicropub(any()))
        .thenReturn(Optional.of("https://foo.bar/com"));
  }

  @Test
  void itParsesDateToInstantAndSendsToService() throws Exception {
    // given
    happyPath();

    // when
    this.mockMvc.perform(
        post("/fit/step-count")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .param("date", "2019-09-12T22:34:56Z"));

    // then
    ArgumentCaptor<Instant> instantArgumentCaptor = ArgumentCaptor.forClass(Instant.class);
    verify(mockStepCountService).sendStepsForDateToMicropub(instantArgumentCaptor.capture());
    Instant actual = instantArgumentCaptor.getValue();
    assertThat(actual).isNotNull();
    LocalDateTime actualDateTime = LocalDateTime.ofInstant(actual, ZoneOffset.UTC);
    assertThat(actualDateTime.getYear()).isEqualTo(2019);
    assertThat(actualDateTime.getMonth()).isEqualTo(Month.SEPTEMBER);
    assertThat(actualDateTime.getDayOfMonth()).isEqualTo(12);
    assertThat(actualDateTime.getHour()).isEqualTo(22);
    assertThat(actualDateTime.getMinute()).isEqualTo(34);
    assertThat(actualDateTime.getSecond()).isEqualTo(56);
  }

  @Test
  void itReturnsRedirectToPublishedUrlIfOneReturned() throws Exception {
    // given
    happyPath();
    when(mockStepCountService.sendStepsForDateToMicropub(any()))
        .thenReturn(Optional.of("https://foo.bar/com"));

    // when
    this.mockMvc
        .perform(
            post("/fit/step-count")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("date", "2019-09-12T12:34:56Z"))
        // then
        .andExpect(MockMvcResultMatchers.status().isAccepted())
        .andExpect(MockMvcResultMatchers.redirectedUrl("https://foo.bar/com"));
  }

  @Test
  void itReturnsInternalServerErrorWhenNoUrlReturned() throws Exception {
    // given
    happyPath();
    when(mockStepCountService.sendStepsForDateToMicropub(any())).thenReturn(Optional.empty());

    // when
    this.mockMvc
        .perform(
            post("/fit/step-count")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("date", "2019-09-12T12:34:56Z"))
        // then
        .andExpect(MockMvcResultMatchers.status().isInternalServerError());
  }

  @Test
  void itReturnsBadRequestWhenDateIsOnlyDate() throws Exception {
    // given
    happyPath();

    // when
    this.mockMvc
        .perform(
            post("/fit/step-count")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("date", "2019-09-12"))
        // then
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @Test
  void itReturnsBadRequestWhenDateIsNotADate() throws Exception {
    // given
    happyPath();

    // when
    this.mockMvc
        .perform(
            post("/fit/step-count")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("date", "flooble"))
        // then
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @Test
  void itDeniesRequestsWithoutMyProfileUrl() throws Exception {
    // given
    fakeAuthenticatedPrincipal =
        new OAuth2IntrospectionAuthenticatedPrincipal(
            Map.of("client_id", "https://bar.foo", "sub", "https://jamietanna.co.uk"),
            Collections.emptyList());
    when(mockBearerTokenResolver.resolve(any())).thenReturn("some token");
    when(mockOpaqueTokenIntrospector.introspect(anyString()))
        .thenReturn(fakeAuthenticatedPrincipal);
    when(profileUrlValidator.isValidProfileUrl(any())).thenReturn(false);

    // when
    this.mockMvc
        .perform(
            post("/fit/step-count")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("date", "2019-09-12T12:34:56Z"))
        // then
        .andExpect(MockMvcResultMatchers.status().isForbidden());
  }

  @Test
  void itDeniesRequestsWithoutCreateScope() throws Exception {
    // given
    Collection<GrantedAuthority> grantedAuthorities =
        AuthorityUtils.commaSeparatedStringToAuthorityList("SCOPE_update");
    fakeAuthenticatedPrincipal =
        new OAuth2IntrospectionAuthenticatedPrincipal(
            Map.of("client_id", "https://bar.foo", "sub", "https://www.jvt.me/"),
            grantedAuthorities);
    when(mockBearerTokenResolver.resolve(any())).thenReturn("some token");
    when(mockOpaqueTokenIntrospector.introspect(anyString()))
        .thenReturn(fakeAuthenticatedPrincipal);

    // when
    this.mockMvc
        .perform(
            post("/fit/step-count")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("date", "2019-09-12T12:34:56Z"))
        // then
        .andExpect(MockMvcResultMatchers.status().isForbidden());
  }
}
