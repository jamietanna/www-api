package me.jvt.www.googlefit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import io.kubernetes.client.openapi.ApiClient;
import me.jvt.www.indieauthcontroller.controller.IndieAuthController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = Application.class)
@AutoConfigureMockMvc
class ApplicationIntegrationTest {

  @MockBean private ApiClient apiClient;

  @Autowired private MockMvc mockMvc;

  @Test
  void contextLoads(ApplicationContext context) {
    assertThat(context).isNotNull();
  }

  @Test
  void hasIndieAuthControllerConfigured(ApplicationContext context) {
    assertThat(context.getBean(IndieAuthController.class)).isNotNull();
  }

  @Test
  void indieAuthControllerIsUnauthenticated() throws Exception {
    mockMvc.perform(get("/fit/indieauth/start")).andExpect(status().isFound());
  }
}
