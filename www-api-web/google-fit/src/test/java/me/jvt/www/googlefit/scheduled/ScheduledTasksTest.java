package me.jvt.www.googlefit.scheduled;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.Optional;
import me.jvt.www.googlefit.service.StepCountService;
import me.jvt.www.googlefit.util.DateUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ScheduledTasksTest {

  @Mock private DateUtil mockDateUtil;
  @Mock private StepCountService mockService;

  private Instant fakeInstant;
  private ScheduledTasks sut;

  @BeforeEach
  void setup() {
    fakeInstant = Instant.now();
    sut = new ScheduledTasks(mockService, mockDateUtil);
  }

  @Test
  void publishYesterdayStepCountTakesYesterdaysDateAndCallServiceWhenValueNotReturned() {
    // given
    when(mockDateUtil.yesterday()).thenReturn(fakeInstant);
    when(mockService.sendStepsForDateToMicropub(any())).thenReturn(Optional.empty());

    // when
    sut.publishYesterdayStepCount();

    // then
    verify(mockDateUtil).yesterday();
    verify(mockService).sendStepsForDateToMicropub(fakeInstant);
  }

  @Test
  void publishYesterdayStepCountTakesYesterdaysDateAndCallServiceWhenValueReturned() {
    // given
    when(mockDateUtil.yesterday()).thenReturn(fakeInstant);
    when(mockService.sendStepsForDateToMicropub(any())).thenReturn(Optional.of("https://oo.bah"));

    // when
    sut.publishYesterdayStepCount();

    // then
    verify(mockDateUtil).yesterday();
    verify(mockService).sendStepsForDateToMicropub(fakeInstant);
  }

  // TODO: logging
}
