package me.jvt.www.googlefit.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.api.services.fitness.Fitness;
import com.google.api.services.fitness.Fitness.Users.Dataset.Aggregate;
import com.google.api.services.fitness.model.AggregateBucket;
import com.google.api.services.fitness.model.AggregateRequest;
import com.google.api.services.fitness.model.AggregateResponse;
import com.google.api.services.fitness.model.DataPoint;
import com.google.api.services.fitness.model.Dataset;
import com.google.api.services.fitness.model.Value;
import java.io.IOException;
import java.time.Instant;
import java.util.Collections;
import java.util.Optional;
import me.jvt.www.googlefit.model.Hmeasure;
import me.jvt.www.googlefit.model.Hmeasure.Properties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@ExtendWith(MockitoExtension.class)
class StepCountClientTest {

  @Mock private Fitness.Users.Dataset mockDataset;
  @Mock private Fitness mockFitness;

  private Instant date;
  private DataPoint dataPoint;

  private StepCountClient sut;

  @BeforeEach
  void setup() throws IOException {
    date = Instant.parse("2019-09-01T00:00:00Z");

    Fitness.Users mockUsers = mock(Fitness.Users.class);
    when(mockFitness.users()).thenReturn(mockUsers);
    when(mockUsers.dataset()).thenReturn(mockDataset);
    Aggregate mockAggregate = mock(Aggregate.class);
    when(mockDataset.aggregate(anyString(), any())).thenReturn(mockAggregate);

    AggregateResponse aggregateResponse = new AggregateResponse();
    when(mockAggregate.execute()).thenReturn(aggregateResponse);
    AggregateBucket bucket = new AggregateBucket();
    aggregateResponse.setBucket(Collections.singletonList(bucket));
    Dataset dataset = new Dataset();
    bucket.setDataset(Collections.singletonList(dataset));
    dataPoint = new DataPoint();
    dataset.setPoint(Collections.singletonList(dataPoint));
    Value value = new Value();
    value.setIntVal(1234);
    dataPoint.setValue(Collections.singletonList(value));

    sut = new StepCountClient(mockFitness);
  }

  @Test
  void retrieveStepsForDateReturnsHmeasure() throws IOException {
    // given

    // when
    Optional<Hmeasure> optionalHmeasure = sut.retrieveStepsForDate(date);

    // then
    assertThat(optionalHmeasure).isPresent();
    Hmeasure actual = optionalHmeasure.get();
    assertThat(actual).isNotNull();
    assertThat(actual.type).containsExactly("measure");
    assertThat(actual.properties).isNotNull();
    Properties properties = actual.properties;
    assertThat(properties.num).containsExactly(1234);
    assertThat(properties.unit).containsExactly("steps");
    assertThat(properties.start).containsExactly("2019-09-01T00:00:00Z");
    assertThat(properties.end).containsExactly("2019-09-02T00:00:00Z");
  }

  @Test
  void retrieveStepsForDateSendsCorrectAggregateRequest() throws IOException {
    // given

    // when
    sut.retrieveStepsForDate(date);

    // then
    ArgumentCaptor<AggregateRequest> aggregateRequestArgumentCaptor =
        ArgumentCaptor.forClass(AggregateRequest.class);
    verify(mockDataset).aggregate(eq("me"), aggregateRequestArgumentCaptor.capture());
    AggregateRequest actualRequest = aggregateRequestArgumentCaptor.getValue();
    assertThat(actualRequest).isNotNull();
    assertThat(actualRequest.getStartTimeMillis()).isEqualTo(1567296000000L);
    assertThat(actualRequest.getEndTimeMillis()).isEqualTo(1567382400000L);
    assertThat(actualRequest.getBucketByTime().getDurationMillis()).isEqualTo(86400000L);

    assertThat(actualRequest.getAggregateBy()).hasSize(1);
    assertThat(actualRequest.getAggregateBy().get(0).getDataTypeName())
        .isEqualTo("com.google.step_count.delta");
  }

  @Test
  void returnsEmptyOptionalIfNoDataPointsReturned() throws IOException {
    // given
    dataPoint.setValue(Collections.emptyList());

    // when
    Optional<Hmeasure> hmeasure = sut.retrieveStepsForDate(date);

    // then
    assertThat(hmeasure).isNotPresent();
  }
}
