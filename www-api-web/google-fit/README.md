# google-fit

An API to make it easier to own Google Fit data, by publishing the previous day's data to a Micropub server on a schedule.

## Gaining an OAuth2 refresh token from Google

The following redirect will allow you to authenticate with the current OAuth2 client I'm using:

```
https://accounts.google.com/o/oauth2/v2/auth?scope=https://www.googleapis.com/auth/fitness.activity.read&access_type=offline&include_granted_scopes=true&response_type=code&state=state_parameter_passthrough_value&redirect_uri=http://localhost:8080/callback&client_id=767982229388-3ukro9ihnsrgi430j9sf7jf8feg3ucot.apps.googleusercontent.com&prompt=consent
```

You can then proceed to exchange the authorization code for a refresh token, per OAuth2.
