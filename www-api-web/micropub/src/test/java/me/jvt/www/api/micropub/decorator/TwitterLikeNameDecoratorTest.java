package me.jvt.www.api.micropub.decorator;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collections;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TwitterLikeNameDecoratorTest {

  TestMicroformats2Object mf2;
  TwitterLikeNameDecorator sut;

  @BeforeEach
  void setup() {
    mf2 = new TestMicroformats2Object();
    mf2.setKind(Kind.likes);

    sut = new TwitterLikeNameDecorator();
  }

  @Test
  void itAddsTheNamePropertyOnTwitterLikes() {
    mf2.getProperties()
        .setLikeOf(
            SingletonListHelper.singletonList(
                "https://twitter.com/jrhennessy/status/1214646294045585408"));

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getName()).containsExactly("Like of @jrhennessy's tweet");
  }

  @Test
  void itReplacesExistingNameOnTwitterLikes() {
    mf2.getProperties().setName(SingletonListHelper.singletonList("Name here"));
    mf2.getProperties()
        .setLikeOf(
            SingletonListHelper.singletonList(
                "https://twitter.com/jrhennessy/status/1214646294045585408"));

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getName()).doesNotContain("Name here");
  }

  @Test
  void itDoesNotReplaceExistingNameOnNotTwitterLikes() {
    mf2.getProperties().setName(SingletonListHelper.singletonList("Name here"));
    mf2.getProperties()
        .setLikeOf(
            SingletonListHelper.singletonList(
                "https://foo.com/jrhennessy/status/1214646294045585408"));

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getName()).containsExactly("Name here");
  }

  @Test
  void itDoesNotReplaceExistingNameWhenNoLikeOfOnInvalidLike() {
    mf2.getProperties().setName(SingletonListHelper.singletonList("Name here"));
    mf2.getProperties().setLikeOf(Collections.emptyList());

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getName()).containsExactly("Name here");
  }

  @Test
  void itDoesNotReplaceExistingNameOnNotLikes() {
    mf2.setKind(Kind.notes);
    mf2.getProperties().setName(SingletonListHelper.singletonList("Name here"));

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getName()).containsExactly("Name here");
  }
}
