package me.jvt.www.api.micropub.validator.property;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintValidatorContext;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import org.hibernate.validator.internal.constraintvalidators.bv.NotBlankValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ContentPropertyValidatorTest {

  private final TestMicroformats2Object mf2 = new TestMicroformats2Object();

  @Mock private ConstraintValidatorContext context;
  @Mock private NotBlankValidator delegate;

  @InjectMocks private ContentPropertyValidator validator;

  @ParameterizedTest
  @NullAndEmptySource
  void itReturnsFalseWhenNotFound(List<Map<String, String>> content) {
    mf2.getProperties().setContent(content);

    boolean actual = validator.isValid(mf2, context);

    assertThat(actual).isFalse();
  }

  @Test
  void itDelegatesToVerifyValue() {
    mf2.getProperties().setContent(content("value", ""));

    boolean actual = validator.isValid(mf2, context);

    assertThat(actual).isFalse();
    verify(delegate).isValid("value", context);
  }

  @Test
  void itDelegatesToVerifyHtml() {
    mf2.getProperties().setContent(content("", "<html>"));

    boolean actual = validator.isValid(mf2, context);

    assertThat(actual).isFalse();
    verify(delegate).isValid("<html>", context);
  }

  @Test
  void itReturnsTrueWhenValueIsValid() {
    when(delegate.isValid("wibble", context)).thenReturn(true);
    mf2.getProperties().setContent(content("wibble", ""));

    boolean actual = validator.isValid(mf2, context);

    assertThat(actual).isTrue();
  }

  @Test
  void itReturnsTrueWhenHtmlIsValid() {
    when(delegate.isValid("<html>", context)).thenReturn(true);
    mf2.getProperties().setContent(content("<html>", ""));

    boolean actual = validator.isValid(mf2, context);

    assertThat(actual).isTrue();
  }

  private List<Map<String, String>> content(String value, String html) {
    List<Map<String, String>> content = new ArrayList<>();
    Map<String, String> theContent = new HashMap<>();
    theContent.put("value", value);
    theContent.put("html", html);
    content.add(theContent);
    return content;
  }
}
