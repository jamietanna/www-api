package me.jvt.www.api.micropub.decorator;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.EnumSource.Mode;
import org.junit.jupiter.params.provider.MethodSource;

class TwitterUrlToAtUsernameContentDecoratorTest {

  TestMicroformats2Object mf2;
  TwitterUrlToAtUsernameContentDecorator sut;

  static Stream<Arguments> arguments() {
    return Stream.of(
        Arguments.of(
            "Hello https://twitter.com/jamietanna",
            "Hello <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/jamietanna\">@jamietanna</a></span>"),
        Arguments.of(
            "Hello https://twitter.com/Jamietanna",
            "Hello <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/Jamietanna\">@Jamietanna</a></span>"),
        Arguments.of(
            "Hello https://twitter.com/jamie0tanna",
            "Hello <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/jamie0tanna\">@jamie0tanna</a></span>"),
        Arguments.of(
            "Hello https://twitter.com/jamie_tanna",
            "Hello <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/jamie_tanna\">@jamie_tanna</a></span>"),
        Arguments.of(
            "https://twitter.com/jamietanna hey",
            "<span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/jamietanna\">@jamietanna</a></span> hey"),
        Arguments.of(
            ".https://twitter.com/jamietanna",
            ".<span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/jamietanna\">@jamietanna</a></span>"),
        Arguments.of(
            "Something https://twitter.com/jamietanna hello",
            "Something <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/jamietanna\">@jamietanna</a></span> hello"),
        Arguments.of(
            "Have you asked https://twitter.com/jamietanna?",
            "Have you asked <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/jamietanna\">@jamietanna</a></span>?"),
        Arguments.of(
            "Have you asked https://twitter.com/jamietanna!",
            "Have you asked <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/jamietanna\">@jamietanna</a></span>!"),
        Arguments.of(
            "Have you asked https://twitter.com/jamietanna'",
            "Have you asked <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/jamietanna\">@jamietanna</a></span>'"),
        Arguments.of(
            "Have you asked https://twitter.com/jamietanna\"",
            "Have you asked <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/jamietanna\">@jamietanna</a></span>\""),
        Arguments.of(
            "https://twitter.com/jamietanna/status/1", "https://twitter.com/jamietanna/status/1"),
        Arguments.of(
            "Maybe https://twitter.com/jamietanna.",
            "Maybe <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/jamietanna\">@jamietanna</a></span>."));
  }

  @BeforeEach
  void setup() {
    mf2 = new TestMicroformats2Object();

    sut = new TwitterUrlToAtUsernameContentDecorator();
  }

  @Test
  void itHandlesNullContentInRepost() {
    mf2.setKind(Kind.reposts);
    mf2.getProperties().setContent(null);

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getContent()).isNull();
  }

  @Test
  void itHandlesEmptyContentInRepost() {
    mf2.setKind(Kind.reposts);
    mf2.getProperties().setContent(new ArrayList<>());

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getContent()).hasSize(0);
  }

  @MethodSource("arguments")
  @ParameterizedTest
  void itRewritesWhenDifferentCasesForNote(String input, String output) {
    mf2.setKind(Kind.notes);
    mf2.getProperties().setContent(content(input));

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getContent().get(0).get("value")).isEqualTo(output);
  }

  @MethodSource("arguments")
  @ParameterizedTest
  void itRewritesWhenDifferentCasesForPhoto(String input, String output) {
    mf2.setKind(Kind.photos);
    mf2.getProperties().setContent(content(input));

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getContent().get(0).get("value")).isEqualTo(output);
  }

  @MethodSource("arguments")
  @ParameterizedTest
  void itRewritesWhenDifferentCasesForReply(String input, String output) {
    mf2.setKind(Kind.replies);
    mf2.getProperties().setContent(content(input));

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getContent().get(0).get("value")).isEqualTo(output);
  }

  @MethodSource("arguments")
  @ParameterizedTest
  void itRewritesWhenDifferentCasesForRepost(String input, String output) {
    mf2.setKind(Kind.reposts);
    mf2.getProperties().setContent(content(input));

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getContent().get(0).get("value")).isEqualTo(output);
  }

  @EnumSource(
      value = Kind.class,
      mode = Mode.INCLUDE,
      names = {"notes", "photos", "replies", "reposts"})
  @ParameterizedTest
  void itRewritesSupportedKinds(Kind kind) {
    mf2.setKind(kind);
    mf2.getProperties().setContent(content("https://twitter.com/jamietanna"));

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getContent().get(0).get("value"))
        .isEqualTo(
            "<span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/jamietanna\">@jamietanna</a></span>");
  }

  @EnumSource(
      value = Kind.class,
      mode = Mode.EXCLUDE,
      names = {"notes", "photos", "replies", "reposts"})
  @ParameterizedTest
  void itIgnoresOtherKinds(Kind kind) {
    mf2.setKind(kind);
    mf2.getProperties().setContent(content("https://twitter.com/username"));

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getContent().get(0).get("value"))
        .isEqualTo("https://twitter.com/username");
  }

  private List<Map<String, String>> content(String value) {
    Map<String, String> content = new HashMap<>();
    content.put("value", value);

    return SingletonListHelper.singletonList(content);
  }
}
