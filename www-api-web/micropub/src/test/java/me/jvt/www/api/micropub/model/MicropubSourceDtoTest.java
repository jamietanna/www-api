package me.jvt.www.api.micropub.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;

class MicropubSourceDtoTest {

  private final MicropubSourceDto dto = new MicropubSourceDto();
  private final Map<String, String> content = new HashMap<>();
  private Properties actual;

  @Nested
  class GetProperties {

    @Test
    void returnsNullWhenNullProperties() {
      assertThat(dto.getProperties()).isNull();
    }

    @ParameterizedTest
    @NullAndEmptySource
    void whenContentIsObjectWithNullOrEmptyHtml(String html) {
      content.put("value", "foo");
      content.put("html", html);

      execute();

      List<String> content = (List<String>) actual.get("content");
      assertThat(content).containsExactly("foo");
    }

    @Test
    void whenContentIsObjectWithNoHtmlKey() {
      content.put("value", "foo");

      execute();

      List<String> content = (List<String>) actual.get("content");
      assertThat(content).containsExactly("foo");
    }

    @Test
    void whenContentHasHtmlValue() {
      content.put("value", "foo");
      content.put("html", "<html>");

      execute();

      assertThat(actual.getContent().get(0).get("html")).isEqualTo("<html>");
      assertThat(actual.getContent().get(0)).doesNotContainKey("value");
    }

    @Test
    void whenContentIsString() {
      Properties properties = new Properties();
      properties.put("content", SingletonListHelper.singletonList("Single value"));
      dto.setProperties(properties);
      actual = dto.getProperties();

      List<String> content = (List<String>) actual.get("content");
      assertThat(content).containsExactly("Single value");
    }
  }

  private void execute() {
    Properties properties = new Properties();
    properties.setContent(SingletonListHelper.singletonList(content));
    dto.setProperties(properties);
    actual = dto.getProperties();
  }
}
