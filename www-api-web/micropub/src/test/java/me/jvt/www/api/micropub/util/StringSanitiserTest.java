package me.jvt.www.api.micropub.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class StringSanitiserTest {

  private StringSanitiser sut;

  static Stream<Arguments> primeNumbers() {
    return Stream.of(
        Arguments.of("  the string", "the string"),
        Arguments.of("\tthestring", "thestring"),
        Arguments.of("\nwoops", "woops"),
        Arguments.of("test  ", "test"),
        Arguments.of("nope to newlines\n", "nope to newlines"),
        Arguments.of("not in the \n middle", "not in the \n middle"),
        Arguments.of(null, ""));
  }

  @BeforeEach
  void setup() {
    sut = new StringSanitiser();
  }

  @ParameterizedTest
  @MethodSource("primeNumbers")
  void parameterised(String input, String expected) {
    // given

    // when
    String actual = sut.removeWhitespace(input);

    // then
    assertThat(actual).isEqualTo(expected);
  }
}
