package me.jvt.www.api.micropub.sanitiser;

import static org.assertj.core.api.Assertions.assertThat;

import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class TwitterUrlSanitiserTest {

  TestMicroformats2Object mf2;
  TwitterUrlSanitiser sut;

  @BeforeEach
  void setup() {
    mf2 = new TestMicroformats2Object();

    sut = new TwitterUrlSanitiser();
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        "https://twitter.com/JamieTanna/status/1206236519549280257?",
        "https://twitter.com/JamieTanna/status/1206236519549280257?s=",
        "https://twitter.com/JamieTanna/status/1206236519549280257?s=19"
      })
  void itSantisesQueryStringFromTwitterComOnLikes(String url) {
    // given
    mf2.getProperties().setLikeOf(SingletonListHelper.singletonList(url));

    // when
    Microformats2Object actual = sut.sanitise(mf2);

    // then
    assertThat(actual.getProperties().getLikeOf())
        .containsExactly("https://twitter.com/JamieTanna/status/1206236519549280257");
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        "https://twitter.com/JamieTanna/status/1206236519549280257?",
        "https://twitter.com/JamieTanna/status/1206236519549280257?s=",
        "https://twitter.com/JamieTanna/status/1206236519549280257?s=19",
        "https://twitter.com/JamieTanna/status/1206236519549280257?s=19&something=else"
      })
  void itSantisesQueryStringFromTwitterComOnReplies(String url) {
    // given
    mf2.getProperties().setInReplyTo(SingletonListHelper.singletonList(url));

    // when
    Microformats2Object actual = sut.sanitise(mf2);

    // then
    assertThat(actual.getProperties().getInReplyTo())
        .containsExactly("https://twitter.com/JamieTanna/status/1206236519549280257");
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        "https://twitter.com/JamieTanna/status/1206236519549280257?",
        "https://twitter.com/JamieTanna/status/1206236519549280257?s=",
        "https://twitter.com/JamieTanna/status/1206236519549280257?s=19"
      })
  void itSantisesQueryStringFromTwitterComOnReposts(String url) {
    // given
    mf2.getProperties().setRepostOf(SingletonListHelper.singletonList(url));

    // when
    Microformats2Object actual = sut.sanitise(mf2);

    // then
    assertThat(actual.getProperties().getRepostOf())
        .containsExactly("https://twitter.com/JamieTanna/status/1206236519549280257");
  }

  @ParameterizedTest
  @ValueSource(strings = {"https://someone.blog/show?p=2"})
  void itDoesNotSanitiseOtherUrlsOnLikes(String url) {
    // given
    mf2.getProperties().setLikeOf(SingletonListHelper.singletonList(url));

    // when
    Microformats2Object actual = sut.sanitise(mf2);

    // then
    assertThat(actual.getProperties().getLikeOf()).containsExactly(url);
  }

  @ParameterizedTest
  @ValueSource(strings = {"https://someone.blog/show?p=2"})
  void itDoesNotSanitiseOtherUrlsOnReplies(String url) {
    // given
    mf2.getProperties().setInReplyTo(SingletonListHelper.singletonList(url));

    // when
    Microformats2Object actual = sut.sanitise(mf2);

    // then
    assertThat(actual.getProperties().getInReplyTo()).containsExactly(url);
  }

  @ParameterizedTest
  @ValueSource(strings = {"https://someone.blog/show?p=2"})
  void itDoesNotSanitiseOtherUrlsOnReposts(String url) {
    // given
    mf2.getProperties().setRepostOf(SingletonListHelper.singletonList(url));

    // when
    Microformats2Object actual = sut.sanitise(mf2);

    // then
    assertThat(actual.getProperties().getRepostOf()).containsExactly(url);
  }
}
