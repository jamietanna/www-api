package me.jvt.www.api.micropub.presentation.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import me.jvt.www.api.micropub.service.QueryService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionClaimNames;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureMockMvc
@SpringBootTest
@ExtendWith(MockitoExtension.class)
class QueryControllerIntegrationTest {
  private static final String VALID_TOKEN = "VALID_TOKEN";
  private static final String VALID_AUTHORIZATION_HEADER = "Bearer " + VALID_TOKEN;
  @Captor private ArgumentCaptor<Optional<Authentication>> authenticationArgumentCaptor;
  @SpyBean private QueryService service;
  @MockBean private OpaqueTokenIntrospector mockIndieAuthOpaqueTokenIntrospector;

  @Autowired private MockMvc mockMvc;

  @Test
  void itHasAllQueriesConfigured() throws Exception {
    mockMvc
        .perform(get("/micropub?q=config"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(
            jsonPath("$.q")
                .value(
                    Matchers.containsInAnyOrder(
                        "category",
                        "config",
                        "contact",
                        "post-types",
                        "properties",
                        "source",
                        "syndicate-to")));
  }

  @Test
  void configHasPostTypesArray() throws Exception {
    mockMvc
        .perform(get("/micropub?q=config"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(jsonPath("$.post-types").isArray());
  }

  @Test
  void configHasPostTypesArrayWithType() throws Exception {
    mockMvc
        .perform(get("/micropub?q=config"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(jsonPath("$.post-types[0].type").isString());
  }

  @Test
  void configHasPostTypesArrayWithName() throws Exception {
    mockMvc
        .perform(get("/micropub?q=config"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(jsonPath("$.post-types[0].name").isString());
  }

  @Test
  void configHasPostTypesArrayWithProperties() throws Exception {
    mockMvc
        .perform(get("/micropub?q=config"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(jsonPath("$.post-types[0].properties").isArray());
  }

  @Test
  void configHasPostTypesArrayWithRequiredProperties() throws Exception {
    mockMvc
        .perform(get("/micropub?q=config"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(jsonPath("$.post-types[0].required-properties").isArray());
  }

  @Test
  void authenticationIsDelegatedIfProvided() throws Exception {
    setupSecurity();

    mockMvc
        .perform(get("/micropub?q=config").header("Authorization", VALID_AUTHORIZATION_HEADER))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk());

    verify(service).handle(any(), any(), authenticationArgumentCaptor.capture());

    Collection<? extends GrantedAuthority> authorities =
        authenticationArgumentCaptor.getValue().get().getAuthorities();
    assertThat(authorities.contains(new SimpleGrantedAuthority("SCOPE_wibble"))).isTrue();
  }

  @Test
  void syndicateToIsSet() throws Exception {
    mockMvc
        .perform(get("/micropub?q=config"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(jsonPath("$.syndicate-to").isArray());
  }

  @Test
  void postTypesIsSetOnQuery() throws Exception {
    mockMvc
        .perform(get("/micropub?q=post-types"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(jsonPath("$.post-types").isArray());
  }

  private void setupSecurity() {
    Collection<GrantedAuthority> grantedAuthorities =
        AuthorityUtils.commaSeparatedStringToAuthorityList("SCOPE_wibble");
    Map<String, Object> attributes = new HashMap<>();
    attributes.put("client_id", "https://foo.bar.com");
    attributes.put("sub", "https://www.jvt.me/");
    attributes.put(
        OAuth2IntrospectionClaimNames.ISSUED_AT, Instant.now().minus(1, ChronoUnit.DAYS));
    attributes.put(
        OAuth2IntrospectionClaimNames.EXPIRES_AT, Instant.now().plus(1, ChronoUnit.DAYS));

    OAuth2AuthenticatedPrincipal fakeAuthenticatedPrincipal =
        new OAuth2IntrospectionAuthenticatedPrincipal(attributes, grantedAuthorities);
    when(mockIndieAuthOpaqueTokenIntrospector.introspect(VALID_TOKEN))
        .thenReturn(fakeAuthenticatedPrincipal);
  }
}
