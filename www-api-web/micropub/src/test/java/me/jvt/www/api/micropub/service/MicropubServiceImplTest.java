package me.jvt.www.api.micropub.service;

import static com.github.valfirst.slf4jtest.LoggingEvent.info;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.github.valfirst.slf4jtest.TestLogger;
import com.github.valfirst.slf4jtest.TestLoggerFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import me.jvt.www.api.micropub.actions.Action;
import me.jvt.www.api.micropub.actions.ReplacePropertyAction;
import me.jvt.www.api.micropub.client.ContactsClient;
import me.jvt.www.api.micropub.client.TaxonomiesClient;
import me.jvt.www.api.micropub.client.WwwProxyClient;
import me.jvt.www.api.micropub.decorator.Microformats2ObjectDecorator;
import me.jvt.www.api.micropub.decorator.Microformats2ObjectDecoratorFactory;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.MicropubContactDto;
import me.jvt.www.api.micropub.model.MicropubContactDto.MicropubContact;
import me.jvt.www.api.micropub.model.MicropubSourceContainerDto;
import me.jvt.www.api.micropub.model.MicropubSourceDto;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.TaxonomiesResponse;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2ObjectFactory;
import me.jvt.www.api.micropub.model.request.MicropubRequest;
import me.jvt.www.api.micropub.repository.MicropubPersistenceRepository;
import me.jvt.www.api.micropub.sanitiser.Microformats2ObjectSanitiser;
import me.jvt.www.api.micropub.sanitiser.SupportedPropertiesSanitiser;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MicropubServiceImplTest {

  @Mock private ContactsClient mockContactsClient;
  @Mock private TaxonomiesClient mockTaxonomiesClient;
  @Mock private Microformats2ObjectDecoratorFactory decoratorFactory;
  @Mock private Microformats2ObjectDecorator mockDecorator0;
  @Mock private Microformats2ObjectDecorator mockDecorator1;
  @Mock private Microformats2ObjectDecorator mockDecorator2;
  @Mock private MicropubPersistenceRepository micropubPersistenceRepository;
  @Mock private PersistenceService persistenceService;
  @Mock private Microformats2ObjectSanitiser mockSanitiser0;
  @Mock private Microformats2ObjectSanitiser mockSanitiser1;
  @Mock private Microformats2ObjectSanitiser mockSanitiser2;
  @Mock private ContextRetrievalService contextRetrievalService;

  @Mock private WwwProxyClient<MicropubSourceContainerDto> sourceProxyClient;
  @Mock private Microformats2ObjectFactory mf2Factory;
  @Mock private SupportedPropertiesSanitiser supportedPropertiesSanitiser;

  private final TestLogger logger = TestLoggerFactory.getTestLogger(MicropubServiceImpl.class);

  private TestMicroformats2Object mf2;
  private MicropubServiceImpl sut;
  private LinkedHashSet<Microformats2ObjectDecorator> decoratorSet;

  @BeforeEach
  void setup() {
    mf2 = new TestMicroformats2Object();
    mf2.getProperties().setCategory(new ArrayList<>());

    lenient().when(mockDecorator0.decorate(any())).thenReturn(mf2);
    lenient().when(mockDecorator1.decorate(any())).thenReturn(mf2);
    lenient().when(mockDecorator2.decorate(any())).thenReturn(mf2);

    lenient().when(mockSanitiser0.sanitise(any())).thenReturn(mf2);
    lenient().when(mockSanitiser1.sanitise(any())).thenReturn(mf2);
    lenient().when(mockSanitiser2.sanitise(any())).thenReturn(mf2);

    decoratorSet = new LinkedHashSet<>();
    decoratorSet.add(mockDecorator0);
    decoratorSet.add(mockDecorator1);
    decoratorSet.add(mockDecorator2);

    LinkedHashSet<Microformats2ObjectSanitiser> sanitiserSet = new LinkedHashSet<>();
    sanitiserSet.add(mockSanitiser0);
    sanitiserSet.add(mockSanitiser1);
    sanitiserSet.add(mockSanitiser2);

    sut =
        new MicropubServiceImpl(
            decoratorFactory,
            sanitiserSet,
            persistenceService,
            micropubPersistenceRepository,
            mockContactsClient,
            mockTaxonomiesClient,
            sourceProxyClient,
            mf2Factory,
            supportedPropertiesSanitiser,
            contextRetrievalService);

    lenient()
        .when(persistenceService.contentPathFromUrl(any()))
        .thenReturn(Optional.of("content/mf2/2020/04/wibble.md"));
    lenient().when(supportedPropertiesSanitiser.sanitise(any())).thenReturn(mf2);
  }

  @Nested
  class Create {
    @Test
    void delegatesToFactory(@Mock MicropubRequest request) {
      sut.create(request);

      verify(mf2Factory).create(request);
    }

    @Test
    void returnsFromDelegate(@Mock MicropubRequest request, @Mock Microformats2Object mf2) {
      when(mf2Factory.create(any())).thenReturn(mf2);

      Microformats2Object actual = sut.create(request);

      assertThat(actual).isSameAs(mf2);
    }
  }

  @Test
  void performDecorationDelegatesToFactoryWithKind() {
    // given
    mf2.setKind(Kind.contacts);

    // when
    sut.performDecoration(mf2);

    // then
    verify(decoratorFactory).provide(Kind.contacts);
  }

  @Test
  void performDecorationIteratesThroughSetAndCallsDecorate() {
    // given
    when(decoratorFactory.provide(any())).thenReturn(decoratorSet);

    // when
    sut.performDecoration(mf2);

    // then
    verify(mockDecorator0).decorate(any(TestMicroformats2Object.class));
    verify(mockDecorator1).decorate(any(TestMicroformats2Object.class));
    verify(mockDecorator2).decorate(any(TestMicroformats2Object.class));
  }

  @Test
  void performDecorationExecutesOnLastReturnedMf2() {
    // given
    when(decoratorFactory.provide(any())).thenReturn(decoratorSet);
    TestMicroformats2Object mf2_1 = new TestMicroformats2Object();
    TestMicroformats2Object mf2_2 = new TestMicroformats2Object();
    TestMicroformats2Object mf2_3 = new TestMicroformats2Object();

    when(mockDecorator0.decorate(any())).thenReturn(mf2_1);
    when(mockDecorator1.decorate(any())).thenReturn(mf2_2);
    when(mockDecorator2.decorate(any())).thenReturn(mf2_3);

    // when
    sut.performDecoration(mf2);

    // then
    verify(mockDecorator0).decorate(mf2);
    verify(mockDecorator1).decorate(mf2_1);
    verify(mockDecorator2).decorate(mf2_2);
  }

  @Test
  void performDecorationExecutesOnLastReturnedMf2AndReturnsThat() {
    // given
    when(decoratorFactory.provide(any())).thenReturn(decoratorSet);

    TestMicroformats2Object mf2_1 = new TestMicroformats2Object();
    TestMicroformats2Object mf2_2 = new TestMicroformats2Object();
    TestMicroformats2Object mf2_3 = new TestMicroformats2Object();

    when(mockDecorator0.decorate(any())).thenReturn(mf2_1);
    when(mockDecorator1.decorate(any())).thenReturn(mf2_2);
    when(mockDecorator2.decorate(any())).thenReturn(mf2_3);

    // when
    Microformats2Object actual = sut.performDecoration(mf2);

    // then
    assertThat(actual).isEqualTo(mf2_3);
  }

  @Test
  void performSanitisationIteratesThroughSetAndCallsSanitise() {
    // given

    // when
    sut.performSanitisation(mf2);

    // then
    verify(mockSanitiser0).sanitise(any(TestMicroformats2Object.class));
    verify(mockSanitiser1).sanitise(any(TestMicroformats2Object.class));
    verify(mockSanitiser2).sanitise(any(TestMicroformats2Object.class));
  }

  @Test
  void performSanitisationExecutesOnLastReturnedMf2() {
    // given
    TestMicroformats2Object mf2_1 = new TestMicroformats2Object();
    TestMicroformats2Object mf2_2 = new TestMicroformats2Object();
    TestMicroformats2Object mf2_3 = new TestMicroformats2Object();

    when(mockSanitiser0.sanitise(any())).thenReturn(mf2_1);
    when(mockSanitiser1.sanitise(any())).thenReturn(mf2_2);
    when(mockSanitiser2.sanitise(any())).thenReturn(mf2_3);

    // when
    sut.performSanitisation(mf2);

    // then
    verify(mockSanitiser0).sanitise(mf2);
    verify(mockSanitiser1).sanitise(mf2_1);
    verify(mockSanitiser2).sanitise(mf2_2);
  }

  @Test
  void performSanitisationExecutesOnLastReturnedMf2AndReturnsThat() {
    // given
    TestMicroformats2Object mf2_1 = new TestMicroformats2Object();
    TestMicroformats2Object mf2_2 = new TestMicroformats2Object();
    TestMicroformats2Object mf2_3 = new TestMicroformats2Object();

    when(mockSanitiser0.sanitise(any())).thenReturn(mf2_1);
    when(mockSanitiser1.sanitise(any())).thenReturn(mf2_2);
    when(mockSanitiser2.sanitise(any())).thenReturn(mf2_3);

    // when
    Microformats2Object actual = sut.performSanitisation(mf2);

    // then
    assertThat(actual).isEqualTo(mf2_3);
  }

  @Test
  void saveSanitisesBeforeCallToRepository(@Mock Microformats2Object sanitised) {
    // given
    when(supportedPropertiesSanitiser.sanitise(any())).thenReturn(sanitised);

    // when
    sut.save(mf2);

    // then
    InOrder inOrder = Mockito.inOrder(supportedPropertiesSanitiser, micropubPersistenceRepository);
    inOrder.verify(supportedPropertiesSanitiser).sanitise(mf2);
    inOrder.verify(micropubPersistenceRepository).save(sanitised);
  }

  @Test
  void saveTriggersContextRetrievalAfterCallToRepository(@Mock Microformats2Object sanitised) {
    // given
    when(supportedPropertiesSanitiser.sanitise(any())).thenReturn(sanitised);

    // when
    sut.save(mf2);

    // then
    InOrder inOrder = Mockito.inOrder(micropubPersistenceRepository, contextRetrievalService);
    inOrder.verify(micropubPersistenceRepository).save(sanitised);
    inOrder.verify(contextRetrievalService).retrieveAndPersistContext(sanitised);
  }

  @Test
  void saveDelegatesToRepository(@Mock Microformats2Object sanitised) {
    // given
    when(supportedPropertiesSanitiser.sanitise(any())).thenReturn(sanitised);

    // when
    sut.save(mf2);

    // then
    verify(micropubPersistenceRepository).save(sanitised);
  }

  @Test
  void saveReturnsSlugReturnedFromRepository() {
    // given
    when(micropubPersistenceRepository.save(any())).thenReturn("This is the slug");

    // when
    String actual = sut.save(mf2);

    // then
    assertThat(actual).isEqualTo("This is the slug");
  }

  @Test
  void retrieveDelegatesToClientWithPathFromService() {
    // given
    when(persistenceService.contentPathFromUrl(any()))
        .thenReturn(Optional.of("/path/to/content.md"));
    when(micropubPersistenceRepository.findById(anyString())).thenReturn(Optional.of(mf2));
    mf2.getContext().setSlug("");

    // when
    sut.retrieve("https://www.jvt.me/mf2/40d3eba5-f620-4e93-8d20-94a376fd9670/", false);

    // then
    verify(micropubPersistenceRepository).findById("/path/to/content.md");
  }

  @Test
  void updateThrowsInvalidMicropubMetadataRequestWhenNotMf2Url() {
    // given
    when(persistenceService.contentPathFromUrl(any())).thenReturn(Optional.empty());

    // when
    assertThatThrownBy(() -> sut.retrieve("https://www.jvt.me/posts/foo/bar", false))
        // then
        .isInstanceOf(InvalidMicropubMetadataRequest.class)
        .hasMessage("URL https://www.jvt.me/posts/foo/bar is not supported for retrieve/update");
  }

  @Test
  void retrieveReturnsEmptyOptionalWhenFileNotFoundInGitLab() {
    // given
    when(micropubPersistenceRepository.findById(anyString())).thenReturn(Optional.empty());

    // when
    Optional<Microformats2Object> optional =
        sut.retrieve("https://www.jvt.me/mf2/2020/04/wibble/", false);

    // then
    assertThat(optional).isNotPresent();
  }

  @Test
  void retrieveLogsWhenFileNotFoundInGitLab() {
    // given
    when(micropubPersistenceRepository.findById(anyString())).thenReturn(Optional.empty());

    // when
    sut.retrieve("https://www.jvt.me/mf2/2020/04/wibble/", false);

    // then
    assertThat(logger.getLoggingEvents())
        .contains(
            info(
                "The source data for {} ({}) could not be found",
                "https://www.jvt.me/mf2/2020/04/wibble/",
                "content/mf2/2020/04/wibble.md"));
  }

  @Test
  void retrieveReturnsEmptyOptionalWhenFileDeletedAndClientHasNoPermissions() {
    // given
    when(micropubPersistenceRepository.findById(anyString())).thenReturn(Optional.of(mf2));
    mf2.getContext().setDeleted(true);

    // when
    Optional<Microformats2Object> optional =
        sut.retrieve("https://www.jvt.me/mf2/2020/04/wibble/", false);

    // then
    assertThat(optional).isNotPresent();
  }

  @Test
  void retrieveLogsWhenFileDeletedAndClientHasNoPermissions() {
    // given
    when(micropubPersistenceRepository.findById(anyString())).thenReturn(Optional.of(mf2));
    mf2.getContext().setDeleted(true);

    // when
    sut.retrieve("https://www.jvt.me/mf2/2020/04/wibble/", false);

    // then
    assertThat(logger.getLoggingEvents())
        .contains(
            info(
                "The source data for {} ({}) was found, but the client has no permission to view it, as it's been marked as deleted",
                "https://www.jvt.me/mf2/2020/04/wibble/",
                "content/mf2/2020/04/wibble.md"));
  }

  @Test
  void retrieveAllDelegatesToProxy() {
    // given
    MicropubSourceContainerDto container = new MicropubSourceContainerDto();
    container.items = Collections.emptyList();
    when(sourceProxyClient.retrieve()).thenReturn(container);

    // when
    sut.retrieveAll();

    // then
    verify(sourceProxyClient).retrieve();
  }

  @Test
  void retrieveAllReturnsValueFromProxyAndRemovesItemsWithEmptyProperties() {
    // given
    MicropubSourceDto validItem = new MicropubSourceDto();
    validItem.setProperties(new Properties());
    MicropubSourceContainerDto container = new MicropubSourceContainerDto();
    container.items = new ArrayList<>();
    container.items.add(validItem);
    container.items.add(new MicropubSourceDto());

    when(sourceProxyClient.retrieve()).thenReturn(container);

    // when
    MicropubSourceContainerDto actual = sut.retrieveAll();

    // then
    assertThat(actual.items).containsExactly(validItem);
  }

  @Test
  void retrieveCategoriesDelegatesToClient() {
    // given
    when(mockTaxonomiesClient.retrieve())
        .thenReturn(new TaxonomiesResponse(Collections.emptySet()));

    // when
    sut.retrieveCategories();

    // then
    verify(mockTaxonomiesClient).retrieve();
  }

  @Test
  void retrieveCategoriesReturnsValueFromClientAndRemovesEmptyStrings() {
    // given
    Set<String> categories = new HashSet<>();
    categories.add("hello");
    categories.add("");
    categories.add("another");

    when(mockTaxonomiesClient.retrieve()).thenReturn(new TaxonomiesResponse(categories));

    // when
    Set<String> actual = sut.retrieveCategories();

    // then
    assertThat(actual).containsExactlyInAnyOrder("hello", "another");
  }

  @Test
  void retrieveContactsDelegatesToClient() {
    // given
    when(mockContactsClient.retrieve()).thenReturn(new MicropubContactDto(Collections.emptyList()));

    // when
    sut.retrieveContacts();

    // then
    verify(mockContactsClient).retrieve();
  }

  @Test
  void retrieveContactsReturnsValueFromClient() {
    // given
    List<MicropubContact> contacts = new ArrayList<>();
    when(mockContactsClient.retrieve()).thenReturn(new MicropubContactDto(contacts));

    // when
    List<MicropubContact> actual = sut.retrieveContacts();

    // then
    assertThat(actual).isEqualTo(contacts);
  }

  @Test
  void retrieveReturnsOptionalFromRepository() {
    // given
    when(micropubPersistenceRepository.findById(anyString())).thenReturn(Optional.of(mf2));
    mf2.getContext().setDeleted(true);
    mf2.getContext().setSlug("");

    // when
    Optional<Microformats2Object> optional =
        sut.retrieve("https://www.jvt.me/mf2/2020/04/wibble/", true);

    // then
    assertThat(optional).isPresent();
    assertThat(optional.get()).isSameAs(mf2);
  }

  @ParameterizedTest
  @NullAndEmptySource
  void retrieveHandlesNullOrEmptyContentList(List<Map<String, String>> contentList) {
    // given
    when(micropubPersistenceRepository.findById(anyString())).thenReturn(Optional.of(mf2));
    mf2.getProperties().setContent(contentList);
    mf2.getContext().setSlug("");

    // when
    Optional<Microformats2Object> optional =
        sut.retrieve("https://www.jvt.me/mf2/2020/04/wibble/", false);

    // then
    // no exception
  }

  @ParameterizedTest
  @NullAndEmptySource
  void retrieveInitialisesContentHtmlAsEmptyStringIfContentIsPresent(String html) {
    // given
    when(micropubPersistenceRepository.findById(anyString())).thenReturn(Optional.of(mf2));
    mf2.getContext().setSlug("");
    Map<String, String> content = new HashMap<>();
    mf2.getProperties().setContent(SingletonListHelper.singletonList(content));
    content.put("html", html);

    // when
    Optional<Microformats2Object> optional =
        sut.retrieve("https://www.jvt.me/mf2/2020/04/wibble/", false);

    // then
    Map<String, String> actual = optional.get().getProperties().getContent().get(0);
    assertThat(actual.get("html")).isEqualTo("");
  }

  @Test
  void retrieveDeduplicatesCategories() {
    // given
    when(micropubPersistenceRepository.findById(anyString())).thenReturn(Optional.of(mf2));
    List<String> categories = new ArrayList<>();
    categories.add("abc");
    categories.add("def");
    categories.add("ghi");
    categories.add("abc");
    categories.add("def");
    categories.add("ghi");
    mf2.getProperties().setCategory(categories);
    mf2.getContext().setSlug("");

    // when
    Optional<Microformats2Object> optional =
        sut.retrieve("https://www.jvt.me/mf2/2020/04/wibble/", false);

    // then
    List<String> actual = optional.get().getProperties().getCategory();
    assertThat(actual).containsExactly("abc", "def", "ghi");
  }

  @Test
  void retrieveOnlyReturnsSupportedProperties() {
    when(micropubPersistenceRepository.findById(anyString())).thenReturn(Optional.of(mf2));

    sut.retrieve("", false);

    verify(supportedPropertiesSanitiser).sanitise(mf2);
  }

  @Test
  void retrieveReturnsMf2FromSupportedPropertiesSanitiser(@Mock Microformats2Object sanitised) {
    when(micropubPersistenceRepository.findById(anyString())).thenReturn(Optional.of(mf2));
    when(supportedPropertiesSanitiser.sanitise(any())).thenReturn(sanitised);

    Optional<Microformats2Object> optional = sut.retrieve("", false);

    assertThat(optional).contains(sanitised);
  }

  @Test
  void retrieveSetSlugIfNotSetOnArticleUsingWeekNoteDecorator() {
    // given
    when(micropubPersistenceRepository.findById(anyString())).thenReturn(Optional.of(mf2));
    mf2.setKind(Kind.articles);
    mf2.getProperties().setName(Collections.singletonList("Week Notes 21#10"));

    // when
    Optional<Microformats2Object> optional =
        sut.retrieve("https://www.jvt.me/mf2/2020/04/wibble/", false);

    // then
    assertThat(optional).isPresent();
    assertThat(optional.get().getContext().getSlug()).isNotNull();
  }

  @Test
  void retrieveDoesNotOverwriteContentHtmlIfHtmlAlreadySet() {
    // given
    when(micropubPersistenceRepository.findById(anyString())).thenReturn(Optional.of(mf2));
    Map<String, String> content = new HashMap<>();
    mf2.getProperties().setContent(SingletonListHelper.singletonList(content));
    content.put("html", "foo");
    mf2.getContext().setSlug("");

    // when
    Optional<Microformats2Object> optional =
        sut.retrieve("https://www.jvt.me/mf2/2020/04/wibble/", false);

    // then
    Map<String, String> actual = optional.get().getProperties().getContent().get(0);
    assertThat(actual.get("html")).isEqualTo("foo");
  }

  @Test
  void updateIteratesThroughActionsAndReturnsFinalValue(
      @Mock Action action0, @Mock Action action1, @Mock Action action2) {
    TestMicroformats2Object mf2 = new TestMicroformats2Object();

    // given
    List<Action> actions = new ArrayList<>();
    actions.add(action0);
    actions.add(action1);
    actions.add(action2);

    // when
    Microformats2Object actual = sut.update(mf2, actions);

    // then
    assertThat(actual).isSameAs(mf2);
    verify(action0).accept(mf2);
    verify(action1).accept(mf2);
    verify(action2).accept(mf2);
  }

  @Test
  void updateDoesNotAllowChangingPostStatusToDraftFromPublished() {
    TestMicroformats2Object mf2 = new TestMicroformats2Object();
    mf2.getProperties().setPostStatus(SingletonListHelper.singletonList("published"));

    ReplacePropertyAction action =
        new ReplacePropertyAction("post-status", SingletonListHelper.singletonList("draft"));

    Microformats2Object actual = sut.update(mf2, SingletonListHelper.singletonList(action));

    assertThat(actual.getProperties().getPostStatus()).containsExactly("published");
  }

  @Test
  void updateAllowsChangingPostStatusToPublishedFromDraft() {
    TestMicroformats2Object mf2 = new TestMicroformats2Object();
    mf2.getProperties().setPostStatus(SingletonListHelper.singletonList("draft"));

    ReplacePropertyAction action =
        new ReplacePropertyAction("post-status", SingletonListHelper.singletonList("published"));

    Microformats2Object actual = sut.update(mf2, SingletonListHelper.singletonList(action));

    assertThat(actual.getProperties().getPostStatus()).containsExactly("published");
  }

  @ParameterizedTest
  @NullAndEmptySource
  void updateHandlesPostStatusAsNullOrEmpty(List<String> postStatus) {
    TestMicroformats2Object mf2 = new TestMicroformats2Object();
    mf2.getProperties().setPostStatus(postStatus);

    ReplacePropertyAction action =
        new ReplacePropertyAction("post-status", SingletonListHelper.singletonList("draft"));

    Microformats2Object actual = sut.update(mf2, SingletonListHelper.singletonList(action));

    assertThat(actual.getProperties().getPostStatus()).containsExactly("draft");
  }

  @Test
  void updateHandlesPostStatusAsInvalidValueAndOverrides() {
    TestMicroformats2Object mf2 = new TestMicroformats2Object();
    mf2.getProperties().setPostStatus(SingletonListHelper.singletonList("foo"));

    ReplacePropertyAction action =
        new ReplacePropertyAction("post-status", SingletonListHelper.singletonList("draft"));

    Microformats2Object actual = sut.update(mf2, SingletonListHelper.singletonList(action));

    assertThat(actual.getProperties().getPostStatus()).containsExactly("draft");
  }
}
