package me.jvt.www.api.micropub.presentation.rest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import me.jvt.www.api.micropub.model.MicropubQueryDto;
import me.jvt.www.api.micropub.service.QueryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.util.MultiValueMap;

@ExtendWith(MockitoExtension.class)
class QueryControllerUnitTest {
  @Mock private QueryService service;

  @InjectMocks private QueryController controller;

  @Test
  void itDelegatesQueryToService() {
    controller.query("config", null, null);

    verify(service).handle(eq("config"), any(), any());
  }

  @Test
  void itDelegatesParamsToService(@Mock MultiValueMap<String, String> params) {
    controller.query("", params, null);

    verify(service).handle(any(), same(params), any());
  }

  @Test
  void itDelegatesAuthenticationToServiceWhenNullAuth() {
    controller.query("", null, null);

    verify(service).handle(any(), any(), eq(Optional.empty()));
  }

  @Test
  void itDelegatesAuthenticationToServiceWhenAuth(@Mock Authentication authentication) {
    controller.query("", null, authentication);

    verify(service).handle(any(), any(), eq(Optional.of(authentication)));
  }

  @Test
  void itReturnsFromDelegate(@Mock MicropubQueryDto dto) {
    when(service.handle(any(), any(), any())).thenReturn(dto);

    MicropubQueryDto actual = controller.query("", null, null);

    assertThat(actual).isSameAs(dto);
  }
}
