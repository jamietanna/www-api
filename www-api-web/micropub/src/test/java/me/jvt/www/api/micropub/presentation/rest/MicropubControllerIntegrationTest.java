package me.jvt.www.api.micropub.presentation.rest;

import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.KindNotSupportedException;
import me.jvt.www.api.micropub.model.posttype.PostTypeDiscoverer;
import me.jvt.www.api.micropub.service.MicropubService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionClaimNames;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureMockMvc
@SpringBootTest
class MicropubControllerIntegrationTest {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final String VALID_TOKEN = "VALID_TOKEN";
  private static final String VALID_AUTHORIZATION_HEADER = "Bearer " + VALID_TOKEN;
  private static final String INVALID_TOKEN = "Bearer INVALID_TOKEN";

  @MockBean private PostTypeDiscoverer discoverer;
  @MockBean private OpaqueTokenIntrospector mockIndieAuthOpaqueTokenIntrospector;

  @MockBean private MicropubService micropubService;

  @Autowired private MockMvc mockMvc;

  @SuppressWarnings("unused")
  abstract class FormTest {
    abstract String contentType();

    @Test
    void accessTokenCannotBeProvidedInBodyAndAuthorizationHeader() throws Exception {
      mockMvc
          .perform(
              (post("/micropub")
                  .contentType(contentType())
                  .header("Authorization", VALID_AUTHORIZATION_HEADER)
                  .param("access_token", VALID_TOKEN)
                  .param("h", "entry")
                  .param("content", "hello world")
                  .param("category[]", "foo")
                  .param("category[]", "bar")))
          // then
          .andExpect(MockMvcResultMatchers.status().isBadRequest());
      // TODO: closer validation
    }

    @Test
    void accessTokenCanBeProvidedInBody() throws Exception {
      when(micropubService.save(any())).thenReturn("/mf2/2020/xx/xx/xxxxx/");
      setupSecurity("SCOPE_create");

      mockMvc
          .perform(
              (post("/micropub")
                  .contentType(contentType())
                  .param("access_token", VALID_TOKEN)
                  .param("h", "entry")
                  .param("content", "hello world")
                  .param("category[]", "foo")
                  .param("category[]", "bar")))
          // then
          .andExpect(MockMvcResultMatchers.status().isAccepted())
          .andExpect(
              MockMvcResultMatchers.redirectedUrl("https://www.jvt.me/mf2/2020/xx/xx/xxxxx/"));
    }

    @Test
    void isRejectedWith401InsufficientScopeIfExpectedScopeNotPresent() throws Exception {
      setupSecurity("SCOPE__NOT__VALID");

      mockMvc
          .perform(
              (post("/micropub")
                  .contentType(contentType())
                  .header("Authorization", VALID_AUTHORIZATION_HEADER)
                  .param("h", "entry")
                  .param("content", "hello world")
                  .param("category[]", "foo")
                  .param("category[]", "bar")))
          // then
          .andDo(MockMvcResultHandlers.print())
          .andExpect(MockMvcResultMatchers.status().isUnauthorized())
          .andExpect(jsonPath("$.error").value("insufficient_scope"));
    }

    @Test
    void isRejectedWith401UnauthorizedIfNoTokenProvided() throws Exception {
      mockMvc
          .perform(
              (post("/micropub")
                  .contentType(contentType())
                  .param("h", "entry")
                  .param("content", "hello world")
                  .param("category[]", "foo")
                  .param("category[]", "bar")))
          // then
          .andDo(MockMvcResultHandlers.print())
          .andExpect(MockMvcResultMatchers.status().isUnauthorized())
          .andExpect(jsonPath("$.error").value("unauthorized"));
    }

    @Test
    void isRejectedWith403ForbiddenIfNotMyProfile() throws Exception {
      setInvalidPrincipal();

      mockMvc
          .perform(
              (post("/micropub")
                  .contentType(contentType())
                  .header("Authorization", INVALID_TOKEN)
                  .param("h", "entry")
                  .param("content", "hello world")
                  .param("category[]", "foo")
                  .param("category[]", "bar")))
          // then
          .andDo(MockMvcResultHandlers.print())
          .andExpect(MockMvcResultMatchers.status().isForbidden())
          .andExpect(jsonPath("$.error").value("forbidden"));
    }

    @Nested
    class Create {

      @Test
      void validPostReturnsSlug() throws Exception {
        when(micropubService.save(any())).thenReturn("/mf2/2020/xx/xx/xxxxx/");
        setupSecurity("SCOPE_create");

        mockMvc
            .perform(
                (post("/micropub")
                    .contentType(contentType())
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .param("h", "entry")
                    .param("content", "hello world")
                    .param("category[]", "foo")
                    .param("category[]", "bar")))
            // then
            .andExpect(MockMvcResultMatchers.status().isAccepted())
            .andExpect(
                MockMvcResultMatchers.redirectedUrl("https://www.jvt.me/mf2/2020/xx/xx/xxxxx/"));
      }

      @ParameterizedTest
      @ValueSource(strings = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_HTML_VALUE})
      void isRejectedWith401InsufficientScopeIfWrongScopeForCreate(String accept) throws Exception {
        setupSecurity("SCOPE_delete");

        mockMvc
            .perform(
                (post("/micropub")
                    .accept(accept)
                    .contentType(contentType())
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .param("h", "entry")
                    .param("content", "hello world")
                    .param("category[]", "foo")
                    .param("category[]", "bar")))
            // then
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isUnauthorized())
            .andExpect(jsonPath("$.error").value("insufficient_scope"));
      }

      @Test
      void htmlRequestsReturnHtmlResponse() throws Exception {
        when(micropubService.save(any())).thenReturn("2020/xx/xx/xxxxx");
        setupSecurity("SCOPE_create");

        mockMvc
            .perform(
                (post("/micropub").contentType(contentType()).accept(MediaType.TEXT_HTML))
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .param("h", "entry")
                    .param("content", "hello world")
                    .param("category[]", "foo")
                    .param("category[]", "bar"))
            // then
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(
                MockMvcResultMatchers.header()
                    .string(HttpHeaders.CONTENT_TYPE, startsWith(MediaType.TEXT_HTML_VALUE)));
      }
    }

    @Nested
    class Delete {

      @ParameterizedTest
      @ValueSource(strings = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_HTML_VALUE})
      void isRejectedWith401InsufficientScopeIfWrongScopeForDelete(String accept) throws Exception {
        setupSecurity("SCOPE_create");

        mockMvc
            .perform(
                (post("/micropub")
                    .accept(accept)
                    .contentType(contentType())
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .param("action", "delete")
                    .param("url", "https://")))
            // then
            .andExpect(MockMvcResultMatchers.status().isUnauthorized())
            .andExpect(jsonPath("$.error").value("insufficient_scope"));
      }

      @ParameterizedTest
      @ValueSource(strings = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_HTML_VALUE})
      void isRejectedWith401InsufficientScopeIfDraftScope(String accept) throws Exception {
        setupSecurity("SCOPE_delete,SCOPE_draft");

        mockMvc
            .perform(
                (post("/micropub")
                    .accept(accept)
                    .contentType(contentType())
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .param("action", "delete")
                    .param("url", "https://")))
            // then
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isUnauthorized())
            .andExpect(jsonPath("$.error").value("insufficient_scope"));
      }

      @Test
      void isAllowedWhenDeleteScope() throws Exception {
        setupSecurity("SCOPE_delete");

        setUpRetrieveAndUpdate();

        mockMvc
            .perform(
                (post("/micropub")
                    .accept(MediaType.APPLICATION_JSON_VALUE)
                    .contentType(contentType())
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .param("action", "delete")
                    .param("url", "https://")))
            // then
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isNoContent());
      }

      @Test
      void isAllowedWhenDeleteScopeAsHtml() throws Exception {
        setupSecurity("SCOPE_delete");

        setUpRetrieveAndUpdate();

        mockMvc
            .perform(
                (post("/micropub")
                    .accept(MediaType.TEXT_HTML_VALUE)
                    .contentType(contentType())
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .param("action", "delete")
                    .param("url", "https://")))
            // then
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isOk());
      }
    }

    @Nested
    class Undelete {

      @ParameterizedTest
      @ValueSource(strings = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_HTML_VALUE})
      void isRejectedWith401InsufficientScopeIfWrongScopeForUndelete(String accept)
          throws Exception {
        setupSecurity("SCOPE_create");

        mockMvc
            .perform(
                (post("/micropub")
                    .accept(accept)
                    .contentType(contentType())
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .param("action", "undelete")
                    .param("url", "https://")))
            // then
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isUnauthorized())
            .andExpect(jsonPath("$.error").value("insufficient_scope"));
      }

      @ParameterizedTest
      @ValueSource(strings = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_HTML_VALUE})
      void isRejectedWith401InsufficientScopeIfDraftScope(String accept) throws Exception {
        setupSecurity("SCOPE_undelete,SCOPE_draft");

        mockMvc
            .perform(
                (post("/micropub")
                    .accept(accept)
                    .contentType(contentType())
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .param("action", "undelete")
                    .param("url", "https://")))
            // then
            .andExpect(MockMvcResultMatchers.status().isUnauthorized())
            .andExpect(jsonPath("$.error").value("insufficient_scope"));
      }

      @Test
      void isAllowedWhenUndeleteScope() throws Exception {
        setupSecurity("SCOPE_undelete");

        setUpRetrieveAndUpdate();

        mockMvc
            .perform(
                (post("/micropub")
                    .accept(MediaType.APPLICATION_JSON_VALUE)
                    .contentType(contentType())
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .param("action", "undelete")
                    .param("url", "https://")))
            // then
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isNoContent());
      }

      @Test
      void isAllowedWhenUndeleteScopeAsHtml() throws Exception {
        setupSecurity("SCOPE_undelete");

        setUpRetrieveAndUpdate();

        mockMvc
            .perform(
                (post("/micropub")
                    .accept(MediaType.TEXT_HTML_VALUE)
                    .contentType(contentType())
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .param("action", "undelete")
                    .param("url", "https://")))
            // then
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isOk());
      }
    }
  }

  @Nested
  class FormPost extends FormTest {

    @Override
    String contentType() {
      return MediaType.APPLICATION_FORM_URLENCODED_VALUE;
    }
  }

  @Nested
  class MultipartFormPost extends FormTest {

    @Override
    String contentType() {
      return MediaType.MULTIPART_FORM_DATA_VALUE;
    }
  }

  @SuppressWarnings("unused")
  abstract class JsonTest {
    abstract ObjectNode body();

    abstract String expectedScope();

    @Test
    void isRejectedWith401InsufficientScopeIfExpectedScopeNotPresent() throws Exception {
      setupSecurity("SCOPE__NOT__VALID");

      mockMvc
          .perform(
              (post("/micropub")
                  .header("Authorization", VALID_AUTHORIZATION_HEADER)
                  .contentType(MediaType.APPLICATION_JSON)
                  .content(OBJECT_MAPPER.writeValueAsString(body()))))
          // then
          .andExpect(MockMvcResultMatchers.status().isUnauthorized())
          .andExpect(jsonPath("$.error").value("insufficient_scope"));
    }

    @Test
    void isRejectedWith401UnauthorizedIfNoTokenProvided() throws Exception {
      mockMvc
          .perform(
              (post("/micropub")
                  .contentType(MediaType.APPLICATION_JSON)
                  .content(OBJECT_MAPPER.writeValueAsString(body()))))
          // then
          .andExpect(MockMvcResultMatchers.status().isUnauthorized())
          .andExpect(jsonPath("$.error").value("unauthorized"));
    }

    @Test
    void isRejectedWith403ForbiddenIfNotMyProfile() throws Exception {
      setInvalidPrincipal();

      mockMvc
          .perform(
              (post("/micropub")
                  .header("Authorization", INVALID_TOKEN)
                  .contentType(MediaType.APPLICATION_JSON)
                  .content(OBJECT_MAPPER.writeValueAsString(body()))))
          // then
          .andExpect(MockMvcResultMatchers.status().isForbidden())
          .andExpect(jsonPath("$.error").value("forbidden"));
    }
  }

  @Nested
  class Json {

    @Nested
    class Create extends JsonTest {
      ObjectNode body() {
        when(discoverer.discover(any(), any())).thenReturn(Kind.notes);

        ObjectNode body = OBJECT_MAPPER.createObjectNode();
        body.set("type", OBJECT_MAPPER.createArrayNode().add("h-entry"));
        ObjectNode properties = OBJECT_MAPPER.createObjectNode();
        properties.set("content", OBJECT_MAPPER.createArrayNode().add("hello world"));
        properties.set("category", OBJECT_MAPPER.createArrayNode().add("foo").add("bar"));
        properties.set(
            "photo",
            OBJECT_MAPPER
                .createArrayNode()
                .add("https://photos.example.com/592829482876343254.jpg"));

        body.set("properties", properties);

        return body;
      }

      @Override
      String expectedScope() {
        return "SCOPE_create";
      }

      @Test
      void isParsedSuccessfullyWhenValid() throws Exception {
        setupSecurity(expectedScope());

        mockMvc
            .perform(
                (post("/micropub")
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(OBJECT_MAPPER.writeValueAsString(body()))))
            // then
            .andExpect(MockMvcResultMatchers.status().isAccepted());
      }

      @Test
      void htmlRequestsReturnHtmlResponse() throws Exception {
        setupSecurity(expectedScope());

        mockMvc
            .perform(
                (post("/micropub")
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.TEXT_HTML)
                    .content(OBJECT_MAPPER.writeValueAsString(body()))))
            // then
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(
                MockMvcResultMatchers.header()
                    .string(HttpHeaders.CONTENT_TYPE, startsWith(MediaType.TEXT_HTML_VALUE)));
      }
    }

    @Nested
    class Delete extends JsonTest {

      @Override
      ObjectNode body() {
        ObjectNode body = OBJECT_MAPPER.createObjectNode();
        body.put("action", "delete");
        body.put("url", "https://www.jvt.me/mf2/something/");

        return body;
      }

      @Override
      String expectedScope() {
        return "SCOPE_delete";
      }

      @Test
      void isRejectedWith401InsufficientScopeIfDraftScope() throws Exception {
        setupSecurity(expectedScope() + ",SCOPE_draft");

        setUpRetrieveAndUpdate();

        mockMvc
            .perform(
                (post("/micropub")
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(OBJECT_MAPPER.writeValueAsString(body()))))
            // then
            .andExpect(MockMvcResultMatchers.status().isUnauthorized())
            .andExpect(jsonPath("$.error").value("insufficient_scope"));
      }

      @Test
      void isParsedWhenValid() throws Exception {
        setupSecurity(expectedScope());

        setUpRetrieveAndUpdate();

        mockMvc
            .perform(
                (post("/micropub")
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(OBJECT_MAPPER.writeValueAsString(body()))))
            // then
            .andExpect(MockMvcResultMatchers.status().isNoContent());
      }
    }

    @Nested
    class Undelete extends JsonTest {

      @Override
      ObjectNode body() {
        ObjectNode body = OBJECT_MAPPER.createObjectNode();
        body.put("action", "undelete");
        body.put("url", "https://www.jvt.me/mf2/something/");

        return body;
      }

      @Override
      String expectedScope() {
        return "SCOPE_undelete";
      }

      @Test
      void isRejectedWith401InsufficientScopeIfDraftScope() throws Exception {
        setupSecurity(expectedScope() + ",SCOPE_draft");

        TestMicroformats2Object mf2 = new TestMicroformats2Object();
        mf2.setKind(Kind.notes);
        when(micropubService.retrieve(anyString(), anyBoolean())).thenReturn(Optional.of(mf2));

        mockMvc
            .perform(
                (post("/micropub")
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(OBJECT_MAPPER.writeValueAsString(body()))))
            // then
            .andExpect(MockMvcResultMatchers.status().isUnauthorized())
            .andExpect(jsonPath("$.error").value("insufficient_scope"));
      }

      @Test
      void isParsedWhenValid() throws Exception {
        setupSecurity(expectedScope());

        TestMicroformats2Object mf2 = new TestMicroformats2Object();
        mf2.setKind(Kind.notes);
        when(micropubService.retrieve(anyString(), anyBoolean())).thenReturn(Optional.of(mf2));

        mockMvc
            .perform(
                (post("/micropub")
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(OBJECT_MAPPER.writeValueAsString(body()))))
            // then
            .andExpect(MockMvcResultMatchers.status().isNoContent());
      }
    }

    @Nested
    class Update {

      @Nested
      class ReplaceAction extends UpdateTest {

        @Override
        ObjectNode body() {
          ObjectNode body = super.body();
          ObjectNode replace = body.putObject("replace");
          replace.set("content", OBJECT_MAPPER.createArrayNode().add("hello moon"));
          return body;
        }

        @Test
        void isParsedWhenValid() throws Exception {
          setupSecurity(expectedScope());

          setUpRetrieveAndUpdate();

          mockMvc
              .perform(
                  (post("/micropub")
                      .header("Authorization", VALID_AUTHORIZATION_HEADER)
                      .contentType(MediaType.APPLICATION_JSON)
                      .content(OBJECT_MAPPER.writeValueAsString(body()))))
              // then
              .andDo(MockMvcResultHandlers.print())
              .andExpect(MockMvcResultMatchers.status().isOk());
        }
      }

      @Nested
      class AddAction extends UpdateTest {

        @Override
        ObjectNode body() {
          ObjectNode body = super.body();
          ObjectNode replace = body.putObject("replace");
          replace.set(
              "syndication",
              OBJECT_MAPPER
                  .createArrayNode()
                  .add(
                      "http://web.archive.org/web/20040104110725/https://aaronpk.example/2014/06/01/9/indieweb"));
          return body;
        }

        @Test
        void isParsedWhenValid() throws Exception {
          setupSecurity(expectedScope());

          setUpRetrieveAndUpdate();

          mockMvc
              .perform(
                  (post("/micropub")
                      .header("Authorization", VALID_AUTHORIZATION_HEADER)
                      .contentType(MediaType.APPLICATION_JSON)
                      .content(OBJECT_MAPPER.writeValueAsString(body()))))
              // then
              .andExpect(MockMvcResultMatchers.status().isOk());
        }
      }

      @Nested
      class RemoveAction {

        @Nested
        class List extends UpdateTest {

          @Override
          ObjectNode body() {
            ObjectNode body = super.body();
            body.set("delete", OBJECT_MAPPER.createArrayNode().add("category"));
            return body;
          }

          @Test
          void isParsedWhenValid() throws Exception {
            setupSecurity(expectedScope());

            setUpRetrieveAndUpdate();

            mockMvc
                .perform(
                    (post("/micropub")
                        .header("Authorization", VALID_AUTHORIZATION_HEADER)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(OBJECT_MAPPER.writeValueAsString(body()))))
                // then
                .andExpect(MockMvcResultMatchers.status().isOk());
          }
        }

        @Nested
        class Object extends UpdateTest {

          @Override
          ObjectNode body() {
            ObjectNode body = super.body();
            ObjectNode delete = body.putObject("delete");
            delete.set("category", OBJECT_MAPPER.createArrayNode().add("indieweb"));
            return body;
          }

          @Test
          void isParsedWhenValid() throws Exception {
            setupSecurity(expectedScope());

            setUpRetrieveAndUpdate();

            mockMvc
                .perform(
                    (post("/micropub")
                        .header("Authorization", VALID_AUTHORIZATION_HEADER)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(OBJECT_MAPPER.writeValueAsString(body()))))
                // then
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
          }
        }
      }

      class UpdateTest extends JsonTest {

        @Override
        ObjectNode body() {
          ObjectNode body = OBJECT_MAPPER.createObjectNode();
          body.put("action", "update");
          body.put("url", "https://wibble");

          return body;
        }

        @Override
        String expectedScope() {
          return "SCOPE_update";
        }
      }
    }
  }

  @Nested
  class ConstraintViolationExceptions {
    @Mock private ConstraintViolation<?> violation;
    @Mock private ConstraintViolation<?> anotherViolation;

    @AfterEach
    void tearDown() {
      Mockito.reset(micropubService);
    }

    @Test
    void constraintViolationExceptionsAreMappedToInvalidRequest() throws Exception {
      when(violation.getMessage()).thenReturn("Field error");
      when(anotherViolation.getMessage()).thenReturn("Object error");
      Set<ConstraintViolation<?>> violations = new HashSet<>();
      violations.add(violation);
      violations.add(anotherViolation);
      ConstraintViolationException exceptionToThrow =
          new ConstraintViolationException("save.arg0", violations);
      when(micropubService.save(any())).thenThrow(exceptionToThrow);
      setupSecurity("SCOPE_create");

      mockMvc
          .perform(
              (post("/micropub")
                  .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                  .param("access_token", VALID_TOKEN)
                  .param("h", "entry")
                  .param("content", "hello world")
                  .param("category[]", "foo")
                  .param("category[]", "bar")))
          // then
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(jsonPath("$.error").value("invalid_request"))
          .andExpect(
              jsonPath("$.error_description")
                  .value(startsWith("Validation failed on the request:")))
          .andExpect(jsonPath("$.error_description").value(stringContainsInOrder("Field error")))
          .andExpect(jsonPath("$.error_description").value(stringContainsInOrder("Object error")));
    }
  }

  @Nested
  class KindNotSupported {

    @AfterEach
    void tearDown() {
      Mockito.reset(micropubService);
    }

    @Test
    void kindNotSupportedExceptionsAreMappedToInvalidRequest() throws Exception {
      setupSecurity("SCOPE_create");

      when(micropubService.create(any())).thenThrow(new KindNotSupportedException("Not supported"));

      mockMvc
          .perform(
              (post("/micropub")
                  .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                  .param("access_token", VALID_TOKEN)
                  .param("h", "entry")
                  .param("content", "hello world")
                  .param("category[]", "foo")
                  .param("category[]", "bar")))
          // then
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(jsonPath("$.error").value("invalid_request"))
          .andExpect(jsonPath("$.error_description").value("Not supported"));
    }
  }

  @Nested
  class InvalidAction {

    @AfterEach
    void tearDown() {
      Mockito.reset(micropubService);
    }

    @Test
    void invalidActionExceptionsAreMappedToInvalidRequest() throws Exception {
      setupSecurity("SCOPE_create");

      mockMvc
          .perform(
              (post("/micropub")
                  .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                  .param("access_token", VALID_TOKEN)
                  .param("action", "foobar")))
          // then
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(jsonPath("$.error").value("invalid_request"))
          .andExpect(
              jsonPath("$.error_description")
                  .value("Action provided is not understood by this server"));
    }
  }

  private void setupSecurity(String authorityString) {
    Collection<GrantedAuthority> grantedAuthorities =
        AuthorityUtils.commaSeparatedStringToAuthorityList(authorityString);
    Map<String, Object> attributes = new HashMap<>();
    attributes.put("client_id", "https://foo.bar.com");
    attributes.put("sub", "https://www.jvt.me/");
    attributes.put(
        OAuth2IntrospectionClaimNames.ISSUED_AT, Instant.now().minus(1, ChronoUnit.DAYS));
    attributes.put(
        OAuth2IntrospectionClaimNames.EXPIRES_AT, Instant.now().plus(1, ChronoUnit.DAYS));

    OAuth2AuthenticatedPrincipal fakeAuthenticatedPrincipal =
        new OAuth2IntrospectionAuthenticatedPrincipal(attributes, grantedAuthorities);
    when(mockIndieAuthOpaqueTokenIntrospector.introspect(VALID_TOKEN))
        .thenReturn(fakeAuthenticatedPrincipal);
  }

  private void setInvalidPrincipal() {
    Collection<GrantedAuthority> grantedAuthorities =
        AuthorityUtils.commaSeparatedStringToAuthorityList("SCOPE_create");
    OAuth2AuthenticatedPrincipal fakeAuthenticatedPrincipal =
        new OAuth2IntrospectionAuthenticatedPrincipal(
            Map.of("client_id", "https://foo.bar.com", "sub", "https://not-jvt.me/"),
            grantedAuthorities);
    when(mockIndieAuthOpaqueTokenIntrospector.introspect(anyString()))
        .thenReturn(fakeAuthenticatedPrincipal);
  }

  private void setUpRetrieveAndUpdate() {
    TestMicroformats2Object mf2 = new TestMicroformats2Object();
    mf2.setKind(Kind.notes);
    when(micropubService.retrieve(anyString(), anyBoolean())).thenReturn(Optional.of(mf2));
    when(micropubService.performSanitisation(any())).thenReturn(mf2);
    when(micropubService.update(any(), any())).thenReturn(mf2);
  }
}
