package me.jvt.www.api.micropub.actions.concrete;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ReplaceContentPropertyActionTest {
  private ReplaceContentPropertyAction sut;
  private TestMicroformats2Object mf2;

  @BeforeEach
  void setup() {
    mf2 = new TestMicroformats2Object();
  }

  @Test
  void itSetsReplacementAsValueIfNotPresent() {
    // given

    sut = new ReplaceContentPropertyAction(SingletonListHelper.singletonList("Wibble"));
    // when
    sut.accept(mf2);

    // then
    assertThat(mf2.getProperties().getContent()).hasSize(1);
    Map<String, String> actual = mf2.getProperties().getContent().get(0);
    assertThat(actual.get("value")).isEqualTo("Wibble");
    assertThat(actual.get("html")).isEqualTo("");
  }

  @Test
  void itReplacesValueIfPresent() {
    // given
    Map<String, String> content = new HashMap<>();
    content.put("value", "removed");
    mf2.getProperties().setContent(SingletonListHelper.singletonList(content));

    sut = new ReplaceContentPropertyAction(SingletonListHelper.singletonList("Replacement"));
    // when
    sut.accept(mf2);

    // then
    assertThat(mf2.getProperties().getContent()).hasSize(1);
    Map<String, String> actual = mf2.getProperties().getContent().get(0);
    assertThat(actual.get("value")).isEqualTo("Replacement");
    assertThat(actual.get("html")).isEqualTo("");
  }
}
