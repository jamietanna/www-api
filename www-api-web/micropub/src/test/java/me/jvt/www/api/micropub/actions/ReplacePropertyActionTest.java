package me.jvt.www.api.micropub.actions;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ReplacePropertyActionTest {

  private ReplacePropertyAction sut;
  private TestMicroformats2Object mf2;

  @BeforeEach
  void setup() {
    mf2 = new TestMicroformats2Object();
  }

  @Test
  void itReplacesSingletonFieldWithSingletonValue() {
    // given
    mf2.getProperties().setName(SingletonListHelper.singletonList("wibble"));

    sut = new ReplacePropertyAction("name", Collections.singletonList("wobble"));

    // when
    sut.accept(mf2);

    // then
    assertThat(mf2.getProperties().getName()).containsExactly("wobble");
  }

  @Test
  void itReplacesSingletonFieldWithMultipleValues() {
    // given
    mf2.getProperties().setName(SingletonListHelper.singletonList("wibble"));

    sut = new ReplacePropertyAction("name", Arrays.asList("ribble", "rabble"));

    // when
    sut.accept(mf2);

    // then
    assertThat(mf2.getProperties().getName()).containsExactly("ribble", "rabble");
  }

  @Test
  void itReplacesMultipleFieldWithSingletonValue() {
    // given
    mf2.getProperties().setName(Arrays.asList("ribble", "rabble"));

    sut = new ReplacePropertyAction("name", Collections.singletonList("wobble"));

    // when
    sut.accept(mf2);

    // then
    assertThat(mf2.getProperties().getName()).containsExactly("wobble");
  }

  @Test
  void itReplacesMultipleFieldWithMultipleValues() {
    // given
    mf2.getProperties().setName(Arrays.asList("ribble", "rabble"));

    sut = new ReplacePropertyAction("name", Arrays.asList("wibble", "wobble"));

    // when
    sut.accept(mf2);

    // then
    assertThat(mf2.getProperties().getName()).containsExactly("wibble", "wobble");
  }

  @Test
  void replaceDoesNotOverwriteOtherProperties() {
    // given
    mf2.getProperties().setName(Arrays.asList("ribble", "rabble"));
    mf2.getProperties().setCategory(SingletonListHelper.singletonList("Wibble"));

    sut = new ReplacePropertyAction("name", Arrays.asList("wibble", "wobble"));

    // when
    sut.accept(mf2);

    // then
    assertThat(mf2.getProperties().getCategory()).containsExactly("Wibble");
  }

  @Test
  void replaceAddsPropertyAsListOfStringsIfDoesNotExist() {
    // given
    sut = new ReplacePropertyAction("new-value", Collections.singletonList("example"));

    // when
    sut.accept(mf2);

    // then
    List<String> actual = (List<String>) mf2.getProperties().get("new-value");
    assertThat(actual).containsExactly("example");
  }

  // TODO: what about non-Strings?
}
