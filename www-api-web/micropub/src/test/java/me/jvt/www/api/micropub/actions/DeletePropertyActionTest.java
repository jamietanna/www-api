package me.jvt.www.api.micropub.actions;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DeletePropertyActionTest {

  private DeletePropertyAction sut;
  private TestMicroformats2Object mf2;

  @BeforeEach
  void setup() {
    mf2 = new TestMicroformats2Object();
  }

  @Test
  void itDeletesPropertyIfExists() {
    // given
    mf2.getProperties().setName(SingletonListHelper.singletonList("name"));

    sut = new DeletePropertyAction("name");

    // when
    sut.accept(mf2);

    // then
    assertThat(mf2.getProperties()).hasSize(0);
  }

  @Test
  void itDeletesPropertyIfExistsAndDeletionsListIsEmpty() {
    // given
    mf2.getProperties().setName(SingletonListHelper.singletonList("name"));

    sut = new DeletePropertyAction("name", Collections.emptyList());

    // when
    sut.accept(mf2);

    // then
    assertThat(mf2.getProperties()).hasSize(0);
  }

  @Test
  void itDeletesPropertyWithMultipleValuesIfExists() {
    // given
    mf2.getProperties().setName(Arrays.asList("foo", "bar"));

    sut = new DeletePropertyAction("name");
    // when
    sut.accept(mf2);

    // then
    assertThat(mf2.getProperties()).hasSize(0);
  }

  @Test
  void itDoesNotDeleteNonExistentProperty() {
    // given
    mf2.getProperties().put("wibble", new Object());

    sut = new DeletePropertyAction("wobble");
    // when
    sut.accept(mf2);

    // then
    assertThat(mf2.getProperties()).hasSize(1);
  }

  @Test
  void itDeletesSpecificValuesIfPresent() {
    // given
    List<String> values = new ArrayList<>();
    values.add("foo");
    values.add("bar");

    mf2.getProperties().put("example", values);

    sut = new DeletePropertyAction("example", SingletonListHelper.singletonList("foo"));
    // when
    sut.accept(mf2);

    // then
    List<String> actual = (List<String>) mf2.getProperties().get("example");
    assertThat(actual).containsExactly("bar");
  }

  // TODO: RSVP
}
