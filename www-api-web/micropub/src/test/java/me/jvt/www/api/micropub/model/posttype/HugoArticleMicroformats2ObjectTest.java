package me.jvt.www.api.micropub.model.posttype;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HugoArticleMicroformats2ObjectTest {
  private static final YAMLMapper MAPPER = new YAMLMapper();
  private HugoArticleMicroformats2Object mf2;

  @Nested
  class GetKind {
    @Test
    void isArticle() {
      mf2 = new HugoArticleMicroformats2Object(MAPPER, null, null);

      assertThat(mf2.getKind()).isEqualTo(Kind.articles);
    }
  }

  @Nested
  class GetProperties {
    @Test
    void returnsFromConstructor(@Mock Properties properties) {
      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, null);

      assertThat(mf2.getProperties()).isSameAs(properties);
    }
  }

  @Nested
  class GetContext {
    @Test
    void returnsFromConstructor(@Mock Context context) {
      mf2 = new HugoArticleMicroformats2Object(MAPPER, null, context);

      assertThat(mf2.getContext()).isSameAs(context);
    }
  }

  @Nested
  class Serialize {

    private final Context context = new Context();
    private final Properties properties = new Properties();

    @BeforeEach
    void setup() {
      properties.setName(Collections.singletonList("Week Notes 21#06"));
      properties.setSummary(Collections.singletonList("What happened in the week of 2021-02-01?"));
      properties.setPublished(Collections.singletonList("2021-02-07T21:42:41+0000"));
      properties.setContent(Collections.singletonList(Collections.singletonMap("html", "")));
    }

    @Test
    void serialisesLicenseForCode() {
      properties.setCodeLicense(Collections.singletonList("foo"));
      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      assertThat(mf2.serialize()).contains("\nlicense_code: \"foo\"\n");
    }

    @Test
    void serialisesLicenseForProse() {
      properties.setProseLicense(Collections.singletonList("woo"));
      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      assertThat(mf2.serialize()).contains("\nlicense_prose: \"woo\"\n");
    }

    @Test
    void serialisesSeries() {
      properties.setSeries(Collections.singletonList("the-series"));
      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      assertThat(mf2.serialize()).contains("\nseries: \"the-series\"\n");
    }

    @Test
    void serialisesImage() {
      properties.setFeatured(Collections.singletonList("https://img"));
      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      assertThat(mf2.serialize()).contains("\nimage: \"https://img\"\n");
    }

    @Test
    void serialisesCanonicalUrl() {
      properties.setRepostOf(Collections.singletonList("https://url"));
      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      assertThat(mf2.serialize()).contains("\ncanonical_url: \"https://url\"\n");
    }

    @Test
    void serialisesSlugWhenPost() {
      context.setSlug("the-post");
      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      assertThat(mf2.serialize()).contains("\nslug: \"the-post\"\n");
    }

    @Test
    void doesNotSerialiseSlugWhenWeekNotes() {
      context.setSlug("week-notes/foo");
      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      assertThat(mf2.serialize()).doesNotContain("\nslug: ");
    }

    @Test
    void onlyHasWeekNotesPropertiesWithValueContent() {
      Map<String, String> content = new HashMap<>();
      content.put("html", "");
      content.put("value", "This is \nraw content");
      properties.setContent(Collections.singletonList(content));
      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      String expected =
          "---\n"
              + "title: \"Week Notes 21#06\"\n"
              + "description: \"What happened in the week of 2021-02-01?\"\n"
              + "date: \"2021-02-07T21:42:41+0000\"\n"
              + "---\nThis is \n"
              + "raw content";

      assertThat(mf2.serialize()).isEqualTo(expected);
    }

    @Test
    void onlyHasWeekNotesPropertiesWithHtmlContent() {
      properties.setContent(
          Collections.singletonList(
              Collections.singletonMap("html", "This is \nraw <strong>content</strong>")));
      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      String expected =
          "---\n"
              + "title: \"Week Notes 21#06\"\n"
              + "description: \"What happened in the week of 2021-02-01?\"\n"
              + "date: \"2021-02-07T21:42:41+0000\"\n"
              + "---\nThis is \n"
              + "raw <strong>content</strong>";

      assertThat(mf2.serialize()).isEqualTo(expected);
    }

    @Test
    void serialisesTheAliases() {
      properties.setContent(Collections.singletonList(Collections.singletonMap("value", "")));
      context.setAliases(Arrays.asList("fdsf", "the-tag"));

      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      assertThat(mf2.serialize()).contains("aliases:\n" + "- \"fdsf\"\n" + "- \"the-tag\"");
    }

    @Test
    void serialisesTheSyndication() {
      properties.setSyndication(Arrays.asList("fdsf", "the-tag"));

      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      assertThat(mf2.serialize()).contains("syndication:\n" + "- \"fdsf\"\n" + "- \"the-tag\"\n");
    }

    @Test
    void serialisesTheTags() {
      properties.setCategory(Arrays.asList("foo", "bar"));

      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      assertThat(mf2.serialize()).contains("tags:\n" + "- \"foo\"\n" + "- \"bar\"\n");
    }

    @Test
    void serializesDeletedWhenTrue() {
      properties.setContent(Collections.singletonList(Collections.singletonMap("value", "")));
      context.setDeleted(true);

      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      assertThat(mf2.serialize()).contains("deleted: true");
    }

    @Test
    void doesNotSerializesDeletedWhenFalse() {
      properties.setContent(Collections.singletonList(Collections.singletonMap("value", "")));
      context.setDeleted(false);

      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      assertThat(mf2.serialize()).doesNotContain("deleted:");
    }

    @Test
    void doesNotSerializeDraftWhenPublished() {
      properties.setContent(Collections.singletonList(Collections.singletonMap("value", "")));
      properties.setPostStatus(Collections.singletonList("draft"));

      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      assertThat(mf2.serialize()).contains("draft: true");
    }

    @Test
    void doesNotSerializeDraftWhenNotSetWhenPublished() {
      properties.setContent(Collections.singletonList(Collections.singletonMap("value", "")));
      properties.setPostStatus(Collections.singletonList("published"));

      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      assertThat(mf2.serialize()).doesNotContain("draft:");
    }

    @Test
    void doesNotSerializeDraftWhenNotSet() {
      properties.setContent(Collections.singletonList(Collections.singletonMap("value", "")));

      mf2 = new HugoArticleMicroformats2Object(MAPPER, properties, context);

      assertThat(mf2.serialize()).doesNotContain("draft:");
    }
  }

  private static String frontMatter(String body) {
    return "---\n" + body + "---\n";
  }
}
