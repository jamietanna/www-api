package me.jvt.www.api.micropub.validator.property;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Collections;
import java.util.List;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Properties.Rsvp;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;

class RsvpPropertyValidatorTest {

  private final TestMicroformats2Object mf2 = new TestMicroformats2Object();
  private final RsvpPropertyValidator validator = new RsvpPropertyValidator();

  @ParameterizedTest
  @EnumSource(Rsvp.class)
  void allValuesAreValidAsEnum(Rsvp rsvp) {
    boolean actual = process(rsvp);

    assertThat(actual).isTrue();
  }

  @ParameterizedTest
  @EnumSource(Rsvp.class)
  void allValuesAreValidAsStrings(Rsvp rsvp) {
    boolean actual = process(rsvp.name());

    assertThat(actual).isTrue();
  }

  @Test
  void stringValuesThatAreNotInEnumAreNotValid() {
    boolean actual = process("foo");

    assertThat(actual).isFalse();
  }

  @ParameterizedTest
  @NullAndEmptySource
  void emptyOrNullRsvpPropertyIsNotValid(List<Rsvp> rsvps) {
    mf2.getProperties().put("rsvp", rsvps);

    boolean actual = validator.isValid(mf2, null);

    assertThat(actual).isFalse();
  }

  @Test
  void missingRsvpPropertyIsNotValid() {
    boolean actual = validator.isValid(mf2, null);

    assertThat(actual).isFalse();
  }

  @Test
  void unknownTypeThrowsInvalidMicropubMetadataException() {
    assertThatThrownBy(() -> process((String) null))
        .isInstanceOf(InvalidMicropubMetadataRequest.class)
        .hasMessage("The RSVP property was not populated with a valid value");
  }

  private boolean process(String rsvp) {
    mf2.getProperties().put("rsvp", Collections.singletonList(rsvp));

    return validator.isValid(mf2, null);
  }

  private boolean process(Rsvp rsvp) {
    mf2.getProperties().setRsvp(Collections.singletonList(rsvp));

    return validator.isValid(mf2, null);
  }
}
