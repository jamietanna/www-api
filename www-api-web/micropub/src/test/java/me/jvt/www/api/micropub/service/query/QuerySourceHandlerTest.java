package me.jvt.www.api.micropub.service.query;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.MicropubSourceContainerDto;
import me.jvt.www.api.micropub.model.MicropubSourceDto;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.service.MicropubService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@ExtendWith(MockitoExtension.class)
class QuerySourceHandlerTest {

  @Mock private MicropubService service;

  @InjectMocks private QuerySourceHandler handler;

  @Nested
  class Query {

    @Test
    void isCorrect() {
      assertThat(handler.getQuery()).isEqualTo("source");
    }
  }

  @Nested
  class Handle {
    @Nested
    class NoUrl {
      @Test
      void itReturnsAllPostsFromDelegate(@Mock MicropubSourceContainerDto container) {
        when(service.retrieveAll()).thenReturn(container);

        MicropubSourceContainerDto actual =
            (MicropubSourceContainerDto) handler.handle(new LinkedMultiValueMap<>(), null);

        assertThat(actual).isSameAs(container);
      }
    }

    @Nested
    class WithUrl {
      private static final String URL = "https://url";

      @BeforeEach
      void setup() {
        mf2 = new TestMicroformats2Object();
        mf2.setKind(Kind.likes);
        Properties properties = new Properties();
        mf2.setProperties(properties);

        properties.setCategory(Collections.singletonList("tag"));
        properties.setLikeOf(Collections.singletonList("https://"));
        properties.setName(Collections.singletonList("Something funny"));
      }

      private TestMicroformats2Object mf2;
      private Authentication authentication;

      @ParameterizedTest
      @NullAndEmptySource
      void butWhenIsNullOrEmptyThrows(String url) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.put("url", Collections.singletonList(url));
        assertThatThrownBy(() -> handler.handle(params, Optional.empty()))
            .isInstanceOf(InvalidMicropubMetadataRequest.class)
            .hasMessageContaining("URL parameter required to have a value");
      }

      @Test
      void emptyAuthenticationDoesNotAllowClientToViewDeleted() {
        when(service.retrieve(any(), anyBoolean())).thenReturn(Optional.of(mf2));

        handler.handle(params(), Optional.empty());

        verify(service).retrieve(URL, false);
      }

      @Test
      void itRetrievesThatPostFromDelegateWhenHasUndeleteScope() {
        when(service.retrieve(any(), anyBoolean())).thenReturn(Optional.of(mf2));
        setScope("SCOPE_undelete");

        handler.handle(params(), Optional.of(authentication));

        verify(service).retrieve(URL, true);
      }

      @Test
      void itRetrievesThatPostFromDelegateWhenDoesNotHaveUndeleteScope() {
        when(service.retrieve(any(), anyBoolean())).thenReturn(Optional.of(mf2));
        setScope("SCOPE_delete");

        handler.handle(params(), Optional.of(authentication));

        verify(service).retrieve(URL, false);
      }

      @Test
      void itRetrievesThatPostFromDelegateWhenDoesNotHaveScopes() {
        when(service.retrieve(any(), anyBoolean())).thenReturn(Optional.of(mf2));
        setNoScopes();

        handler.handle(params(), Optional.of(authentication));

        verify(service).retrieve(URL, false);
      }

      @Test
      void itThrowsWhenPostDoesNotExist() {
        when(service.retrieve(any(), anyBoolean())).thenReturn(Optional.empty());
        setNoScopes();

        assertThatThrownBy(() -> handler.handle(params(), Optional.of(authentication)))
            .isInstanceOf(InvalidMicropubMetadataRequest.class)
            .hasMessageContaining("Post " + URL + " does not exist");
      }

      @Nested
      class WhenNoProperties {
        @Test
        void itReturnsTypeFromKind() {
          when(service.retrieve(any(), anyBoolean())).thenReturn(Optional.of(mf2));
          setNoScopes();

          MicropubSourceDto actual =
              (MicropubSourceDto) handler.handle(params(), Optional.of(authentication));

          assertThat(actual.getType()).containsExactly("h-entry");
        }

        @Test
        void itReturnsProperties() {
          when(service.retrieve(any(), anyBoolean())).thenReturn(Optional.of(mf2));
          setNoScopes();

          MicropubSourceDto actual =
              (MicropubSourceDto) handler.handle(params(), Optional.of(authentication));

          assertThat(actual.getProperties().getCategory()).containsExactly("tag");
          assertThat(actual.getProperties().getName()).containsExactly("Something funny");
          assertThat(actual.getProperties().getLikeOf()).containsExactly("https://");
        }

        @ParameterizedTest
        @NullAndEmptySource
        void whenContentIsObjectWithNullOrEmptyHtml(String html) {
          Map<String, String> contentMap = new HashMap<>();
          contentMap.put("value", "foo");
          contentMap.put("html", html);
          mf2.getProperties().setContent(Collections.singletonList(contentMap));

          MultiValueMap<String, String> params = params();
          params.put("properties", Collections.singletonList("content"));

          when(service.retrieve(any(), anyBoolean())).thenReturn(Optional.of(mf2));
          setNoScopes();

          MicropubSourceDto actual =
              (MicropubSourceDto) handler.handle(params(), Optional.of(authentication));

          List<String> content = (List<String>) actual.getProperties().get("content");
          assertThat(content).containsExactly("foo");
        }
      }

      @Nested
      class WhenProperties {

        @BeforeEach
        void setup() {
          setNoScopes();
          when(service.retrieve(any(), anyBoolean())).thenReturn(Optional.of(mf2));
        }

        @Test
        void typeIsIgnored() {
          MultiValueMap<String, String> params = params();
          params.put("properties", Collections.singletonList("category"));

          MicropubSourceDto actual =
              (MicropubSourceDto) handler.handle(params, Optional.of(authentication));

          assertThat(actual.getType()).isNull();
        }

        @Test
        void whenSinglePropertyItReturnsThatProperty() {
          MultiValueMap<String, String> params = params();
          params.put("properties", Collections.singletonList("category"));

          MicropubSourceDto actual =
              (MicropubSourceDto) handler.handle(params, Optional.of(authentication));

          assertThat(actual.getProperties()).containsOnlyKeys("category");
          assertThat(actual.getProperties().getCategory()).containsExactly("tag");
        }

        @Test
        void whenSinglePropertyWithMultipleValuesOnlyFirstIsRespected() {
          MultiValueMap<String, String> params = params();
          params.put("properties", Arrays.asList("category", "foo"));

          MicropubSourceDto actual =
              (MicropubSourceDto) handler.handle(params, Optional.of(authentication));

          assertThat(actual.getProperties()).containsOnlyKeys("category");
        }

        @Test
        void nullPropertyValuesAreIgnored() {
          MultiValueMap<String, String> params = params();
          params.put("properties[]", Arrays.asList("category", "not-set"));

          MicropubSourceDto actual =
              (MicropubSourceDto) handler.handle(params, Optional.of(authentication));

          assertThat(actual.getProperties()).containsOnlyKeys("category");
        }

        @Test
        void whenMultipleProperties() {
          MultiValueMap<String, String> params = params();
          params.put("properties[]", Arrays.asList("category", "like-of"));

          MicropubSourceDto actual =
              (MicropubSourceDto) handler.handle(params, Optional.of(authentication));

          assertThat(actual.getProperties()).containsOnlyKeys("category", "like-of");

          assertThat(actual.getProperties().getCategory()).containsExactly("tag");
          assertThat(actual.getProperties().getLikeOf()).containsExactly("https://");
        }

        @ParameterizedTest
        @NullAndEmptySource
        void whenContentIsObjectWithNullOrEmptyHtml(String html) {
          Map<String, String> contentMap = new HashMap<>();
          contentMap.put("value", "foo");
          contentMap.put("html", html);
          mf2.getProperties().setContent(Collections.singletonList(contentMap));

          MultiValueMap<String, String> params = params();
          params.put("properties", Collections.singletonList("content"));

          MicropubSourceDto actual =
              (MicropubSourceDto) handler.handle(params, Optional.of(authentication));

          List<String> content = (List<String>) actual.getProperties().get("content");
          assertThat(content).containsExactly("foo");
        }

        @Test
        void whenContentIsObjectWithNoHtmlKey() {
          mf2.getProperties()
              .setContent(Collections.singletonList(Collections.singletonMap("value", "foo")));

          MultiValueMap<String, String> params = params();
          params.put("properties", Collections.singletonList("content"));

          MicropubSourceDto actual =
              (MicropubSourceDto) handler.handle(params, Optional.of(authentication));

          List<String> content = (List<String>) actual.getProperties().get("content");
          assertThat(content).containsExactly("foo");
        }

        @Test
        void whenContentHasHtmlValue() {
          mf2.getProperties()
              .setContent(Collections.singletonList(Collections.singletonMap("html", "<html>")));

          MultiValueMap<String, String> params = params();
          params.put("properties", Collections.singletonList("content"));

          MicropubSourceDto actual =
              (MicropubSourceDto) handler.handle(params, Optional.of(authentication));

          assertThat(actual.getProperties().getContent().get(0).get("html")).isEqualTo("<html>");
          assertThat(actual.getProperties().getContent().get(0)).doesNotContainKey("value");
        }
      }

      private MultiValueMap<String, String> params() {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.put("url", Arrays.asList(URL, "ignored"));
        return params;
      }

      private void setNoScopes() {
        authentication = new TestingAuthenticationToken(null, null);
      }

      private void setScope(String scope) {
        authentication =
            new TestingAuthenticationToken(
                null, null, Collections.singletonList(new SimpleGrantedAuthority(scope)));
      }
    }
  }
}
