package me.jvt.www.api.micropub.config.argumentresolvers;

import static com.github.valfirst.slf4jtest.LoggingEvent.debug;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.valfirst.slf4jtest.TestLogger;
import com.github.valfirst.slf4jtest.TestLoggerFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import me.jvt.www.api.micropub.converter.RequestBodyConverter;
import me.jvt.www.api.micropub.model.request.MicropubRequest;
import me.jvt.www.api.micropub.model.request.MicropubRequest.MicropubAction;
import me.jvt.www.api.micropub.model.request.MicropubRequest.MicropubAction.InvalidActionException;
import me.jvt.www.api.micropub.util.RequestDebugUtil;
import me.jvt.www.servlet.MultiReadHttpServletRequestSupplier;
import me.jvt.www.servlet.ServletInputStreamReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.mock.web.DelegatingServletInputStream;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;

@ExtendWith(MockitoExtension.class)
class MicropubRequestArgumentResolverTest {

  private final MultiReadHttpServletRequestSupplier supplier =
      new MultiReadHttpServletRequestSupplier();
  private final ServletInputStreamReader servletInputStreamReader = new ServletInputStreamReader();
  private final ObjectMapper objectMapper = new ObjectMapper();
  private final TestLogger logger =
      TestLoggerFactory.getTestLogger(MicropubRequestArgumentResolver.class);

  @Mock private MethodParameter parameter;
  @Mock private ModelAndViewContainer mavContainer;
  @Mock private HttpServletRequest httpServletRequest;

  @SuppressWarnings("unused")
  @Mock
  private HttpServletResponse httpServletResponse;

  @Mock private WebDataBinderFactory binderFactory;
  @Mock private ServletWebRequest webRequest;
  @Mock private RequestBodyConverter converter;
  @Mock private MicropubRequest mockRequest;
  @Mock private RequestDebugUtil requestDebugUtil;

  private MicropubRequestArgumentResolver sut;

  private MicropubRequest actual;

  @BeforeEach
  void setup() {
    sut =
        new MicropubRequestArgumentResolver(
            objectMapper, supplier, servletInputStreamReader, converter, requestDebugUtil);
    when(webRequest.getRequest()).thenReturn(httpServletRequest);
  }

  private void setupRequestBody(JsonNode bodyNode) throws IOException {
    String body = objectMapper.writeValueAsString(bodyNode);

    when(httpServletRequest.getContentType()).thenReturn(MediaType.APPLICATION_JSON_VALUE);
    when(httpServletRequest.getInputStream())
        .thenReturn(new DelegatingServletInputStream(new ByteArrayInputStream(body.getBytes())));
  }

  @Nested
  class AsForm {
    @Captor private ArgumentCaptor<Map<String, String[]>> paramsCaptor;
    private Map<String, String[]> params;

    @BeforeEach
    void setup() {
      params = new HashMap<>();
      params.put("access_token", new String[] {"j.w.t"});
      params.put("action", new String[] {"delete"});
      params.put("h", new String[] {"wibble"});
      params.put("category[]", new String[] {"abc", "def"});

      when(httpServletRequest.getParameterMap()).thenReturn(Collections.unmodifiableMap(params));
    }

    @Nested
    class FormPost {

      @BeforeEach
      void setup() {
        when(httpServletRequest.getContentType())
            .thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        when(requestDebugUtil.formParameters(any())).thenReturn("{params}");
        when(converter.convert(any(), anyMap())).thenReturn(mockRequest);
      }

      @Test
      void itIsDebugLogged() throws Exception {
        sendRequest();

        assertThat(logger.getLoggingEvents())
            .contains(debug("Received form request with body {}", "{params}"));
      }

      @Test
      void itHandlesNotExactMediaType() {
        when(httpServletRequest.getContentType())
            .thenReturn("application/x-www-form-urlencoded; charset=UTF-8");

        assertDoesNotThrow(MicropubRequestArgumentResolverTest.this::sendRequest);
      }

      @Nested
      class WithAction {

        @BeforeEach
        void setup() throws Exception {
          sendRequest();

          verify(converter).convert(eq(MicropubAction.DELETE), paramsCaptor.capture());
        }

        @Test
        void itDelegatesSingleValueToConverter() {
          Map<String, String[]> params = paramsCaptor.getValue();
          assertThat(params).containsKey("h");
          assertThat(params.get("h")).containsExactly("wibble");
        }

        @Test
        void itDelegatesMultiValueToConverter() {
          Map<String, String[]> params = paramsCaptor.getValue();
          assertThat(params).containsKey("h");
          assertThat(params.get("category[]")).containsExactly("abc", "def");
        }

        @Test
        void itDoesNotDelegatesAccessToken() {
          Map<String, String[]> params = paramsCaptor.getValue();
          assertThat(params).doesNotContainKey("access_token");
        }

        @Test
        void itDoesNotDelegatesAction() {
          Map<String, String[]> params = paramsCaptor.getValue();
          assertThat(params).doesNotContainKey("action");
        }

        @Test
        void itReturnsFromConverter() {
          assertThat(actual).isSameAs(mockRequest);
        }
      }

      @Nested
      class WithoutAction {

        @Test
        void itDefaultsToCreate() throws Exception {
          params.remove("action");

          sendRequest();

          verify(converter).convert(eq(MicropubAction.CREATE), anyMap());
        }

        @Test
        void itReturnsFromConverter() throws Exception {
          params.remove("action");

          sendRequest();

          assertThat(actual).isSameAs(mockRequest);
        }
      }
    }

    @Nested
    class MultiPartFormData {
      @BeforeEach
      void setup() {
        when(httpServletRequest.getContentType()).thenReturn(MediaType.MULTIPART_FORM_DATA_VALUE);
        when(converter.convert(any(), anyMap())).thenReturn(mockRequest);
      }

      @Test
      void itHandlesNotExactMediaType() {
        when(httpServletRequest.getContentType()).thenReturn("multipart/form-data; charset=UTF-8");

        assertDoesNotThrow(MicropubRequestArgumentResolverTest.this::sendRequest);
      }

      @Nested
      class WithAction {

        @BeforeEach
        void setup() throws Exception {
          sendRequest();

          verify(converter).convert(eq(MicropubAction.DELETE), paramsCaptor.capture());
        }

        @Test
        void itDelegatesSingleValueToConverter() {
          Map<String, String[]> params = paramsCaptor.getValue();
          assertThat(params.get("h")).containsExactly("wibble");
        }

        @Test
        void itDelegatesMultiValueToConverter() {
          Map<String, String[]> params = paramsCaptor.getValue();
          assertThat(params.get("category[]")).containsExactly("abc", "def");
        }

        @Test
        void itDoesNotDelegatesAccessToken() {
          Map<String, String[]> params = paramsCaptor.getValue();
          assertThat(params).doesNotContainKey("access_token");
        }

        @Test
        void itDoesNotDelegatesAction() {
          Map<String, String[]> params = paramsCaptor.getValue();
          assertThat(params).doesNotContainKey("action");
        }
      }

      @Nested
      class WithoutAction {

        @Test
        void itDefaultsToCreate() throws Exception {
          params.remove("action");

          sendRequest();

          verify(converter).convert(eq(MicropubAction.CREATE), anyMap());
        }

        @Test
        void itReturnsFromConverter() throws Exception {
          params.remove("action");

          sendRequest();

          assertThat(actual).isSameAs(mockRequest);
        }
      }
    }
  }

  @Nested
  class AsJson {
    @Captor private ArgumentCaptor<JsonNode> bodyCaptor;

    @Test
    void itIsDebugLogged() throws Exception {
      when(httpServletRequest.getContentType()).thenReturn(MediaType.APPLICATION_JSON_VALUE);
      when(requestDebugUtil.json(any())).thenReturn("{b64-json}");
      setupRequestBody(objectMapper.createObjectNode());

      sendRequest();

      assertThat(logger.getLoggingEvents())
          .contains(debug("Received JSON with encoded body {}", "{b64-json}"));
    }

    @Test
    void itHandlesNotExactMediaType() throws IOException {
      when(httpServletRequest.getContentType()).thenReturn("application/json; charset=UTF-8");

      setupRequestBody(objectMapper.createObjectNode());

      assertDoesNotThrow(MicropubRequestArgumentResolverTest.this::sendRequest);
    }

    @Nested
    class Create {

      @BeforeEach
      void setup() throws Exception {
        ObjectNode properties = objectMapper.createObjectNode();
        properties.set("category", objectMapper.createArrayNode().add("foo"));

        ObjectNode body = objectMapper.createObjectNode();
        body.set("properties", properties);

        setupRequestBody(body);

        when(converter.convert(any(), any(JsonNode.class))).thenReturn(mockRequest);

        sendRequest();

        verify(converter).convert(eq(MicropubAction.CREATE), bodyCaptor.capture());
      }

      @Test
      void itReturnsFromConverter() {
        assertThat(actual).isSameAs(mockRequest);
      }

      @Test
      void itDelegatesToConverter() {
        JsonNode body = bodyCaptor.getValue();
        assertThat(body.has("properties")).isTrue();

        JsonNode properties = body.get("properties");
        assertThat(properties.get("category").get(0).asText()).isEqualTo("foo");
      }

      @Test
      void itDoesNotDelegatesAction() {
        JsonNode body = bodyCaptor.getValue();
        assertThat(body.has("action")).isFalse();
      }
    }

    @Nested
    class Delete {
      @BeforeEach
      void setup() throws Exception {
        ObjectNode body =
            objectMapper.createObjectNode().put("action", "delete").put("url", "https://something");

        setupRequestBody(body);

        when(converter.convert(any(), any(JsonNode.class))).thenReturn(mockRequest);

        sendRequest();

        verify(converter).convert(eq(MicropubAction.DELETE), bodyCaptor.capture());
      }

      @Test
      void itReturnsFromConverter() {
        assertThat(actual).isSameAs(mockRequest);
      }

      @Test
      void itDelegatesToConverter() {
        JsonNode body = bodyCaptor.getValue();
        assertThat(body.get("url").asText()).isEqualTo("https://something");
      }

      @Test
      void itDelegatesActionToConverter() {
        JsonNode body = bodyCaptor.getValue();
        assertThat(body.has("action")).isTrue();
      }
    }

    @Nested
    class Undelete {
      @BeforeEach
      void setup() throws Exception {
        ObjectNode body =
            objectMapper
                .createObjectNode()
                .put("action", "undelete")
                .put("url", "https://something");

        setupRequestBody(body);

        when(converter.convert(any(), any(JsonNode.class))).thenReturn(mockRequest);

        sendRequest();

        verify(converter).convert(eq(MicropubAction.UNDELETE), bodyCaptor.capture());
      }

      @Test
      void itReturnsFromConverter() {
        assertThat(actual).isSameAs(mockRequest);
      }

      @Test
      void itDelegatesToConverter() {
        JsonNode body = bodyCaptor.getValue();
        assertThat(body.get("url").asText()).isEqualTo("https://something");
      }

      @Test
      void itDelegatesActionToConverter() {
        JsonNode body = bodyCaptor.getValue();
        assertThat(body.has("action")).isTrue();
      }
    }

    @Nested
    class Update {

      @BeforeEach
      void setup() throws Exception {
        ObjectNode body =
            objectMapper.createObjectNode().put("action", "update").put("url", "https://something");

        setupRequestBody(body);

        when(converter.convert(any(), any(JsonNode.class))).thenReturn(mockRequest);

        sendRequest();

        verify(converter).convert(eq(MicropubAction.UPDATE), bodyCaptor.capture());
      }

      @Test
      void itReturnsFromConverter() {
        assertThat(actual).isSameAs(mockRequest);
      }

      @Test
      void itDelegatesUrlToConverter() {
        JsonNode body = bodyCaptor.getValue();
        assertThat(body.has("url")).isTrue();
      }

      @Test
      void itDelegatesActionToConverter() {
        JsonNode body = bodyCaptor.getValue();
        assertThat(body.has("action")).isTrue();
      }
    }
  }

  @Nested
  class Unknown {

    @Test
    void itThrowsInvalidActionExceptionWhenInvalidAction() throws Exception {
      ObjectNode body = objectMapper.createObjectNode().put("action", "INVALID_ACTION");

      setupRequestBody(body);
      assertThatThrownBy(MicropubRequestArgumentResolverTest.this::sendRequest)
          .isInstanceOf(InvalidActionException.class)
          .hasMessage("Action provided is not understood by this server");
    }

    @Test
    void itThrowsIllegalStateExceptionWhenInvalidContentType() {
      when(httpServletRequest.getContentType()).thenReturn(MediaType.TEXT_HTML_VALUE);

      assertThatThrownBy(MicropubRequestArgumentResolverTest.this::sendRequest)
          .isInstanceOf(IllegalStateException.class)
          .hasMessage("Content type provided was not supported");
    }
  }

  private void sendRequest() throws Exception {
    actual =
        (MicropubRequest) sut.resolveArgument(parameter, mavContainer, webRequest, binderFactory);
  }
}
