package me.jvt.www.api.micropub.model;

import static org.assertj.core.api.Assertions.assertThat;

import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class QueryPropertiesDtoTest {
  @Nested
  class Property {
    @Test
    void copiesNameFromProperty() {
      QueryPropertiesDto.Property actual =
          new QueryPropertiesDto.Property(MicroformatsProperty.NAME);

      assertThat(actual.getName()).isEqualTo("name");
    }

    @Test
    void copiesDisplayNameFromProperty() {
      QueryPropertiesDto.Property actual =
          new QueryPropertiesDto.Property(MicroformatsProperty.NAME);

      assertThat(actual.getDisplayName()).isEqualTo("Title");
    }

    @Test
    void copiesHintsFromProperty() {
      QueryPropertiesDto.Property actual =
          new QueryPropertiesDto.Property(MicroformatsProperty.SYNDICATION);

      assertThat(actual.getHints()).containsExactly("multivalue", "url");
    }

    @Test
    void rsvpSetsOptions() {
      QueryPropertiesDto.Property actual =
          new QueryPropertiesDto.Property(MicroformatsProperty.RSVP);

      assertThat(actual.getOptions()).isNotNull();
      assertThat(actual.getOptions()).containsExactlyInAnyOrder("yes", "no", "maybe", "interested");
    }

    @Test
    void rsvpSetsDefault() {
      QueryPropertiesDto.Property actual =
          new QueryPropertiesDto.Property(MicroformatsProperty.RSVP);

      assertThat(actual.getDefault()).isNotNull();
      assertThat(actual.getDefault()).isEqualTo("yes");
    }

    @Test
    void postStatusSetsOptions() {
      QueryPropertiesDto.Property actual =
          new QueryPropertiesDto.Property(MicroformatsProperty.POST_STATUS);

      assertThat(actual.getDefault()).isEqualTo("published");
    }

    @Test
    void postStatusSetsDefault() {
      QueryPropertiesDto.Property actual =
          new QueryPropertiesDto.Property(MicroformatsProperty.POST_STATUS);

      assertThat(actual.getDefault()).isEqualTo("published");
    }

    @Test
    void readStatusSetsOptions() {
      QueryPropertiesDto.Property actual =
          new QueryPropertiesDto.Property(MicroformatsProperty.READ_STATUS);

      assertThat(actual.getOptions()).isNotNull();
      assertThat(actual.getOptions()).containsExactlyInAnyOrder("to-read", "reading", "finished");
    }

    @Test
    void readSetsDefault() {
      QueryPropertiesDto.Property actual =
          new QueryPropertiesDto.Property(MicroformatsProperty.READ_STATUS);

      assertThat(actual.getDefault()).isNotNull();
      assertThat(actual.getDefault()).isEqualTo("finished");
    }
  }
}
