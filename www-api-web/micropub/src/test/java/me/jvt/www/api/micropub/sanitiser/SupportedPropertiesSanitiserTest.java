package me.jvt.www.api.micropub.sanitiser;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SupportedPropertiesSanitiserTest {

  private final TestMicroformats2Object mf2 = new TestMicroformats2Object();

  private final SupportedPropertiesSanitiser sanitiser = new SupportedPropertiesSanitiser();

  @Test
  void itRemovesPropertiesNotSupported(
      @Mock Kind kind,
      @Mock MicroformatsProperty required0,
      @Mock MicroformatsProperty required1,
      @Mock MicroformatsProperty optional0,
      @Mock MicroformatsProperty optional1) {
    mf2.setKind(kind);
    when(kind.getRequiredProperties()).thenReturn(Arrays.asList(required0, required1));
    when(kind.getOptionalProperties()).thenReturn(Arrays.asList(optional0, optional1));

    when(required0.getName()).thenReturn("req0");
    when(required1.getName()).thenReturn("req1");
    when(optional0.getName()).thenReturn("opt0");
    when(optional1.getName()).thenReturn("opt1");

    mf2.getProperties().put("req0", Collections.singletonList("foo"));
    mf2.getProperties().put("req1", Collections.singletonList("foo"));
    mf2.getProperties().put("opt0", Collections.singletonList("foo"));
    mf2.getProperties().put("opt1", Collections.singletonList("foo"));
    mf2.getProperties().put("invalid", Collections.emptyList());
    mf2.getProperties().put("also-to-remove", Collections.emptyList());

    Microformats2Object actual = sanitiser.sanitise(mf2);

    assertThat(actual.getProperties()).containsOnlyKeys("req0", "req1", "opt0", "opt1");
  }

  @Test
  void itHandlesMissingProperties(
      @Mock Kind kind, @Mock MicroformatsProperty required0, @Mock MicroformatsProperty required1) {
    mf2.setKind(kind);
    when(kind.getRequiredProperties()).thenReturn(Arrays.asList(required0, required1));

    when(required0.getName()).thenReturn("req0");
    when(required1.getName()).thenReturn("req1");

    mf2.getProperties().put("req0", Collections.singletonList("foo"));

    Microformats2Object actual = sanitiser.sanitise(mf2);

    assertThat(actual.getProperties()).containsOnlyKeys("req0");
  }

  @Test
  void itHasKind() {
    mf2.setKind(Kind.articles);

    Microformats2Object actual = sanitiser.sanitise(mf2);

    assertThat(actual.getKind()).isEqualTo(Kind.articles);
  }

  @Test
  void itHasContext(@Mock Microformats2Object.Context context) {
    mf2.setKind(Kind.articles);
    mf2.setContext(context);

    Microformats2Object actual = sanitiser.sanitise(mf2);

    assertThat(actual.getContext()).isSameAs(context);
  }
}
