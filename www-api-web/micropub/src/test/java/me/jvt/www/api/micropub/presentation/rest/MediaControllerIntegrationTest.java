package me.jvt.www.api.micropub.presentation.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.startsWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import me.jvt.www.api.micropub.exception.InvalidMediaException;
import me.jvt.www.api.micropub.service.MicropubMediaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionClaimNames;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.multipart.MultipartFile;

@AutoConfigureMockMvc
@SpringBootTest
class MediaControllerIntegrationTest {
  private static final String VALID_TOKEN = "VALID_TOKEN";
  private static final String VALID_AUTHORIZATION_HEADER = "Bearer " + VALID_TOKEN;

  @MockBean private MicropubMediaService mockMicropubMediaService;

  @MockBean private OpaqueTokenIntrospector mockIndieAuthOpaqueTokenIntrospector;

  @Autowired private MockMvc mockMvc;

  @Nested
  class Rest {

    @BeforeEach
    void setUp() {
      Mockito.clearInvocations(mockMicropubMediaService, mockIndieAuthOpaqueTokenIntrospector);
    }

    @Nested
    class SadPath {

      @Test
      void accessTokenCannotBeProvidedInBodyAndAuthorizationHeader() throws Exception {
        mockMvc
            .perform(
                (multipart("/micropub/media")
                    .file(mockMultipartFile())
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)
                    .param("access_token", VALID_TOKEN)))
            // then
            .andExpect(MockMvcResultMatchers.status().isBadRequest());
        // TODO: closer validation
      }

      @Test
      void mediaEndpointReturnsBadRequestIfNotImageType() throws Exception {
        setupSecurity("SCOPE_media");
        when(mockMicropubMediaService.validateAndSave(any()))
            .thenThrow(new InvalidMediaException("Invalid media type provided"));
        MockMultipartFile mockMultipartFile =
            new MockMultipartFile("file", "test.txt", "text/plain", "test data".getBytes());

        // when
        mockMvc
            .perform(
                (multipart("/micropub/media").file(mockMultipartFile))
                    .header("Authorization", VALID_AUTHORIZATION_HEADER))
            // then
            .andExpect(MockMvcResultMatchers.status().isBadRequest())
            .andExpect(jsonPath("$.error").value("invalid_request"))
            .andExpect(jsonPath("$.error_description").value("Invalid media type provided"));
      }

      @Test
      void mediaEndpointReturnsUnauthorizedAndInsufficientScopeIfNotMediaScope() throws Exception {
        // given
        setupSecurity("SCOPE_create");

        // when
        mockMvc
            .perform(
                (multipart("/micropub/media").file(mockMultipartFile()))
                    .header("Authorization", VALID_AUTHORIZATION_HEADER))
            // then
            .andExpect(MockMvcResultMatchers.status().isUnauthorized())
            .andExpect(jsonPath("$.error").value("insufficient_scope"));
      }

      @Test
      void isRejectedWith401InsufficientScopeIfExpectedScopeNotPresent() throws Exception {
        setupSecurity("SCOPE__NOT__VALID");

        mockMvc
            .perform(
                (multipart("/micropub/media")
                    .file(mockMultipartFile())
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)))
            // then
            .andExpect(MockMvcResultMatchers.status().isUnauthorized())
            .andExpect(jsonPath("$.error").value("insufficient_scope"));
      }

      @Test
      void isRejectedWith401UnauthorizedIfNoTokenProvided() throws Exception {
        mockMvc
            .perform((multipart("/micropub/media").file(mockMultipartFile())))
            // then
            .andExpect(MockMvcResultMatchers.status().isUnauthorized())
            .andExpect(jsonPath("$.error").value("unauthorized"));
      }

      @Test
      void isRejectedWith403ForbiddenIfNotMyProfile() throws Exception {
        setInvalidPrincipal();

        mockMvc
            .perform(
                (multipart("/micropub/media")
                    .file(mockMultipartFile())
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)))
            // then
            .andExpect(MockMvcResultMatchers.status().isForbidden())
            .andExpect(jsonPath("$.error").value("forbidden"));
      }
    }

    @Nested
    class HappyPath {
      @BeforeEach
      void setup() {
        setupSecurity("SCOPE_media");
        when(mockMicropubMediaService.validateAndSave(any())).thenReturn("/the/file");
      }

      @Test
      void accessTokenCanBeInAuthorizationHeader() throws Exception {
        mockMvc
            .perform(
                (multipart("/micropub/media")
                    .file(mockMultipartFile())
                    .header("Authorization", VALID_AUTHORIZATION_HEADER)))
            .andExpect(MockMvcResultMatchers.status().isCreated());
      }

      @Test
      void accessTokenCanBeInBody() throws Exception {
        mockMvc
            .perform(
                (multipart("/micropub/media")
                    .file(mockMultipartFile())
                    .param("access_token", VALID_TOKEN)))
            .andExpect(MockMvcResultMatchers.status().isCreated());
      }

      @Test
      void mediaEndpointIgnoresOtherFiles() throws Exception {
        // given

        // when
        mockMvc
            .perform(
                (multipart("/micropub/media")
                        .file(mockMultipartFile())
                        .file(mockMultipartFile("not-file")))
                    .header("Authorization", VALID_AUTHORIZATION_HEADER))
            .andExpect(MockMvcResultMatchers.status().isCreated());
        // then
        ArgumentCaptor<MultipartFile> multipartFileArgumentCaptor =
            ArgumentCaptor.forClass(MultipartFile.class);
        verify(mockMicropubMediaService).validateAndSave(multipartFileArgumentCaptor.capture());

        MultipartFile actual = multipartFileArgumentCaptor.getValue();
        assertThat(actual).isNotNull();
        assertThat(actual.getBytes()).isEqualTo("test data".getBytes());
        assertThat(actual.getContentType()).isEqualTo("image/jpeg");
        assertThat(actual.getName()).isEqualTo("file");
      }

      @Test
      void mediaEndpointRequiresMediaScope() throws Exception {
        // given

        // when
        mockMvc
            .perform(
                (multipart("/micropub/media")
                        .file(mockMultipartFile())
                        .file(mockMultipartFile("not-file")))
                    .header("Authorization", VALID_AUTHORIZATION_HEADER))
            // then
            .andExpect(MockMvcResultMatchers.status().isCreated());
      }
    }
  }

  @Nested
  class Html {
    @Test
    void itReturnsHtmlResponse() throws Exception {
      setupSecurity("SCOPE_media");
      when(mockMicropubMediaService.validateAndSave(any())).thenReturn("/the/file");

      mockMvc
          .perform(
              (multipart("/micropub/media")
                  .file(mockMultipartFile())
                  .header("Authorization", VALID_AUTHORIZATION_HEADER)
                  .accept(MediaType.TEXT_HTML)))
          .andExpect(MockMvcResultMatchers.status().isOk())
          .andExpect(
              MockMvcResultMatchers.header()
                  .string(HttpHeaders.CONTENT_TYPE, startsWith(MediaType.TEXT_HTML_VALUE)));
    }
  }

  private MockMultipartFile mockMultipartFile() {
    return mockMultipartFile("file");
  }

  private MockMultipartFile mockMultipartFile(String name) {
    return new MockMultipartFile(name, "test.jpg", "image/jpeg", "test data".getBytes());
  }

  private void setupSecurity(String authorityString) {
    Collection<GrantedAuthority> grantedAuthorities =
        AuthorityUtils.commaSeparatedStringToAuthorityList(authorityString);
    Map<String, Object> attributes = new HashMap<>();
    attributes.put("client_id", "https://foo.bar.com");
    attributes.put("sub", "https://www.jvt.me/");
    attributes.put(
        OAuth2IntrospectionClaimNames.ISSUED_AT, Instant.now().minus(1, ChronoUnit.DAYS));
    attributes.put(
        OAuth2IntrospectionClaimNames.EXPIRES_AT, Instant.now().plus(1, ChronoUnit.DAYS));

    OAuth2AuthenticatedPrincipal fakeAuthenticatedPrincipal =
        new OAuth2IntrospectionAuthenticatedPrincipal(attributes, grantedAuthorities);
    when(mockIndieAuthOpaqueTokenIntrospector.introspect(VALID_TOKEN))
        .thenReturn(fakeAuthenticatedPrincipal);
  }

  private void setInvalidPrincipal() {
    Collection<GrantedAuthority> grantedAuthorities =
        AuthorityUtils.commaSeparatedStringToAuthorityList("SCOPE_create");
    OAuth2AuthenticatedPrincipal fakeAuthenticatedPrincipal =
        new OAuth2IntrospectionAuthenticatedPrincipal(
            Map.of("client_id", "https://foo.bar.com", "sub", "https://not-jvt.me/"),
            grantedAuthorities);
    when(mockIndieAuthOpaqueTokenIntrospector.introspect(anyString()))
        .thenReturn(fakeAuthenticatedPrincipal);
  }
}
