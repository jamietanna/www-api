package me.jvt.www.api.micropub.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.multipart.MultipartFile;

class RequestDebugUtilTest {

  private final ObjectMapper objectMapper = new ObjectMapper();

  private RequestDebugUtil sut;

  @BeforeEach
  void setup() {
    sut = new RequestDebugUtil(objectMapper);
  }

  @Test
  void formParametersOutputsParameters() {
    // given
    Map<String, String[]> formParameters = new HashMap<>();
    formParameters.put("category[]", new String[] {"food", "chocolate"});

    // when
    String actual = sut.formParameters(formParameters);

    // then
    assertThat(actual).isEqualTo("{category[]=[food, chocolate]}");
  }

  @Test
  void formParametersSanitisesAccessTokenField() {
    // given
    Map<String, String[]> formParameters = new HashMap<>();
    formParameters.put("category[]", new String[] {"food", "chocolate"});
    formParameters.put("access_token", new String[] {"eyJ..."});

    // when
    String actual = sut.formParameters(formParameters);

    // then
    assertThat(actual).startsWith("{");
    assertThat(actual).contains("category[]=[food, chocolate]");
    assertThat(actual).contains("access_token=[####]");
    assertThat(actual).endsWith("}");
  }

  @Test
  void itSerialisesJsonAndBase64Encodes() {
    JsonNode node = objectMapper.createObjectNode();

    String actual = sut.json(node);

    assertThat(actual).isEqualTo("e30=");
  }

  @Test
  void mediaEndpointReturnsDetailsFromTheFile() {
    // given
    MultipartFile mockMultipartFile = mock(MultipartFile.class);
    when(mockMultipartFile.getName()).thenReturn("form-param-name");
    when(mockMultipartFile.getOriginalFilename()).thenReturn("wibble.jpg");
    when(mockMultipartFile.getContentType()).thenReturn("image/jpeg");

    // when
    String actual = sut.mediaEndpoint(mockMultipartFile);

    // then
    assertThat(actual)
        .isEqualTo("{name=form-param-name, originalFilename=wibble.jpg, contentType=image/jpeg}");
  }
}
