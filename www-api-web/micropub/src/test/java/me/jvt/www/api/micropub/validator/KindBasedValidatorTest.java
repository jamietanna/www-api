package me.jvt.www.api.micropub.validator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collections;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;
import me.jvt.www.api.micropub.model.posttype.MicroformatsPropertyValidatorsFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class KindBasedValidatorTest {
  @Mock private ConstraintValidator<Annotation, Microformats2Object> validator0;
  @Mock private ConstraintValidator<Annotation, Microformats2Object> validator1;
  @Mock private Microformats2Object mf2;
  @Mock private Kind kind;
  @Mock private MicroformatsProperty property0;
  @Mock private MicroformatsProperty property1;

  @Mock private ConstraintValidatorContext context;
  @Mock private ConstraintViolationBuilder constraintViolationBuilder;
  @Mock private MicroformatsPropertyValidatorsFactory factory;

  @InjectMocks private KindBasedValidator validator;

  void setupContext() {
    when(context.buildConstraintViolationWithTemplate(anyString()))
        .thenReturn(constraintViolationBuilder);
  }

  @Test
  void itReturnsFalseWhenNullKind() {
    boolean actual = validator.isValid(mf2, context);

    assertThat(actual).isFalse();
  }

  @Nested
  class Required {
    @BeforeEach
    void setup() {
      when(mf2.getKind()).thenReturn(kind);

      when(kind.getRequiredProperties()).thenReturn(Arrays.asList(property0, property1));
      when(factory.validatorsForProperty(property0))
          .thenReturn(Collections.singletonList(validator0));
      when(factory.validatorsForProperty(property1))
          .thenReturn(Collections.singletonList(validator1));
    }

    @Test
    void itReturnsFalseWhenAllRequiredAreFalse() {
      setupContext();

      boolean actual = validator.isValid(mf2, context);

      assertThat(actual).isFalse();
    }

    @Test
    void itReturnsFalseWhenAnyRequiredAreFalse() {
      setupContext();
      when(validator0.isValid(any(), any())).thenReturn(true);

      boolean actual = validator.isValid(mf2, context);

      assertThat(actual).isFalse();
    }

    @Test
    void itReturnsTrueWhenAllRequiredAreTrue() {
      when(validator0.isValid(any(), any())).thenReturn(true);
      when(validator1.isValid(any(), any())).thenReturn(true);

      boolean actual = validator.isValid(mf2, context);

      assertThat(actual).isTrue();
    }
  }

  @Nested
  class Optional {
    private final Properties properties = new Properties();

    @BeforeEach
    void setup() {
      when(mf2.getKind()).thenReturn(kind);

      when(mf2.getProperties()).thenReturn(properties);

      when(kind.getRequiredProperties()).thenReturn(Collections.singletonList(property0));
      when(factory.validatorsForProperty(property0))
          .thenReturn(Collections.singletonList(validator0));

      when(kind.getOptionalProperties()).thenReturn(Collections.singletonList(property1));
    }

    @Test
    void itReturnsTrueWhenRequiredAreTrueAndOptionalAreNotPresent() {
      when(validator0.isValid(any(), any())).thenReturn(true);
      when(property1.getName()).thenReturn("some-prop");

      boolean actual = validator.isValid(mf2, context);

      assertThat(actual).isTrue();
    }

    @Test
    void itReturnsFalseWhenRequiredAreTrueButOptionalIsPresentAndInvalid() {
      setupContext();

      when(validator0.isValid(any(), any())).thenReturn(true);
      when(validator1.isValid(any(), any())).thenReturn(false);
      when(factory.validatorsForProperty(property1))
          .thenReturn(Collections.singletonList(validator1));
      when(property1.getName()).thenReturn("some-prop");
      properties.put("some-prop", "example");

      boolean actual = validator.isValid(mf2, context);

      assertThat(actual).isFalse();
    }

    @Test
    void itDoesNotValidateOptionalPropertiesThatAreNull() {
      when(validator0.isValid(any(), any())).thenReturn(true);
      when(property1.getName()).thenReturn("context-value");

      validator.isValid(mf2, context);

      verifyNoInteractions(validator1);
    }
  }
}
