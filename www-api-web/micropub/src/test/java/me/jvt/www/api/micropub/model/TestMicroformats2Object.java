package me.jvt.www.api.micropub.model;

import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

public class TestMicroformats2Object implements Microformats2Object {

  private Kind kind;
  private Properties properties;
  private Context context;

  public TestMicroformats2Object() {
    properties = new Properties();
    context = new Context();
  }

  @Override
  public Kind getKind() {
    return kind;
  }

  public void setKind(Kind kind) {
    this.kind = kind;
  }

  @Override
  public Properties getProperties() {
    return properties;
  }

  public void setProperties(Properties properties) {
    this.properties = properties;
  }

  @Override
  public Context getContext() {
    return context;
  }

  public void setContext(Context context) {
    this.context = context;
  }

  @Override
  public String serialize() {
    return "Not Implemented";
  }
}
