package me.jvt.www.api.micropub.decorator;

import static org.assertj.core.api.Assertions.assertThat;

import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.MicropubSyndicateTo;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class TwitterSyndicationDecoratorTest {

  private TestMicroformats2Object mf2;
  private TwitterSyndicationDecorator sut;

  @BeforeEach
  void setup() {
    mf2 = new TestMicroformats2Object();

    sut = new TwitterSyndicationDecorator();
  }

  @Test
  void itAddsTwitterSyndicationIfNote() {
    // given
    mf2.setKind(Kind.notes);
    mf2.getProperties().setSyndication(null);

    // when
    Microformats2Object actual = sut.decorate(mf2);

    // then
    assertThat(actual.getProperties().getSyndication())
        .containsExactly(MicropubSyndicateTo.BRIDGY_TWITTER.getUid());
  }

  @Test
  void itAddsTwitterSyndicationIfPhoto() {
    // given
    mf2.setKind(Kind.photos);
    mf2.getProperties().setSyndication(null);

    // when
    Microformats2Object actual = sut.decorate(mf2);

    // then
    assertThat(actual.getProperties().getSyndication())
        .containsExactly(MicropubSyndicateTo.BRIDGY_TWITTER.getUid());
  }

  @Test
  void itAddsTwitterSyndicationIfReplyToTwitterCom() {
    // given
    mf2.setKind(Kind.replies);
    mf2.getProperties()
        .setInReplyTo(SingletonListHelper.singletonList("https://twitter.com/status"));
    mf2.getProperties().setSyndication(null);

    // when
    Microformats2Object actual = sut.decorate(mf2);

    // then
    assertThat(actual.getProperties().getSyndication())
        .containsExactly(MicropubSyndicateTo.BRIDGY_TWITTER.getUid());
  }

  @ParameterizedTest
  @ValueSource(strings = {"https://jvt.me/mf2/", "https://www.twitter.com"})
  void itDoesNotAddTwitterSyndicationIfNotReplyToTwitterCom(String inReplyTo) {
    // given
    mf2.setKind(Kind.replies);
    mf2.getProperties().setInReplyTo(SingletonListHelper.singletonList(inReplyTo));
    mf2.getProperties().setSyndication(null);

    // when
    Microformats2Object actual = sut.decorate(mf2);

    // then
    assertThat(actual.getProperties().getSyndication()).isNull();
  }

  @Test
  void itAddsTwitterSyndicationIfRepostOfTwitterCom() {
    // given
    mf2.setKind(Kind.reposts);
    mf2.getProperties()
        .setRepostOf(SingletonListHelper.singletonList("https://twitter.com/status"));
    mf2.getProperties().setSyndication(null);

    // when
    Microformats2Object actual = sut.decorate(mf2);

    // then
    assertThat(actual.getProperties().getSyndication())
        .containsExactly(MicropubSyndicateTo.BRIDGY_TWITTER.getUid());
  }

  @ParameterizedTest
  @ValueSource(strings = {"https://jvt.me/mf2/", "https://www.twitter.com"})
  void itDoesNotAddTwitterSyndicationIfNotRepostOfTwitterCom(String repostOf) {
    // given
    mf2.setKind(Kind.reposts);
    mf2.getProperties().setRepostOf(SingletonListHelper.singletonList(repostOf));
    mf2.getProperties().setSyndication(null);

    // when
    Microformats2Object actual = sut.decorate(mf2);

    // then
    assertThat(actual.getProperties().getSyndication()).isNull();
  }

  @Test
  void itAddsTwitterSyndicationIfLikeOfTwitterCom() {
    // given
    mf2.setKind(Kind.likes);
    mf2.getProperties().setLikeOf(SingletonListHelper.singletonList("https://twitter.com/status"));
    mf2.getProperties().setSyndication(null);

    // when
    Microformats2Object actual = sut.decorate(mf2);

    // then
    assertThat(actual.getProperties().getSyndication())
        .containsExactly(MicropubSyndicateTo.BRIDGY_TWITTER.getUid());
  }

  @ParameterizedTest
  @ValueSource(strings = {"https://jvt.me/mf2/", "https://www.twitter.com"})
  void itDoesNotAddTwitterSyndicationIfNotLikeOfTwitterCom(String likeOf) {
    // given
    mf2.setKind(Kind.likes);
    mf2.getProperties().setLikeOf(SingletonListHelper.singletonList(likeOf));
    mf2.getProperties().setSyndication(null);

    // when
    Microformats2Object actual = sut.decorate(mf2);

    // then
    assertThat(actual.getProperties().getSyndication()).isNull();
  }

  @Test
  void itAppendsSyndicationIfAlreadySet() {
    // given
    mf2.setKind(Kind.notes);
    mf2.getProperties()
        .setSyndication(SingletonListHelper.singletonList(MicropubSyndicateTo.INDIENEWS.getUid()));

    // when
    Microformats2Object actual = sut.decorate(mf2);

    // then
    assertThat(actual.getProperties().getSyndication())
        .containsExactlyInAnyOrder(
            MicropubSyndicateTo.INDIENEWS.getUid(), MicropubSyndicateTo.BRIDGY_TWITTER.getUid());
  }
}
