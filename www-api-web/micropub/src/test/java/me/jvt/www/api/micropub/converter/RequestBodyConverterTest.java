package me.jvt.www.api.micropub.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.jvt.www.api.micropub.actions.Action;
import me.jvt.www.api.micropub.actions.ActionParser;
import me.jvt.www.api.micropub.actions.DeletePostAction;
import me.jvt.www.api.micropub.actions.UndeletePostAction;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.request.MicropubRequest;
import me.jvt.www.api.micropub.model.request.MicropubRequest.MicropubAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RequestBodyConverterTest {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  @Mock private ActionParser actionParser;

  private MicropubRequest actual;

  private RequestBodyConverter sut;

  @BeforeEach
  void setup() {
    sut = new RequestBodyConverter(OBJECT_MAPPER, actionParser);
  }

  @Nested
  class Create {
    private final MicropubAction ACTION = MicropubAction.CREATE;

    @Nested
    class Parameters {
      @BeforeEach
      void setup() {
        Map<String, String[]> map = new HashMap<>();

        map.put("h", arr("entry"));
        map.put("mp-syndicate-to", arr("https://go-here", "https://and-here"));
        map.put("category[]", arr("git", "ruby"));
        map.put("content", arr("hello world"));

        actual = sut.convert(ACTION, map);
      }

      @Test
      void itResolvesSyndication() {
        Properties properties = actual.getContext().getProperties();
        assertThat(properties.getSyndication())
            .containsExactly("https://go-here", "https://and-here");
      }

      @Test
      void itResolvesMultiValueParameters() {
        Properties properties = actual.getContext().getProperties();
        assertThat(properties.getCategory()).containsExactly("git", "ruby");
      }

      @Test
      void itSetsH() {
        String h = actual.getContext().getContext().getH();

        assertThat(h).isEqualTo("h-entry");
      }

      @Test
      void areIgnoredWhenEmptyString() {
        Map<String, String[]> body = new HashMap<>();

        body.put("h", arr("entry"));
        body.put("bookmark-of", arr(""));
        body.put("content", arr(""));
        body.put("mp-syndicate-to", arr(""));

        actual = sut.convert(ACTION, body);

        Properties properties = actual.getContext().getProperties();
        assertThat(properties).doesNotContainKey("bookmark-of");
        assertThat(properties).doesNotContainKey("content");
        assertThat(properties).doesNotContainKey("syndication");
      }

      @Nested
      class Content {
        @Test
        void asText() {
          Properties properties = actual.getContext().getProperties();
          List<Map<String, String>> content = properties.getContent();
          assertThat(content.get(0).get("value")).isEqualTo("hello world");
        }

        @Test
        void asHtml() {
          Map<String, String[]> body = new HashMap<>();

          body.put("h", arr("entry"));
          body.put("content[html]", arr("<h1>Hi</h1>"));

          actual = sut.convert(ACTION, body);

          Properties properties = actual.getContext().getProperties();
          List<Map<String, String>> content = properties.getContent();
          assertThat(content.get(0).get("html")).isEqualTo("<h1>Hi</h1>");
        }
      }

      @Nested
      class Photo {
        @Test
        void withAlt() {
          Map<String, String[]> map = new HashMap<>();

          map.put("h", arr("entry"));
          map.put("photo[]", arr("https://url"));
          map.put("mp-photo-alt[]", arr("alt"));

          actual = sut.convert(ACTION, map);

          List<Map<String, String>> photo = actual.getContext().getProperties().getPhoto();

          assertThat(photo.get(0).get("photo")).isEqualTo("https://url");
          assertThat(photo.get(0).get("alt")).isEqualTo("alt");
        }

        @Test
        void withMultipleAlt() {
          Map<String, String[]> map = new HashMap<>();

          map.put("h", arr("entry"));
          map.put("photo[]", arr("https://url", "https://url2"));
          map.put("mp-photo-alt[]", arr("alt", "alt2"));

          actual = sut.convert(ACTION, map);

          List<Map<String, String>> photo = actual.getContext().getProperties().getPhoto();

          assertThat(photo.get(0).get("photo")).isEqualTo("https://url");
          assertThat(photo.get(0).get("alt")).isEqualTo("alt");

          assertThat(photo.get(1).get("photo")).isEqualTo("https://url2");
          assertThat(photo.get(1).get("alt")).isEqualTo("alt2");
        }

        @Test
        void withoutAlt() {
          Map<String, String[]> map = new HashMap<>();

          map.put("h", arr("entry"));
          map.put("photo[]", arr("https://url", "https://url2"));

          actual = sut.convert(ACTION, map);

          List<Map<String, String>> photo = actual.getContext().getProperties().getPhoto();

          assertThat(photo.get(0).get("photo")).isEqualTo("https://url");
          assertThat(photo.get(0)).doesNotContainKey("alt");

          assertThat(photo.get(1).get("photo")).isEqualTo("https://url2");
          assertThat(photo.get(1)).doesNotContainKey("alt");
        }

        @Test
        void whenAltBeforePhoto() {
          Map<String, String[]> map = new HashMap<>();

          map.put("h", arr("entry"));
          map.put("mp-photo-alt[]", arr("alt", "alt2"));
          map.put("photo[]", arr("https://url", "https://url2"));

          actual = sut.convert(ACTION, map);

          List<Map<String, String>> photo = actual.getContext().getProperties().getPhoto();

          assertThat(photo.get(0).get("photo")).isEqualTo("https://url");
          assertThat(photo.get(0).get("alt")).isEqualTo("alt");

          assertThat(photo.get(1).get("photo")).isEqualTo("https://url2");
          assertThat(photo.get(1).get("alt")).isEqualTo("alt2");
        }

        @Test
        void isNotSetIfNoPhoto() {
          Map<String, String[]> map = new HashMap<>();
          map.put("h", arr("entry"));

          actual = sut.convert(ACTION, map);

          assertThat(actual.getContext().getProperties().getPhoto()).isNull();
        }

        @ParameterizedTest
        @NullAndEmptySource
        void isNotSetIfPhotoNotActuallySet(String photo) {
          Map<String, String[]> map = new HashMap<>();

          map.put("h", arr("entry"));
          map.put("photo[]", arr(photo));

          actual = sut.convert(ACTION, map);

          assertThat(actual.getContext().getProperties().getPhoto()).isNull();
        }
      }

      @Nested
      class Slug {

        @Test
        void itDoesNotResolveIfNotSet() {
          Map<String, String[]> map = new HashMap<>();
          actual = sut.convert(ACTION, map);

          Microformats2Object.Context properties = actual.getContext().getContext();
          assertThat(properties.getSlug()).isNull();
        }

        @Test
        void itDoesNotResolveIfEmpty() {
          Map<String, String[]> map = new HashMap<>();
          map.put("mp-slug", arr(""));
          actual = sut.convert(ACTION, map);

          Microformats2Object.Context properties = actual.getContext().getContext();
          assertThat(properties.getSlug()).isNull();
        }

        @Test
        void itResolvesSlugIfSingleton() {
          Map<String, String[]> map = new HashMap<>();
          map.put("mp-slug", arr("the-slug-here"));
          actual = sut.convert(ACTION, map);

          Microformats2Object.Context properties = actual.getContext().getContext();
          assertThat(properties.getSlug()).isEqualTo("the-slug-here");
        }

        @Test
        void itResolvesLastItem() {
          Map<String, String[]> map = new HashMap<>();
          map.put("mp-slug", arr("first", "last"));
          actual = sut.convert(ACTION, map);

          Microformats2Object.Context properties = actual.getContext().getContext();
          assertThat(properties.getSlug()).isEqualTo("last");
        }
      }
    }

    @Nested
    class Json {
      @BeforeEach
      void setup() {
        JsonNode type = OBJECT_MAPPER.createArrayNode().add("h-entry");
        ObjectNode properties = OBJECT_MAPPER.createObjectNode();
        properties.set("content", OBJECT_MAPPER.createArrayNode().add("hello world"));
        properties.set("category", OBJECT_MAPPER.createArrayNode().add("foo").add("bar"));

        ObjectNode photo = OBJECT_MAPPER.createObjectNode();
        photo.put("value", "https://photos.example.com/globe.gif");
        photo.put("alt", "Spinning globe animation");
        properties.set("photo", OBJECT_MAPPER.createArrayNode().add(photo));

        ObjectNode body = OBJECT_MAPPER.createObjectNode().set("type", type);
        body.set("properties", properties);

        actual = sut.convert(ACTION, body);
      }

      @Test
      void itResolvesCategory() {
        Properties properties = actual.getContext().getProperties();

        assertThat(properties.getCategory()).containsExactlyInAnyOrder("foo", "bar");
      }

      @Test
      void itSetsH() {
        String h = actual.getContext().getContext().getH();

        assertThat(h).isEqualTo("h-entry");
      }

      @Nested
      class Content {

        @Test
        void asArray() {
          Properties properties = actual.getContext().getProperties();

          List<Map<String, String>> content = properties.getContent();
          assertThat(content.get(0).get("value")).isEqualTo("hello world");
        }

        @Test
        void asObject() {
          ObjectNode body = bodyWithContentObject();

          actual = sut.convert(ACTION, body);

          List<Map<String, String>> actualContent =
              actual.getContext().getProperties().getContent();
          assertThat(actualContent.get(0).get("value")).isEqualTo("<h1>Object</h1>");
        }
      }

      @Nested
      class Photo {

        @Test
        void withAlt() {
          Properties properties = actual.getContext().getProperties();

          List<Map<String, String>> photo = properties.getPhoto();
          assertThat(photo).hasSize(1);
          assertThat(photo.get(0).get("photo")).isEqualTo("https://photos.example.com/globe.gif");
          assertThat(photo.get(0).get("alt")).isEqualTo("Spinning globe animation");
        }

        @Test
        void withoutAlt() {
          ObjectNode body = bodyWithNoPhotoAlt();

          actual = sut.convert(ACTION, body);

          Properties properties = actual.getContext().getProperties();

          List<Map<String, String>> photo = properties.getPhoto();
          assertThat(photo).hasSize(1);
          assertThat(photo.get(0).get("photo")).isEqualTo("https://photos.example.com/globe.gif");
          assertThat(photo.get(0)).doesNotContainKey("alt");
        }
      }

      private ObjectNode bodyWithContentObject() {
        JsonNode type = OBJECT_MAPPER.createArrayNode().add("h-entry");
        ObjectNode properties = OBJECT_MAPPER.createObjectNode();
        ObjectNode content = OBJECT_MAPPER.createObjectNode().put("html", "<h1>Object</h1>");
        properties.set("content", OBJECT_MAPPER.createArrayNode().add(content));

        ObjectNode body = OBJECT_MAPPER.createObjectNode().set("type", type);
        body.set("properties", properties);
        return body;
      }

      private ObjectNode bodyWithNoPhotoAlt() {
        JsonNode type = OBJECT_MAPPER.createArrayNode().add("h-entry");
        ObjectNode properties = OBJECT_MAPPER.createObjectNode();

        properties.set(
            "photo", OBJECT_MAPPER.createArrayNode().add("https://photos.example.com/globe.gif"));

        ObjectNode body = OBJECT_MAPPER.createObjectNode().set("type", type);
        body.set("properties", properties);
        return body;
      }
    }
  }

  @Nested
  class Delete {
    private final MicropubAction ACTION = MicropubAction.DELETE;

    @Nested
    class FormPost {
      @BeforeEach
      void setup() {
        Map<String, String[]> map = new HashMap<>();

        map.put("url", arr("https://something"));

        actual = sut.convert(ACTION, map);
      }

      @Test
      void itResolvesUrl() {
        assertThat(actual.getContext().get("url")).isEqualTo("https://something");
      }

      @Test
      void itResolvesAction() {
        assertThat(actual.getAction()).isEqualTo(MicropubAction.DELETE);
      }

      @Test
      void itResolvesActions() {
        assertThat(actual.getActions()).containsExactly(new DeletePostAction());
      }

      @Test
      void itThrowsInvalidMicropubMetadataRequestWhenUrlIsNotSet() {
        assertThatThrownBy(() -> sut.convert(ACTION, new HashMap<>()))
            .isInstanceOf(InvalidMicropubMetadataRequest.class)
            .hasMessage("URL parameter required");
      }
    }

    @Nested
    class Json {
      @BeforeEach
      void setup() {
        ObjectNode body = OBJECT_MAPPER.createObjectNode().put("url", "https://something");

        actual = sut.convert(ACTION, body);
      }

      @Test
      void itResolvesUrl() {
        assertThat(actual.getContext().get("url")).isEqualTo("https://something");
      }

      @Test
      void itResolvesAction() {
        assertThat(actual.getAction()).isEqualTo(MicropubAction.DELETE);
      }

      @Test
      void itResolvesActions() {
        assertThat(actual.getActions()).containsExactly(new DeletePostAction());
      }

      @Test
      void itThrowsInvalidMicropubMetadataRequestWhenUrlIsNotSet() {
        ObjectNode body = OBJECT_MAPPER.createObjectNode();

        assertThatThrownBy(() -> sut.convert(ACTION, body))
            .isInstanceOf(InvalidMicropubMetadataRequest.class)
            .hasMessage("URL parameter required");
      }
    }
  }

  @Nested
  class Undelete {
    private final MicropubAction ACTION = MicropubAction.UNDELETE;

    @Nested
    class FormPost {
      @BeforeEach
      void setup() {
        Map<String, String[]> map = new HashMap<>();

        map.put("url", arr("https://something"));

        actual = sut.convert(ACTION, map);
      }

      @Test
      void itResolvesAction() {
        assertThat(actual.getAction()).isEqualTo(MicropubAction.UNDELETE);
      }

      @Test
      void itResolvesUrl() {
        assertThat(actual.getContext().get("url")).isEqualTo("https://something");
      }

      @Test
      void itResolvesActions() {
        assertThat(actual.getActions()).containsExactly(new UndeletePostAction());
      }

      @Test
      void itThrowsInvalidMicropubMetadataRequestWhenUrlIsNotSet() {
        assertThatThrownBy(() -> sut.convert(ACTION, new HashMap<>()))
            .isInstanceOf(InvalidMicropubMetadataRequest.class)
            .hasMessage("URL parameter required");
      }
    }

    @Nested
    class Json {
      @BeforeEach
      void setup() {
        ObjectNode body = OBJECT_MAPPER.createObjectNode().put("url", "https://something");

        actual = sut.convert(ACTION, body);
      }

      @Test
      void itResolvesAction() {
        assertThat(actual.getAction()).isEqualTo(MicropubAction.UNDELETE);
      }

      @Test
      void itResolvesActions() {
        assertThat(actual.getActions()).containsExactly(new UndeletePostAction());
      }

      @Test
      void itResolvesUrl() {
        assertThat(actual.getContext().get("url")).isEqualTo("https://something");
      }

      @Test
      void itThrowsInvalidMicropubMetadataRequestWhenUrlIsNotSet() {
        ObjectNode body = OBJECT_MAPPER.createObjectNode();

        assertThatThrownBy(() -> sut.convert(ACTION, body))
            .isInstanceOf(InvalidMicropubMetadataRequest.class)
            .hasMessage("URL parameter required");
      }
    }
  }

  @Nested
  class Update {

    private final MicropubAction ACTION = MicropubAction.UPDATE;

    @Captor private ArgumentCaptor<Map<String, Object>> actionParserArgCaptor;
    @Mock private Action action;

    @BeforeEach
    void setup() {
      ObjectNode add = OBJECT_MAPPER.createObjectNode();
      add.set(
          "syndication",
          OBJECT_MAPPER
              .createArrayNode()
              .add(
                  "http://web.archive.org/web/20040104110725/https://aaronpk.example/2014/06/01/9/indieweb"));
      add.set("category", OBJECT_MAPPER.createArrayNode().add("micropub").add("indieweb"));

      ObjectNode delete =
          OBJECT_MAPPER
              .createObjectNode()
              .set("delete", OBJECT_MAPPER.createArrayNode().add("property"));
      delete.put("category", "indieweb");

      ObjectNode replace = OBJECT_MAPPER.createObjectNode();
      replace.set("content", OBJECT_MAPPER.createArrayNode().add("hello moon"));

      ObjectNode body = OBJECT_MAPPER.createObjectNode().put("url", "https://something");
      body.set("add", add);
      body.set("delete", delete);
      body.set("replace", replace);

      when(actionParser.parse(any())).thenReturn(Collections.singletonList(action));

      actual = sut.convert(ACTION, body);
    }

    @Test
    void itResolvesAction() {
      assertThat(actual.getAction()).isEqualTo(MicropubAction.UPDATE);
    }

    @Test
    void itResolvesUrl() {
      assertThat(actual.getContext().get("url")).isEqualTo("https://something");
    }

    @Test
    void itDelegatesToActionParser() {
      verify(actionParser).parse(actionParserArgCaptor.capture());

      Map<String, Object> args = actionParserArgCaptor.getValue();
      assertThat(args).containsKey("add");
      assertThat(args).containsKey("delete");
      assertThat(args).containsKey("replace");
    }

    @Test
    void itReturnsValueFromParser() {
      List<Action> actions = actual.getActions();
      assertThat(actions).containsExactly(action);
    }

    @Test
    void itThrowsInvalidMicropubMetadataRequestWhenUrlIsNotSet() {
      ObjectNode body = OBJECT_MAPPER.createObjectNode();

      assertThatThrownBy(() -> sut.convert(ACTION, body))
          .isInstanceOf(InvalidMicropubMetadataRequest.class)
          .hasMessage("URL parameter required");
    }
  }

  private String[] arr(String s) {
    return new String[] {s};
  }

  private String[] arr(String... s) {
    return s;
  }
}
