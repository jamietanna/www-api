package me.jvt.www.api.micropub.model.posttype;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HugoMicroformats2JsonTest {

  private final ObjectMapper objectMapper = new ObjectMapper();

  @Mock private PostTypeDiscoverer discoverer;

  private Properties properties = new Properties();
  private Microformats2Object.Context context = new Context();

  private HugoMicroformats2Json mf2;

  @BeforeEach
  void setup() {
    when(discoverer.discover(anyString(), any())).thenReturn(Kind.likes);
  }

  HugoMicroformats2Json create() {
    context.setH("h-wibble");

    return new HugoMicroformats2Json(objectMapper, discoverer, properties, context);
  }

  @Nested
  class GetKind {
    @Test
    void itDelegatesToDiscoverer() {
      mf2 = create();

      verify(discoverer).discover("h-wibble", properties);
    }

    @Test
    void itIsComputedFromPost() {
      when(discoverer.discover(anyString(), any())).thenReturn(Kind.notes);

      mf2 = create();

      assertThat(mf2.getKind()).isEqualTo(Kind.notes);
    }
  }

  @Nested
  class GetProperties {
    @Test
    void returnsFromConstructor(@Mock Properties mockProperties) {
      properties = mockProperties;

      mf2 = create();

      assertThat(mf2.getProperties()).isSameAs(mockProperties);
    }
  }

  @Nested
  class GetContext {
    @Test
    void returnsFromConstructor(@Mock Microformats2Object.Context mockContext) {
      context = mockContext;
      when(context.getH()).thenReturn("something");

      mf2 = create();

      assertThat(mf2.getContext()).isSameAs(context);
    }
  }

  @Nested
  class Serialize {
    private JsonNode parsed;

    void deserialize() throws JsonProcessingException {
      mf2 = create();
      String actual = mf2.serialize();
      parsed = objectMapper.readValue(actual, JsonNode.class);
    }

    @Test
    void itIsPrettyPrinted() {
      mf2 = create();
      String actual = mf2.serialize();

      assertThat(actual).contains("  \"deleted\"");
    }

    @Nested
    class Aliases extends ArrayValueTest {

      protected Aliases() {
        super("aliases", Arrays.asList("/something/", "/else"));
      }
    }

    @Nested
    class ClientID extends TextValueTest {

      protected ClientID() {
        super("client_id", "https://client");
      }
    }

    @Nested
    class Deleted {
      @ParameterizedTest
      @ValueSource(booleans = {true, false})
      void f(boolean bool) throws JsonProcessingException {
        context.put("deleted", bool);

        deserialize();

        BooleanNode node = (BooleanNode) parsed.get("deleted");
        assertThat(node.asBoolean()).isEqualTo(bool);
      }
    }

    @Nested
    class PostStatus {

      @ParameterizedTest
      @NullAndEmptySource
      void serializesToFalseWhenNullOrEmpty(List<String> postStatus)
          throws JsonProcessingException {
        properties.setPostStatus(postStatus);

        deserialize();

        BooleanNode node = (BooleanNode) parsed.get("draft");
        assertThat(node.asBoolean()).isFalse();
      }

      @Test
      void serializesToFalseWhenPublished() throws JsonProcessingException {
        properties.setPostStatus(Collections.singletonList(Properties.PostStatus.published.name()));

        deserialize();

        BooleanNode node = (BooleanNode) parsed.get("draft");
        assertThat(node.asBoolean()).isFalse();
      }

      @Test
      void serializesToTrueWhenDraft() throws JsonProcessingException {
        properties.setPostStatus(Collections.singletonList(Properties.PostStatus.draft.name()));

        deserialize();

        BooleanNode node = (BooleanNode) parsed.get("draft");
        assertThat(node.asBoolean()).isTrue();
      }

      @Test
      void isSerializedInProperties() throws JsonProcessingException {
        properties.setPostStatus(Collections.singletonList(Properties.PostStatus.draft.name()));

        deserialize();

        ObjectNode propertiesNode = (ObjectNode) parsed.get("properties");
        assertThat(propertiesNode.get("post-status")).containsExactly(new TextNode("draft"));
      }

      @Test
      void itHandlesStringValues() throws JsonProcessingException {
        properties.put("post-status", Collections.singletonList("draft"));

        deserialize();

        BooleanNode node = (BooleanNode) parsed.get("draft");
        assertThat(node.asBoolean()).isTrue();
      }
    }

    @Nested
    class H {
      @Test
      void itSetsFromHEntry() throws JsonProcessingException {
        deserialize();

        TextNode node = (TextNode) parsed.get("h");
        assertThat(node.textValue()).isEqualTo("h-entry");
      }

      @Test
      void itSetsFromHCard() throws JsonProcessingException {
        when(discoverer.discover(anyString(), any())).thenReturn(Kind.contacts);

        deserialize();

        TextNode node = (TextNode) parsed.get("h");
        assertThat(node.textValue()).isEqualTo("h-card");
      }
    }

    @Nested
    class Slug extends TextValueTest {

      protected Slug() {
        super("slug", "/some/url/");
      }
    }

    @Nested
    class Tags {

      @Test
      void itSerializesCategoriesInProperties() throws JsonProcessingException {
        List<String> categories = Arrays.asList("abc", "def");
        properties.setCategory(categories);

        deserialize();

        ObjectNode propertiesNode = (ObjectNode) parsed.get("properties");
        assertThat(propertiesNode.get("category"))
            .containsExactly(new TextNode("abc"), new TextNode("def"));
      }

      @Test
      void itSerializesTags() throws JsonProcessingException {
        List<String> categories = Arrays.asList("abc", "def");
        properties.setCategory(categories);

        deserialize();

        ArrayNode arrayNode = (ArrayNode) parsed.get("tags");
        assertThat(arrayNode).containsExactly(new TextNode("abc"), new TextNode("def"));
      }

      @ParameterizedTest
      @NullAndEmptySource
      void itDoesNotSerializeIfNotFound(List<String> categories) throws JsonProcessingException {
        properties.setCategory(categories);

        deserialize();

        assertThat(parsed.has("tags")).isFalse();
      }
    }

    @Nested
    class GenericProperties {
      @Test
      void itSerializesProperties() throws JsonProcessingException {
        properties.setLikeOf(Collections.singletonList("https://url"));
        HashMap<String, String> content = new HashMap<>();
        content.put("value", "the text");
        properties.setContent(Collections.singletonList(content));

        deserialize();

        ObjectNode propertiesNode = (ObjectNode) parsed.get("properties");
        assertThat(propertiesNode.get("like-of")).containsExactly(new TextNode("https://url"));
        ArrayNode contentNode = (ArrayNode) propertiesNode.get("content");
        ObjectNode innerContentNode = (ObjectNode) contentNode.get(0);
        assertThat(innerContentNode.get("value")).isEqualTo(new TextNode("the text"));
      }
    }

    @Nested
    class KindTest {
      @ParameterizedTest
      @EnumSource(Kind.class)
      void itSerialisesKind(Kind kind) throws JsonProcessingException {
        when(discoverer.discover(anyString(), any())).thenReturn(kind);

        deserialize();

        TextNode node = (TextNode) parsed.get("kind");
        assertThat(node.textValue()).isEqualTo(kind.name());
      }
    }

    class TextValueTest {
      protected final String property;
      protected final String expected;

      protected TextValueTest(String property, String expected) {
        this.property = property;
        this.expected = expected;
      }

      @Test
      @SuppressWarnings("unused")
      void itSerializesIfFound() throws JsonProcessingException {
        context.put(property, expected);

        deserialize();

        TextNode node = (TextNode) parsed.get(property);
        assertThat(node.textValue()).isEqualTo(expected);
      }

      @Test
      @SuppressWarnings("unused")
      void itDoesNotSerializeIfNotFound() throws JsonProcessingException {
        deserialize();

        assertThat(parsed.has(expected)).isFalse();
      }
    }

    class ArrayValueTest {

      private final String property;
      private final List<String> expected;
      private final JsonNode[] expectedNodes;

      protected ArrayValueTest(String property, List<String> expected) {
        this.property = property;
        this.expected = expected;
        expectedNodes = new JsonNode[expected.size()];
        for (int i = 0; i < expected.size(); i++) {
          expectedNodes[i] = new TextNode(expected.get(i));
        }
      }

      @Test
      @SuppressWarnings("unused")
      void itSerializesIfFound() throws JsonProcessingException {
        context.put(property, expected);

        deserialize();

        ArrayNode arrayNode = (ArrayNode) parsed.get(property);
        assertThat(arrayNode).containsExactly(expectedNodes);
      }

      @Test
      @SuppressWarnings("unused")
      void itDoesNotSerializeIfNotFound() throws JsonProcessingException {
        deserialize();

        assertThat(parsed.has(property)).isFalse();
      }
    }

    class JsonNodeTest {

      private final String property;
      private final JsonNode expected;

      protected JsonNodeTest(String property, JsonNode expected) {
        this.property = property;
        this.expected = expected;
      }

      @Test
      @SuppressWarnings("unused")
      void itSerializesIfFound() throws JsonProcessingException {
        context.put(property, expected);

        deserialize();

        JsonNode node = parsed.get(property);
        assertThat(node).isEqualTo(expected);
      }

      @Test
      @SuppressWarnings("unused")
      void itDoesNotSerializeIfNotFound() throws JsonProcessingException {
        deserialize();
        assertThat(parsed.has(property)).isFalse();
      }
    }
  }
}
