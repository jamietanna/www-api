package me.jvt.www.api.micropub.decorator;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collections;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.MicropubSyndicateTo;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GitHubSyndicationDecoratorTest {
  private TestMicroformats2Object mf2;
  private GitHubSyndicationDecorator sut;

  @BeforeEach
  void setup() {
    mf2 = new TestMicroformats2Object();

    sut = new GitHubSyndicationDecorator();
  }

  @Test
  void itAddsSyndicationIfReplyToGitHubUrl() {
    // given
    mf2.setKind(Kind.replies);
    mf2.getProperties()
        .setInReplyTo(SingletonListHelper.singletonList("https://github.com/jamietanna/repo"));

    // when
    sut.decorate(mf2);

    // then
    assertThat(mf2.getProperties().getSyndication())
        .containsExactly(MicropubSyndicateTo.BRIDGY_GITHUB.getUid());
  }

  @Test
  void itAddsSyndicationIfLikeOfGitHubUrl() {
    // given
    mf2.setKind(Kind.likes);
    mf2.getProperties()
        .setLikeOf(SingletonListHelper.singletonList("https://github.com/jamietanna/repo"));

    // when
    sut.decorate(mf2);

    // then
    assertThat(mf2.getProperties().getSyndication())
        .containsExactly(MicropubSyndicateTo.BRIDGY_GITHUB.getUid());
  }

  @Test
  void itAppendsSyndicationToExisting() {
    // given
    mf2.setKind(Kind.likes);
    mf2.getProperties().setSyndication(SingletonListHelper.singletonList("https://foo.bar"));
    mf2.getProperties()
        .setLikeOf(SingletonListHelper.singletonList("https://github.com/jamietanna/repo"));

    // when
    sut.decorate(mf2);

    // then
    assertThat(mf2.getProperties().getSyndication())
        .containsExactlyInAnyOrder("https://foo.bar", MicropubSyndicateTo.BRIDGY_GITHUB.getUid());
  }

  @Test
  void itDoesNotAddSyndicationIfNotGitHubUrl() {
    // given
    mf2.setKind(Kind.likes);
    mf2.getProperties()
        .setLikeOf(SingletonListHelper.singletonList("https://gitlab.com/jamietanna/repo"));
    mf2.getProperties().setSyndication(Collections.emptyList());

    // when
    sut.decorate(mf2);

    // then
    assertThat(mf2.getProperties().getSyndication())
        .doesNotContain(MicropubSyndicateTo.BRIDGY_GITHUB.getUid());
  }

  @Test
  void itReturnsSameMf2Object() {
    // given
    mf2.setKind(Kind.likes);
    mf2.getProperties()
        .setLikeOf(SingletonListHelper.singletonList("https://gitlab.com/jamietanna/repo"));

    // when
    Microformats2Object actual = sut.decorate(mf2);

    // then
    assertThat(actual).isSameAs(mf2);
  }
}
