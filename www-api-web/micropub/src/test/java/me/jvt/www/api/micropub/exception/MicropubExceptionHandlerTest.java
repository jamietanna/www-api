package me.jvt.www.api.micropub.exception;

import static com.github.valfirst.slf4jtest.LoggingEvent.warn;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.github.valfirst.slf4jtest.TestLogger;
import com.github.valfirst.slf4jtest.TestLoggerFactory;
import java.util.HashSet;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import me.jvt.www.api.micropub.model.MicropubErrorResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
class MicropubExceptionHandlerTest {
  private final TestLogger logger = TestLoggerFactory.getTestLogger(MicropubExceptionHandler.class);

  private MicropubExceptionHandler handler = new MicropubExceptionHandler();

  @Nested
  class InvalidRequests {

    @Mock private Exception e;
    private ResponseEntity<MicropubErrorResponse> actual;

    @BeforeEach
    void setup() {
      when(e.getMessage()).thenReturn("The full description");
      actual = handler.invalidRequests(e);
    }

    @Test
    void are400s() {
      assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void areInvalidRequest() {
      assertThat(actual.getBody().getError()).isEqualTo("invalid_request");
    }

    @Test
    void putsExceptionMessage() {
      assertThat(actual.getBody().getErrorDescription()).isEqualTo("The full description");
    }

    @Test
    void itLogsAsWarn() {
      assertThat(logger.getLoggingEvents())
          .contains(warn("An invalid request was rejected for reason: {}", "The full description"));
    }
  }

  @Nested
  class ConstraintViolationExceptions {

    @Mock private ConstraintViolationException e;
    @Mock private ConstraintViolation<?> violation0;
    @Mock private ConstraintViolation<?> violation1;

    private ResponseEntity<MicropubErrorResponse> actual;

    @BeforeEach
    void setup() {
      Set<ConstraintViolation<?>> violations = new HashSet<>();
      violations.add(violation0);
      violations.add(violation1);

      when(e.getConstraintViolations()).thenReturn(violations);

      when(violation0.getMessage()).thenReturn("The first");
      when(violation1.getMessage()).thenReturn("The second");

      actual = handler.constraintViolationException(e);
    }

    @Test
    void are400s() {
      assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void areInvalidRequest() {
      assertThat(actual.getBody().getError()).isEqualTo("invalid_request");
    }

    @Test
    void putsExceptionMessage() {
      assertThat(actual.getBody().getErrorDescription())
          .startsWith("Validation failed on the request: ");
      assertThat(actual.getBody().getErrorDescription()).contains("The first");
      assertThat(actual.getBody().getErrorDescription()).contains("The second");
    }

    @Test
    void itLogsAsWarn() {
      assertThat(logger.getLoggingEvents())
          .contains(
              warn(
                  "An invalid request was rejected for reason: {}",
                  "Validation failed on the request: The first | The second"));
    }
  }
}
