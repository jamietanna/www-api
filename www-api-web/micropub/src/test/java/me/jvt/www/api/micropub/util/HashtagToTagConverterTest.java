package me.jvt.www.api.micropub.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class HashtagToTagConverterTest {

  private HashtagToTagConverter sut;

  static Stream<Arguments> arguments() {
    return Stream.of(
        Arguments.of("#TechNott", "tech-nottingham"),
        Arguments.of("#IndieWeb", "indieweb"),
        Arguments.of("#IndieWebCamp", "indiewebcamp"),
        Arguments.of("#WitNotts", "wit-notts"),
        Arguments.of("#WiTNotts", "wit-notts"),
        Arguments.of("#LifeAtCapitalOne", "capital-one"),
        Arguments.of("#PHPMiNDS", "phpminds"),
        Arguments.of("#PHPMinds", "phpminds"),
        Arguments.of("#PhpMinds", "phpminds"),
        Arguments.of("#NottsJS", "notts-js"),
        Arguments.of("#NottsJs", "notts-js"),
        Arguments.of("#DevOpsNotts", "devops-notts"),
        Arguments.of("#DotNetNotts", "dotnetnotts"),
        Arguments.of("#Something", "something"),
        Arguments.of("#SomethingElse", "something-else"),
        Arguments.of("#psd2", "psd2"),
        Arguments.of("#dni", "diversity-and-inclusion"),
        Arguments.of("#FOSDEM", "fosdem"),
        Arguments.of("#DevOpsDays", "devopsdays"),
        Arguments.of("#DevOpsDaysLDN", "devopsdays-london"),
        Arguments.of("#badhashtag", "badhashtag"));
  }

  @BeforeEach
  void setup() {
    sut = new HashtagToTagConverter();
  }

  @ParameterizedTest
  @MethodSource("arguments")
  void convertConverts(String hashtag, String expectedTag) {
    assertThat(sut.convert(hashtag)).isEqualTo(expectedTag);
  }
}
