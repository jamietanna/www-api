package me.jvt.www.api.micropub.context;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Stream;
import me.jvt.www.api.micropub.client.GranaryTwitterClient;
import me.jvt.www.api.micropub.model.GranaryTwitterHentry;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Answers;

class TwitterInteractionContextRetrieverTest {

  GranaryTwitterClient mockGranaryTwitterClient;
  GranaryTwitterHentry fakeGranaryTwitterHentry;
  JsonNode mockJsonNode;
  ObjectMapper objectMapper;

  TwitterInteractionContextRetriever sut;

  private static Stream<Arguments> validMf2Types() {
    TestMicroformats2Object like = new TestMicroformats2Object();
    like.setKind(Kind.likes);
    like.getProperties()
        .setLikeOf(SingletonListHelper.singletonList("https://twitter.com/jamietanna/status/1"));

    TestMicroformats2Object reply = new TestMicroformats2Object();
    reply.setKind(Kind.replies);
    reply
        .getProperties()
        .setInReplyTo(SingletonListHelper.singletonList("https://twitter.com/jamietanna/status/2"));

    TestMicroformats2Object repost = new TestMicroformats2Object();
    repost.setKind(Kind.reposts);
    repost
        .getProperties()
        .setRepostOf(SingletonListHelper.singletonList("https://twitter.com/jamietanna/status/3"));

    return Stream.of(Arguments.of(like, "1"), Arguments.of(reply, "2"), Arguments.of(repost, "3"));
  }

  @BeforeEach
  void setup() {
    mockGranaryTwitterClient = mock(GranaryTwitterClient.class);
    objectMapper = mock(ObjectMapper.class);
    mockJsonNode = mock(JsonNode.class);

    fakeGranaryTwitterHentry = new GranaryTwitterHentry();
    fakeGranaryTwitterHentry.setItems(Collections.singletonList(mockJsonNode));

    sut = new TwitterInteractionContextRetriever(mockGranaryTwitterClient, objectMapper);
    var object = mock(ObjectNode.class, Answers.RETURNS_DEEP_STUBS);
    lenient().when(objectMapper.createObjectNode()).thenReturn(object);
  }

  @ParameterizedTest
  @MethodSource("validMf2Types")
  void itDelegatesToGranaryTwitterClientForContext(Microformats2Object mf2, String activityId) {
    // given
    when(mockGranaryTwitterClient.tweetToHentry(anyString())).thenReturn(fakeGranaryTwitterHentry);

    // when
    sut.retrieve(mf2);

    // then
    verify(mockGranaryTwitterClient).tweetToHentry(activityId);
  }

  @ParameterizedTest
  @MethodSource("validMf2Types")
  void itReturnsResultFromGranaryTwitterClient(Microformats2Object mf2) {
    // given
    when(mockGranaryTwitterClient.tweetToHentry(anyString())).thenReturn(fakeGranaryTwitterHentry);
    fakeGranaryTwitterHentry.setItems(new ArrayList<>());
    fakeGranaryTwitterHentry.getItems().add(mockJsonNode);

    // when
    var actual = sut.retrieve(mf2);

    // then
    assertThat(actual).containsExactly(container(mockJsonNode));
  }

  @Test
  void itDoesNotHandleNonTwitterInteractions() {
    // given
    TestMicroformats2Object like = new TestMicroformats2Object();
    like.setKind(Kind.likes);
    like.getProperties()
        .setLikeOf(SingletonListHelper.singletonList("https://example.com/jamietanna/status/1"));

    // when
    var actual = sut.retrieve(like);

    // then
    assertThat(actual).isEmpty();
    verifyNoInteractions(mockGranaryTwitterClient);
  }

  // only 0th

  @Test
  void itDoesNotHandleNotes() {
    // given
    TestMicroformats2Object like = new TestMicroformats2Object();
    like.setKind(Kind.notes);

    // when
    var actual = sut.retrieve(like);

    // then
    assertThat(actual).isEmpty();
    verifyNoInteractions(mockGranaryTwitterClient);
  }

  @Test
  void itReturnsAllEntriesForAGivenUrl() {
    // given
    fakeGranaryTwitterHentry = new GranaryTwitterHentry();
    fakeGranaryTwitterHentry.setItems(new ArrayList<>());
    fakeGranaryTwitterHentry.getItems().add(mockJsonNode);
    var mockJsonNode2 = mock(JsonNode.class);
    fakeGranaryTwitterHentry.getItems().add(mockJsonNode2);

    TestMicroformats2Object like = new TestMicroformats2Object();
    like.setKind(Kind.likes);
    like.getProperties()
        .setLikeOf(SingletonListHelper.singletonList("https://twitter.com/jamietanna/status/1"));

    when(mockGranaryTwitterClient.tweetToHentry(anyString())).thenReturn(fakeGranaryTwitterHentry);

    var actual = sut.retrieve(like);

    assertThat(actual).containsExactly(container(mockJsonNode, mockJsonNode2));
  }

  @Test
  void itDoesNothingIfNoMf2JsonItemsReturned() {
    // given
    fakeGranaryTwitterHentry = new GranaryTwitterHentry();
    fakeGranaryTwitterHentry.setItems(Collections.emptyList());

    TestMicroformats2Object like = new TestMicroformats2Object();
    like.setKind(Kind.likes);
    like.getProperties()
        .setLikeOf(SingletonListHelper.singletonList("https://twitter.com/jamietanna/status/1"));

    when(mockGranaryTwitterClient.tweetToHentry(anyString())).thenReturn(fakeGranaryTwitterHentry);

    // when
    var actual = sut.retrieve(like);

    // then
    assertThat(actual).isEmpty();
    verify(mockGranaryTwitterClient).tweetToHentry("1");
  }

  private JsonNode container(JsonNode... nodes) {
    var container = objectMapper.createObjectNode();
    for (var node : nodes) {
      container.putArray("items").add(node);
    }

    return container;
  }
}
