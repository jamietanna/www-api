package me.jvt.www.api.micropub.service.query;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.Set;
import me.jvt.www.api.micropub.model.MicropubConfigDto;
import me.jvt.www.api.micropub.model.MicropubSyndicateTo;
import me.jvt.www.api.micropub.service.MicropubConfigurationService;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class QuerySyndicationHandlerTest {

  @Mock private MicropubConfigurationService configuration;

  @InjectMocks private QuerySyndicationHandler handler;

  @Nested
  class Query {

    @Test
    void isCorrect() {
      assertThat(handler.getQuery()).isEqualTo("syndicate-to");
    }
  }

  @Nested
  class Handle {
    @Test
    void itReturnsFromDelegate(@Mock Set<MicropubSyndicateTo> syndication) {
      when(configuration.getSyndicationConfiguration()).thenReturn(syndication);

      MicropubConfigDto actual = (MicropubConfigDto) handler.handle(null, Optional.empty());

      assertThat(actual.getSyndicateTo()).isSameAs(syndication);
    }
  }
}
