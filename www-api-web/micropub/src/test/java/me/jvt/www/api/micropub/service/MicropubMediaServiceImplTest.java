package me.jvt.www.api.micropub.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import me.jvt.www.api.micropub.exception.InvalidMediaException;
import me.jvt.www.api.micropub.repository.MicropubPersistenceRepository;
import me.jvt.www.api.micropub.util.MediaTypeHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

class MicropubMediaServiceImplTest {

  private MediaTypeHelper mockMediaTypeHelper;
  private MicropubPersistenceRepository mockMicropubPersistenceRepository;
  private MultipartFile mockMultipartFile;

  private MicropubMediaServiceImpl sut;

  @BeforeEach
  void setup() {
    mockMediaTypeHelper = mock(MediaTypeHelper.class);
    mockMicropubPersistenceRepository = mock(MicropubPersistenceRepository.class);
    mockMultipartFile = mock(MultipartFile.class);

    when(mockMediaTypeHelper.validate(any())).thenReturn(true);
    when(mockMultipartFile.getContentType()).thenReturn("image/png");

    sut =
        new MicropubMediaServiceImpl(
            "https://foo.bar", mockMicropubPersistenceRepository, mockMediaTypeHelper);
  }

  @Test
  void validateAndSaveChecksMediaType() {
    // given

    // when
    sut.validateAndSave(mockMultipartFile);

    // then
    verify(mockMultipartFile).getContentType();
  }

  @ParameterizedTest
  @ValueSource(strings = {"image/jpeg", "image/png", "image/gif"})
  void validateAndSaveAllowsImageMediaType(String mediaType) {
    // given
    when(mockMultipartFile.getContentType()).thenReturn(mediaType);

    // when
    sut.validateAndSave(mockMultipartFile);

    // then
    // no exception
    verify(mockMultipartFile).getContentType();
  }

  @ParameterizedTest
  @ValueSource(strings = {"image.jpg", "image.jpeg", "image.png", "image.gif"})
  void
      validateAndSaveAllowsImageMediaTypeFilenameWhenNoContentTypeProvidedButFilenameAllowsInference(
          String filename) {
    // given
    when(mockMultipartFile.getContentType()).thenReturn(null);
    when(mockMultipartFile.getOriginalFilename()).thenReturn(filename);

    // when
    sut.validateAndSave(mockMultipartFile);

    // then
    // no exception
    verify(mockMultipartFile).getContentType();
    verify(mockMultipartFile).getOriginalFilename();
  }

  @Test
  void validateAndSaveAllowsImageMediaTypeFilenameWhenNoContentTypeProvidedAndRequiresContents()
      throws IOException {
    // given
    when(mockMultipartFile.getContentType()).thenReturn(null);
    when(mockMultipartFile.getOriginalFilename()).thenReturn("/not/inferrable");
    when(mockMultipartFile.getBytes())
        .thenReturn(new byte[] {(byte) 255, (byte) 216, (byte) 255, (byte) 224});

    // when
    sut.validateAndSave(mockMultipartFile);

    // then
    // no exception
    verify(mockMultipartFile).getContentType();
    verify(mockMultipartFile).getOriginalFilename();
  }

  @ParameterizedTest
  @ValueSource(strings = {"image.jpg", "image.jpeg", "image.png", "image.gif"})
  void validateAndSaveAllowsImageMediaTypeFilenameWhenContentTypeIsOctetStream(String filename) {
    // given
    when(mockMultipartFile.getContentType()).thenReturn("application/octet-stream");
    when(mockMultipartFile.getOriginalFilename()).thenReturn(filename);

    // when
    sut.validateAndSave(mockMultipartFile);

    // then
    // no exception
    verify(mockMultipartFile).getContentType();
    verify(mockMultipartFile).getOriginalFilename();
  }

  @Test
  void validateAndSaveThrowsInvalidMediaExceptionIfInvalidMediaType() {
    // given
    when(mockMediaTypeHelper.validate(any())).thenReturn(false);

    // when
    assertThatThrownBy(() -> sut.validateAndSave(mockMultipartFile))
        // then
        .isInstanceOf(InvalidMediaException.class)
        .hasMessage("Invalid media type provided");
  }

  @Test
  void validateAndSaveThrowsInvalidMediaExceptionIfNoContentTypeOrInferrableBody()
      throws IOException {
    // given
    when(mockMultipartFile.getContentType()).thenReturn(null);
    when(mockMultipartFile.getOriginalFilename()).thenReturn("");
    when(mockMultipartFile.getBytes()).thenReturn(new byte[] {});

    // when
    assertThatThrownBy(() -> sut.validateAndSave(mockMultipartFile))
        // then
        .isInstanceOf(InvalidMediaException.class)
        .hasMessage("Invalid media type provided");
  }

  @Test
  void validateAndSaveDelegatesToMicropubPersistenceRepositoryWhenValidated() {
    // given

    // when
    sut.validateAndSave(mockMultipartFile);

    // then
    verify(mockMicropubPersistenceRepository).save(mockMultipartFile, MediaType.IMAGE_PNG);
  }

  @Test
  void validateAndSaveReturnsUrlWithBaseUrl() {
    // given
    when(mockMicropubPersistenceRepository.save(any(MultipartFile.class), any()))
        .thenReturn("post/abc.jpg");

    // when
    String actual = sut.validateAndSave(mockMultipartFile);

    // then
    assertThat(actual).isEqualTo("https://foo.bar/post/abc.jpg");
  }
}
