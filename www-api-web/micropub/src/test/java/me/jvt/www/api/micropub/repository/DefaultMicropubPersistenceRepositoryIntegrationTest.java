package me.jvt.www.api.micropub.repository;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import javax.validation.ConstraintViolationException;
import me.jvt.www.api.micropub.client.GitLabPostClient;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.service.MicropubConflictService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class DefaultMicropubPersistenceRepositoryIntegrationTest {
  @SuppressWarnings("unused")
  @MockBean(name = "postClient")
  private GitLabPostClient postClient;

  @SuppressWarnings("unused")
  @MockBean(name = "mediaClient")
  private GitLabPostClient mediaClient;

  @SuppressWarnings("unused")
  @MockBean
  private MicropubConflictService micropubConflictService;

  @Autowired private MicropubPersistenceRepository repository;

  @Test
  void invalidMf2IsRejected() {
    assertThatThrownBy(() -> repository.save(new TestMicroformats2Object()))
        .isInstanceOf(ConstraintViolationException.class)
        .hasMessageContaining("The provided data did not abide by the restrictions of the Kind.");
  }
}
