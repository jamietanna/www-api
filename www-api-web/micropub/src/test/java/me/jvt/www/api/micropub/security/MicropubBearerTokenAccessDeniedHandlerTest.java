package me.jvt.www.api.micropub.security;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import java.security.Principal;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import me.jvt.www.indieauth.security.ProfileUrlValidator;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.server.resource.web.access.BearerTokenAccessDeniedHandler;

@ExtendWith(MockitoExtension.class)
class MicropubBearerTokenAccessDeniedHandlerTest {

  @Mock private BearerTokenAccessDeniedHandler mockDelegate;
  @Mock private HttpServletRequest request;
  @Mock private HttpServletResponse response;
  @Mock private AccessDeniedException accessDeniedException;
  @Mock private PrintWriter printWriter;
  @Mock private ProfileUrlValidator profileUrlValidator;
  @Mock private Principal principal;

  @InjectMocks private MicropubBearerTokenAccessDeniedHandler sut;

  @BeforeEach
  void setup() {
    when(response.getHeader(HttpHeaders.WWW_AUTHENTICATE)).thenReturn("");
  }

  @Test
  void handleDelegatesToBearerTokenAccessDeniedHandler() throws Exception {
    // given

    // when
    sut.handle(request, response, accessDeniedException);

    // then
    verify(mockDelegate).handle(request, response, accessDeniedException);
  }

  @Test
  void handleSetsMicropubCompliantInsufficientScopeIfSetInDelegate() throws Exception {
    // given
    when(response.getHeader(HttpHeaders.WWW_AUTHENTICATE))
        .thenReturn("... error=\"insufficient_scope\", ....");
    when(request.getUserPrincipal()).thenReturn(principal);
    when(profileUrlValidator.isValidProfileUrl(any())).thenReturn(true);
    when(response.getWriter()).thenReturn(printWriter);

    // when
    sut.handle(request, response, accessDeniedException);

    // then
    verify(response).setContentType("application/json");
    verify(response).setStatus(401);
    verify(printWriter)
        .println(
            "{"
                + "\"error\": \"insufficient_scope\", "
                + "\"error_description\": \"The request requires higher privileges than provided by the access token.\""
                + "}");
  }

  @Test
  void handleSetsMicropubCompliantForbiddenWhenInvalidProfileUrl() throws Exception {
    // given
    when(response.getHeader(HttpHeaders.WWW_AUTHENTICATE))
        .thenReturn("... error=\"insufficient_scope\", ....");
    when(request.getUserPrincipal()).thenReturn(principal);
    when(response.getWriter()).thenReturn(printWriter);

    // when
    sut.handle(request, response, accessDeniedException);

    // then
    verify(response).setContentType("application/json");
    verify(response).setStatus(403);
    verify(printWriter)
        .println(
            "{"
                + "\"error\": \"forbidden\", "
                + "\"error_description\": \"Profile URL for this token is not able to perform this action.\""
                + "}");
  }

  @Test
  void handleDelegatesPrincipalNameToProfileUrlValidator() throws Exception {
    // given
    when(response.getHeader(HttpHeaders.WWW_AUTHENTICATE))
        .thenReturn("... error=\"insufficient_scope\", ....");
    when(request.getUserPrincipal()).thenReturn(principal);
    when(principal.getName()).thenReturn("https://me");
    when(response.getWriter()).thenReturn(printWriter);

    // when
    sut.handle(request, response, accessDeniedException);

    // then
    verify(profileUrlValidator).isValidProfileUrl("https://me");
  }
}
