package me.jvt.www.api.micropub.service.query;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;
import me.jvt.www.api.micropub.model.MicropubContactDto;
import me.jvt.www.api.micropub.model.MicropubContactDto.MicropubContact;
import me.jvt.www.api.micropub.service.MicropubService;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class QueryContactHandlerTest {

  @Mock private MicropubService service;

  @InjectMocks private QueryContactHandler handler;

  @Nested
  class Query {

    @Test
    void isCorrect() {
      assertThat(handler.getQuery()).isEqualTo("contact");
    }
  }

  @Nested
  class Handle {
    @Test
    void itReturnsFromDelegate(@Mock MicropubContact contact) {
      when(service.retrieveContacts()).thenReturn(Collections.singletonList(contact));

      MicropubContactDto actual = (MicropubContactDto) handler.handle(null, Optional.empty());

      assertThat(actual.getContacts()).isEqualTo(Collections.singletonList(contact));
    }
  }
}
