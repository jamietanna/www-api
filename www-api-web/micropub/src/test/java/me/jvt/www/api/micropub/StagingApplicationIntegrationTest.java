package me.jvt.www.api.micropub;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = Application.class)
@AutoConfigureMockMvc
@ActiveProfiles("staging")
class StagingApplicationIntegrationTest {

  @Autowired MockMvc mockMvc;

  @Test
  void contextLoads(ApplicationContext context) {
    assertThat(context).isNotNull();
  }

  @Test
  void configEndpointReturnsProdBaseUrlForMediaEndpoint() throws Exception {
    // given

    // when
    this.mockMvc
        .perform(get("/micropub?q=config"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(
            jsonPath("$.media-endpoint").value("https://www-api.staging.jvt.me/micropub/media"));
  }
}
