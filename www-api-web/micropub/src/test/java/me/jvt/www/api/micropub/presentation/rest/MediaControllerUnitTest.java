package me.jvt.www.api.micropub.presentation.rest;

import static com.github.valfirst.slf4jtest.LoggingEvent.debug;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.github.valfirst.slf4jtest.TestLogger;
import com.github.valfirst.slf4jtest.TestLoggerFactory;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import me.jvt.www.api.micropub.service.MicropubMediaService;
import me.jvt.www.api.micropub.util.RequestDebugUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
class MediaControllerUnitTest {
  @Mock private MicropubMediaService micropubMediaService;
  @Mock private RequestDebugUtil mockRequestDebugUtil;

  @Mock private MultipartFile file;

  @InjectMocks private MediaController controller;

  private final TestLogger logger = TestLoggerFactory.getTestLogger(MediaController.class);

  @BeforeEach
  void setup() {
    when(micropubMediaService.validateAndSave(any())).thenReturn("/media.jpg");
  }

  @Test
  void itDelegatesToMediaServiceToValidateAndSave() {
    controller.mediaEndpoint(file);

    verify(micropubMediaService).validateAndSave(file);
  }

  @Nested
  class Rest {

    @Test
    void locationHeaderContainsSlug() {
      ResponseEntity<String> response = controller.mediaEndpoint(file);

      assertThat(response.getHeaders().get("Location")).containsExactly("/media.jpg");
    }

    @Test
    void itReturns201Created() {
      ResponseEntity<String> response = controller.mediaEndpoint(file);

      assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    void whenMediaEndpointItLogsMediaDetailsToDebugStream() {
      // given
      when(mockRequestDebugUtil.mediaEndpoint(any())).thenReturn("{params}");

      // when
      controller.mediaEndpoint(file);

      // then
      assertThat(logger.getLoggingEvents())
          .contains(debug("Received media endpoint request with body {}", "{params}"));
    }
  }

  @Nested
  class Html {
    @Test
    void itReturnsHtml() {
      String actual = controller.mediaEndpointAsHtml(file);

      assertThat(actual).isEqualTo(getFile());
    }

    @Test
    void whenMediaEndpointItLogsMediaDetailsToDebugStream() {
      // given
      when(mockRequestDebugUtil.mediaEndpoint(any())).thenReturn("{params}");

      // when
      controller.mediaEndpointAsHtml(file);

      // then
      assertThat(logger.getLoggingEvents())
          .contains(debug("Received media endpoint request with body {}", "{params}"));
    }
  }

  private String getFile() {
    try {
      File resource =
          new ClassPathResource(String.format("fixtures/micropub/%s", "media.html")).getFile();
      return new String(Files.readAllBytes(resource.toPath())).trim();
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }
}
