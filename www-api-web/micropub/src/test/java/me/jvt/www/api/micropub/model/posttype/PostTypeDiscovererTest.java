package me.jvt.www.api.micropub.model.posttype;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.Properties.Rsvp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;

class PostTypeDiscovererTest {

  private final Properties properties = new Properties();
  private final PostTypeDiscoverer discoverer = new PostTypeDiscoverer();

  @BeforeEach
  void setup() {
    properties.put("not-real-property", "foo");
  }

  @ParameterizedTest
  @NullAndEmptySource
  void throwsKindNotSupportedExceptionWhenNoH(String h) {
    assertThatThrownBy(() -> discoverer.discover(h, null))
        .isInstanceOf(KindNotSupportedException.class)
        .hasMessage("The `h` was not set in the request");
  }

  @Test
  void throwsKindNotSupportedExceptionWhenNullProperties() {
    assertThatThrownBy(() -> discoverer.discover("h", null))
        .isInstanceOf(KindNotSupportedException.class)
        .hasMessage("No properties could be found in the request");
  }

  @Test
  void throwsKindNotSupportedExceptionWhenEmptyProperties() {
    assertThatThrownBy(() -> discoverer.discover("h", new Properties()))
        .isInstanceOf(KindNotSupportedException.class)
        .hasMessage("No properties could be found in the request");
  }

  @Nested
  class Entry {

    @Test
    void rsvp() {
      properties.setRsvp(Collections.singletonList(Rsvp.no));

      assertThat(Kind.rsvps).isEqualTo(process());
    }

    @Test
    void reply() {
      properties.setInReplyTo(Collections.singletonList("https://foo.bar"));

      assertThat(Kind.replies).isEqualTo(process());
    }

    @Test
    void repost() {
      properties.setRepostOf(Collections.singletonList("https://foo.bar"));

      assertThat(Kind.reposts).isEqualTo(process());
    }

    @Test
    void like() {
      properties.setLikeOf(Collections.singletonList("https://foo.bar"));

      assertThat(Kind.likes).isEqualTo(process());
    }

    @Test
    void bookmark() {
      properties.setBookmarkOf(Collections.singletonList("https://foo.bar"));

      assertThat(Kind.bookmarks).isEqualTo(process());
    }

    @Test
    void videoIsDiscoveredAsNoteIfHasContent() {
      properties.setContent(
          Collections.singletonList(Collections.singletonMap("value", "something")));
      properties.put("video", Collections.singletonList("https://foo.bar"));

      assertThat(Kind.notes).isEqualTo(process());
    }

    @Test
    void photo() {
      Map<String, String> photo = new HashMap<>();
      photo.put("alt", null);
      photo.put("photo", "https://media.jvt.example/foo.jpg");
      properties.setPhoto(Collections.singletonList(photo));

      assertThat(Kind.photos).isEqualTo(process());
    }

    @Test
    void photoOnReply() {
      properties.setInReplyTo(Collections.singletonList("https://foo.bar"));
      Map<String, String> photo = new HashMap<>();
      photo.put("alt", null);
      photo.put("photo", "https://media.jvt.example/foo.jpg");
      properties.setPhoto(Collections.singletonList(photo));

      assertThat(Kind.replies).isEqualTo(process());
    }

    @Test
    void articleWhenTitleAndContent() {
      properties.setName(Collections.singletonList("The title"));
      properties.setContent(
          Collections.singletonList(Collections.singletonMap("value", "something")));

      Kind actual = discoverer.discover("h-entry", properties);

      assertThat(actual).isEqualTo(Kind.articles);
    }

    @Test
    void readsAreDiscoveredByReadOfBeingPresent() {
      properties.put("read-of", Collections.singletonList("https://foo"));

      assertThat(Kind.reads).isEqualTo(process());
    }

    @Test
    void listensAreDiscoveredByListenOfBeingPresent() {
      properties.put("listen-of", Collections.singletonList("https://foo"));

      assertThat(Kind.listens).isEqualTo(process());
    }

    @Test
    void listensHaveAName() {
      properties.put("name", Collections.singletonList("The title"));
      properties.put("listen-of", Collections.singletonList("https://foo"));

      assertThat(Kind.listens).isEqualTo(process());
    }

    @Test
    void defaultsToNoteIfContentWithNoTitle() {
      properties.setContent(
          Collections.singletonList(Collections.singletonMap("value", "something")));

      assertThat(Kind.notes).isEqualTo(process());
    }

    @Test
    void itThrowsKindNotSupportedExceptionWhen() {
      assertThatThrownBy(() -> discoverer.discover("h-entry", properties))
          .isInstanceOf(KindNotSupportedException.class)
          .hasMessage("No kind could be determined from the provided h=entry");
    }

    private Kind process() {
      return discoverer.discover("h-entry", properties);
    }
  }

  @Nested
  class Card {
    @Test
    void isDeterminedFromH() {
      Kind actual = discoverer.discover("h-card", properties);

      assertThat(actual).isEqualTo(Kind.contacts);
    }
  }

  @Nested
  class Event {
    @Test
    void isDeterminedFromH() {
      Kind actual = discoverer.discover("h-event", properties);

      assertThat(actual).isEqualTo(Kind.events);
    }
  }

  @Nested
  class Measure {
    @Test
    void stepsAreDeterminedFromUnit() {
      properties.put("unit", Collections.singletonList("steps"));

      Kind actual = discoverer.discover("h-measure", properties);

      assertThat(actual).isEqualTo(Kind.steps);
    }

    @Test
    void itThrowsKindNotSupportedExceptionWhenNotSteps() {
      properties.put("unit", Collections.singletonList("foo"));

      assertThatThrownBy(() -> discoverer.discover("h-measure", properties))
          .isInstanceOf(KindNotSupportedException.class)
          .hasMessage("Only step counts are supported");
    }
  }

  @Nested
  class Unknown {
    @Test
    void itThrowsKindNotSupportedExceptionWhenHNotMatched() {
      assertThatThrownBy(() -> discoverer.discover("h-wibble", properties))
          .isInstanceOf(KindNotSupportedException.class)
          .hasMessage("h not supported");
    }
  }
}
