package me.jvt.www.api.micropub.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.micropub.model.MicropubConfigDto;
import me.jvt.www.api.micropub.model.TaxonomiesResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class WwwProxyClientTest {

  @Mock RequestSpecificationFactory mockFactory;

  @Mock(answer = Answers.RETURNS_SELF)
  RequestSpecification mockRequestSpecification;

  @Mock Response mockResponse;

  @BeforeEach
  void setup() {
    when(mockFactory.newRequestSpecification()).thenReturn(mockRequestSpecification);

    when(mockRequestSpecification.get(anyString())).thenReturn(mockResponse);
    when(mockResponse.getStatusCode()).thenReturn(200);
  }

  @Test
  void callsPublicEndpoint() {
    // given
    when(mockResponse.as(any(Class.class))).thenReturn(new Object());
    WwwProxyClient<?> sut = new WwwProxyClient<>(Object.class, mockFactory, "/source.json");

    // when
    sut.retrieve();

    // then
    verify(mockRequestSpecification).get("https://www.jvt.me/source.json");
  }

  @Test
  void casts() {
    // given
    when(mockResponse.as(any(Class.class))).thenReturn(new Object());
    WwwProxyClient<?> sut = new WwwProxyClient<>(TaxonomiesResponse.class, mockFactory, "");

    // when
    sut.retrieve();

    // then
    verify(mockResponse).as(TaxonomiesResponse.class);
  }

  @Test
  void itThrowsRuntimeExceptionIfNot200Ok() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(400);
    WwwProxyClient<?> sut = new WwwProxyClient<>(Object.class, mockFactory, "/source.json");
    // when

    assertThatThrownBy(sut::retrieve)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Call to https://www.jvt.me/source.json failed with HTTP Status 400");
  }

  @Test
  void returnsValueAsTheClass() {
    // given
    when(mockResponse.as(any(Class.class))).thenReturn(new MicropubConfigDto());
    WwwProxyClient<MicropubConfigDto> sut =
        new WwwProxyClient<>(MicropubConfigDto.class, mockFactory, "");

    // when
    MicropubConfigDto actual = sut.retrieve();

    // then
    assertThat(actual).isNotNull();
  }
}
