package me.jvt.www.api.micropub.presentation.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.MicropubSourceDto;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.request.MicropubRequest;
import me.jvt.www.api.micropub.model.request.MicropubRequest.MicropubAction;
import me.jvt.www.api.micropub.service.MicropubRequestService;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
class MicropubControllerUnitTest {

  @Mock private MicropubRequest request;
  @Mock private MicropubRequest.RequestContext requestContext;

  @Mock private Authentication authentication;

  @Mock private Microformats2Object mf2;

  @Mock private MicropubRequestService delegate;

  private MicropubController controller;

  @BeforeEach
  void setup() {
    String micropubRedirectUrlFormat = "https://url/%s";
    controller = new MicropubController(delegate, micropubRedirectUrlFormat);
  }

  @Nested
  class MicropubRest extends MicropubRestTest {

    @Override
    ResponseEntity<?> process() {
      return controller.micropubForm(request, authentication);
    }

    @Test
    void itThrowsInvalidMicropubMetadataRequestWhenUpdateAction() {
      when(request.getAction()).thenReturn(MicropubAction.UPDATE);

      assertThatThrownBy(this::process)
          .isInstanceOf(InvalidMicropubMetadataRequest.class)
          .hasMessageContaining("Update is not supported on Micropub form requests");
    }
  }

  @Nested
  class MultiPartMicropubRest extends MicropubRestTest {

    @Override
    ResponseEntity<?> process() {
      return controller.micropubMultiPartForm(request, authentication, Optional.empty());
    }

    @Test
    void itThrowsInvalidMicropubMetadataRequestWhenFileIsProvided(@Mock MultipartFile file) {
      assertThatThrownBy(
              () -> controller.micropubMultiPartForm(request, authentication, Optional.of(file)))
          .isInstanceOf(InvalidMicropubMetadataRequest.class)
          .hasMessageContaining("Files must be uploaded to the media endpoint");
    }

    @Test
    void itThrowsInvalidMicropubMetadataRequestWhenUpdateAction() {
      when(request.getAction()).thenReturn(MicropubAction.UPDATE);

      assertThatThrownBy(this::process)
          .isInstanceOf(InvalidMicropubMetadataRequest.class)
          .hasMessageContaining("Update is not supported on Micropub form requests");
    }
  }

  @Nested
  class MicropubHtml extends MicropubHtmlTest {

    @Override
    String process() {
      return controller.micropubFormAsHtml(request, authentication);
    }
  }

  @Nested
  class MultiPartMicropubHtml extends MicropubHtmlTest {

    @Override
    String process() {
      return controller.micropubMultiPartFormAsHtml(request, authentication, Optional.empty());
    }
  }

  @Nested
  class JsonRest extends MicropubRestTest {

    @Override
    ResponseEntity<?> process() {
      return controller.json(request, authentication);
    }
  }

  @Nested
  class JsonHtml extends MicropubHtmlTest {

    @Override
    String process() {
      return controller.jsonAsHtml(request, authentication);
    }
  }

  @Nested
  class JsonRestUpdate {

    ResponseEntity<?> process() {
      return controller.json(request, authentication);
    }

    @Mock private MicropubRequestService.UpdateResponse response;

    @BeforeEach
    void setup() {
      when(request.getAction()).thenReturn(MicropubAction.UPDATE);
      Properties properties = new Properties();
      when(mf2.getKind()).thenReturn(Kind.notes);
      when(mf2.getProperties()).thenReturn(properties);
      properties.setPublished(SingletonListHelper.singletonList("the-date"));

      when(delegate.handle(any(), any())).thenReturn(response);
      when(response.getNewObject()).thenReturn(mf2);
    }

    @Test
    void itDelegates() {
      process();

      verify(delegate).handle(request, authentication);
    }

    @Test
    void itReturnsOK() {
      ResponseEntity<MicropubSourceDto> response = (ResponseEntity<MicropubSourceDto>) process();

      assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void itReturnsKindInResponse() {
      ResponseEntity<MicropubSourceDto> response = (ResponseEntity<MicropubSourceDto>) process();

      MicropubSourceDto body = response.getBody();

      assertThat(body.getType()).containsExactly("h-entry");
    }

    @Test
    void itReturnsPropertiesInResponse() {
      ResponseEntity<MicropubSourceDto> response = (ResponseEntity<MicropubSourceDto>) process();

      MicropubSourceDto body = response.getBody();

      assertThat(body.getProperties().getPublished()).containsExactly("the-date");
    }

    @ParameterizedTest
    @NullAndEmptySource
    void whenContentIsObjectWithNullOrEmptyHtml(String html) {
      Map<String, String> content = new HashMap<>();
      content.put("html", html);
      content.put("value", "text value");
      mf2.getProperties().setContent(SingletonListHelper.singletonList(content));

      ResponseEntity<MicropubSourceDto> response = (ResponseEntity<MicropubSourceDto>) process();

      MicropubSourceDto body = response.getBody();

      List<String> theContent = (List<String>) body.getProperties().get("content");
      assertThat(theContent).containsExactly("text value");
    }

    @Test
    void whenContentIsHtmlWithValue() {
      Map<String, String> content = new HashMap<>();
      content.put("html", "<html>");
      content.put("value", "text value");
      mf2.getProperties().setContent(SingletonListHelper.singletonList(content));

      ResponseEntity<MicropubSourceDto> response = (ResponseEntity<MicropubSourceDto>) process();

      MicropubSourceDto body = response.getBody();

      assertThat(body.getProperties().getContent().get(0).get("html")).isEqualTo("<html>");
      assertThat(body.getProperties().getContent().get(0)).doesNotContainKey("value");
    }

    @ParameterizedTest
    @NullAndEmptySource
    void whenContentIsObjectWithOnlyText(String html) {
      Map<String, String> content = new HashMap<>();
      content.put("html", html);
      content.put("value", "text value");
      mf2.getProperties().setContent(SingletonListHelper.singletonList(content));

      ResponseEntity<MicropubSourceDto> response = (ResponseEntity<MicropubSourceDto>) process();

      MicropubSourceDto body = response.getBody();

      List<String> theContent = (List<String>) body.getProperties().get("content");
      assertThat(theContent).containsExactly("text value");
    }
  }

  @Nested
  class JsonHtmlUpdate {

    String process() {
      return controller.jsonAsHtml(request, authentication);
    }

    @Mock private MicropubRequestService.UpdateResponse response;

    @BeforeEach
    void setup() {
      when(request.getAction()).thenReturn(MicropubAction.UPDATE);
      when(request.getContext()).thenReturn(requestContext);
      when(requestContext.getUrl()).thenReturn("/to/update/");
    }

    @Test
    void itReturnsLinkToDeletedPostInBody() {
      String response = process();

      assertThat(response).isEqualTo(getFile("update.html"));
    }
  }

  abstract class MicropubRestTest {

    abstract ResponseEntity<?> process();

    @Nested
    class Create {
      @Mock private MicropubRequestService.CreateResponse response;

      @BeforeEach
      void setup() {
        when(request.getAction()).thenReturn(MicropubAction.CREATE);
        when(delegate.handle(any(), any())).thenReturn(response);
        when(response.getSlug()).thenReturn("the/slug/");
      }

      @Test
      void itDelegates() {
        process();

        verify(delegate).handle(request, authentication);
      }

      @Test
      void locationHeaderContainsSlug() {
        ResponseEntity<?> response = process();

        assertThat(response.getHeaders().get("Location")).containsExactly("https://url/the/slug/");
      }

      @Test
      void itReturnsAccepted() {
        ResponseEntity<?> response = process();

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
      }
    }

    @Nested
    class Delete {
      @Mock private MicropubRequestService.DeleteUndeleteResponse response;

      @BeforeEach
      void setup() {
        when(request.getAction()).thenReturn(MicropubAction.DELETE);
        when(delegate.handle(any(), any())).thenReturn(response);
      }

      @Test
      void itReturnsNoContent() {
        ResponseEntity<?> response = process();

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
      }
    }

    @Nested
    class Undelete {
      @Mock private MicropubRequestService.DeleteUndeleteResponse response;

      @BeforeEach
      void setup() {
        when(request.getAction()).thenReturn(MicropubAction.DELETE);
        when(delegate.handle(any(), any())).thenReturn(response);
      }

      @Test
      void itReturnsNoContent() {
        ResponseEntity<?> response = process();

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
      }
    }
  }

  abstract class MicropubHtmlTest {

    abstract String process();

    @Nested
    class Create {
      @Mock private MicropubRequestService.CreateResponse response;

      @BeforeEach
      void setup() {
        when(request.getAction()).thenReturn(MicropubAction.CREATE);
        when(delegate.handle(any(), any())).thenReturn(response);
        when(response.getSlug()).thenReturn("the/slug/");
      }

      @Test
      void itDelegates() {
        process();

        verify(delegate).handle(request, authentication);
      }

      @Test
      void itReturnsLinkToCreatedPostInBody() {
        String response = process();

        assertThat(response).isEqualTo(getFile("create.html"));
      }
    }

    @Nested
    class Delete {
      @Mock private MicropubRequestService.DeleteUndeleteResponse response;

      @BeforeEach
      void setup() {
        when(request.getContext()).thenReturn(requestContext);
        when(requestContext.getUrl()).thenReturn("/to/delete/");
        when(request.getAction()).thenReturn(MicropubAction.DELETE);
        when(delegate.handle(any(), any())).thenReturn(response);
      }

      @Test
      void itReturnsLinkToDeletedPostInBody() {
        String response = process();

        assertThat(response).isEqualTo(getFile("delete.html"));
      }
    }

    @Nested
    class Undelete {
      @Mock private MicropubRequestService.DeleteUndeleteResponse response;

      @BeforeEach
      void setup() {
        when(request.getContext()).thenReturn(requestContext);
        when(requestContext.getUrl()).thenReturn("/to/undelete/");
        when(request.getAction()).thenReturn(MicropubAction.UNDELETE);
        when(delegate.handle(any(), any())).thenReturn(response);
      }

      @Test
      void itReturnsLinkToDeletedPostInBody() {
        String response = process();

        assertThat(response).isEqualTo(getFile("undelete.html"));
      }
    }
  }

  private String getFile(String filename) {
    try {
      File resource =
          new ClassPathResource(String.format("fixtures/micropub/%s", filename)).getFile();
      return new String(Files.readAllBytes(resource.toPath())).trim();
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }
}
