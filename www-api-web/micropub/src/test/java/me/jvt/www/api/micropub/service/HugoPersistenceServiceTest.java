package me.jvt.www.api.micropub.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.service.PersistenceService.PersistenceAction;
import me.jvt.www.api.micropub.util.RandomStringGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.EnumSource.Mode;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HugoPersistenceServiceTest {
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final String URL = "http://example.com/foo.mp4";
  @Mock private TestMicroformats2Object mf2;
  @Mock private Microformats2Object.Context context;
  @Mock private Properties properties;
  @Mock private RandomStringGenerator randomStringGenerator;
  @Mock private ObjectMapper objectMapper;
  @Mock private ObjectWriter objectWriter;

  private PersistenceAction persistenceAction;

  @InjectMocks private HugoPersistenceService service;

  @BeforeEach
  void setup() {
    lenient().when(mf2.getProperties()).thenReturn(properties);
    lenient().when(mf2.getContext()).thenReturn(context);
    lenient().when(properties.getNickname()).thenReturn(Collections.singletonList("jamietanna"));

    lenient()
        .when(mf2.getProperties().getPublished())
        .thenReturn(Collections.singletonList("2019-09-22T08:00:01.604+02:00"));
    lenient().when(randomStringGenerator.randomAlphanumeric(5)).thenReturn("ab134");
  }

  @ParameterizedTest
  @EnumSource(value = Kind.class)
  void itHandlesAllKinds(Kind kind) {
    lenient().when(mf2.getKind()).thenReturn(kind);
    lenient().when(mf2.getContext()).thenReturn(context);
    lenient().when(context.getSlug()).thenReturn("slug");

    assertThat(service.determineAction(mf2)).isNotNull();
  }

  @Nested
  class DeterminePersistenceAction {
    @Nested
    class Cite {

      @Test
      void throwsIllegalArgumentExceptionWhenNull() {
        JsonNode cite = null;
        assertThatThrownBy(() -> service.determineAction(cite))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Cite cannot be null");
      }

      @Test
      void throwsIllegalArgumentExceptionWhenArrayNode() {
        assertThatThrownBy(() -> service.determineAction(OBJECT_MAPPER.createArrayNode()))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Cite cannot be ArrayNode");
      }

      @Test
      void returnsEmptyListWhenEmptyNode() {
        JsonNode cite = OBJECT_MAPPER.createObjectNode();

        List<PersistenceAction> actions = service.determineAction(cite);

        assertThat(actions).isEmpty();
      }

      @Test
      void returnsEmptyWhenNoCites() {
        JsonNode cite = OBJECT_MAPPER.createObjectNode();
        cite.withArray("items");
        List<PersistenceAction> actions = service.determineAction(cite);

        assertThat(actions).isEmpty();
      }

      @Test
      void doesNotSetCommitMessage() {
        ObjectNode cite = OBJECT_MAPPER.createObjectNode();
        cite.putArray("items").add(node(URL));

        List<PersistenceAction> actions = service.determineAction(cite);

        assertThat(actions.get(0).getCommitMessage()).isNull();
      }

      @Test
      void sanitisesUrlWithNoPathOrTrailingSlash() {
        ObjectNode cite = OBJECT_MAPPER.createObjectNode();
        cite.putArray("items").add(node("http://example.com"));

        List<PersistenceAction> actions = service.determineAction(cite);

        assertThat(actions.get(0).getPath()).endsWith("example.com/_.json");
      }

      @Test
      void sanitisesUrlWithNoPathButWithTrailingSlash() {
        ObjectNode cite = OBJECT_MAPPER.createObjectNode();
        cite.putArray("items").add(node("http://example.com/"));

        List<PersistenceAction> actions = service.determineAction(cite);

        assertThat(actions.get(0).getPath()).endsWith("example.com/_.json");
      }

      @Test
      void sanitisesUrlWithQuery() {
        ObjectNode cite = OBJECT_MAPPER.createObjectNode();
        cite.putArray("items").add(node("http://example.com/foo.mp4?example=bar&abc=def"));

        List<PersistenceAction> actions = service.determineAction(cite);

        assertThat(actions.get(0).getPath())
            .endsWith("example.com/_foo_mp4_example_bar_abc_def.json");
      }

      @Test
      void sanitisesUrlWithFragment() {
        ObjectNode cite = OBJECT_MAPPER.createObjectNode();
        cite.putArray("items").add(node("http://example.com/foo.mp4#t=10,20"));

        List<PersistenceAction> actions = service.determineAction(cite);

        assertThat(actions.get(0).getPath()).endsWith("example.com/_foo_mp4_t_10_20.json");
      }

      @Test
      void setsPathInRepo() {
        ObjectNode cite = OBJECT_MAPPER.createObjectNode();
        cite.putArray("items").add(node(URL));

        List<PersistenceAction> actions = service.determineAction(cite);

        assertThat(actions.get(0).getPath()).startsWith("data/cites/example.com/");
      }

      @Test
      void doesNotSetSlug() {
        ObjectNode cite = OBJECT_MAPPER.createObjectNode();
        cite.putArray("items").add(node(URL));

        List<PersistenceAction> actions = service.determineAction(cite);

        assertThat(actions.get(0).getSlug()).isNull();
      }

      @Test
      void handlesMultiple() {
        ObjectNode cite = OBJECT_MAPPER.createObjectNode();
        cite.putArray("items").add(node("https://url.com")).add(node("https://foo.com"));

        List<PersistenceAction> actions = service.determineAction(cite);

        assertThat(actions).hasSize(2);
      }

      @ParameterizedTest
      @NullAndEmptySource
      void itIgnoresCitesWithNullOrEmptyUrlProperty(String url) {
        ObjectNode cite = OBJECT_MAPPER.createObjectNode();
        cite.putArray("items").add(node(URL)).add(node(url));

        List<PersistenceAction> actions = service.determineAction(cite);

        assertThat(actions).hasSize(1);
      }

      @Test
      void itIgnoresCitesWithNoProperties() {
        ObjectNode cite = OBJECT_MAPPER.createObjectNode();
        cite.putArray("items").add(OBJECT_MAPPER.createObjectNode());

        List<PersistenceAction> actions = service.determineAction(cite);

        assertThat(actions).hasSize(0);
      }
    }

    @Nested
    class Mf2 {

      @Nested
      class Contacts {

        @BeforeEach
        void setup() {
          when(mf2.getKind()).thenReturn(Kind.contacts);
          when(properties.getNickname()).thenReturn(Collections.singletonList("jamietanna"));
          persistenceAction = service.determineAction(mf2);
        }

        @Test
        void itSetsTheSlug() {
          assertThat(persistenceAction.getSlug()).isEqualTo("jamietanna");
        }

        @Test
        void itSetsTheUrlPath() {
          assertThat(persistenceAction.getUrlPath()).isEqualTo("/contacts/jamietanna/");
        }

        @Test
        void itSetsThePath() {
          assertThat(persistenceAction.getPath()).isEqualTo("content/contacts/jamietanna.md");
        }

        @Test
        void itSetsTheCommitMessage() {
          assertThat(persistenceAction.getCommitMessage()).isEqualTo("contact jamietanna");
        }
      }

      @Nested
      class Events {

        @Nested
        class WhenNoSlugInMf2 {

          @BeforeEach
          void setup() {
            when(mf2.getKind()).thenReturn(Kind.events);
            persistenceAction = service.determineAction(mf2);
          }

          @Test
          void itSetsTheSlug() {
            assertThat(persistenceAction.getSlug()).isEqualTo("2019-09-ab134");
          }

          @Test
          void itSetsTheUrlPath() {
            assertThat(persistenceAction.getUrlPath())
                .isEqualTo("/events/personal-events/2019-09-ab134/");
          }

          @Test
          void itSetsThePath() {
            assertThat(persistenceAction.getPath())
                .isEqualTo("content/events/personal-events/2019-09-ab134.md");
          }

          @Test
          void itSetsTheCommitMessage() {
            assertThat(persistenceAction.getCommitMessage()).isEqualTo("events 2019-09-ab134");
          }
        }

        @Nested
        class WhenSlugInMf2 {

          @BeforeEach
          void setup() {
            when(mf2.getKind()).thenReturn(Kind.events);
            when(mf2.getContext()).thenReturn(context);
            when(context.getSlug()).thenReturn("the-slug");
            persistenceAction = service.determineAction(mf2);
          }

          @Test
          void itSetsTheSlug() {
            assertThat(persistenceAction.getSlug()).isEqualTo("the-slug");
          }

          @Test
          void itSetsTheUrlPath() {
            assertThat(persistenceAction.getUrlPath())
                .isEqualTo("/events/personal-events/the-slug/");
          }

          @Test
          void itSetsThePath() {
            assertThat(persistenceAction.getPath())
                .isEqualTo("content/events/personal-events/the-slug.md");
          }

          @Test
          void itSetsTheCommitMessage() {
            assertThat(persistenceAction.getCommitMessage()).isEqualTo("events the-slug");
          }
        }
      }

      @Nested
      class OtherKinds {
        @Nested
        class WhenNoSlugInMf2 {

          void execute(Kind kind) {
            when(mf2.getKind()).thenReturn(kind);
            persistenceAction = service.determineAction(mf2);
          }

          @ParameterizedTest
          @EnumSource(
              value = Kind.class,
              names = {"articles", "contacts", "events"},
              mode = Mode.EXCLUDE)
          void itSetsTheSlug(Kind kind) {
            execute(kind);

            assertThat(persistenceAction.getSlug()).isEqualTo("2019/09/ab134");
          }

          @ParameterizedTest
          @EnumSource(
              value = Kind.class,
              names = {"articles", "contacts", "events"},
              mode = Mode.EXCLUDE)
          void itSetsTheUrlPath(Kind kind) {
            execute(kind);

            assertThat(persistenceAction.getUrlPath()).isEqualTo("/mf2/2019/09/ab134/");
          }

          @ParameterizedTest
          @EnumSource(
              value = Kind.class,
              names = {"articles", "contacts", "events"},
              mode = Mode.EXCLUDE)
          void itSetsThePath(Kind kind) {
            execute(kind);

            assertThat(persistenceAction.getPath()).isEqualTo("content/mf2/2019/09/ab134.md");
          }

          @ParameterizedTest
          @EnumSource(
              value = Kind.class,
              names = {"articles", "contacts", "events"},
              mode = Mode.EXCLUDE)
          void itSetsTheCommitMessage(Kind kind) {
            execute(kind);

            assertThat(persistenceAction.getCommitMessage())
                .isEqualTo(String.format("%s 2019/09/ab134", kind.toString()));
          }
        }

        @Nested
        class WhenSlugInMf2 {

          @BeforeEach
          void setup() {
            when(mf2.getContext()).thenReturn(context);
            when(context.getSlug()).thenReturn("the-slug");
          }

          void setup(Kind kind) {
            when(mf2.getKind()).thenReturn(kind);
            persistenceAction = service.determineAction(mf2);
          }

          @ParameterizedTest
          @EnumSource(
              value = Kind.class,
              names = {"articles", "contacts", "events"},
              mode = Mode.EXCLUDE)
          void itSetsTheSlug(Kind kind) {
            setup(kind);

            assertThat(persistenceAction.getSlug()).isEqualTo("the-slug");
          }

          @ParameterizedTest
          @EnumSource(
              value = Kind.class,
              names = {"articles", "contacts", "events"},
              mode = Mode.EXCLUDE)
          void itSetsTheUrlPath(Kind kind) {
            setup(kind);

            assertThat(persistenceAction.getUrlPath()).isEqualTo("/mf2/the-slug/");
          }

          @ParameterizedTest
          @EnumSource(
              value = Kind.class,
              names = {"articles", "contacts", "events"},
              mode = Mode.EXCLUDE)
          void itSetsThePath(Kind kind) {
            setup(kind);

            assertThat(persistenceAction.getPath()).isEqualTo("content/mf2/the-slug.md");
          }

          @ParameterizedTest
          @EnumSource(
              value = Kind.class,
              names = {"articles", "contacts", "events"},
              mode = Mode.EXCLUDE)
          void itSetsTheCommitMessage(Kind kind) {
            setup(kind);

            assertThat(persistenceAction.getCommitMessage())
                .isEqualTo(String.format("%s the-slug", kind.toString()));
          }
        }

        @Nested
        class DateFormats {

          @BeforeEach
          void setup() {
            when(mf2.getKind()).thenReturn(Kind.likes);
          }

          @Test
          void formatTODONAME() {
            when(mf2.getProperties().getPublished())
                .thenReturn(Collections.singletonList("2019-09-22T08:00:01.604+02:00"));

            assertDoesNotThrow(() -> service.determineAction(mf2));
          }

          @Test
          void anotherFormatTODONAME() {
            when(mf2.getProperties().getPublished())
                .thenReturn(Collections.singletonList("2020-02-25T08:20:27+0000"));

            assertDoesNotThrow(() -> service.determineAction(mf2));
          }

          @Test
          void otherwiseThrowsIllegalState() {
            when(mf2.getProperties().getPublished())
                .thenReturn(Collections.singletonList("wibble"));

            assertThatThrownBy(() -> service.determineAction(mf2))
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("Publish date wibble could not be parsed");
          }
        }
      }
    }

    @Nested
    class Article {
      @Nested
      class WhenNoSlug {
        @ParameterizedTest
        @NullAndEmptySource
        void itThrowsIllegalStateException(String slug) {
          when(mf2.getContext()).thenReturn(context);
          when(context.getSlug()).thenReturn(slug);
          when(mf2.getKind()).thenReturn(Kind.articles);

          assertThatThrownBy(() -> service.determineAction(mf2))
              .isInstanceOf(IllegalStateException.class)
              .hasMessage("Missing slug in post, something has gone wrong internally");
        }
      }

      @Nested
      class WhenSlug {
        @Nested
        class WhenPost {
          @BeforeEach
          void setup(@Mock Properties properties) {
            when(mf2.getContext()).thenReturn(context);
            when(context.getSlug()).thenReturn("the-slug");
            when(mf2.getKind()).thenReturn(Kind.articles);
            when(mf2.getProperties()).thenReturn(properties);
            when(properties.getPublished())
                .thenReturn(Collections.singletonList("2021-03-04T00:00:00+0000"));
            persistenceAction = service.determineAction(mf2);
          }

          @Test
          void itDoesNotSetTheSlug() {
            assertThat(persistenceAction.getSlug()).isNull();
          }

          @Test
          void itSetsTheUrlPath() {
            assertThat(persistenceAction.getUrlPath()).isEqualTo("/posts/2021/03/04/the-slug/");
          }

          @Test
          void itSetsThePath() {
            assertThat(persistenceAction.getPath())
                .isEqualTo("content/posts/2021-03-04-the-slug.md");
          }

          @Test
          void itSetsTheCommitMessage() {
            assertThat(persistenceAction.getCommitMessage())
                .isEqualTo("article posts/2021/03/04/the-slug");
          }
        }

        @Nested
        class WhenWeekNotes {

          @BeforeEach
          void setup() {
            when(mf2.getContext()).thenReturn(context);
            when(context.getSlug()).thenReturn("week-notes/2021/02");
            when(mf2.getKind()).thenReturn(Kind.articles);
            persistenceAction = service.determineAction(mf2);
          }

          @Test
          void itDoesNotSetTheSlug() {
            assertThat(persistenceAction.getSlug()).isNull();
          }

          @Test
          void itSetsTheUrlPath() {
            assertThat(persistenceAction.getUrlPath()).isEqualTo("/week-notes/2021/02/");
          }

          @Test
          void itSetsThePath() {
            assertThat(persistenceAction.getPath()).isEqualTo("content/week-notes/2021/02.md");
          }

          @Test
          void itSetsTheCommitMessage() {
            assertThat(persistenceAction.getCommitMessage())
                .isEqualTo("article week-notes/2021/02");
          }
        }
      }
    }
  }

  @Nested
  class Serialize {
    @Nested
    class Cite {

      @Test
      void throwsIllegalArgumentExceptionWhenNull() {
        JsonNode cite = null;
        assertThatThrownBy(() -> service.serialize(cite))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Cite cannot be null");
      }

      @Test
      void throwsIllegalArgumentExceptionWhenArrayNode() {
        assertThatThrownBy(() -> service.serialize(OBJECT_MAPPER.createArrayNode()))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Cite cannot be ArrayNode");
      }

      @Test
      void returnsEmptyListWhenEmptyNode() {
        JsonNode cite = OBJECT_MAPPER.createObjectNode();

        List<String> serializations = service.serialize(cite);

        assertThat(serializations).isEmpty();
      }

      @Test
      void returnsEmptyWhenNoCites() {
        JsonNode cite = OBJECT_MAPPER.createObjectNode();
        cite.withArray("items");
        List<String> serializations = service.serialize(cite);

        assertThat(serializations).isEmpty();
      }

      @ParameterizedTest
      @NullAndEmptySource
      void itIgnoresCitesWithNullOrEmptyUrlProperty(String url) {
        ObjectNode cite = OBJECT_MAPPER.createObjectNode();
        cite.putArray("items").add(node(url));

        List<PersistenceAction> actions = service.determineAction(cite);

        assertThat(actions).hasSize(0);
      }

      @Test
      void itIgnoresCitesWithNoProperties() {
        ObjectNode cite = OBJECT_MAPPER.createObjectNode();
        cite.putArray("items").add(OBJECT_MAPPER.createObjectNode());

        List<PersistenceAction> actions = service.determineAction(cite);

        assertThat(actions).hasSize(0);
      }

      @Test
      void itThrowsIllegalStateExceptionWhenIoExceptionOnWrite(
          @Mock JsonProcessingException exception) throws JsonProcessingException {
        when(objectMapper.writerWithDefaultPrettyPrinter()).thenReturn(objectWriter);

        when(objectWriter.writeValueAsString(any())).thenThrow(exception);

        ObjectNode cite = OBJECT_MAPPER.createObjectNode();
        cite.withArray("items").add(node(""));
        assertThatThrownBy(() -> service.serialize(cite))
            .isInstanceOf(IllegalStateException.class)
            .hasMessage("Something went wrong when trying to serialize")
            .hasCause(exception);
      }

      @Nested
      class HappyPath {

        private final ObjectNode cite = OBJECT_MAPPER.createObjectNode();

        @BeforeEach
        void setup() {
          cite.withArray("items");
          when(objectMapper.writerWithDefaultPrettyPrinter()).thenReturn(objectWriter);
        }

        @Test
        void whenOneValue() throws JsonProcessingException {
          cite.withArray("items").add(node(""));
          when(objectWriter.writeValueAsString(any())).thenReturn("serialized");

          List<String> serializations = service.serialize(cite);

          assertThat(serializations).containsExactly("serialized");
        }

        @Test
        void whenMultipleValues() throws JsonProcessingException {
          cite.withArray("items").add(node("")).add(node("")).add(node(""));
          when(objectWriter.writeValueAsString(any())).thenReturn("serialized", "again", "thrice");

          List<String> serializations = service.serialize(cite);

          assertThat(serializations).containsExactly("serialized", "again", "thrice");
        }
      }
    }

    @Nested
    class Mf2 {
      @Mock private PersistenceAction persistenceAction;

      @BeforeEach
      void setup() {
        when(mf2.serialize()).thenReturn("new-content");
      }

      String execute(Kind kind) {
        when(mf2.getKind()).thenReturn(kind);

        return service.serialize(persistenceAction, mf2);
      }

      @Nested
      class Contacts {
        @Test
        void itReturnsPrettyPrintedContent() {
          String actual = execute(Kind.contacts);

          assertThat(actual).isEqualTo("new-content");
        }
      }

      @Nested
      class Others {
        @BeforeEach
        void setup() {
          when(persistenceAction.getSlug()).thenReturn("slug");
        }

        @ParameterizedTest
        @EnumSource(
            value = Kind.class,
            names = {"articles", "contacts"},
            mode = Mode.EXCLUDE)
        void itSetsSlug(Kind kind) {
          execute(kind);

          verify(context).setSlug("slug");
        }

        @ParameterizedTest
        @EnumSource(
            value = Kind.class,
            names = {"articles", "contacts"},
            mode = Mode.EXCLUDE)
        void itReturnsPrettyPrintedContent(Kind kind) {
          String actual = execute(kind);

          assertThat(actual).isEqualTo("new-content");
        }
      }
    }
  }

  @Nested
  class ContentPathFromUrl {
    @Test
    void handlesWhenWeekNotesArticles() {
      Optional<String> optionalPath =
          service.contentPathFromUrl("https://www.jvt.me/week-notes/2021/06/");

      assertThat(optionalPath).contains("content/week-notes/2021/06.md");
    }

    @Test
    void handlesContact() {
      Optional<String> optionalPath =
          service.contentPathFromUrl("https://www.jvt.me/contacts/www.jvt.me/");

      assertThat(optionalPath).isPresent();
      assertThat(optionalPath.get()).isEqualTo("content/contacts/www.jvt.me.md");
    }

    @Test
    void handlesPersonalEvent() {
      Optional<String> optionalPath =
          service.contentPathFromUrl("https://www.jvt.me/events/personal-events/2020-05-14wzc/");

      assertThat(optionalPath).isPresent();
      assertThat(optionalPath.get()).isEqualTo("content/events/personal-events/2020-05-14wzc.md");
    }

    @Test
    void returnsEmptyOptionalWhenNotPersonalEvent() {
      Optional<String> optionalPath =
          service.contentPathFromUrl("https://www.jvt.me/events/2020-05-14wzc/");

      assertThat(optionalPath).isNotPresent();
    }

    @Test
    void handlesMf2AsUUID() {
      Optional<String> optionalPath =
          service.contentPathFromUrl(
              "https://www.jvt.me/mf2/40d3eba5-f620-4e93-8d20-94a376fd9670/");

      assertThat(optionalPath).isPresent();
      assertThat(optionalPath.get())
          .isEqualTo("content/mf2/40d3eba5-f620-4e93-8d20-94a376fd9670.md");
    }

    @Test
    void handlesMf2AsSlug() {
      Optional<String> optionalPath =
          service.contentPathFromUrl("https://www.jvt.me/mf2/2020/04/wibble/");

      assertThat(optionalPath).isPresent();
      assertThat(optionalPath.get()).isEqualTo("content/mf2/2020/04/wibble.md");
    }

    @Test
    void itHandlesLocalhostUrls() {
      Optional<String> optionalPath =
          service.contentPathFromUrl("http://localhost:1313/mf2/2020/04/wibble/");

      assertThat(optionalPath).isPresent();
      assertThat(optionalPath.get()).isEqualTo("content/mf2/2020/04/wibble.md");
    }

    @Test
    void itHandlesHttpUrls() {
      Optional<String> optionalPath =
          service.contentPathFromUrl("http://www.jvt.me/mf2/2020/04/wibble/");

      assertThat(optionalPath).isPresent();
      assertThat(optionalPath.get()).isEqualTo("content/mf2/2020/04/wibble.md");
    }

    @Test
    void handlesPostsUrl() {
      Optional<String> optionalPath =
          service.contentPathFromUrl("https://www.jvt.me/posts/2012/02/03/foo-bar/");

      assertThat(optionalPath).contains("content/posts/2012-02-03-foo-bar.md");
    }
  }

  private JsonNode node(String url) {
    ObjectNode node = OBJECT_MAPPER.createObjectNode();
    ObjectNode properties = node.putObject("properties");
    properties.putArray("url").add(url);
    return node;
  }
}
