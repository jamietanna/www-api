package me.jvt.www.api.micropub.actions;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AddPropertyActionTest {
  private AddPropertyAction sut;
  private TestMicroformats2Object mf2;

  @BeforeEach
  void setup() {
    mf2 = new TestMicroformats2Object();
  }

  @Test
  void itAddsIfDoesNotExist() {
    // given

    sut = new AddPropertyAction("new-value", Collections.singletonList("example"));

    // when
    sut.accept(mf2);

    // then
    List<String> actual = (List<String>) mf2.getProperties().get("new-value");
    assertThat(actual).containsExactly("example");
  }

  @Test
  void itAppendsIfDoesExist() {
    // given
    mf2.getProperties().setName(SingletonListHelper.singletonList("indieweb"));

    sut = new AddPropertyAction("name", Collections.singletonList("micropub"));

    // when
    sut.accept(mf2);

    // then
    List<String> actual = (List<String>) mf2.getProperties().get("name");
    assertThat(actual).containsExactly("indieweb", "micropub");
  }

  @Test
  void itAppendsMultipleValues() {
    // given
    mf2.getProperties().setName(SingletonListHelper.singletonList("indieweb"));

    sut = new AddPropertyAction("name", Arrays.asList("micropub", "mf2"));

    // when
    sut.accept(mf2);

    // then
    List<String> actual = (List<String>) mf2.getProperties().get("name");
    assertThat(actual).containsExactly("indieweb", "micropub", "mf2");
  }

  @Test
  void itDoesNotAddIfEmpty() {
    // given
    sut = new AddPropertyAction("new-value", Collections.emptyList());

    // when
    sut.accept(mf2);

    // then
    List<String> actual = (List<String>) mf2.getProperties().get("new-value");
    assertThat(actual).isNull();
  }
}
