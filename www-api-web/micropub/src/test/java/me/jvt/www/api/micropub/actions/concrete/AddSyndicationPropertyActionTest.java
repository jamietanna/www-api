package me.jvt.www.api.micropub.actions.concrete;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import me.jvt.www.api.micropub.model.MicropubSyndicateTo;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AddSyndicationPropertyActionTest {
  private AddSyndicationPropertyAction sut;
  private TestMicroformats2Object mf2;

  @BeforeEach
  void setup() {
    mf2 = new TestMicroformats2Object();
  }

  @Test
  void itAddsIfSyndicationDoesNotExist() {
    // given

    sut = new AddSyndicationPropertyAction(Collections.singletonList("example"));

    // when
    sut.accept(mf2);

    // then
    List<String> actual = mf2.getProperties().getSyndication();
    assertThat(actual).containsExactly("example");
  }

  @Test
  void itAppendsWhenSyndication() {
    sut =
        new AddSyndicationPropertyAction(
            SingletonListHelper.singletonList(MicropubSyndicateTo.BRIDGY_TWITTER.getUid()));

    // when
    sut.accept(mf2);

    // then
    List<String> actual = mf2.getProperties().getSyndication();
    assertThat(actual).containsExactlyInAnyOrder("https://brid.gy/publish/twitter");
  }

  @Test
  void itReplacesBridgyTwitterSyndicationWithSyndicatedLink() {
    mf2.getProperties()
        .setSyndication(
            SingletonListHelper.singletonList(MicropubSyndicateTo.BRIDGY_TWITTER.getUid()));
    sut =
        new AddSyndicationPropertyAction(
            SingletonListHelper.singletonList(
                "https://twitter.com/JamieTanna/status/1235637201821675522"));

    // when
    sut.accept(mf2);

    // then
    List<String> actual = mf2.getProperties().getSyndication();
    assertThat(actual).containsExactly("https://twitter.com/JamieTanna/status/1235637201821675522");
  }

  @Test
  void itReplacesIndieNewsSyndicationWithSyndicatedLink() {
    mf2.getProperties()
        .setSyndication(SingletonListHelper.singletonList(MicropubSyndicateTo.INDIENEWS.getUid()));
    sut =
        new AddSyndicationPropertyAction(
            SingletonListHelper.singletonList(
                "https://news.indieweb.org/en/www.jvt.me/posts/2020/03/22/at-mention-people/"));

    // when
    sut.accept(mf2);

    // then
    List<String> actual = mf2.getProperties().getSyndication();
    assertThat(actual)
        .containsExactly(
            "https://news.indieweb.org/en/www.jvt.me/posts/2020/03/22/at-mention-people/");
  }

  @Test
  void itHandlesMultipleExistingSyndicationLinks() {
    List<String> syndication = new ArrayList<>();
    syndication.add(MicropubSyndicateTo.INDIENEWS.getUid());
    syndication.add("https://twitter.com/JamieTanna");
    mf2.getProperties().setSyndication(syndication);
    sut =
        new AddSyndicationPropertyAction(
            SingletonListHelper.singletonList(
                "https://news.indieweb.org/en/www.jvt.me/posts/2020/03/22/at-mention-people/"));

    // when
    sut.accept(mf2);

    // then
    List<String> actual = mf2.getProperties().getSyndication();
    assertThat(actual)
        .containsExactlyInAnyOrder(
            "https://news.indieweb.org/en/www.jvt.me/posts/2020/03/22/at-mention-people/",
            "https://twitter.com/JamieTanna");
  }

  @Test
  void itReplacesMultipleSyndicationLinks() {
    List<String> syndication = new ArrayList<>();
    syndication.add(MicropubSyndicateTo.INDIENEWS.getUid());

    mf2.getProperties().setSyndication(syndication);
    sut =
        new AddSyndicationPropertyAction(
            List.of("https://news.indieweb.org/en/www.jvt.me/posts/2020/03/22/at-mention-people/"));

    // when
    sut.accept(mf2);

    // then
    List<String> actual = mf2.getProperties().getSyndication();
    assertThat(actual)
        .containsExactlyInAnyOrder(
            "https://news.indieweb.org/en/www.jvt.me/posts/2020/03/22/at-mention-people/");
  }
}
