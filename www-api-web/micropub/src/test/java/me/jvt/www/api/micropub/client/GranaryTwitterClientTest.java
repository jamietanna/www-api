package me.jvt.www.api.micropub.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.RETURNS_SELF;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.micropub.model.GranaryTwitterHentry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GranaryTwitterClientTest {
  GranaryTwitterHentry granaryTwitterHentry;

  RequestSpecificationFactory mockRequestSpecificationFactory;
  RequestSpecification mockRequestSpecification;
  Response mockResponse;

  GranaryTwitterClient sut;

  @BeforeEach
  void setup() {
    granaryTwitterHentry = new GranaryTwitterHentry();

    mockRequestSpecification = mock(RequestSpecification.class, RETURNS_SELF);
    mockResponse = mock(Response.class);
    when(mockResponse.getStatusCode()).thenReturn(200);

    when(mockRequestSpecification.get(anyString())).thenReturn(mockResponse);

    mockRequestSpecificationFactory = mock(RequestSpecificationFactory.class);
    when(mockRequestSpecificationFactory.newRequestSpecification())
        .thenReturn(mockRequestSpecification);

    when(mockResponse.as(any(Class.class))).thenReturn(granaryTwitterHentry);

    sut = new GranaryTwitterClient(mockRequestSpecificationFactory, "key", "shh");
  }

  @Test
  void itCallsGranaryBaseUrl() {
    // given

    // when
    sut.tweetToHentry("1");

    // then
    verify(mockRequestSpecification).baseUri("https://granary.io/twitter/@me/@all/@app");
  }

  @Test
  void itPerformsGetForTheActivityId() {
    // given

    // when
    sut.tweetToHentry("1");

    // then
    verify(mockRequestSpecification).get("/1");
  }

  @Test
  void itSendsFormatMf2JsonInQueryParameter() {
    // given

    // when
    sut.tweetToHentry("1");

    // then
    verify(mockRequestSpecification).queryParam("format", "mf2-json");
  }

  @Test
  void itSendsAccessTokenKeyFromConstructorAsQueryParameter() {
    // given

    // when
    sut.tweetToHentry("1");

    // then
    verify(mockRequestSpecification).queryParam("access_token_key", "key");
  }

  @Test
  void itSendsAccessTokenSecretFromConstructorAsQueryParameter() {
    // given

    // when
    sut.tweetToHentry("1");

    // then
    verify(mockRequestSpecification).queryParam("access_token_secret", "shh");
  }

  @Test
  void itThrowsRuntimeExceptionIfNot200Ok() {
    // given
    when(mockRequestSpecification.get(anyString())).thenReturn(mockResponse);
    when(mockResponse.getStatusCode()).thenReturn(500);

    // when
    assertThatThrownBy(() -> sut.tweetToHentry(""))
        // then
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Call to Granary failed with error code 500");
    verify(mockResponse, atLeast(1)).getStatusCode();
  }

  @Test
  void itCastsToGranaryTwitterHentry() {
    // given
    when(mockRequestSpecification.get(anyString())).thenReturn(mockResponse);

    // when
    GranaryTwitterHentry actual = sut.tweetToHentry("1");

    // then
    assertThat(actual).isSameAs(granaryTwitterHentry);
    verify(mockResponse).as(GranaryTwitterHentry.class);
  }
}
