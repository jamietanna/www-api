package me.jvt.www.api.micropub.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.RETURNS_SELF;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.util.ArrayList;
import java.util.List;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.micropub.model.ContentDeduplicationResult;
import me.jvt.www.api.micropub.model.ContentDeduplicationResult.ContentDeduplicationEntry;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ContentDeduplicationClientTest {

  RequestSpecification mockRequestSpecification;
  Response mockResponse;
  ContentDeduplicationResult result;

  ContentDeduplicationClient sut;

  @BeforeEach
  void setup() {
    mockRequestSpecification = mock(RequestSpecification.class, RETURNS_SELF);
    mockResponse = mock(Response.class);
    result = new ContentDeduplicationResult();

    RequestSpecificationFactory mockFactory = mock(RequestSpecificationFactory.class);
    when(mockFactory.newRequestSpecification()).thenReturn(mockRequestSpecification);

    when(mockRequestSpecification.get(anyString())).thenReturn(mockResponse);
    when(mockResponse.as(any(Class.class))).thenReturn(result);

    sut = new ContentDeduplicationClient(mockFactory);
  }

  @Test
  void itCallsPublicEndpoint() {
    // given

    // when
    sut.retrieveContentDeduplicationData();

    // then
    verify(mockRequestSpecification).get("https://www.jvt.me/content-deduplication.json");
  }

  @Test
  void itCastsAsContentDeduplicationResult() {
    // given

    // when
    ContentDeduplicationResult actual = sut.retrieveContentDeduplicationData();

    // then
    assertThat(actual).isSameAs(result);
  }

  @Test
  void itSantisesUrlsForMf2SlugRedirects() {
    // given
    result = new ContentDeduplicationResult();

    result.put(
        "https://wibble",
        SingletonListHelper.singletonList(
            new ContentDeduplicationEntry(Kind.likes, "https://www.jvt.me/mf2/2020/abc/")));
    List<ContentDeduplicationEntry> entries = new ArrayList<>();
    entries.add(
        new ContentDeduplicationEntry(
            Kind.likes, "https://www.jvt.me/mf2/e83c5498-beb3-4ef1-9e0b-9506ac250b7a/"));
    entries.add(
        new ContentDeduplicationEntry(
            Kind.bookmarks, "https://www.jvt.me/mf2/fffc5498-beb3-4ef1-9e0b-9506ac250b7a/"));

    result.put("https://foo", entries);
    when(mockResponse.as(any(Class.class))).thenReturn(result);

    // when
    ContentDeduplicationResult actual = sut.retrieveContentDeduplicationData();

    // then
    ContentDeduplicationEntry notUuid = actual.get("https://wibble").get(0);
    assertThat(notUuid.getSlug()).isEqualTo("2020/abc");

    ContentDeduplicationEntry uuid0 = actual.get("https://foo").get(0);
    assertThat(uuid0.getSlug()).isEqualTo("e83c5498-beb3-4ef1-9e0b-9506ac250b7a");

    ContentDeduplicationEntry uuid1 = actual.get("https://foo").get(1);
    assertThat(uuid1.getSlug()).isEqualTo("fffc5498-beb3-4ef1-9e0b-9506ac250b7a");
  }
}
