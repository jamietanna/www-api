package me.jvt.www.api.micropub.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MicropubPostTypeDtoTest {
  @Mock private Kind kind;
  @Mock private MicroformatsProperty property0;
  @Mock private MicroformatsProperty property1;

  private MicropubPostTypeDto dto;

  @BeforeEach
  void setup() {
    when(kind.getType()).thenReturn("type");
    when(kind.getName()).thenReturn("Name");
    when(kind.getH()).thenReturn("h-entry");

    when(kind.getOptionalProperties()).thenReturn(Collections.emptyList());
    when(kind.getRequiredProperties()).thenReturn(Collections.emptyList());
  }

  @Test
  void itTakesHFromKindWithoutHPrefix() {
    dto = new MicropubPostTypeDto(kind);

    assertThat(dto.getH()).isEqualTo("entry");
  }

  @Test
  void itTakesTypeFromKind() {
    dto = new MicropubPostTypeDto(kind);

    assertThat(dto.getType()).isEqualTo("type");
  }

  @Test
  void itTakesNameFromKind() {
    dto = new MicropubPostTypeDto(kind);

    assertThat(dto.getName()).isEqualTo("Name");
  }

  @Test
  void itTakesRequiredPropertiesFromKind() {
    when(property0.getName()).thenReturn("foo");
    when(property1.getName()).thenReturn("bar");

    when(kind.getRequiredProperties()).thenReturn(Arrays.asList(property0, property1));

    MicropubPostTypeDto dto = new MicropubPostTypeDto(kind);

    assertThat(dto.getRequiredProperties()).containsExactly("foo", "bar");
  }

  @Test
  void itReturnsAllValuesInProperties(
      @Mock MicroformatsProperty optional0, @Mock MicroformatsProperty optional1) {
    when(kind.getRequiredProperties()).thenReturn(Arrays.asList(property0, property1));
    when(kind.getOptionalProperties()).thenReturn(Arrays.asList(optional0, optional1));

    when(property0.getName()).thenReturn("foo");
    when(property1.getName()).thenReturn("bar");
    when(optional0.getName()).thenReturn("optional");
    when(optional1.getName()).thenReturn("another");

    MicropubPostTypeDto dto = new MicropubPostTypeDto(kind);

    assertThat(dto.getProperties()).containsExactlyInAnyOrder("foo", "bar", "optional", "another");
  }

  @Test
  void itAddsMpPhotoAltWhenPhotoPropertyWhenRequired() {
    when(property0.getName()).thenReturn("photo");
    when(kind.getRequiredProperties()).thenReturn(Collections.singletonList(property0));

    MicropubPostTypeDto dto = new MicropubPostTypeDto(kind);

    assertThat(dto.getRequiredProperties()).containsExactly("photo", "mp-photo-alt");
  }

  @Test
  void itAddsMpPhotoAltWhenPhotoPropertyWhenOptional() {
    when(property0.getName()).thenReturn("photo");
    when(kind.getOptionalProperties()).thenReturn(Collections.singletonList(property0));

    MicropubPostTypeDto dto = new MicropubPostTypeDto(kind);

    assertThat(dto.getProperties()).containsExactly("photo", "mp-photo-alt");
  }

  @Test
  void itDoesNotReturnEventPropertyWhenRequired() {
    when(property0.getName()).thenReturn("event");

    when(kind.getRequiredProperties()).thenReturn(Collections.singletonList(property0));

    MicropubPostTypeDto dto = new MicropubPostTypeDto(kind);

    assertThat(dto.getRequiredProperties()).isEmpty();
  }

  @Test
  void itDoesNotReturnEventPropertyWhenOptional() {
    when(property0.getName()).thenReturn("event");

    when(kind.getOptionalProperties()).thenReturn(Collections.singletonList(property0));

    MicropubPostTypeDto dto = new MicropubPostTypeDto(kind);

    assertThat(dto.getProperties()).isEmpty();
  }
}
