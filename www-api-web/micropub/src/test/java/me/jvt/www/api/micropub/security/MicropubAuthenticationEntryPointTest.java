package me.jvt.www.api.micropub.security;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.server.resource.web.BearerTokenAuthenticationEntryPoint;

@ExtendWith(MockitoExtension.class)
class MicropubAuthenticationEntryPointTest {

  @Mock BearerTokenAuthenticationEntryPoint mockDelegate;
  @Mock HttpServletRequest request;
  @Mock HttpServletResponse response;
  @Mock AuthenticationException authException;
  @Mock private PrintWriter printWriter;

  @InjectMocks MicropubAuthenticationEntryPoint sut;

  @Test
  void commenceDelegates() throws Exception {
    // given

    // when
    sut.commence(request, response, authException);

    // then
    verify(mockDelegate).commence(request, response, authException);
  }

  @Test
  void commenceAddsMicropubErrorResponseIfInsufficientAuthenticationException() throws Exception {
    // given
    authException = mock(InsufficientAuthenticationException.class);
    when(response.getWriter()).thenReturn(printWriter);

    // when
    sut.commence(request, response, authException);

    // then
    verify(response).setContentType("application/json");
    verify(response).setStatus(HttpStatus.UNAUTHORIZED.value());
    verify(printWriter)
        .println(
            "{"
                + "\"error\": \"unauthorized\", "
                + "\"error_description\": \"No authentication was provided.\""
                + "}");
  }
}
