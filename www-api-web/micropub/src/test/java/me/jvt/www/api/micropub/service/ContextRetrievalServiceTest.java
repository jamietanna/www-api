package me.jvt.www.api.micropub.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.LinkedHashSet;
import java.util.List;
import me.jvt.www.api.micropub.client.PostClient;
import me.jvt.www.api.micropub.context.ContextRetriever;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ContextRetrievalServiceTest {
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  @Mock private ContextRetriever retriever1;
  @Mock private ContextRetriever retriever2;

  @Mock private PostClient client;

  @Captor private ArgumentCaptor<Microformats2Object.Context> contextArgumentCaptor;

  private final TestMicroformats2Object mf2 = new TestMicroformats2Object();

  private ContextRetrievalService service;

  @BeforeEach
  void setup() {
    var retrievers = new LinkedHashSet<ContextRetriever>();
    retrievers.add(retriever1);
    retrievers.add(retriever2);
    service = new ContextRetrievalService(retrievers, client, OBJECT_MAPPER);
  }

  @Nested
  class RetrieveAndPersistContext {
    @Test
    void itDelegatesToConfiguredDecorators() {
      var decorated1 = new TestMicroformats2Object();
      when(retriever1.retrieve(any())).thenReturn(List.of());
      when(retriever2.retrieve(any())).thenReturn(List.of());

      service.retrieveAndPersistContext(mf2);

      var inorder = Mockito.inOrder(retriever1, retriever2);

      inorder.verify(retriever1).retrieve(mf2);
      inorder.verify(retriever2).retrieve(mf2);
    }

    @Test
    void itDelegatesToPostClient() {
      when(retriever1.retrieve(any())).thenReturn(List.of());
      when(retriever2.retrieve(any())).thenReturn(List.of(container(object("https://1"))));

      service.retrieveAndPersistContext(mf2);

      verify(client).upsertCite(contextArgumentCaptor.capture());
      assertThat(contextArgumentCaptor.getValue()).isNotNull();
      assertThat(contextArgumentCaptor.getValue().getContext())
          .isEqualTo(container(object("https://1")));
    }

    @Test
    void itCombinesObjects() {
      var object1 = object("https://1");
      var object2 = object("https://2");
      when(retriever1.retrieve(any())).thenReturn(List.of(container(object1)));
      when(retriever2.retrieve(any())).thenReturn(List.of(container(object2)));

      service.retrieveAndPersistContext(mf2);

      verify(client).upsertCite(contextArgumentCaptor.capture());
      assertThat(contextArgumentCaptor.getValue()).isNotNull();
      assertThat(contextArgumentCaptor.getValue().getContext())
          .isEqualTo(container(object1, object2));
    }

    @Test
    void removesDuplicateUrlsAndKeepsLargestProperties() {
      var object1 = object("https://1");
      var object2 = object("https://1");
      object2.with("properties").withArray("another").add("foo");
      object2.with("properties").withArray("more").add("extra");
      when(retriever1.retrieve(any())).thenReturn(List.of(container(object1)));
      when(retriever2.retrieve(any())).thenReturn(List.of(container(object2)));

      service.retrieveAndPersistContext(mf2);

      verify(client).upsertCite(contextArgumentCaptor.capture());
      assertThat(contextArgumentCaptor.getValue()).isNotNull();
      assertThat(contextArgumentCaptor.getValue().getContext()).isEqualTo(container(object2));
    }

    @Test
    void itRemovesObjectsWithEmptyUrl() {
      var object1 = OBJECT_MAPPER.createObjectNode();
      object1.putObject("properties").putArray("url");
      var object2 = object("https://2");
      when(retriever1.retrieve(any())).thenReturn(List.of(container(object1)));
      when(retriever2.retrieve(any())).thenReturn(List.of(container(object2)));

      service.retrieveAndPersistContext(mf2);

      verify(client).upsertCite(contextArgumentCaptor.capture());
      assertThat(contextArgumentCaptor.getValue()).isNotNull();
      assertThat(contextArgumentCaptor.getValue().getContext()).isEqualTo(container(object2));
    }

    @Test
    void itRemovesObjectsWithNoUrl() {
      var object1 = OBJECT_MAPPER.createObjectNode();
      object1.putObject("properties");
      var object2 = object("https://2");
      when(retriever1.retrieve(any())).thenReturn(List.of(container(object1)));
      when(retriever2.retrieve(any())).thenReturn(List.of(container(object2)));

      service.retrieveAndPersistContext(mf2);

      verify(client).upsertCite(contextArgumentCaptor.capture());
      assertThat(contextArgumentCaptor.getValue()).isNotNull();
      assertThat(contextArgumentCaptor.getValue().getContext()).isEqualTo(container(object2));
    }
  }

  protected JsonNode container(JsonNode... nodes) {
    var container = OBJECT_MAPPER.createObjectNode();
    container.putArray("items");
    for (var node : nodes) {
      container.withArray("items").add(node);
    }
    return container;
  }

  protected ObjectNode object(String url) {
    var object = OBJECT_MAPPER.createObjectNode();
    object.putObject("properties").putArray("url").add(url);
    return object;
  }
}
