package me.jvt.www.api.micropub.model.posttype;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object.Context;
import me.jvt.www.api.micropub.model.request.MicropubRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.EnumSource.Mode;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class Microformats2ObjectFactoryTest {

  @SuppressWarnings("unused")
  @Mock
  private ObjectMapper objectMapper;

  @Mock private YAMLMapper yamlMapper;

  @Mock private PostTypeDiscoverer discoverer;
  @Mock private MicropubRequest request;
  @Mock private MicropubRequest.RequestContext requestContext;
  @Mock private Context context;
  @Mock private Properties properties;

  @Mock private Microformats2ObjectParser parser0;
  @Mock private Microformats2ObjectParser parser1;

  private Microformats2ObjectFactory factory;

  @BeforeEach
  void setup() {
    List<Microformats2ObjectParser> parsers = new ArrayList<>();
    parsers.add(parser0);
    parsers.add(parser1);

    factory = new Microformats2ObjectFactory(objectMapper, yamlMapper, discoverer, parsers);
  }

  @Nested
  class Constructor {
    @Test
    void doesNotAllowEmptyParsers() {
      assertThatThrownBy(
              () ->
                  new Microformats2ObjectFactory(
                      objectMapper, yamlMapper, discoverer, Collections.emptyList()))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Must provide at least one Microformats2ObjectParser");
    }
  }

  @Nested
  class Create {

    @Nested
    class Mf2 {

      @BeforeEach
      void setup() {
        when(request.getContext()).thenReturn(requestContext);
        when(request.getContext().getProperties()).thenReturn(properties);
        when(requestContext.getContext()).thenReturn(context);
        when(context.getH()).thenReturn("h-wibble");
      }

      @ParameterizedTest
      @EnumSource(
          value = Kind.class,
          mode = Mode.EXCLUDE,
          names = {"articles"})
      void defaultsToHugoJson(Kind kind) {
        discover(kind);

        Microformats2Object actual = factory.create(request);

        assertThat(actual).isInstanceOf(HugoMicroformats2Json.class);
      }

      @ParameterizedTest
      @EnumSource(value = Kind.class)
      void itDelegatesToDiscoverer(Kind kind) {
        discover(kind);

        factory.create(request);

        verify(discoverer, atLeastOnce()).discover("h-wibble", properties);
      }

      @Test
      void itSetsContext() {
        discover(Kind.likes);

        Microformats2Object actual = factory.create(request);

        assertThat(actual.getContext()).isSameAs(context);
      }

      @ParameterizedTest
      @EnumSource(value = Kind.class)
      void itSetsKind(Kind kind) {
        discover(kind);

        Microformats2Object actual = factory.create(request);

        assertThat(actual.getKind()).isEqualTo(kind);
      }

      @Test
      void itSetsProperties() {
        discover(Kind.likes);

        Microformats2Object actual = factory.create(request);

        assertThat(actual.getProperties()).isSameAs(properties);
      }
    }

    @Nested
    class Article {

      @BeforeEach
      void setup() {
        when(request.getContext()).thenReturn(requestContext);
        when(request.getContext().getProperties()).thenReturn(properties);
        when(requestContext.getContext()).thenReturn(context);
        when(context.getH()).thenReturn("h-wibble");
        discover(Kind.articles);
      }

      @Test
      void returnsArticleMf2Object() {
        Microformats2Object actual = factory.create(request);

        assertThat(actual).isInstanceOf(HugoArticleMicroformats2Object.class);
      }

      @Test
      void itDelegatesToDiscoverer() {
        factory.create(request);

        verify(discoverer).discover("h-wibble", properties);
      }

      @Test
      void itSetsContext() {
        Microformats2Object actual = factory.create(request);

        assertThat(actual.getContext()).isSameAs(context);
      }

      @Test
      void itSetsKind() {
        Microformats2Object actual = factory.create(request);

        assertThat(actual.getKind()).isEqualTo(Kind.articles);
      }

      @Test
      void itSetsProperties() {
        Microformats2Object actual = factory.create(request);

        assertThat(actual.getProperties()).isSameAs(properties);
      }
    }

    private void discover(Kind kind) {
      when(discoverer.discover(anyString(), any())).thenReturn(kind);
    }
  }

  @Nested
  class Deserialize {
    @Test
    void itDelegatesToParser() {
      factory.deserialize("the-post");

      verify(parser0).deserialize("the-post");
    }

    @Test
    void itReturnsFromDelegate(@Mock Microformats2Object mf2) {
      when(parser0.deserialize(any())).thenReturn(mf2);

      Microformats2Object actual = factory.deserialize("the-post");

      assertThat(actual).isSameAs(mf2);
    }

    @Test
    void itDelegatesToSecondParserIfAnythingGoesWrongOnFirst() {
      when(parser0.deserialize(anyString())).thenThrow(new RuntimeException());

      factory.deserialize("the-post");

      verify(parser1).deserialize("the-post");
    }

    @Test
    void itReturnsFromSecondParserIfAnythingGoesWrongOnFirst(@Mock Microformats2Object mf2) {
      when(parser0.deserialize(anyString())).thenThrow(new RuntimeException());
      when(parser1.deserialize(any())).thenReturn(mf2);

      Microformats2Object actual = factory.deserialize("the-post");

      assertThat(actual).isSameAs(mf2);
    }

    @Test
    void itThrowsIllegalStateExceptionWhenBothFail() {
      when(parser0.deserialize(anyString())).thenThrow(new RuntimeException());
      when(parser1.deserialize(anyString())).thenThrow(new RuntimeException());

      assertThatThrownBy(() -> factory.deserialize("the-post"))
          .isInstanceOf(IllegalStateException.class)
          .hasMessage("Could not parse post");
    }
  }
}
