package me.jvt.www.api.micropub.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.MicropubConfigDto;
import me.jvt.www.api.micropub.model.MicropubPostTypeDto;
import me.jvt.www.api.micropub.model.MicropubPostTypesDto;
import me.jvt.www.api.micropub.model.MicropubQueryDto;
import me.jvt.www.api.micropub.service.query.QueryHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.util.MultiValueMap;

@ExtendWith(MockitoExtension.class)
class QueryServiceTest {
  @Mock private QueryHandler handler0;
  @Mock private QueryHandler handler1;

  private QueryService service;

  @BeforeEach
  void setup() {
    service = new QueryService(handler0, handler1);

    when(handler0.getQuery()).thenReturn("wibble");
  }

  @Test
  void configCallsMatchingDelegateAndAddsSupported() {
    setupConfig();

    MicropubConfigDto actual = (MicropubConfigDto) service.handle("config", null, Optional.empty());

    assertThat(actual.getQueries()).containsExactlyInAnyOrder("config", "post-types");
  }

  @Test
  void configDelegatesForPostTypes() {
    setupConfig();

    service.handle("config", null, Optional.empty());

    verify(handler1).handle(null, Optional.empty());
  }

  @Test
  void configAddsPostTypes(@Mock MicropubPostTypeDto postType) {
    setupConfig(postType);

    MicropubConfigDto actual = (MicropubConfigDto) service.handle("config", null, Optional.empty());

    assertThat(actual.getPostTypes()).isEqualTo(Collections.singletonList(postType));
  }

  @Test
  void configThrowsIllegalStateExceptionWhenNoPostTypesHandler() {
    when(handler0.getQuery()).thenReturn("not-matched");
    when(handler1.getQuery()).thenReturn("config");

    MicropubConfigDto dto = new MicropubConfigDto();
    when(handler1.handle(any(), any())).thenReturn(dto);

    assertThatThrownBy(() -> service.handle("config", null, Optional.empty()))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("No post-types handler was configured");
  }

  @Test
  void itDelegatesParameters(@Mock MultiValueMap<String, String> params) {
    service.handle("wibble", params, Optional.empty());

    verify(handler0).handle(eq(params), any());
  }

  @Test
  void itDelegatesAuthentication(@Mock Optional<Authentication> authentication) {
    service.handle("wibble", null, authentication);

    verify(handler0).handle(any(), same(authentication));
  }

  @Test
  void itReturnsFromDelegate(@Mock MicropubQueryDto dto) {
    when(handler0.handle(any(), any())).thenReturn(dto);

    MicropubQueryDto actual = service.handle("wibble", null, Optional.empty());

    assertThat(actual).isSameAs(dto);
  }

  @Test
  void itThrowsWhenQueryNotSupported() {
    when(handler0.getQuery()).thenReturn("wibble");
    when(handler1.getQuery()).thenReturn("config");

    assertThatThrownBy(() -> service.handle("wobble", null, Optional.empty()))
        .isInstanceOf(InvalidMicropubMetadataRequest.class)
        .hasMessage("Query `wobble` is not supported");
  }

  private void setupConfig() {
    setupConfig(mock(MicropubPostTypeDto.class));
  }

  private void setupConfig(MicropubPostTypeDto postType) {
    when(handler0.getQuery()).thenReturn("post-types");
    when(handler1.getQuery()).thenReturn("config");

    MicropubConfigDto dto = new MicropubConfigDto();
    when(handler1.handle(any(), any())).thenReturn(dto);

    MicropubPostTypesDto postTypes = new MicropubPostTypesDto();
    postTypes.postTypes = Collections.singletonList(postType);
    when(handler0.handle(any(), any())).thenReturn(postTypes);
  }
}
