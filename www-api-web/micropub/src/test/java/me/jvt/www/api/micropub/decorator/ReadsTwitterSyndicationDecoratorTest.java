package me.jvt.www.api.micropub.decorator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ReadsTwitterSyndicationDecoratorTest {

  private final TestMicroformats2Object mf2 = new TestMicroformats2Object();

  private final ReadsTwitterSyndicationDecorator decorator = new ReadsTwitterSyndicationDecorator();

  @BeforeEach
  void setup() {
    mf2.setKind(Kind.reads);
  }

  @Test
  void itAddsSyndicationToTwitter() {
    decorator.decorate(mf2);

    assertThat(mf2.getProperties().getSyndication())
        .containsExactly("https://brid.gy/publish/twitter");
  }

  @Test
  void itReturnsSelf() {
    Microformats2Object actual = decorator.decorate(mf2);

    assertThat(actual).isEqualTo(mf2);
  }

  @Test
  void itHandlesOtherKinds(@Mock Microformats2Object mf2) {
    when(mf2.getKind()).thenReturn(Kind.articles);

    decorator.decorate(mf2);

    verify(mf2).getKind();
    verifyNoMoreInteractions(mf2);
  }
}
