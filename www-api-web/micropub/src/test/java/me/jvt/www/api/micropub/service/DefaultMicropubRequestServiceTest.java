package me.jvt.www.api.micropub.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import me.jvt.www.api.micropub.actions.Action;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.request.MicropubRequest;
import me.jvt.www.api.micropub.model.request.MicropubRequest.MicropubAction;
import me.jvt.www.api.micropub.service.MicropubRequestService.UpdateResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;

@ExtendWith(MockitoExtension.class)
class DefaultMicropubRequestServiceTest {
  @Mock private MicropubRequest request;
  @Mock private MicropubRequest.RequestContext requestContext;
  @Mock private Microformats2Object.Context context;
  @Mock private OAuth2AuthenticatedPrincipal principal;
  @Mock private Microformats2Object mf2;

  @Mock private MicropubService micropubService;

  @Captor ArgumentCaptor<MicropubRequest> requestCaptor;
  @Captor private ArgumentCaptor<Microformats2Object> mf2Captor;

  private Authentication authentication;
  private final Properties properties = new Properties();

  @InjectMocks private DefaultMicropubRequestService service;

  @Nested
  class Create {
    @BeforeEach
    void setup() {
      when(request.getAction()).thenReturn(MicropubAction.CREATE);
      when(request.getContext()).thenReturn(requestContext);
      when(requestContext.getContext()).thenReturn(context);

      when(micropubService.create(any())).thenReturn(mf2);
      authentication =
          new AnonymousAuthenticationToken(
              "example", principal, AuthorityUtils.createAuthorityList("SCOPE_foo"));
      when(principal.getAttribute(eq("client_id"))).thenReturn("https://foo");

      when(micropubService.performSanitisation(mf2)).thenReturn(mf2);
      when(micropubService.performDecoration(mf2)).thenReturn(mf2);
      when(micropubService.save(mf2)).thenReturn("/the/slug/");
    }

    @Test
    void constructsMf2ObjectFromFactory() {
      process();

      verify(micropubService).create(request);
    }

    @Test
    void sanitisesMf2Object() {
      process();

      verify(micropubService).performSanitisation(mf2);
    }

    @Test
    void decoratesMf2Object() {
      process();

      verify(micropubService).performDecoration(mf2);
    }

    @Test
    void itSetsClientIdInContext() {
      process();

      verify(context).setClientId("https://foo");
    }

    @Test
    void itSetsClientIdInContextBeforeFactory() {
      process();

      InOrder inOrder = Mockito.inOrder(context, micropubService);
      inOrder.verify(context).setClientId("https://foo");
      inOrder.verify(micropubService).create(requestCaptor.capture());
    }

    @Test
    void savesMf2Object() {
      process();

      verify(micropubService).save(mf2);
    }

    @Test
    void returnsCreateResponse() {
      MicropubRequestService.MicropubResponse response = process();

      assertThat(response).isInstanceOf(MicropubRequestService.CreateResponse.class);
    }

    @Test
    void slugIsReturned() {
      MicropubRequestService.MicropubResponse response = process();

      MicropubRequestService.CreateResponse actual =
          (MicropubRequestService.CreateResponse) response;
      assertThat(actual.getSlug()).isEqualTo("/the/slug/");
    }

    @Test
    void itSetsPostStatusToDraftIfDraftScope() {
      Collection<? extends GrantedAuthority> authorities =
          Collections.singleton(new SimpleGrantedAuthority("SCOPE_draft"));
      authentication = new AnonymousAuthenticationToken("example", principal, authorities);
      when(mf2.getProperties()).thenReturn(properties);

      process();

      verify(micropubService).save(mf2Captor.capture());

      assertThat(mf2Captor.getValue().getProperties().getPostStatus()).containsExactly("draft");
    }
  }

  @Nested
  class Delete {
    private static final String URL = "https://post";

    @Mock private Action action;
    private List<Action> actions;

    @Nested
    class HappyPath {

      @BeforeEach
      void setup() {
        actions = Collections.singletonList(action);

        when(request.getAction()).thenReturn(MicropubAction.DELETE);
        when(request.getActions()).thenReturn(actions);

        when(request.getContext()).thenReturn(requestContext);
        when(requestContext.getUrl()).thenReturn(URL);

        when(micropubService.retrieve(anyString(), anyBoolean())).thenReturn(Optional.of(mf2));
        when(micropubService.update(any(), any())).thenReturn(mf2);

        when(micropubService.save(mf2)).thenReturn("/the/slug/");
      }

      @Test
      void itRetrievesWhenUndeleteScopeIsNotPresent() {
        process();

        verify(micropubService).retrieve(URL, false);
      }

      @Test
      void itRetrievesWhenUndeleteScopeIsPresent() {
        Collection<? extends GrantedAuthority> authorities =
            Collections.singleton(new SimpleGrantedAuthority("SCOPE_undelete"));
        authentication = new AnonymousAuthenticationToken("example", "something", authorities);

        process();

        verify(micropubService).retrieve(URL, true);
      }

      @Test
      void itDelegatesToServiceToUpdate() {

        process();

        verify(micropubService).update(mf2, actions);
      }

      @Test
      void returnsDeleteUndeleteResponse() {
        MicropubRequestService.MicropubResponse response = process();

        assertThat(response).isInstanceOf(MicropubRequestService.DeleteUndeleteResponse.class);
      }
    }

    @Nested
    class SadPath {
      @BeforeEach
      void setup() {
        when(request.getAction()).thenReturn(MicropubAction.DELETE);

        when(request.getContext()).thenReturn(requestContext);
        when(requestContext.getUrl()).thenReturn(URL);

        when(micropubService.retrieve(anyString(), anyBoolean())).thenReturn(Optional.of(mf2));
      }

      @Test
      void itThrowsInvalidMicropubMetadataRequestWhenPostDoesNotExist() {
        when(micropubService.retrieve(anyString(), anyBoolean())).thenReturn(Optional.empty());

        assertThatThrownBy(DefaultMicropubRequestServiceTest.this::process)
            .isInstanceOf(InvalidMicropubMetadataRequest.class)
            .hasMessage("Post https://post does not exist");
      }
    }
  }

  @Nested
  class Undelete {
    private static final String URL = "https://post";

    @Mock private Action action;
    private List<Action> actions;

    @Nested
    class HappyPath {

      @BeforeEach
      void setup() {
        actions = Collections.singletonList(action);

        when(request.getAction()).thenReturn(MicropubAction.UNDELETE);
        when(request.getActions()).thenReturn(actions);

        when(request.getContext()).thenReturn(requestContext);
        when(requestContext.getUrl()).thenReturn(URL);

        when(micropubService.retrieve(anyString(), anyBoolean())).thenReturn(Optional.of(mf2));
        when(micropubService.update(any(), any())).thenReturn(mf2);

        when(micropubService.save(mf2)).thenReturn("/the/slug/");
      }

      @Test
      void itRetrievesWhenUndeleteScopeIsNotPresent() {
        process();

        verify(micropubService).retrieve(URL, false);
      }

      @Test
      void itRetrievesWhenUndeleteScopeIsPresent() {
        Collection<? extends GrantedAuthority> authorities =
            Collections.singleton(new SimpleGrantedAuthority("SCOPE_undelete"));
        authentication = new AnonymousAuthenticationToken("example", "something", authorities);

        process();

        verify(micropubService).retrieve(URL, true);
      }

      @Test
      void itDelegatesToServiceToUpdate() {
        process();

        verify(micropubService).update(mf2, actions);
      }

      @Test
      void returnsDeleteUndeleteResponse() {
        MicropubRequestService.MicropubResponse response = process();

        assertThat(response).isInstanceOf(MicropubRequestService.DeleteUndeleteResponse.class);
      }
    }

    @Nested
    class SadPath {
      @BeforeEach
      void setup() {
        when(request.getAction()).thenReturn(MicropubAction.DELETE);

        when(request.getContext()).thenReturn(requestContext);
        when(requestContext.getUrl()).thenReturn(URL);

        when(micropubService.retrieve(anyString(), anyBoolean())).thenReturn(Optional.of(mf2));
      }

      @Test
      void itThrowsInvalidMicropubMetadataRequestWhenPostDoesNotExist() {
        when(micropubService.retrieve(anyString(), anyBoolean())).thenReturn(Optional.empty());

        assertThatThrownBy(DefaultMicropubRequestServiceTest.this::process)
            .isInstanceOf(InvalidMicropubMetadataRequest.class)
            .hasMessage("Post https://post does not exist");
      }
    }
  }

  @Nested
  class Update {

    private static final String URL = "https://post";

    @Mock private Action action;
    private List<Action> actions;

    @Nested
    class HappyPath {

      @BeforeEach
      void setup() {
        actions = Collections.singletonList(action);

        when(request.getAction()).thenReturn(MicropubAction.UPDATE);
        when(request.getActions()).thenReturn(actions);

        when(request.getContext()).thenReturn(requestContext);
        when(requestContext.getUrl()).thenReturn(URL);

        when(micropubService.retrieve(anyString(), anyBoolean())).thenReturn(Optional.of(mf2));
        when(micropubService.performSanitisation(any())).thenReturn(mf2);
        when(micropubService.update(any(), any())).thenReturn(mf2);

        when(micropubService.save(mf2)).thenReturn("/the/slug/");

        when(micropubService.retrieve(anyString(), anyBoolean())).thenReturn(Optional.of(mf2));

        Collection<? extends GrantedAuthority> authorities =
            Collections.singleton(new SimpleGrantedAuthority("SCOPE_foo"));
        authentication = new AnonymousAuthenticationToken("example", principal, authorities);
      }

      @Test
      void itRetrievesWhenUndeleteScopeIsNotPresent() {

        process();

        verify(micropubService).retrieve(URL, false);
      }

      @Test
      void itRetrievesWhenUndeleteScopeIsPresent() {
        Collection<? extends GrantedAuthority> authorities =
            Collections.singleton(new SimpleGrantedAuthority("SCOPE_undelete"));
        authentication = new AnonymousAuthenticationToken("example", "something", authorities);

        process();

        verify(micropubService).retrieve(URL, true);
      }

      @Test
      void itSanitisesPost() {

        process();

        verify(micropubService).performSanitisation(mf2);
      }

      @Test
      void itDelegatesToServiceToUpdate() {
        process();

        verify(micropubService).update(mf2, actions);
      }

      @Test
      void itReturnsUpdateResponse() {
        MicropubRequestService.MicropubResponse response = process();

        assertThat(response).isInstanceOf(MicropubRequestService.UpdateResponse.class);
      }

      @Test
      void itReturnsKindNewMf2Inresponse() {
        MicropubRequestService.MicropubResponse response = process();

        MicropubRequestService.UpdateResponse actual =
            (MicropubRequestService.UpdateResponse) response;

        assertThat(((UpdateResponse) response).getNewObject()).isSameAs(mf2);
      }
    }

    @Nested
    class SadPath {
      @BeforeEach
      void setup() {
        when(request.getAction()).thenReturn(MicropubAction.UPDATE);

        when(request.getContext()).thenReturn(requestContext);
        when(requestContext.getUrl()).thenReturn(URL);

        when(micropubService.retrieve(anyString(), anyBoolean())).thenReturn(Optional.of(mf2));
      }

      @Test
      void itThrowsInvalidMicropubMetadataRequestWhenPostDoesNotExist() {
        when(micropubService.retrieve(anyString(), anyBoolean())).thenReturn(Optional.empty());

        assertThatThrownBy(DefaultMicropubRequestServiceTest.this::process)
            .isInstanceOf(InvalidMicropubMetadataRequest.class)
            .hasMessage("Post https://post does not exist");
      }

      @Test
      void itThrowsAccessDeniedExceptionWhenPostExistsWithNoPostStatusWhenDraftScope() {
        Collection<? extends GrantedAuthority> authorities =
            Collections.singleton(new SimpleGrantedAuthority("SCOPE_draft"));
        authentication = new AnonymousAuthenticationToken("example", principal, authorities);
        when(micropubService.performSanitisation(any())).thenReturn(mf2);
        when(mf2.getProperties()).thenReturn(properties);

        assertThatThrownBy(DefaultMicropubRequestServiceTest.this::process)
            .isInstanceOf(AccessDeniedException.class)
            .hasMessage("Published post cannot be updated when `draft` scope is present");
      }

      @Test
      void itThrowsAccessDeniedExceptionWhenPostExistsAndIsNotDraftWhenDraftScope() {
        Collection<? extends GrantedAuthority> authorities =
            Collections.singleton(new SimpleGrantedAuthority("SCOPE_draft"));
        authentication = new AnonymousAuthenticationToken("example", principal, authorities);
        when(micropubService.performSanitisation(any())).thenReturn(mf2);
        when(mf2.getProperties()).thenReturn(properties);
        properties.setPostStatus(Collections.singletonList("published"));

        assertThatThrownBy(DefaultMicropubRequestServiceTest.this::process)
            .isInstanceOf(AccessDeniedException.class)
            .hasMessage("Published post cannot be updated when `draft` scope is present");
      }
    }
  }

  private MicropubRequestService.MicropubResponse process() {
    return service.handle(request, authentication);
  }
}
