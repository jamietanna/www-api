package me.jvt.www.api.micropub.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;

class MicropubSyndicateToTest {

  static Stream<Arguments> rewrite() {
    return Stream.of(
        Arguments.of(
            MicropubSyndicateTo.INDIENEWS,
            "https://news.indieweb.org/en/www.jvt.me/posts/2020/03/22/at-mention-people/"),
        Arguments.of(
            MicropubSyndicateTo.BRIDGY_GITHUB,
            "https://github.com/indieweb/indieauth/issues/31#issuecomment-589516629"),
        Arguments.of(
            MicropubSyndicateTo.BRIDGY_TWITTER,
            "https://twitter.com/JamieTanna/status/1235637201821675522"));
  }

  @ParameterizedTest
  @MethodSource("rewrite")
  void rewriteSyndicationLink(MicropubSyndicateTo previous, String newSyndication) {
    // given

    // when
    boolean shouldRewrite = MicropubSyndicateTo.rewrite(previous.getUid(), newSyndication);

    // then
    assertThat(shouldRewrite).isTrue();
  }

  @ParameterizedTest
  @EnumSource(MicropubSyndicateTo.class)
  void rewriteDoesNotRewriteIfNotFound(MicropubSyndicateTo syndicateTo) {
    // given

    // when
    boolean shouldRewrite =
        MicropubSyndicateTo.rewrite(syndicateTo.getUid(), "https://example.com");

    // then
    assertThat(shouldRewrite).isFalse();
  }
}
