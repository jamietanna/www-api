package me.jvt.www.api.micropub.context;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.JsonNode;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import me.jvt.www.api.micropub.client.Microformats2PipeClient;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OpengraphMf2ContextRetrieverTest {

  abstract class BaseTest {
    protected ContextRetriever retriever;

    @Mock protected Microformats2PipeClient client;

    @BeforeEach
    void setup() {
      retriever = retriever();
    }

    protected ContextRetriever retriever() {
      return new OpengraphMf2ContextRetriever(client);
    }
  }

  abstract class BaseTestCase extends BaseTest {

    protected abstract Microformats2Object prepare(List<String> values);

    protected List<JsonNode> perform() {
      return perform(List.of());
    }

    protected List<JsonNode> perform(List<String> values) {
      return retriever.retrieve(prepare(values));
    }

    @Test
    void delegates() {
      perform(List.of("https://some-site.com/page/1234"));

      verify(client)
          .retrieve(
              URLDecoder.decode(
                  "https://opengraph-mf2.tanna.dev/url?url=https://some-site.com/page/1234",
                  StandardCharsets.UTF_8));
    }

    @Test
    void setsContextIfPresent(@Mock JsonNode node) {
      when(client.retrieve(any())).thenReturn(Optional.of(node));

      var actual = perform(List.of("https://some-site.com/page/1234"));

      assertThat(actual).containsExactly(node);
    }

    @Test
    void doesNotSetContextIfEmpty() {
      var actual = perform();

      assertThat(actual).isEmpty();
    }
  }

  @Nested
  class Url extends BaseTestCase {

    @Override
    protected Microformats2Object prepare(List<String> values) {
      var mf2 = new TestMicroformats2Object();
      mf2.getProperties().setUrl(values);
      return mf2;
    }
  }

  @Nested
  class RepostOf extends BaseTestCase {

    @Override
    protected Microformats2Object prepare(List<String> values) {
      var mf2 = new TestMicroformats2Object();
      mf2.getProperties().setRepostOf(values);
      return mf2;
    }
  }

  @Nested
  class LikeOf extends BaseTestCase {

    @Override
    protected Microformats2Object prepare(List<String> values) {
      var mf2 = new TestMicroformats2Object();
      mf2.getProperties().setLikeOf(values);
      return mf2;
    }
  }

  @Nested
  class InReplyTo extends BaseTestCase {

    @Override
    protected Microformats2Object prepare(List<String> values) {
      var mf2 = new TestMicroformats2Object();
      mf2.getProperties().setInReplyTo(values);
      return mf2;
    }
  }

  @Nested
  class BookmarkOf extends BaseTestCase {

    @Override
    protected Microformats2Object prepare(List<String> values) {
      var mf2 = new TestMicroformats2Object();
      mf2.getProperties().setBookmarkOf(values);
      return mf2;
    }
  }

  @Nested
  class ListenOf extends BaseTestCase {

    @Override
    protected Microformats2Object prepare(List<String> values) {
      var mf2 = new TestMicroformats2Object();
      mf2.getProperties().setListenOf(values);
      return mf2;
    }
  }

  @Nested
  class WhenMultiplePropertiesSet extends BaseTest {
    @Test
    void itReturnsAll(@Mock JsonNode node) {
      when(client.retrieve(any())).thenReturn(Optional.of(node));

      var mf2 = new TestMicroformats2Object();
      mf2.getProperties().setBookmarkOf(List.of("https://foo"));
      mf2.getProperties().setRepostOf(List.of("https://foo"));

      var actual = retriever.retrieve(mf2);

      assertThat(actual).containsExactly(node, node);
    }
  }

  @Nested
  class UrlEncoding extends BaseTest {
    @Test
    void isCorrectForUrlParameter() {
      var mf2 = new TestMicroformats2Object();
      mf2.getProperties().setBookmarkOf(List.of("https://foo/page?p=4"));

      retriever.retrieve(mf2);

      verify(client).retrieve("https://opengraph-mf2.tanna.dev/url?url=https://foo/page?p%3D4");
    }
  }
}
