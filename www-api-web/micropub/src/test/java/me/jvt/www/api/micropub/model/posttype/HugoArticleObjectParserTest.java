package me.jvt.www.api.micropub.model.posttype;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

class HugoArticleObjectParserTest {
  private final YAMLMapper MAPPER = new YAMLMapper();
  private final HugoArticleObjectParser parser = new HugoArticleObjectParser(MAPPER);

  @Test
  void itIgnoresUnknownProperties() {
    Microformats2Object actual = parse();

    assertThat(actual).isNotNull();
  }

  @Test
  void itMapsName() {
    Microformats2Object actual = parse();

    assertThat(actual.getProperties().getName()).containsExactly("Week Notes 21#06");
  }

  @Test
  void itMapsSummary() {
    Microformats2Object actual = parse();

    assertThat(actual.getProperties().getSummary())
        .containsExactly("What happened in the week of 2021-02-01?");
  }

  @Test
  void itMapsPublished() {
    Microformats2Object actual = parse();

    assertThat(actual.getProperties().getPublished()).containsExactly("2021-02-07T21:42:41+0000");
  }

  @Test
  void itMapsContentAsValue() {
    Microformats2Object actual = parse();

    List<Map<String, String>> content = actual.getProperties().getContent();
    assertThat(content.get(0)).containsEntry("value", "This is \nraw <strong>content</strong>");
  }

  @Test
  void itSetsHEntry() {
    Microformats2Object actual = parse();

    assertThat(actual.getContext().getH()).isEqualTo("h-entry");
  }

  @Test
  void itMapsAliases() {
    Microformats2Object actual = parse();

    assertThat(actual.getContext().getAliases()).containsExactly("foo", "bar");
  }

  @Test
  void itMapsSyndication() {
    Microformats2Object actual = parse();

    assertThat(actual.getProperties().getSyndication())
        .containsExactly("https://away", "https://url");
  }

  @Test
  void itMapsTags() {
    Microformats2Object actual = parse();

    assertThat(actual.getProperties().getCategory()).containsExactly("abc", "hugo");
  }

  @Test
  void itMapsDeleted() {
    Microformats2Object actual = parseWithDeleted();

    assertThat(actual.getContext().isDeleted()).isTrue();
  }

  @Test
  void itMapsDraftWhenFalse() {
    Microformats2Object actual = parseWithDraft(false);

    assertThat(actual.getProperties().getPostStatus()).containsExactly("published");
  }

  @Test
  void itMapsDraftWhenTrue() {
    Microformats2Object actual = parseWithDraft(true);

    assertThat(actual.getProperties().getPostStatus()).containsExactly("draft");
  }

  @Test
  void itDoesNotMapAliasesWhenAbsent() {
    Microformats2Object actual = parseWithoutAlias();

    assertThat(actual.getContext().getAliases()).isNull();
  }

  @Test
  void itMapsLicenseForCode() {
    Microformats2Object actual = parse();

    assertThat(actual.getProperties().getCodeLicense()).containsExactly("cc-1");
  }

  @Test
  void itMapsLicenseForProse() {
    Microformats2Object actual = parse();

    assertThat(actual.getProperties().getProseLicense()).containsExactly("cc-2");
  }

  @Test
  void itMapsSeries() {
    Microformats2Object actual = parse();

    assertThat(actual.getProperties().getSeries()).containsExactly("pretty-print-json");
  }

  @Test
  void itMapsImage() {
    Microformats2Object actual = parse();

    assertThat(actual.getProperties().getFeatured()).containsExactly("/some/img.png");
  }

  @Test
  void itMapsCanonicalUrl() {
    Microformats2Object actual = parse();

    assertThat(actual.getProperties().getRepostOf()).containsExactly("canonical");
  }

  @Test
  void itMapsSlug() {
    Microformats2Object actual = parse();

    assertThat(actual.getContext().getSlug()).isEqualTo("the-slug");
  }

  private Microformats2Object parseWithoutAlias() {
    return parser.deserialize(
        "---\n"
            + "title: \"Week Notes 21#06\"\n"
            + "description: \"What happened in the week of 2021-02-01?\"\n"
            + "date: 2021-02-07T21:42:41+0000\n"
            + "not_real: fooo\n"
            + "---\nThis is \n"
            + "raw <strong>content</strong>");
  }

  private Microformats2Object parse() {
    return parser.deserialize(
        "---\n"
            + "title: \"Week Notes 21#06\"\n"
            + "description: \"What happened in the week of 2021-02-01?\"\n"
            + "date: 2021-02-07T21:42:41+0000\n"
            + "not_real: fooo\n"
            + "license_code: cc-1\n"
            + "license_prose: cc-2\n"
            + "series: pretty-print-json\n"
            + "image: /some/img.png\n"
            + "canonical_url: canonical\n"
            + "slug: the-slug\n"
            + "aliases:\n"
            + "- foo\n"
            + "- bar\n"
            + "syndication:\n"
            + "- https://away\n"
            + "- https://url\n"
            + "tags:\n"
            + "- abc\n"
            + "- hugo\n"
            + "---\nThis is \n"
            + "raw <strong>content</strong>");
  }

  private Microformats2Object parseWithDraft(boolean isDraft) {
    return parser.deserialize(
        "---\n"
            + "title: \"Week Notes 21#06\"\n"
            + "description: \"What happened in the week of 2021-02-01?\"\n"
            + "date: 2021-02-07T21:42:41+0000\n"
            + "deleted: true\n"
            + "draft: "
            + isDraft
            + "\n"
            + "---\nThis is \n"
            + "raw <strong>content</strong>");
  }

  private Microformats2Object parseWithDeleted() {
    return parser.deserialize(
        "---\n"
            + "title: \"Week Notes 21#06\"\n"
            + "description: \"What happened in the week of 2021-02-01?\"\n"
            + "date: 2021-02-07T21:42:41+0000\n"
            + "deleted: true\n"
            + "---\nThis is \n"
            + "raw <strong>content</strong>");
  }
}
