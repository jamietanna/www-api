package me.jvt.www.api.micropub.decorator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.HashtagToTagConverter;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class HashtagToTagDecoratorTest {

  HashtagToTagConverter mockConverter;
  HashtagToTagDecorator sut;
  TestMicroformats2Object mf2;

  @BeforeEach
  void setup() {
    mockConverter = mock(HashtagToTagConverter.class);
    when(mockConverter.convert(anyString())).thenReturn("tag");

    sut = new HashtagToTagDecorator(mockConverter);

    mf2 = new TestMicroformats2Object();
    mf2.getProperties().setContent(new ArrayList<>());
    mf2.getProperties().getContent().add(new HashMap<>());
  }

  @ParameterizedTest
  @ValueSource(strings = {"#Hashtag", "#PSD2"})
  void itExtractsHashtagFromContentValueAndPassesToConverter(String hashtag) {
    mf2.getProperties()
        .getContent()
        .get(0)
        .put("value", String.format("This is a %s post", hashtag));

    sut.decorate(mf2);

    verify(mockConverter).convert(hashtag);
  }

  @Test
  void itExtractsMultipleHashtagsFromContentValueAndPassesToConverter() {
    mf2.getProperties().getContent().get(0).put("value", "This is a #Hashtag post #TN");

    sut.decorate(mf2);

    verify(mockConverter).convert("#Hashtag");
    verify(mockConverter).convert("#TN");
  }

  @Test
  void itSavesConvertedHashtagAsTag() {
    mf2.getProperties().getContent().get(0).put("value", "This is a #Hashtag post");

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getCategory()).containsExactly("tag");
  }

  @Test
  void itSavesConvertedHashtagAsTagWhenNullTags() {
    mf2.getProperties().getContent().get(0).put("value", "This is a #Hashtag post");
    mf2.getProperties().setCategory(null);

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getCategory()).containsExactly("tag");
  }

  @Test
  void itSavesConvertedHashtagAsTagWhenEmptyTags() {
    mf2.getProperties().getContent().get(0).put("value", "This is a #Hashtag post");
    mf2.getProperties().setCategory(new ArrayList<>());

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getCategory()).containsExactly("tag");
  }

  @Test
  void itAppendsHashtagToExistingTags() {
    mf2.getProperties().getContent().get(0).put("value", "This is a #hashtag post");
    mf2.getProperties().setCategory(SingletonListHelper.singletonList("foo"));
    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getCategory()).containsExactly("foo", "tag");
  }

  @Test
  void itDoesNotCallConverterIfNoHashtagsFound() {
    mf2.getProperties().getContent().get(0).put("value", "This is not a hashtag post");
    mf2.getProperties().setCategory(SingletonListHelper.singletonList("foo"));
    sut.decorate(mf2);

    verifyNoInteractions(mockConverter);
  }

  @Test
  void itDoesNotCallConverterIfNoContent() {
    mf2.getProperties().setContent(null);
    mf2.getProperties().setCategory(SingletonListHelper.singletonList("foo"));
    sut.decorate(mf2);

    verifyNoInteractions(mockConverter);
  }

  @Test
  void itReplacesTheHashtagInContentWithLinkToTag() {
    mf2.getProperties().getContent().get(0).put("value", "This is a #Hashtag post");

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getContent().get(0).get("value"))
        .isEqualTo("This is a <a href=\"/tags/tag/\">#Hashtag</a> post");
  }

  @Test
  void itReplacesTheHashtagIfThereIsAPrecedingSpace() {
    mf2.getProperties()
        .getContent()
        .get(0)
        .put("value", "Check it out at https://www.jvt.me#anchor");

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getContent().get(0).get("value"))
        .isEqualTo("Check it out at https://www.jvt.me#anchor");
  }

  @Test
  void itReplacesTheHashtagIfItIsAtTheStartOfTheLine() {
    mf2.getProperties().getContent().get(0).put("value", "#IndieWeb is the best!");

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getContent().get(0).get("value"))
        .isEqualTo("<a href=\"/tags/tag/\">#IndieWeb</a> is the best!");
  }

  @Test
  void itDoesNotSetHtmlIfNotSet() {
    Map<String, String> content = mf2.getProperties().getContent().get(0);
    content.put("value", "This is a #Hashtag post");

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getContent().get(0)).doesNotContainKey("html");
  }

  @Test
  void itDoesNotDestroyHtmlIfSet() {
    Map<String, String> content = mf2.getProperties().getContent().get(0);
    content.put("html", "<me>");
    content.put("value", "This is a #Hashtag post");

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getContent().get(0))
        .containsEntry("value", "This is a <a href=\"/tags/tag/\">#Hashtag</a> post");
    assertThat(actual.getProperties().getContent().get(0)).containsEntry("html", "<me>");
  }

  @Test
  void itDoesNotReplaceInHtml() {
    Map<String, String> content = mf2.getProperties().getContent().get(0);
    content.put("html", "This is a #Hashtag post");
    content.put("value", "");

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getContent().get(0))
        .containsEntry("html", "This is a #Hashtag post");
  }
}
