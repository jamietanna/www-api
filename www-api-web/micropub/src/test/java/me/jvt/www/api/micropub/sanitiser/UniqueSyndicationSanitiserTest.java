package me.jvt.www.api.micropub.sanitiser;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;

class UniqueSyndicationSanitiserTest {
  private UniqueSyndicationSanitiser sut;
  private TestMicroformats2Object mf2;

  @BeforeEach
  void setup() {
    mf2 = new TestMicroformats2Object();
    sut = new UniqueSyndicationSanitiser();
  }

  @ParameterizedTest
  @NullAndEmptySource
  void itDoesNothingIfSyndicationIsNotSet(List<String> categories) {
    // given
    mf2.getProperties().setSyndication(categories);

    // when
    sut.sanitise(mf2);

    // then
    assertThat(mf2.getProperties().getSyndication()).isEqualTo(categories);
  }

  @Test
  void itRemovesDuplicates() {
    // given
    mf2.getProperties().setSyndication(new ArrayList<>());
    mf2.getProperties().getSyndication().add("hello");
    mf2.getProperties().getSyndication().add("no");
    mf2.getProperties().getSyndication().add("hello");

    // when
    sut.sanitise(mf2);

    // then
    assertThat(mf2.getProperties().getSyndication()).containsExactly("hello", "no");
  }

  @Test
  void itReturnsTheMf2Object() {
    // given

    // when
    Microformats2Object actual = sut.sanitise(mf2);

    // then
    assertThat(mf2).isSameAs(actual);
  }
}
