package me.jvt.www.api.micropub.actions;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.jvt.www.api.micropub.actions.concrete.AddSyndicationPropertyAction;
import me.jvt.www.api.micropub.actions.concrete.ReplaceContentPropertyAction;
import me.jvt.www.api.micropub.actions.concrete.ReplaceRsvpPropertyAction;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class ActionParserTest {
  private static final DateTimeFormatter DATE_TIME_FORMATTER =
      DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("UTC"));
  private static final Instant NOW = Instant.now();
  private static final Clock FIXED_CLOCK = Clock.fixed(NOW, ZoneId.of("UTC"));

  private ActionParser sut;
  private Map<String, Object> request;

  @BeforeEach
  void setup() {
    sut = new ActionParser(FIXED_CLOCK);
    request = new HashMap<>();
  }

  @Test
  void itThrowsInvalidMicropubMetadataRequestIfActionIsNotUpdate() {
    // given
    request.put("action", "wibble");

    // when
    assertThatThrownBy(() -> sut.parse(request))
        // then
        .isInstanceOf(InvalidMicropubMetadataRequest.class)
        .hasMessage("Only `delete`, `undelete` and `update` are supported");
  }

  @Nested
  class Update {
    @BeforeEach
    void setup() {
      request.put("action", "update");
    }

    @Test
    void itParsesReplacePropertyAction() {
      // given
      Map<String, List<String>> replace = new HashMap<>();
      replace.put("arbitrary", SingletonListHelper.singletonList("wibble"));
      request.put("replace", replace);

      // when
      List<Action> actions = sut.parse(request);

      // then
      ReplacePropertyAction expected =
          new ReplacePropertyAction("arbitrary", SingletonListHelper.singletonList("wibble"));
      assertThat(actions).containsExactly(expected);
    }

    @Test
    void itParsesAddPropertyAction() {
      // given
      Map<String, List<String>> add = new HashMap<>();
      add.put("something", SingletonListHelper.singletonList("https://something"));
      request.put("add", add);

      // when
      List<Action> actions = sut.parse(request);

      // then
      AddPropertyAction expected =
          new AddPropertyAction(
              "something", SingletonListHelper.singletonList("https://something"));
      assertThat(actions).containsExactly(expected);
    }

    @Test
    void itAddsAReplacePublishActionWhenPostStatusModifiedToPublished() {
      Map<String, List<String>> replace = new HashMap<>();
      replace.put("post-status", SingletonListHelper.singletonList("published"));
      request.put("replace", replace);

      // when
      List<Action> actions = sut.parse(request);

      // then
      ReplacePropertyAction publish =
          new ReplacePropertyAction(
              "published", Collections.singletonList(DATE_TIME_FORMATTER.format(NOW)));
      assertThat(actions).usingFieldByFieldElementComparator().contains(publish);
    }

    @Test
    void itDoesNotAddDeletePublishActionWhenPostStatusNotModifiedToPublished() {
      Map<String, List<String>> replace = new HashMap<>();
      replace.put("post-status", SingletonListHelper.singletonList("draft"));
      request.put("replace", replace);

      // when
      List<Action> actions = sut.parse(request);

      // then
      assertThat(actions).hasSize(1);
    }

    @Test
    void itParsesDeletePropertyActionWithNoValues() {
      // given
      request.put("delete", SingletonListHelper.singletonList("category"));

      // when
      List<Action> actions = sut.parse(request);

      // then
      DeletePropertyAction expected = new DeletePropertyAction("category");
      assertThat(actions).containsExactly(expected);
    }

    @Test
    void itParsesDeletePropertyActionWithSpecifiedValues() {
      // given
      Map<String, List<String>> delete = new HashMap<>();
      delete.put("foo", SingletonListHelper.singletonList("https://something"));
      request.put("delete", delete);

      // when
      List<Action> actions = sut.parse(request);

      // then
      DeletePropertyAction expected =
          new DeletePropertyAction("foo", SingletonListHelper.singletonList("https://something"));
      assertThat(actions).containsExactly(expected);
    }

    @Test
    void itParsesMultipleActions() {
      // given
      Map<String, List<String>> replace = new HashMap<>();
      replace.put("wibble", SingletonListHelper.singletonList("wibble"));
      replace.put("foo", SingletonListHelper.singletonList("bar"));
      request.put("replace", replace);
      request.put("delete", SingletonListHelper.singletonList("category"));

      // when
      List<Action> actions = sut.parse(request);

      // then
      assertThat(actions)
          .containsExactlyInAnyOrder(
              new ReplacePropertyAction("wibble", SingletonListHelper.singletonList("wibble")),
              new ReplacePropertyAction("foo", SingletonListHelper.singletonList("bar")),
              new DeletePropertyAction("category"));
    }

    @Test
    void itThrowsInvalidMicropubMetadataRequestIfNoActionsFound() {
      // given

      // when
      assertThatThrownBy(() -> sut.parse(request))
          // then
          .isInstanceOf(InvalidMicropubMetadataRequest.class)
          .hasMessage("No update actions could be parsed from the request");
    }

    @Test
    void itParsesAddContentAsSubclass() {
      // given
      Map<String, List<String>> add = new HashMap<>();
      add.put("content", SingletonListHelper.singletonList("https://something"));
      request.put("add", add);

      // when
      List<Action> actions = sut.parse(request);

      // then
      ReplaceContentPropertyAction expected =
          new ReplaceContentPropertyAction(SingletonListHelper.singletonList("https://something"));
      assertThat(actions).containsExactly(expected);
    }

    @Test
    void itParsesAddSyndicationAsSubclass() {
      // given
      Map<String, List<String>> add = new HashMap<>();
      add.put("syndication", SingletonListHelper.singletonList("https://something"));
      request.put("add", add);

      // when
      List<Action> actions = sut.parse(request);

      // then
      AddSyndicationPropertyAction expected =
          new AddSyndicationPropertyAction(SingletonListHelper.singletonList("https://something"));
      assertThat(actions).containsExactly(expected);
    }

    @Test
    void itParsesReplaceContentAsSubclass() {
      // given
      Map<String, List<String>> add = new HashMap<>();
      add.put("content", SingletonListHelper.singletonList("https://something"));
      request.put("replace", add);

      // when
      List<Action> actions = sut.parse(request);

      // then
      ReplaceContentPropertyAction expected =
          new ReplaceContentPropertyAction(SingletonListHelper.singletonList("https://something"));
      assertThat(actions).containsExactly(expected);
    }

    @Test
    void itParsesReplaceRsvpAsSubclass() {
      // given
      Map<String, List<String>> add = new HashMap<>();
      add.put("rsvp", SingletonListHelper.singletonList("yes"));
      request.put("replace", add);

      // when
      List<Action> actions = sut.parse(request);

      // then
      ReplaceRsvpPropertyAction expected =
          new ReplaceRsvpPropertyAction(SingletonListHelper.singletonList("yes"));
      assertThat(actions).containsExactly(expected);
    }
  }

  @Nested
  class Delete {
    @BeforeEach
    void setup() {
      request.put("action", "delete");
    }

    @Test
    void itParsesDeletePostAction() {
      // given

      // when
      List<Action> actions = sut.parse(request);

      // then
      DeletePostAction expected = new DeletePostAction();
      assertThat(actions).containsExactly(expected);
    }
  }

  @Nested
  class Undelete {
    @BeforeEach
    void setup() {
      request.put("action", "undelete");
    }

    @Test
    void itParsesUndeletePostAction() {
      // given

      // when
      List<Action> actions = sut.parse(request);

      // then
      UndeletePostAction expected = new UndeletePostAction();
      assertThat(actions).containsExactly(expected);
    }
  }
}
