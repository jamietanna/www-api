package me.jvt.www.api.micropub.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import me.jvt.www.api.micropub.client.MediaClient;
import me.jvt.www.api.micropub.client.PostClient;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.service.MicropubConflictService;
import me.jvt.www.api.micropub.service.PersistenceService;
import me.jvt.www.api.micropub.service.PersistenceService.PersistenceAction;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
class DefaultMicropubPersistenceRepositoryTest {

  private MicropubConflictService mockMicropubConflictService;
  private PostClient mockClientPost;
  private MediaClient mockClientMedia;
  private TestMicroformats2Object mf2;
  private DefaultMicropubPersistenceRepository sut;

  @Mock private PersistenceService persistenceService;
  @Mock private PersistenceAction action;

  @BeforeEach
  void setup() {
    mockClientPost = mock(PostClient.class);
    mockClientMedia = mock(MediaClient.class);
    mockMicropubConflictService = mock(MicropubConflictService.class);

    lenient()
        .when(mockMicropubConflictService.isConflictFoundInCache(mf2))
        .thenReturn(Optional.empty());

    mf2 = new TestMicroformats2Object();
    mf2.setKind(Kind.bookmarks);
    mf2.getProperties().setBookmarkOf(SingletonListHelper.singletonList("https://foo.bar"));

    sut =
        new DefaultMicropubPersistenceRepository(
            mockClientPost, mockClientMedia, mockMicropubConflictService, persistenceService);
  }

  @Test
  void saveCallsAddContentIfNoSlug() {
    sut.save(mf2);

    verify(mockClientPost).addContent(mf2);
  }

  @Test
  void saveCallsUpdateContentIfASlugIsPresentAndFound(@Mock Microformats2Object mock) {
    // given
    when(action.getPath()).thenReturn("/content/path");
    when(persistenceService.determineAction(any(Microformats2Object.class))).thenReturn(action);
    when(mockClientPost.getContent(any())).thenReturn(Optional.of(mock));
    mf2.getContext().setSlug("something");

    // when
    sut.save(mf2);

    // then
    verify(mockClientPost).updateContent(mf2);
  }

  @Test
  void saveCallsAddContentIfASlugIsPresentButPostNotFound() {
    // given
    when(action.getPath()).thenReturn("/content/path");
    when(persistenceService.determineAction(any(Microformats2Object.class))).thenReturn(action);
    when(mockClientPost.getContent(any())).thenReturn(Optional.empty());
    mf2.getContext().setSlug("something");

    // when
    sut.save(mf2);

    // then
    verify(mockClientPost).addContent(mf2);
  }

  @Test
  void saveDelegatesObjectToPersistenceService() {
    // given
    when(action.getPath()).thenReturn("/content/path");
    when(persistenceService.determineAction(any(Microformats2Object.class))).thenReturn(action);
    when(mockClientPost.getContent(any())).thenReturn(Optional.empty());
    mf2.getContext().setSlug("something");

    // when
    sut.save(mf2);

    // then
    verify(persistenceService).determineAction(mf2);
  }

  @Test
  void saveDelegatesContentPathToPostClient() {
    // given
    when(action.getPath()).thenReturn("/content/path");
    when(persistenceService.determineAction(any(Microformats2Object.class))).thenReturn(action);
    when(mockClientPost.getContent(any())).thenReturn(Optional.empty());
    mf2.getContext().setSlug("something");

    // when
    sut.save(mf2);

    // then
    verify(mockClientPost).getContent("/content/path");
  }

  @Test
  void saveReturnsItsSlugIfASlugIsPresent() {
    // given
    mf2.getContext().setSlug("something");
    when(mockClientPost.updateContent(any())).thenReturn("client-slug");
    when(persistenceService.determineAction(any(Microformats2Object.class))).thenReturn(action);
    when(mockClientPost.getContent(any())).thenReturn(Optional.of(new TestMicroformats2Object()));

    // when
    String actual = sut.save(mf2);

    // then
    assertThat(actual).isEqualTo("client-slug");
  }

  @Test
  void saveChecksForConflicts() {
    // given

    // when
    sut.save(mf2);

    // then
    verify(mockMicropubConflictService).isConflictFoundInCache(mf2);
  }

  @Test
  void saveChecksForConflictsReturnsSlugIfExists() {
    // given
    when(mockMicropubConflictService.isConflictFoundInCache(mf2)).thenReturn(Optional.of("/slug/"));

    // when
    String actual = sut.save(mf2);

    // then
    assertThat(actual).isEqualTo("/slug/");
    verify(mockClientPost, times(0)).addContent(mf2);
  }

  @Test
  void saveReturnsPostSlugFromClientWhenNotInCache() {
    // given
    when(mockMicropubConflictService.isConflictFoundInCache(mf2)).thenReturn(Optional.empty());
    when(mockClientPost.addContent(any(TestMicroformats2Object.class))).thenReturn("/abc/slug/");

    // when
    String actual = sut.save(mf2);

    // then
    assertThat(actual).isNotNull();

    verify(mockClientPost).addContent(mf2);
    assertThat(actual).isEqualTo("/abc/slug/");
  }

  @Test
  void saveDelegatesToMockClient() {
    // given

    // when
    sut.save(mf2);

    // then
    verify(mockClientPost).addContent(mf2);
  }

  @Test
  void saveUpdatesCacheAfterSavingInClient() {
    // given
    when(mockClientPost.addContent(any(TestMicroformats2Object.class))).thenReturn("/abc/slug/");

    // when
    sut.save(mf2);

    // then
    verify(mockMicropubConflictService).updateCacheWithSlug(mf2, "/abc/slug/");
  }

  @Test
  void saveReturnsMediaUrlFromClient() {
    // given
    MultipartFile file = mock(MultipartFile.class);
    when(mockClientMedia.addContent(any(MultipartFile.class), any())).thenReturn("/abc/123.jpg");

    // when
    String actual = sut.save(file, MediaType.IMAGE_JPEG);

    // then
    assertThat(actual).isEqualTo("/abc/123.jpg");
  }

  @Test
  void saveDelegatesMediaToMockClient() {
    // given
    MultipartFile file = mock(MultipartFile.class);

    // when
    sut.save(file, MediaType.IMAGE_JPEG);

    // then
    verify(mockClientMedia).addContent(file, MediaType.IMAGE_JPEG);
  }

  @Test
  void findByIdDelegatesToClient() {
    // given

    // when
    sut.findById("content/mf2/wibble.md");

    // then
    verify(mockClientPost).getContent("content/mf2/wibble.md");
  }

  @Test
  void findByIdReturnsValueFromDelegate() {
    // given
    TestMicroformats2Object mockMf2 = mock(TestMicroformats2Object.class);
    when(mockClientPost.getContent(any())).thenReturn(Optional.of(mockMf2));

    // when
    Optional<Microformats2Object> optional = sut.findById("content/mf2/wibble.md");

    // then
    assertThat(optional).isPresent();
    assertThat(optional.get()).isSameAs(mockMf2);
  }
}
