package me.jvt.www.api.micropub.actions;

import static org.assertj.core.api.Assertions.assertThat;

import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DeletePostActionTest {

  private DeletePostAction sut;
  private TestMicroformats2Object mf2;

  @BeforeEach
  void setup() {
    mf2 = new TestMicroformats2Object();
    sut = new DeletePostAction();
  }

  @Test
  void whenAlreadyDeleted() {
    // given
    mf2.getContext().setDeleted(true);

    // when
    sut.accept(mf2);

    // then
    assertThat(mf2.getContext().isDeleted()).isTrue();
  }

  @Test
  void whenNotAlreadyDeleted() {
    // given
    mf2.getContext().setDeleted(false);

    // when
    sut.accept(mf2);

    // then
    assertThat(mf2.getContext().isDeleted()).isTrue();
  }
}
