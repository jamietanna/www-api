package me.jvt.www.api.micropub.service.query;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;
import me.jvt.www.api.micropub.model.QueryPropertiesDto;
import me.jvt.www.api.micropub.model.QueryPropertiesDto.Property;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.EnumSource.Mode;
import org.springframework.util.LinkedMultiValueMap;

class QueryPropertiesHandlerTest {

  private final QueryPropertiesHandler handler = new QueryPropertiesHandler();

  @Nested
  class GetQuery {
    @Test
    void returnsProperties() {
      String actual = handler.getQuery();

      assertThat(actual).isEqualTo("properties");
    }
  }

  @Nested
  class Handle {
    private final QueryPropertiesDto actual =
        (QueryPropertiesDto) handler.handle(new LinkedMultiValueMap<>(), Optional.empty());

    @ParameterizedTest
    @EnumSource(
        value = MicroformatsProperty.class,
        names = {"POST_STATUS", "RSVP", "READ_STATUS"})
    void enumsHaveOptionsList(MicroformatsProperty property) {
      Optional<Property> maybeProperty =
          actual.getProperties().stream()
              .filter(p -> p.getName().equals(property.getName()))
              .findFirst();
      assertThat(maybeProperty).isNotEmpty();

      Property found = maybeProperty.get();
      assertThat(found.getOptions()).isNotNull();
    }

    @ParameterizedTest
    @EnumSource(
        value = MicroformatsProperty.class,
        names = {"POST_STATUS", "RSVP", "READ_STATUS"},
        mode = Mode.EXCLUDE)
    void nonEnumsDoNotHaveOptionsList(MicroformatsProperty property) {
      Optional<Property> maybeProperty =
          actual.getProperties().stream()
              .filter(p -> p.getName().equals(property.getName()))
              .findFirst();
      assertThat(maybeProperty).isNotEmpty();

      Property found = maybeProperty.get();
      assertThat(found.getOptions()).isNull();
    }
  }
}
