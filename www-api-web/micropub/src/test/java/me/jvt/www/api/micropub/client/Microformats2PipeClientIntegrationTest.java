package me.jvt.www.api.micropub.client;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.WebClient;

class Microformats2PipeClientIntegrationTest {
  @ExtendWith(SpringExtension.class)
  @Import({IntegrationTestBase.Config.class, JacksonAutoConfiguration.class})
  @DirtiesContext
  @Nested
  abstract class IntegrationTestBase {

    @Autowired protected ObjectMapper objectMapper;
    @Autowired protected MockWebServer server;

    @Autowired protected Microformats2PipeClient client;

    @TestConfiguration
    static class Config {

      @Bean
      public MockWebServer webServer() {
        return new MockWebServer();
      }

      @Bean
      public WebClient webClient() {
        return WebClient.builder().build();
      }

      @Bean
      public Microformats2PipeClient client(MockWebServer server, WebClient webClient) {
        return new Microformats2PipeClient(webClient);
      }
    }

    //        protected JsonNode fetch() {
    //            return fetcher.fetch(server.url("").toString());
    //        }

    protected String serialise(JsonNode container) {
      try {
        return objectMapper.writeValueAsString(container);
      } catch (JsonProcessingException e) {
        throw new IllegalStateException(e);
      }
    }

    protected Optional<JsonNode> retrieve() {
      return client.retrieve(server.url("") + "/foo");
    }

    protected MockResponse success(JsonNode... nodes) {
      var container = container(nodes);

      return new MockResponse()
          .setBody(serialise(container))
          .setResponseCode(200)
          .setHeader("content-type", "application/mf2+json");
    }

    protected JsonNode container(JsonNode... nodes) {
      var container = objectMapper.createObjectNode();
      container.putArray("items");
      for (var node : nodes) {
        container.withArray("items").add(node);
      }
      return container;
    }
  }

  @Nested
  class WhenFound extends IntegrationTestBase {
    @Test
    void itReturnsWhenOneElement() {
      server.enqueue(success(validObject()));

      var actual = retrieve();

      assertThat(actual).isPresent();
      assertThat(actual).contains(container(validObject()));
    }

    @Test
    void itReturnsWhenAtLeastOneElement() {
      var expected = validObject();
      var other = validObject();
      other.put("foo", "bar");
      server.enqueue(success(expected, other));

      var actual = retrieve();

      assertThat(actual).isPresent();
      assertThat(actual).contains(container(expected, other));
    }

    @Test
    void itReturnsEmptyWhenNoItems() {
      server.enqueue(success());

      var actual = retrieve();

      assertThat(actual).isEmpty();
    }

    protected ObjectNode validObject() {
      var node = objectMapper.createObjectNode();
      node.putArray("type").add("h-entry");
      node.putObject("properties").putArray("name").add("The title");
      return node;
    }
  }

  @Nested
  class Request extends IntegrationTestBase {
    private RecordedRequest request;

    @BeforeEach
    void setup() throws InterruptedException {
      server.enqueue(success(objectMapper.createObjectNode()));

      retrieve();

      request = server.takeRequest(100, TimeUnit.MILLISECONDS);
      assertThat(request).isNotNull();
    }

    @Test
    void isGet() {
      assertThat(request.getMethod()).isEqualTo("GET");
    }

    @Test
    void isCorrectPath() {
      assertThat(request.getPath()).isEqualTo("/foo");
    }

    @Test
    void hasAccept() {
      assertThat(request.getHeader("accept")).isEqualTo("application/mf2+json");
    }
  }

  @Nested
  class When4xx extends IntegrationTestBase {
    @Test
    void itReturnsEmpty() {
      server.enqueue(new MockResponse().setResponseCode(HttpStatus.NOT_FOUND.value()));

      var actual = retrieve();

      assertThat(actual).isEmpty();
    }
  }

  @Nested
  class When5xx extends IntegrationTestBase {
    @Test
    void itReturnsEmpty() {
      server.enqueue(new MockResponse().setResponseCode(HttpStatus.INTERNAL_SERVER_ERROR.value()));

      var actual = retrieve();

      assertThat(actual).isEmpty();
    }
  }
}
