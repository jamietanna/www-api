package me.jvt.www.api.micropub.sanitiser;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.junit.jupiter.api.Test;

class LowercaseCategorySanitiserTest {
  private final TestMicroformats2Object mf2 = new TestMicroformats2Object();

  private final LowercaseCategorySanitiser sanitiser = new LowercaseCategorySanitiser();

  @Test
  void itDoesNothingWhenNoCategory() {
    sanitiser.sanitise(mf2);

    assertThat(mf2.getProperties().getCategory()).isNull();
  }

  @Test
  void itLowercasesCategories() {
    mf2.getProperties().setCategory(Arrays.asList("Foo", "bAR", "other"));

    sanitiser.sanitise(mf2);

    assertThat(mf2.getProperties().getCategory()).containsExactly("foo", "bar", "other");
  }

  @Test
  void itReturnsMf2() {

    Microformats2Object actual = sanitiser.sanitise(mf2);

    assertThat(actual).isSameAs(mf2);
  }
}
