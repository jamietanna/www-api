package me.jvt.www.api.micropub.actions.concrete;

import static org.assertj.core.api.Assertions.assertThat;

import me.jvt.www.api.micropub.model.Properties.Rsvp;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ReplaceRsvpPropertyActionTest {

  private ReplaceRsvpPropertyAction sut;
  private TestMicroformats2Object mf2;

  @BeforeEach
  void setup() {
    mf2 = new TestMicroformats2Object();
  }

  @Test
  void itReplacesRsvpField() {
    // given
    mf2.getProperties().setRsvp(SingletonListHelper.singletonList(Rsvp.yes));
    mf2.getProperties().setInReplyTo(SingletonListHelper.singletonList("https://foo.bar"));
    sut = new ReplaceRsvpPropertyAction(SingletonListHelper.singletonList("maybe"));

    // when
    sut.accept(mf2);

    // then
    assertThat(mf2.getProperties().getRsvp()).containsExactly(Rsvp.maybe);
  }
}
