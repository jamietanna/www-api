package me.jvt.www.api.micropub.decorator;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;

class PublishedDecoratorTest {
  private static final DateTimeFormatter DATE_TIME_FORMATTER =
      DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("UTC"));

  private static final Instant NOW = Instant.now();

  private static final Clock FIXED_CLOCK = Clock.fixed(NOW, ZoneId.of("UTC"));

  private final TestMicroformats2Object mf2 = new TestMicroformats2Object();
  private final PublishedDecorator sut = new PublishedDecorator(FIXED_CLOCK);

  @Test
  void itHandlesNoProperty() {
    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getPublished())
        .containsExactly(DATE_TIME_FORMATTER.format(NOW));
  }

  @ParameterizedTest
  @NullAndEmptySource
  void nullOrEmptyIsOverridden(List<String> published) {
    mf2.getProperties().setPublished(published);

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getPublished())
        .containsExactly(DATE_TIME_FORMATTER.format(NOW));
  }

  @Test
  void whenPresentDoesNothing() {
    mf2.getProperties().setPublished(SingletonListHelper.singletonList("something"));

    Microformats2Object actual = sut.decorate(mf2);

    assertThat(actual.getProperties().getPublished()).containsExactly("something");
  }
}
