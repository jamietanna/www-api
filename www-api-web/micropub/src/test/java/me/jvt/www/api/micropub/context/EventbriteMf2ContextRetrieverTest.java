package me.jvt.www.api.micropub.context;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.List;
import java.util.Optional;
import me.jvt.www.api.micropub.client.Microformats2PipeClient;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class EventbriteMf2ContextRetrieverTest {

  protected ContextRetriever retriever;

  @Mock protected Microformats2PipeClient client;

  @BeforeEach
  void setup() {
    retriever = retriever();
  }

  protected ContextRetriever retriever() {
    return new EventbriteMf2ContextRetriever(client);
  }

  protected Microformats2Object prepare(List<String> values) {
    TestMicroformats2Object mf2 = new TestMicroformats2Object();
    mf2.getProperties().setInReplyTo(values);
    return mf2;
  }

  protected List<JsonNode> perform() {
    return perform(List.of());
  }

  protected List<JsonNode> perform(List<String> values) {
    return retriever.retrieve(prepare(values));
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        "eventbrite.co.uk",
        "www.eventbrite.co.uk",
        "eventbrite.com",
        "www.eventbrite.com"
      })
  void delegates(String domain) {
    perform(List.of(String.format("https://%s/e/ddd-east-midlands-tickets-58629047058", domain)));

    verify(client)
        .retrieve("https://eventbrite-mf2.herokuapp.com/e/ddd-east-midlands-tickets-58629047058");
  }

  @Test
  void setsContextIfPresent(@Mock JsonNode node) {
    when(client.retrieve(any())).thenReturn(Optional.of(node));

    var actual = perform(List.of("https://eventbrite.com/e/ddd-east-midlands-tickets-58629047058"));

    assertThat(actual).containsExactly(node);
  }

  @Test
  void doesNotSetContextIfEmpty() {
    var actual = perform();

    assertThat(actual).isEmpty();
  }

  @Test
  void doesNotDelegateWhenNotEventbriteUrl() {
    perform(List.of("https://not-meetup"));

    verifyNoInteractions(client);
  }
}
