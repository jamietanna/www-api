package me.jvt.www.api.micropub.validator.property;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;
import java.util.List;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Properties.PostStatus;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;

class PostStatusPropertyValidatorTest {

  private final TestMicroformats2Object mf2 = new TestMicroformats2Object();
  private final PostStatusPropertyValidator validator = new PostStatusPropertyValidator();

  @ParameterizedTest
  @EnumSource(PostStatus.class)
  void allValuesAreValidAsEnum(PostStatus postStatus) {
    boolean actual = process(postStatus);

    assertThat(actual).isTrue();
  }

  @ParameterizedTest
  @EnumSource(PostStatus.class)
  void allValuesAreValidAsString(PostStatus postStatus) {
    boolean actual = process(postStatus.name());

    assertThat(actual).isTrue();
  }

  @ParameterizedTest
  @EnumSource(PostStatus.class)
  void allValuesAreValidAsStrings(PostStatus postStatus) {
    boolean actual = process(postStatus.name());

    assertThat(actual).isTrue();
  }

  @Test
  void stringValuesThatAreNotInEnumAreNotValid() {
    boolean actual = process("foo");

    assertThat(actual).isFalse();
  }

  @ParameterizedTest
  @NullAndEmptySource
  void emptyOrNullPostStatusPropertyIsNotValid(List<PostStatus> postStatus) {
    mf2.getProperties().put("post-status", postStatus);

    boolean actual = validator.isValid(mf2, null);

    assertThat(actual).isFalse();
  }

  @Test
  void missingPostStatusPropertyIsNotValid() {
    boolean actual = validator.isValid(mf2, null);

    assertThat(actual).isFalse();
  }

  @Test
  void unknownTypeThrowsInvalidMicropubMetadataException() {
    assertThatThrownBy(() -> process((String) null))
        .isInstanceOf(InvalidMicropubMetadataRequest.class)
        .hasMessage("The post-status property was not populated with a valid value");
  }

  private boolean process(String postStatus) {
    mf2.getProperties().put("post-status", Collections.singletonList(postStatus));

    return validator.isValid(mf2, null);
  }

  private boolean process(PostStatus postStatus) {
    mf2.getProperties().put("post-status", Collections.singletonList(postStatus));

    return validator.isValid(mf2, null);
  }
}
