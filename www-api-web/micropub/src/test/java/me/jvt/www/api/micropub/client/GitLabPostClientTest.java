package me.jvt.www.api.micropub.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.micropub.client.AbstractGitLabClient.Configuration;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.gitlab.CreateCommitRequestBody;
import me.jvt.www.api.micropub.model.gitlab.CreateCommitRequestBody.Action;
import me.jvt.www.api.micropub.model.gitlab.CreateCommitRequestBody.Action.ActionType;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2ObjectFactory;
import me.jvt.www.api.micropub.service.PersistenceService;
import me.jvt.www.api.micropub.service.PersistenceService.PersistenceAction;
import me.jvt.www.api.micropub.util.MediaTypeHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

@ExtendWith(MockitoExtension.class)
class GitLabPostClientTest {

  public static final String SLUG = "the-slug";
  public static final String URL_PATH = "/the/the-slug/";
  public static final String PATH = "/path/to/file";
  public static final String COMMIT_MESSAGE = "Commit this";

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  @Mock MediaTypeHelper mockMediaTypeHelper;
  @Mock private RequestSpecificationFactory mockRequestSpecificationFactory;

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification mockRequestSpecification;

  @Mock private Response mockResponse;
  @Mock private PersistenceAction persistenceAction;
  @Mock private PersistenceService factory;

  @Mock private Microformats2ObjectFactory parser;

  private TestMicroformats2Object mf2;
  private GitLabPostClient sut;

  @BeforeEach
  void setup() {
    sut =
        new GitLabPostClient(
            new Configuration("https://gitlab.local/api/v4", "1234", "access-token"),
            mockRequestSpecificationFactory,
            factory,
            parser);
    Mockito.lenient().when(mockMediaTypeHelper.extension(any())).thenReturn(".png");
    Mockito.lenient()
        .when(mockRequestSpecificationFactory.newRequestSpecification())
        .thenReturn(mockRequestSpecification);
    Mockito.lenient().when(mockRequestSpecification.get(anyString())).thenReturn(mockResponse);
    Mockito.lenient().when(mockRequestSpecification.post(anyString())).thenReturn(mockResponse);

    mf2 = new TestMicroformats2Object();

    lenient()
        .when(factory.determineAction(any(Microformats2Object.class)))
        .thenReturn(persistenceAction);
    lenient().when(factory.serialize(any(), any())).thenReturn("This is the pretty content");
    setupPath();
  }

  private void setupPath() {
    lenient().when(this.persistenceAction.getSlug()).thenReturn(GitLabPostClientTest.SLUG);
    lenient().when(this.persistenceAction.getUrlPath()).thenReturn(GitLabPostClientTest.URL_PATH);
    lenient().when(this.persistenceAction.getPath()).thenReturn(GitLabPostClientTest.PATH);
    lenient()
        .when(this.persistenceAction.getCommitMessage())
        .thenReturn(GitLabPostClientTest.COMMIT_MESSAGE);
  }

  @Test
  void addContentCallsToGitlab() {
    // given
    mf2.setKind(Kind.bookmarks);
    when(mockResponse.getStatusCode()).thenReturn(HttpStatus.CREATED.value());

    // when
    sut.addContent(mf2);

    // then
    verify(mockRequestSpecification).baseUri("https://gitlab.local/api/v4");
    verify(mockRequestSpecification).contentType("application/json");
    verify(mockRequestSpecification).header("PRIVATE-TOKEN", "access-token");
    verify(mockRequestSpecification).post("/projects/1234/repository/commits");
    ArgumentCaptor<CreateCommitRequestBody> requestBodyArgumentCaptor =
        ArgumentCaptor.forClass(CreateCommitRequestBody.class);
    verify(mockRequestSpecification).body(requestBodyArgumentCaptor.capture());

    CreateCommitRequestBody requestBody = requestBodyArgumentCaptor.getValue();
    assertThat(requestBody).isNotNull();
    assertThat(requestBody.getBranch()).isEqualTo("main");
    assertThat(requestBody.getCommitMessage())
        .isEqualTo(String.format("MP: Add %s", COMMIT_MESSAGE));

    List<Action> actions = requestBody.getActions();
    assertThat(actions).hasSize(1);
    Action action = actions.get(0);
    assertThat(action).isNotNull();
    assertThat(action.getAction()).isEqualTo("create");
    assertThat(action.getContent()).isEqualTo("This is the pretty content\n");
    assertThat(action.getFilePath()).isEqualTo(PATH);
  }

  @Test
  void itReturnsGeneratedSlug() {
    // given
    TestMicroformats2Object mf2 = new TestMicroformats2Object();
    mf2.setKind(Kind.bookmarks);
    when(mockResponse.getStatusCode()).thenReturn(HttpStatus.CREATED.value());

    // when
    String urlPath = sut.addContent(mf2);

    // then
    assertThat(urlPath).isEqualTo(URL_PATH);
  }

  @Test
  void addContentCallsToGitlabButRetriesIfConflictFromGitLab() {
    // given
    RequestSpecification mockRequestSpecification2 =
        mock(RequestSpecification.class, Answers.RETURNS_SELF);
    when(mockRequestSpecificationFactory.newRequestSpecification())
        .thenReturn(mockRequestSpecification, mockRequestSpecification2);

    TestMicroformats2Object mf2 = new TestMicroformats2Object();
    mf2.setKind(Kind.replies);
    setUpConflict();

    Response mockResponse2 = mock(Response.class);
    when(mockRequestSpecification2.post(anyString())).thenReturn(mockResponse2);
    when(mockResponse2.getStatusCode()).thenReturn(HttpStatus.CREATED.value());

    // when
    sut.addContent(mf2);

    // then
    // verify that we hit the second
    verify(mockRequestSpecification2).post(eq("/projects/1234/repository/commits"));

    verify(factory, times(2)).determineAction(any(Microformats2Object.class));
  }

  @Test
  void addContentToGitlabThrowsRuntimeErrorIfNotSuccessOrConflictFromGitLab() {
    // given
    TestMicroformats2Object mf2 = new TestMicroformats2Object();
    mf2.setKind(Kind.replies);
    when(mockResponse.getStatusCode()).thenReturn(HttpStatus.CONFLICT.value());

    // when
    assertThatThrownBy(() -> sut.addContent(mf2))
        // then
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Was unable to process the request: HTTP 409 returned from GitLab");
  }

  @Test
  void getContentCallsGitLab() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(200);
    //    when(mockResponse.as(TestMicroformats2Object.class)).thenReturn(mf2);
    //    when(mockResponse.asString()).thenReturn("this is the body");
    when(parser.deserialize(any())).thenReturn(mf2);

    // when
    sut.getContent("content/mf2/wibble.md");

    // then
    verify(mockRequestSpecification).baseUri("https://gitlab.local/api/v4");
    verify(mockRequestSpecification).header("PRIVATE-TOKEN", "access-token");
    verify(mockRequestSpecification).urlEncodingEnabled(false);
    verify(mockRequestSpecification)
        .get("/projects/1234/repository/files/content%2Fmf2%2Fwibble.md/raw?ref=main");
  }

  @Test
  void getContentReturnsOptionalWithContentFromGitLabIfFound() {
    // given
    mf2.getContext().setClientId("https://example.local");
    when(mockResponse.asString()).thenReturn("this is the body");
    when(parser.deserialize(any())).thenReturn(mf2);
    when(mockResponse.getStatusCode()).thenReturn(200);

    // when
    Optional<Microformats2Object> optional = sut.getContent("content/mf2/wibble.md");

    // then
    assertThat(optional).isPresent();
    Microformats2Object actual = optional.get();
    assertThat(actual).isNotNull();

    assertThat(actual.getContext().getClientId()).isEqualTo("https://example.local");

    verify(parser).deserialize("this is the body");
  }

  @Test
  void getContentReturnsEmptyOptionalIf404FromGitLab() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(404);

    // when
    Optional<Microformats2Object> optional = sut.getContent("content/mf2/wibble.md");

    // then
    assertThat(optional).isNotPresent();
  }

  @Test
  void getContentThrowsRuntimeExceptionWhenUnexpectedStatusCodeReturnedFromGitLab() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(500);

    // when
    assertThatThrownBy(() -> sut.getContent("content/mf2/wibble.md"))
        // then
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Was unable to process the request: HTTP 500 returned from GitLab");
  }

  @Test
  void updateContentCallsGitLab() {
    // given
    TestMicroformats2Object mf2 = new TestMicroformats2Object();
    mf2.setKind(Kind.bookmarks);
    when(mockResponse.getStatusCode()).thenReturn(HttpStatus.CREATED.value());
    mf2.getContext().setSlug(PATH);

    // when
    sut.updateContent(mf2);

    // then
    verify(mockRequestSpecification).baseUri("https://gitlab.local/api/v4");
    verify(mockRequestSpecification).contentType("application/json");
    verify(mockRequestSpecification).header("PRIVATE-TOKEN", "access-token");
    verify(mockRequestSpecification).post("/projects/1234/repository/commits");
    ArgumentCaptor<CreateCommitRequestBody> requestBodyArgumentCaptor =
        ArgumentCaptor.forClass(CreateCommitRequestBody.class);
    verify(mockRequestSpecification).body(requestBodyArgumentCaptor.capture());

    CreateCommitRequestBody requestBody = requestBodyArgumentCaptor.getValue();
    assertThat(requestBody).isNotNull();
    assertThat(requestBody.getBranch()).isEqualTo("main");
    assertThat(requestBody.getCommitMessage())
        .isEqualTo(String.format("MP: Update %s", COMMIT_MESSAGE));

    List<Action> actions = requestBody.getActions();
    assertThat(actions).hasSize(1);
    Action action = actions.get(0);
    assertThat(action).isNotNull();
    assertThat(action.getAction()).isEqualTo("update");
    assertThat(action.getContent()).isEqualTo("This is the pretty content\n");
    assertThat(action.getFilePath()).isEqualTo(PATH);
  }

  @Test
  void updateContentReturnsSlug() {
    // given
    TestMicroformats2Object mf2 = new TestMicroformats2Object();
    mf2.setKind(Kind.bookmarks);
    mf2.getContext().setSlug("slug");
    when(mockResponse.getStatusCode()).thenReturn(HttpStatus.CREATED.value());

    // when
    String actual = sut.updateContent(mf2);

    // then
    assertThat(actual).isEqualTo("/the/the-slug/");
  }

  @Test
  void updateContentThrowsRuntimeExceptionIfGitLabDoesNotReturn200() {
    // given
    TestMicroformats2Object mf2 = new TestMicroformats2Object();
    mf2.setKind(Kind.replies);
    when(mockResponse.getStatusCode()).thenReturn(HttpStatus.CONFLICT.value());

    // when
    assertThatThrownBy(() -> sut.updateContent(mf2))
        // then
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Was unable to process the request: HTTP 409 returned from GitLab");
  }

  void setUpConflict() {
    when(mockResponse.getStatusCode()).thenReturn(HttpStatus.BAD_REQUEST.value());
    when(mockResponse.path(anyString())).thenReturn("A file with this name already exists");
  }

  @Nested
  class UpsertCite {
    @Mock private Microformats2Object.Context context;
    @Captor private ArgumentCaptor<CreateCommitRequestBody> requestBodyArgumentCaptor;

    @Test
    void returnsEmptyWhenNoContext() {
      assertThat(sut.upsertCite(context)).isEqualTo(0);
    }

    @Test
    void returnsEmptyWhenEmptyContext() {
      when(context.getContext()).thenReturn(OBJECT_MAPPER.createObjectNode());

      assertThat(sut.upsertCite(context)).isEqualTo(0);
      verifyNoInteractions(mockRequestSpecification);
    }

    @Nested
    class HappyPath {
      @Mock(answer = Answers.RETURNS_SELF)
      private RequestSpecification getFileRequest;

      @Mock private Response getFileResponse;

      @BeforeEach
      void setup() {
        when(context.getContext()).thenReturn(cite());
        when(mockResponse.getStatusCode()).thenReturn(HttpStatus.CREATED.value());
        setupUpdate(false);
      }

      @Test
      void setsBaseUri() {
        // given
        defaultAction();

        // when
        sut.upsertCite(context);

        // then
        verify(mockRequestSpecification).baseUri("https://gitlab.local/api/v4");
      }

      @Test
      void setsContentType() {
        // given
        defaultAction();

        // when
        sut.upsertCite(context);

        // then
        verify(mockRequestSpecification).contentType("application/json");
      }

      @Test
      void setsToken() {
        // given
        defaultAction();

        // when
        sut.upsertCite(context);

        // then
        verify(mockRequestSpecification).header("PRIVATE-TOKEN", "access-token");
      }

      @Test
      void posts() {
        // given
        defaultAction();

        // when
        sut.upsertCite(context);

        // then
        verify(mockRequestSpecification).post("/projects/1234/repository/commits");
      }

      @Test
      void commitsToMain() {
        // given
        defaultAction();

        // when
        sut.upsertCite(context);

        // then
        verify(mockRequestSpecification).body(requestBodyArgumentCaptor.capture());

        CreateCommitRequestBody requestBody = requestBodyArgumentCaptor.getValue();
        assertThat(requestBody.getBranch()).isEqualTo("main");
        assertThat(requestBody.getCommitMessage()).isEqualTo("MP: Upsert 1 cite");
      }

      @Test
      void hasUpdateForAllItems(@Mock PersistenceAction persistenceAction1) {
        // given
        when(factory.determineAction(any(JsonNode.class)))
            .thenReturn(Arrays.asList(persistenceAction, persistenceAction1));
        setupAction(persistenceAction, "/cites/c0.json");
        setupAction(persistenceAction1, "/cites/c1.json");
        when(factory.serialize(any(JsonNode.class))).thenReturn(Arrays.asList("cite 0", "cite 1"));

        // when
        sut.upsertCite(context);

        // then
        verify(mockRequestSpecification).body(requestBodyArgumentCaptor.capture());

        CreateCommitRequestBody requestBody = requestBodyArgumentCaptor.getValue();
        assertThat(requestBody.getCommitMessage()).isEqualTo("MP: Upsert 2 cites");

        List<Action> actions = requestBody.getActions();
        Action action0 = new Action();
        action0.setAction(ActionType.CREATE);
        action0.setContent("cite 0\n");
        action0.setFilePath("/cites/c0.json");

        Action action1 = new Action();
        action1.setAction(ActionType.CREATE);
        action1.setContent("cite 1\n");
        action1.setFilePath("/cites/c1.json");

        assertThat(actions).containsExactly(action0, action1);
      }

      @Test
      void returnsNumberOfCreatedElements(@Mock PersistenceAction persistenceAction1) {
        // given
        when(factory.determineAction(any(JsonNode.class)))
            .thenReturn(Arrays.asList(persistenceAction, persistenceAction1));
        setupAction(persistenceAction, "/cites/c0.json");
        setupAction(persistenceAction1, "/cites/c1.json");
        when(factory.serialize(any(JsonNode.class))).thenReturn(Arrays.asList("cite 0", "cite 1"));

        // when
        int count = sut.upsertCite(context);

        // then
        assertThat(count).isEqualTo(2);
      }

      @Test
      void updatesItemsThatAreFoundInRepo() {
        // given
        setupUpdate(true);
        when(factory.determineAction(any(JsonNode.class)))
            .thenReturn(Collections.singletonList(persistenceAction));
        setupAction(persistenceAction, "/cites/c0.json");
        when(factory.serialize(any(JsonNode.class)))
            .thenReturn(Collections.singletonList("cite 0"));

        // when
        sut.upsertCite(context);

        // then
        assertUpdate();
      }

      private void defaultAction() {
        when(factory.determineAction(any(JsonNode.class)))
            .thenReturn(Collections.singletonList(persistenceAction));
        setupAction(persistenceAction, "");
        when(factory.serialize(any(JsonNode.class)))
            .thenReturn(Collections.singletonList("cite 0"));
      }

      private void setupUpdate(boolean isPresent) {
        when(mockRequestSpecificationFactory.newRequestSpecification())
            .thenReturn(getFileRequest, mockRequestSpecification);

        when(getFileRequest.get(anyString())).thenReturn(getFileResponse);
        if (isPresent) {
          when(getFileResponse.getStatusCode()).thenReturn(200);
        } else {
          when(getFileResponse.getStatusCode()).thenReturn(404);
        }
      }

      private void assertUpdate() {
        verify(getFileRequest).baseUri("https://gitlab.local/api/v4");
        verify(getFileRequest).header("PRIVATE-TOKEN", "access-token");
        verify(getFileRequest).urlEncodingEnabled(false);
        verify(getFileRequest).get("/projects/1234/repository/files/%2Fcites%2Fc0.json?ref=main");

        verify(mockRequestSpecification).body(requestBodyArgumentCaptor.capture());

        CreateCommitRequestBody requestBody = requestBodyArgumentCaptor.getValue();
        List<Action> actions = requestBody.getActions();
        Action action0 = new Action();
        action0.setAction(ActionType.UPDATE);
        action0.setContent("cite 0\n");
        action0.setFilePath("/cites/c0.json");

        assertThat(actions).containsExactly(action0);
      }
    }

    @Test
    void throwsRuntimeExceptionIfGitLabDoesNotReturn200() {
      // given
      when(context.getContext()).thenReturn(cite());
      when(mockResponse.getStatusCode()).thenReturn(HttpStatus.CONFLICT.value());

      // when
      assertThatThrownBy(() -> sut.upsertCite(context))
          // then
          .isInstanceOf(RuntimeException.class)
          .hasMessage("Was unable to process the request: HTTP 409 returned from GitLab");
    }

    private ObjectNode cite() {
      ObjectNode cite = OBJECT_MAPPER.createObjectNode();

      ArrayNode items = cite.putArray("items");

      ObjectNode item = OBJECT_MAPPER.createObjectNode();
      items.add(item);
      item.putObject("properties").putArray("url").add("wibble");

      return cite;
    }

    private void setupAction(PersistenceAction action, String path) {
      when(action.getPath()).thenReturn(path);
    }
  }
}
