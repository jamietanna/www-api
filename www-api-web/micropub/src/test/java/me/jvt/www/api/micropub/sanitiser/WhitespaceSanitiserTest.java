package me.jvt.www.api.micropub.sanitiser;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import me.jvt.www.api.micropub.util.StringSanitiser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class WhitespaceSanitiserTest {

  StringSanitiser mockStringSanitiser;

  TestMicroformats2Object mf2;
  WhitespaceSanitiser sut;

  @BeforeEach
  void setup() {
    mockStringSanitiser = mock(StringSanitiser.class);

    mf2 = new TestMicroformats2Object();

    sut = new WhitespaceSanitiser(mockStringSanitiser);
    when(mockStringSanitiser.removeWhitespace(anyString())).thenReturn("sanitised-url");
  }

  @Test
  void itSanitisesBookmarkOf() {
    // given
    mf2.getProperties().setBookmarkOf(SingletonListHelper.singletonList("to-sanitise"));

    // when
    Microformats2Object actual = sut.sanitise(mf2);

    // then
    assertThat(actual.getProperties().getBookmarkOf()).containsExactly("sanitised-url");
    verify(mockStringSanitiser).removeWhitespace("to-sanitise");
  }

  @Test
  void itSanitisesLikeOf() {
    // given
    mf2.getProperties().setLikeOf(SingletonListHelper.singletonList("to-sanitise"));

    // when
    Microformats2Object actual = sut.sanitise(mf2);

    // then
    assertThat(actual.getProperties().getLikeOf()).containsExactly("sanitised-url");
    verify(mockStringSanitiser).removeWhitespace("to-sanitise");
  }

  @Test
  void itSanitisesReplyTo() {
    // given
    mf2.getProperties().setInReplyTo(SingletonListHelper.singletonList("to-sanitise"));

    // when
    Microformats2Object actual = sut.sanitise(mf2);

    // then
    assertThat(actual.getProperties().getInReplyTo()).containsExactly("sanitised-url");
    verify(mockStringSanitiser).removeWhitespace("to-sanitise");
  }

  @Test
  void itSanitisesRepostOf() {
    // given
    mf2.getProperties().setRepostOf(SingletonListHelper.singletonList("to-sanitise"));

    // when
    Microformats2Object actual = sut.sanitise(mf2);

    // then
    assertThat(actual.getProperties().getRepostOf()).containsExactly("sanitised-url");
    verify(mockStringSanitiser).removeWhitespace("to-sanitise");
  }

  @Test
  void itSanitisesContentHtml() {
    // given
    mf2.setKind(Kind.notes);
    mf2.getProperties().setContent(SingletonListHelper.singletonList(new HashMap<>()));
    mf2.getProperties().getContent().get(0).put("html", "<html> content ");

    // when
    Microformats2Object actual = sut.sanitise(mf2);

    // then
    assertThat(actual.getProperties().getContent().get(0).get("html")).isEqualTo("sanitised-url");
    verify(mockStringSanitiser).removeWhitespace("<html> content ");
  }

  @Test
  void itSanitisesContentValue() {
    // given
    mf2.setKind(Kind.notes);
    mf2.getProperties().setContent(SingletonListHelper.singletonList(new HashMap<>()));
    mf2.getProperties().getContent().get(0).put("value", " content to sanitise ");

    // when
    Microformats2Object actual = sut.sanitise(mf2);

    // then
    assertThat(actual.getProperties().getContent().get(0).get("value")).isEqualTo("sanitised-url");
    verify(mockStringSanitiser).removeWhitespace(" content to sanitise ");
  }

  @Test
  void itSanitisesName() {
    // given
    mf2.getProperties().setName(SingletonListHelper.singletonList("to-sanitise"));

    // when
    Microformats2Object actual = sut.sanitise(mf2);

    // then
    assertThat(actual.getProperties().getName()).containsExactly("sanitised-url");
    verify(mockStringSanitiser).removeWhitespace("to-sanitise");
  }

  @Test
  void itSanitisesNameAndOtherFields() {
    // given
    mf2.getProperties().setBookmarkOf(SingletonListHelper.singletonList("to-sanitise"));
    mf2.getProperties().setName(SingletonListHelper.singletonList("to-sanitise"));

    // when
    Microformats2Object actual = sut.sanitise(mf2);

    // then
    assertThat(actual.getProperties().getBookmarkOf()).containsExactly("sanitised-url");
    assertThat(actual.getProperties().getName()).containsExactly("sanitised-url");
    verify(mockStringSanitiser, times(2)).removeWhitespace("to-sanitise");
  }
}
