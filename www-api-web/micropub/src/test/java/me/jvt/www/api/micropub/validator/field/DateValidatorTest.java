package me.jvt.www.api.micropub.validator.field;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.util.stream.Stream;
import javax.validation.ConstraintValidatorContext;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DateValidatorTest {
  @Mock private ConstraintValidatorContext constraintValidatorContext;

  @InjectMocks private DateValidator validator;

  static Stream<Arguments> validDates() {
    return Stream.of(
        Arguments.of("2020-05-18T00:00:00Z"),
        Arguments.of("2020-05-19T08:00:04.406Z"),
        Arguments.of("2020-05-25T19:56:08+0000"),
        Arguments.of("2020-05-25T19:56:08+0100"),
        Arguments.of("2020-07-25T19:42:00+01:00"));
  }

  // TODO: not working? 2020-05-25T19:56:08+0000 2020-05-25T19:56:08+00:00

  static Stream<Arguments> invalidDates() {
    return Stream.of(Arguments.of("2020-05-19T17:30:00 0100"));
  }

  @ParameterizedTest
  @MethodSource("validDates")
  void validDatesReturnTrue(String date) {
    boolean isValid = validator.isValid(date, constraintValidatorContext);

    assertThat(isValid).isTrue();
  }

  @ParameterizedTest
  @MethodSource("invalidDates")
  void invalidDatesReturnTrue(String date) {
    boolean isValid = validator.isValid(date, constraintValidatorContext);

    assertThat(isValid).isFalse();
  }
}
