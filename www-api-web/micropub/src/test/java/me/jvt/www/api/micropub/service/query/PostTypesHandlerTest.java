package me.jvt.www.api.micropub.service.query;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.MicropubPostTypeDto;
import me.jvt.www.api.micropub.model.MicropubPostTypesDto;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

class PostTypesHandlerTest {

  private final PostTypesHandler handler = new PostTypesHandler();

  @Nested
  class Query {
    @Test
    void isCorrect() {
      assertThat(handler.getQuery()).isEqualTo("post-types");
    }
  }

  @Nested
  class Handle {
    @ParameterizedTest
    @EnumSource(Kind.class)
    void hasMappingsForAllKinds(Kind kind) {
      MicropubPostTypesDto actual = (MicropubPostTypesDto) handler.handle(null, Optional.empty());

      assertThat(actual.postTypes).contains(new MicropubPostTypeDto(kind));
    }
  }
}
