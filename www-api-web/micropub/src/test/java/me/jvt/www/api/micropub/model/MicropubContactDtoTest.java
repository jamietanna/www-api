package me.jvt.www.api.micropub.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collections;
import me.jvt.www.api.micropub.model.MicropubContactDto.MicropubContact;
import org.junit.jupiter.api.Test;

class MicropubContactDtoTest {

  MicropubContactDto sut;

  @Test
  void itReturnsEmpty() {
    sut = new MicropubContactDto(Collections.emptyList());

    assertThat(sut.getContacts()).isEmpty();
  }

  @Test
  void itProvidesName() {
    sut = new MicropubContactDto(Collections.singletonList(contact("", "", null)));

    assertThat(sut.getContacts().get(0).getName()).isEqualTo("Jamie");
  }

  @Test
  void itProvidesNickname() {
    sut =
        new MicropubContactDto(
            Collections.singletonList(contact("https://url.com", "jvt.me", null)));

    assertThat(sut.getContacts().get(0).getNickname()).isEqualTo("jvt.me");
  }

  @Test
  void itProvidesTwitterUrlIfNotNull() {
    sut =
        new MicropubContactDto(
            Collections.singletonList(contact("https://url.com", "jvt.me", "jamietanna")));

    assertThat(sut.getContacts().get(0).getSilos().get("twitter")).isEqualTo("jamietanna");
  }

  @Test
  void itDoesNotProvidesTwitterUrlIfNull() {
    sut =
        new MicropubContactDto(
            Collections.singletonList(contact("https://url.com", "jvt.me", null)));

    assertThat(sut.getContacts().get(0).getSilos()).doesNotContainKey("twitter");
  }

  @Test
  void itProvidesUrl() {
    sut = new MicropubContactDto(Collections.singletonList(contact("https://url.com", "", null)));

    assertThat(sut.getContacts().get(0).getUrl()).isEqualTo("https://url.com");
  }

  @Test
  void itIgnoresEmptyObject() {
    sut = new MicropubContactDto(Collections.singletonList(new MicropubContact()));

    assertThat(sut.getContacts()).isEmpty();
  }

  @Test
  void itReturnsEmptyMapWhenNull() {
    MicropubContact sut = new MicropubContact();
    sut.setSilos(null);

    assertThat(sut.getSilos()).isEmpty();
  }

  private MicropubContact contact(String url, String nickname, String twitterUrl) {
    MicropubContact contact = new MicropubContact();
    contact.setName("Jamie");
    contact.setNickname(nickname);
    contact.setUrl(url);

    if (null != twitterUrl) {
      contact.setSilos(Collections.singletonMap("twitter", twitterUrl));
    }

    return contact;
  }
}
