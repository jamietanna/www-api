package me.jvt.www.api.micropub.validator.field;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;
import javax.validation.ConstraintValidatorContext;
import org.hibernate.validator.internal.constraintvalidators.hv.URLValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HasPhotoUrlValidatorTest {

  private final Map<String, String> photo = new HashMap<>();
  @Mock private ConstraintValidatorContext constraintValidatorContext;
  @Mock private URLValidator delegate;
  @InjectMocks private HasPhotoUrlValidator validator;

  @ParameterizedTest
  @NullAndEmptySource
  void itReturnsFalseIfNotPresent(Map<String, String> photo) {
    boolean isValid = validator.isValid(photo, constraintValidatorContext);

    assertThat(isValid).isFalse();
  }

  @ParameterizedTest
  @NullAndEmptySource
  void itReturnsFalseWhenPhotoPropertyIsNullOrEmpty(String photoProperty) {
    photo.put("photo", photoProperty);

    boolean isValid = validator.isValid(photo, constraintValidatorContext);

    assertThat(isValid).isFalse();
  }

  @Test
  void itDelegatesToUrlValidator() {
    photo.put("photo", "something");

    validator.isValid(photo, constraintValidatorContext);

    verify(delegate).isValid("something", constraintValidatorContext);
  }

  @ParameterizedTest
  @ValueSource(booleans = {true, false})
  void itReturnsFromDelegate(boolean isValid) {
    photo.put("photo", "something");
    when(delegate.isValid(any(), any())).thenReturn(isValid);

    boolean actual = validator.isValid(photo, constraintValidatorContext);

    assertThat(actual).isEqualTo(isValid);
  }
}
