package me.jvt.www.api.micropub.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import me.jvt.www.api.micropub.client.ContentDeduplicationClient;
import me.jvt.www.api.micropub.model.ContentDeduplicationResult;
import me.jvt.www.api.micropub.model.ContentDeduplicationResult.ContentDeduplicationEntry;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class MicropubConflictServiceImplTest {

  ContentDeduplicationResult publicDeduplication;
  TestMicroformats2Object mf2;
  MicropubConflictServiceImpl sut;
  private ContentDeduplicationClient mockContentDeduplicationClient;

  static Stream<Arguments> mf2s() {
    TestMicroformats2Object bookmark = new TestMicroformats2Object();
    bookmark.setKind(Kind.bookmarks);
    bookmark.getProperties().setBookmarkOf(SingletonListHelper.singletonList("https://foo.bar"));

    TestMicroformats2Object like = new TestMicroformats2Object();
    like.setKind(Kind.likes);
    like.getProperties().setLikeOf(SingletonListHelper.singletonList("https://foo.bar"));

    TestMicroformats2Object repost = new TestMicroformats2Object();
    repost.setKind(Kind.reposts);
    repost.getProperties().setRepostOf(SingletonListHelper.singletonList("https://foo.bar"));

    TestMicroformats2Object rsvp = new TestMicroformats2Object();
    rsvp.setKind(Kind.rsvps);
    rsvp.getProperties().setInReplyTo(SingletonListHelper.singletonList("https://foo.bar"));

    return Stream.of(
        Arguments.of(bookmark), Arguments.of(like), Arguments.of(repost), Arguments.of(rsvp));
  }

  static Stream<Arguments> ignoredMf2s() {
    TestMicroformats2Object reply = new TestMicroformats2Object();
    reply.setKind(Kind.replies);
    reply.getProperties().setInReplyTo(SingletonListHelper.singletonList("https://foo.bar"));

    TestMicroformats2Object note = new TestMicroformats2Object();
    note.setKind(Kind.notes);

    TestMicroformats2Object step = new TestMicroformats2Object();
    step.setKind(Kind.steps);

    return Stream.of(Arguments.of(reply), Arguments.of(note), Arguments.of(step));
  }

  @BeforeEach
  void setup() {
    mockContentDeduplicationClient = mock(ContentDeduplicationClient.class);
    publicDeduplication = new ContentDeduplicationResult();
    when(mockContentDeduplicationClient.retrieveContentDeduplicationData())
        .thenReturn(publicDeduplication);

    mf2 = new TestMicroformats2Object();
    mf2.setKind(Kind.bookmarks);
    mf2.getProperties().setBookmarkOf(SingletonListHelper.singletonList("https://foo.bar"));

    sut = new MicropubConflictServiceImpl(mockContentDeduplicationClient);
  }

  @Test
  void isConflictFoundInCacheReturnsEmptyWhenNothingFoundInPublicOrInMemory() {
    // given
    publicDeduplication.clear();
    // in-memory will be clear

    // when
    Optional<String> maybeUrl = sut.isConflictFoundInCache(mf2);

    // then
    assertThat(maybeUrl).isNotPresent();
  }

  @Test
  void isConflictFoundInCacheDelegatesToClient() {
    // given

    // when
    sut.isConflictFoundInCache(mf2);

    // then
    verify(mockContentDeduplicationClient).retrieveContentDeduplicationData();
  }

  @ParameterizedTest
  @MethodSource("ignoredMf2s")
  void isConflictFoundInCacheIgnoresCertainKinds(Microformats2Object mf2) {
    // given

    // when
    sut.isConflictFoundInCache(mf2);

    // then
    verify(mockContentDeduplicationClient, times(0)).retrieveContentDeduplicationData();
  }

  @ParameterizedTest
  @MethodSource("mf2s")
  void isConflictFoundInCacheReturnsUrlWhenUrlFoundInPublic(Microformats2Object mf2) {
    // given
    List<ContentDeduplicationEntry> mf2ResultList = new ArrayList<>();
    ContentDeduplicationEntry entry =
        new ContentDeduplicationEntry(mf2.getKind(), "https://jvt.example/post/slug/");
    mf2ResultList.add(entry);
    publicDeduplication.put("https://foo.bar", mf2ResultList);
    // in-memory will be clear

    // when
    Optional<String> maybeUrl = sut.isConflictFoundInCache(mf2);

    // then
    assertThat(maybeUrl).isPresent();
    assertThat(maybeUrl.get()).isEqualTo("https://jvt.example/post/slug/");
  }

  @ParameterizedTest
  @MethodSource("mf2s")
  void isConflictFoundInCacheReturnsUrlWhenUrlFoundInCache(Microformats2Object mf2) {
    // given
    publicDeduplication.clear();
    sut.updateCacheWithSlug(mf2, "https://slug");

    // when
    Optional<String> maybeUrl = sut.isConflictFoundInCache(mf2);

    // then
    assertThat(maybeUrl).isPresent();
    assertThat(maybeUrl.get()).isEqualTo("https://slug");
  }

  @ParameterizedTest
  @MethodSource("ignoredMf2s")
  void updateCacheWithSlugIgnoresCertainKinds(Microformats2Object mf2) {
    // given

    // when
    sut.updateCacheWithSlug(mf2, "slug");

    // then
    // no exception
  }
}
