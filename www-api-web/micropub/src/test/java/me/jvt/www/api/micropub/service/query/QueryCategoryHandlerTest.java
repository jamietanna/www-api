package me.jvt.www.api.micropub.service.query;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.Set;
import me.jvt.www.api.micropub.model.MicropubCategoriesDto;
import me.jvt.www.api.micropub.service.MicropubService;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class QueryCategoryHandlerTest {

  @Mock private MicropubService service;

  @InjectMocks private QueryCategoryHandler handler;

  @Nested
  class Query {

    @Test
    void isCorrect() {
      assertThat(handler.getQuery()).isEqualTo("category");
    }
  }

  @Nested
  class Handle {
    @Test
    void itReturnsFromDelegate(@Mock Set<String> categories) {
      when(service.retrieveCategories()).thenReturn(categories);

      MicropubCategoriesDto actual = (MicropubCategoriesDto) handler.handle(null, Optional.empty());

      assertThat(actual.getCategories()).isSameAs(categories);
    }
  }
}
