package me.jvt.www.api.micropub.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.stream.Stream;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.MediaType;

class MediaTypeHelperTest {
  MediaTypeHelper sut;

  private static Stream<Arguments> validMediaTypes() {
    return Stream.of(
        Arguments.of(MediaType.IMAGE_GIF),
        Arguments.of(MediaType.IMAGE_JPEG),
        Arguments.of(MediaType.IMAGE_PNG));
  }

  @BeforeEach
  void setup() {
    sut = new MediaTypeHelper();
  }

  @Test
  void extensionMapsGif() {
    String actual = sut.extension(MediaType.IMAGE_GIF);

    assertThat(actual).isEqualTo(".gif");
  }

  @Test
  void extensionMapsJpeg() {
    String actual = sut.extension(MediaType.IMAGE_JPEG);

    assertThat(actual).isEqualTo(".jpeg");
  }

  @Test
  void extensionMapsPng() {
    String actual = sut.extension(MediaType.IMAGE_PNG);

    assertThat(actual).isEqualTo(".png");
  }

  @Test
  void otherwiseExtensionThrowsInvalidMicropubMetadataRequest() {
    assertThatThrownBy(() -> sut.extension(MediaType.TEXT_PLAIN))
        .isInstanceOf(InvalidMicropubMetadataRequest.class)
        .hasMessage("Media Type not supported");
  }

  @ParameterizedTest
  @MethodSource("validMediaTypes")
  void validMediaTypesAreValid(MediaType mediaType) {
    assertThat(sut.validate(mediaType)).isTrue();
  }

  @Test
  void invalidMediaTypesAreNotValid() {
    assertThat(sut.validate(MediaType.TEXT_PLAIN)).isFalse();
  }
}
