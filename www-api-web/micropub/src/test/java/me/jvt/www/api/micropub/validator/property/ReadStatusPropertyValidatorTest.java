package me.jvt.www.api.micropub.validator.property;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Collections;
import java.util.List;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Properties.ReadStatus;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;

class ReadStatusPropertyValidatorTest {

  private final TestMicroformats2Object mf2 = new TestMicroformats2Object();
  private final ReadStatusPropertyValidator validator = new ReadStatusPropertyValidator();

  @ParameterizedTest
  @EnumSource(ReadStatus.class)
  void allValuesAreValidAsEnum(ReadStatus readStatus) {
    boolean actual = process(readStatus);

    assertThat(actual).isTrue();
  }

  @ParameterizedTest
  @EnumSource(ReadStatus.class)
  void allValuesAreValidAsString(ReadStatus readStatus) {
    boolean actual = process(readStatus.getProperty());

    assertThat(actual).isTrue();
  }

  @ParameterizedTest
  @EnumSource(ReadStatus.class)
  void allValuesAreValidAsStrings(ReadStatus readStatus) {
    boolean actual = process(readStatus.name());

    assertThat(actual).isTrue();
  }

  @Test
  void stringValuesThatAreNotInEnumAreNotValid() {
    boolean actual = process("foo");

    assertThat(actual).isFalse();
  }

  @ParameterizedTest
  @NullAndEmptySource
  void emptyOrNullReadStatusPropertyIsNotValid(List<ReadStatus> readStatus) {
    mf2.getProperties().put("read-status", readStatus);

    boolean actual = validator.isValid(mf2, null);

    assertThat(actual).isFalse();
  }

  @Test
  void missingReadStatusPropertyIsNotValid() {
    boolean actual = validator.isValid(mf2, null);

    assertThat(actual).isFalse();
  }

  @Test
  void unknownTypeThrowsInvalidMicropubMetadataException() {
    assertThatThrownBy(() -> process((String) null))
        .isInstanceOf(InvalidMicropubMetadataRequest.class)
        .hasMessage("The read-status property was not populated with a valid value");
  }

  private boolean process(String readStatus) {
    mf2.getProperties().put("read-status", Collections.singletonList(readStatus));

    return validator.isValid(mf2, null);
  }

  private boolean process(ReadStatus readStatus) {
    mf2.getProperties().setReadStatus(Collections.singletonList(readStatus));

    return validator.isValid(mf2, null);
  }
}
