package me.jvt.www.api.micropub.sanitiser;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;

class UniqueCategorySanitiserTest {

  private UniqueCategorySanitiser sut;
  private TestMicroformats2Object mf2;

  @BeforeEach
  void setup() {
    mf2 = new TestMicroformats2Object();
    sut = new UniqueCategorySanitiser();
  }

  @ParameterizedTest
  @NullAndEmptySource
  void itDoesNothingIfCategoriesAreNotSet(List<String> categories) {
    // given
    mf2.getProperties().setCategory(categories);

    // when
    sut.sanitise(mf2);

    // then
    assertThat(mf2.getProperties().getCategory()).isEqualTo(categories);
  }

  @Test
  void itRemovesDuplicates() {
    // given
    mf2.getProperties().setCategory(new ArrayList<>());
    mf2.getProperties().getCategory().add("hello");
    mf2.getProperties().getCategory().add("no");
    mf2.getProperties().getCategory().add("hello");

    // when
    sut.sanitise(mf2);

    // then
    assertThat(mf2.getProperties().getCategory()).containsExactly("hello", "no");
  }

  @Test
  void itReturnsTheMf2Object() {
    // given

    // when
    Microformats2Object actual = sut.sanitise(mf2);

    // then
    assertThat(mf2).isSameAs(actual);
  }
}
