package me.jvt.www.api.micropub.validator.property;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;

class ReadOfPropertyValidatorTest {

  private final TestMicroformats2Object mf2 = new TestMicroformats2Object();
  private final ReadOfPropertyValidator validator = new ReadOfPropertyValidator();

  @Nested
  class StringValue {
    @ParameterizedTest
    @NullAndEmptySource
    void nullOrEmptyListAreNotValid(List<Object> list) {
      set(list);

      expectFail();
    }

    @ParameterizedTest
    @NullAndEmptySource
    void nullOrEmptyStringValuesAreNotValid(String value) {
      set(value);

      expectFail();
    }

    @Test
    void nonEmptyStringIsValid() {
      set("value");

      expectSuccess();
    }
  }

  @Nested
  class Hcite {

    private final Map<String, Object> properties = new Properties();
    private final Map<String, Object> hcite = new HashMap<>();

    @BeforeEach
    void setup() {
      properties.put("name", Collections.singletonList("non-empty"));
      properties.put("author", Collections.singletonList("non-empty"));
      properties.put("uid", Collections.singletonList("non-empty"));

      hcite.put("type", Collections.singletonList("h-cite"));
      hcite.put("properties", properties);

      mf2.getProperties().setReadOf(Collections.singletonList(hcite));
    }

    @Test
    void typeMustBeHcite() {
      hcite.put("type", Collections.singletonList("h-wibble"));

      set(hcite);

      expectFail();
    }

    @Test
    void propertiesMustBeSetInHcite() {
      hcite.remove("properties");

      expectFail();
    }

    @ParameterizedTest
    @NullAndEmptySource
    void nameMustBeNotNullOrEmptyList(List<String> list) {
      properties.put("name", list);

      expectFail();
    }

    @ParameterizedTest
    @NullAndEmptySource
    void authorMustBeNotNullOrEmptyList(List<String> list) {
      properties.put("author", list);

      expectFail();
    }

    @ParameterizedTest
    @NullAndEmptySource
    void ifUidIsPresentItMustBeNotNullOrEmptyList(List<String> list) {
      properties.put("uid", list);

      expectFail();
    }

    @Test
    void uidIsRequired() {
      properties.remove("uid");

      expectFail();
    }

    @Test
    void otherwiseItValidates() {
      expectSuccess();
    }
  }

  @Nested
  class Unknown {

    @Test
    void throwsInvalidMicropubMetadataException() {
      mf2.getProperties().setReadOf(Collections.singletonList(Collections.singletonList("wibble")));

      assertThatThrownBy(() -> validator.isValid(mf2, null))
          .isInstanceOf(InvalidMicropubMetadataRequest.class)
          .hasMessage("The read-of property was not populated with a valid value");
    }
  }

  private void expectFail() {
    boolean actual = validator.isValid(mf2, null);

    assertThat(actual).isFalse();
  }

  private void expectSuccess() {
    boolean actual = validator.isValid(mf2, null);

    assertThat(actual).isTrue();
  }

  private void set(Object readOf) {
    set(Collections.singletonList(readOf));
  }

  private void set(List<Object> readOf) {
    mf2.getProperties().setReadOf(readOf);
  }
}
