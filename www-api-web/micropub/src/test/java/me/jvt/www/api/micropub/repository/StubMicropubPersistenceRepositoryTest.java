package me.jvt.www.api.micropub.repository;

import static com.github.valfirst.slf4jtest.LoggingEvent.info;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.valfirst.slf4jtest.TestLogger;
import com.github.valfirst.slf4jtest.TestLoggerFactory;
import java.util.Arrays;
import java.util.Optional;
import me.jvt.www.api.micropub.client.PostClient;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.service.MicropubConflictService;
import me.jvt.www.api.micropub.service.PersistenceService;
import me.jvt.www.api.micropub.service.PersistenceService.PersistenceAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
class StubMicropubPersistenceRepositoryTest {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  @Mock PostClient mockClient;
  @Mock MicropubConflictService mockMicropubConflictService;

  @Mock PersistenceService persistenceService;
  @Mock PersistenceAction action;

  @Mock MultipartFile mockFile;

  private final TestLogger logger =
      TestLoggerFactory.getTestLogger(StubMicropubPersistenceRepository.class);
  final TestMicroformats2Object mf2 = new TestMicroformats2Object();
  StubMicropubPersistenceRepository sut;

  @BeforeEach
  void setup() {
    sut =
        new StubMicropubPersistenceRepository(
            persistenceService, mockMicropubConflictService, mockClient);

    lenient()
        .when(persistenceService.determineAction(any(Microformats2Object.class)))
        .thenReturn(action);
    lenient().when(persistenceService.serialize(any(), any())).thenReturn("b64-encoded-str");
    lenient()
        .when(persistenceService.serialize(any()))
        .thenReturn(Arrays.asList("b64-encoded-cite", "b64-encoded-cite-2"));
  }

  @Test
  void saveDoesNotCheckForConflictsIfASlugIsPresent() {
    // given
    mf2.getContext().setSlug("something");

    // when
    sut.save(mf2);

    // then
    verify(mockMicropubConflictService, times(0)).isConflictFoundInCache(any());
  }

  @Test
  void saveLogsUpdateStringIfASlugIsPresent() {
    // given
    mf2.getContext().setSlug("something");

    // when
    sut.save(mf2);

    // then
    assertThat(logger.getLoggingEvents())
        .contains(
            info(
                "Received request to create new object with encoded body {}",
                "YjY0LWVuY29kZWQtc3Ry"));
  }

  @Test
  void saveReturnsBase64EncodedValueOUpdateOfPost() {
    // given
    mf2.getContext().setSlug("something");

    // when
    String actual = sut.save(mf2);

    // then
    assertThat(actual).isEqualTo("YjY0LWVuY29kZWQtc3Ry");
  }

  @Test
  void saveChecksForConflicts() {
    // given

    // when
    sut.save(mf2);

    // then
    verify(mockMicropubConflictService).isConflictFoundInCache(mf2);
  }

  @Test
  void saveChecksForConflictsReturnsSlugIfExists() {
    // given
    when(mockMicropubConflictService.isConflictFoundInCache(mf2)).thenReturn(Optional.of("/slug/"));

    // when
    String actual = sut.save(mf2);

    // then
    assertThat(actual).isEqualTo("/slug/");
  }

  @Test
  void itConvertsHugoMf2ToStringOnSaveOfPost() {
    // given

    // when
    sut.save(mf2);

    // then
    verify(persistenceService).serialize(any(), eq(mf2));
  }

  @Test
  void itReturnsBase64EncodedValueOnSaveOfPost() {
    // given

    // when
    String actual = sut.save(mf2);

    // then
    assertThat(actual).isEqualTo("YjY0LWVuY29kZWQtc3Ry");
  }

  @Test
  void saveUpdatesCacheAfterSavingInClient() {
    // given

    // when
    sut.save(mf2);

    // then
    verify(mockMicropubConflictService).updateCacheWithSlug(mf2, "YjY0LWVuY29kZWQtc3Ry");
  }

  @Test
  void itLogsPostOnSaveOfPost() {
    // given

    // when
    sut.save(mf2);

    // then
    assertThat(logger.getLoggingEvents())
        .contains(
            info(
                "Received request to create new object with encoded body {}",
                "YjY0LWVuY29kZWQtc3Ry"));
  }

  @Test
  void itLogsContextsOnSaveOfPost(@Mock JsonNode context) {
    // given
    mf2.getContext().setContext(context);

    // when
    sut.save(mf2);

    // then
    assertThat(logger.getLoggingEvents())
        .contains(
            info(
                "Received request to upsert cite with encoded body {}", "YjY0LWVuY29kZWQtY2l0ZQ=="),
            info(
                "Received request to upsert cite with encoded body {}",
                "YjY0LWVuY29kZWQtY2l0ZS0y"));
  }

  @Test
  void itDoesNotAttemptToSerializeNullContext() {
    mf2.getContext().setContext(null);

    sut.save(mf2);

    verify(persistenceService, times(0)).serialize(any());
  }

  @Test
  void itReturnsTheFilenameOnSave() {
    // given
    when(mockFile.getOriginalFilename()).thenReturn("wibble.jpg");

    // when
    String actual = sut.save(mockFile, MediaType.IMAGE_JPEG);

    // then
    assertThat(actual).isEqualTo("wibble.jpg");
  }

  @Test
  void itLogsOnSaveOfFile() {
    // given
    when(mockFile.getOriginalFilename()).thenReturn("wobble.gif");

    // when
    sut.save(mockFile, MediaType.IMAGE_PNG);

    // then
    assertThat(logger.getLoggingEvents())
        .contains(
            info(
                "Received request to save a file with original filename {} and content type {}",
                "wobble.gif",
                "image/png"));
  }

  @Test
  void findByIdDelegatesToGitLabClient() {
    // given
    Optional<Microformats2Object> optional = Optional.of(mf2);
    when(mockClient.getContent(any())).thenReturn(optional);

    // when
    Optional<Microformats2Object> actualOptional = sut.findById("wibble");

    // then
    verify(mockClient).getContent("wibble");
    assertThat(actualOptional).isPresent();
    assertThat(actualOptional.get()).isSameAs(mf2);
  }
}
