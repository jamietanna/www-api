package me.jvt.www.api.micropub.model.posttype;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import me.jvt.www.api.micropub.model.Kind;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DefaultMicroformats2ObjectParserTest {

  private final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  @Mock private PostTypeDiscoverer discoverer;

  private DefaultMicroformats2ObjectParser parser;

  @BeforeEach
  void setup() {
    parser = new DefaultMicroformats2ObjectParser(OBJECT_MAPPER, discoverer);
  }

  @Nested
  class HugoJson {
    private final ObjectNode node = OBJECT_MAPPER.createObjectNode();
    private final ObjectNode context = node.putObject("context");
    private final ObjectNode properties = node.putObject("properties");
    private Microformats2Object actual;

    @BeforeEach
    void setup() {
      when(discoverer.discover(any(), any())).thenReturn(Kind.likes);

      node.putArray("aliases").add("/1/").add("/2/");
      node.put("client_id", "https://client");
      node.put("date", "the-date");
      node.put("deleted", true);
      node.put("h", "h-wibble");
      node.put("kind", "likes");
      node.put("slug", "/the/slug/");

      context.putArray("wibble").add("hello");

      properties.putArray("published").add("the-date");
      properties.putArray("category").add("indieweb").add("micropub");
      properties.putArray("in-reply-to").add("https://");

      process();
    }

    @Test
    void aliases() {
      assertThat(actual.getContext().getAliases()).containsExactly("/1/", "/2/");
    }

    @Test
    void aliasesNotPresent() {
      node.remove("aliases");

      process();

      assertThat(actual.getContext().getAliases()).isNull();
    }

    @Test
    void clientId() {
      assertThat(actual.getContext().getClientId()).isEqualTo("https://client");
    }

    @Test
    void clientIdNotPresent() {
      node.remove("client_id");

      process();

      assertThat(actual.getContext().getClientId()).isNull();
    }

    @Test
    void dateIsTakenFromProperties() {
      assertThat(actual.getProperties().getPublished()).containsExactly("the-date");
    }

    @Test
    void deletedDefaultsToFalse() {
      node.remove("deleted");

      process();

      assertThat(actual.getContext().isDeleted()).isFalse();
    }

    @Test
    void deletedWhenPresent() {
      assertThat(actual.getContext().isDeleted()).isTrue();
    }

    @Test
    void h() {
      assertThat(actual.getContext().getH()).isEqualTo("h-wibble");
    }

    @Test
    void hNotPresent() {
      node.remove("h");

      process();

      assertThat(actual.getContext().getH()).isNull();
    }

    /*
     * Kind is discovered, so we don't need to test deserialization.
     */

    @Test
    void properties() {
      assertThat(actual.getProperties().getInReplyTo()).containsExactly("https://");
    }

    @Test
    void slug() {
      assertThat(actual.getContext().getSlug()).isEqualTo("/the/slug/");
    }

    @Test
    void slugNotPresent() {
      node.remove("slug");

      process();

      assertThat(actual.getContext().getSlug()).isNull();
    }

    @Test
    void tagsAreTakenFromProperties() {
      assertThat(actual.getProperties().getCategory()).containsExactly("indieweb", "micropub");
    }

    private void process() {
      try {
        actual = parser.deserialize(OBJECT_MAPPER.writeValueAsString(node));
      } catch (JsonProcessingException e) {
        throw new RuntimeException(e);
      }
    }
  }
}
