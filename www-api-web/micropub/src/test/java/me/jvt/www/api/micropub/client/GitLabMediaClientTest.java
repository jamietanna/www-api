package me.jvt.www.api.micropub.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.io.IOException;
import java.util.List;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.micropub.client.AbstractGitLabClient.Configuration;
import me.jvt.www.api.micropub.model.gitlab.CreateCommitRequestBody;
import me.jvt.www.api.micropub.model.gitlab.CreateCommitRequestBody.Action;
import me.jvt.www.api.micropub.util.MediaTypeHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
class GitLabMediaClientTest {
  @Mock MediaTypeHelper mockMediaTypeHelper;
  @Mock private RequestSpecificationFactory mockRequestSpecificationFactory;

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification mockRequestSpecification;

  @Mock private Response mockResponse;

  private GitLabMediaClient sut;

  @BeforeEach
  void setup() {
    sut =
        new GitLabMediaClient(
            new Configuration("https://gitlab.local/api/v4", "1234", "access-token"),
            mockRequestSpecificationFactory,
            mockMediaTypeHelper);

    Mockito.lenient().when(mockMediaTypeHelper.extension(any())).thenReturn(".png");
    Mockito.lenient()
        .when(mockRequestSpecificationFactory.newRequestSpecification())
        .thenReturn(mockRequestSpecification);
    Mockito.lenient().when(mockRequestSpecification.post(anyString())).thenReturn(mockResponse);
  }

  @Test
  void addContentWithMediaCallsToGitlab() throws Exception {
    // given
    MultipartFile multipartFile = mock(MultipartFile.class);
    when(multipartFile.getBytes()).thenReturn("test".getBytes());

    when(mockResponse.getStatusCode()).thenReturn(HttpStatus.CREATED.value());

    // when
    sut.addContent(multipartFile, MediaType.IMAGE_PNG);

    // then
    verify(mockRequestSpecification).baseUri("https://gitlab.local/api/v4");
    verify(mockRequestSpecification).contentType("application/json");
    verify(mockRequestSpecification).header("PRIVATE-TOKEN", "access-token");
    verify(mockRequestSpecification).post("/projects/1234/repository/commits");
    ArgumentCaptor<CreateCommitRequestBody> requestBodyArgumentCaptor =
        ArgumentCaptor.forClass(CreateCommitRequestBody.class);
    verify(mockRequestSpecification).body(requestBodyArgumentCaptor.capture());

    CreateCommitRequestBody requestBody = requestBodyArgumentCaptor.getValue();
    assertThat(requestBody).isNotNull();
    assertThat(requestBody.getBranch()).isEqualTo("main");
    assertThat(requestBody.getCommitMessage()).isEqualTo("MP: Add file from media endpoint");

    List<Action> actions = requestBody.getActions();
    assertThat(actions).hasSize(1);
    Action action = actions.get(0);
    assertThat(action).isNotNull();
    assertThat(action.getAction()).isEqualTo("create");
    assertThat(action.getContent()).isEqualTo("dGVzdA==");
    assertThat(action.getEncoding()).isEqualTo("base64");
    assertThat(action.getFilePath()).isEqualTo("public/2b200a668f.png");

    verify(mockMediaTypeHelper).extension(MediaType.IMAGE_PNG);
  }

  @Test
  void itReturnsGeneratedSlugForMedia() throws Exception {
    // given
    MultipartFile multipartFile = mock(MultipartFile.class);
    when(multipartFile.getBytes()).thenReturn("test".getBytes());

    when(mockResponse.getStatusCode()).thenReturn(HttpStatus.CREATED.value());

    // when
    String slug = sut.addContent(multipartFile, MediaType.IMAGE_PNG);

    // then
    assertThat(slug).isEqualTo("2b200a668f.png");
  }

  @Test
  void addContentForMediaEndpointDoesNotThrowWhen400ResponseFromGitLabIfFileAlreadyExists()
      throws Exception {
    // given
    MultipartFile multipartFile = mock(MultipartFile.class);
    when(multipartFile.getBytes()).thenReturn("test".getBytes());
    setUpConflict();

    // when
    String slug = sut.addContent(multipartFile, MediaType.IMAGE_PNG);

    // then
    assertThat(slug).isEqualTo("2b200a668f.png");
    verify(mockResponse).path("message");
  }

  @Test
  void addContentForMediaEndpointThrowsWhen400ResponseFromGitLabButNotFileAlreadyExists()
      throws Exception {
    // given
    MultipartFile multipartFile = mock(MultipartFile.class);
    when(multipartFile.getBytes()).thenReturn("test".getBytes());

    when(mockResponse.getStatusCode()).thenReturn(HttpStatus.BAD_REQUEST.value());
    when(mockResponse.path(anyString())).thenReturn("Not a conflict error message");

    // when
    assertThatThrownBy(() -> sut.addContent(multipartFile, MediaType.IMAGE_PNG))
        // then
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Was unable to process the request: HTTP 400 returned from GitLab");
  }

  @Test
  void addContentForMediaEndpointThrowsRuntimeExceptionIfCannotProcessFile() throws IOException {
    // given

    MultipartFile multipartFile = mock(MultipartFile.class);
    IOException exceptionToThrow = new IOException("Something went wrong");
    when(multipartFile.getBytes()).thenThrow(exceptionToThrow);

    // when
    assertThatThrownBy(() -> sut.addContent(multipartFile, MediaType.IMAGE_PNG))
        // then
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Was unable to parse the bytes from the MultipartFile")
        .hasCause(exceptionToThrow);
  }

  @Test
  void addContentForMediaEndpointThrowsRuntimeExceptionIfNotCreatedStatusCode() throws IOException {
    // given
    RequestSpecification mockRequestSpecification2 =
        mock(RequestSpecification.class, Answers.RETURNS_SELF);
    when(mockRequestSpecificationFactory.newRequestSpecification())
        .thenReturn(mockRequestSpecification, mockRequestSpecification2);

    MultipartFile multipartFile = mock(MultipartFile.class);
    when(multipartFile.getBytes()).thenReturn("test".getBytes());

    when(mockResponse.getStatusCode()).thenReturn(HttpStatus.CONFLICT.value());

    // when
    assertThatThrownBy(() -> sut.addContent(multipartFile, MediaType.IMAGE_PNG))
        // then
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Was unable to process the request: HTTP 409 returned from GitLab");
  }

  void setUpConflict() {
    when(mockResponse.getStatusCode()).thenReturn(HttpStatus.BAD_REQUEST.value());
    when(mockResponse.path(anyString())).thenReturn("A file with this name already exists");
  }
}
