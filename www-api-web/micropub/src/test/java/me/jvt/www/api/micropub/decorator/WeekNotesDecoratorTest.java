package me.jvt.www.api.micropub.decorator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.EnumSource.Mode;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class WeekNotesDecoratorTest {

  private final TestMicroformats2Object mf2 = new TestMicroformats2Object();
  private final WeekNotesDecorator decorator = new WeekNotesDecorator();

  @ParameterizedTest
  @EnumSource(value = Kind.class, names = "articles", mode = Mode.EXCLUDE)
  void itIgnoresOtherKinds(Kind kind, @Mock Microformats2Object mf2) {
    when(mf2.getKind()).thenReturn(kind);

    decorator.decorate(mf2);

    verify(mf2).getKind();
    verifyNoMoreInteractions(mf2);
  }

  @Nested
  class WhenArticle {
    private Microformats2Object actual;

    @BeforeEach
    void setup() {
      mf2.setKind(Kind.articles);
    }

    @ParameterizedTest
    @NullAndEmptySource
    void itThrowsInvalidMicropubMetadataRequestWhenNoTitleProperty(List<String> name) {
      mf2.getProperties().setName(name);

      assertThatThrownBy(() -> decorator.decorate(mf2))
          .isInstanceOf(InvalidMicropubMetadataRequest.class)
          .hasMessage("Articles require a title");
    }

    @Test
    void itThrowsInvalidMicropubMetadataRequestWhenEmptyTitle() {
      mf2.getProperties().setName(Collections.singletonList(""));

      assertThatThrownBy(() -> decorator.decorate(mf2))
          .isInstanceOf(InvalidMicropubMetadataRequest.class)
          .hasMessage("Articles require a title");
    }

    @Nested
    class WhenWeekNotesTitle {

      @Test
      void itSetsSummaryFromParsedDate() {
        processDefault();

        assertThat(mf2.getProperties().getSummary())
            .containsExactly("What happened in the week of 2021-02-08?");
      }

      @Test
      void itSetsPostStatusAsDraft() {
        processDefault();

        assertThat(mf2.getProperties().getPostStatus()).containsExactly("draft");
      }

      @Test
      void itSetsPublishedFromParsedDateAt2300() {
        processDefault();

        assertThat(mf2.getProperties().getPublished()).containsExactly("2021-02-14T23:00:00Z");
      }

      @Test
      void itSetsSlug() {
        processDefault();

        assertThat(mf2.getContext().getSlug()).isEqualTo("week-notes/2021/06");
      }

      @Test
      void itOverridesExistingSummary() {
        mf2.getProperties().setSummary(Collections.singletonList("ignored"));

        processDefault();

        assertThat(mf2.getProperties().getSummary()).doesNotContain("ignored");
      }

      @Test
      void itDoesNotOverridePostStatus() {
        mf2.getProperties().setPostStatus(Collections.singletonList("foo"));

        processDefault();

        assertThat(mf2.getProperties().getPostStatus()).containsExactly("foo");
      }

      @Test
      void itDoesNotOverwritePublished() {
        mf2.getProperties().setPublished(Collections.singletonList("ignored"));

        processDefault();

        assertThat(mf2.getProperties().getPublished()).contains("ignored");
      }

      @Test
      void itIgnoresCaseInsensitiveName() {
        mf2.getProperties().setName(Collections.singletonList("week nOTEs 21#06"));

        actual = decorator.decorate(mf2);

        assertThat(mf2.getContext().getSlug()).isNull();
      }

      @Test
      void week52IsCorrect() {
        mf2.getProperties().setName(Collections.singletonList("Week Notes 20#52"));
        actual = decorator.decorate(mf2);

        assertThat(mf2.getProperties().getSummary())
            .containsExactly("What happened in the week of 2020-12-21?");
      }

      @Test
      void week53IsCorrect() {
        mf2.getProperties().setName(Collections.singletonList("Week Notes 20#53"));
        actual = decorator.decorate(mf2);

        assertThat(mf2.getProperties().getSummary())
            .containsExactly("What happened in the week of 2020-12-28?");
      }

      private void processDefault() {
        mf2.getProperties().setName(Collections.singletonList("Week Notes 21#06"));
        actual = decorator.decorate(mf2);
      }
    }

    @Test
    void itReturnsSame() {
      mf2.getProperties().setName(Collections.singletonList("f"));

      actual = decorator.decorate(mf2);

      assertThat(actual).isSameAs(mf2);
    }
  }
}
