package me.jvt.www.api.micropub.validator.field;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.util.HashMap;
import java.util.Map;
import javax.validation.ConstraintValidatorContext;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HasAltTextValidatorTest {
  private final Map<String, String> photo = new HashMap<>();
  private final HasAltTextValidator validator = new HasAltTextValidator();
  @Mock private ConstraintValidatorContext constraintValidatorContext;

  @ParameterizedTest
  @NullAndEmptySource
  void itReturnsFalseIfNotPresent(Map<String, String> photo) {
    boolean isValid = validator.isValid(photo, constraintValidatorContext);

    assertThat(isValid).isFalse();
  }

  @Test
  void itReturnsFalseWhenPhotoPropertyButNoAltProperty() {
    photo.put("photo", "something");

    boolean isValid = validator.isValid(photo, constraintValidatorContext);

    assertThat(isValid).isFalse();
  }

  @ParameterizedTest
  @NullAndEmptySource
  void itReturnsFalseWhenAltPropertyIsNullOrEmpty(String alt) {
    photo.put("alt", alt);

    boolean isValid = validator.isValid(photo, constraintValidatorContext);

    assertThat(isValid).isFalse();
  }

  @Test
  void itReturnsTrueWhenAltTextIsNotEmpty() {
    photo.put("alt", "something descriptive");

    boolean isValid = validator.isValid(photo, constraintValidatorContext);

    assertThat(isValid).isTrue();
  }
}
