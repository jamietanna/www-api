package me.jvt.www.api.micropub.model.posttype;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import javax.validation.ConstraintValidator;
import me.jvt.www.api.micropub.util.SingletonSetHelper;
import me.jvt.www.api.micropub.validator.property.ContentPropertyValidator;
import me.jvt.www.api.micropub.validator.property.DatePropertyValidator;
import me.jvt.www.api.micropub.validator.property.NotEmptyPropertyValidator;
import me.jvt.www.api.micropub.validator.property.PhotoPropertyValidator;
import me.jvt.www.api.micropub.validator.property.PostStatusPropertyValidator;
import me.jvt.www.api.micropub.validator.property.ReadOfPropertyValidator;
import me.jvt.www.api.micropub.validator.property.ReadStatusPropertyValidator;
import me.jvt.www.api.micropub.validator.property.RsvpPropertyValidator;
import me.jvt.www.api.micropub.validator.property.UrlPropertyValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

class MicroformatsPropertyValidatorsFactoryTest {

  private List<ConstraintValidator<?, Microformats2Object>> actual;

  private final MicroformatsPropertyValidatorsFactory factory =
      new MicroformatsPropertyValidatorsFactory(SingletonSetHelper.singletonSet("https://foo"));

  @ParameterizedTest
  @EnumSource(MicroformatsProperty.class)
  void allValuesHaveMappings(MicroformatsProperty property) {
    process(property);

    assertThat(actual).isNotEmpty();
  }

  @Test
  void photo() {
    process(MicroformatsProperty.PHOTO);

    assertThat(actual).hasSize(1);
    assertThat(actual.get(0)).isInstanceOf(PhotoPropertyValidator.class);
  }

  @Test
  void content() {
    process(MicroformatsProperty.CONTENT);

    assertThat(actual).hasSize(1);
    assertThat(actual.get(0)).isInstanceOf(ContentPropertyValidator.class);
  }

  @Test
  void readOf() {
    process(MicroformatsProperty.READ_OF);

    assertThat(actual).hasSize(1);
    assertThat(actual.get(0)).isInstanceOf(ReadOfPropertyValidator.class);
  }

  @Test
  void readStatus() {
    process(MicroformatsProperty.READ_STATUS);

    assertThat(actual).hasSize(1);
    assertThat(actual.get(0)).isInstanceOf(ReadStatusPropertyValidator.class);
  }

  @Test
  void rsvp() {
    process(MicroformatsProperty.RSVP);

    assertThat(actual).hasSize(1);
    assertThat(actual.get(0)).isInstanceOf(RsvpPropertyValidator.class);
  }

  @Test
  void postStatus() {
    process(MicroformatsProperty.POST_STATUS);

    assertThat(actual).hasSize(1);
    assertThat(actual.get(0)).isInstanceOf(PostStatusPropertyValidator.class);
  }

  @ParameterizedTest
  @EnumSource(
      value = MicroformatsProperty.class,
      names = {"PUBLISHED", "END", "START"})
  void dates(MicroformatsProperty property) {
    process(property);

    assertThat(actual).hasSize(1);
    assertThat(actual.get(0)).isInstanceOf(DatePropertyValidator.class);
  }

  @ParameterizedTest
  @EnumSource(
      value = MicroformatsProperty.class,
      names = {
        "NAME",
        "NICKNAME",
        "CATEGORY",
        "SUMMARY",
        "NUM",
        "UNIT",
        "REL_TWITTER",
        "CODE_LICENSE",
        "PROSE_LICENSE",
        "SERIES",
        "FEATURED"
      })
  void notBlank(MicroformatsProperty property) {
    process(property);

    assertThat(actual).hasSize(1);
    assertThat(actual.get(0)).isInstanceOf(NotEmptyPropertyValidator.class);
  }

  @ParameterizedTest
  @EnumSource(
      value = MicroformatsProperty.class,
      names = {
        "URL",
        "REPOST_OF",
        "LIKE_OF",
        "IN_REPLY_TO",
        "BOOKMARK_OF",
        "SYNDICATION",
        "LISTEN_OF"
      })
  void urls(MicroformatsProperty property) {
    process(property);

    assertThat(actual).hasSize(1);
    assertThat(actual.get(0)).isInstanceOf(UrlPropertyValidator.class);
  }

  void process(MicroformatsProperty property) {
    actual = factory.validatorsForProperty(property);
  }
}
