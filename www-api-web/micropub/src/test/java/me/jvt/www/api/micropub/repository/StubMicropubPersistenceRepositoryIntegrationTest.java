package me.jvt.www.api.micropub.repository;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import javax.validation.ConstraintViolationException;
import me.jvt.www.api.micropub.model.TestMicroformats2Object;
import me.jvt.www.api.micropub.service.MicropubConflictService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("staging")
public class StubMicropubPersistenceRepositoryIntegrationTest {

  @MockBean private MicropubConflictService micropubConflictService;

  @Autowired private MicropubPersistenceRepository repository;

  @Test
  void invalidMf2IsRejected() {
    assertThatThrownBy(() -> repository.save(new TestMicroformats2Object()))
        .isInstanceOf(ConstraintViolationException.class)
        .hasMessageContaining("The provided data did not abide by the restrictions of the Kind.");
  }
}
