package me.jvt.www.api.micropub.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Stream;
import me.jvt.www.api.micropub.model.MicropubConfigDto;
import me.jvt.www.api.micropub.model.MicropubSyndicateTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class MicropubConfigurationServiceTest {

  private MicropubConfigurationService sut;

  private static Stream<Arguments> provideSyndicationSettings() {
    return Arrays.stream(MicropubSyndicateTo.values()).map(Arguments::of);
  }

  @BeforeEach
  void setup() {
    sut = new MicropubConfigurationService("https://something.foo.com");
  }

  @Test
  void configSupportsMediaEndpointAndUsesBaseUrl() {
    MicropubConfigDto config = sut.getConfiguration();

    assertThat(config.getMediaEndpoint()).isEqualTo("https://something.foo.com/micropub/media");
  }

  @ParameterizedTest
  @MethodSource("provideSyndicationSettings")
  void configSupportsSyndicationForValues(MicropubSyndicateTo expected) {
    MicropubConfigDto config = sut.getConfiguration();

    assertThat(config.getSyndicateTo()).contains(expected);
  }

  @Test
  void configDoesNotReturnSupportedQueries() {
    assertThat(sut.getConfiguration().getQueries()).isNull();
  }

  @ParameterizedTest
  @MethodSource("provideSyndicationSettings")
  void syndicationConfigSupportsSyndicationForValues(MicropubSyndicateTo expected) {
    Set<MicropubSyndicateTo> syndicateTo = sut.getSyndicationConfiguration();

    assertThat(syndicateTo).contains(expected);
  }
}
