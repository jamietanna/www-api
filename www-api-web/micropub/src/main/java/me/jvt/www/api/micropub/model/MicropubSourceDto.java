package me.jvt.www.api.micropub.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class MicropubSourceDto extends MicropubQueryDto {
  @JsonInclude(Include.NON_NULL)
  private List<String> type;

  private Properties properties;

  public List<String> getType() {
    return type;
  }

  public void setType(List<String> type) {
    this.type = type;
  }

  public Properties getProperties() {
    if (null == properties) {
      return null;
    }

    if (properties.containsKey("content")) {
      List<Object> contentObj = (List<Object>) properties.get("content");
      if (contentObj.get(0) instanceof String) {
        properties.put("content", Collections.singletonList(contentObj.get(0)));
      } else {
        Map<String, String> content = properties.getContent().get(0);
        if (null == content.get("html") || content.get("html").isEmpty()) {
          properties.put("content", Collections.singletonList(content.get("value")));
        } else {
          properties.put(
              "content",
              Collections.singletonList(Collections.singletonMap("html", content.get("html"))));
        }
      }
    }

    return properties;
  }

  public void setProperties(Properties properties) {
    this.properties = properties;
  }
}
