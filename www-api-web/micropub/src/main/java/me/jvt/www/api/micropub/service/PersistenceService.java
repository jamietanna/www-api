package me.jvt.www.api.micropub.service;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.List;
import java.util.Optional;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object.Context;

public interface PersistenceService {

  /**
   * Determine the persistence information ({@link PersistenceAction}) for a given {@link Kind} of
   * content.
   *
   * @param mf2 the {@link Microformats2Object} to read from
   * @return the {@link PersistenceAction} containing information about how to save the content
   */
  PersistenceAction determineAction(Microformats2Object mf2);

  /**
   * Determine the persistence information ({@link PersistenceAction}) for the cite(s) stored in a
   * {@link Context}.
   *
   * @param cite the {@link JsonNode} representation of the cite
   * @return the {@link PersistenceAction}s containing information about how to save the content
   */
  List<PersistenceAction> determineAction(JsonNode cite);

  /**
   * Prepare the {@link Microformats2Object} for saving it in the persistence tier.
   *
   * @param persistenceAction the {@link PersistenceAction} that was determined
   * @param mf2 the {@link Microformats2Object} to save
   * @return the String representation of the {@link Microformats2Object}
   */
  String serialize(PersistenceAction persistenceAction, Microformats2Object mf2);

  /**
   * Prepare the {@link JsonNode} representation of the cite for saving it in the persistence tier.
   *
   * @param cite the cite object to serialise
   * @return the String representation(s) of the cite
   */
  List<String> serialize(JsonNode cite);

  /**
   * For a given URL, return the path that the file can be found on-disk.
   *
   * @param url public URL of the post
   * @return the path on-disk, if present, or an empty {@link Optional}.
   */
  Optional<String> contentPathFromUrl(String url);

  class PersistenceAction {

    private final String slug;
    private final String urlPath;
    private final String path;
    private final String commitMessage;

    PersistenceAction(String slug, String urlPath, String path, String commitMessage) {
      this.slug = slug;
      this.urlPath = urlPath;
      this.path = path;
      this.commitMessage = commitMessage;
    }

    public String getSlug() {
      return slug;
    }

    public String getUrlPath() {
      return urlPath;
    }

    public String getPath() {
      return path;
    }

    public String getCommitMessage() {
      return commitMessage;
    }
  }
}
