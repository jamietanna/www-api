package me.jvt.www.api.micropub.security;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import me.jvt.www.indieauth.security.ProfileUrlValidator;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.server.resource.web.access.BearerTokenAccessDeniedHandler;
import org.springframework.security.web.access.AccessDeniedHandler;

public class MicropubBearerTokenAccessDeniedHandler implements AccessDeniedHandler {

  public static final String ERROR_INSUFFICIENT_SCOPE = "error=\"insufficient_scope\"";
  public static final String ROLE_ME = "ME";
  public static final String INSUFFICIENT_SCOPE_JSON_BODY =
      "{\"error\": \"insufficient_scope\", \"error_description\": "
          + "\"The request requires higher privileges than provided by the access token.\""
          + "}";
  public static final String FORBIDDEN_JSON_BODY =
      "{\"error\": \"forbidden\", \"error_description\": "
          + "\"Profile URL for this token is not able to perform this action.\""
          + "}";

  private final BearerTokenAccessDeniedHandler delegate;
  private final ProfileUrlValidator profileUrlValidator;

  public MicropubBearerTokenAccessDeniedHandler(
      BearerTokenAccessDeniedHandler delegate, ProfileUrlValidator profileUrlValidator) {
    this.delegate = delegate;
    this.profileUrlValidator = profileUrlValidator;
  }

  @Override
  public void handle(
      HttpServletRequest request,
      HttpServletResponse response,
      AccessDeniedException accessDeniedException)
      throws IOException {
    delegate.handle(request, response, accessDeniedException);
    String wwwAuthenticate = response.getHeader(HttpHeaders.WWW_AUTHENTICATE);
    if (wwwAuthenticate.contains(ERROR_INSUFFICIENT_SCOPE)) {
      if (profileUrlValidator.isValidProfileUrl(request.getUserPrincipal().getName())) {
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.getWriter().println(INSUFFICIENT_SCOPE_JSON_BODY);
      } else {
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpStatus.FORBIDDEN.value());
        response.getWriter().println(FORBIDDEN_JSON_BODY);
      }
    }
  }
}
