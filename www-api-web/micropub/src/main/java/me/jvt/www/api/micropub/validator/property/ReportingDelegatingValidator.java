package me.jvt.www.api.micropub.validator.property;

import java.lang.annotation.Annotation;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ReportingDelegatingValidator<A extends Annotation, O>
    implements ConstraintValidator<A, O> {

  private final ConstraintValidator<A, O> delegate;

  public ReportingDelegatingValidator(ConstraintValidator<A, O> delegate) {
    this.delegate = delegate;
  }

  @Override
  public boolean isValid(O value, ConstraintValidatorContext context) {
    boolean isValid = delegate.isValid(value, context);

    if (!isValid) {
      context
          .buildConstraintViolationWithTemplate(
              String.format("Validation for %s failed", delegate.getClass()))
          .addConstraintViolation();
    }
    return isValid;
  }
}
