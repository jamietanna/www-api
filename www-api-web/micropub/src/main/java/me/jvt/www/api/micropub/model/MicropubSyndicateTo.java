package me.jvt.www.api.micropub.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Optional;
import java.util.function.Predicate;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum MicropubSyndicateTo {
  INDIENEWS(
      "https://news.indieweb.org/en",
      "IndieNews",
      s -> s.startsWith("https://news.indieweb.org/en/")),
  XYZ_BLOGGING("https://indieweb.xyz/en/blogging", "/en/Blogging"),
  XYZ_INDIEWEB("https://indieweb.xyz/en/indieweb", "/en/IndieWeb"),
  XYZ_MICROFORMATS("https://indieweb.xyz/en/microformats", "/en/Microformats"),
  BRIDGY_GITHUB(
      "https://brid.gy/publish/github",
      "Brid.gy GitHub Syndication",
      s -> s.startsWith("https://github.com/")),
  BRIDGY_TWITTER(
      "https://brid.gy/publish/twitter",
      "Brid.gy Twitter Syndication",
      s -> s.startsWith("https://twitter.com/")),
  XYZ_TOOLS("https://indieweb.xyz/en/tools", "/en/Tools");

  private final String uid;
  private final String name;
  private final Predicate<String> doesUrlMatch;

  MicropubSyndicateTo(String uid, String name) {
    this.uid = uid;
    this.name = name;
    this.doesUrlMatch = ignored -> false;
  }

  MicropubSyndicateTo(String uid, String name, Predicate<String> doesUrlMatch) {
    this.uid = uid;
    this.name = name;
    this.doesUrlMatch = doesUrlMatch;
  }

  public static boolean rewrite(String previousSyndication, String newSyndication) {
    Optional<MicropubSyndicateTo> optional = fromUid(previousSyndication);
    return optional.map(syndicateTo -> syndicateTo.doesUrlMatch.test(newSyndication)).orElse(false);
  }

  static Optional<MicropubSyndicateTo> fromUid(String uid) {
    for (MicropubSyndicateTo syndicateTo : MicropubSyndicateTo.values()) {
      if (syndicateTo.getUid().equals(uid)) {
        return Optional.of(syndicateTo);
      }
    }
    return Optional.empty();
  }

  public String getUid() {
    return uid;
  }

  public String getName() {
    return name;
  }
}
