package me.jvt.www.api.micropub.decorator;

import java.util.LinkedHashSet;
import me.jvt.www.api.micropub.model.Kind;

public class Microformats2ObjectDecoratorFactory {

  private final LinkedHashSet<Microformats2ObjectDecorator> decorators;

  public Microformats2ObjectDecoratorFactory(
      LinkedHashSet<Microformats2ObjectDecorator> decorators) {
    this.decorators = decorators;
  }

  public LinkedHashSet<Microformats2ObjectDecorator> provide(Kind kind) {
    return decorators; // TODO: this should be implemented as Kind-specific
  }
}
