package me.jvt.www.api.micropub.decorator;

import me.jvt.www.api.micropub.actions.concrete.AddSyndicationPropertyAction;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.MicropubSyndicateTo;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;

public class GitHubSyndicationDecorator implements Microformats2ObjectDecorator {

  private static final String GITHUB_PREFIX = "https://github.com";

  private static boolean isGitHubLike(Microformats2Object mf2) {
    return Kind.likes.equals(mf2.getKind())
        && mf2.getProperties().getLikeOf().get(0).startsWith(GITHUB_PREFIX);
  }

  private static boolean isGitHubReply(Microformats2Object mf2) {
    return Kind.replies.equals(mf2.getKind())
        && mf2.getProperties().getInReplyTo().get(0).startsWith(GITHUB_PREFIX);
  }

  @Override
  public Microformats2Object decorate(Microformats2Object mf2) {
    if (isGitHubLike(mf2) || isGitHubReply(mf2)) {
      AddSyndicationPropertyAction action =
          new AddSyndicationPropertyAction(
              SingletonListHelper.singletonList(MicropubSyndicateTo.BRIDGY_GITHUB.getUid()));
      action.accept(mf2);
    }

    return mf2;
  }
}
