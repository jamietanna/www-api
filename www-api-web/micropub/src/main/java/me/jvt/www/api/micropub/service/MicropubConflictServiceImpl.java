package me.jvt.www.api.micropub.service;

import java.util.List;
import java.util.Optional;
import me.jvt.www.api.micropub.client.ContentDeduplicationClient;
import me.jvt.www.api.micropub.model.ContentDeduplicationResult;
import me.jvt.www.api.micropub.model.ContentDeduplicationResult.ContentDeduplicationEntry;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;

public class MicropubConflictServiceImpl implements MicropubConflictService {

  private final ContentDeduplicationClient contentDeduplicationClient;
  private final ContentDeduplicationResult localContentDeduplicationResult;

  public MicropubConflictServiceImpl(ContentDeduplicationClient contentDeduplicationClient) {
    this.contentDeduplicationClient = contentDeduplicationClient;
    localContentDeduplicationResult = new ContentDeduplicationResult();
  }

  @Override
  public Optional<String> isConflictFoundInCache(Microformats2Object mf2) {
    Optional<String> maybeUrl = urlForPost(mf2);
    if (!maybeUrl.isPresent()) {
      return Optional.empty();
    }

    ContentDeduplicationResult publicContentDeduplication =
        contentDeduplicationClient.retrieveContentDeduplicationData();

    Optional<String> isFoundOnSite =
        isFoundInContentDeduplicationEntry(
            mf2.getKind(), maybeUrl.get(), publicContentDeduplication);

    if (isFoundOnSite.isPresent()) {
      return isFoundOnSite;
    }

    return isFoundInContentDeduplicationEntry(
        mf2.getKind(), maybeUrl.get(), localContentDeduplicationResult);
  }

  @Override
  public void updateCacheWithSlug(Microformats2Object mf2, String slug) {
    ContentDeduplicationEntry entry = new ContentDeduplicationEntry(mf2.getKind(), slug);
    Optional<String> maybeUrl = urlForPost(mf2);
    maybeUrl.ifPresent(
        s -> localContentDeduplicationResult.put(s, SingletonListHelper.singletonList(entry)));
  }

  private Optional<String> isFoundInContentDeduplicationEntry(
      Kind kind, String url, ContentDeduplicationResult contentDeduplicationResult) {
    List<ContentDeduplicationEntry> entries = contentDeduplicationResult.get(url);
    if (null != entries) {
      for (ContentDeduplicationEntry entry : entries) {
        if (kind == entry.getKind()) {
          return Optional.of(entry.getSlug());
        }
      }
    }

    return Optional.empty();
  }

  private Optional<String> urlForPost(Microformats2Object mf2) {
    String url;
    switch (mf2.getKind()) {
      case bookmarks:
        url = mf2.getProperties().getBookmarkOf().get(0);
        break;
      case likes:
        url = mf2.getProperties().getLikeOf().get(0);
        break;
      case reposts:
        url = mf2.getProperties().getRepostOf().get(0);
        break;
      case rsvps:
        url = mf2.getProperties().getInReplyTo().get(0);
        break;
      default:
        return Optional.empty();
    }

    return Optional.of(url);
  }
}
