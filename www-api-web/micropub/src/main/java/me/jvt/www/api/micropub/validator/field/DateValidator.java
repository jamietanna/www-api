package me.jvt.www.api.micropub.validator.field;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateValidator implements ConstraintValidator<ValidDateConstraint, String> {

  private static final DateTimeFormatter HUGO_DATE_TIME_FORMATTER =
      DateTimeFormatter.ISO_OFFSET_DATE_TIME;
  // adapted from https://stackoverflow.com/a/46488425
  private static final DateTimeFormatter ISO_OFFSET_DATE_TIME_NO_MILLISECONDS_OR_COLON_IN_OFFSET =
      DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss[.SSS][xxx][xx]['Z']");

  @Override
  public boolean isValid(String date, ConstraintValidatorContext context) {
    return isValidHugoDate(date) || isValidJavaDate(date);
  }

  private boolean isValidHugoDate(String date) {
    try {
      HUGO_DATE_TIME_FORMATTER.parse(date);
      return true;
    } catch (DateTimeParseException e) {
      return false;
    }
  }

  private boolean isValidJavaDate(String date) {
    try {
      ISO_OFFSET_DATE_TIME_NO_MILLISECONDS_OR_COLON_IN_OFFSET.parse(date);
      return true;
    } catch (DateTimeParseException e) {
      return false;
    }
  }
}
