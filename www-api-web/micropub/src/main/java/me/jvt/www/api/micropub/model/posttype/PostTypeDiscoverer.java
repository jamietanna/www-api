package me.jvt.www.api.micropub.model.posttype;

import java.util.List;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.Properties;

public class PostTypeDiscoverer {
  public Kind discover(String h, Properties mf2Properties) {
    if (null == h || h.isEmpty()) {
      throw new KindNotSupportedException("The `h` was not set in the request");
    }

    if (null == mf2Properties || mf2Properties.isEmpty()) {
      throw new KindNotSupportedException("No properties could be found in the request");
    }

    if (h.equals("h-entry")) {
      return handleEntry(mf2Properties);
    }

    if (h.equals("h-card")) {
      return Kind.contacts;
    }

    if (h.equals("h-measure")) {
      if (null != mf2Properties.getUnit() && mf2Properties.getUnit().contains("steps")) {
        return Kind.steps;
      }
      throw new KindNotSupportedException("Only step counts are supported");
    }

    if (h.equals("h-event")) {
      return Kind.events;
    }

    throw new KindNotSupportedException("h not supported");
  }

  private Kind handleEntry(Properties mf2Properties) {
    if (isNotEmpty(mf2Properties.getRsvp())) {
      return Kind.rsvps;
    }

    if (isNotEmpty(mf2Properties.getInReplyTo())) {
      return Kind.replies;
    }

    if (isNotEmpty(mf2Properties.getRepostOf())) {
      return Kind.reposts;
    }

    if (isNotEmpty(mf2Properties.getLikeOf())) {
      return Kind.likes;
    }

    if (isNotEmpty(mf2Properties.getBookmarkOf())) {
      return Kind.bookmarks;
    }

    if (isNotEmpty(mf2Properties.getPhoto())) {
      return Kind.photos;
    }

    if (null != mf2Properties.getListenOf()) {
      return Kind.listens;
    }

    if (isNotEmpty(mf2Properties.getName())) {
      return Kind.articles;
    }

    if (isNotEmpty(mf2Properties.getContent())) {
      return Kind.notes;
    }

    if (null != mf2Properties.getReadOf()) {
      return Kind.reads;
    }

    throw new KindNotSupportedException("No kind could be determined from the provided h=entry");
  }

  private boolean isNotEmpty(List<?> property) {
    if (null == property) {
      return false;
    }

    return !property.isEmpty();
  }
}
