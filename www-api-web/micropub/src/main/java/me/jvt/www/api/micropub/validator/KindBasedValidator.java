package me.jvt.www.api.micropub.validator;

import java.util.function.Predicate;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;
import me.jvt.www.api.micropub.model.posttype.MicroformatsPropertyValidatorsFactory;
import me.jvt.www.api.micropub.validator.property.ReportingDelegatingValidator;

public class KindBasedValidator implements ConstraintValidator<ValidKind, Microformats2Object> {

  private final MicroformatsPropertyValidatorsFactory factory;

  public KindBasedValidator(MicroformatsPropertyValidatorsFactory factory) {
    this.factory = factory;
  }

  @Override
  public boolean isValid(Microformats2Object mf2, ConstraintValidatorContext context) {
    if (null == mf2.getKind()) {
      return false;
    }

    if (!areRequiredPropertiesValid(mf2, context)) {
      return false;
    }

    return mf2.getKind().getOptionalProperties().parallelStream()
        .allMatch(areOptionalPropertiesValid(mf2, context));
  }

  private boolean areRequiredPropertiesValid(
      Microformats2Object mf2, ConstraintValidatorContext context) {
    return mf2.getKind().getRequiredProperties().parallelStream()
        .allMatch(
            property ->
                factory.validatorsForProperty(property).parallelStream()
                    .allMatch(performValidation(mf2, context)));
  }

  private Predicate<MicroformatsProperty> areOptionalPropertiesValid(
      Microformats2Object mf2, ConstraintValidatorContext context) {
    return property -> {
      if (!mf2.getProperties().containsKey(property.getName())) {
        return true;
      }
      // TODO: what if it's sent, but not

      return factory.validatorsForProperty(property).parallelStream()
          .allMatch(performValidation(mf2, context));
    };
  }

  private Predicate<ConstraintValidator<?, Microformats2Object>> performValidation(
      Microformats2Object mf2, ConstraintValidatorContext context) {
    return v -> new ReportingDelegatingValidator<>(v).isValid(mf2, context);
  }
}
