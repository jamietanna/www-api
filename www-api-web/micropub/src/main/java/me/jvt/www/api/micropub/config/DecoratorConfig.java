package me.jvt.www.api.micropub.config;

import java.util.LinkedHashSet;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.micropub.client.GranaryTwitterClient;
import me.jvt.www.api.micropub.decorator.GitHubSyndicationDecorator;
import me.jvt.www.api.micropub.decorator.HashtagToTagDecorator;
import me.jvt.www.api.micropub.decorator.Microformats2ObjectDecorator;
import me.jvt.www.api.micropub.decorator.Microformats2ObjectDecoratorFactory;
import me.jvt.www.api.micropub.decorator.PublishedDecorator;
import me.jvt.www.api.micropub.decorator.ReadsTwitterSyndicationDecorator;
import me.jvt.www.api.micropub.decorator.TwitterLikeNameDecorator;
import me.jvt.www.api.micropub.decorator.TwitterSyndicationDecorator;
import me.jvt.www.api.micropub.decorator.TwitterUrlToAtUsernameContentDecorator;
import me.jvt.www.api.micropub.decorator.WeekNotesDecorator;
import me.jvt.www.api.micropub.util.HashtagToTagConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class DecoratorConfig {

  @Bean
  public HashtagToTagConverter hashtagToTagConverter() {
    return new HashtagToTagConverter();
  }

  @Bean
  public HashtagToTagDecorator hashtagToTagDecorator(HashtagToTagConverter hashtagToTagConverter) {
    return new HashtagToTagDecorator(hashtagToTagConverter);
  }

  @Bean
  public GitHubSyndicationDecorator gitHubSyndicationDecorator() {
    return new GitHubSyndicationDecorator();
  }

  @Bean
  public TwitterSyndicationDecorator twitterSyndicationDecorator() {
    return new TwitterSyndicationDecorator();
  }

  @Bean
  public GranaryTwitterClient granaryTwitterClient(
      RequestSpecificationFactory requestSpecificationFactory,
      @Value("${granary.access-token.key}") String granaryAccessTokenKey,
      @Value("${granary.access-token.secret}") String granaryAccessTokenSecret) {
    return new GranaryTwitterClient(
        requestSpecificationFactory, granaryAccessTokenKey, granaryAccessTokenSecret);
  }

  @Bean
  public TwitterLikeNameDecorator twitterLikeNameDecorator() {
    return new TwitterLikeNameDecorator();
  }

  @Bean
  public TwitterUrlToAtUsernameContentDecorator twitterUrlToAtUsernameContentDecorator() {
    return new TwitterUrlToAtUsernameContentDecorator();
  }

  @Bean
  public PublishedDecorator publishedDecorator() {
    return new PublishedDecorator();
  }

  @Bean
  public LinkedHashSet<Microformats2ObjectDecorator> mf2Decorators(
      HashtagToTagDecorator hashtagToTagDecorator,
      GitHubSyndicationDecorator gitHubSyndicationDecorator,
      TwitterSyndicationDecorator twitterSyndicationDecorator,
      TwitterLikeNameDecorator twitterLikeNameDecorator,
      TwitterUrlToAtUsernameContentDecorator twitterUrlToAtUsernameContentDecorator,
      WeekNotesDecorator weekNotesDecorator,
      ReadsTwitterSyndicationDecorator readsTwitterSyndicationDecorator,
      PublishedDecorator publishedDecorator) {
    LinkedHashSet<Microformats2ObjectDecorator> decorators = new LinkedHashSet<>();
    decorators.add(hashtagToTagDecorator);
    decorators.add(gitHubSyndicationDecorator);
    decorators.add(twitterSyndicationDecorator);
    decorators.add(twitterLikeNameDecorator);
    decorators.add(twitterUrlToAtUsernameContentDecorator);
    decorators.add(readsTwitterSyndicationDecorator);
    decorators.add(weekNotesDecorator);
    decorators.add(publishedDecorator);
    return decorators;
  }

  @Bean
  public Microformats2ObjectDecoratorFactory decoratorFactory(
      LinkedHashSet<Microformats2ObjectDecorator> decorators) {
    return new Microformats2ObjectDecoratorFactory(decorators);
  }
}
