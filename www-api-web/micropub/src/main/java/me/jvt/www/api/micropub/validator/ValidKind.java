package me.jvt.www.api.micropub.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = KindBasedValidator.class)
@Target({ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidKind {
  String message() default "The provided data did not abide by the restrictions of the Kind.";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
