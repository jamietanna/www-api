package me.jvt.www.api.micropub.sanitiser;

import java.util.List;
import java.util.stream.Collectors;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

public class UniqueCategorySanitiser implements Microformats2ObjectSanitiser {

  @Override
  public Microformats2Object sanitise(Microformats2Object mf2) {
    List<String> categories = mf2.getProperties().getCategory();
    if (null != categories) {
      List<String> distinctCategories = categories.stream().distinct().collect(Collectors.toList());
      mf2.getProperties().setCategory(distinctCategories);
    }
    return mf2;
  }
}
