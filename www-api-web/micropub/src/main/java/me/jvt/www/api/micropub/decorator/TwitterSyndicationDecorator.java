package me.jvt.www.api.micropub.decorator;

import java.net.MalformedURLException;
import java.net.URL;
import me.jvt.www.api.micropub.actions.concrete.AddSyndicationPropertyAction;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.MicropubSyndicateTo;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;

public class TwitterSyndicationDecorator implements Microformats2ObjectDecorator {

  @Override
  public Microformats2Object decorate(Microformats2Object mf2) {
    if (Kind.notes == mf2.getKind()
        || Kind.photos == mf2.getKind()
        || (Kind.replies == mf2.getKind()
            && isTwitterUrl(mf2.getProperties().getInReplyTo().get(0)))
        || (Kind.likes == mf2.getKind() && isTwitterUrl(mf2.getProperties().getLikeOf().get(0)))
        || (Kind.reposts == mf2.getKind()
            && isTwitterUrl(mf2.getProperties().getRepostOf().get(0)))) {
      AddSyndicationPropertyAction action =
          new AddSyndicationPropertyAction(
              SingletonListHelper.singletonList(MicropubSyndicateTo.BRIDGY_TWITTER.getUid()));
      action.accept(mf2);
    }
    return mf2;
  }

  private boolean isTwitterUrl(String url) {
    try {
      URL parsedUrl = new URL(url);
      if ("twitter.com".equals(parsedUrl.getHost())) {
        return true;
      }
    } catch (MalformedURLException e) {
      throw new InvalidMicropubMetadataRequest("Invalid URL", e);
    }

    return false;
  }
}
