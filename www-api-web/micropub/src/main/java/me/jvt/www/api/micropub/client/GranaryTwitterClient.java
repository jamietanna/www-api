package me.jvt.www.api.micropub.client;

import io.restassured.response.Response;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.micropub.model.GranaryTwitterHentry;

public class GranaryTwitterClient {

  private final RequestSpecificationFactory requestSpecificationFactory;
  private final String accessTokenKey;
  private final String accessTokenSecret;

  public GranaryTwitterClient(
      RequestSpecificationFactory requestSpecificationFactory,
      String accessTokenKey,
      String accessTokenSecret) {
    this.requestSpecificationFactory = requestSpecificationFactory;
    this.accessTokenKey = accessTokenKey;
    this.accessTokenSecret = accessTokenSecret;
  }

  public GranaryTwitterHentry tweetToHentry(String activityId) {
    Response response =
        requestSpecificationFactory
            .newRequestSpecification()
            .baseUri("https://granary.io/twitter/@me/@all/@app")
            .queryParam("format", "mf2-json")
            .queryParam("access_token_key", accessTokenKey)
            .queryParam("access_token_secret", accessTokenSecret)
            .get(String.format("/%s", activityId));
    if (200 != response.getStatusCode()) {
      throw new RuntimeException(
          String.format("Call to Granary failed with error code %d", response.getStatusCode()));
    }
    return response.as(GranaryTwitterHentry.class);
  }
}
