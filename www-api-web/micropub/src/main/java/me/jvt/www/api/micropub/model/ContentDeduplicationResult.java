package me.jvt.www.api.micropub.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.List;
import me.jvt.www.api.micropub.model.ContentDeduplicationResult.ContentDeduplicationEntry;

public class ContentDeduplicationResult extends HashMap<String, List<ContentDeduplicationEntry>> {
  public static class ContentDeduplicationEntry {
    private Kind kind;

    @JsonProperty("url")
    private String slug;

    @SuppressWarnings("unused")
    public ContentDeduplicationEntry() {}

    public ContentDeduplicationEntry(Kind kind, String slug) {
      this.setKind(kind);
      this.setSlug(slug);
    }

    public Kind getKind() {
      return kind;
    }

    public void setKind(Kind kind) {
      this.kind = kind;
    }

    public String getSlug() {
      return slug;
    }

    public void setSlug(String slug) {
      this.slug = slug;
    }
  }
}
