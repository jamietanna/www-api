package me.jvt.www.api.micropub.actions;

import java.util.function.Consumer;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

/** A Micropub Update action, for a given type of action. */
public interface Action extends Consumer<Microformats2Object> {}
