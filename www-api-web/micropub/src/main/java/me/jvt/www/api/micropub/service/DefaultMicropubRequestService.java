package me.jvt.www.api.micropub.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.request.MicropubRequest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;

public class DefaultMicropubRequestService implements MicropubRequestService {

  private static final GrantedAuthority DRAFT_SCOPE = new SimpleGrantedAuthority("SCOPE_draft");
  private static final String UNDELETE_SCOPE = "SCOPE_undelete";
  private static final String CLIENT_ID_ATTRIBUTE = "client_id";
  private final MicropubService micropubService;

  @SuppressWarnings("unused")
  public DefaultMicropubRequestService(MicropubService micropubService) {
    this.micropubService = micropubService;
  }

  @Override
  public MicropubResponse handle(MicropubRequest micropubRequest, Authentication authentication) {
    Microformats2Object mf2;
    switch (micropubRequest.getAction()) {
      case CREATE:
        decorateWithClientId(micropubRequest, authentication);
        mf2 = handleCreate(micropubRequest);
        setDraft(mf2, authentication);
        String slug = micropubService.save(mf2);
        return new MicropubRequestService.CreateResponse(slug);
      case DELETE:
      case UNDELETE:
        mf2 = retrieveAndUpdate(micropubRequest, authentication);
        micropubService.save(mf2);
        return new MicropubRequestService.DeleteUndeleteResponse();
      case UPDATE:
        mf2 = retrieveAndUpdate(micropubRequest, authentication);
        mf2 = micropubService.performSanitisation(mf2);
        rejectIfDraft(mf2, authentication);
        micropubService.save(mf2);
        return new MicropubRequestService.UpdateResponse(mf2);
      default:
        throw new IllegalStateException(
            String.format("Action %s is not supported", micropubRequest.getAction()));
    }
  }

  private void decorateWithClientId(
      MicropubRequest micropubRequest, Authentication authentication) {
    OAuth2AuthenticatedPrincipal oauth2AuthenticatedPrincipal =
        (OAuth2AuthenticatedPrincipal) authentication.getPrincipal();
    micropubRequest
        .getContext()
        .getContext()
        .setClientId(oauth2AuthenticatedPrincipal.getAttribute(CLIENT_ID_ATTRIBUTE));
  }

  private boolean canClientViewDeleted(Authentication authentication) {
    boolean canClientViewDeleted = false;
    if (null != authentication) {
      canClientViewDeleted =
          authentication.getAuthorities().contains(new SimpleGrantedAuthority(UNDELETE_SCOPE));
    }
    return canClientViewDeleted;
  }

  private Microformats2Object handleCreate(MicropubRequest micropubRequest) {
    Microformats2Object mf2 = micropubService.create(micropubRequest);
    mf2 = micropubService.performSanitisation(mf2);
    mf2 = micropubService.performDecoration(mf2);

    return mf2;
  }

  private Microformats2Object retrieveAndUpdate(
      MicropubRequest micropubRequest, Authentication authentication) {
    String url = micropubRequest.getContext().getUrl();
    Optional<Microformats2Object> maybeMf2 =
        micropubService.retrieve(url, canClientViewDeleted(authentication));
    if (!maybeMf2.isPresent()) {
      throw new InvalidMicropubMetadataRequest(String.format("Post %s does not exist", url));
    }

    Microformats2Object mf2 = maybeMf2.get();
    mf2 = micropubService.update(mf2, micropubRequest.getActions());
    return mf2;
  }

  private static void setDraft(Microformats2Object mf2, Authentication authentication) {
    if (authentication.getAuthorities().contains(DRAFT_SCOPE)) {
      mf2.getProperties().setPostStatus(Collections.singletonList("draft"));
    }
  }

  private static void rejectIfDraft(Microformats2Object mf2, Authentication authentication) {
    if (!authentication.getAuthorities().contains(DRAFT_SCOPE)) {
      return;
    }

    List<String> postStatus = mf2.getProperties().getPostStatus();
    if (null == postStatus || postStatus.isEmpty() || postStatus.contains("published")) {
      throw new AccessDeniedException(
          "Published post cannot be updated when `draft` scope is present");
    }
  }
}
