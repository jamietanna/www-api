package me.jvt.www.api.micropub.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import java.time.Clock;
import java.util.*;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.micropub.actions.ActionParser;
import me.jvt.www.api.micropub.client.*;
import me.jvt.www.api.micropub.context.*;
import me.jvt.www.api.micropub.converter.RequestBodyConverter;
import me.jvt.www.api.micropub.decorator.*;
import me.jvt.www.api.micropub.model.MicropubSourceContainerDto;
import me.jvt.www.api.micropub.model.posttype.DefaultMicroformats2ObjectParser;
import me.jvt.www.api.micropub.model.posttype.HugoArticleObjectParser;
import me.jvt.www.api.micropub.model.posttype.Microformats2ObjectFactory;
import me.jvt.www.api.micropub.model.posttype.Microformats2ObjectParser;
import me.jvt.www.api.micropub.model.posttype.MicroformatsPropertyValidatorsFactory;
import me.jvt.www.api.micropub.model.posttype.PostTypeDiscoverer;
import me.jvt.www.api.micropub.repository.MicropubPersistenceRepository;
import me.jvt.www.api.micropub.sanitiser.Microformats2ObjectSanitiser;
import me.jvt.www.api.micropub.sanitiser.SupportedPropertiesSanitiser;
import me.jvt.www.api.micropub.security.MicropubBearerTokenAccessDeniedHandler;
import me.jvt.www.api.micropub.service.*;
import me.jvt.www.api.micropub.util.MediaTypeHelper;
import me.jvt.www.api.micropub.util.RandomStringGenerator;
import me.jvt.www.api.micropub.util.RequestDebugUtil;
import me.jvt.www.api.micropub.util.StringSanitiser;
import me.jvt.www.indieauth.security.IndieAuthBearerTokenResolver;
import me.jvt.www.indieauth.security.ProfileUrlValidator;
import me.jvt.www.logging.LoggingConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.oauth2.server.resource.introspection.NimbusOpaqueTokenIntrospector;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.security.oauth2.server.resource.web.access.BearerTokenAccessDeniedHandler;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@ComponentScan({"me.jvt.www.servlet", "me.jvt.www.indieauth.security"})
@Import({LoggingConfiguration.class, JacksonAutoConfiguration.class})
public class SpringConfiguration {

  @Bean
  public Clock clock() {
    return Clock.systemUTC();
  }

  @Bean
  public ActionParser actionParser(Clock clock) {
    return new ActionParser(clock);
  }

  @Bean
  public ContentDeduplicationClient contentDeduplicationClient(
      RequestSpecificationFactory requestSpecificationFactory) {
    return new ContentDeduplicationClient(requestSpecificationFactory);
  }

  @Bean
  public OpaqueTokenIntrospector indieAuthOpaqueTokenIntrospector(
      @Value("${indieauth.endpoint.token_introspection}")
          String indieAuthTokenIntrospectionEndpoint,
      RestTemplateBuilder restTemplateBuilder) {
    return new NimbusOpaqueTokenIntrospector(
        indieAuthTokenIntrospectionEndpoint, restTemplateBuilder.build());
  }

  @Bean
  public RandomStringGenerator randomStringGenerator() {
    return new RandomStringGenerator();
  }

  @Bean
  public RequestSpecificationFactory requestSpecificationFactory() {
    return new RequestSpecificationFactory();
  }

  @Bean
  public RequestDebugUtil requestDebugUtil(ObjectMapper objectMapper) {
    return new RequestDebugUtil(objectMapper);
  }

  @Bean
  public IndieAuthBearerTokenResolver bearerTokenResolver() {
    return new IndieAuthBearerTokenResolver();
  }

  @Bean
  public StringSanitiser stringSanitiser() {
    return new StringSanitiser();
  }

  @Bean
  public MicropubConfigurationService micropubConfiguration(@Value("${baseUrl}") String baseUrl) {
    return new MicropubConfigurationService(baseUrl);
  }

  @Bean
  public Microformats2PipeClient microformats2PipeClient(WebClient webClient) {
    return new Microformats2PipeClient(webClient);
  }

  @Bean
  public ContextRetrievalService contextRetrievalService(
      TwitterInteractionContextRetriever twitterInteractionContextRetriever,
      BooksMf2ContextRetriever booksMf2ContextRetriever,
      OpengraphMf2ContextRetriever opengraphMf2ContextRetriever,
      MeetupMf2ContextRetriever meetupMf2ContextRetriever,
      EventbriteMf2ContextRetriever eventbriteMf2ContextRetriever,
      PhpMicroformatsContextRetriever phpMicroformatsContextRetriever,
      PostClient postClient,
      ObjectMapper objectMapper) {
    var retrievers = new LinkedHashSet<ContextRetriever>();
    retrievers.add(booksMf2ContextRetriever);
    retrievers.add(opengraphMf2ContextRetriever);
    retrievers.add(meetupMf2ContextRetriever);
    retrievers.add(eventbriteMf2ContextRetriever);
    retrievers.add(phpMicroformatsContextRetriever);
    retrievers.add(twitterInteractionContextRetriever);
    return new ContextRetrievalService(retrievers, postClient, objectMapper);
  }

  @Bean
  public MicropubService micropubService(
      Microformats2ObjectDecoratorFactory decoratorFactory,
      LinkedHashSet<Microformats2ObjectSanitiser> microformats2ObjectSanitisers,
      MicropubPersistenceRepository micropubPersistenceRepository,
      PersistenceService persistenceService,
      ContactsClient contactsClient,
      TaxonomiesClient taxonomiesClient,
      WwwProxyClient<MicropubSourceContainerDto> sourceProxyClient,
      Microformats2ObjectFactory microformats2ObjectFactory,
      SupportedPropertiesSanitiser supportedPropertiesSanitiser,
      ContextRetrievalService contextRetrievalService) {
    return new MicropubServiceImpl(
        decoratorFactory,
        microformats2ObjectSanitisers,
        persistenceService,
        micropubPersistenceRepository,
        contactsClient,
        taxonomiesClient,
        sourceProxyClient,
        microformats2ObjectFactory,
        supportedPropertiesSanitiser,
        contextRetrievalService);
  }

  @Bean
  public MicropubRequestService micropubRequestService(MicropubService micropubService) {
    return new DefaultMicropubRequestService(micropubService);
  }

  @Bean
  public MicropubConflictService micropubConflictService(
      ContentDeduplicationClient contentDeduplicationClient) {
    return new MicropubConflictServiceImpl(contentDeduplicationClient);
  }

  @Bean
  public MicropubMediaService micropubMediaService(
      @Value("${media.baseUrl}") String baseUrl,
      MicropubPersistenceRepository micropubPersistenceRepository,
      MediaTypeHelper mediaTypeHelper) {
    return new MicropubMediaServiceImpl(baseUrl, micropubPersistenceRepository, mediaTypeHelper);
  }

  @Bean
  public MediaTypeHelper mediaTypeHelper() {
    return new MediaTypeHelper();
  }

  @Bean
  public TaxonomiesClient categoriesClient(
      RequestSpecificationFactory requestSpecificationFactory) {
    return new TaxonomiesClient(requestSpecificationFactory);
  }

  @Bean
  public ContactsClient contactsClient(RequestSpecificationFactory requestSpecificationFactory) {
    return new ContactsClient(requestSpecificationFactory);
  }

  @Bean
  public SourceProxyClient sourceProxyClient(
      RequestSpecificationFactory requestSpecificationFactory) {
    return new SourceProxyClient(requestSpecificationFactory);
  }

  @Bean
  public RequestBodyConverter requestBodyConverter(
      ObjectMapper objectMapper, ActionParser actionParser) {
    return new RequestBodyConverter(objectMapper, actionParser);
  }

  @Bean
  public PostTypeDiscoverer discoverer() {
    return new PostTypeDiscoverer();
  }

  @Bean
  public DefaultMicroformats2ObjectParser jsonParser(
      ObjectMapper objectMapper, PostTypeDiscoverer discoverer) {
    return new DefaultMicroformats2ObjectParser(objectMapper, discoverer);
  }

  @Bean
  public YAMLMapper yamlMapper() {
    return new YAMLMapper();
  }

  @Bean
  public HugoArticleObjectParser articleParser(YAMLMapper yamlMapper) {
    return new HugoArticleObjectParser(yamlMapper);
  }

  @Bean
  public List<Microformats2ObjectParser> parsers(HugoArticleObjectParser... parsers) {
    return Arrays.asList(parsers);
  }

  @Bean
  public Microformats2ObjectFactory microformats2ObjectFactory(
      ObjectMapper objectMapper,
      YAMLMapper yamlMapper,
      PostTypeDiscoverer discoverer,
      Microformats2ObjectParser... parsers) {
    return new Microformats2ObjectFactory(
        objectMapper, yamlMapper, discoverer, Arrays.asList(parsers));
  }

  @Bean
  public MicroformatsPropertyValidatorsFactory microformatsPropertyValidatorsFactory(
      @Value("#{'${validator.photo.prefixes}'.split(',')}") Set<String> photoPrefixes) {
    return new MicroformatsPropertyValidatorsFactory(photoPrefixes);
  }

  @Bean
  public BearerTokenAccessDeniedHandler bearerTokenAccessDeniedHandler() {
    return new BearerTokenAccessDeniedHandler();
  }

  @Bean
  public MicropubBearerTokenAccessDeniedHandler accessDeniedHandler(
      BearerTokenAccessDeniedHandler delegate, ProfileUrlValidator profileUrlValidator) {
    return new MicropubBearerTokenAccessDeniedHandler(delegate, profileUrlValidator);
  }
}
