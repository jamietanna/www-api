package me.jvt.www.api.micropub.presentation.rest;

import java.net.URI;
import me.jvt.www.api.micropub.service.MicropubMediaService;
import me.jvt.www.api.micropub.util.RequestDebugUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class MediaController {
  private static final Logger LOGGER = LoggerFactory.getLogger(MediaController.class);
  private static final String HTML_BODY =
      "<html><body><p>Media <img src=\"%s\" /> was uploaded as <a href=\"%s\">%s</a>.</p></body></html>";

  private final MicropubMediaService micropubMediaService;
  private final RequestDebugUtil requestDebugUtil;

  public MediaController(
      MicropubMediaService micropubMediaService, RequestDebugUtil requestDebugUtil) {
    this.micropubMediaService = micropubMediaService;
    this.requestDebugUtil = requestDebugUtil;
  }

  @PostMapping(path = "/micropub/media", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  @PreAuthorize(
      "@profileUrlValidator.isValidProfileUrl(authentication.name) && hasAuthority('SCOPE_media')")
  public ResponseEntity<String> mediaEndpoint(@RequestParam("file") MultipartFile file) {
    LOGGER.debug(
        "Received media endpoint request with body {}", requestDebugUtil.mediaEndpoint(file));

    String createdUrl = micropubMediaService.validateAndSave(file);
    return ResponseEntity.created(URI.create(createdUrl)).build();
  }

  @PostMapping(
      path = "/micropub/media",
      consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
      produces = MediaType.TEXT_HTML_VALUE)
  @PreAuthorize(
      "@profileUrlValidator.isValidProfileUrl(authentication.name) && hasAuthority('SCOPE_media')")
  public String mediaEndpointAsHtml(@RequestParam("file") MultipartFile file) {
    LOGGER.debug(
        "Received media endpoint request with body {}", requestDebugUtil.mediaEndpoint(file));

    String createdUrl = micropubMediaService.validateAndSave(file);
    return String.format(HTML_BODY, createdUrl, createdUrl, createdUrl);
  }
}
