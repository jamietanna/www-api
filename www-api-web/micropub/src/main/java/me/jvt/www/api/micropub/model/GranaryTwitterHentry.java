package me.jvt.www.api.micropub.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GranaryTwitterHentry {
  private List<JsonNode> items;

  public List<JsonNode> getItems() {
    return items;
  }

  public void setItems(List<JsonNode> items) {
    this.items = items;
  }
}
