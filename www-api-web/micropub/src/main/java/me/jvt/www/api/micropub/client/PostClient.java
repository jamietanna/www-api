package me.jvt.www.api.micropub.client;

import java.util.Optional;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object.Context;

public interface PostClient {
  String addContent(Microformats2Object mf2);

  String updateContent(Microformats2Object mf2);

  int upsertCite(Context context);

  Optional<Microformats2Object> getContent(String filePath);
}
