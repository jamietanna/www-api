package me.jvt.www.api.micropub.service.query;

import java.util.Optional;
import me.jvt.www.api.micropub.model.MicropubContactDto;
import me.jvt.www.api.micropub.model.MicropubQueryDto;
import me.jvt.www.api.micropub.service.MicropubService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

@Component
public class QueryContactHandler implements QueryHandler {

  private final MicropubService delegate;

  public QueryContactHandler(MicropubService delegate) {
    this.delegate = delegate;
  }

  @Override
  public String getQuery() {
    return "contact";
  }

  @Override
  public MicropubQueryDto handle(
      MultiValueMap<String, String> parameters, Optional<Authentication> maybeAuthentication) {
    return new MicropubContactDto(delegate.retrieveContacts());
  }
}
