package me.jvt.www.api.micropub.decorator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import me.jvt.www.api.micropub.actions.AddPropertyAction;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.HashtagToTagConverter;
import me.jvt.www.api.micropub.util.SingletonListHelper;

public class HashtagToTagDecorator implements Microformats2ObjectDecorator {

  private static final Pattern HASHTAG_REGEX = Pattern.compile("( #[a-zA-Z0-9]+|^#[a-zA-Z0-9]+)");
  private final HashtagToTagConverter hashtagToTagConverter;

  public HashtagToTagDecorator(HashtagToTagConverter hashtagToTagConverter) {
    this.hashtagToTagConverter = hashtagToTagConverter;
  }

  @Override
  public Microformats2Object decorate(Microformats2Object mf2) {
    if (null == mf2.getProperties().getContent()) {
      return mf2;
    }
    List<String> hashtags = new ArrayList<>();
    Matcher m = HASHTAG_REGEX.matcher(mf2.getProperties().getContent().get(0).get("value"));
    while (m.find()) {
      hashtags.add(m.group());
    }

    String content = mf2.getProperties().getContent().get(0).get("value");
    List<String> categoriesToAdd = new ArrayList<>();
    for (String hashtag : hashtags) {
      hashtag = hashtag.replace(" ", "");
      String tag = hashtagToTagConverter.convert(hashtag);
      categoriesToAdd.add(tag);

      content =
          content.replaceAll(hashtag, String.format("<a href=\"/tags/%s/\">%s</a>", tag, hashtag));
    }
    if (!categoriesToAdd.isEmpty()) {
      AddPropertyAction addCategories = new AddPropertyAction("category", categoriesToAdd);
      addCategories.accept(mf2);
    }

    Map<String, String> contentProperty = new HashMap<>();
    String html = mf2.getProperties().getContent().get(0).get("html");
    if (null != html) {
      contentProperty.put("html", html);
    }
    contentProperty.put("value", content);

    mf2.getProperties().setContent(SingletonListHelper.singletonList(contentProperty));

    return mf2;
  }
}
