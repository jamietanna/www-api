package me.jvt.www.api.micropub.validator.property;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.validator.field.HasAltTextValidator;
import me.jvt.www.api.micropub.validator.field.UrlValidator;

public class PhotoPropertyValidator
    implements ConstraintValidator<Annotation, Microformats2Object> {

  private final UrlValidator urlValidator;
  private final HasAltTextValidator altTextValidator;
  private final Set<String> prefixes;

  public PhotoPropertyValidator(
      UrlValidator urlValidator, HasAltTextValidator altTextValidator, Set<String> prefixes) {
    this.urlValidator = urlValidator;
    this.altTextValidator = altTextValidator;
    this.prefixes = prefixes;
  }

  @Override
  public boolean isValid(Microformats2Object mf2, ConstraintValidatorContext context) {
    List<Map<String, String>> photo = mf2.getProperties().getPhoto();
    if (null == photo) {
      return false;
    }
    for (Map<String, String> photoEntry : photo) {
      if (!photoEntry.containsKey("photo")) {
        return false;
      }

      if (!urlValidator.isValid(photoEntry.get("photo"), context)) {
        return false;
      }

      if (!altTextValidator.isValid(photoEntry, context)) {
        return false;
      }

      if (prefixes.stream().noneMatch(p -> photoEntry.get("photo").startsWith(p))) {
        return false;
      }
    }
    return true;
  }
}
