package me.jvt.www.api.micropub.util;

public class HashtagToTagConverter {

  public String convert(String hashtag) {
    String converted = specialConvert(hashtag);

    if ('#' == converted.charAt(0)) {
      return camelToKebabCase(converted.substring(1));
    } else {
      return converted;
    }
  }

  private String specialConvert(String hashtag) {
    // switch
    if (hashtag.equals(hashtag.toUpperCase())) {
      return hashtag.toLowerCase();
    }

    switch (hashtag) {
      case "#TechNott":
        return "tech-nottingham";
      case "#IndieWeb":
        return "indieweb";
      case "#IndieWebCamp":
        return "indiewebcamp";
      case "#WiTNotts":
        return "wit-notts";
      case "#NottsJS":
      case "#NottsJs":
        return "notts-js";
      case "#LifeAtCapitalOne":
        return "capital-one";
      case "#DevOpsNotts":
        return "devops-notts";
      case "#DotNetNotts":
        return "dotnetnotts";
      case "#PhpMinds":
      case "#PHPMinds":
      case "#PHPMiNDS":
        return "phpminds";
      case "#dni":
        return "diversity-and-inclusion";
      case "#DevOpsDays":
        return "devopsdays";
      case "#DevOpsDaysLDN":
        return "devopsdays-london";
      default:
        // otherwise return it again
        return hashtag;
    }
  }

  // https://gist.github.com/nblackburn/875e6ff75bc8ce171c758bf75f304707#gistcomment-2754350
  private String camelToKebabCase(String str) {
    return str.replaceAll("([a-z0-9])([A-Z])", "$1-$2").toLowerCase();
  }
}
