package me.jvt.www.api.micropub.service.query;

import java.util.Optional;
import me.jvt.www.api.micropub.model.MicropubConfigDto;
import me.jvt.www.api.micropub.model.MicropubQueryDto;
import me.jvt.www.api.micropub.service.MicropubConfigurationService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

@Component
public class QuerySyndicationHandler implements QueryHandler {

  private final MicropubConfigurationService delegate;

  public QuerySyndicationHandler(MicropubConfigurationService delegate) {
    this.delegate = delegate;
  }

  @Override
  public String getQuery() {
    return "syndicate-to";
  }

  @Override
  public MicropubQueryDto handle(
      MultiValueMap<String, String> parameters, Optional<Authentication> maybeAuthentication) {
    MicropubConfigDto dto = new MicropubConfigDto();
    dto.setSyndicateTo(delegate.getSyndicationConfiguration());
    return dto;
  }
}
