package me.jvt.www.api.micropub.model.posttype;

public interface Microformats2ObjectParser {

  /**
   * For a given object, deserialize it from a String, such as from the persistence tier.
   *
   * @param post the String representation of the post
   * @return Microformats2Object representation of the post
   */
  Microformats2Object deserialize(String post);
}
