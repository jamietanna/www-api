package me.jvt.www.api.micropub.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;

public class MicropubPostTypeDto {

  private final String type;
  private final String name;
  private final String h;

  @JsonProperty("required-properties")
  private final List<String> requiredProperties;

  private final List<String> optionalProperties;

  public MicropubPostTypeDto(Kind kind) {
    this.type = kind.getType();
    this.name = kind.getName();
    this.h = kind.getH().replace("h-", "");
    this.requiredProperties =
        kind.getRequiredProperties().stream()
            .map(MicroformatsProperty::getName)
            .collect(Collectors.toList());
    if (requiredProperties.contains("photo")) {
      requiredProperties.add("mp-photo-alt");
    }
    this.optionalProperties =
        kind.getOptionalProperties().stream()
            .map(MicroformatsProperty::getName)
            .collect(Collectors.toList());
    if (optionalProperties.contains("photo")) {
      optionalProperties.add("mp-photo-alt");
    }
    requiredProperties.remove("event");
    optionalProperties.remove("event");
  }

  public String getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public String getH() {
    return h;
  }

  public List<String> getRequiredProperties() {
    return requiredProperties;
  }

  public List<String> getProperties() {
    return Stream.concat(requiredProperties.stream(), optionalProperties.stream())
        .collect(Collectors.toList());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MicropubPostTypeDto that = (MicropubPostTypeDto) o;
    return Objects.equals(type, that.type)
        && Objects.equals(name, that.name)
        && Objects.equals(requiredProperties, that.requiredProperties)
        && Objects.equals(optionalProperties, that.optionalProperties);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, name, requiredProperties, optionalProperties);
  }
}
