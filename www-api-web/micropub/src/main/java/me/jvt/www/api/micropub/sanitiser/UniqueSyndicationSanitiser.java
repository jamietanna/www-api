package me.jvt.www.api.micropub.sanitiser;

import java.util.List;
import java.util.stream.Collectors;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

public class UniqueSyndicationSanitiser implements Microformats2ObjectSanitiser {

  @Override
  public Microformats2Object sanitise(Microformats2Object mf2) {
    List<String> syndication = mf2.getProperties().getSyndication();
    if (null != syndication) {
      List<String> distinctSyndication =
          syndication.stream().distinct().collect(Collectors.toList());
      mf2.getProperties().setSyndication(distinctSyndication);
    }
    return mf2;
  }
}
