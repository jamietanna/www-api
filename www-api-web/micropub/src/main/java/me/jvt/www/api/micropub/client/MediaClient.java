package me.jvt.www.api.micropub.client;

import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

public interface MediaClient {
  String addContent(MultipartFile file, MediaType mediaType);
}
