package me.jvt.www.api.micropub.context;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import me.jvt.www.api.micropub.client.GranaryTwitterClient;
import me.jvt.www.api.micropub.model.GranaryTwitterHentry;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.springframework.stereotype.Component;

@Component
public class TwitterInteractionContextRetriever implements ContextRetriever {

  private static final Pattern TWITTER_URL_PATTERN =
      Pattern.compile("^https://twitter.com/[^/]+/status/([0-9]+)");

  private final GranaryTwitterClient granaryTwitterClient;
  private final ObjectMapper objectMapper;

  public TwitterInteractionContextRetriever(
      GranaryTwitterClient granaryTwitterClient, ObjectMapper objectMapper) {
    this.granaryTwitterClient = granaryTwitterClient;
    this.objectMapper = objectMapper;
  }

  @Override
  public List<JsonNode> retrieve(Microformats2Object mf2) {
    var nodes = new ArrayList<JsonNode>();
    String twitterUrl = null;
    switch (mf2.getKind()) {
      case likes:
        twitterUrl = mf2.getProperties().getLikeOf().get(0);
        break;
      case replies:
        twitterUrl = mf2.getProperties().getInReplyTo().get(0);
        break;
      case reposts:
        twitterUrl = mf2.getProperties().getRepostOf().get(0);
        break;
    }

    if (null == twitterUrl) {
      return nodes;
    }

    Optional<String> maybeActivityId = activityIdFromTwitterUrl(twitterUrl);
    if (!maybeActivityId.isPresent()) {
      return nodes;
    }

    GranaryTwitterHentry granaryTwitterHentry =
        granaryTwitterClient.tweetToHentry(maybeActivityId.get());
    if (0 == granaryTwitterHentry.getItems().size()) {
      return nodes;
    }

    var container = objectMapper.createObjectNode();
    for (var item : granaryTwitterHentry.getItems()) {
      container.withArray("items").add(item);
    }
    nodes.add(container);
    return nodes;
  }

  private Optional<String> activityIdFromTwitterUrl(String url) {
    Matcher matcher = TWITTER_URL_PATTERN.matcher(url);
    if (matcher.find()) {
      return Optional.of(matcher.group(1));
    }
    return Optional.empty();
  }
}
