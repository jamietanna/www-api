package me.jvt.www.api.micropub.model.posttype;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.Properties.PostStatus;

class HugoMicroformats2Json implements Microformats2Object {

  private final ObjectMapper objectMapper;
  private final Kind kind;
  private final Properties properties;
  private final Microformats2Object.Context context;

  HugoMicroformats2Json(
      ObjectMapper objectMapper,
      PostTypeDiscoverer postTypeDiscoverer,
      Properties properties,
      Microformats2Object.Context context) {
    this.objectMapper = objectMapper;
    this.kind = postTypeDiscoverer.discover(context.getH(), properties);
    this.properties = properties;
    this.context = context;
  }

  @Override
  public Kind getKind() {
    return kind;
  }

  @Override
  public Properties getProperties() {
    return properties;
  }

  @Override
  public Microformats2Object.Context getContext() {
    return context;
  }

  @Override
  public String serialize() {
    HugoMicroformats2 mf2 = new HugoMicroformats2();

    // Hugo-specific
    mf2.setAliases(context.getAliases());

    //
    mf2.setClientId(context.getClientId());
    mf2.setDeleted(context.isDeleted());
    mf2.setKind(kind);
    mf2.setH(kind.getH());
    mf2.setSlug(context.getSlug());

    setDraft(mf2);

    //
    mf2.setTags(properties.getCategory());

    // generic properties
    mf2.setProperties(properties);

    try {
      return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(mf2);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }

  private void setDraft(HugoMicroformats2 mf2) {
    List<String> postStatus = properties.getPostStatus();
    if (null == postStatus || postStatus.isEmpty()) {
      return;
    }

    PostStatus actual = PostStatus.published;
    try {
      actual = PostStatus.valueOf(postStatus.get(0));
    } catch (IllegalArgumentException e) {
      // ignore
    }

    mf2.setDraft(PostStatus.draft.equals(actual));
  }

  private Properties onlySupportedProperties(Kind k, Properties old) {
    Properties properties = new Properties();

    for (MicroformatsProperty property : k.getRequiredProperties()) {
      Object value = old.get(property.getName());
      if (null != value) {
        properties.put(property.getName(), value);
      }
    }

    for (MicroformatsProperty property : k.getOptionalProperties()) {
      Object value = old.get(property.getName());
      if (null != value) {
        properties.put(property.getName(), value);
      }
    }

    return properties;
  }

  private static class HugoMicroformats2 {

    @JsonProperty("client_id")
    @JsonInclude(Include.NON_NULL)
    private String clientId;

    @JsonInclude(Include.NON_NULL)
    private String date;

    private boolean deleted;
    private boolean draft;
    private String h;
    private Properties properties;
    private Kind kind;

    @JsonInclude(Include.NON_NULL)
    private String slug;

    private List<String> aliases;

    public HugoMicroformats2() {
      this.properties = new Properties();
    }

    public String getClientId() {
      return clientId;
    }

    public void setClientId(String clientId) {
      this.clientId = clientId;
    }

    public String getDate() {
      if (null == properties.getPublished()) {
        return null;
      }

      return properties.getPublished().get(0);
    }

    public boolean isDeleted() {
      return deleted;
    }

    public void setDeleted(boolean deleted) {
      this.deleted = deleted;
    }

    public boolean isDraft() {
      return draft;
    }

    public void setDraft(boolean draft) {
      this.draft = draft;
    }

    public String getH() {
      return h;
    }

    public void setH(String h) {
      this.h = h;
    }

    public Properties getProperties() {
      return properties;
    }

    public void setProperties(Properties properties) {
      this.properties = properties;
    }

    @JsonInclude(Include.NON_EMPTY)
    public List<String> getTags() {
      return properties.getCategory();
    }

    public void setTags(List<String> tags) {
      properties.setCategory(tags);
    }

    public Kind getKind() {
      return kind;
    }

    public void setKind(Kind kind) {
      this.kind = kind;
    }

    public String getSlug() {
      return slug;
    }

    public void setSlug(String slug) {
      this.slug = slug;
    }

    @JsonInclude(Include.NON_NULL)
    public List<String> getAliases() {
      return aliases;
    }

    public void setAliases(List<String> aliases) {
      this.aliases = aliases;
    }
  }
}
