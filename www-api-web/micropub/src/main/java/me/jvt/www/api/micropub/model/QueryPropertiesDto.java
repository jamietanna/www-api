package me.jvt.www.api.micropub.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import me.jvt.www.api.micropub.model.Properties.ReadStatus;
import me.jvt.www.api.micropub.model.Properties.Rsvp;
import me.jvt.www.api.micropub.model.posttype.Hint;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;

public class QueryPropertiesDto extends MicropubQueryDto {

  private List<Property> properties;

  public List<Property> getProperties() {
    return properties;
  }

  public void setProperties(List<Property> properties) {
    this.properties = properties;
  }

  public static class Property {

    private String name;

    @JsonProperty("display-name")
    private String displayName;

    private List<String> hints;

    @JsonInclude(Include.NON_NULL)
    private List<String> options;

    @JsonProperty("default")
    @JsonInclude(Include.NON_NULL)
    private String defaultValue;

    public Property() {}

    public Property(MicroformatsProperty property) {
      setName(property.getName());
      setDisplayName(property.getDisplayName());
      setHints(property.getHints().stream().map(Hint::getName).collect(Collectors.toList()));
      setOptionsAndDefault(property);
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getDisplayName() {
      return displayName;
    }

    public void setDisplayName(String displayName) {
      this.displayName = displayName;
    }

    public List<String> getHints() {
      return hints;
    }

    public void setHints(List<String> hints) {
      this.hints = hints;
    }

    public List<String> getOptions() {
      return options;
    }

    public void setOptions(List<String> options) {
      this.options = options;
    }

    private void setOptionsAndDefault(MicroformatsProperty property) {
      List<String> options = new ArrayList<>();
      switch (property) {
        case RSVP -> {
          for (Rsvp rsvp : Rsvp.values()) {
            options.add(rsvp.name());
          }
          setDefault(Rsvp.yes.name());
        }
        case POST_STATUS -> {
          for (Properties.PostStatus postStatus : Properties.PostStatus.values()) {
            options.add(postStatus.name());
          }
          setDefault(Properties.PostStatus.published.name());
        }
        case READ_STATUS -> {
          for (ReadStatus readStatus : ReadStatus.values()) {
            options.add(readStatus.getProperty());
          }
          setDefault(ReadStatus.finished.getProperty());
        }
      }
      if (!options.isEmpty()) {
        setOptions(options);
      }
    }

    public String getDefault() {
      return defaultValue;
    }

    public void setDefault(String defaultValue) {
      this.defaultValue = defaultValue;
    }
  }
}
