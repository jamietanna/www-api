package me.jvt.www.api.micropub.util;

import java.util.HashSet;
import java.util.Set;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import org.springframework.http.MediaType;

public class MediaTypeHelper {
  private static final Set<MediaType> VALID_MEDIA_TYPES;

  static {
    VALID_MEDIA_TYPES = new HashSet<>();

    VALID_MEDIA_TYPES.add(MediaType.IMAGE_JPEG);
    VALID_MEDIA_TYPES.add(MediaType.IMAGE_GIF);
    VALID_MEDIA_TYPES.add(MediaType.IMAGE_PNG);
  }

  public String extension(MediaType mediaType) {
    if (!validate(mediaType)) {
      throw new InvalidMicropubMetadataRequest("Media Type not supported");
    }
    if (mediaType.equals(MediaType.IMAGE_GIF)) {
      return ".gif";
    } else if (mediaType.equals(MediaType.IMAGE_JPEG)) {
      return ".jpeg";
    } else if (mediaType.equals(MediaType.IMAGE_PNG)) {
      return ".png";
    }

    return null;
  }

  public boolean validate(MediaType mediaType) {
    return VALID_MEDIA_TYPES.contains(mediaType);
  }
}
