package me.jvt.www.api.micropub.actions.concrete;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import me.jvt.www.api.micropub.actions.AddPropertyAction;
import me.jvt.www.api.micropub.model.MicropubSyndicateTo;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

public class AddSyndicationPropertyAction extends AddPropertyAction {

  public AddSyndicationPropertyAction(List<String> additions) {
    super("syndication", additions);
  }

  @Override
  protected void addIfNotEmpty(Microformats2Object mf2, List existing) {
    mf2.getProperties().setSyndication(handleSyndication((List<String>) existing));
  }

  List<String> handleSyndication(List<String> syndication) {
    // https://stackoverflow.com/a/35821940
    List<String> newSyndication = new ArrayList<>(syndication);

    for (String addition : additions) {
      newSyndication.replaceAll(
          previous -> MicropubSyndicateTo.rewrite(previous, addition) ? addition : previous);
    }
    newSyndication.addAll(additions);
    return newSyndication.stream().distinct().collect(Collectors.toList());
  }
}
