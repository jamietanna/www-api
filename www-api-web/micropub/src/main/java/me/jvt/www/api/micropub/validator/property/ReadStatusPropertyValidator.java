package me.jvt.www.api.micropub.validator.property;

import java.lang.annotation.Annotation;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Properties.ReadStatus;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;

public class ReadStatusPropertyValidator
    implements ConstraintValidator<Annotation, Microformats2Object> {

  @Override
  public boolean isValid(Microformats2Object mf2, ConstraintValidatorContext context) {
    if (!mf2.getProperties().containsKey(MicroformatsProperty.READ_STATUS.getName())) {
      return false;
    }
    List<?> readStatuses = mf2.getProperties().getReadStatus();

    if (null == readStatuses || readStatuses.isEmpty()) {
      return false;
    }

    if (readStatuses.get(0) instanceof ReadStatus) {
      return true;
    } else if (readStatuses.get(0) instanceof String) {
      try {
        ReadStatus.valueOf((String) readStatuses.get(0));
        return true;
      } catch (IllegalArgumentException e) {
        // fall through
      }

      for (ReadStatus readStatus : ReadStatus.values()) {
        if (readStatus.getProperty().equals(readStatuses.get(0))) {
          return true;
        }
      }

      return false;
    }

    throw new InvalidMicropubMetadataRequest(
        "The read-status property was not populated with a valid value");
  }
}
