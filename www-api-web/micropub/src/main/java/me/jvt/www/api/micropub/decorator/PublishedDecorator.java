package me.jvt.www.api.micropub.decorator;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import me.jvt.www.api.micropub.actions.ReplacePropertyAction;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;

public class PublishedDecorator implements Microformats2ObjectDecorator {
  private static final DateTimeFormatter DATE_TIME_FORMATTER =
      DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("UTC"));

  private final Clock clock;

  public PublishedDecorator() {
    clock = Clock.systemUTC();
  }

  PublishedDecorator(Clock clock) {
    this.clock = clock;
  }

  /**
   * Decorate the object with a <code>published</code> property, if not already present. ,
   *
   * @param mf2 the given {@link Microformats2Object} to decorate
   * @return the output {@link Microformats2Object}, which may be different than <code>
   *     mf2</code>
   */
  @Override
  public Microformats2Object decorate(Microformats2Object mf2) {
    if (null == mf2.getProperties().getPublished()
        || mf2.getProperties().getPublished().isEmpty()) {
      String date = DATE_TIME_FORMATTER.format(Instant.now(clock));
      ReplacePropertyAction action =
          new ReplacePropertyAction("published", SingletonListHelper.singletonList(date));
      action.accept(mf2);
    }

    return mf2;
  }
}
