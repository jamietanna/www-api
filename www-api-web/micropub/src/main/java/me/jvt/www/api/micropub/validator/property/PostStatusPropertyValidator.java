package me.jvt.www.api.micropub.validator.property;

import java.lang.annotation.Annotation;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Properties.PostStatus;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;

public class PostStatusPropertyValidator
    implements ConstraintValidator<Annotation, Microformats2Object> {

  @Override
  public boolean isValid(Microformats2Object mf2, ConstraintValidatorContext context) {
    if (!mf2.getProperties().containsKey(MicroformatsProperty.POST_STATUS.getName())) {
      return false;
    }
    List<?> postStatuses = mf2.getProperties().getPostStatus();

    if (null == postStatuses || postStatuses.isEmpty()) {
      return false;
    }

    if (postStatuses.get(0) instanceof PostStatus) {
      return true;
    } else if (postStatuses.get(0) instanceof String) {
      try {
        PostStatus.valueOf((String) postStatuses.get(0));
        return true;
      } catch (IllegalArgumentException e) {
        // fall through
      }

      for (PostStatus postStatus : PostStatus.values()) {
        if (postStatus.name().equals(postStatuses.get(0))) {
          return true;
        }
      }

      return false;
    }

    throw new InvalidMicropubMetadataRequest(
        "The post-status property was not populated with a valid value");
  }
}
