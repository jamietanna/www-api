package me.jvt.www.api.micropub.client;

import io.restassured.response.Response;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.micropub.model.gitlab.CreateCommitRequestBody;
import me.jvt.www.api.micropub.model.gitlab.CreateCommitRequestBody.Action;
import me.jvt.www.api.micropub.model.gitlab.CreateCommitRequestBody.Action.ActionType;
import me.jvt.www.api.micropub.util.MediaTypeHelper;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

public class GitLabMediaClient extends AbstractGitLabClient implements MediaClient {
  private final MediaTypeHelper mediaTypeHelper;

  public GitLabMediaClient(
      Configuration configuration,
      RequestSpecificationFactory requestSpecificationFactory,
      MediaTypeHelper mediaTypeHelper) {
    super(configuration, requestSpecificationFactory);
    this.mediaTypeHelper = mediaTypeHelper;
  }

  public String addContent(MultipartFile file, MediaType mediaType) {
    String slug;
    CreateCommitRequestBody commitRequestBody = new CreateCommitRequestBody();
    commitRequestBody.setBranch("main");
    commitRequestBody.setCommitMessage("MP: Add file from media endpoint");
    try {
      slug = DigestUtils.sha256Hex(Base64.getEncoder().encode(file.getBytes())).substring(0, 10);
    } catch (IOException e) {
      throw new RuntimeException("Was unable to parse the bytes from the MultipartFile", e);
    }

    Action action = new Action();
    action.setAction(ActionType.CREATE);

    slug += mediaTypeHelper.extension(mediaType);

    action.setEncoding("base64");
    action.setFilePath(String.format("public/%s", slug));
    try {
      action.setContent(Base64.getEncoder().encodeToString(file.getBytes()));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    List<Action> actions = SingletonListHelper.singletonList(action);

    commitRequestBody.setActions(actions);

    Response gitLabResponse =
        this.requestSpecificationFactory
            .newRequestSpecification()
            .baseUri(this.configuration.getApiEndpoint())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header("PRIVATE-TOKEN", this.configuration.getAccessToken())
            .body(commitRequestBody)
            .post(COMMITS_API_FORMAT_STRING);
    if (HttpStatus.CREATED.value() == gitLabResponse.getStatusCode()
        || isConflict(gitLabResponse)) {
      return slug;
    } else {
      throw new RuntimeException(
          String.format(
              "Was unable to process the request: HTTP %d returned from GitLab",
              gitLabResponse.getStatusCode()));
    }
  }
}
