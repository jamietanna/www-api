package me.jvt.www.api.micropub.service;

import java.util.Optional;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

public interface MicropubConflictService {
  Optional<String> isConflictFoundInCache(Microformats2Object mf2);

  void updateCacheWithSlug(Microformats2Object mf2, String slug);
}
