package me.jvt.www.api.micropub.actions;

import java.util.List;
import java.util.Objects;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

public class AddPropertyAction implements Action {

  protected final String field;
  protected final List<String> additions;

  public AddPropertyAction(String field, List<String> additions) {
    this.field = field;
    this.additions = additions;
  }

  @Override
  public void accept(Microformats2Object mf2) {
    if (additions.isEmpty()) {
      return;
    }

    List existing = (List) mf2.getProperties().get(field);
    if (null == existing) {
      mf2.getProperties().put(field, additions);
    } else {
      addIfNotEmpty(mf2, existing);
    }
  }

  protected void addIfNotEmpty(Microformats2Object mf2, List existing) {
    existing.addAll(additions);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AddPropertyAction that = (AddPropertyAction) o;
    return Objects.equals(field, that.field) && Objects.equals(additions, that.additions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(field, additions);
  }
}
