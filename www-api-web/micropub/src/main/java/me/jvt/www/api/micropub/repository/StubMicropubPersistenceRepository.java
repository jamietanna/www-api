package me.jvt.www.api.micropub.repository;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import me.jvt.www.api.micropub.client.PostClient;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.service.MicropubConflictService;
import me.jvt.www.api.micropub.service.PersistenceService;
import me.jvt.www.api.micropub.service.PersistenceService.PersistenceAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

public class StubMicropubPersistenceRepository implements MicropubPersistenceRepository {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(StubMicropubPersistenceRepository.class);

  private final PersistenceService persistenceService;
  private final MicropubConflictService micropubConflictService;
  private final PostClient gitLabClient;

  public StubMicropubPersistenceRepository(
      PersistenceService persistenceService,
      MicropubConflictService micropubConflictService,
      PostClient postClient) {
    this.persistenceService = persistenceService;
    this.micropubConflictService = micropubConflictService;
    this.gitLabClient = postClient;
  }

  @Override
  public String save(MultipartFile multipartFile, MediaType mediaType) {
    LOGGER.info(
        "Received request to save a file with original filename {} and content type {}",
        multipartFile.getOriginalFilename(),
        mediaType.toString());
    return multipartFile.getOriginalFilename();
  }

  @Override
  public String save(@Valid Microformats2Object content) {
    boolean isUpdate = content.getContext().getSlug() != null;

    PersistenceAction action = persistenceService.determineAction(content);
    String serializedPost = persistenceService.serialize(action, content);
    String serializedAndEncodedPost = Base64.getEncoder().encodeToString(serializedPost.getBytes());
    JsonNode context = content.getContext().getContext();
    if (null != context) {
      List<String> serializedCites = persistenceService.serialize(context);
      for (String serializedCite : serializedCites) {
        LOGGER.info(
            "Received request to upsert cite with encoded body {}",
            Base64.getEncoder().encodeToString(serializedCite.getBytes()));
      }
    }

    if (isUpdate) {
      LOGGER.info(
          "Received request to update {} with encoded body {}",
          content.getContext().getSlug(),
          serializedAndEncodedPost);
    } else {
      Optional<String> maybeExistingUrl = micropubConflictService.isConflictFoundInCache(content);
      if (maybeExistingUrl.isPresent()) {
        return maybeExistingUrl.get();
      }

      LOGGER.info(
          "Received request to create new object with encoded body {}", serializedAndEncodedPost);

      micropubConflictService.updateCacheWithSlug(content, serializedAndEncodedPost);
    }
    return serializedAndEncodedPost;
  }

  @Override
  public Optional<Microformats2Object> findById(String s) {
    return gitLabClient.getContent(s);
  }
}
