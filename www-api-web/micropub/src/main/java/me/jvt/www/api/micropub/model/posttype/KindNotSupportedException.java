package me.jvt.www.api.micropub.model.posttype;

import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;

public class KindNotSupportedException extends InvalidMicropubMetadataRequest {

  public KindNotSupportedException(String message) {
    super(message);
  }
}
