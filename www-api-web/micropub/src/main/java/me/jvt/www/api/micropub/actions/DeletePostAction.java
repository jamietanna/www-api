package me.jvt.www.api.micropub.actions;

import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

public class DeletePostAction implements Action {
  @Override
  public void accept(Microformats2Object mf2) {
    mf2.getContext().setDeleted(true);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    return o != null && getClass() == o.getClass();
  }
}
