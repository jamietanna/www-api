package me.jvt.www.api.micropub.service;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import me.jvt.www.api.micropub.actions.Action;
import me.jvt.www.api.micropub.actions.ReplacePropertyAction;
import me.jvt.www.api.micropub.client.ContactsClient;
import me.jvt.www.api.micropub.client.TaxonomiesClient;
import me.jvt.www.api.micropub.client.WwwProxyClient;
import me.jvt.www.api.micropub.decorator.Microformats2ObjectDecorator;
import me.jvt.www.api.micropub.decorator.Microformats2ObjectDecoratorFactory;
import me.jvt.www.api.micropub.decorator.WeekNotesDecorator;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.MicropubContactDto.MicropubContact;
import me.jvt.www.api.micropub.model.MicropubSourceContainerDto;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2ObjectFactory;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;
import me.jvt.www.api.micropub.model.request.MicropubRequest;
import me.jvt.www.api.micropub.repository.MicropubPersistenceRepository;
import me.jvt.www.api.micropub.sanitiser.Microformats2ObjectSanitiser;
import me.jvt.www.api.micropub.sanitiser.SupportedPropertiesSanitiser;
import me.jvt.www.api.micropub.sanitiser.UniqueCategorySanitiser;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MicropubServiceImpl implements MicropubService {

  private static final Logger LOG = LoggerFactory.getLogger(MicropubServiceImpl.class);

  private final Microformats2ObjectDecoratorFactory decoratorFactory;
  private final LinkedHashSet<Microformats2ObjectSanitiser> microformats2ObjectSanitisers;
  private final PersistenceService persistenceService;
  private final MicropubPersistenceRepository micropubPersistenceRepository;
  private final ContactsClient contactsClient;
  private final TaxonomiesClient taxonomiesClient;
  private final WwwProxyClient<MicropubSourceContainerDto> sourceProxyClient;
  private final Microformats2ObjectFactory mf2Factory;
  private final SupportedPropertiesSanitiser supportedPropertiesSanitiser;
  private final ContextRetrievalService contextRetrievalService;

  public MicropubServiceImpl(
      Microformats2ObjectDecoratorFactory decoratorFactory,
      LinkedHashSet<Microformats2ObjectSanitiser> microformats2ObjectSanitisers,
      PersistenceService persistenceService,
      MicropubPersistenceRepository micropubPersistenceRepository,
      ContactsClient contactsClient,
      TaxonomiesClient taxonomiesClient,
      WwwProxyClient<MicropubSourceContainerDto> sourceProxyClient,
      Microformats2ObjectFactory mf2Factory,
      SupportedPropertiesSanitiser supportedPropertiesSanitiser,
      ContextRetrievalService contextRetrievalService) {
    this.decoratorFactory = decoratorFactory;
    this.microformats2ObjectSanitisers = microformats2ObjectSanitisers;
    this.persistenceService = persistenceService;
    this.micropubPersistenceRepository = micropubPersistenceRepository;
    this.contactsClient = contactsClient;
    this.taxonomiesClient = taxonomiesClient;
    this.sourceProxyClient = sourceProxyClient;
    this.mf2Factory = mf2Factory;
    this.supportedPropertiesSanitiser = supportedPropertiesSanitiser;
    this.contextRetrievalService = contextRetrievalService;
  }

  @Override
  public Microformats2Object create(MicropubRequest request) {
    return mf2Factory.create(request);
  }

  @Override
  public Microformats2Object performDecoration(Microformats2Object mf2) {
    Microformats2Object currentMf2 = mf2;
    for (Microformats2ObjectDecorator decorator : decoratorFactory.provide(mf2.getKind())) {
      currentMf2 = decorator.decorate(currentMf2);
    }
    return currentMf2;
  }

  @Override
  public Microformats2Object performSanitisation(Microformats2Object mf2) {
    Microformats2Object currentMf2 = mf2;
    for (Microformats2ObjectSanitiser sanitiser : microformats2ObjectSanitisers) {
      currentMf2 = sanitiser.sanitise(currentMf2);
    }
    return currentMf2;
  }

  @Override
  public String save(Microformats2Object mf2) {
    var sanitised = supportedPropertiesSanitiser.sanitise(mf2);
    String slug = micropubPersistenceRepository.save(sanitised);
    contextRetrievalService.retrieveAndPersistContext(sanitised);
    return slug;
  }

  @Override
  public Optional<Microformats2Object> retrieve(String url, boolean canClientViewDeleted) {
    Optional<String> optionalPath = persistenceService.contentPathFromUrl(url);
    if (!optionalPath.isPresent()) {
      throw new InvalidMicropubMetadataRequest(
          String.format("URL %s is not supported for retrieve/update", url));
    }
    String path = optionalPath.get();

    Optional<Microformats2Object> optionalMf2 = micropubPersistenceRepository.findById(path);
    if (!optionalMf2.isPresent()) {
      LOG.info("The source data for {} ({}) could not be found", url, path);
      return Optional.empty();
    }

    Microformats2Object mf2 = optionalMf2.get();
    if (mf2.getContext().isDeleted() && !canClientViewDeleted) {
      LOG.info(
          "The source data for {} ({}) was found, but the client has no permission to view it, as it's been marked as deleted",
          url,
          path);
      return Optional.empty();
    }

    if (null == mf2.getContext().getSlug() && Kind.articles.equals(mf2.getKind())) {
      mf2 = new WeekNotesDecorator().decorate(mf2);
    }

    List<Map<String, String>> contentList = mf2.getProperties().getContent();
    if (null != contentList && !contentList.isEmpty()) {
      Map<String, String> content = contentList.get(0);
      if (null == content.get("html") || content.get("html").isEmpty()) {
        content.put("html", "");
      }
    }

    new UniqueCategorySanitiser().sanitise(mf2);
    mf2 = supportedPropertiesSanitiser.sanitise(mf2);

    return Optional.of(mf2);
  }

  @Override
  public MicropubSourceContainerDto retrieveAll() {
    MicropubSourceContainerDto container = sourceProxyClient.retrieve();
    container.items.removeIf(c -> null == c.getProperties());
    return container;
  }

  @Override
  public Set<String> retrieveCategories() {
    Set<String> categories = taxonomiesClient.retrieve().getCategories();
    categories.removeIf(s -> s.equals(""));
    return categories;
  }

  @Override
  public List<MicropubContact> retrieveContacts() {
    return contactsClient.retrieve().getContacts();
  }

  @Override
  public Microformats2Object update(Microformats2Object mf2, List<Action> actions) {
    Optional<Properties.PostStatus> previousPostStatus = getPostStatus(mf2);
    actions.forEach(a -> a.accept(mf2));

    if (previousPostStatus.isPresent()
        && previousPostStatus.get().equals(Properties.PostStatus.published)) {
      new ReplacePropertyAction(
              MicroformatsProperty.POST_STATUS.getName(),
              SingletonListHelper.singletonList(Properties.PostStatus.published.name()))
          .accept(mf2);
    }
    return mf2;
  }

  private static Optional<Properties.PostStatus> getPostStatus(Microformats2Object mf2) {
    List<String> postStatuses = mf2.getProperties().getPostStatus();
    Optional<Properties.PostStatus> postStatus = Optional.empty();
    if (!(null == postStatuses || postStatuses.isEmpty())) {
      try {
        postStatus = Optional.of(Properties.PostStatus.valueOf(postStatuses.get(0)));
      } catch (IllegalArgumentException e) {
        // ignore
      }
    }

    return postStatus;
  }
}
