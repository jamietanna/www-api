package me.jvt.www.api.micropub.context;

import com.fasterxml.jackson.databind.JsonNode;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;
import me.jvt.www.api.micropub.client.Microformats2PipeClient;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.springframework.stereotype.Component;

@Component
public class MeetupMf2ContextRetriever implements ContextRetriever {
  private final Microformats2PipeClient client;

  public MeetupMf2ContextRetriever(Microformats2PipeClient client) {
    this.client = client;
  }

  @Override
  public List<JsonNode> retrieve(Microformats2Object mf2) {
    if (mf2.getProperties().getInReplyTo() == null
        || mf2.getProperties().getInReplyTo().isEmpty()) {
      return List.of();
    }
    URI existingUrl;
    try {
      existingUrl = URI.create(mf2.getProperties().getInReplyTo().get(0));
    } catch (IllegalArgumentException ignored) {
      return List.of();
    }
    if (!(existingUrl.getHost().equals("meetup.com")
        || existingUrl.getHost().equals("www.meetup.com"))) {
      return List.of();
    }

    try {
      URL pipeUrl = new URL("https", "meetup-mf2.herokuapp.com", existingUrl.getPath());
      var maybeContext = client.retrieve(pipeUrl.toString());
      if (maybeContext.isPresent()) {
        return List.of(maybeContext.get());
      }
    } catch (MalformedURLException e) {
      throw new IllegalStateException(e);
    }
    return List.of();
  }
}
