package me.jvt.www.api.micropub.actions;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import me.jvt.www.api.micropub.actions.concrete.AddSyndicationPropertyAction;
import me.jvt.www.api.micropub.actions.concrete.ReplaceContentPropertyAction;
import me.jvt.www.api.micropub.actions.concrete.ReplaceRsvpPropertyAction;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Properties.PostStatus;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;

public class ActionParser {

  private static final String ADD_PROPERTY_FIELD = "add";
  private static final String DELETE_PROPERY_FIELD = "delete";
  private static final String REPLACE_PROPERY_FIELD = "replace";
  private static final String DELETE_ACTION = "delete";
  private static final String UNDELETE_ACTION = "undelete";
  private static final String UPDATE_ACTION = "update";

  private static final String CONTENT = "content";
  private static final String SYNDICATION = "syndication";
  private static final String RSVP = "rsvp";

  private static final Set<String> SUPPORTED_ACTIONS;
  private static final String UNSUPPORTED_ERROR =
      "Only `delete`, `undelete` and `update` are supported";

  private static final DateTimeFormatter DATE_TIME_FORMATTER =
      DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("UTC"));

  static {
    SUPPORTED_ACTIONS = new HashSet<>();
    SUPPORTED_ACTIONS.add(DELETE_ACTION);
    SUPPORTED_ACTIONS.add(UNDELETE_ACTION);
    SUPPORTED_ACTIONS.add(UPDATE_ACTION);
  }

  private final Clock clock;

  public ActionParser(Clock clock) {
    this.clock = clock;
  }

  public List<Action> parse(Map<String, Object> requestBody) {
    String action = (String) requestBody.get("action");
    if (!SUPPORTED_ACTIONS.contains(action)) {
      throw new InvalidMicropubMetadataRequest(UNSUPPORTED_ERROR);
    }
    List<Action> actions = new ArrayList<>();

    if (requestBody.containsKey(ADD_PROPERTY_FIELD)) {
      Map<String, List<String>> add =
          (Map<String, List<String>>) requestBody.get(ADD_PROPERTY_FIELD);
      for (Map.Entry<String, List<String>> entry : add.entrySet()) {
        if (CONTENT.equals(entry.getKey())) {
          actions.add(new ReplaceContentPropertyAction(entry.getValue()));
        } else if (SYNDICATION.equals(entry.getKey())) {
          actions.add(new AddSyndicationPropertyAction(entry.getValue()));
        } else {
          actions.add(new AddPropertyAction(entry.getKey(), entry.getValue()));
        }
      }
    }

    if (requestBody.containsKey(DELETE_PROPERY_FIELD)) {
      if (requestBody.get(DELETE_PROPERY_FIELD) instanceof Collection) {
        List<String> delete = (List<String>) requestBody.get(DELETE_PROPERY_FIELD);
        for (String property : delete) {
          actions.add(new DeletePropertyAction(property));
        }
      } else {
        /* TODO: validate that there is the correct thing here to cast to, else raise an InvalidMicropubMetadataRequest */
        Map<String, List<String>> delete =
            (Map<String, List<String>>) requestBody.get(DELETE_PROPERY_FIELD);
        for (Map.Entry<String, List<String>> entry : delete.entrySet()) {
          actions.add(new DeletePropertyAction(entry.getKey(), entry.getValue()));
        }
      }
    }

    if (requestBody.containsKey(REPLACE_PROPERY_FIELD)) {
      Map<String, List<String>> add =
          (Map<String, List<String>>) requestBody.get(REPLACE_PROPERY_FIELD);
      for (Map.Entry<String, List<String>> entry : add.entrySet()) {
        if (CONTENT.equals(entry.getKey())) {
          actions.add(new ReplaceContentPropertyAction(entry.getValue()));
        } else if (RSVP.equals(entry.getKey())) {
          actions.add(new ReplaceRsvpPropertyAction(entry.getValue()));
        } else {
          actions.add(new ReplacePropertyAction(entry.getKey(), entry.getValue()));
          if (MicroformatsProperty.POST_STATUS.getName().equals(entry.getKey())) {
            if (entry.getValue().contains(PostStatus.published.name())) {
              actions.add(
                  new ReplacePropertyAction(
                      MicroformatsProperty.PUBLISHED.getName(),
                      Collections.singletonList(DATE_TIME_FORMATTER.format(Instant.now(clock)))));
            }
          }
        }
      }
    }

    if (DELETE_ACTION.equals(action)) {
      actions.add(new DeletePostAction());
    }

    if (UNDELETE_ACTION.equals(action)) {
      actions.add(new UndeletePostAction());
    }

    if (actions.isEmpty()) {
      throw new InvalidMicropubMetadataRequest(
          "No update actions could be parsed from the request");
    }

    return actions;
  }
}
