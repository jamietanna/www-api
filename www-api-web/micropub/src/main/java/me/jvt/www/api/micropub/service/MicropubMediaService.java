package me.jvt.www.api.micropub.service;

import me.jvt.www.api.micropub.exception.InvalidMediaException;
import org.springframework.web.multipart.MultipartFile;

public interface MicropubMediaService {

  /**
   * Validate the {@link MultipartFile} is well-formed, and then save it in the persistence tier.
   *
   * @param file to save
   * @return the URL of the created media file
   * @throws InvalidMediaException if the media uploaded is not valid
   */
  String validateAndSave(MultipartFile file) throws InvalidMediaException;
}
