package me.jvt.www.api.micropub.model;

import java.util.Set;

public class MicropubCategoriesDto extends MicropubQueryDto {

  private final Set<String> categories;

  public MicropubCategoriesDto(Set<String> categories) {
    this.categories = categories;
  }

  public Set<String> getCategories() {
    return categories;
  }
}
