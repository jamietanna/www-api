package me.jvt.www.api.micropub.repository;

import java.util.Optional;
import javax.validation.Valid;
import me.jvt.www.api.micropub.client.MediaClient;
import me.jvt.www.api.micropub.client.PostClient;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.service.MicropubConflictService;
import me.jvt.www.api.micropub.service.PersistenceService;
import me.jvt.www.api.micropub.service.PersistenceService.PersistenceAction;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

public class DefaultMicropubPersistenceRepository implements MicropubPersistenceRepository {

  private final PostClient postClient;
  private final MediaClient mediaClient;
  private final MicropubConflictService micropubConflictService;
  private final PersistenceService persistenceService;

  public DefaultMicropubPersistenceRepository(
      PostClient postClient,
      MediaClient mediaClient,
      MicropubConflictService micropubConflictService,
      PersistenceService persistenceService) {
    this.postClient = postClient;
    this.mediaClient = mediaClient;
    this.micropubConflictService = micropubConflictService;
    this.persistenceService = persistenceService;
  }

  @Override
  public String save(MultipartFile multipartFile, MediaType mediaType) {
    return this.mediaClient.addContent(multipartFile, mediaType);
  }

  public String save(@Valid Microformats2Object content) {
    if (content.getContext().getSlug() != null) {
      PersistenceAction action = persistenceService.determineAction(content);
      Optional<Microformats2Object> maybePost = postClient.getContent(action.getPath());
      if (maybePost.isPresent()) {
        return postClient.updateContent(content);
      } else {
        return postClient.addContent(content);
      }
    }

    Optional<String> maybeExistingUrl = micropubConflictService.isConflictFoundInCache(content);
    if (maybeExistingUrl.isPresent()) {
      return maybeExistingUrl.get();
    }

    String slug = this.postClient.addContent(content);

    micropubConflictService.updateCacheWithSlug(content, slug);

    return slug;
  }

  public Optional<Microformats2Object> findById(String s) {
    return postClient.getContent(s);
  }
}
