package me.jvt.www.api.micropub.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TaxonomiesResponse {

  private Set<String> categories;

  public TaxonomiesResponse() {}

  public TaxonomiesResponse(Set<String> categories) {
    this.categories = categories;
  }

  public Set<String> getCategories() {
    return categories;
  }

  @JsonProperty("tags")
  public void setCategories(Set<String> categories) {
    this.categories = categories;
  }
}
