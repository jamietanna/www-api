package me.jvt.www.api.micropub.client;

import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.micropub.model.MicropubContactDto;

public class ContactsClient extends WwwProxyClient<MicropubContactDto> {

  public ContactsClient(RequestSpecificationFactory requestSpecificationFactory) {
    super(MicropubContactDto.class, requestSpecificationFactory, "/contacts.json");
  }
}
