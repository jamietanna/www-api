package me.jvt.www.api.micropub.model;

import java.util.List;

public class MicropubSourceContainerDto extends MicropubQueryDto {
  public List<MicropubSourceDto> items;
}
