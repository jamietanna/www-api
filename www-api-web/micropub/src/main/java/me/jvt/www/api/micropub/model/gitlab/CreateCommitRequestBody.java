package me.jvt.www.api.micropub.model.gitlab;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Objects;

public class CreateCommitRequestBody {

  @JsonProperty("branch")
  private String branch;

  @JsonProperty("commit_message")
  private String commitMessage;

  @JsonProperty("actions")
  private List<Action> actions;

  public String getBranch() {
    return branch;
  }

  public void setBranch(String branch) {
    this.branch = branch;
  }

  public List<Action> getActions() {
    return actions;
  }

  public void setActions(List<Action> actions) {
    this.actions = actions;
  }

  public String getCommitMessage() {
    return commitMessage;
  }

  public void setCommitMessage(String commitMessage) {
    this.commitMessage = commitMessage;
  }

  public static class Action {

    @JsonProperty("action")
    private String action;

    @JsonProperty("content")
    private String content;

    private String encoding = "text";

    @JsonProperty("file_path")
    private String filePath;

    public String getEncoding() {
      return encoding;
    }

    public void setEncoding(String encoding) {
      this.encoding = encoding;
    }

    public String getAction() {
      return action;
    }

    public void setAction(ActionType action) {
      this.action = action.getAction();
    }

    public String getFilePath() {
      return filePath;
    }

    public void setFilePath(String filePath) {
      this.filePath = filePath;
    }

    public String getContent() {
      return content;
    }

    public void setContent(String content) {
      this.content = content;
    }

    public enum ActionType {
      CREATE("create"),
      UPDATE("update");

      private final String action;

      ActionType(String action) {
        this.action = action;
      }

      public String getAction() {
        return action;
      }
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      Action action1 = (Action) o;
      return Objects.equals(action, action1.action)
          && Objects.equals(content, action1.content)
          && Objects.equals(encoding, action1.encoding)
          && Objects.equals(filePath, action1.filePath);
    }

    @Override
    public int hashCode() {
      return Objects.hash(action, content, encoding, filePath);
    }
  }
}
