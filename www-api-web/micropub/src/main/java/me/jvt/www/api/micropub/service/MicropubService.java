package me.jvt.www.api.micropub.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import me.jvt.www.api.micropub.actions.Action;
import me.jvt.www.api.micropub.decorator.Microformats2ObjectDecorator;
import me.jvt.www.api.micropub.model.MicropubContactDto.MicropubContact;
import me.jvt.www.api.micropub.model.MicropubSourceContainerDto;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.request.MicropubRequest;
import me.jvt.www.api.micropub.sanitiser.Microformats2ObjectSanitiser;

public interface MicropubService {

  /**
   * Construct the relevant {@link Microformats2Object} for the provided {@link MicropubRequest}.
   *
   * @param request the request from the Micropub endpoint
   * @return a {@link Microformats2Object} representation
   */
  Microformats2Object create(MicropubRequest request);

  /**
   * Perform decoration(s) on the provided {@link Microformats2Object} using pre-registered {@link
   * Microformats2ObjectDecorator}.
   *
   * @param mf2 the given {@link Microformats2Object} to decorate
   * @return the output {@link Microformats2Object}, which may be different than <code>
   * mf2</code>
   */
  Microformats2Object performDecoration(Microformats2Object mf2);

  /**
   * Perform sanitisation on the provided {@link Microformats2Object} using pre-registered {@link
   * Microformats2ObjectSanitiser}.
   *
   * @param mf2 the given {@link Microformats2Object} to decorate
   * @return the output {@link Microformats2Object}, which may be different than <code>
   * mf2</code>
   */
  Microformats2Object performSanitisation(Microformats2Object mf2);

  /**
   * Save the {@link Microformats2Object} object in the persistence tier.
   *
   * @param mf2 to save
   * @return the slug of the post, as a String
   */
  String save(Microformats2Object mf2);

  /**
   * Retrieve a given {@link Microformats2Object} object from the persistence tier, given the post's
   * public URL.
   *
   * @param url of the post to retrieve
   * @param canClientViewDeleted whether the client has permissions to view the file's content, if
   *     it is deleted
   * @return the {@link Microformats2Object} representation of that file, or an empty {@link
   *     Optional}
   */
  Optional<Microformats2Object> retrieve(String url, boolean canClientViewDeleted);

  /**
   * Retrieve a list of all pieces of content that the site currently has (in an undeleted form).
   *
   * @return a list of all pieces of content on the site.
   */
  MicropubSourceContainerDto retrieveAll();

  /**
   * Retrieve the categories that the site is currently using, i.e. to enable autocomplete.
   *
   * @return the categories currently used on the site
   */
  Set<String> retrieveCategories();

  /**
   * Retrieve the contacts that the site knows about, for use with @-mentioning.
   *
   * @return the {@link MicropubContact} representations of the contacts
   */
  List<MicropubContact> retrieveContacts();

  /**
   * Update a {@link Microformats2Object} with a set of given {@link Action}s, returning the result.
   *
   * @param mf2 the mf2 object to update
   * @param actions the {@link Action}s to apply to the object
   * @return the updated mf2 object
   */
  Microformats2Object update(Microformats2Object mf2, List<Action> actions);
}
