package me.jvt.www.api.micropub.exception;

public class InvalidMicropubMetadataRequest extends IllegalArgumentException {

  public InvalidMicropubMetadataRequest(String message) {
    super(message);
  }

  public InvalidMicropubMetadataRequest(String message, Throwable e) {
    super(message, e);
  }
}
