package me.jvt.www.api.micropub.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MicropubContactDto extends MicropubQueryDto {

  private List<MicropubContact> contacts;

  public MicropubContactDto() {}

  public MicropubContactDto(List<MicropubContact> contacts) {
    this.contacts = contacts;
  }

  public List<MicropubContact> getContacts() {
    return contacts.stream().filter(not(MicropubContact::isEmpty)).collect(Collectors.toList());
  }

  private static <R> Predicate<R> not(Predicate<R> predicate) {
    return predicate.negate();
  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class MicropubContact {

    private String name;
    private String nickname;
    private String url;
    private String internalUrl;
    private Map<String, String> silos;

    public MicropubContact() {
      silos = new HashMap<>();
    }

    @JsonProperty("_internal_url")
    public String getInternalUrl() {
      return internalUrl;
    }

    @JsonProperty("_internal_url")
    public void setInternalUrl(String internalUrl) {
      this.internalUrl = internalUrl;
    }

    public String getNickname() {
      return nickname;
    }

    public void setNickname(String nickname) {
      this.nickname = nickname;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public Map<String, String> getSilos() {
      return silos;
    }

    public void setSilos(Map<String, String> silos) {
      if (null == silos) {
        this.silos = new HashMap<>();
      } else {
        this.silos = silos;
      }
    }

    public String getUrl() {
      return url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    boolean isEmpty() {
      return null == name && null == nickname && null == url;
    }
  }
}
