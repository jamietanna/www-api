package me.jvt.www.api.micropub.model.posttype;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.List;
import me.jvt.www.api.micropub.model.Properties;

public class DefaultMicroformats2ObjectParser implements Microformats2ObjectParser {

  private final ObjectMapper objectMapper;
  private final PostTypeDiscoverer postTypeDiscoverer;

  public DefaultMicroformats2ObjectParser(
      ObjectMapper objectMapper, PostTypeDiscoverer postTypeDiscoverer) {
    this.objectMapper = objectMapper;
    this.postTypeDiscoverer = postTypeDiscoverer;
  }

  @Override
  public Microformats2Object deserialize(String post) {
    ObjectNode node;
    try {
      node = objectMapper.readValue(post, ObjectNode.class);

      Microformats2Object.Context context = parseContext(node);
      Properties properties = objectMapper.treeToValue(node.get("properties"), Properties.class);
      return new HugoMicroformats2Json(objectMapper, postTypeDiscoverer, properties, context);
    } catch (JsonProcessingException e) {
      throw new IllegalStateException("Something went wrong while processing the JSON", e);
    }
  }

  private List<String> parseList(ObjectNode parent) {
    JsonNode node = parent.get("aliases");
    return objectMapper.convertValue(node, new TypeReference<List<String>>() {});
  }

  private Microformats2Object.Context parseContext(ObjectNode node) {
    Microformats2Object.Context context = new Microformats2Object.Context();

    context.setAliases(parseList(node));
    context.setClientId(getOrNull(node, "client_id"));
    context.setDeleted(getOrDefaultFalse(node));
    context.setH(getOrNull(node, "h"));
    context.setSlug(getOrNull(node, "slug"));

    return context;
  }

  private boolean getOrDefaultFalse(JsonNode parent) {
    JsonNode node = parent.get("deleted");
    if (null == node) {
      return false;
    }

    return node.booleanValue();
  }

  private String getOrNull(JsonNode parent, String field) {
    JsonNode node = parent.get(field);
    if (null == node) {
      return null;
    }

    return node.asText();
  }
}
