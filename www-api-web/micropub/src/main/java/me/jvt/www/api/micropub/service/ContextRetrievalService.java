package me.jvt.www.api.micropub.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import java.util.*;
import me.jvt.www.api.micropub.client.PostClient;
import me.jvt.www.api.micropub.context.ContextRetriever;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.springframework.scheduling.annotation.Async;

public class ContextRetrievalService {
  private final LinkedHashSet<ContextRetriever> retrievers;
  private final PostClient client;
  private final ObjectMapper objectMapper;

  public ContextRetrievalService(
      LinkedHashSet<ContextRetriever> retrievers, PostClient client, ObjectMapper objectMapper) {
    this.retrievers = retrievers;
    this.client = client;
    this.objectMapper = objectMapper;
  }

  @Async
  public void retrieveAndPersistContext(Microformats2Object mf2) {
    var nodes = new ArrayList<JsonNode>();
    for (var retriever : retrievers) {
      nodes.addAll(retriever.retrieve(mf2));
    }

    var itemsNode = objectMapper.createArrayNode();
    for (var node : nodes) {
      ArrayNode items = node.withArray("items");
      itemsNode.addAll(items);
    }

    var container = objectMapper.createObjectNode();
    container.withArray("items").addAll(deduplicate(itemsNode));
    mf2.getContext().setContext(container);

    client.upsertCite(mf2.getContext());
  }

  private ArrayNode deduplicate(ArrayNode items) {
    var urls = new HashMap<String, List<JsonNode>>();
    for (var item : items) {
      if (!item.get("properties").has("url") || item.get("properties").get("url").isEmpty()) {
        continue;
      }
      var url = item.get("properties").get("url").get(0).textValue();
      urls.computeIfAbsent(url, k -> new ArrayList<>());
      urls.get(url).add(item);
    }

    var deduplicated = objectMapper.createArrayNode();

    for (var entry : urls.entrySet()) {
      var largest =
          entry.getValue().stream().max(Comparator.comparingInt(l -> l.get("properties").size()));
      largest.ifPresent(deduplicated::add);
    }

    return deduplicated;
  }
}
