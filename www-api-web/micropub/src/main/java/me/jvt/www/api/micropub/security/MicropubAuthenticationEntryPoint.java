package me.jvt.www.api.micropub.security;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.server.resource.web.BearerTokenAuthenticationEntryPoint;
import org.springframework.security.web.AuthenticationEntryPoint;

public class MicropubAuthenticationEntryPoint implements AuthenticationEntryPoint {

  public static final String UNAUTHORIZED_JSON_BODY =
      "{"
          + "\"error\": \"unauthorized\", "
          + "\"error_description\": \"No authentication was provided.\""
          + "}";
  private final BearerTokenAuthenticationEntryPoint delegate;

  public MicropubAuthenticationEntryPoint() {
    delegate = new BearerTokenAuthenticationEntryPoint();
  }

  MicropubAuthenticationEntryPoint(BearerTokenAuthenticationEntryPoint delegate) {
    this.delegate = delegate;
  }

  @Override
  public void commence(
      HttpServletRequest request,
      HttpServletResponse response,
      AuthenticationException authException)
      throws IOException {
    delegate.commence(request, response, authException);
    if (authException instanceof InsufficientAuthenticationException) {
      response.setContentType(MediaType.APPLICATION_JSON_VALUE);
      response.setStatus(HttpStatus.UNAUTHORIZED.value());
      response.getWriter().println(UNAUTHORIZED_JSON_BODY);
    }
  }
}
