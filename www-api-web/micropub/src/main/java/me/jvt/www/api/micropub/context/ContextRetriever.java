package me.jvt.www.api.micropub.context;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.List;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

public interface ContextRetriever {
  default List<JsonNode> retrieve(Microformats2Object mf2) {
    return List.of();
  }
}
