package me.jvt.www.api.micropub.validator.field;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = HasPhotoUrlValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface HasPhotoUrlConstraint {
  String message() default "Photo does not have URL";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
