package me.jvt.www.api.micropub.actions;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

public class DeletePropertyAction implements Action {

  protected final String property;
  protected final List<String> deletions;

  public DeletePropertyAction(String property) {
    this.property = property;
    this.deletions = Collections.emptyList();
  }

  public DeletePropertyAction(String property, List<String> deletions) {
    this.property = property;
    this.deletions = deletions;
  }

  @Override
  public void accept(Microformats2Object mf2) {
    Properties properties = mf2.getProperties();
    if (!properties.containsKey(property)) {
      return;
    }

    if (deletions.isEmpty()) {
      properties.remove(property);
    } else {
      Collection values = (Collection) mf2.getProperties().get(property);
      values.removeAll(deletions);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeletePropertyAction that = (DeletePropertyAction) o;
    return Objects.equals(property, that.property) && Objects.equals(deletions, that.deletions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(property, deletions);
  }
}
