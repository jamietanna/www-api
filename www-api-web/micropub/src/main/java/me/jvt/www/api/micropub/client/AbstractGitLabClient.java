package me.jvt.www.api.micropub.client;

import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import java.util.Collections;
import java.util.List;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.micropub.model.gitlab.CreateCommitRequestBody;
import me.jvt.www.api.micropub.model.gitlab.CreateCommitRequestBody.Action;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

public class AbstractGitLabClient {

  private static final String ALREADY_EXISTS_ERROR_MESSAGE = "A file with this name already exists";

  static {
    // required to handle the /raw call and parse it correctly
    RestAssured.defaultParser = Parser.JSON;
  }

  protected final String COMMITS_API_FORMAT_STRING;
  protected final Configuration configuration;
  protected final RequestSpecificationFactory requestSpecificationFactory;

  public AbstractGitLabClient(
      Configuration configuration, RequestSpecificationFactory requestSpecificationFactory) {
    this.configuration = configuration;
    this.requestSpecificationFactory = requestSpecificationFactory;
    this.COMMITS_API_FORMAT_STRING =
        String.format("/projects/%s/repository/commits", configuration.getProjectId());
  }

  protected boolean isConflict(Response gitLabResponse) {
    return HttpStatus.BAD_REQUEST.value() == gitLabResponse.getStatusCode()
        && ALREADY_EXISTS_ERROR_MESSAGE.equals(gitLabResponse.path("message"));
  }

  protected Response sendPost(
      CreateCommitRequestBody commitRequestBody, String content, Action action) {
    action.setContent(content + "\n");
    return sendPost(commitRequestBody, Collections.singletonList(action));
  }

  protected Response sendPost(CreateCommitRequestBody commitRequestBody, List<Action> actions) {
    commitRequestBody.setActions(actions);

    return this.requestSpecificationFactory
        .newRequestSpecification()
        .baseUri(this.configuration.getApiEndpoint())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .header("PRIVATE-TOKEN", this.configuration.getAccessToken())
        .body(commitRequestBody)
        .post(COMMITS_API_FORMAT_STRING);
  }

  public static class Configuration {

    private final String apiEndpoint;
    private final String projectId;
    private final String accessToken;

    public Configuration(String apiEndpoint, String projectId, String accessToken) {

      this.apiEndpoint = apiEndpoint;
      this.projectId = projectId;
      this.accessToken = accessToken;
    }

    public String getProjectId() {
      return projectId;
    }

    public String getAccessToken() {
      return accessToken;
    }

    public String getApiEndpoint() {
      return apiEndpoint;
    }
  }
}
