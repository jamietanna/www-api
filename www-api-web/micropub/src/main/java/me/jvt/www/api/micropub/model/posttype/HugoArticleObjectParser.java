package me.jvt.www.api.micropub.model.posttype;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object.Context;
import me.jvt.www.api.micropub.util.SingletonListHelper;

public class HugoArticleObjectParser implements Microformats2ObjectParser {

  private final YAMLMapper mapper;

  public HugoArticleObjectParser(YAMLMapper mapper) {
    this.mapper = mapper;
  }

  @Override
  public Microformats2Object deserialize(String post) {

    try (BufferedReader br = new BufferedReader(new StringReader(post)); ) {
      ObjectNode m = parseFrontMatter(br);
      String content = parseContent(br);
      Properties properties = new Properties();
      properties.setPublished(SingletonListHelper.singletonList(m.get("date").textValue()));
      properties.setName(SingletonListHelper.singletonList(m.get("title").textValue()));
      properties.setSummary(SingletonListHelper.singletonList(m.get("description").textValue()));
      JsonNode draft = m.get("draft");
      if (null == draft || !draft.booleanValue()) {
        properties.setPostStatus(SingletonListHelper.singletonList("published"));
      } else {
        properties.setPostStatus(SingletonListHelper.singletonList("draft"));
      }
      if (null != m.get("image")) {
        properties.setFeatured(SingletonListHelper.singletonList(m.get("image").textValue()));
      }
      if (null != m.get("canonical_url")) {
        properties.setRepostOf(
            SingletonListHelper.singletonList(m.get("canonical_url").textValue()));
      }
      if (null != m.get("series")) {
        properties.setSeries(SingletonListHelper.singletonList(m.get("series").textValue()));
      }
      if (null != m.get("license_code")) {
        properties.setCodeLicense(
            SingletonListHelper.singletonList(m.get("license_code").textValue()));
      }
      if (null != m.get("license_prose")) {
        properties.setProseLicense(
            SingletonListHelper.singletonList(m.get("license_prose").textValue()));
      }

      properties.setContent(content(content));

      Context context = new Context();
      context.setH("h-entry");

      context.setDeleted(null != m.get("deleted") && m.get("deleted").booleanValue());

      ArrayNode aliasProperty = m.withArray("aliases");
      if (null != aliasProperty && !aliasProperty.isEmpty()) {
        List<String> aliases = new ArrayList<>();
        aliasProperty.forEach(a -> aliases.add(a.textValue()));
        context.setAliases(aliases);
      }

      ArrayNode syndicationProperty = m.withArray("syndication");
      if (null != syndicationProperty && !syndicationProperty.isEmpty()) {
        List<String> syndication = new ArrayList<>();
        syndicationProperty.forEach(a -> syndication.add(a.textValue()));
        properties.setSyndication(syndication);
      }

      ArrayNode categoriesProperty = m.withArray("tags");
      if (null != categoriesProperty && !categoriesProperty.isEmpty()) {
        List<String> categories = new ArrayList<>();
        categoriesProperty.forEach(a -> categories.add(a.textValue()));
        properties.setCategory(categories);
      }

      if (null != m.get("slug")) {
        context.setSlug(m.get("slug").textValue());
      }

      return new HugoArticleMicroformats2Object(mapper, properties, context);
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }

  private String parseContent(BufferedReader br) {
    return br.lines().collect(Collectors.joining("\n"));
  }

  private static List<Map<String, String>> content(String value) {
    Map<String, String> content = new HashMap<>();
    content.put("value", value);

    return SingletonListHelper.singletonList(content);
  }

  /*
  detect YAML front matter https://stackoverflow.com/a/11985755/2257038
  */
  private ObjectNode parseFrontMatter(BufferedReader br) throws IOException {
    String line;
    line = br.readLine();
    while (line.isEmpty()) {
      line = br.readLine();
    }
    if (!line.matches("[-]{3,}")) {
      throw new IllegalStateException("No YAML Front Matter");
    }
    final String delimiter = line;

    // scan YAML front matter
    StringBuilder sb = new StringBuilder();
    line = br.readLine();
    while (!line.equals(delimiter)) {
      sb.append(line);
      sb.append("\n");
      line = br.readLine();
    }
    String fm = sb.toString();
    return mapper.readValue(fm, ObjectNode.class);
  }
}
