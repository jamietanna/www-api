package me.jvt.www.api.micropub.sanitiser;

import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

public interface Microformats2ObjectSanitiser {
  /**
   * Perform some sort of sanitisation on the pro:%vided {@link Microformats2Object} and then return
   * it.
   *
   * @param mf2 the given {@link Microformats2Object} to decorate
   * @return the output {@link Microformats2Object}, which may be different than <code>
   *     mf2</code>
   */
  Microformats2Object sanitise(Microformats2Object mf2);
}
