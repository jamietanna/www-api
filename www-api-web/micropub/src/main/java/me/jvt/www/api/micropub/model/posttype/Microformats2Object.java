package me.jvt.www.api.micropub.model.posttype;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.HashMap;
import java.util.List;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.validator.ValidKind;
import org.springframework.validation.annotation.Validated;

@ValidKind
@Validated
public interface Microformats2Object {

  /**
   * The {@link Kind} that this post is.
   *
   * @return the kind
   */
  Kind getKind();

  /**
   * The core properties that make up the post.
   *
   * @return the properties for the post
   */
  Properties getProperties();

  /**
   * Get extra context about the post, which is not part of the core Microformats2 representation of
   * the given post.
   *
   * @return context about the post
   */
  Context getContext();

  /**
   * For a given object, serialize it to a String, ready to be saved in the persistence tier.
   *
   * @return String representation of the post
   */
  String serialize();

  class Context extends HashMap<String, Object> {
    private static final String ALIASES = "aliases";
    private static final String CLIENT_ID = "client_id";
    private static final String CONTEXT = "context";
    private static final String DELETED = "deleted";
    private static final String SLUG = "slug";
    private static final String H = "h";

    @SuppressWarnings("unchecked")
    public List<String> getAliases() {
      return (List<String>) get(ALIASES);
    }

    public void setAliases(List<String> aliases) {
      put(ALIASES, aliases);
    }

    public String getClientId() {
      return (String) get(CLIENT_ID);
    }

    public void setClientId(String clientId) {
      put(CLIENT_ID, clientId);
    }

    public JsonNode getContext() {
      return (JsonNode) get(CONTEXT);
    }

    public void setContext(JsonNode context) {
      put(CONTEXT, context);
    }

    public boolean isDeleted() {
      return (boolean) getOrDefault(DELETED, false);
    }

    public void setDeleted(boolean isDeleted) {
      put(DELETED, isDeleted);
    }

    public String getH() {
      return (String) get(H);
    }

    public void setH(String h) {
      put(H, h);
    }

    public String getSlug() {
      return (String) get(SLUG);
    }

    public void setSlug(String slug) {
      put(SLUG, slug);
    }
  }
}
