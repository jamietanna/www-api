package me.jvt.www.api.micropub.config;

import me.jvt.www.api.micropub.client.MediaClient;
import me.jvt.www.api.micropub.client.PostClient;
import me.jvt.www.api.micropub.repository.DefaultMicropubPersistenceRepository;
import me.jvt.www.api.micropub.repository.MicropubPersistenceRepository;
import me.jvt.www.api.micropub.service.MicropubConflictService;
import me.jvt.www.api.micropub.service.PersistenceService;
import me.jvt.www.api.micropub.util.MediaTypeHelper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("default")
public class ProductionSpringConfiguration {

  @Bean
  public MediaTypeHelper mediaTypeExtensionHelper() {
    return new MediaTypeHelper();
  }

  @Bean
  public MicropubPersistenceRepository gitlabApiRepository(
      PostClient postClient,
      MediaClient mediaClient,
      MicropubConflictService micropubConflictService,
      PersistenceService persistenceService) {
    return new DefaultMicropubPersistenceRepository(
        postClient, mediaClient, micropubConflictService, persistenceService);
  }
}
