package me.jvt.www.api.micropub.validator.field;

import java.util.Map;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class HasAltTextValidator
    implements ConstraintValidator<HasAltTextConstraint, Map<String, String>> {

  @Override
  public boolean isValid(Map<String, String> value, ConstraintValidatorContext context) {
    if (null == value || value.isEmpty()) {
      return false;
    }

    if (!value.containsKey("alt")) {
      return false;
    }

    String alt = value.get("alt");
    if (null == alt) {
      return false;
    }

    return !alt.isEmpty();
  }
}
