package me.jvt.www.api.micropub.validator.property;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.hibernate.validator.internal.constraintvalidators.bv.NotBlankValidator;

public class ContentPropertyValidator
    implements ConstraintValidator<Annotation, Microformats2Object> {

  private final NotBlankValidator delegate;

  public ContentPropertyValidator(NotBlankValidator delegate) {
    this.delegate = delegate;
  }

  @Override
  public boolean isValid(Microformats2Object mf2, ConstraintValidatorContext context) {
    List<Map<String, String>> content = mf2.getProperties().getContent();
    if (null == content || content.isEmpty()) {
      return false;
    }

    Map<String, String> theContent = content.get(0);
    return delegate.isValid(theContent.get("value"), context)
        || delegate.isValid(theContent.get("html"), context);
  }
}
