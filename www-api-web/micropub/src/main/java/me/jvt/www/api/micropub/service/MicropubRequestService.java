package me.jvt.www.api.micropub.service;

import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.request.MicropubRequest;
import org.springframework.security.core.Authentication;

public interface MicropubRequestService {

  MicropubResponse handle(MicropubRequest request, Authentication authentication);

  class MicropubResponse {}

  class CreateResponse extends MicropubResponse {

    private final String slug;

    CreateResponse(String slug) {
      this.slug = slug;
    }

    /**
     * The slug for the new post.
     *
     * @return the slug for the post
     */
    public String getSlug() {
      return this.slug;
    }
  }

  class DeleteUndeleteResponse extends MicropubResponse {}

  class UpdateResponse extends MicropubResponse {

    private final Microformats2Object newObject;

    UpdateResponse(Microformats2Object newObject) {
      this.newObject = newObject;
    }

    /**
     * The new {@link Microformats2Object} that was created by the update request.
     *
     * @return the new {@link Microformats2Object}
     */
    public Microformats2Object getNewObject() {
      return this.newObject;
    }
  }
}
