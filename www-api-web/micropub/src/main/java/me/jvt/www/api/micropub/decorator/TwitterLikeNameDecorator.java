package me.jvt.www.api.micropub.decorator;

import java.net.MalformedURLException;
import java.net.URL;
import me.jvt.www.api.micropub.actions.ReplacePropertyAction;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;

public class TwitterLikeNameDecorator implements Microformats2ObjectDecorator {

  @Override
  public Microformats2Object decorate(Microformats2Object mf2) {
    if (Kind.likes != mf2.getKind()) {
      return mf2;
    }

    if (mf2.getProperties().getLikeOf().isEmpty()) {
      return mf2;
    }

    URL likeOf;
    try {
      likeOf = new URL(mf2.getProperties().getLikeOf().get(0));
    } catch (MalformedURLException e) {
      throw new InvalidMicropubMetadataRequest("The like-of URL was invalid", e);
    }

    if ("twitter.com".equals(likeOf.getHost())) {
      String path = likeOf.getPath();
      String twitterUsername = path.split("/")[1];
      ReplacePropertyAction action =
          new ReplacePropertyAction(
              "name",
              SingletonListHelper.singletonList(
                  String.format("Like of @%s's tweet", twitterUsername)));
      action.accept(mf2);
    }

    return mf2;
  }
}
