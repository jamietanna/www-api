package me.jvt.www.api.micropub.util;

public class StringSanitiser {
  public String removeWhitespace(String str) {
    if (null == str) {
      return "";
    } else {
      return str.replaceAll("^\\s+", "").replaceAll("\\s+$", "");
    }
  }
}
