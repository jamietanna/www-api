package me.jvt.www.api.micropub.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import me.jvt.www.api.micropub.model.MicropubConfigDto;
import me.jvt.www.api.micropub.model.MicropubSyndicateTo;

public class MicropubConfigurationService {

  private final String baseUrl;

  public MicropubConfigurationService(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  public MicropubConfigDto getConfiguration() {
    MicropubConfigDto config = new MicropubConfigDto();
    config.setSyndicateTo(getSyndicationConfiguration());
    config.setMediaEndpoint(String.format("%s/micropub/media", baseUrl));

    return config;
  }

  public Set<MicropubSyndicateTo> getSyndicationConfiguration() {
    return new HashSet<>(Arrays.asList(MicropubSyndicateTo.values()));
  }
}
