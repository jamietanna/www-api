package me.jvt.www.api.micropub.config.argumentresolvers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import me.jvt.www.api.micropub.converter.RequestBodyConverter;
import me.jvt.www.api.micropub.model.request.MicropubRequest;
import me.jvt.www.api.micropub.model.request.MicropubRequest.MicropubAction;
import me.jvt.www.api.micropub.model.request.MicropubRequest.MicropubAction.InvalidActionException;
import me.jvt.www.api.micropub.util.RequestDebugUtil;
import me.jvt.www.servlet.MultiReadHttpServletRequest;
import me.jvt.www.servlet.MultiReadHttpServletRequestSupplier;
import me.jvt.www.servlet.ServletInputStreamReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class MicropubRequestArgumentResolver implements HandlerMethodArgumentResolver {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(MicropubRequestArgumentResolver.class);

  private final ObjectMapper objectMapper;
  private final MultiReadHttpServletRequestSupplier supplier;
  private final ServletInputStreamReader servletInputStreamReader;
  private final RequestBodyConverter converter;
  private final RequestDebugUtil requestDebugUtil;

  public MicropubRequestArgumentResolver(
      ObjectMapper objectMapper,
      MultiReadHttpServletRequestSupplier supplier,
      ServletInputStreamReader servletInputStreamReader,
      RequestBodyConverter converter,
      RequestDebugUtil requestDebugUtil) {
    this.objectMapper = objectMapper;
    this.supplier = supplier;
    this.servletInputStreamReader = servletInputStreamReader;
    this.converter = converter;
    this.requestDebugUtil = requestDebugUtil;
  }

  @Override
  public boolean supportsParameter(MethodParameter parameter) {
    return parameter.getParameter().getType() == MicropubRequest.class;
  }

  @Override
  public Object resolveArgument(
      @NonNull MethodParameter parameter,
      ModelAndViewContainer mavContainer,
      @NonNull NativeWebRequest webRequest,
      WebDataBinderFactory binderFactory)
      throws Exception {
    HttpServletRequest unwrappedRequest = ((ServletWebRequest) webRequest).getRequest();
    MultiReadHttpServletRequest request = supplier.supply(unwrappedRequest);
    MediaType mediaType = MediaType.parseMediaType(request.getContentType());

    if (compareMediaType(MediaType.APPLICATION_FORM_URLENCODED, mediaType)
        || compareMediaType(MediaType.MULTIPART_FORM_DATA, mediaType)) {
      return resolveFromForm(request);
    } else if (compareMediaType(MediaType.APPLICATION_JSON, mediaType)) {
      return resolveFromJson(request);
    }
    throw new IllegalStateException("Content type provided was not supported");
  }

  private MicropubRequest resolveFromForm(HttpServletRequest request)
      throws InvalidActionException {
    Map<String, String[]> parameters = new HashMap<>(request.getParameterMap());
    LOGGER.debug("Received form request with body {}", requestDebugUtil.formParameters(parameters));
    MicropubAction action = MicropubAction.CREATE;
    if (parameters.containsKey("action")) {
      action = MicropubAction.fromString(parameters.get("action")[0]);
    }

    parameters.remove("access_token");
    parameters.remove("action");

    return converter.convert(action, parameters);
  }

  private MicropubRequest resolveFromJson(HttpServletRequest request)
      throws IOException, InvalidActionException {
    ObjectNode body =
        objectMapper.readValue(
            servletInputStreamReader.toString(request.getInputStream()), ObjectNode.class);
    LOGGER.debug("Received JSON with encoded body {}", requestDebugUtil.json(body));
    MicropubAction action = MicropubAction.fromString(asStringOrNull(body.get("action")));
    return converter.convert(action, body);
  }

  private String asStringOrNull(JsonNode node) {
    if (null == node) {
      return null;
    }

    return node.asText();
  }

  private boolean compareMediaType(MediaType lhs, MediaType rhs) {
    return lhs.getType().equals(rhs.getType()) && lhs.getSubtype().equals(rhs.getSubtype());
  }
}
