package me.jvt.www.api.micropub.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;

public enum Kind {
  bookmarks(
      "bookmark",
      "Bookmark",
      "h-entry",
      Arrays.asList(MicroformatsProperty.BOOKMARK_OF, MicroformatsProperty.PUBLISHED),
      Arrays.asList(
          MicroformatsProperty.NAME,
          MicroformatsProperty.CATEGORY,
          MicroformatsProperty.CONTENT,
          MicroformatsProperty.POST_STATUS,
          MicroformatsProperty.SYNDICATION)),
  likes(
      "like",
      "Like",
      "h-entry",
      Arrays.asList(MicroformatsProperty.LIKE_OF, MicroformatsProperty.PUBLISHED),
      Arrays.asList(
          MicroformatsProperty.CATEGORY,
          MicroformatsProperty.CONTENT,
          MicroformatsProperty.NAME,
          MicroformatsProperty.POST_STATUS,
          MicroformatsProperty.SYNDICATION)),
  replies(
      "reply",
      "Reply",
      "h-entry",
      Arrays.asList(
          MicroformatsProperty.CONTENT,
          MicroformatsProperty.IN_REPLY_TO,
          MicroformatsProperty.PUBLISHED),
      Arrays.asList(
          MicroformatsProperty.CATEGORY,
          MicroformatsProperty.NAME,
          MicroformatsProperty.PHOTO,
          MicroformatsProperty.POST_STATUS,
          MicroformatsProperty.SYNDICATION)),
  reads(
      "reads",
      "Reading",
      "h-entry",
      Arrays.asList(
          MicroformatsProperty.PUBLISHED,
          MicroformatsProperty.READ_OF,
          MicroformatsProperty.READ_STATUS),
      Arrays.asList(MicroformatsProperty.POST_STATUS, MicroformatsProperty.SYNDICATION)),
  reposts(
      "repost",
      "Repost",
      "h-entry",
      Arrays.asList(MicroformatsProperty.PUBLISHED, MicroformatsProperty.REPOST_OF),
      Arrays.asList(
          MicroformatsProperty.CONTENT,
          MicroformatsProperty.CATEGORY,
          MicroformatsProperty.POST_STATUS,
          MicroformatsProperty.SYNDICATION)),
  rsvps(
      "rsvp",
      "RSVP",
      "h-entry",
      Arrays.asList(
          MicroformatsProperty.IN_REPLY_TO,
          MicroformatsProperty.PUBLISHED,
          MicroformatsProperty.RSVP),
      Arrays.asList(
          MicroformatsProperty.CATEGORY,
          MicroformatsProperty.CONTENT,
          MicroformatsProperty.POST_STATUS,
          MicroformatsProperty.SYNDICATION)),
  notes(
      "note",
      "Note",
      "h-entry",
      Arrays.asList(MicroformatsProperty.CONTENT, MicroformatsProperty.PUBLISHED),
      Arrays.asList(
          MicroformatsProperty.CATEGORY,
          MicroformatsProperty.POST_STATUS,
          MicroformatsProperty.SYNDICATION)),
  photos(
      "photo",
      "Photo",
      "h-entry",
      Arrays.asList(MicroformatsProperty.PHOTO, MicroformatsProperty.PUBLISHED),
      Arrays.asList(
          MicroformatsProperty.CATEGORY,
          MicroformatsProperty.CONTENT,
          MicroformatsProperty.POST_STATUS,
          MicroformatsProperty.SYNDICATION)),
  steps(
      "step",
      "Step Counts",
      "h-measure",
      Arrays.asList(
          MicroformatsProperty.UNIT,
          MicroformatsProperty.NUM,
          MicroformatsProperty.PUBLISHED,
          MicroformatsProperty.START,
          MicroformatsProperty.END),
      Collections.singletonList(MicroformatsProperty.POST_STATUS)),
  events(
      "event",
      "Event",
      "h-event",
      Arrays.asList(
          MicroformatsProperty.END,
          MicroformatsProperty.NAME,
          MicroformatsProperty.PUBLISHED,
          MicroformatsProperty.START),
      Arrays.asList(MicroformatsProperty.CONTENT, MicroformatsProperty.URL)),
  contacts(
      "contact",
      "Contact",
      "h-card",
      Arrays.asList(
          MicroformatsProperty.NAME, MicroformatsProperty.NICKNAME, MicroformatsProperty.URL),
      Arrays.asList(MicroformatsProperty.REL_TWITTER)),
  listens(
      "listen",
      "Listen",
      "h-entry",
      Arrays.asList(MicroformatsProperty.LISTEN_OF, MicroformatsProperty.PUBLISHED),
      Arrays.asList(
          MicroformatsProperty.NAME,
          MicroformatsProperty.CATEGORY,
          MicroformatsProperty.CONTENT,
          MicroformatsProperty.POST_STATUS,
          MicroformatsProperty.SYNDICATION)),
  articles(
      "article",
      "Article",
      "h-entry",
      Arrays.asList(
          MicroformatsProperty.CONTENT,
          MicroformatsProperty.NAME,
          MicroformatsProperty.PUBLISHED,
          MicroformatsProperty.SUMMARY),
      Arrays.asList(
          MicroformatsProperty.CATEGORY,
          MicroformatsProperty.POST_STATUS,
          MicroformatsProperty.CODE_LICENSE,
          MicroformatsProperty.PROSE_LICENSE,
          MicroformatsProperty.SERIES,
          MicroformatsProperty.FEATURED,
          MicroformatsProperty.SYNDICATION));

  private final String type;
  private final String name;
  private final String h;
  private final List<MicroformatsProperty> requiredProperties;
  private final List<MicroformatsProperty> optionalProperties;

  Kind(
      String type,
      String name,
      String h,
      List<MicroformatsProperty> requiredProperties,
      List<MicroformatsProperty> optionalProperties) {
    this.type = type;
    this.name = name;
    this.h = h;
    this.requiredProperties = requiredProperties;
    this.optionalProperties = optionalProperties;
  }

  public String getH() {
    return h;
  }

  public List<MicroformatsProperty> getRequiredProperties() {
    return requiredProperties;
  }

  public List<MicroformatsProperty> getOptionalProperties() {
    return optionalProperties;
  }

  public String getType() {
    return type;
  }

  public String getName() {
    return name;
  }
}
