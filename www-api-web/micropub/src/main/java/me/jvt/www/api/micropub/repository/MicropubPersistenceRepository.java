package me.jvt.www.api.micropub.repository;

import java.util.Optional;
import javax.validation.Valid;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.multipart.MultipartFile;

@Validated
public interface MicropubPersistenceRepository {

  String save(MultipartFile file, MediaType mediaType);

  String save(@Valid Microformats2Object content);

  Optional<Microformats2Object> findById(String s);
}
