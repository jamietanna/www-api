package me.jvt.www.api.micropub.client;

import io.restassured.response.Response;
import me.jvt.www.api.core.RequestSpecificationFactory;
import org.springframework.http.HttpStatus;

/** Proxy a URL from www.jvt.me, and return it as a given type */
public class WwwProxyClient<T> {

  private final Class<T> responseType;
  private final RequestSpecificationFactory factory;
  private final String url;

  public WwwProxyClient(Class<T> responseType, RequestSpecificationFactory factory, String url) {
    this.responseType = responseType;
    this.factory = factory;
    this.url = String.format("https://www.jvt.me%s", url);
  }

  public T retrieve() {
    Response response = factory.newRequestSpecification().get(url);
    if (HttpStatus.OK.value() != response.getStatusCode()) {
      throw new RuntimeException(
          String.format("Call to %s failed with HTTP Status %d", url, response.getStatusCode()));
    }
    return response.as(responseType);
  }
}
