package me.jvt.www.api.micropub.validator.property;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.function.Predicate;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.hibernate.validator.internal.constraintvalidators.bv.NotBlankValidator;

public class NotEmptyPropertyValidator
    implements ConstraintValidator<Annotation, Microformats2Object> {

  private final String property;
  private final NotBlankValidator notBlankValidator;

  public NotEmptyPropertyValidator(String property, NotBlankValidator notBlankValidator) {
    this.property = property;
    this.notBlankValidator = notBlankValidator;
  }

  @Override
  public boolean isValid(Microformats2Object mf2, ConstraintValidatorContext context) {
    List<String> propertyValue = (List<String>) mf2.getProperties().get(property);

    if (null == propertyValue || propertyValue.isEmpty()) {
      return report(false, context);
    }

    return report(propertyValue.stream().allMatch(performValidation(context)), context);
  }

  private Predicate<String> performValidation(ConstraintValidatorContext context) {
    return v -> {
      boolean isValid = new ReportingDelegatingValidator<>(notBlankValidator).isValid(v, context);
      report(isValid, context);
      return isValid;
    };
  }

  private boolean report(boolean isValid, ConstraintValidatorContext context) {
    if (!isValid) {
      context
          .buildConstraintViolationWithTemplate(String.format("Property `%s` was empty", property))
          .addConstraintViolation();
    }

    return isValid;
  }
}
