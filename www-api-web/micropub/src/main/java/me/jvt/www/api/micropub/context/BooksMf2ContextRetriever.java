package me.jvt.www.api.micropub.context;

import com.fasterxml.jackson.databind.JsonNode;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import me.jvt.www.api.micropub.client.Microformats2PipeClient;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.springframework.stereotype.Component;

@Component
public class BooksMf2ContextRetriever implements ContextRetriever {
  private final Microformats2PipeClient client;

  public BooksMf2ContextRetriever(Microformats2PipeClient client) {
    this.client = client;
  }

  @Override
  public List<JsonNode> retrieve(Microformats2Object mf2) {
    if (mf2.getProperties().getReadOf() != null && !mf2.getProperties().getReadOf().isEmpty()) {
      var genericRead = mf2.getProperties().getReadOf().get(0);
      if (genericRead instanceof String read) {
        URL parsed;
        try {
          parsed = new URL(read);
        } catch (MalformedURLException ignored) {
          return List.of();
        }
        if (parsed.getHost().equals("books-mf2.herokuapp.com")) {
          URL pipeUrl;
          try {
            pipeUrl = new URL("https", "books-mf2.herokuapp.com", parsed.getPath());
          } catch (MalformedURLException e) {
            throw new IllegalStateException(e);
          }
          var maybeContext = client.retrieve(pipeUrl.toString());
          if (maybeContext.isPresent()) {
            return List.of(maybeContext.get());
          }
        }
      }
    }
    return List.of();
  }
}
