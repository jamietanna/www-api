package me.jvt.www.api.micropub.exception;

public class InvalidMediaException extends IllegalArgumentException {

  public InvalidMediaException(String message) {
    super(message);
  }
}
