package me.jvt.www.api.micropub.client;

import java.util.List;
import java.util.Map.Entry;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.micropub.model.ContentDeduplicationResult;
import me.jvt.www.api.micropub.model.ContentDeduplicationResult.ContentDeduplicationEntry;

public class ContentDeduplicationClient {

  private final RequestSpecificationFactory requestSpecificationFactory;

  public ContentDeduplicationClient(RequestSpecificationFactory requestSpecificationFactory) {
    this.requestSpecificationFactory = requestSpecificationFactory;
  }

  public ContentDeduplicationResult retrieveContentDeduplicationData() {
    ContentDeduplicationResult result =
        requestSpecificationFactory
            .newRequestSpecification()
            .get("https://www.jvt.me/content-deduplication.json")
            .as(ContentDeduplicationResult.class);
    for (Entry<String, List<ContentDeduplicationEntry>> entries : result.entrySet()) {
      for (ContentDeduplicationEntry entry : entries.getValue()) {
        String slug = entry.getSlug();
        slug = slug.replaceAll("https://www.jvt.me/mf2/", "");
        slug = slug.replaceAll("/$", "");

        entry.setSlug(slug);
      }
    }

    return result;
  }
}
