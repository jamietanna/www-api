package me.jvt.www.api.micropub.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.StdKeySerializers.StringKeySerializer;
import com.google.common.base.CaseFormat;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.jvt.www.api.micropub.model.Properties.KebabCaseKeySerialiser;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;

@JsonSerialize(keyUsing = KebabCaseKeySerialiser.class)
public class Properties extends HashMap<String, Object> {

  public List<String> getName() {
    return (List<String>) get(MicroformatsProperty.NAME);
  }

  public void setName(List<String> name) {
    put(MicroformatsProperty.NAME, name);
  }

  public List<String> getNickname() {
    return (List<String>) get(MicroformatsProperty.NICKNAME);
  }

  public void setNickname(List<String> nickname) {
    put(MicroformatsProperty.NICKNAME, nickname);
  }

  public List<Map<String, String>> getPhoto() {
    return (List<Map<String, String>>) get(MicroformatsProperty.PHOTO);
  }

  public void setPhoto(List<Map<String, String>> photo) {
    put(MicroformatsProperty.PHOTO, photo);
  }

  public List<String> getLikeOf() {
    return (List<String>) get(MicroformatsProperty.LIKE_OF);
  }

  public void setLikeOf(List<String> likeOf) {
    put(MicroformatsProperty.LIKE_OF, likeOf);
  }

  public List<String> getBookmarkOf() {
    return (List<String>) get(MicroformatsProperty.BOOKMARK_OF);
  }

  public void setBookmarkOf(List<String> bookmarkOf) {
    put(MicroformatsProperty.BOOKMARK_OF, bookmarkOf);
  }

  public List<String> getInReplyTo() {
    return (List<String>) get(MicroformatsProperty.IN_REPLY_TO);
  }

  public void setInReplyTo(List<String> inReplyTo) {
    put(MicroformatsProperty.IN_REPLY_TO, inReplyTo);
  }

  public List<String> getPublished() {
    return (List<String>) get(MicroformatsProperty.PUBLISHED);
  }

  public void setPublished(List<String> published) {
    put(MicroformatsProperty.PUBLISHED, published);
  }

  public List<String> getRepostOf() {
    return (List<String>) get(MicroformatsProperty.REPOST_OF);
  }

  public void setRepostOf(List<String> repostOf) {
    put(MicroformatsProperty.REPOST_OF, repostOf);
  }

  public List<String> getCategory() {
    return (List<String>) get(MicroformatsProperty.CATEGORY);
  }

  public void setCategory(List<String> category) {
    put(MicroformatsProperty.CATEGORY, category);
  }

  public List<Rsvp> getRsvp() {
    return (List<Rsvp>) get(MicroformatsProperty.RSVP);
  }

  public void setRsvp(List<Rsvp> rsvp) {
    put(MicroformatsProperty.RSVP, rsvp);
  }

  public List<Map<String, String>> getContent() {
    return (List<Map<String, String>>) get(MicroformatsProperty.CONTENT);
  }

  public void setContent(List<Map<String, String>> content) {
    put(MicroformatsProperty.CONTENT, content);
  }

  public List<String> getSyndication() {
    return (List<String>) get(MicroformatsProperty.SYNDICATION);
  }

  public void setSyndication(List<String> syndication) {
    put(MicroformatsProperty.SYNDICATION, syndication);
  }

  public List<String> getSummary() {
    return (List<String>) get(MicroformatsProperty.SUMMARY);
  }

  public void setSummary(List<String> summary) {
    put(MicroformatsProperty.SUMMARY, summary);
  }

  public List<String> getStart() {
    return (List<String>) get(MicroformatsProperty.START);
  }

  public void setStart(List<String> start) {
    put(MicroformatsProperty.START, start);
  }

  public List<String> getEnd() {
    return (List<String>) get(MicroformatsProperty.END);
  }

  public void setEnd(List<String> end) {
    put(MicroformatsProperty.END, end);
  }

  public List<String> getNum() {
    return (List<String>) get(MicroformatsProperty.NUM);
  }

  public void setNum(List<String> num) {
    put(MicroformatsProperty.NUM, num);
  }

  public List<String> getUnit() {
    return (List<String>) get(MicroformatsProperty.UNIT);
  }

  public void setUnit(List<String> unit) {
    put(MicroformatsProperty.UNIT, unit);
  }

  public List<String> getUrl() {
    return (List<String>) get(MicroformatsProperty.URL);
  }

  public void setUrl(List<String> url) {
    put(MicroformatsProperty.URL, url);
  }

  public List<Object> getReadOf() {
    return (List<Object>) get(MicroformatsProperty.READ_OF);
  }

  public void setReadOf(List<Object> readOf) {
    put(MicroformatsProperty.READ_OF, readOf);
  }

  public List<ReadStatus> getReadStatus() {
    return (List<ReadStatus>) get(MicroformatsProperty.READ_STATUS);
  }

  public void setReadStatus(List<ReadStatus> url) {
    put(MicroformatsProperty.READ_STATUS.getName(), url);
  }

  public List<String> getPostStatus() {
    return (List<String>) get(MicroformatsProperty.POST_STATUS);
  }

  public void setPostStatus(List<String> postStatus) {
    put(MicroformatsProperty.POST_STATUS, postStatus);
  }

  public List<String> getCodeLicense() {
    return (List<String>) get(MicroformatsProperty.CODE_LICENSE);
  }

  public void setCodeLicense(List<String> codeLicense) {
    put(MicroformatsProperty.CODE_LICENSE, codeLicense);
  }

  public List<String> getProseLicense() {
    return (List<String>) get(MicroformatsProperty.PROSE_LICENSE);
  }

  public void setProseLicense(List<String> proseLicense) {
    put(MicroformatsProperty.PROSE_LICENSE, proseLicense);
  }

  public List<String> getSeries() {
    return (List<String>) get(MicroformatsProperty.SERIES);
  }

  public void setSeries(List<String> series) {
    put(MicroformatsProperty.SERIES, series);
  }

  public List<String> getFeatured() {
    return (List<String>) get(MicroformatsProperty.FEATURED);
  }

  public void setFeatured(List<String> featured) {
    put(MicroformatsProperty.FEATURED, featured);
  }

  public List<String> getListenOf() {
    return (List<String>) get(MicroformatsProperty.LISTEN_OF);
  }

  public void setListenOf(List<String> listenOf) {
    put(MicroformatsProperty.LISTEN_OF, listenOf);
  }

  private Object get(MicroformatsProperty property) {
    return get(property.getName());
  }

  private void put(MicroformatsProperty property, Object value) {
    put(property.getName(), value);
  }

  public enum PostStatus {
    draft,
    published;
  }

  public enum ReadStatus {
    toRead("to-read"),
    reading("reading"),
    finished;

    private String property;

    ReadStatus() {}

    ReadStatus(String property) {
      this.property = property;
    }

    public String getProperty() {
      if (null == property) {
        return name();
      }

      return property;
    }
  }

  public enum Rsvp {
    yes,
    no,
    maybe,
    interested
  }

  static class KebabCaseKeySerialiser extends StringKeySerializer {

    @Override
    public void serialize(Object value, JsonGenerator g, SerializerProvider provider)
        throws IOException {
      g.writeFieldName(CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_HYPHEN, (String) value));
    }
  }
}
