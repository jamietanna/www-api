package me.jvt.www.api.micropub.converter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.jvt.www.api.micropub.actions.Action;
import me.jvt.www.api.micropub.actions.ActionParser;
import me.jvt.www.api.micropub.actions.DeletePostAction;
import me.jvt.www.api.micropub.actions.UndeletePostAction;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object.Context;
import me.jvt.www.api.micropub.model.request.MicropubRequest;
import me.jvt.www.api.micropub.model.request.MicropubRequest.MicropubAction;
import me.jvt.www.api.micropub.model.request.MicropubRequest.RequestContext;
import me.jvt.www.api.micropub.util.SingletonListHelper;

public class RequestBodyConverter {

  private final ObjectMapper objectMapper;
  private final ActionParser actionParser;

  public RequestBodyConverter(ObjectMapper objectMapper, ActionParser actionParser) {
    this.objectMapper = objectMapper;
    this.actionParser = actionParser;
  }

  public MicropubRequest convert(MicropubAction action, JsonNode requestBody) {
    RequestContext requestContext = new RequestContext();

    List<Action> actions = null;
    if (MicropubAction.UPDATE.equals(action)) {
      actions = resolveActions(requestBody);
    }

    JsonNode propertiesNode = requestBody.get("properties");

    Context context = new Context();
    Properties properties =
        objectMapper.convertValue(propertiesNode, new TypeReference<Properties>() {});

    if (MicropubAction.CREATE.equals(action)) {
      String h = requestBody.get("type").get(0).textValue();
      context.setH(h);

      handleContentNode(propertiesNode, properties);
      handlePhotoNode(propertiesNode, properties);
    }

    if (MicropubAction.DELETE.equals(action)) {
      actions = Collections.singletonList(new DeletePostAction());
    }

    if (MicropubAction.UNDELETE.equals(action)) {
      actions = Collections.singletonList(new UndeletePostAction());
    }

    if (hasActions(action)) {
      setUrl(requestBody, requestContext);
    }

    requestContext.setContext(context);
    requestContext.setProperties(properties);

    return new MicropubRequest(action, requestContext, actions);
  }

  public MicropubRequest convert(MicropubAction action, Map<String, String[]> requestBody) {
    RequestContext requestContext = new RequestContext();
    Context context = new Context();
    Properties properties = new Properties();

    List<Action> actions = null;

    if (MicropubAction.DELETE.equals(action)) {
      actions = Collections.singletonList(new DeletePostAction());
    }

    if (MicropubAction.UNDELETE.equals(action)) {
      actions = Collections.singletonList(new UndeletePostAction());
    }

    if (action.equals(MicropubAction.DELETE) || action.equals(MicropubAction.UNDELETE)) {
      if (!requestBody.containsKey("url")) {
        throw new InvalidMicropubMetadataRequest("URL parameter required");
      }
      requestContext.put("url", requestBody.get("url")[0]);
    }

    for (Map.Entry<String, String[]> entry : requestBody.entrySet()) {
      String parameter = entry.getKey().replace("[]", "");
      if (parameter.equals("access_token")
          || parameter.equals("photo")
          || parameter.equals("mp-photo-alt")) {
        continue;
      }

      if (entry.getValue()[0].isEmpty()) {
        continue;
      }

      if (!parameter.startsWith("mp-")) {
        switch (parameter) {
          case "content":
            {
              Map<String, String> content = new HashMap<>();
              content.put("value", entry.getValue()[0]);
              properties.put("content", SingletonListHelper.singletonList(content));
              break;
            }
          case "content[html]":
            {
              Map<String, String> content = new HashMap<>();
              content.put("html", entry.getValue()[0]);
              properties.put("content", SingletonListHelper.singletonList(content));
              break;
            }
          case "h":
            context.setH(String.format("h-%s", entry.getValue()[0]));
            break;
          default:
            properties.put(parameter, Arrays.asList(entry.getValue()));
            break;
        }
      } else {
        if ("mp-syndicate-to".equals(parameter)) {
          properties.setSyndication(Arrays.asList(entry.getValue()));
        }
        if ("mp-slug".equals(parameter)) {
          context.setSlug(entry.getValue()[entry.getValue().length - 1]);
        }
      }
    }

    List<Map<String, String>> photo = handlePhoto(requestBody);
    if (!photo.isEmpty()) {
      properties.put("photo", photo);
    }

    requestContext.setContext(context);
    requestContext.setProperties(properties);

    return new MicropubRequest(action, requestContext, actions);
  }

  private String asStringOrNull(JsonNode node) {
    if (null == node) {
      return null;
    }

    return node.asText();
  }

  private boolean hasActions(MicropubAction action) {
    return MicropubAction.DELETE.equals(action)
        || MicropubAction.UNDELETE.equals(action)
        || MicropubAction.UPDATE.equals(action);
  }

  private void handleContentNode(JsonNode propertiesNode, Properties properties) {
    if (!propertiesNode.has("content")) {
      return;
    }

    ArrayNode contentNode = (ArrayNode) propertiesNode.get("content");
    Map<String, String> content = new HashMap<>();
    if (contentNode.get(0).isObject()) {
      content.put("value", contentNode.get(0).get("html").textValue());
    } else {
      content.put("value", contentNode.get(0).textValue());
    }
    properties.put("content", SingletonListHelper.singletonList(content));
  }

  private List<Map<String, String>> handlePhoto(Map<String, String[]> requestBody) {
    String[] photo = get(requestBody, "photo");
    String[] alt = get(requestBody, "mp-photo-alt");

    if (null == photo) {
      return Collections.emptyList();
    }

    List<Map<String, String>> photos = new ArrayList<>();

    for (int i = 0; i < photo.length; i++) {
      Map<String, String> thisPhoto = new HashMap<>();

      if (null == photo[i] || photo[i].isEmpty()) {
        continue;
      }

      thisPhoto.put("photo", photo[i]);

      if (null != alt) {
        thisPhoto.put("alt", alt[i]);
      }

      photos.add(thisPhoto);
    }

    return photos;
  }

  private void handlePhotoNode(JsonNode propertiesNode, Properties properties) {
    if (!propertiesNode.has("photo")) {
      return;
    }

    ArrayNode photoNode = (ArrayNode) propertiesNode.get("photo");
    Map<String, String> photo = new HashMap<>();
    if (photoNode.get(0).isObject()) {
      photo.put("photo", photoNode.get(0).get("value").textValue());
      photo.put("alt", photoNode.get(0).get("alt").textValue());
    } else {
      photo.put("photo", photoNode.get(0).textValue());
    }
    properties.put("photo", SingletonListHelper.singletonList(photo));
  }

  private List<Action> resolveActions(JsonNode node) {
    Map<String, Object> map =
        objectMapper.convertValue(node, new TypeReference<Map<String, Object>>() {});
    return actionParser.parse(map);
  }

  private void setUrl(JsonNode node, RequestContext requestContext) {
    String url = asStringOrNull(node.get("url"));
    if (null == url) {
      throw new InvalidMicropubMetadataRequest("URL parameter required");
    }
    requestContext.put("url", url);
  }

  private String[] get(Map<String, String[]> map, String key) {
    if (null != map.get(key)) {
      return map.get(key);
    }
    if (null != map.get(key + "[]")) {
      return map.get(key + "[]");
    }

    return null;
  }
}
