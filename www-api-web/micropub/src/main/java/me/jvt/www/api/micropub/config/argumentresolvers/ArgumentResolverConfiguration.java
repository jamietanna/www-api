package me.jvt.www.api.micropub.config.argumentresolvers;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import me.jvt.www.api.micropub.converter.RequestBodyConverter;
import me.jvt.www.api.micropub.util.RequestDebugUtil;
import me.jvt.www.servlet.MultiReadHttpServletRequestSupplier;
import me.jvt.www.servlet.ServletInputStreamReader;
import org.springframework.stereotype.Component;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class ArgumentResolverConfiguration implements WebMvcConfigurer {

  private final ObjectMapper objectMapper;
  private final MultiReadHttpServletRequestSupplier supplier;
  private final ServletInputStreamReader servletInputStreamReader;
  private final RequestBodyConverter requestBodyConverter;
  private final RequestDebugUtil requestDebugUtil;

  public ArgumentResolverConfiguration(
      ObjectMapper objectMapper,
      MultiReadHttpServletRequestSupplier supplier,
      ServletInputStreamReader servletInputStreamReader,
      RequestBodyConverter requestBodyConverter,
      RequestDebugUtil requestDebugUtil) {
    this.objectMapper = objectMapper;
    this.supplier = supplier;
    this.servletInputStreamReader = servletInputStreamReader;
    this.requestBodyConverter = requestBodyConverter;
    this.requestDebugUtil = requestDebugUtil;
  }

  @Override
  public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
    resolvers.add(
        new MicropubRequestArgumentResolver(
            objectMapper,
            supplier,
            servletInputStreamReader,
            requestBodyConverter,
            requestDebugUtil));
  }
}
