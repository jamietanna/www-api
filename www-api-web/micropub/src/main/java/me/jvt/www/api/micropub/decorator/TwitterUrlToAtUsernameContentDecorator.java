package me.jvt.www.api.micropub.decorator;

import java.util.Map;
import me.jvt.www.api.micropub.actions.concrete.ReplaceContentPropertyAction;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;

public class TwitterUrlToAtUsernameContentDecorator implements Microformats2ObjectDecorator {

  @Override
  public Microformats2Object decorate(Microformats2Object mf2) {
    if (Kind.notes == mf2.getKind()
        || Kind.photos == mf2.getKind()
        || Kind.reposts == mf2.getKind()
        || Kind.replies == mf2.getKind()) {
      if (null != mf2.getProperties().getContent()
          && 0 != mf2.getProperties().getContent().size()) {
        Map<String, String> content = mf2.getProperties().getContent().get(0);
        String replacement =
            (content.get("value"))
                .replaceAll(
                    "(https://twitter.com/(?![a-zA-Z0-9_]+/)([a-zA-Z0-9_]+))",
                    "<span class=\"h-card\"><a class=\"u-url\" href=\"$1\">@$2</a></span>");

        ReplaceContentPropertyAction action =
            new ReplaceContentPropertyAction(SingletonListHelper.singletonList(replacement));
        action.accept(mf2);
      }
    }

    return mf2;
  }
}
