package me.jvt.www.api.micropub.validator.property;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

public class ReadOfPropertyValidator
    implements ConstraintValidator<Annotation, Microformats2Object> {

  @Override
  public boolean isValid(Microformats2Object mf2, ConstraintValidatorContext context) {
    List<?> readOf = mf2.getProperties().getReadOf();

    if (null == readOf || readOf.isEmpty()) {
      return false;
    }

    if (null == readOf.get(0)) {
      return false;
    }

    if (readOf.get(0) instanceof String) {
      return !((String) readOf.get(0)).isEmpty();
    } else if (readOf.get(0) instanceof Map) {
      Map<String, Object> hcite = (Map<String, Object>) readOf.get(0);
      if (!isValidType(hcite.get("type"))) {
        return false;
      }
      Map<String, Object> properties = (Map<String, Object>) hcite.get("properties");
      if (null == properties) {
        return false;
      }

      if (isEmpty(properties.get("name"))) {
        return false;
      }
      if (isEmpty(properties.get("author"))) {
        return false;
      }

      if (!isEmpty(properties.get("uid"))) {
        return true;
      }

      return false;
    }

    throw new InvalidMicropubMetadataRequest(
        "The read-of property was not populated with a valid value");
  }

  private boolean isValidType(Object type) {
    if (null == type) {
      return false;
    }
    List<String> theType = (List<String>) type;

    return !isEmpty(theType) && "h-cite".equals(theType.get(0));
  }

  private boolean isEmpty(Object o) {
    return isEmpty((List<String>) o);
  }

  private boolean isEmpty(List<String> list) {
    if (null == list) {
      return true;
    }

    if (list.isEmpty()) {
      return true;
    }

    if (null == list.get(0) || list.get(0).isEmpty()) {
      return true;
    }

    return list.get(0).isEmpty();
  }
}
