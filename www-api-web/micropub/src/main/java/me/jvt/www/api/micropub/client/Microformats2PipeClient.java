package me.jvt.www.api.micropub.client;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.Optional;
import java.util.function.Function;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

public class Microformats2PipeClient {
  private final WebClient webClient;

  public Microformats2PipeClient(WebClient webClient) {
    this.webClient = webClient;
  }

  public Optional<JsonNode> retrieve(String path) {
    var node =
        webClient
            .get()
            .uri(path)
            .accept(MediaType.valueOf("application/mf2+json"))
            .retrieve()
            .bodyToMono(JsonNode.class)
            .onErrorResume(WebClientResponseException.class, ignoreErrorStatuses())
            .block();

    if (node == null) {
      return Optional.empty();
    }
    var items = node.withArray("items");
    if (items == null || items.isEmpty()) {
      return Optional.empty();
    }
    return Optional.of(node);
  }

  private static Function<WebClientResponseException, Mono<? extends JsonNode>>
      ignoreErrorStatuses() {
    return ex -> {
      if (ex.getStatusCode().isError()) {
        return Mono.empty();
      }
      return Mono.error(ex);
    };
  }
}
