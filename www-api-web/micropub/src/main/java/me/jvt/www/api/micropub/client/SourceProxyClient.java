package me.jvt.www.api.micropub.client;

import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.micropub.model.MicropubSourceContainerDto;

public class SourceProxyClient extends WwwProxyClient<MicropubSourceContainerDto> {

  public SourceProxyClient(RequestSpecificationFactory requestSpecificationFactory) {
    super(MicropubSourceContainerDto.class, requestSpecificationFactory, "/source.json");
  }
}
