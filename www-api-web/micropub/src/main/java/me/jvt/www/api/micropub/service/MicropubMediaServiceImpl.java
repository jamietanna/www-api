package me.jvt.www.api.micropub.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URLConnection;
import me.jvt.www.api.micropub.exception.InvalidMediaException;
import me.jvt.www.api.micropub.repository.MicropubPersistenceRepository;
import me.jvt.www.api.micropub.util.MediaTypeHelper;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

public class MicropubMediaServiceImpl implements MicropubMediaService {

  private final String baseUrl;
  private final MicropubPersistenceRepository micropubPersistenceRepository;
  private final MediaTypeHelper mediaTypeHelper;

  public MicropubMediaServiceImpl(
      String baseUrl,
      MicropubPersistenceRepository micropubPersistenceRepository,
      MediaTypeHelper mediaTypeHelper) {
    this.baseUrl = baseUrl;
    this.micropubPersistenceRepository = micropubPersistenceRepository;
    this.mediaTypeHelper = mediaTypeHelper;
  }

  @Override
  public String validateAndSave(MultipartFile file) throws InvalidMediaException {
    String mediaType = file.getContentType();
    if (null == mediaType || mediaType.equals(MediaType.APPLICATION_OCTET_STREAM_VALUE)) {
      mediaType = URLConnection.guessContentTypeFromName(file.getOriginalFilename());
    }
    if (null == mediaType) {
      try {
        mediaType =
            URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(file.getBytes()));
      } catch (IOException e) {
        // ignore
      }
    }
    if (null == mediaType || !mediaTypeHelper.validate(MediaType.valueOf(mediaType))) {
      throw new InvalidMediaException("Invalid media type provided");
    }

    return String.format(
        "%s/%s", baseUrl, micropubPersistenceRepository.save(file, MediaType.valueOf(mediaType)));
  }
}
