package me.jvt.www.api.micropub.model.request;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import me.jvt.www.api.micropub.actions.Action;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object.Context;

public class MicropubRequest {

  private MicropubAction action;
  private RequestContext context;
  private List<Action> actions;

  public MicropubRequest() {}

  public MicropubRequest(MicropubAction action, RequestContext context) {
    this.action = action;
    this.context = context;
    this.actions = Collections.emptyList();
  }

  public MicropubRequest(MicropubAction action, RequestContext context, List<Action> actions) {
    this.action = action;
    this.context = context;
    if (null == actions) {
      this.actions = Collections.emptyList();
    } else {
      this.actions = actions;
    }
  }

  public MicropubAction getAction() {
    return action;
  }

  public RequestContext getContext() {
    return context;
  }

  public List<Action> getActions() {
    return actions;
  }

  public enum MicropubAction {
    CREATE,
    UPDATE,
    DELETE,
    UNDELETE;

    public static MicropubAction fromString(String action) throws InvalidActionException {
      if (null == action) {
        return MicropubAction.CREATE;
      }

      switch (action) {
        case "create":
          return MicropubAction.CREATE;
        case "update":
          return MicropubAction.UPDATE;
        case "delete":
          return MicropubAction.DELETE;
        case "undelete":
          return MicropubAction.UNDELETE;
      }

      throw new InvalidActionException("Action provided is not understood by this server");
    }

    public static class InvalidActionException extends Exception {
      InvalidActionException(String message) {
        super(message);
      }
    }
  }

  public static class RequestContext extends HashMap<String, Object> {
    public void setContext(Context context) {
      put("mf2Context", context);
    }

    public Context getContext() {
      return (Context) get("mf2Context");
    }

    public Properties getProperties() {
      return (Properties) get("mf2Properties");
    }

    public void setProperties(Properties properties) {
      put("mf2Properties", properties);
    }

    public void setUrl(String url) {
      put("url", url);
    }

    public String getUrl() {
      return (String) get("url");
    }
  }
}
