package me.jvt.www.api.micropub.service.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import me.jvt.www.api.micropub.model.MicropubQueryDto;
import me.jvt.www.api.micropub.model.QueryPropertiesDto;
import me.jvt.www.api.micropub.model.QueryPropertiesDto.Property;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

@Component
public class QueryPropertiesHandler implements QueryHandler {

  @Override
  public String getQuery() {
    return "properties";
  }

  @Override
  public MicropubQueryDto handle(
      MultiValueMap<String, String> parameters, Optional<Authentication> maybeAuthentication) {
    QueryPropertiesDto dto = new QueryPropertiesDto();
    List<Property> properties = new ArrayList<>();
    for (MicroformatsProperty property : MicroformatsProperty.values()) {
      properties.add(new QueryPropertiesDto.Property(property));
    }
    dto.setProperties(properties);
    return dto;
  }
}
