package me.jvt.www.api.micropub.exception;

import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import me.jvt.www.api.micropub.model.MicropubErrorResponse;
import me.jvt.www.api.micropub.model.posttype.KindNotSupportedException;
import me.jvt.www.api.micropub.model.request.MicropubRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class MicropubExceptionHandler extends ResponseEntityExceptionHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(MicropubExceptionHandler.class);

  @ExceptionHandler(ConstraintViolationException.class)
  public final ResponseEntity<MicropubErrorResponse> constraintViolationException(
      ConstraintViolationException constraintViolationException) {
    Set<String> messages =
        constraintViolationException.getConstraintViolations().stream()
            .map(ConstraintViolation::getMessage)
            .collect(Collectors.toCollection(TreeSet::new));
    return handleInvalidRequest(
        String.format("Validation failed on the request: %s", String.join(" | ", messages)));
  }

  @ExceptionHandler({
    KindNotSupportedException.class,
    InvalidMediaException.class,
    InvalidMicropubMetadataRequest.class,
    MicropubRequest.MicropubAction.InvalidActionException.class
  })
  public final ResponseEntity<MicropubErrorResponse> invalidRequests(Exception e) {
    return handleInvalidRequest(e.getMessage());
  }

  private ResponseEntity<MicropubErrorResponse> handleInvalidRequest(String message) {
    MicropubErrorResponse errorResponse = new MicropubErrorResponse();
    errorResponse.setError("invalid_request");
    errorResponse.setErrorDescription(message);

    LOGGER.warn("An invalid request was rejected for reason: {}", message);
    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }
}
