package me.jvt.www.api.micropub.config;

import me.jvt.www.api.micropub.security.MicropubAuthenticationEntryPoint;
import me.jvt.www.api.micropub.security.MicropubBearerTokenAccessDeniedHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.security.oauth2.server.resource.web.BearerTokenResolver;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  private final MicropubAuthenticationEntryPoint authenticationEntryPoint =
      new MicropubAuthenticationEntryPoint();
  private final OpaqueTokenIntrospector tokenIntrospector;
  private final BearerTokenResolver bearerTokenResolver;
  private final MicropubBearerTokenAccessDeniedHandler accessDeniedHandler;

  public SecurityConfiguration(
      OpaqueTokenIntrospector tokenIntrospector,
      BearerTokenResolver bearerTokenResolver,
      MicropubBearerTokenAccessDeniedHandler accessDeniedHandler) {
    this.tokenIntrospector = tokenIntrospector;
    this.bearerTokenResolver = bearerTokenResolver;
    this.accessDeniedHandler = accessDeniedHandler;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    // @formatter:off
    http.httpBasic()
        .disable()
        .csrf()
        .disable()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .authorizeRequests()
        .antMatchers(HttpMethod.GET, "/micropub")
        .permitAll()
        .antMatchers("/actuator/health")
        .permitAll()
        .antMatchers("/actuator/health/*")
        .permitAll()
        .anyRequest()
        .authenticated()
        .and()
        .exceptionHandling()
        .accessDeniedHandler(accessDeniedHandler)
        .authenticationEntryPoint(authenticationEntryPoint)
        .and()
        .oauth2ResourceServer()
        .bearerTokenResolver(bearerTokenResolver)
        .opaqueToken()
        .introspector(tokenIntrospector);
    // @formatter:on
  }
}
