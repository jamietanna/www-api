package me.jvt.www.api.micropub.presentation.rest;

import java.util.Optional;
import me.jvt.www.api.micropub.model.MicropubQueryDto;
import me.jvt.www.api.micropub.service.QueryService;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QueryController {

  private final QueryService delegate;

  public QueryController(QueryService delegate) {
    this.delegate = delegate;
  }

  @GetMapping(path = "/micropub", produces = MediaType.APPLICATION_JSON_VALUE, params = "q")
  public MicropubQueryDto query(
      @RequestParam("q") String query,
      @RequestParam MultiValueMap<String, String> params,
      Authentication authentication) {
    Optional<Authentication> maybeAuthentication = Optional.empty();
    if (null != authentication) {
      maybeAuthentication = Optional.of(authentication);
    }
    return delegate.handle(query, params, maybeAuthentication);
  }
}
