package me.jvt.www.api.micropub.actions.concrete;

import java.util.ArrayList;
import java.util.List;
import me.jvt.www.api.micropub.actions.ReplacePropertyAction;
import me.jvt.www.api.micropub.model.Properties.Rsvp;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

public class ReplaceRsvpPropertyAction extends ReplacePropertyAction {

  public ReplaceRsvpPropertyAction(List<String> replacements) {
    super("rsvp", replacements);
  }

  @Override
  public void accept(Microformats2Object mf2) {
    List<Rsvp> replacements = new ArrayList<>();
    for (String replacement : this.replacements) {
      replacements.add(Rsvp.valueOf(replacement));
    }

    mf2.getProperties().put(this.field, replacements);
  }
}
