package me.jvt.www.api.micropub.validator.property;

import java.lang.annotation.Annotation;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.validator.field.DateValidator;

public class DatePropertyValidator implements ConstraintValidator<Annotation, Microformats2Object> {

  private final String property;
  private final DateValidator dateValidator;

  public DatePropertyValidator(String property, DateValidator dateValidator) {
    this.property = property;
    this.dateValidator = dateValidator;
  }

  @Override
  public boolean isValid(Microformats2Object mf2, ConstraintValidatorContext context) {
    List<String> propertyValue = (List<String>) mf2.getProperties().get(property);

    if (null == propertyValue || propertyValue.isEmpty()) {
      context
          .buildConstraintViolationWithTemplate(
              String.format("Property `%s` was not present", property))
          .addConstraintViolation();
      return false;
    }

    boolean isValid = dateValidator.isValid(propertyValue.get(0), context);
    return report(isValid, context);
  }

  private boolean report(boolean isValid, ConstraintValidatorContext context) {
    if (!isValid) {
      context
          .buildConstraintViolationWithTemplate(
              String.format("Property `%s` was not a valid date", property))
          .addConstraintViolation();
    }
    return isValid;
  }
}
