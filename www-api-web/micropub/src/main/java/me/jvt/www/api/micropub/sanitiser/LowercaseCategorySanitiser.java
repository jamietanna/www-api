package me.jvt.www.api.micropub.sanitiser;

import java.util.List;
import java.util.stream.Collectors;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.springframework.stereotype.Component;

@Component
public class LowercaseCategorySanitiser implements Microformats2ObjectSanitiser {

  @Override
  public Microformats2Object sanitise(Microformats2Object mf2) {
    List<String> categories = mf2.getProperties().getCategory();
    if (null == categories) {
      return mf2;
    }

    mf2.getProperties()
        .setCategory(categories.stream().map(String::toLowerCase).collect(Collectors.toList()));

    return mf2;
  }
}
