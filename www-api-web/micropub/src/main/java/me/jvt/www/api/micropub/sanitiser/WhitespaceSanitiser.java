package me.jvt.www.api.micropub.sanitiser;

import java.util.Map;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.StringSanitiser;

public class WhitespaceSanitiser implements Microformats2ObjectSanitiser {

  private final StringSanitiser stringSanitiserDelegate;

  public WhitespaceSanitiser(StringSanitiser stringSanitiser) {
    stringSanitiserDelegate = stringSanitiser;
  }

  @Override
  public Microformats2Object sanitise(Microformats2Object mf2) {
    if (null != mf2.getProperties().getBookmarkOf()) {
      mf2.getProperties()
          .getBookmarkOf()
          .set(
              0,
              stringSanitiserDelegate.removeWhitespace(mf2.getProperties().getBookmarkOf().get(0)));
    } else if (null != mf2.getProperties().getLikeOf()) {
      mf2.getProperties()
          .getLikeOf()
          .set(0, stringSanitiserDelegate.removeWhitespace(mf2.getProperties().getLikeOf().get(0)));
    } else if (null != mf2.getProperties().getInReplyTo()) {
      mf2.getProperties()
          .getInReplyTo()
          .set(
              0,
              stringSanitiserDelegate.removeWhitespace(mf2.getProperties().getInReplyTo().get(0)));
    } else if (null != mf2.getProperties().getRepostOf()) {
      mf2.getProperties()
          .getRepostOf()
          .set(
              0,
              stringSanitiserDelegate.removeWhitespace(mf2.getProperties().getRepostOf().get(0)));
    }

    if (null != mf2.getProperties().getName()) {
      mf2.getProperties()
          .getName()
          .set(0, stringSanitiserDelegate.removeWhitespace(mf2.getProperties().getName().get(0)));
    }

    if (null != mf2.getProperties().getContent()) {
      Map<String, String> content = mf2.getProperties().getContent().get(0);
      content.put("html", stringSanitiserDelegate.removeWhitespace(content.get("html")));
      content.put("value", stringSanitiserDelegate.removeWhitespace(content.get("value")));
    }

    return mf2;
  }
}
