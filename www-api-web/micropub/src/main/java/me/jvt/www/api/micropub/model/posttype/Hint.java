package me.jvt.www.api.micropub.model.posttype;

public enum Hint {
  DATE("date"),
  MULTILINE("multiline"),
  MULTIVALUE("multivalue"),
  URL("url");

  private final String name;

  Hint(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
