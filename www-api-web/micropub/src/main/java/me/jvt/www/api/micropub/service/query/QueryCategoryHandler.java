package me.jvt.www.api.micropub.service.query;

import java.util.Optional;
import me.jvt.www.api.micropub.model.MicropubCategoriesDto;
import me.jvt.www.api.micropub.model.MicropubQueryDto;
import me.jvt.www.api.micropub.service.MicropubService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

@Component
public class QueryCategoryHandler implements QueryHandler {

  private final MicropubService delegate;

  public QueryCategoryHandler(MicropubService delegate) {
    this.delegate = delegate;
  }

  @Override
  public String getQuery() {
    return "category";
  }

  @Override
  public MicropubQueryDto handle(
      MultiValueMap<String, String> parameters, Optional<Authentication> maybeAuthentication) {
    return new MicropubCategoriesDto(delegate.retrieveCategories());
  }
}
