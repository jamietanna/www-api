package me.jvt.www.api.micropub.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.RandomStringGenerator;
import org.springframework.stereotype.Component;

@Component
public class HugoPersistenceService implements PersistenceService {
  private static final String CITE_CHARACTERS_TO_REPLACE = "[^a-zA-Z0-9-]";
  private static final String CITE_CHARACTERS_REPLACEMENT = "_";

  private static final int SLUG_RANDOM_CHARACTER_COUNT = 5;
  private static final String ARTICLES_PATH = "content/%s.md";
  private static final String CONTACTS_PATH = "content/contacts/%s.md";
  private static final String EVENTS_SLUG = "%d-%02d-%s";
  private static final String EVENTS_PATH = "content/events/personal-events/%s.md";
  private static final String OTHERS_SLUG = "%d/%02d/%s";
  private static final String OTHERS_PATH = "content/mf2/%s.md";
  private static final String CITES_PATH = "data/cites/%s/%s.json";

  private static final String ARTICLES_URL_PATH = "/%s/";

  private static final String ARTICLES_URL_PREFIX = "/posts/";
  private static final String WEEK_NOTES_URL_PREFIX = "/week-notes/";
  private static final String CONTACT_CONTENT_URL_PREFIX = "/contacts/";
  private static final String MF2_CONTENT_URL_PREFIX = "/mf2/";
  private static final String EVENT_CONTENT_URL_PREFIX = "/events/personal-events/";

  private static final DateTimeFormatter DATE_TIME_FORMATTER =
      DateTimeFormatter.ISO_OFFSET_DATE_TIME;
  // adapted from https://stackoverflow.com/a/46488425
  private static final DateTimeFormatter ISO_OFFSET_DATE_TIME_NO_MILLISECONDS_OR_COLON_IN_OFFSET =
      DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss[.SSS][xxx][xx]['Z']");
  private static final DateTimeFormatter POSTS_PATH_DATE_FORMAT =
      DateTimeFormatter.ofPattern("yyyy-MM-dd");
  private static final DateTimeFormatter POSTS_URLPATH_DATE_FORMAT =
      DateTimeFormatter.ofPattern("yyyy/MM/dd");

  private final RandomStringGenerator randomStringGenerator;
  private final ObjectMapper objectMapper;

  public HugoPersistenceService(
      RandomStringGenerator randomStringGenerator, ObjectMapper objectMapper) {
    this.randomStringGenerator = randomStringGenerator;
    this.objectMapper = objectMapper;
  }

  @Override
  public PersistenceAction determineAction(Microformats2Object mf2) {
    Kind kind = mf2.getKind();

    if (Kind.articles.equals(kind)) {
      return actionForArticles(mf2);
    }

    if (Kind.contacts.equals(kind)) {
      return actionForContacts(mf2);
    }

    ZonedDateTime publishDate = parseDate(mf2.getProperties().getPublished().get(0));

    switch (kind) {
      case events:
        return actionForEvents(mf2.getContext().getSlug(), publishDate);
      case bookmarks:
      case likes:
      case notes:
      case photos:
      case reads:
      case replies:
      case reposts:
      case rsvps:
      case steps:
      case listens:
        return actionForOthers(mf2.getContext().getSlug(), publishDate, kind);
    }

    throw new IllegalStateException(
        String.format("The Kind, %s, is not supported at this time", kind));
  }

  @Override
  public List<PersistenceAction> determineAction(JsonNode cite) {
    if (null == cite) {
      throw new IllegalArgumentException("Cite cannot be null");
    }

    if (cite.isArray()) {
      throw new IllegalArgumentException("Cite cannot be ArrayNode");
    }

    List<PersistenceAction> actions = new ArrayList<>();
    if (cite.isEmpty()) {
      return actions;
    }

    for (JsonNode citeObject : cite.get("items")) {
      if (!citeObject.has("properties")) {
        continue;
      }
      JsonNode properties = citeObject.get("properties");
      if (!properties.has("url")) {
        continue;
      }

      JsonNode urlNode = properties.get("url");
      if (urlNode.isEmpty()) {
        continue;
      }

      String url = urlNode.get(0).textValue();
      try {
        URL parsed = new URL(url);
        String path = String.format(CITES_PATH, parsed.getHost(), sanitiseCiteUrl(parsed));
        actions.add(new PersistenceAction(null, null, path, null));
      } catch (MalformedURLException e) {
        // ignore
      }
    }

    return actions;
  }

  @Override
  public String serialize(PersistenceAction persistenceAction, Microformats2Object mf2) {
    switch (mf2.getKind()) {
      case bookmarks:
      case events:
      case likes:
      case notes:
      case photos:
      case reads:
      case replies:
      case reposts:
      case rsvps:
      case steps:
      case listens:
        mf2.getContext().setSlug(persistenceAction.getSlug());
      case contacts:
        // do nothing
    }
    return mf2.serialize();
  }

  @Override
  public List<String> serialize(JsonNode cite) {
    if (null == cite) {
      throw new IllegalArgumentException("Cite cannot be null");
    }

    if (cite.isArray()) {
      throw new IllegalArgumentException("Cite cannot be ArrayNode");
    }

    List<String> serializations = new ArrayList<>();
    if (cite.isEmpty()) {
      return serializations;
    }

    for (JsonNode citeObject : cite.get("items")) {
      if (!citeObject.has("properties")) {
        continue;
      }

      try {
        serializations.add(
            objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(citeObject));
      } catch (JsonProcessingException e) {
        throw new IllegalStateException("Something went wrong when trying to serialize", e);
      }
    }
    return serializations;
  }

  @Override
  public Optional<String> contentPathFromUrl(String publicUrl) {
    URL url;
    try {
      url = new URL(publicUrl);
    } catch (MalformedURLException e) {
      return Optional.empty();
    }

    String path = url.getPath();

    if (path.startsWith(MF2_CONTENT_URL_PREFIX)) {
      path = path.replace(MF2_CONTENT_URL_PREFIX, "content/mf2/");
      path = path.replaceAll("/$", ".md");
      return Optional.of(path);
    } else if (path.startsWith(EVENT_CONTENT_URL_PREFIX)) {
      path = path.replace(EVENT_CONTENT_URL_PREFIX, "content/events/personal-events/");
      path = path.replaceAll("/$", ".md");
      return Optional.of(path);
    } else if (path.startsWith(CONTACT_CONTENT_URL_PREFIX)) {
      path = path.replace(CONTACT_CONTENT_URL_PREFIX, "content/contacts/");
      path = path.replaceAll("/$", ".md");
      return Optional.of(path);
    } else if (path.startsWith(WEEK_NOTES_URL_PREFIX)) {
      path = path.replace(WEEK_NOTES_URL_PREFIX, "content/week-notes/");
      path = path.replaceAll("/$", ".md");
      return Optional.of(path);
    } else if (path.startsWith(ARTICLES_URL_PREFIX)) {
      path = path.replace(ARTICLES_URL_PREFIX, "");
      path = path.replaceAll("/$", ".md");
      path = path.replace("/", "-");
      return Optional.of("content/posts/" + path);
    }

    return Optional.empty();
  }

  private PersistenceAction actionForArticles(Microformats2Object mf2) {
    String slug = mf2.getContext().getSlug();
    if (null == slug || slug.isEmpty()) {
      throw new IllegalStateException("Missing slug in post, something has gone wrong internally");
    }

    if (slug.startsWith("week-notes/")) {
      String urlPath = String.format(ARTICLES_URL_PATH, slug);
      return new PersistenceAction(
          null, urlPath, String.format(ARTICLES_PATH, slug), String.format("article %s", slug));
    } else {
      ZonedDateTime publishDate = parseDate(mf2.getProperties().getPublished().get(0));
      String urlPath =
          String.format(
              "posts/%s/%s",
              POSTS_URLPATH_DATE_FORMAT.format(publishDate), mf2.getContext().getSlug());
      String path =
          String.format(
              ARTICLES_PATH,
              String.format(
                  "posts/%s-%s",
                  POSTS_PATH_DATE_FORMAT.format(publishDate), mf2.getContext().getSlug()));
      return new PersistenceAction(
          null, "/" + urlPath + "/", path, String.format("article %s", urlPath));
    }
  }

  private PersistenceAction actionForContacts(Microformats2Object mf2) {
    List<String> nickname = mf2.getProperties().getNickname();
    String slug = nickname.get(0);
    String path = String.format(CONTACTS_PATH, slug);
    String urlPath = CONTACT_CONTENT_URL_PREFIX + slug + "/";

    return new PersistenceAction(slug, urlPath, path, String.format("contact %s", slug));
  }

  private PersistenceAction actionForEvents(String mf2Slug, ZonedDateTime publishDate) {
    String slug;
    if (null == mf2Slug) {
      slug =
          String.format(
              EVENTS_SLUG,
              publishDate.get(ChronoField.YEAR),
              publishDate.get(ChronoField.MONTH_OF_YEAR),
              randomStringGenerator.randomAlphanumeric(SLUG_RANDOM_CHARACTER_COUNT));
    } else {
      slug = mf2Slug;
    }
    String path = String.format(EVENTS_PATH, slug);
    String urlPath = EVENT_CONTENT_URL_PREFIX + slug + "/";
    return new PersistenceAction(slug, urlPath, path, String.format("events %s", slug));
  }

  private PersistenceAction actionForOthers(String mf2Slug, ZonedDateTime publishDate, Kind kind) {
    String slug;
    if (null == mf2Slug) {
      slug =
          String.format(
              OTHERS_SLUG,
              publishDate.get(ChronoField.YEAR),
              publishDate.get(ChronoField.MONTH_OF_YEAR),
              randomStringGenerator.randomAlphanumeric(SLUG_RANDOM_CHARACTER_COUNT));
    } else {
      slug = mf2Slug;
    }
    String path = String.format(OTHERS_PATH, slug);
    String urlPath = MF2_CONTENT_URL_PREFIX + slug + "/";
    return new PersistenceAction(
        slug, urlPath, path, String.format("%s %s", kind.toString(), slug));
  }

  private ZonedDateTime parseDate(String date) {
    try {
      return ZonedDateTime.parse(date, DATE_TIME_FORMATTER);
    } catch (DateTimeParseException e) {
      // ignore
    }

    try {
      return ZonedDateTime.parse(date, ISO_OFFSET_DATE_TIME_NO_MILLISECONDS_OR_COLON_IN_OFFSET);
    } catch (DateTimeParseException e) {
      // ignore
    }

    throw new IllegalStateException(String.format("Publish date %s could not be parsed", date));
  }

  private String sanitiseCiteUrl(URL url) {
    StringBuilder builder = new StringBuilder();
    String path = url.getPath();
    if (path.length() == 0) {
      path = CITE_CHARACTERS_REPLACEMENT;
    }
    builder.append(path);

    if (null != url.getQuery()) {
      builder.append("?");
      builder.append(url.getQuery());
    }
    if (null != url.getRef()) {
      builder.append("#");
      builder.append(url.getRef());
    }
    return builder.toString().replaceAll(CITE_CHARACTERS_TO_REPLACE, CITE_CHARACTERS_REPLACEMENT);
  }
}
