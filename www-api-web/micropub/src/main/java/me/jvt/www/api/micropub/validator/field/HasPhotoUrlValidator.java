package me.jvt.www.api.micropub.validator.field;

import java.util.Map;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;
import org.hibernate.validator.internal.constraintvalidators.hv.URLValidator;

public class HasPhotoUrlValidator
    implements ConstraintValidator<HasPhotoUrlConstraint, Map<String, String>> {

  private final URLValidator delegate;

  public HasPhotoUrlValidator(URLValidator delegate) {
    this.delegate = delegate;
  }

  @Override
  public boolean isValid(Map<String, String> value, ConstraintValidatorContext context) {
    if (value == null || value.isEmpty()) {
      return false;
    }

    if (!value.containsKey(MicroformatsProperty.PHOTO.getName())) {
      return false;
    }

    String photo = value.get(MicroformatsProperty.PHOTO.getName());
    if (null == photo || photo.isEmpty()) {
      return false;
    }

    return delegate.isValid(photo, context);
  }
}
