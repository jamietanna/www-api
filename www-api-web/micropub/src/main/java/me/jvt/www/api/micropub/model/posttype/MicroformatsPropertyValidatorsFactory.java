package me.jvt.www.api.micropub.model.posttype;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintValidator;
import me.jvt.www.api.micropub.validator.field.DateValidator;
import me.jvt.www.api.micropub.validator.field.HasAltTextValidator;
import me.jvt.www.api.micropub.validator.field.UrlValidator;
import me.jvt.www.api.micropub.validator.property.ContentPropertyValidator;
import me.jvt.www.api.micropub.validator.property.DatePropertyValidator;
import me.jvt.www.api.micropub.validator.property.NotEmptyPropertyValidator;
import me.jvt.www.api.micropub.validator.property.PhotoPropertyValidator;
import me.jvt.www.api.micropub.validator.property.PostStatusPropertyValidator;
import me.jvt.www.api.micropub.validator.property.ReadOfPropertyValidator;
import me.jvt.www.api.micropub.validator.property.ReadStatusPropertyValidator;
import me.jvt.www.api.micropub.validator.property.RsvpPropertyValidator;
import me.jvt.www.api.micropub.validator.property.UrlPropertyValidator;
import org.hibernate.validator.internal.constraintvalidators.bv.NotBlankValidator;

public class MicroformatsPropertyValidatorsFactory {

  private static final UrlValidator URL_VALIDATOR = new UrlValidator();
  private static final NotBlankValidator NOT_BLANK_VALIDATOR = new NotBlankValidator();
  private static final DateValidator DATE_VALIDATOR = new DateValidator();
  private final Set<String> photoPrefixes;
  private static final RsvpPropertyValidator RSVP_VALIDATOR = new RsvpPropertyValidator();
  private static final HasAltTextValidator HAS_ALT_TEXT_VALIDATOR = new HasAltTextValidator();

  public MicroformatsPropertyValidatorsFactory(Set<String> photoPrefixes) {
    this.photoPrefixes = photoPrefixes;
  }

  public List<ConstraintValidator<?, Microformats2Object>> validatorsForProperty(
      MicroformatsProperty property) {
    switch (property) {
      case NAME:
      case NICKNAME:
      case CATEGORY:
      case SUMMARY:
      case NUM:
      case UNIT:
      case REL_TWITTER:
      case CODE_LICENSE:
      case PROSE_LICENSE:
      case SERIES:
      case FEATURED:
        return Collections.singletonList(notEmpty(property));
      case URL:
      case REPOST_OF:
      case LIKE_OF:
      case IN_REPLY_TO:
      case BOOKMARK_OF:
      case SYNDICATION:
      case LISTEN_OF:
        return Collections.singletonList(url(property));
      case PUBLISHED:
      case END:
      case START:
        return Collections.singletonList(date(property));
      case RSVP:
        return Collections.singletonList(RSVP_VALIDATOR);
      case PHOTO:
        return Collections.singletonList(
            new PhotoPropertyValidator(URL_VALIDATOR, HAS_ALT_TEXT_VALIDATOR, photoPrefixes));
      case CONTENT:
        return Collections.singletonList(new ContentPropertyValidator(NOT_BLANK_VALIDATOR));
      case READ_OF:
        return Collections.singletonList(new ReadOfPropertyValidator());
      case READ_STATUS:
        return Collections.singletonList(new ReadStatusPropertyValidator());
      case POST_STATUS:
        return Collections.singletonList(new PostStatusPropertyValidator());
    }

    return Collections.emptyList();
  }

  private ConstraintValidator<?, Microformats2Object> date(MicroformatsProperty property) {
    return new DatePropertyValidator(property.getName(), DATE_VALIDATOR);
  }

  private ConstraintValidator<?, Microformats2Object> notEmpty(MicroformatsProperty property) {
    return new NotEmptyPropertyValidator(property.getName(), NOT_BLANK_VALIDATOR);
  }

  private ConstraintValidator<?, Microformats2Object> url(MicroformatsProperty property) {
    return new UrlPropertyValidator(property.getName(), URL_VALIDATOR);
  }
}
