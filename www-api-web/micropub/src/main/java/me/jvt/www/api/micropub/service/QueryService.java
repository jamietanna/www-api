package me.jvt.www.api.micropub.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.MicropubConfigDto;
import me.jvt.www.api.micropub.model.MicropubPostTypesDto;
import me.jvt.www.api.micropub.model.MicropubQueryDto;
import me.jvt.www.api.micropub.service.query.QueryHandler;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

@Component
public class QueryService {

  private final Set<QueryHandler> handlers;

  public QueryService(QueryHandler... handlers) {
    this.handlers = new HashSet<>(Arrays.asList(handlers));
  }

  /**
   * Perform a Micropub query.
   *
   * @param query the query
   * @param parameters the querystring parameters
   * @param maybeAuthentication authentication details for the caller, if any
   * @return the response DTO
   * @throws InvalidMicropubMetadataRequest if the query is not supported, or was performed
   *     incorrectly
   */
  public MicropubQueryDto handle(
      String query,
      MultiValueMap<String, String> parameters,
      Optional<Authentication> maybeAuthentication)
      throws InvalidMicropubMetadataRequest {
    Optional<QueryHandler> maybeHandler = getHandler(query);

    if (!maybeHandler.isPresent()) {
      throw new InvalidMicropubMetadataRequest(String.format("Query `%s` is not supported", query));
    }

    QueryHandler handler = maybeHandler.get();
    MicropubQueryDto dto = handler.handle(parameters, maybeAuthentication);

    if (query.equals("config")) {
      decorateQueryConfig(dto);
    }

    return dto;
  }

  private void decorateQueryConfig(MicropubQueryDto dto) {
    MicropubConfigDto actual = (MicropubConfigDto) dto;
    actual.setQueries(handlers.stream().map(QueryHandler::getQuery).collect(Collectors.toSet()));

    Optional<QueryHandler> maybeHandler = getHandler("post-types");
    if (!maybeHandler.isPresent()) {
      throw new IllegalStateException("No post-types handler was configured");
    }

    QueryHandler postTypesHandler = maybeHandler.get();
    MicropubPostTypesDto postTypesDto =
        (MicropubPostTypesDto) postTypesHandler.handle(null, Optional.empty());
    actual.setPostTypes(postTypesDto.postTypes);
  }

  private Optional<QueryHandler> getHandler(String query) {
    return handlers.stream().filter(h -> query.equals(h.getQuery())).findFirst();
  }
}
