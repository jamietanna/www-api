package me.jvt.www.api.micropub.model.posttype;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.Properties.PostStatus;

public class HugoArticleMicroformats2Object implements Microformats2Object {

  private final YAMLMapper mapper;
  private final Properties properties;
  private final Context context;

  public HugoArticleMicroformats2Object(YAMLMapper mapper, Properties properties, Context context) {
    this.mapper = mapper;
    this.properties = properties;
    this.context = context;
  }

  @Override
  public Kind getKind() {
    return Kind.articles;
  }

  @Override
  public Properties getProperties() {
    return properties;
  }

  @Override
  public Context getContext() {
    return context;
  }

  @Override
  public String serialize() {
    ObjectNode node = mapper.createObjectNode();
    node.put("title", properties.getName().get(0));
    node.put("description", properties.getSummary().get(0));
    node.put("date", properties.getPublished().get(0));

    if (null != context.getAliases()) {
      ArrayNode aliases = node.withArray("aliases");
      context.getAliases().forEach(aliases::add);
    }

    if (null != properties.getSyndication()) {
      ArrayNode syndication = node.withArray("syndication");
      properties.getSyndication().forEach(syndication::add);
    }

    if (null != properties.getCategory()) {
      ArrayNode categories = node.withArray("tags");
      properties.getCategory().forEach(categories::add);
    }

    List<String> postStatus = properties.getPostStatus();
    if (null != postStatus && PostStatus.draft.name().equals(postStatus.get(0))) {
      node.put("draft", true);
    }

    if (context.isDeleted()) {
      node.put("deleted", context.isDeleted());
    }

    if (!isEmpty(properties.getCodeLicense())) {
      node.put("license_code", properties.getCodeLicense().get(0));
    }

    if (!isEmpty(properties.getProseLicense())) {
      node.put("license_prose", properties.getProseLicense().get(0));
    }

    if (!isEmpty(properties.getSeries())) {
      node.put("series", properties.getSeries().get(0));
    }

    if (!isEmpty(properties.getFeatured())) {
      node.put("image", properties.getFeatured().get(0));
    }

    if (!isEmpty(properties.getRepostOf())) {
      node.put("canonical_url", properties.getRepostOf().get(0));
    }

    if (null != context.getSlug()) {
      if (!context.getSlug().startsWith("week-notes/")) {
        node.put("slug", context.getSlug());
      }
    }

    StringJoiner joiner = new StringJoiner("\n");
    try {
      joiner.add(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(node).trim());
    } catch (JsonProcessingException e) {
      throw new IllegalStateException(e);
    }

    joiner.add("---");
    Map<String, String> contentMap = properties.getContent().get(0);
    String content = contentMap.get("html");
    if (null != content && !content.isEmpty()) {
      joiner.add(content);
    } else {
      joiner.add(contentMap.get("value"));
    }
    return joiner.toString();
  }

  private static boolean isEmpty(List<?> l) {
    return l == null || l.isEmpty();
  }
}
