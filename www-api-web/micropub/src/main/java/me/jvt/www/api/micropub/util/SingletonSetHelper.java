package me.jvt.www.api.micropub.util;

import java.util.HashSet;
import java.util.Set;

public class SingletonSetHelper {

  public static <T> Set<T> singletonSet(T value) {
    Set<T> set = new HashSet<>();
    set.add(value);
    return set;
  }
}
