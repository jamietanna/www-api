package me.jvt.www.api.micropub.util;

import java.util.ArrayList;
import java.util.List;

public class SingletonListHelper {

  public static <T> List<T> singletonList(T value) {
    List<T> list = new ArrayList<>();
    list.add(value);
    return list;
  }
}
