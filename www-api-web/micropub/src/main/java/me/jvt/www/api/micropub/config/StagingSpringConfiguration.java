package me.jvt.www.api.micropub.config;

import me.jvt.www.api.micropub.client.PostClient;
import me.jvt.www.api.micropub.repository.MicropubPersistenceRepository;
import me.jvt.www.api.micropub.repository.StubMicropubPersistenceRepository;
import me.jvt.www.api.micropub.service.MicropubConflictService;
import me.jvt.www.api.micropub.service.PersistenceService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("staging")
public class StagingSpringConfiguration {

  @Bean
  public MicropubPersistenceRepository stubMicropubPersistenceRepository(
      PersistenceService persistenceService,
      MicropubConflictService micropubConflictService,
      PostClient postClient) {
    return new StubMicropubPersistenceRepository(
        persistenceService, micropubConflictService, postClient);
  }
}
