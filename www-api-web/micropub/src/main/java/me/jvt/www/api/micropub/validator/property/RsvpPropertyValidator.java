package me.jvt.www.api.micropub.validator.property;

import java.lang.annotation.Annotation;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Properties.Rsvp;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;

public class RsvpPropertyValidator implements ConstraintValidator<Annotation, Microformats2Object> {

  @Override
  public boolean isValid(Microformats2Object mf2, ConstraintValidatorContext context) {
    if (!mf2.getProperties().containsKey(MicroformatsProperty.RSVP.getName())) {
      return false;
    }
    List<?> rsvp = mf2.getProperties().getRsvp();

    if (null == rsvp || rsvp.isEmpty()) {
      return false;
    }

    if (rsvp.get(0) instanceof Rsvp) {
      return true;
    } else if (rsvp.get(0) instanceof String) {
      try {
        Rsvp.valueOf((String) rsvp.get(0));
        return true;
      } catch (IllegalArgumentException e) {
        return false;
      }
    }

    throw new InvalidMicropubMetadataRequest(
        "The RSVP property was not populated with a valid value");
  }
}
