package me.jvt.www.api.micropub.actions;

import java.util.List;
import java.util.Objects;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

public class ReplacePropertyAction implements Action {

  protected final String field;
  protected final List<String> replacements;

  public ReplacePropertyAction(String field, List<String> replacements) {
    this.field = field;
    this.replacements = replacements;
  }

  @Override
  public void accept(Microformats2Object mf2) {
    mf2.getProperties().put(field, replacements);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ReplacePropertyAction that = (ReplacePropertyAction) o;
    return Objects.equals(field, that.field) && Objects.equals(replacements, that.replacements);
  }

  @Override
  public int hashCode() {
    return Objects.hash(field, replacements);
  }
}
