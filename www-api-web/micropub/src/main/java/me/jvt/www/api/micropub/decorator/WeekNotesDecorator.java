package me.jvt.www.api.micropub.decorator;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.IsoFields;
import java.time.temporal.TemporalAdjusters;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import me.jvt.www.api.micropub.actions.ReplacePropertyAction;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.springframework.stereotype.Component;

@Component
public class WeekNotesDecorator implements Microformats2ObjectDecorator {

  private static final DateTimeFormatter DATE_TIME_FORMATTER =
      DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("UTC"));
  private static final Pattern WEEK_NOTES_TITLE = Pattern.compile("^Week Notes (\\d+)#(\\d+)$");
  public static final String SLUG_FORMAT = "week-notes/%d/%02d";

  @Override
  public Microformats2Object decorate(Microformats2Object mf2) {
    if (Kind.articles.equals(mf2.getKind())) {
      List<String> nameProperty = mf2.getProperties().getName();
      if (null == nameProperty || nameProperty.isEmpty() || nameProperty.get(0).isEmpty()) {
        throw new InvalidMicropubMetadataRequest("Articles require a title");
      }

      String title = nameProperty.get(0);
      Matcher m = WEEK_NOTES_TITLE.matcher(title);

      if (m.matches()) {
        int year = 2000 + Integer.parseInt(m.group(1));
        int week = Integer.parseInt(m.group(2));
        LocalDate desiredDate =
            LocalDate.now()
                .withYear(year)
                .with(IsoFields.WEEK_OF_WEEK_BASED_YEAR, week)
                .with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        mf2.getContext().setSlug(String.format(SLUG_FORMAT, year, week));
        mf2.getProperties()
            .setSummary(
                SingletonListHelper.singletonList(
                    String.format(
                        "What happened in the week of %s?",
                        desiredDate.format(DateTimeFormatter.ISO_DATE))));
        List<String> postStatusProperty = mf2.getProperties().getPostStatus();
        if (postStatusProperty == null || postStatusProperty.isEmpty()) {
          new ReplacePropertyAction(
                  MicroformatsProperty.POST_STATUS.getName(),
                  Collections.singletonList(Properties.PostStatus.draft.name()))
              .accept(mf2);
        }

        Instant published = desiredDate.plusDays(6).atTime(23, 0).toInstant(ZoneOffset.UTC);
        if (null == mf2.getProperties().getPublished()
            || mf2.getProperties().getPublished().isEmpty()) {
          mf2.getProperties()
              .setPublished(
                  SingletonListHelper.singletonList(DATE_TIME_FORMATTER.format(published)));
        }
      }
    }

    return mf2;
  }
}
