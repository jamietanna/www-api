package me.jvt.www.api.micropub.util;

import org.apache.commons.lang3.RandomStringUtils;

public class RandomStringGenerator {

  public String randomAlphanumeric(int count) {
    return RandomStringUtils.randomAlphanumeric(count).toLowerCase();
  }
}
