package me.jvt.www.api.micropub.service.query;

import java.util.ArrayList;
import java.util.Optional;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.MicropubPostTypeDto;
import me.jvt.www.api.micropub.model.MicropubPostTypesDto;
import me.jvt.www.api.micropub.model.MicropubQueryDto;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

@Component
public class PostTypesHandler implements QueryHandler {

  @Override
  public String getQuery() {
    return "post-types";
  }

  @Override
  public MicropubQueryDto handle(
      MultiValueMap<String, String> parameters, Optional<Authentication> maybeAuthentication) {
    MicropubPostTypesDto dto = new MicropubPostTypesDto();
    dto.postTypes = new ArrayList<>();
    for (Kind kind : Kind.values()) {
      dto.postTypes.add(new MicropubPostTypeDto(kind));
    }

    return dto;
  }
}
