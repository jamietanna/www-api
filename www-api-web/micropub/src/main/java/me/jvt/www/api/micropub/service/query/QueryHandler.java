package me.jvt.www.api.micropub.service.query;

import java.util.Optional;
import me.jvt.www.api.micropub.model.MicropubQueryDto;
import org.springframework.security.core.Authentication;
import org.springframework.util.MultiValueMap;

public interface QueryHandler {

  /**
   * Return the value of the query, which is contained in the <code>q</code> parameter in the
   * querystring.
   *
   * @return the value, i.e. <code>config</code>
   */
  String getQuery();

  /**
   * Handle the query, and return the DTO that can be returned directly to the caller.
   *
   * @param parameters the querystring parameters from the incoming request
   * @param maybeAuthentication the caller's authentication, if any
   * @return the DTO that can be returned directly to the caller.
   */
  MicropubQueryDto handle(
      MultiValueMap<String, String> parameters, Optional<Authentication> maybeAuthentication);
}
