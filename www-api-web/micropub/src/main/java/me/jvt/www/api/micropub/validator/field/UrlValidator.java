package me.jvt.www.api.micropub.validator.field;

import java.lang.annotation.Annotation;
import java.net.MalformedURLException;
import java.net.URL;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UrlValidator implements ConstraintValidator<Annotation, String> {

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    try {
      new URL(value);
    } catch (MalformedURLException e) {
      return false;
    }

    return true;
  }
}
