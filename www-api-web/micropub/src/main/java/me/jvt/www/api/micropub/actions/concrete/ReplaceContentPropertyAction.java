package me.jvt.www.api.micropub.actions.concrete;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.jvt.www.api.micropub.actions.ReplacePropertyAction;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

public class ReplaceContentPropertyAction extends ReplacePropertyAction {

  public ReplaceContentPropertyAction(List<String> replacements) {
    super("content", replacements);
  }

  @Override
  public void accept(Microformats2Object mf2) {
    mf2.getProperties().put(field, replacementsToList());
  }

  private List<Map<String, String>> replacementsToList() {
    List<Map<String, String>> content = new ArrayList<>();
    for (String replacement : replacements) {
      Map<String, String> c = new HashMap<>();
      c.put("html", "");
      c.put("value", replacement);
      content.add(c);
    }

    return content;
  }
}
