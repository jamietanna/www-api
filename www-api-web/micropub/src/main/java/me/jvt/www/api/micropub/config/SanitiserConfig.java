package me.jvt.www.api.micropub.config;

import java.util.LinkedHashSet;
import me.jvt.www.api.micropub.sanitiser.*;
import me.jvt.www.api.micropub.util.StringSanitiser;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class SanitiserConfig {

  @Bean
  public TwitterUrlSanitiser twitterUrlSanitiser() {
    return new TwitterUrlSanitiser();
  }

  @Bean
  public WhitespaceSanitiser whitespaceSanitiser(StringSanitiser stringSanitiser) {
    return new WhitespaceSanitiser(stringSanitiser);
  }

  @Bean
  public UniqueCategorySanitiser uniqueCategorySanitiser() {
    return new UniqueCategorySanitiser();
  }

  @Bean
  public UniqueSyndicationSanitiser uniqueSyndicationSanitiser() {
    return new UniqueSyndicationSanitiser();
  }

  @Bean
  public LinkedHashSet<Microformats2ObjectSanitiser> mf2Sanitisers(
      LowercaseCategorySanitiser lowercaseCategorySanitiser,
      TwitterUrlSanitiser twitterUrlSanitiser,
      WhitespaceSanitiser whitespaceSanitiser,
      UniqueCategorySanitiser uniqueCategorySanitiser,
      UniqueSyndicationSanitiser uniqueSyndicationSanitiser) {
    LinkedHashSet<Microformats2ObjectSanitiser> sanitisers = new LinkedHashSet<>();
    sanitisers.add(lowercaseCategorySanitiser);
    sanitisers.add(twitterUrlSanitiser);
    sanitisers.add(whitespaceSanitiser);
    sanitisers.add(uniqueCategorySanitiser);
    sanitisers.add(uniqueSyndicationSanitiser);
    return sanitisers;
  }
}
