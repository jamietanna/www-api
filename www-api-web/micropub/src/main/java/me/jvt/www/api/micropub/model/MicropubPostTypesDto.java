package me.jvt.www.api.micropub.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class MicropubPostTypesDto extends MicropubQueryDto {

  @JsonProperty("post-types")
  public List<MicropubPostTypeDto> postTypes;
}
