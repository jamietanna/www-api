package me.jvt.www.api.micropub.context;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import me.jvt.www.api.micropub.client.Microformats2PipeClient;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class PhpMicroformatsContextRetriever implements ContextRetriever {
  private final Microformats2PipeClient client;

  public PhpMicroformatsContextRetriever(Microformats2PipeClient client) {
    this.client = client;
  }

  @Override
  public List<JsonNode> retrieve(Microformats2Object mf2) {
    var nodes = new ArrayList<JsonNode>();
    retrieveContext(mf2.getProperties().getBookmarkOf()).ifPresent(nodes::add);
    retrieveContext(mf2.getProperties().getInReplyTo()).ifPresent(nodes::add);
    retrieveContext(mf2.getProperties().getLikeOf()).ifPresent(nodes::add);
    retrieveContext(mf2.getProperties().getListenOf()).ifPresent(nodes::add);
    retrieveContext(mf2.getProperties().getRepostOf()).ifPresent(nodes::add);
    retrieveContext(mf2.getProperties().getUrl()).ifPresent(nodes::add);

    return nodes;
  }

  private Optional<JsonNode> retrieveContext(List<String> url) {
    if (null == url || url.isEmpty()) {
      return Optional.empty();
    }

    var uri =
        UriComponentsBuilder.fromUriString("https://php.microformats.io/")
            .queryParam("url", url.get(0))
            .build()
            .encode()
            .toString();
    return client.retrieve(uri);
  }
}
