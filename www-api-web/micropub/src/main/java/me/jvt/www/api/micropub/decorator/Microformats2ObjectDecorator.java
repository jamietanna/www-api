package me.jvt.www.api.micropub.decorator;

import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

public interface Microformats2ObjectDecorator {

  /**
   * Perform some sort of decoration to the provided {@link Microformats2Object} and then return it.
   *
   * <p>Note that although this uses the term <code>decorator</code> it is not actually using the
   * decorator design pattern.
   *
   * @param mf2 the given {@link Microformats2Object} to decorate
   * @return the output {@link Microformats2Object}, which may be different than <code>
   *     mf2</code>
   */
  Microformats2Object decorate(Microformats2Object mf2);
}
