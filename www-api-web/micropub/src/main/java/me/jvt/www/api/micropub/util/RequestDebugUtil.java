package me.jvt.www.api.micropub.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

public class RequestDebugUtil {
  private static final String ACCESS_TOKEN_FIELD_TO_SANITISE = "access_token";
  private final ObjectMapper objectMapper;

  public RequestDebugUtil(ObjectMapper objectMapper) {

    this.objectMapper = objectMapper;
  }

  private String formParameters(MultiValueMap<String, String> formParameters) {
    StringJoiner parameterLogMessageJoiner = new StringJoiner(", ");
    for (Map.Entry<String, List<String>> entry : formParameters.entrySet()) {
      if (!ACCESS_TOKEN_FIELD_TO_SANITISE.equals(entry.getKey())) {
        parameterLogMessageJoiner.add(
            String.format("%s=%s", entry.getKey(), entry.getValue().toString()));
      } else {
        parameterLogMessageJoiner.add("access_token=[####]");
      }
    }

    return String.format("{%s}", parameterLogMessageJoiner.toString());
  }

  public String formParameters(Map<String, String[]> formParameters) {
    StringJoiner parameterLogMessageJoiner = new StringJoiner(", ");
    for (Map.Entry<String, String[]> entry : formParameters.entrySet()) {
      if (!ACCESS_TOKEN_FIELD_TO_SANITISE.equals(entry.getKey())) {
        parameterLogMessageJoiner.add(
            String.format("%s=%s", entry.getKey(), Arrays.toString(entry.getValue())));
      } else {
        parameterLogMessageJoiner.add("access_token=[####]");
      }
    }

    return String.format("{%s}", parameterLogMessageJoiner.toString());
  }

  public String mediaEndpoint(MultipartFile multipartFile) {
    return String.format(
        "{name=%s, originalFilename=%s, contentType=%s}",
        multipartFile.getName(),
        multipartFile.getOriginalFilename(),
        multipartFile.getContentType());
  }

  public String json(JsonNode body) {
    try {
      return Base64.getEncoder().encodeToString(objectMapper.writeValueAsString(body).getBytes());
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }
}
