package me.jvt.www.api.micropub.config;

import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.micropub.client.AbstractGitLabClient;
import me.jvt.www.api.micropub.client.AbstractGitLabClient.Configuration;
import me.jvt.www.api.micropub.client.GitLabMediaClient;
import me.jvt.www.api.micropub.client.GitLabPostClient;
import me.jvt.www.api.micropub.client.PostClient;
import me.jvt.www.api.micropub.model.posttype.Microformats2ObjectFactory;
import me.jvt.www.api.micropub.service.PersistenceService;
import me.jvt.www.api.micropub.util.MediaTypeHelper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class GitLabClientConfiguration {
  @Value("${gitlab.api.endpoint}")
  private String gitlabApiEndpoint;

  @Value("${gitlab.api.projectId}")
  private String gitlabApiProjectId;

  @Value("${gitlab.api.accessToken}")
  private String gitlabApiAccessToken;

  @Bean(name = "gitlab.api.endpoint")
  public String getGitlabApiEndpoint() {
    return gitlabApiEndpoint;
  }

  @Bean(name = "gitlab.api.projectId")
  public String getGitlabApiProjectId() {
    return gitlabApiProjectId;
  }

  @Bean(name = "gitlab.api.accessToken")
  public String getGitlabApiAccessToken() {
    return gitlabApiAccessToken;
  }

  @Bean
  public Configuration postGitlabConfiguration(
      @Qualifier("gitlab.api.endpoint") String gitlabApiEndpoint,
      @Qualifier("gitlab.api.projectId") String gitlabApiProjectId,
      @Qualifier("gitlab.api.accessToken") String gitlabApiAccessToken) {
    return new Configuration(gitlabApiEndpoint, gitlabApiProjectId, gitlabApiAccessToken);
  }

  @Bean
  public Configuration mediaGitlabConfiguration(
      @Qualifier("gitlab.api.endpoint") String gitlabApiEndpoint,
      @Value("${gitlab.api.media.projectId}") String gitlabApiProjectId,
      @Qualifier("gitlab.api.accessToken") String gitlabApiAccessToken) {
    return new Configuration(gitlabApiEndpoint, gitlabApiProjectId, gitlabApiAccessToken);
  }

  @Bean
  public PostClient postGitLabClient(
      @Qualifier("postGitlabConfiguration") Configuration gitlabConfiguration,
      RequestSpecificationFactory requestSpecificationFactory,
      PersistenceService persistenceService,
      Microformats2ObjectFactory objectFactory) {
    return new GitLabPostClient(
        gitlabConfiguration, requestSpecificationFactory, persistenceService, objectFactory);
  }

  @Bean
  public GitLabMediaClient mediaGitLabClient(
      @Qualifier("mediaGitlabConfiguration") AbstractGitLabClient.Configuration gitlabConfiguration,
      RequestSpecificationFactory requestSpecificationFactory,
      MediaTypeHelper mediaTypeHelper) {
    return new GitLabMediaClient(gitlabConfiguration, requestSpecificationFactory, mediaTypeHelper);
  }
}
