package me.jvt.www.api.micropub.sanitiser;

import java.net.MalformedURLException;
import java.net.URL;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;

public class TwitterUrlSanitiser implements Microformats2ObjectSanitiser {

  @Override
  public Microformats2Object sanitise(Microformats2Object mf2) {
    if (null != mf2.getProperties().getLikeOf() && 0 != mf2.getProperties().getLikeOf().size()) {
      mf2.getProperties()
          .getLikeOf()
          .set(0, sanitiseTwitterUrl(mf2.getProperties().getLikeOf().get(0)));
    } else if (null != mf2.getProperties().getInReplyTo()
        && 0 != mf2.getProperties().getInReplyTo().size()) {
      mf2.getProperties()
          .getInReplyTo()
          .set(0, sanitiseTwitterUrl(mf2.getProperties().getInReplyTo().get(0)));
    } else if (null != mf2.getProperties().getRepostOf()
        && 0 != mf2.getProperties().getRepostOf().size()) {
      mf2.getProperties()
          .getRepostOf()
          .set(0, sanitiseTwitterUrl(mf2.getProperties().getRepostOf().get(0)));
    }

    return mf2;
  }

  private String sanitiseTwitterUrl(String url) {
    try {
      URL parsedUrl = new URL(url);
      if ("twitter.com".equals(parsedUrl.getHost())) {
        return url.split("\\?")[0];
        // TODO: trim querystring
      }
    } catch (MalformedURLException e) {
      throw new InvalidMicropubMetadataRequest("The URL was invalid", e);
    }

    return url;
  }
}
