package me.jvt.www.api.micropub.sanitiser;

import java.util.HashSet;
import java.util.Set;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.posttype.MicroformatsProperty;
import org.springframework.stereotype.Component;

/**
 * A {@link Microformats2ObjectSanitiser} that removes any properties from the {@link
 * Microformats2Object} that are not supported by its {@link me.jvt.www.api.micropub.model.Kind}, to
 * avoid properties that may break / confuse the consumer - be that my site, or a client.
 *
 * <p>Should be executed as late as possible before saving the object.
 */
@Component
public class SupportedPropertiesSanitiser implements Microformats2ObjectSanitiser {
  @Override
  public Microformats2Object sanitise(Microformats2Object mf2) {
    propertiesToRemove(mf2.getKind(), mf2.getProperties());
    return mf2;
  }

  private void propertiesToRemove(Kind k, Properties old) {
    Set<String> validProperties = new HashSet<>();

    for (MicroformatsProperty property : k.getRequiredProperties()) {
      Object value = old.get(property.getName());
      if (null != value) {
        validProperties.add(property.getName());
      }
    }

    for (MicroformatsProperty property : k.getOptionalProperties()) {
      Object value = old.get(property.getName());
      if (null != value) {
        validProperties.add(property.getName());
      }
    }

    old.keySet().removeIf(p -> !validProperties.contains(p));
  }
}
