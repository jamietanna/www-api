package me.jvt.www.api.micropub.model.posttype;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum MicroformatsProperty {
  NAME("name", "Title"),
  NICKNAME("nickname"),
  PHOTO("photo", Collections.singletonList(Hint.URL)),
  PUBLISHED("published", "Date Published", Collections.singletonList(Hint.DATE)),
  CATEGORY("category", "Category / tag", Collections.singletonList(Hint.MULTIVALUE)),
  RSVP("rsvp", "RSVP"),
  CONTENT("content", "content", Collections.singletonList(Hint.MULTILINE)),
  SYNDICATION("syndication", "Syndication URL", Arrays.asList(Hint.MULTIVALUE, Hint.URL)),
  SUMMARY("summary"),
  END("end", "End Date", Collections.singletonList(Hint.DATE)),
  START("start", "Start Date", Collections.singletonList(Hint.DATE)),
  NUM("num", "Number"),
  UNIT("unit"),
  URL("url", Collections.singletonList(Hint.URL)),
  REL_TWITTER("rel=twitter", "Twitter username"),
  REPOST_OF("repost-of", "URL to be Reposted", Collections.singletonList(Hint.URL)),
  LIKE_OF("like-of", "URL to Like", Collections.singletonList(Hint.URL)),
  IN_REPLY_TO("in-reply-to", "URL to Reply To", Collections.singletonList(Hint.URL)),
  BOOKMARK_OF("bookmark-of", "URL to Bookmark", Collections.singletonList(Hint.URL)),
  READ_STATUS("read-status", "Reading Status"),
  READ_OF("read-of", "URL to Mark Reading Status Of", Collections.singletonList(Hint.URL)),
  POST_STATUS("post-status", "Whether the post is `published` or `draft`"),
  CODE_LICENSE("code-license", "The license for all code snippets in the post"),
  PROSE_LICENSE("prose-license", "The license for all written content in the post"),
  SERIES("series", "A set of posts with a common theme / a set order"),
  FEATURED("featured", "The featured image to be used for "),
  LISTEN_OF("listen-of", Collections.singletonList(Hint.URL));

  private final String name;
  private final String displayName;
  private final List<Hint> hints;

  MicroformatsProperty(String name) {
    this.name = name;
    this.displayName = name.substring(0, 1).toUpperCase() + name.substring(1);
    this.hints = Collections.emptyList();
  }

  MicroformatsProperty(String name, List<Hint> hints) {
    this.name = name;
    this.displayName = name.substring(0, 1).toUpperCase() + name.substring(1);
    this.hints = hints;
  }

  MicroformatsProperty(String name, String displayName) {
    this.name = name;
    this.displayName = displayName;
    this.hints = Collections.emptyList();
  }

  MicroformatsProperty(String name, String displayName, List<Hint> hints) {
    this.name = name;
    this.displayName = displayName;
    this.hints = hints;
  }

  public String getName() {
    return name;
  }

  public String getDisplayName() {
    return displayName;
  }

  public List<Hint> getHints() {
    return hints;
  }
}
