package me.jvt.www.api.micropub.validator.property;

import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.validator.field.UrlValidator;

public class UrlPropertyValidator
    implements ConstraintValidator<java.lang.annotation.Annotation, Microformats2Object> {

  private final String property;
  private final UrlValidator urlValidator;

  public UrlPropertyValidator(String property, UrlValidator urlValidator) {
    this.property = property;
    this.urlValidator = urlValidator;
  }

  @Override
  public boolean isValid(Microformats2Object mf2, ConstraintValidatorContext context) {
    List<String> propertyValue = (List<String>) mf2.getProperties().get(property);

    if (null == propertyValue
        || propertyValue.isEmpty()
        || null == propertyValue.get(0)
        || propertyValue.get(0).isEmpty()) {
      context
          .buildConstraintViolationWithTemplate(
              String.format("Property `%s` was not set", property))
          .addConstraintViolation();
      return false;
    }

    return urlValidator.isValid(propertyValue.get(0), context);
  }
}
