package me.jvt.www.api.micropub.presentation.rest;

import java.util.Optional;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.MicropubSourceDto;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.request.MicropubRequest;
import me.jvt.www.api.micropub.model.request.MicropubRequest.MicropubAction;
import me.jvt.www.api.micropub.service.MicropubRequestService;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class MicropubController {

  private static final String CREATE_HTML_BODY =
      "<html><body><p>The post has been created at <a href=\"%s\">%s</a>. You can retrieve the metadata using <a href=\"/micropub?q=source&url=%s\">q=source</a>.</p></body></html>";
  private static final String DELETE_HTML_BODY =
      "<html><body><p>The post <a href=\"%s\">%s</a> has been deleted.</p></body></html>";
  private static final String UNDELETE_HTML_BODY =
      "<html><body><p>The post <a href=\"%s\">%s</a> has been undeleted.</p></body></html>";
  private static final String UPDATE_HTML_BODY =
      "<html><body><p>The post at <a href=\"%s\">%s</a> has been updated.</p></body></html>";

  private final MicropubRequestService delegate;
  private final String micropubRedirectUrlFormat;

  public MicropubController(
      MicropubRequestService delegate,
      @Value("${micropub.redirect.url.format}") String micropubRedirectUrlFormat) {
    this.delegate = delegate;
    this.micropubRedirectUrlFormat = micropubRedirectUrlFormat;
  }

  @PostMapping(path = "/micropub", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  @PreAuthorize(
      "@profileUrlValidator.isValidProfileUrl(authentication.name) && ("
          + "   (#micropubRequest.getAction().name() == 'CREATE' && hasAuthority('SCOPE_create'))"
          + "|| (#micropubRequest.getAction().name() == 'DELETE' && hasAuthority('SCOPE_delete') && !hasAuthority('SCOPE_draft'))"
          + "|| (#micropubRequest.getAction().name() == 'UNDELETE' && hasAuthority('SCOPE_undelete') && !hasAuthority('SCOPE_draft'))"
          + ")")
  public ResponseEntity<?> micropubForm(
      MicropubRequest micropubRequest, Authentication authentication) {
    if (MicropubAction.UPDATE.equals(micropubRequest.getAction())) {
      throw new InvalidMicropubMetadataRequest("Update is not supported on Micropub form requests");
    }

    return handleMicropubRequest(micropubRequest, authentication);
  }

  @PostMapping(
      path = "/micropub",
      consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
      produces = MediaType.TEXT_HTML_VALUE)
  @PreAuthorize(
      "@profileUrlValidator.isValidProfileUrl(authentication.name) && ("
          + "   (#micropubRequest.getAction().name() == 'CREATE' && hasAuthority('SCOPE_create'))"
          + "|| (#micropubRequest.getAction().name() == 'DELETE' && hasAuthority('SCOPE_delete') && !hasAuthority('SCOPE_draft'))"
          + "|| (#micropubRequest.getAction().name() == 'UNDELETE' && hasAuthority('SCOPE_undelete') && !hasAuthority('SCOPE_draft'))"
          + ")")
  public String micropubFormAsHtml(MicropubRequest micropubRequest, Authentication authentication) {
    return handleMicropubRequestHtml(micropubRequest, authentication);
  }

  @PostMapping(path = "/micropub", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  @PreAuthorize(
      "@profileUrlValidator.isValidProfileUrl(authentication.name) && ("
          + "   (#micropubRequest.getAction().name() == 'CREATE' && hasAuthority('SCOPE_create'))"
          + "|| (#micropubRequest.getAction().name() == 'DELETE' && hasAuthority('SCOPE_delete') && !hasAuthority('SCOPE_draft'))"
          + "|| (#micropubRequest.getAction().name() == 'UNDELETE' && hasAuthority('SCOPE_undelete') && !hasAuthority('SCOPE_draft'))"
          + ")")
  public ResponseEntity<?> micropubMultiPartForm(
      MicropubRequest micropubRequest,
      Authentication authentication,
      @SuppressWarnings("OptionalUsedAsFieldOrParameterType") Optional<MultipartFile> file) {
    if (file.isPresent()) {
      throw new InvalidMicropubMetadataRequest("Files must be uploaded to the media endpoint");
    }

    if (MicropubAction.UPDATE.equals(micropubRequest.getAction())) {
      throw new InvalidMicropubMetadataRequest("Update is not supported on Micropub form requests");
    }

    return handleMicropubRequest(micropubRequest, authentication);
  }

  @PostMapping(
      path = "/micropub",
      consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
      produces = MediaType.TEXT_HTML_VALUE)
  @PreAuthorize(
      "@profileUrlValidator.isValidProfileUrl(authentication.name) && ("
          + "   (#micropubRequest.getAction().name() == 'CREATE' && hasAuthority('SCOPE_create'))"
          + "|| (#micropubRequest.getAction().name() == 'DELETE' && hasAuthority('SCOPE_delete') && !hasAuthority('SCOPE_draft'))"
          + "|| (#micropubRequest.getAction().name() == 'UNDELETE' && hasAuthority('SCOPE_undelete') && !hasAuthority('SCOPE_draft'))"
          + ")")
  public String micropubMultiPartFormAsHtml(
      MicropubRequest micropubRequest,
      Authentication authentication,
      @SuppressWarnings("OptionalUsedAsFieldOrParameterType") Optional<MultipartFile> file) {
    if (file.isPresent()) {
      throw new InvalidMicropubMetadataRequest("Files must be uploaded to the media endpoint");
    }

    if (MicropubAction.UPDATE.equals(micropubRequest.getAction())) {
      throw new InvalidMicropubMetadataRequest("Update is not supported on Micropub form requests");
    }

    return handleMicropubRequestHtml(micropubRequest, authentication);
  }

  @PostMapping(path = "/micropub", consumes = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize(
      "@profileUrlValidator.isValidProfileUrl(authentication.name) && ("
          + "   (#request.getAction().name() == 'CREATE' && hasAuthority('SCOPE_create'))"
          + "|| (#request.getAction().name() == 'DELETE' && hasAuthority('SCOPE_delete') && !hasAuthority('SCOPE_draft'))"
          + "|| (#request.getAction().name() == 'UNDELETE' && hasAuthority('SCOPE_undelete') && !hasAuthority('SCOPE_draft'))"
          + "|| (#request.getAction().name() == 'UPDATE' && hasAuthority('SCOPE_update'))"
          + ")")
  public ResponseEntity<?> json(MicropubRequest request, Authentication authentication) {
    return handleMicropubRequest(request, authentication);
  }

  @PostMapping(
      path = "/micropub",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.TEXT_HTML_VALUE)
  @PreAuthorize(
      "@profileUrlValidator.isValidProfileUrl(authentication.name) && ("
          + "   (#request.getAction().name() == 'CREATE' && hasAuthority('SCOPE_create'))"
          + "|| (#request.getAction().name() == 'DELETE' && hasAuthority('SCOPE_delete') && !hasAuthority('SCOPE_draft'))"
          + "|| (#request.getAction().name() == 'UNDELETE' && hasAuthority('SCOPE_undelete') && !hasAuthority('SCOPE_draft'))"
          + "|| (#request.getAction().name() == 'UPDATE' && hasAuthority('SCOPE_update') && !hasAuthority('SCOPE_draft'))"
          + ")")
  public String jsonAsHtml(MicropubRequest request, Authentication authentication) {
    return handleMicropubRequestHtml(request, authentication);
  }

  private ResponseEntity<?> handleMicropubRequest(
      MicropubRequest micropubRequest, Authentication authentication) {
    MicropubRequestService.MicropubResponse response =
        delegate.handle(micropubRequest, authentication);
    switch (micropubRequest.getAction()) {
      case CREATE:
        MicropubRequestService.CreateResponse createResponse =
            (MicropubRequestService.CreateResponse) response;
        return ResponseEntity.accepted()
            .header("Location", redirectedUrl(createResponse.getSlug()))
            .build();
      case DELETE:
      case UNDELETE:
        return ResponseEntity.noContent().build();
      case UPDATE:
        MicropubRequestService.UpdateResponse updateResponse =
            (MicropubRequestService.UpdateResponse) response;
        Microformats2Object mf2 = updateResponse.getNewObject();
        MicropubSourceDto actualResponse = new MicropubSourceDto();
        actualResponse.setType(SingletonListHelper.singletonList(mf2.getKind().getH()));
        actualResponse.setProperties(mf2.getProperties());
        return new ResponseEntity<>(actualResponse, HttpStatus.OK);
      default:
        throw new IllegalStateException(
            String.format("Action %s is not supported", micropubRequest.getAction()));
    }
  }

  private String handleMicropubRequestHtml(
      MicropubRequest micropubRequest, Authentication authentication) {
    MicropubRequestService.MicropubResponse response =
        delegate.handle(micropubRequest, authentication);
    switch (micropubRequest.getAction()) {
      case CREATE:
        MicropubRequestService.CreateResponse createResponse =
            (MicropubRequestService.CreateResponse) response;
        String url = redirectedUrl(createResponse.getSlug());
        return String.format(CREATE_HTML_BODY, url, url, url);
      case DELETE:
      case UNDELETE:
      case UPDATE:
        return htmlBody(micropubRequest.getAction(), micropubRequest.getContext().getUrl());
      default:
        throw new IllegalStateException(
            String.format("Action %s is not supported", micropubRequest.getAction()));
    }
  }

  private String redirectedUrl(String slug) {
    return String.format(micropubRedirectUrlFormat, slug);
  }

  private String htmlBody(MicropubAction action, String url) {
    switch (action) {
      case DELETE:
        return String.format(DELETE_HTML_BODY, url, url);
      case UNDELETE:
        return String.format(UNDELETE_HTML_BODY, url, url);
      case UPDATE:
        return String.format(UPDATE_HTML_BODY, url, url);
      default:
        throw new IllegalStateException(String.format("Action %s is not supported", action));
    }
  }
}
