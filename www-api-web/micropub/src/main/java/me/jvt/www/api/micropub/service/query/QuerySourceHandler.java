package me.jvt.www.api.micropub.service.query;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import me.jvt.www.api.micropub.exception.InvalidMicropubMetadataRequest;
import me.jvt.www.api.micropub.model.MicropubQueryDto;
import me.jvt.www.api.micropub.model.MicropubSourceDto;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.service.MicropubService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

@Component
public class QuerySourceHandler implements QueryHandler {

  private final MicropubService delegate;

  public QuerySourceHandler(MicropubService delegate) {
    this.delegate = delegate;
  }

  @Override
  public String getQuery() {
    return "source";
  }

  @Override
  public MicropubQueryDto handle(
      MultiValueMap<String, String> parameters, Optional<Authentication> maybeAuthentication) {
    if (!parameters.containsKey("url")) {

      return delegate.retrieveAll();
    }

    String url = parameters.getFirst("url");
    if (null == url || url.isEmpty()) {
      throw new InvalidMicropubMetadataRequest("URL parameter required to have a value");
    }

    Optional<Microformats2Object> maybeMf2 =
        delegate.retrieve(url, canClientViewDeleted(maybeAuthentication));
    if (!maybeMf2.isPresent()) {
      throw new InvalidMicropubMetadataRequest(String.format("Post %s does not exist", url));
    }

    Microformats2Object mf2 = maybeMf2.get();

    String properties = parameters.getFirst("properties");
    if (null != properties) {
      return handle(mf2, Collections.singletonList(properties));
    }

    if (null != parameters.get("properties[]")) {
      return handle(mf2, parameters.get("properties[]"));
    }

    return handle(mf2);
  }

  private MicropubSourceDto handle(Microformats2Object mf2) {
    MicropubSourceDto dto = new MicropubSourceDto();

    dto.setProperties(mf2.getProperties());
    dto.setType(Collections.singletonList(mf2.getKind().getH()));

    return dto;
  }

  private MicropubQueryDto handle(Microformats2Object mf2, List<String> properties) {
    MicropubSourceDto dto = new MicropubSourceDto();

    dto.setProperties(new Properties());

    for (String property : properties) {
      Object value = mf2.getProperties().get(property);
      if (null != value) {
        dto.getProperties().put(property, value);
      }
    }

    return dto;
  }

  private boolean canClientViewDeleted(Optional<Authentication> maybeAuthentication) {
    return maybeAuthentication
        .map(value -> value.getAuthorities().contains(new SimpleGrantedAuthority("SCOPE_undelete")))
        .orElse(false);
  }
}
