package me.jvt.www.api.micropub.client;

import com.fasterxml.jackson.databind.JsonNode;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.micropub.model.gitlab.CreateCommitRequestBody;
import me.jvt.www.api.micropub.model.gitlab.CreateCommitRequestBody.Action;
import me.jvt.www.api.micropub.model.gitlab.CreateCommitRequestBody.Action.ActionType;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object.Context;
import me.jvt.www.api.micropub.model.posttype.Microformats2ObjectFactory;
import me.jvt.www.api.micropub.service.PersistenceService;
import me.jvt.www.api.micropub.service.PersistenceService.PersistenceAction;
import org.springframework.http.HttpStatus;

public class GitLabPostClient extends AbstractGitLabClient implements PostClient {

  private final PersistenceService factory;

  private final Microformats2ObjectFactory objectFactory;

  public GitLabPostClient(
      Configuration configuration,
      RequestSpecificationFactory requestSpecificationFactory,
      PersistenceService persistenceService,
      Microformats2ObjectFactory objectFactory) {
    super(configuration, requestSpecificationFactory);
    this.factory = persistenceService;
    this.objectFactory = objectFactory;
  }

  public String addContent(Microformats2Object mf2) {
    CreateCommitRequestBody commitRequestBody = new CreateCommitRequestBody();
    boolean hasPostBeenCreated = false;
    PersistenceAction persistenceAction = null;
    while (!hasPostBeenCreated) {
      persistenceAction = factory.determineAction(mf2);
      String content = factory.serialize(persistenceAction, mf2);

      commitRequestBody.setBranch("main");

      Action action = new Action();
      action.setAction(ActionType.CREATE);
      action.setFilePath(persistenceAction.getPath());
      commitRequestBody.setCommitMessage(
          String.format("MP: Add %s", persistenceAction.getCommitMessage()));
      Response gitLabResponse = sendPost(commitRequestBody, content, action);
      if (HttpStatus.CREATED.value() == gitLabResponse.getStatusCode()) {
        hasPostBeenCreated = true;
      } else if (!isConflict(gitLabResponse)) {
        throw new RuntimeException(
            String.format(
                "Was unable to process the request: HTTP %d returned from GitLab",
                gitLabResponse.getStatusCode()));
      }
    }
    return persistenceAction.getUrlPath();
  }

  public String updateContent(Microformats2Object mf2) {
    CreateCommitRequestBody commitRequestBody = new CreateCommitRequestBody();
    commitRequestBody.setBranch("main");

    PersistenceAction persistenceAction = factory.determineAction(mf2);
    String content = factory.serialize(persistenceAction, mf2);

    Action action = new Action();
    action.setAction(ActionType.UPDATE);
    action.setFilePath(persistenceAction.getPath());
    commitRequestBody.setCommitMessage(
        String.format("MP: Update %s", persistenceAction.getCommitMessage()));

    Response gitLabResponse = sendPost(commitRequestBody, content, action);
    if (HttpStatus.CREATED.value() == gitLabResponse.getStatusCode()) {
      return persistenceAction.getUrlPath();
    } else {
      throw new RuntimeException(
          String.format(
              "Was unable to process the request: HTTP %d returned from GitLab",
              gitLabResponse.getStatusCode()));
    }
  }

  @Override
  public int upsertCite(Context context) {
    JsonNode cite = context.getContext();
    if (null == cite || cite.isEmpty()) {
      return 0;
    }
    List<PersistenceAction> persistenceActions = factory.determineAction(cite);
    List<String> serialisations = factory.serialize(cite);

    List<Action> actions = new ArrayList<>();
    for (int i = 0; i < persistenceActions.size(); i++) {
      Action action = new Action();
      String filePath = persistenceActions.get(i).getPath();
      if (isFilePresent(filePath)) {
        action.setAction(ActionType.UPDATE);
      } else {
        action.setAction(ActionType.CREATE);
      }
      action.setContent(serialisations.get(i) + "\n");
      action.setFilePath(filePath);
      actions.add(action);
    }

    CreateCommitRequestBody commitRequestBody = new CreateCommitRequestBody();
    commitRequestBody.setBranch("main");
    if (1 == persistenceActions.size()) {
      commitRequestBody.setCommitMessage("MP: Upsert 1 cite");
    } else {
      commitRequestBody.setCommitMessage(
          String.format("MP: Upsert %d cites", persistenceActions.size()));
    }

    Response gitLabResponse = sendPost(commitRequestBody, actions);
    if (HttpStatus.CREATED.value() == gitLabResponse.getStatusCode()) {
      return actions.size();
    } else {
      throw new RuntimeException(
          String.format(
              "Was unable to process the request: HTTP %d returned from GitLab",
              gitLabResponse.getStatusCode()));
    }
  }

  public Optional<Microformats2Object> getContent(String filePath) {
    Response response = getFile(filePath, true);
    if (HttpStatus.OK.value() == response.getStatusCode()) {
      return Optional.of(objectFactory.deserialize(response.asString()));
    } else if (HttpStatus.NOT_FOUND.value() == response.getStatusCode()) {
      return Optional.empty();
    } else {
      throw new RuntimeException(
          String.format(
              "Was unable to process the request: HTTP %d returned from GitLab",
              response.getStatusCode()));
    }
  }

  private Response getFile(String filePath, boolean rawContent) {
    RequestSpecification request = requestSpecificationFactory.newRequestSpecification();
    String encodedFilePath;
    try {
      encodedFilePath = URLEncoder.encode(filePath, StandardCharsets.UTF_8.toString());
    } catch (UnsupportedEncodingException e) {
      throw new RuntimeException(e);
    }

    String apiUrl;
    if (rawContent) {
      apiUrl = "/projects/%s/repository/files/%s/raw?ref=main";
    } else {
      apiUrl = "/projects/%s/repository/files/%s?ref=main";
    }

    return request
        .baseUri(this.configuration.getApiEndpoint())
        .header("PRIVATE-TOKEN", this.configuration.getAccessToken())
        .urlEncodingEnabled(false)
        .get(String.format(apiUrl, configuration.getProjectId(), encodedFilePath));
  }

  private boolean isFilePresent(String filePath) {
    Response response = getFile(filePath, false);
    return (HttpStatus.OK.value() == response.getStatusCode());
  }
}
