package me.jvt.www.api.micropub.model.posttype;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import java.util.List;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.Properties;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object.Context;
import me.jvt.www.api.micropub.model.request.MicropubRequest;

public class Microformats2ObjectFactory {

  private final ObjectMapper objectMapper;
  private final YAMLMapper yamlMapper;
  private final PostTypeDiscoverer discoverer;
  private final List<Microformats2ObjectParser> parsers;

  public Microformats2ObjectFactory(
      ObjectMapper objectMapper,
      YAMLMapper yamlMapper,
      PostTypeDiscoverer discoverer,
      List<Microformats2ObjectParser> parsers) {
    if (parsers.isEmpty()) {
      throw new IllegalArgumentException("Must provide at least one Microformats2ObjectParser");
    }
    this.objectMapper = objectMapper;
    this.yamlMapper = yamlMapper;
    this.discoverer = discoverer;
    this.parsers = parsers;
  }

  /**
   * Construct the relevant {@link Microformats2Object} for the provided {@link MicropubRequest}.
   *
   * @param request the request from the Micropub endpoint
   * @return a {@link Microformats2Object} representation
   * @throws IllegalStateException when calculated {@link Kind} is not supported
   */
  public Microformats2Object create(MicropubRequest request) {
    Context context = request.getContext().getContext();
    Properties properties = request.getContext().getProperties();
    Kind kind = discoverer.discover(context.getH(), properties);
    if (Kind.articles.equals(kind)) {
      return new HugoArticleMicroformats2Object(yamlMapper, properties, context);
    } else {
      return new HugoMicroformats2Json(objectMapper, discoverer, properties, context);
    }
  }

  public Microformats2Object deserialize(String post) {
    for (Microformats2ObjectParser parser : parsers) {
      try {
        return parser.deserialize(post);
      } catch (Exception e) {
        // ignore
      }
    }

    throw new IllegalStateException("Could not parse post");
  }
}
