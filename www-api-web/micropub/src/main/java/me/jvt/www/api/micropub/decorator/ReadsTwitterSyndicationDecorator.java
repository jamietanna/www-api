package me.jvt.www.api.micropub.decorator;

import me.jvt.www.api.micropub.actions.concrete.AddSyndicationPropertyAction;
import me.jvt.www.api.micropub.model.Kind;
import me.jvt.www.api.micropub.model.MicropubSyndicateTo;
import me.jvt.www.api.micropub.model.posttype.Microformats2Object;
import me.jvt.www.api.micropub.util.SingletonListHelper;
import org.springframework.stereotype.Component;

@Component
public class ReadsTwitterSyndicationDecorator implements Microformats2ObjectDecorator {

  @Override
  public Microformats2Object decorate(Microformats2Object mf2) {
    if (Kind.reads.equals(mf2.getKind())) {
      AddSyndicationPropertyAction action =
          new AddSyndicationPropertyAction(
              SingletonListHelper.singletonList(MicropubSyndicateTo.BRIDGY_TWITTER.getUid()));
      action.accept(mf2);
    }

    return mf2;
  }
}
