package me.jvt.www.api.micropub.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Set;

public class MicropubConfigDto extends MicropubQueryDto {
  @JsonInclude(Include.NON_NULL)
  @JsonProperty("syndicate-to")
  private Set<MicropubSyndicateTo> syndicateTo;

  @JsonInclude(Include.NON_NULL)
  @JsonProperty("media-endpoint")
  private String mediaEndpoint;

  @JsonInclude(Include.NON_NULL)
  @JsonProperty("q")
  private Set<String> queries;

  @JsonProperty("post-types")
  private List<MicropubPostTypeDto> postTypes;

  public Set<MicropubSyndicateTo> getSyndicateTo() {
    return syndicateTo;
  }

  public void setSyndicateTo(Set<MicropubSyndicateTo> syndicateTo) {
    this.syndicateTo = syndicateTo;
  }

  public String getMediaEndpoint() {
    return mediaEndpoint;
  }

  public void setMediaEndpoint(String mediaEndpoint) {
    this.mediaEndpoint = mediaEndpoint;
  }

  public Set<String> getQueries() {
    return queries;
  }

  public void setQueries(Set<String> queries) {
    this.queries = queries;
  }

  public List<MicropubPostTypeDto> getPostTypes() {
    return postTypes;
  }

  public void setPostTypes(List<MicropubPostTypeDto> postTypes) {
    this.postTypes = postTypes;
  }
}
