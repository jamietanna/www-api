package me.jvt.www.api.micropub.client;

import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.micropub.model.TaxonomiesResponse;

public class TaxonomiesClient extends WwwProxyClient<TaxonomiesResponse> {
  public TaxonomiesClient(RequestSpecificationFactory requestSpecificationFactory) {
    super(TaxonomiesResponse.class, requestSpecificationFactory, "/taxonomies.json");
  }
}
