package me.jvt.www.api.micropub.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MicropubErrorResponse {

  private String error;
  private String errorDescription;

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  @JsonProperty("error_description")
  public String getErrorDescription() {
    return errorDescription;
  }

  public void setErrorDescription(String errorDescription) {
    this.errorDescription = errorDescription;
  }
}
