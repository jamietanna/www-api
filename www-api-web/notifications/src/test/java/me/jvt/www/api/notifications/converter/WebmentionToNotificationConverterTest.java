package me.jvt.www.api.notifications.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.stream.Stream;
import me.jvt.www.api.notifications.model.webmentionio.WebhookBody;
import me.jvt.www.api.notifications.model.webmentionio.WebhookBody.Post;
import me.jvt.www.api.notifications.model.webmentionio.WebhookBody.Post.Author;
import me.jvt.www.api.pushover.model.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class WebmentionToNotificationConverterTest {
  WebmentionToNotificationConverter sut;
  WebhookBody webhookBody;

  @BeforeEach
  void setup() {
    sut = new WebmentionToNotificationConverter();
  }

  @ParameterizedTest
  @MethodSource("arguments")
  void convertsCorrectly(
      String authorName, String wmProperty, String expectedTitle, String expectedBody) {
    // given
    setupWebhookBody(authorName, wmProperty);

    // when
    Message actual = sut.convert(webhookBody);

    // then
    assertThat(actual.getTitle()).isEqualTo(expectedTitle);
    assertThat(actual.getMessage()).isEqualTo(expectedBody);
    assertThat(actual.getUrl()).isEqualTo("https://foo.bar");
  }

  @ParameterizedTest
  @ValueSource(strings = {"", "Jamie T"})
  void unmatchedThrowsIllegalArgumentException(String authorName) {
    // given
    setupWebhookBody(authorName, "wibble-to");

    // when
    assertThatThrownBy(
            () -> {
              sut.convert(webhookBody);
            })
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("wibble-to is not a valid wm-property");
  }

  private void setupWebhookBody(String authorName, String wmProperty) {
    webhookBody = new WebhookBody();
    Post post = new Post();

    Author author = new Author();
    author.setName(authorName);
    post.setAuthor(author);
    post.setWmProperty(wmProperty);
    post.setWmSource("https://foo.bar");
    post.setWmTarget("https://mine.baz");
    webhookBody.setPost(post);
  }

  // TODO: if profile URL, then just say "you"

  static Stream<Arguments> arguments() {
    return Stream.of(
        Arguments.of(
            "Jamie T",
            "bookmark-of",
            "Jamie T bookmarked your post",
            "New bookmark received from https://foo.bar on post https://mine.baz"),
        Arguments.of(
            "",
            "bookmark-of",
            "https://foo.bar bookmarked your post",
            "New bookmark received from https://foo.bar on post https://mine.baz"),
        Arguments.of(
            "Jamie T",
            "like-of",
            "Jamie T liked your post",
            "New like received from https://foo.bar on post https://mine.baz"),
        Arguments.of(
            "",
            "like-of",
            "https://foo.bar liked your post",
            "New like received from https://foo.bar on post https://mine.baz"),
        Arguments.of(
            "Jamie T",
            "mention-of",
            "Jamie T mentioned your post",
            "New mention received from https://foo.bar on post https://mine.baz"),
        Arguments.of(
            "",
            "mention-of",
            "https://foo.bar mentioned your post",
            "New mention received from https://foo.bar on post https://mine.baz"),
        Arguments.of(
            "Jamie T",
            "in-reply-to",
            "New reply from Jamie T",
            "New reply received from https://foo.bar on post https://mine.baz"),
        Arguments.of(
            "",
            "in-reply-to",
            "New reply from https://foo.bar",
            "New reply received from https://foo.bar on post https://mine.baz"),
        Arguments.of(
            "Jamie T",
            "repost-of",
            "New repost from Jamie T",
            "New repost received from https://foo.bar on post https://mine.baz"),
        Arguments.of(
            "",
            "repost-of",
            "New repost from https://foo.bar",
            "New repost received from https://foo.bar on post https://mine.baz"),
        Arguments.of(
            "Jamie T",
            "rsvp",
            "New RSVP from Jamie T",
            "New RSVP received from https://foo.bar on post https://mine.baz"),
        Arguments.of(
            "",
            "rsvp",
            "New RSVP from https://foo.bar",
            "New RSVP received from https://foo.bar on post https://mine.baz"));
  }
}
