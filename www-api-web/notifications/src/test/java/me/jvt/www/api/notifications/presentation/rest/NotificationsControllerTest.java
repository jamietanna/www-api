package me.jvt.www.api.notifications.presentation.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import me.jvt.www.api.notifications.converter.WebmentionToNotificationConverter;
import me.jvt.www.api.notifications.model.webmentionio.WebhookBody;
import me.jvt.www.api.notifications.security.WebmentionIoSharedSecretValidator;
import me.jvt.www.api.pushover.PushoverClient;
import me.jvt.www.api.pushover.model.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@WebMvcTest(NotificationsController.class)
class NotificationsControllerTest {

  @MockBean WebmentionIoSharedSecretValidator mockSharedSecretValidator;
  @MockBean WebmentionToNotificationConverter mockWebmentionToNotificationConverter;
  @MockBean PushoverClient mockPushoverClient;
  Message mockMessage;

  static ObjectMapper objectMapper = new ObjectMapper();

  @Autowired MockMvc mockMvc;

  ObjectNode request;

  @BeforeEach
  void setup() {
    mockMessage = mock(Message.class);
    when(mockWebmentionToNotificationConverter.convert(any())).thenReturn(mockMessage);
    when(mockSharedSecretValidator.isValid("abc-wibble")).thenReturn(true);
    request = objectMapper.createObjectNode();
    ObjectNode post = request.putObject("post");
    ObjectNode author = post.putObject("author");
    author.put("url", "https://example.com");
  }

  @Test
  void formBodyToWebmentionReturns400() throws Exception {
    // given

    // when
    this.mockMvc
        .perform(
            (post("/notifications/webmention").contentType(MediaType.APPLICATION_FORM_URLENCODED)))
        // then
        .andExpect(MockMvcResultMatchers.status().isUnsupportedMediaType())
        .andExpect(jsonPath("$.error").value("invalid_request"))
        .andExpect(jsonPath("$.error_description").value("Must be JSON formatted request"));
  }

  @Test
  void emptyPostToWebmentionReturns400() throws Exception {
    // given

    // when
    this.mockMvc
        .perform(
            (post("/notifications/webmention").content("").contentType(MediaType.APPLICATION_JSON)))
        // then
        .andExpect(MockMvcResultMatchers.status().isBadRequest())
        .andExpect(jsonPath("$.error").value("invalid_request"))
        .andExpect(jsonPath("$.error_description").value("Invalid POST request body sent"));
  }

  @Test
  void webmentionPostWithInvalidSecretReturns401() throws Exception {
    // given
    request.put("secret", "not-the-secret");

    // when
    this.mockMvc
        .perform(
            (post("/notifications/webmention")
                .content(objectMapper.writeValueAsBytes(request))
                .contentType(MediaType.APPLICATION_JSON)))
        // then
        .andExpect(MockMvcResultMatchers.status().isUnauthorized())
        .andExpect(jsonPath("$.error").value("invalid_request"))
        .andExpect(jsonPath("$.error_description").value("Invalid shared secret provided"));

    verify(mockSharedSecretValidator).isValid("not-the-secret");
  }

  @Test
  void webmentionPostWithValidSecretReturns202() throws Exception {
    // given
    request.put("secret", "abc-wibble");
    ObjectNode post = (ObjectNode) request.get("post");
    post.put("wm-source", "https://example.com/mf2/2020/01/tvzd8/");

    // when
    this.mockMvc
        .perform(
            (post("/notifications/webmention")
                .content(objectMapper.writeValueAsBytes(request))
                .contentType(MediaType.APPLICATION_JSON)))
        // then
        .andExpect(MockMvcResultMatchers.status().isAccepted());

    verify(mockSharedSecretValidator).isValid("abc-wibble");
  }

  @Test
  void webmentionPostWithValidSecretConvertsBodyToLink() throws Exception {
    // given
    request.put("secret", "abc-wibble");
    ObjectNode post = (ObjectNode) request.get("post");
    post.put("wm-property", "rsvp");
    post.put("wm-source", "https://example.com/mf2/2020/01/tvzd8/");

    // when
    this.mockMvc
        .perform(
            (post("/notifications/webmention")
                .content(objectMapper.writeValueAsBytes(request))
                .contentType(MediaType.APPLICATION_JSON)))
        // then
        .andExpect(MockMvcResultMatchers.status().isAccepted());

    ArgumentCaptor<WebhookBody> webhookBodyArgumentCaptor =
        ArgumentCaptor.forClass(WebhookBody.class);
    verify(mockWebmentionToNotificationConverter).convert(webhookBodyArgumentCaptor.capture());

    WebhookBody actual = webhookBodyArgumentCaptor.getValue();
    assertThat(actual.getPost().getWmProperty()).isEqualTo("rsvp");
    assertThat(actual.getPost().getWmSource()).isEqualTo("https://example.com/mf2/2020/01/tvzd8/");
  }

  @Test
  void webmentionPostWithValidSecretSendsMessageViaPushoverOnceConverted() throws Exception {
    // given
    request.put("secret", "abc-wibble");
    ObjectNode post = (ObjectNode) request.get("post");
    post.put("wm-property", "rsvp");
    post.put("wm-source", "https://example.com/mf2/2020/01/tvzd8/");

    // when
    this.mockMvc
        .perform(
            (post("/notifications/webmention")
                .content(objectMapper.writeValueAsBytes(request))
                .contentType(MediaType.APPLICATION_JSON)))
        // then
        .andExpect(MockMvcResultMatchers.status().isAccepted());

    verify(mockPushoverClient).sendMessage(mockMessage);
  }

  @Test
  void webmentionPostFromMyDomainDoesNotTriggerNotification() throws Exception {
    // given
    request.put("secret", "abc-wibble");
    ObjectNode post = (ObjectNode) request.get("post");
    post.put("wm-property", "rsvp");
    post.put("wm-source", "https://www.jvt.me/mf2/2020/01/tvzd8/");

    // when
    this.mockMvc
        .perform(
            (post("/notifications/webmention")
                .content(objectMapper.writeValueAsBytes(request))
                .contentType(MediaType.APPLICATION_JSON)))
        // then
        .andExpect(MockMvcResultMatchers.status().isAccepted());

    verify(mockPushoverClient, times(0)).sendMessage(mockMessage);
  }

  @ValueSource(strings = {"https://twitter.com/jamietanna", "https://twitter.com/JamieTanna"})
  @ParameterizedTest
  void webmentionPostFromMyTwitterDoesNotTriggerNotification(String authorUrl) throws Exception {
    // given
    request.put("secret", "abc-wibble");
    ObjectNode post = (ObjectNode) request.get("post");
    post.put("wm-source", "https://example.com/mf2/2020/01/tvzd8/");
    ObjectNode author = (ObjectNode) post.get("author");
    author.put("url", authorUrl);

    // when
    this.mockMvc
        .perform(
            (post("/notifications/webmention")
                .content(objectMapper.writeValueAsBytes(request))
                .contentType(MediaType.APPLICATION_JSON)))
        // then
        .andExpect(MockMvcResultMatchers.status().isAccepted());

    verify(mockPushoverClient, times(0)).sendMessage(mockMessage);
  }

  @Test
  void webmentionFromTwitterLikeDoesNotTriggerNotification() throws Exception {
    // given
    request.put("secret", "abc-wibble");
    ObjectNode post = (ObjectNode) request.get("post");
    post.put("wm-source", "https://brid.gy/like/twitter/JamieTanna/1384174802802335750/70655349");

    // when
    this.mockMvc
        .perform(
            (post("/notifications/webmention")
                .content(objectMapper.writeValueAsBytes(request))
                .contentType(MediaType.APPLICATION_JSON)))
        // then
        .andExpect(MockMvcResultMatchers.status().isAccepted());

    verify(mockPushoverClient, times(0)).sendMessage(mockMessage);
  }
}
