package me.jvt.www.api.notifications.security;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class WebmentionIoSharedSecretValidatorImplTest {

  WebmentionIoSharedSecretValidatorImpl sut;

  @BeforeEach
  void setup() {
    sut = new WebmentionIoSharedSecretValidatorImpl("secret");
  }

  @Test
  void validSecretValidates() {
    assertThat(sut.isValid("secret")).isTrue();
  }

  @ParameterizedTest
  @ValueSource(strings = {"Secret", " secret", "secret ", "another-secret"})
  void invalidMatchesDoNotValidate(String invalid) {
    assertThat(sut.isValid(invalid)).isFalse();
  }
}
