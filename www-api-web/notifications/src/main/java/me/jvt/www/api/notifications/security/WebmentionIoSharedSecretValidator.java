package me.jvt.www.api.notifications.security;

public interface WebmentionIoSharedSecretValidator {
  boolean isValid(String sharedSecret);
}
