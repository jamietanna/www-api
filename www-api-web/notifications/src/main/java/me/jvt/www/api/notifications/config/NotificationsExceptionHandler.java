package me.jvt.www.api.notifications.config;

import me.jvt.www.api.notifications.exception.InvalidSharedSecretException;
import me.jvt.www.api.notifications.model.response.ErrorResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class NotificationsExceptionHandler extends ResponseEntityExceptionHandler {

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(
      HttpMessageNotReadableException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.error = "invalid_request";
    errorResponse.errorDescription = "Invalid POST request body sent";
    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }

  @Override
  protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
      HttpMediaTypeNotSupportedException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.error = "invalid_request";
    errorResponse.errorDescription = "Must be JSON formatted request";
    return new ResponseEntity<>(errorResponse, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
  }

  @ExceptionHandler(InvalidSharedSecretException.class)
  public ResponseEntity<ErrorResponse> handleInvalidSharedSecretException(
      InvalidSharedSecretException e, WebRequest request) {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.error = "invalid_request";
    errorResponse.errorDescription = "Invalid shared secret provided";
    return new ResponseEntity<>(errorResponse, HttpStatus.UNAUTHORIZED);
  }
}
