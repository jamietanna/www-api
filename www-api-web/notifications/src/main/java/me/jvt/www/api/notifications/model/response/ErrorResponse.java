package me.jvt.www.api.notifications.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorResponse {
  public String error;

  @JsonProperty("error_description")
  public String errorDescription;
}
