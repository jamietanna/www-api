package me.jvt.www.api.notifications.model.webmentionio;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WebhookBody {
  private String secret;

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public Post getPost() {
    return post;
  }

  public void setPost(Post post) {
    this.post = post;
  }

  private String source;
  private String target;
  private Post post;

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Post {
    private Author author;

    @JsonProperty("wm-property")
    private String wmProperty;

    @JsonProperty("wm-source")
    private String wmSource;

    public Author getAuthor() {
      return author;
    }

    public void setAuthor(Author author) {
      this.author = author;
    }

    public String getWmProperty() {
      return wmProperty;
    }

    public void setWmProperty(String wmProperty) {
      this.wmProperty = wmProperty;
    }

    public String getWmSource() {
      return wmSource;
    }

    public void setWmSource(String wmSource) {
      this.wmSource = wmSource;
    }

    public String getWmTarget() {
      return wmTarget;
    }

    public void setWmTarget(String wmTarget) {
      this.wmTarget = wmTarget;
    }

    @JsonProperty("wm-target")
    private String wmTarget;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Author {
      private String name;
      private String type;
      private String url;

      public String getType() {
        return type;
      }

      public void setType(String type) {
        this.type = type;
      }

      public String getName() {
        return name;
      }

      public void setName(String name) {
        this.name = name;
      }

      public String getUrl() {
        return url;
      }

      public void setUrl(String url) {
        this.url = url;
      }
    }
  }

  public String getSecret() {
    return secret;
  }

  public void setSecret(String secret) {
    this.secret = secret;
  }
}
