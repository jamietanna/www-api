package me.jvt.www.api.notifications.presentation.rest;

import java.net.MalformedURLException;
import java.net.URL;
import me.jvt.www.api.notifications.converter.WebmentionToNotificationConverter;
import me.jvt.www.api.notifications.exception.InvalidSharedSecretException;
import me.jvt.www.api.notifications.model.webmentionio.WebhookBody;
import me.jvt.www.api.notifications.security.WebmentionIoSharedSecretValidator;
import me.jvt.www.api.pushover.PushoverClient;
import me.jvt.www.api.pushover.model.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NotificationsController {

  private static final String SOURCE_DOMAIN_TO_NOT_SEND_WEBMENTIONS_FROM = "www.jvt.me";
  private static final String TWITTER_URL_TO_NOT_SEND_WEBMENTIONS_FROM =
      "https://twitter.com/jamietanna";
  private static final String TWITTER_LIKE_URL_PREFIX = "https://brid.gy/like/twitter/";

  private final WebmentionIoSharedSecretValidator webmentionIoSharedSecretValidator;
  private final WebmentionToNotificationConverter webmentionToNotificationConverter;
  private final PushoverClient pushoverClient;

  public NotificationsController(
      WebmentionIoSharedSecretValidator webmentionIoSharedSecretValidator,
      WebmentionToNotificationConverter webmentionToNotificationConverter,
      PushoverClient pushoverClient) {
    this.webmentionIoSharedSecretValidator = webmentionIoSharedSecretValidator;
    this.webmentionToNotificationConverter = webmentionToNotificationConverter;
    this.pushoverClient = pushoverClient;
  }

  @PostMapping(value = "/notifications/webmention", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> webmention(@RequestBody WebhookBody webhookBody) {
    if (!webmentionIoSharedSecretValidator.isValid(webhookBody.getSecret())) {
      throw new InvalidSharedSecretException();
    }

    if (ignoreWebmention(webhookBody)) {
      return new ResponseEntity<>("", HttpStatus.ACCEPTED);
    }

    Message message = webmentionToNotificationConverter.convert(webhookBody);
    pushoverClient.sendMessage(message);

    return new ResponseEntity<>("", HttpStatus.ACCEPTED);
  }

  private boolean ignoreWebmention(WebhookBody webhookBody) {
    URL sourceUrl;
    try {
      sourceUrl = new URL(webhookBody.getPost().getWmSource());
    } catch (MalformedURLException e) {
      throw new RuntimeException(e); // TODO
    }

    // don't send a push notification if it's a self-webmention as I don't really about myself
    // mentioning me!
    if (SOURCE_DOMAIN_TO_NOT_SEND_WEBMENTIONS_FROM.equals(sourceUrl.getHost())) {
      return true;
    }

    // don't send a push notification if it's myself on Twitter referencing my post(s)
    if (TWITTER_URL_TO_NOT_SEND_WEBMENTIONS_FROM.equalsIgnoreCase(
        webhookBody.getPost().getAuthor().getUrl())) {
      return true;
    }

    // twitter likes of posts can be quite noisy, and although they'll show up in my
    // dashboard/notifications feed, they shouldn't push notify me, especially as I get Twitter's
    // notifications too
    if (sourceUrl.toString().startsWith(TWITTER_LIKE_URL_PREFIX)) {
      return true;
    }

    return false;
  }
}
