package me.jvt.www.api.notifications.security;

public class WebmentionIoSharedSecretValidatorImpl implements WebmentionIoSharedSecretValidator {

  private final String sharedSecret;

  public WebmentionIoSharedSecretValidatorImpl(String sharedSecret) {
    this.sharedSecret = sharedSecret;
  }

  @Override
  public boolean isValid(String sharedSecret) {
    return this.sharedSecret.equals(sharedSecret);
  }
}
