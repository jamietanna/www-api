package me.jvt.www.api.notifications.converter;

import me.jvt.www.api.notifications.model.webmentionio.WebhookBody;
import me.jvt.www.api.notifications.model.webmentionio.WebhookBody.Post;
import me.jvt.www.api.notifications.model.webmentionio.WebhookBody.Post.Author;
import me.jvt.www.api.pushover.model.Message;

public class WebmentionToNotificationConverter {
  public Message convert(WebhookBody webhookBody) {
    Post thePost = webhookBody.getPost();

    String title = null;
    String message = null;
    String url = thePost.getWmSource();

    String author;
    Author theAuthor = thePost.getAuthor();
    if (theAuthor.getName().isEmpty()) {
      author = thePost.getWmSource();
    } else {
      author = theAuthor.getName();
    }

    switch (thePost.getWmProperty()) {
      case "bookmark-of":
        title = String.format("%s bookmarked your post", author);
        message =
            String.format(
                "New bookmark received from %s on post %s",
                thePost.getWmSource(), thePost.getWmTarget());
        break;
      case "like-of":
        title = String.format("%s liked your post", author);
        message =
            String.format(
                "New like received from %s on post %s",
                thePost.getWmSource(), thePost.getWmTarget());
        break;
      case "mention-of":
        title = String.format("%s mentioned your post", author);
        message =
            String.format(
                "New mention received from %s on post %s",
                thePost.getWmSource(), thePost.getWmTarget());
        break;
      case "in-reply-to":
        title = String.format("New reply from %s", author);
        message =
            String.format(
                "New reply received from %s on post %s",
                thePost.getWmSource(), thePost.getWmTarget());
        break;
      case "repost-of":
        title = String.format("New repost from %s", author);
        message =
            String.format(
                "New repost received from %s on post %s",
                thePost.getWmSource(), thePost.getWmTarget());
        break;
      case "rsvp":
        title = String.format("New RSVP from %s", author);
        message =
            String.format(
                "New RSVP received from %s on post %s",
                thePost.getWmSource(), thePost.getWmTarget());
        break;
      default:
        throw new IllegalArgumentException(
            String.format("%s is not a valid wm-property", thePost.getWmProperty()));
    }

    return new Message(title, message, url);
  }
}
