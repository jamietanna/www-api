package me.jvt.www.api.notifications.config;

import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.api.notifications.converter.WebmentionToNotificationConverter;
import me.jvt.www.api.notifications.security.WebmentionIoSharedSecretValidator;
import me.jvt.www.api.notifications.security.WebmentionIoSharedSecretValidatorImpl;
import me.jvt.www.api.pushover.PushoverClient;
import me.jvt.www.logging.LoggingConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(LoggingConfiguration.class)
public class SpringConfiguration {
  @Bean
  public PushoverClient pushoverClient(
      RequestSpecificationFactory requestSpecificationFactory,
      @Value("${pushover.app-token}") String appToken,
      @Value("${pushover.user-key}") String userKey) {
    return new PushoverClient(requestSpecificationFactory, appToken, userKey);
  }

  @Bean
  public RequestSpecificationFactory requestSpecificationFactory() {
    return new RequestSpecificationFactory();
  }

  @Bean
  public WebmentionIoSharedSecretValidator webmentionIoSharedSecretValidator(
      @Value("${notifications.webmention.shared-secret}") String sharedSecret) {
    return new WebmentionIoSharedSecretValidatorImpl(sharedSecret);
  }

  @Bean
  public WebmentionToNotificationConverter webmentionToNotificationConverter() {
    return new WebmentionToNotificationConverter();
  }
}
