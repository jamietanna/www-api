package me.jvt.www.indieauth.token;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.when;

import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import java.text.ParseException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Stream;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.token.bertocci.BertocciTokenAdapter;
import me.jvt.www.indieauth.token.model.Scope;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BertocciTokenAdapterTest {
  private static final Instant NOW = Instant.now().truncatedTo(ChronoUnit.SECONDS);
  private static final Instant AUTH_TIME_FOR_REFRESH =
      Instant.now().truncatedTo(ChronoUnit.SECONDS);

  @Mock private SignedJWT jwt;

  private BertocciTokenAdapter token;

  @Nested
  class HappyPath {

    @BeforeEach
    void setup() throws ParseException {
      JWTClaimsSet claims = claimsBuilder().build();
      when(jwt.getJWTClaimsSet()).thenReturn(claims);

      token = new BertocciTokenAdapter(jwt);
    }

    @Test
    void mapsSubject() {
      assertThat(token.getSubject()).isEqualTo(ProfileUrl.of("https://me/"));
    }

    @Test
    void mapsClientId() {
      assertThat(token.getClientId()).isEqualTo("https://client");
    }

    @Test
    void mapsExpirationTime() {
      assertThat(token.getExpirationTime()).isEqualTo(NOW.getEpochSecond());
    }

    @Test
    void mapsIssueTime() {
      assertThat(token.getIssueTime()).isEqualTo(NOW.plus(1, ChronoUnit.SECONDS).getEpochSecond());
    }

    @Test
    void mapsScope() {
      assertThat(token.getScope()).containsExactly(Scope.CREATE, Scope.DELETE);
    }

    @Test
    void mapsAudienceAsFirstEntry() {
      assertThat(token.getAudience()).containsExactly("used", "also used");
    }

    @Test
    void mapsIssuer() {
      assertThat(token.getIssuer()).isEqualTo("IGNORED");
    }

    @Test
    void mapsAuthTime() {
      assertThat(token.getAuthTime()).isEqualTo(AUTH_TIME_FOR_REFRESH.getEpochSecond());
    }
  }

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  class TokenType {
    @ParameterizedTest
    @MethodSource("tokenTypes")
    void whenValid(String tokenType, TokenAdapter.TokenType expected) throws ParseException {
      JWTClaimsSet claims = claimsBuilder().claim("token_type", tokenType).build();
      when(jwt.getJWTClaimsSet()).thenReturn(claims);

      token = new BertocciTokenAdapter(jwt);

      assertThat(token.getTokenType()).isEqualTo(expected);
    }

    @Test
    void throwsIllegalArgumentExceptionWhenInvalidTokenType() throws ParseException {
      JWTClaimsSet claims = claimsBuilder().claim("token_type", "not_supported").build();
      when(jwt.getJWTClaimsSet()).thenReturn(claims);

      assertThatThrownBy(() -> new BertocciTokenAdapter(jwt))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("The token_type=not_supported is not supported at this time");
    }

    Stream<Arguments> tokenTypes() {
      return Stream.of(
          Arguments.of("access_token", TokenAdapter.TokenType.ACCESS_TOKEN),
          Arguments.of("refresh_token", TokenAdapter.TokenType.REFRESH_TOKEN));
    }
  }

  private JWTClaimsSet.Builder claimsBuilder() {
    return new JWTClaimsSet.Builder()
        .issuer("IGNORED")
        .subject("https://me")
        .expirationTime(Date.from(NOW))
        .issueTime(Date.from(NOW.plus(1, ChronoUnit.SECONDS)))
        .claim("client_id", "https://client")
        .claim("auth_time", AUTH_TIME_FOR_REFRESH.getEpochSecond())
        .audience(Arrays.asList("used", "also used"))
        .claim("token_type", "refresh_token")
        .claim("scope", "create delete");
  }
}
