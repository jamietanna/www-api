package me.jvt.www.indieauth.granttype.authorizationcode;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import me.jvt.www.indieauth.authorization.AuthorizationRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class InMemoryAuthorizationCodeStoreTest {
  @Mock private AuthorizationRequest request;

  private final InMemoryAuthorizationCodeStore store =
      new InMemoryAuthorizationCodeStore(Clock.systemUTC());

  @Test
  void canBeRetrievedOnce() throws InvalidAuthorizationCodeException {
    String code = store.put(request);

    assertThat(store.get(code)).isSameAs(request);
  }

  @Test
  void isARandomString() {
    String code = store.put(request);

    assertThat(code).matches("^[a-zA-Z0-9]+$");
  }

  @Test
  void addingSameItemIsAllowed() throws InvalidAuthorizationCodeException {
    String first = store.put(request);
    String second = store.put(request);

    assertThat(store.get(first)).isEqualTo(store.get(second));
  }

  @Test
  void addingSameItemIsAllowedButCodeIsDifferent() {
    String first = store.put(request);
    String second = store.put(request);

    assertThat(first).isNotEqualTo(second);
  }

  @Test
  void cannotBeRetrievedMoreThanOnceAndThrowsInvalidAuthorizationCodeException()
      throws InvalidAuthorizationCodeException {
    String pushed = store.put(request);
    store.get(pushed);

    assertThatThrownBy(() -> store.get(pushed))
        .isInstanceOf(InvalidAuthorizationCodeException.class);
  }

  @ParameterizedTest
  @NullAndEmptySource
  void ifNotFoundThrowsInvalidAuthorizationCodeException(String request) {
    assertThatThrownBy(() -> store.get(request))
        .isInstanceOf(InvalidAuthorizationCodeException.class);
  }

  @Test
  void cannotBeRetrievedAfterTheTimeLimitAndThrowsInvalidAuthorizationCodeException(
      @Mock Clock clock) {
    Instant now = Instant.now().truncatedTo(ChronoUnit.SECONDS);
    Instant expired = now.plus(6, ChronoUnit.MINUTES);
    when(clock.instant()).thenReturn(now, expired);

    InMemoryAuthorizationCodeStore store = new InMemoryAuthorizationCodeStore(clock);
    String pushed = store.put(request);

    assertThatThrownBy(() -> store.get(pushed))
        .isInstanceOf(InvalidAuthorizationCodeException.class);
  }
}
