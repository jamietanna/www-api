package me.jvt.www.indieauth.token;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import com.nimbusds.jwt.SignedJWT;
import java.text.ParseException;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.exception.InvalidGrantException;
import me.jvt.www.indieauth.token.bertocci.BertocciTokenCreator;
import me.jvt.www.indieauth.token.model.Scope;
import me.jvt.www.indieauth.token.model.Token;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BertocciTokenCreatorTest {

  private static final Instant NOW = Instant.now().truncatedTo(ChronoUnit.SECONDS);
  private static final Instant AUTH_TIME_FOR_REFRESH =
      Instant.now().truncatedTo(ChronoUnit.SECONDS);
  private static final Clock FIXED_CLOCK = Clock.fixed(NOW, ZoneId.of("UTC"));
  private static final int ACCESS_TOKEN_DAYS = 30;
  private static final int REFRESH_TOKEN_DAYS = 90;
  public static final String CLIENT_ID = "https://the-client";

  private static RSAKey key;

  private static JWSVerifier verifier;

  @BeforeAll
  static void setupClass() throws JOSEException {
    key = new RSAKeyGenerator(2048).keyIDFromThumbprint(true).generate();
    verifier = new RSASSAVerifier(key);
  }

  @Mock private TokenVerifier tokenVerifier;
  @Mock private TokenAdapter verified;

  private Token token;
  private SignedJWT parsed;
  private TokenCreator creator;

  @BeforeEach
  void setup() {
    creator =
        new BertocciTokenCreator(
            key,
            "https://issuer",
            FIXED_CLOCK,
            "https://default-resource-server",
            JWSAlgorithm.RS512,
            tokenVerifier,
            ACCESS_TOKEN_DAYS,
            REFRESH_TOKEN_DAYS);
  }

  @Test
  void itThrowsIllegalArgumentExceptionIfInvalidKey() {
    assertThatThrownBy(
            () ->
                creator =
                    new BertocciTokenCreator(
                        key.toPublicJWK(),
                        "",
                        FIXED_CLOCK,
                        "",
                        JWSAlgorithm.RS512,
                        null,
                        ACCESS_TOKEN_DAYS,
                        REFRESH_TOKEN_DAYS))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Invalid key provided");
  }

  @Test
  void tokensAreNotGrantedWhenNoScopes() {
    assertThatThrownBy(
            () -> creator.create(CLIENT_ID, ProfileUrl.of("https://me/"), Collections.emptySet()))
        .isInstanceOf(InvalidGrantException.class);
  }

  @Test
  void createWithNoResourceIssuesToken() {
    token =
        creator.create(CLIENT_ID, ProfileUrl.of("https://me/"), asSet(Scope.DELETE, Scope.CREATE));

    assertThat(token.getAccessToken()).isNotNull();
  }

  @Test
  void createWithResourceIssuesToken() {
    token =
        creator.create(
            CLIENT_ID,
            ProfileUrl.of("https://me/"),
            asSet(Scope.DELETE, Scope.CREATE),
            "https://resource");

    assertThat(token.getAccessToken()).isNotNull();
  }

  @Nested
  class AccessToken extends HappyPathAccessToken {

    @Override
    void processWithAudience(String audience) throws ParseException {
      token =
          creator.create(
              CLIENT_ID, ProfileUrl.of("https://me/"), asSet(Scope.DELETE, Scope.CREATE), audience);
      parsed = SignedJWT.parse(token.getAccessToken());
    }
  }

  @Nested
  class RefreshToken {
    @Test
    void isIssuedWhenRequested() {
      process(true);

      assertThat(token.getRefreshToken()).isNotNull();
    }

    @Test
    void isNotIssuedByDefault() {
      token =
          creator.create(
              CLIENT_ID, ProfileUrl.of("https://me/"), asSet(Scope.DELETE, Scope.CREATE));

      assertThat(token.getRefreshToken()).isNull();
    }

    @Test
    void isNotIssuedWhenNotRequestedWithParameter() {
      process(false);

      assertThat(token.getRefreshToken()).isNull();
    }

    @Nested
    class HappyPath extends HappyPathRefreshToken {

      @Override
      void processWithAudience(String audience) throws ParseException {
        token =
            creator.create(
                CLIENT_ID,
                ProfileUrl.of("https://me/"),
                asSet(Scope.DELETE, Scope.CREATE),
                audience,
                true);
        parsed = SignedJWT.parse(token.getRefreshToken());
      }
    }

    private void process(boolean issue) {
      token =
          creator.create(
              CLIENT_ID, ProfileUrl.of("https://me/"), asSet(Scope.DELETE, Scope.CREATE), issue);
      if (issue) {
        try {
          parsed = SignedJWT.parse(token.getRefreshToken());
        } catch (ParseException e) {
          throw new IllegalStateException(e);
        }
      }
    }
  }

  @Nested
  class Refresh {
    @ParameterizedTest
    @NullAndEmptySource
    void itThrowsInvalidGrantExceptionWhenNullOrEmptyRefreshToken(String refreshToken) {
      assertThatThrownBy(() -> creator.refresh(refreshToken, CLIENT_ID))
          .isInstanceOf(InvalidGrantException.class);
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"http://the-client", "https://the-client/"})
    void itThrowsInvalidGrantExceptionWhenNullOrEmptyOrNotExactClientId(
        String clientId, @Mock SignedJWT jwt) throws ParseException {
      try {
        when(tokenVerifier.verify(anyString())).thenReturn(verified);
      } catch (TokenVerificationException e) {
        throw new IllegalStateException(e);
      }
      validClaims();

      assertThatThrownBy(() -> creator.refresh("foo", clientId))
          .isInstanceOf(InvalidGrantException.class);
    }

    @Test
    void itThrowsInvalidGrantExceptionWhenInvalidSignature() throws TokenVerificationException {
      TokenVerificationException cause = new TokenVerificationException("Something went wrong");

      when(tokenVerifier.verify(any())).thenThrow(cause);

      assertThatThrownBy(() -> creator.refresh("eyJ...", CLIENT_ID))
          .isInstanceOf(InvalidGrantException.class)
          .hasCause(cause);
    }

    @Test
    void itThrowsInvalidGrantExceptionWhenAccessTokenIsUsedToRefresh(@Mock SignedJWT jwt)
        throws ParseException {
      try {
        when(tokenVerifier.verify(anyString())).thenReturn(verified);
      } catch (TokenVerificationException e) {
        throw new IllegalStateException(e);
      }
      validClaims();

      assertThatThrownBy(() -> creator.refresh("token", CLIENT_ID))
          .isInstanceOf(InvalidGrantException.class);
    }

    @Nested
    class AccessTokenIsReIssued extends HappyPathAccessToken {

      @Override
      void processWithAudience(String audience) throws ParseException {
        try {
          when(tokenVerifier.verify(anyString())).thenReturn(verified);
        } catch (TokenVerificationException e) {
          throw new IllegalStateException(e);
        }
        validClaims();
        when(verified.getAudience()).thenReturn(Collections.singletonList(audience));
        when(verified.getTokenType()).thenReturn(TokenAdapter.TokenType.REFRESH_TOKEN);

        token = creator.refresh("token", CLIENT_ID);
        parsed = SignedJWT.parse(token.getAccessToken());
      }
    }

    @Nested
    class RefreshTokenIsReIssued extends HappyPathRefreshToken {

      @Override
      void processWithAudience(String audience) throws ParseException {
        try {
          when(tokenVerifier.verify(anyString())).thenReturn(verified);
        } catch (TokenVerificationException e) {
          throw new IllegalStateException(e);
        }
        validClaims();
        when(verified.getAudience()).thenReturn(Collections.singletonList(audience));
        when(verified.getTokenType()).thenReturn(TokenAdapter.TokenType.REFRESH_TOKEN);

        token = creator.refresh("token", CLIENT_ID);
        parsed = SignedJWT.parse(token.getRefreshToken());
      }
    }

    private void validClaims() {
      lenient().when(verified.getIssuer()).thenReturn("IGNORED");
      lenient().when(verified.getSubject()).thenReturn(ProfileUrl.of("https://me/"));
      lenient().when(verified.getClientId()).thenReturn(CLIENT_ID);
      lenient().when(verified.getAuthTime()).thenReturn(AUTH_TIME_FOR_REFRESH.getEpochSecond());
      lenient().when(verified.getScope()).thenReturn(Scope.fromScopeString("create delete"));
    }
  }

  private Set<Scope> asSet(Scope... scopes) {
    return new HashSet<>(Arrays.asList(scopes));
  }

  abstract class HappyPathAccessToken {
    protected void process() throws ParseException {
      processWithAudience("https://default-resource-server");
    }

    abstract void processWithAudience(String audience) throws ParseException;

    @BeforeEach
    void setup() throws ParseException {
      process();
    }

    @Test
    void issuerIsConfiguredInConstructor() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getIssuer()).isEqualTo("https://issuer");
    }

    @Test
    void expiryIsSetToConfiguredDays() throws ParseException {
      Instant expected = NOW.plus(ACCESS_TOKEN_DAYS, ChronoUnit.DAYS);

      assertThat(parsed.getJWTClaimsSet().getExpirationTime().toInstant()).isEqualTo(expected);
    }

    @Test
    void expiresInInTokenIsSetToConfiguredDays() {
      assertThat(token.getExpiresIn()).isEqualTo(60 * 60 * 24 * ACCESS_TOKEN_DAYS);
    }

    @Test
    void audienceIsDefaultedToConfigured() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getAudience())
          .containsExactly("https://default-resource-server");
    }

    @Test
    void audienceCanBeOverridden() throws ParseException {
      processWithAudience("https://resource-server/path");

      assertThat(parsed.getJWTClaimsSet().getAudience())
          .containsExactly("https://resource-server/path");
    }

    @Test
    void subjectIsPassedProfileUrl() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getSubject()).isEqualTo("https://me/");
    }

    @Test
    void clientIdIsPassed() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getStringClaim("client_id")).isEqualTo(CLIENT_ID);
    }

    @Test
    void issuedTimeIsSetToNow() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getIssueTime().toInstant()).isEqualTo(NOW);
    }

    @Test
    void jwtIdIsImplemented() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getJWTID()).isNotNull();
    }

    @Test
    void hasAuthTime() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getLongClaim("auth_time"))
          .isEqualTo(NOW.getEpochSecond());
    }

    @Test
    void acrIsNotYetImplemented() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getClaim("acr")).isNull();
    }

    @Test
    void amrIsNotYetImplemented() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getClaim("amr")).isNull();
    }

    @Test
    void scopeIsPassedAndConvertedToSpaceSeparated() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getStringClaim("scope")).isEqualTo("create delete");
    }

    @Test
    void isSigned() {
      assertThatCode(() -> parsed.verify(verifier)).doesNotThrowAnyException();
    }

    @Test
    void isDelegatedAlgorithm() {
      assertThat(parsed.getHeader().getAlgorithm()).isEqualTo(JWSAlgorithm.RS512);
    }

    @Test
    void hasCorrectJwtType() {
      assertThat(parsed.getHeader().getType()).isEqualTo(new JOSEObjectType("at+jwt"));
    }

    @Test
    void hasTokenTypeOfAccessToken() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getStringClaim("token_type")).isEqualTo("access_token");
    }
  }

  abstract class HappyPathRefreshToken {

    protected void process() throws ParseException {
      processWithAudience("https://default-resource-server");
    }

    abstract void processWithAudience(String audience) throws ParseException;

    @BeforeEach
    void setup() throws ParseException {
      process();
    }

    @Test
    void issuerIsConfiguredInConstructor() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getIssuer()).isEqualTo("https://issuer");
    }

    @Test
    void expiryIsSetToConfiguredDays() throws ParseException {
      Instant expected = NOW.plus(REFRESH_TOKEN_DAYS, ChronoUnit.DAYS);

      assertThat(parsed.getJWTClaimsSet().getExpirationTime().toInstant()).isEqualTo(expected);
    }

    @Test
    void audienceIsDefaultedToConfigured() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getAudience())
          .containsExactly("https://default-resource-server");
    }

    @Test
    void audienceCanBeOverridden() throws ParseException {
      processWithAudience("https://resource-server/path");

      assertThat(parsed.getJWTClaimsSet().getAudience())
          .containsExactly("https://resource-server/path");
    }

    @Test
    void subjectIsPassedProfileUrl() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getSubject()).isEqualTo("https://me/");
    }

    @Test
    void clientIdIsPassed() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getStringClaim("client_id")).isEqualTo(CLIENT_ID);
    }

    @Test
    void issuedTimeIsSetToNow() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getIssueTime().toInstant()).isEqualTo(NOW);
    }

    @Test
    void jwtIdIsImplemented() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getJWTID()).isNotNull();
    }

    @Test
    void hasAuthTime() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getLongClaim("auth_time"))
          .isEqualTo(NOW.getEpochSecond());
    }

    @Test
    void acrIsNotYetImplemented() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getClaim("acr")).isNull();
    }

    @Test
    void amrIsNotYetImplemented() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getClaim("amr")).isNull();
    }

    @Test
    void scopeIsPassedAndConvertedToSpaceSeparated() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getStringClaim("scope")).isEqualTo("create delete");
    }

    @Test
    void isSigned() {
      assertThatCode(() -> parsed.verify(verifier)).doesNotThrowAnyException();
    }

    @Test
    void isDelegatedAlgorithm() {
      assertThat(parsed.getHeader().getAlgorithm()).isEqualTo(JWSAlgorithm.RS512);
    }

    @Test
    void hasCorrectJwtType() {
      assertThat(parsed.getHeader().getType()).isEqualTo(new JOSEObjectType("rt+jwt"));
    }

    @Test
    void hasTokenTypeOfRefreshToken() throws ParseException {
      assertThat(parsed.getJWTClaimsSet().getStringClaim("token_type")).isEqualTo("refresh_token");
    }
  }
}
