package me.jvt.www.indieauth.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;
import me.jvt.www.indieauth.controllers.TokenController;
import me.jvt.www.indieauth.exception.InvalidGrantException;
import me.jvt.www.indieauth.exception.UnsupportedGrantTypeException;
import me.jvt.www.indieauth.token.TokenService;
import me.jvt.www.indieauth.token.model.TokenGrantDto;
import me.jvt.www.indieauth.token.model.TokenGrantRequest;
import me.jvt.www.indieauth.token.model.TokenVerificationDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@AutoConfigureMockMvc
@WebMvcTest(TokenController.class)
@ExtendWith(SpringExtension.class)
class TokenControllerIntegrationTest {
  @Autowired private MockMvc mvc;

  @MockBean private TokenService service;

  @Nested
  class Exchange {

    private final TokenGrantDto response = new TokenGrantDto();
    @Captor private ArgumentCaptor<TokenGrantRequest> requestArgumentCaptor;
    private TokenGrantRequest request;
    protected ResponseEntity<TokenGrantDto> actual;
    private ResultActions resultActions;

    @BeforeEach
    void setup() throws Exception {
      Mockito.reset(service);
      when(service.tokenExchange(any())).thenReturn(response);

      executeAndCapture(Optional.of("verifier"), false);
    }

    @Test
    void mapsGrantType() {
      assertThat(request.getGrantType()).isEqualTo("grant");
    }

    @Test
    void mapsCodeIfPresent() {
      assertThat(request.getCode()).isEqualTo("code");
    }

    @Test
    void doesNotMapCodeIfNotPresent() throws Exception {
      executeAndCapture(Optional.empty(), false, false);

      assertThat(request.getCode()).isNull();
    }

    @Test
    void mapsRefreshTokenIfPresent() throws Exception {
      executeAndCapture(Optional.empty(), false, false);

      assertThat(request.getRefreshToken()).isEqualTo("rt");
    }

    @Test
    void doesNotMapRefreshTokenIfNotPresent() {
      assertThat(request.getRefreshToken()).isNull();
    }

    @Test
    void mapsClientId() {
      assertThat(request.getClientId()).isEqualTo("https://client_id");
    }

    @Test
    void prioritisesClientIdInAuthorizationHeaderWhenPresent() throws Exception {
      executeAndCapture(Optional.of("verifier"), true);

      assertThat(request.getClientId()).isEqualTo("the-client");
    }

    @Test
    void mapsRedirectUri() {
      assertThat(request.getRedirectUri()).isEqualTo("https://to-me");
    }

    @Test
    void mapsCodeVerifierIfPresent() {
      assertThat(request.getCodeVerifier()).isEqualTo("verifier");
    }

    @Test
    void doesNotMapCodeVerifierIfNotPresent() throws Exception {
      executeAndCapture(Optional.empty(), false);

      assertThat(request.getCodeVerifier()).isNull();
    }

    @Test
    void unsupportedGrantTypeExceptionIsMappedAsBadRequest() throws Exception {
      Mockito.reset(service);
      when(service.tokenExchange(any())).thenThrow(new UnsupportedGrantTypeException());

      execute(Optional.empty(), false);

      resultActions.andExpect(status().isBadRequest());
    }

    @Test
    void invalidGrantExceptionIsMappedAsBadRequest() throws Exception {
      Mockito.reset(service);
      when(service.tokenExchange(any())).thenThrow(new InvalidGrantException());

      execute(Optional.empty(), false);

      resultActions.andExpect(status().isBadRequest());
    }

    private void execute(Optional<String> codeVerifier, boolean withAuth) throws Exception {
      execute(codeVerifier, withAuth, true);
    }

    private void execute(
        Optional<String> codeVerifier, boolean withAuth, boolean isAuthorizationCodeGrant)
        throws Exception {
      MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
      params.add("grant_type", "grant");
      if (isAuthorizationCodeGrant) {
        params.add("code", "code");
      } else {
        params.add("refresh_token", "rt");
      }
      params.add("client_id", "https://client_id");
      params.add("redirect_uri", "https://to-me");
      codeVerifier.ifPresent(v -> params.add("code_verifier", v));

      MockHttpServletRequestBuilder builder =
          post("/token").contentType(MediaType.APPLICATION_FORM_URLENCODED).params(params);
      if (withAuth) {
        builder.with(httpBasic("the-client", ""));
      }
      resultActions = mvc.perform(builder);
    }

    private void executeAndCapture(Optional<String> codeVerifier, boolean withAuth)
        throws Exception {
      executeAndCapture(codeVerifier, withAuth, true);
    }

    private void executeAndCapture(
        Optional<String> codeVerifier, boolean withAuth, boolean isAuthorizationCodeGrant)
        throws Exception {
      execute(codeVerifier, withAuth, isAuthorizationCodeGrant);
      verify(service, atLeastOnce()).tokenExchange(requestArgumentCaptor.capture());
      request = requestArgumentCaptor.getValue();
    }
  }

  @Nested
  class Verify {
    @Nested
    class Get {
      @Nested
      class Active {
        @Test
        void returns200() throws Exception {
          TokenVerificationDto dto = new TokenVerificationDto();
          dto.setActive(true);
          when(service.verifyToken(eq("x.y.z"))).thenReturn(dto);

          mvc.perform(get("/token").header("Authorization", "Bearer x.y.z"))
              .andExpect(status().isOk());
        }
      }

      @Nested
      class NotActive {
        @Test
        void returns400() throws Exception {
          TokenVerificationDto dto = new TokenVerificationDto();
          dto.setActive(false);
          when(service.verifyToken(any())).thenReturn(dto);

          mvc.perform(get("/token").header("Authorization", "Bearer x.y.z"))
              .andExpect(status().isBadRequest());
        }
      }
    }

    @Nested
    class Post {
      @Nested
      class Active {
        @Test
        void returns200() throws Exception {
          TokenVerificationDto dto = new TokenVerificationDto();
          dto.setActive(true);
          when(service.verifyToken(any())).thenReturn(dto);

          mvc.perform(post("/token_info").queryParam("token", "x.y.z")).andExpect(status().isOk());
        }
      }

      @Nested
      class NotActive {
        @Test
        void returns200() throws Exception {
          TokenVerificationDto dto = new TokenVerificationDto();
          dto.setActive(false);
          when(service.verifyToken(any())).thenReturn(dto);

          mvc.perform(post("/token_info").queryParam("token", "x.y.z")).andExpect(status().isOk());
        }
      }
    }
  }
}
