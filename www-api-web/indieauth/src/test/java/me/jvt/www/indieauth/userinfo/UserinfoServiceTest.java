package me.jvt.www.indieauth.userinfo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.exception.InsufficientScopeException;
import me.jvt.www.indieauth.exception.InvalidTokenException;
import me.jvt.www.indieauth.exception.NoAuthenticationProvidedException;
import me.jvt.www.indieauth.token.TokenAdapter;
import me.jvt.www.indieauth.token.TokenService;
import me.jvt.www.indieauth.token.model.Scope;
import me.jvt.www.indieauth.token.model.UserinfoResponseDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UserinfoServiceTest {
  @Mock private TokenService tokenService;
  @Mock private TokenAdapter token;

  @InjectMocks private UserinfoService service;

  @Nested
  class Retrieve {
    @Nested
    class WhenValid {
      private UserinfoResponseDto response;

      @BeforeEach
      void setup()
          throws InvalidTokenException, NoAuthenticationProvidedException,
              InsufficientScopeException {
        when(tokenService.verifyAndUnpack(any())).thenReturn(Optional.of(token));
        when(token.getScope()).thenReturn(Set.of(Scope.PROFILE, Scope.EMAIL));
        when(token.getSubject()).thenReturn(ProfileUrl.of("https://foo/"));
        when(token.getClientId()).thenReturn("https://client_id");
        response = service.retrieve("eyJ");
      }

      @Test
      void validatesToken() {
        verify(tokenService).verifyAndUnpack("eyJ");
      }

      @Test
      void mapsHardcodedName() {
        assertThat(response.name()).isEqualTo("Jamie Tanna");
      }

      @Test
      void mapsUrlFromToken() {
        assertThat(response.url()).isEqualTo(ProfileUrl.of("https://foo/"));
      }

      @Test
      void mapsHardcodedPhoto() {
        assertThat(response.photo()).isEqualTo("https://www.jvt.me/img/profile.jpg");
      }

      @Test
      void mapsHardcodedEmailWithTransformedClientId() {
        String encoded =
            Base64.getUrlEncoder()
                .encodeToString("https://client_id".getBytes(StandardCharsets.UTF_8));

        assertThat(response.email()).contains("indieauth+" + encoded + "@jamietanna.co.uk");
      }
    }

    @Nested
    class WhenNullOrEmpty {
      @ParameterizedTest
      @NullAndEmptySource
      void throwsNoAuthenticationProvidedException(String token) {
        assertThatThrownBy(() -> service.retrieve(token))
            .isInstanceOf(NoAuthenticationProvidedException.class)
            .hasMessage("Token was null/empty");
      }
    }

    @Nested
    class WhenTokenServiceFailsUnpack {
      @Test
      void throwsInvalidTokenException() {
        when(tokenService.verifyAndUnpack(any())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> service.retrieve("foo"))
            .isInstanceOf(InvalidTokenException.class)
            .hasMessage("The token was not deemed valid");
      }
    }

    @Nested
    class WhenNoProfileScope {
      @Test
      void itThrowsInsufficientScopeException() {
        when(tokenService.verifyAndUnpack(any())).thenReturn(Optional.of(token));
        when(token.getScope()).thenReturn(Collections.emptySet());

        assertThatThrownBy(() -> service.retrieve("eyJ"))
            .isInstanceOf(InsufficientScopeException.class)
            .hasMessage("`profile` scope was not present");
      }
    }

    @Nested
    class WhenNoEmailScope {
      @Test
      void itDoesNotReturnEmail()
          throws InvalidTokenException, NoAuthenticationProvidedException,
              InsufficientScopeException {
        when(tokenService.verifyAndUnpack(any())).thenReturn(Optional.of(token));
        when(token.getScope()).thenReturn(Set.of(Scope.PROFILE));

        var response = service.retrieve("eyJ");

        assertThat(response.email()).isEmpty();
      }
    }
  }
}
