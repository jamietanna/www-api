package me.jvt.www.indieauth.config;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import me.jvt.www.indieauth.exception.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

class GlobalExceptionHandlerTest {

  private GlobalExceptionHandler handler = new GlobalExceptionHandler();

  @Nested
  class HandleInvalidGrantException {
    ResponseEntity<OAuthErrorDto> actual;

    @BeforeEach
    void setup() {
      actual = handler.handleInvalidGrantException(new InvalidGrantException(null));
    }

    @Test
    void isMappedToBadRequest() {
      assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void hasErrorInvalidGrant() {
      assertThat(actual.getBody().getError()).isEqualTo("invalid_grant");
    }

    @Test
    void hasNoErrorDescription() {
      assertThat(actual.getBody().getErrorDescription()).isNull();
    }
  }

  @Nested
  class HandleUnsupportedGrantTypeException {
    ResponseEntity<OAuthErrorDto> actual;

    @BeforeEach
    void setup() {
      actual = handler.handleUnsupportedGrantType(new UnsupportedGrantTypeException());
    }

    @Test
    void isMappedToBadRequest() {
      assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void hasErrorInvalidGrant() {
      assertThat(actual.getBody().getError()).isEqualTo("unsupported_grant_type");
    }

    @Test
    void hasNoErrorDescription() {
      assertThat(actual.getBody().getErrorDescription()).isNull();
    }
  }

  @Nested
  class HandleWwwAuthenticateException {

    private ResponseEntity<Void> actual;

    @Nested
    class WhenNoErrorPresent {
      @BeforeEach
      void setup() {
        actual = handler.handleWwwAuthenticateException(new WwwAuthenticateException("human desc"));
      }

      @Test
      void isUnauthorized() {
        assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
      }

      @Test
      void addsWwwAuthenticateHeader() {
        assertThat(actual.getHeaders()).containsEntry("Www-Authenticate", List.of("Bearer"));
      }
    }

    @Nested
    class WhenErrorPresent {

      @BeforeEach
      void setup() {
        actual =
            handler.handleWwwAuthenticateException(
                new WwwAuthenticateException("the_error", "human desc"));
      }

      @Test
      void isUnauthorized() {
        assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
      }

      @Test
      void addsWwwAuthenticateHeader() {
        assertThat(actual.getHeaders())
            .containsEntry(
                "Www-Authenticate",
                List.of("Bearer error=\"the_error\", error_description=\"human desc\""));
      }
    }

    @Nested
    class WhenNoAuthentication {

      @BeforeEach
      void setup() {
        actual =
            handler.handleWwwAuthenticateException(
                new NoAuthenticationProvidedException(
                    "This is an exception message, not to be returned to the caller"));
      }

      @Test
      void isUnauthorized() {
        assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
      }

      @Test
      void addsWwwAuthenticateHeaderWithNoErrorOrErrorDescription() {
        assertThat(actual.getHeaders()).containsEntry("Www-Authenticate", List.of("Bearer"));
      }
    }

    @Nested
    class WheninvalidTokenException {

      @BeforeEach
      void setup() {
        actual = handler.handleWwwAuthenticateException(new InvalidTokenException("foo bar baz"));
      }

      @Test
      void isUnauthorized() {
        assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
      }

      @Test
      void addsWwwAuthenticateHeader() {
        assertThat(actual.getHeaders())
            .containsEntry(
                "Www-Authenticate",
                List.of("Bearer error=\"invalid_token\", error_description=\"foo bar baz\""));
      }
    }
  }
}
