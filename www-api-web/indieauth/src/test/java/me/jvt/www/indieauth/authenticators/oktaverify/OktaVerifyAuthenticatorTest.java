package me.jvt.www.indieauth.authenticators.oktaverify;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import me.jvt.www.indieauth.authenticators.AuthenticationNotSupportedException;
import me.jvt.www.indieauth.authenticators.Context;
import me.jvt.www.indieauth.authenticators.InternalAuthenticationException;
import me.jvt.www.indieauth.domain.ProfileUrl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OktaVerifyAuthenticatorTest {

  @Mock private OktaFacade facade;
  @Mock private SleepFacade sleepFacade;

  private final Context context = new Context();
  private OktaVerifyAuthenticator authenticator;

  @BeforeEach
  void setup() {
    authenticator =
        new OktaVerifyAuthenticator(
            Collections.singletonList(ProfileUrl.of("https://me/")),
            "user",
            "factor",
            facade,
            sleepFacade);
  }

  @Test
  void id() {
    assertThat(authenticator.id()).isEqualTo("push");
  }

  @Test
  void name() {
    assertThat(authenticator.name()).isEqualTo("Push Notification");
  }

  @Test
  void throwsAuthenticationNotSupportedExceptionWhenInvalidMe() {
    assertThatThrownBy(() -> authenticator.authenticate(ProfileUrl.of("https://not-me"), context))
        .isInstanceOf(AuthenticationNotSupportedException.class);
  }

  @Test
  void delegatesToFacadeToVerify()
      throws InternalAuthenticationException, AuthenticationNotSupportedException {
    when(facade.isTransactionSuccess(any(), any(), any())).thenReturn(VerificationResult.SUCCESS);
    context.setUserAgent("agent");
    context.setXForwardedFor("ip");

    authenticator.authenticate(ProfileUrl.of("https://me/"), context);

    verify(facade).triggerVerify("user", "factor", "ip", "agent");
  }

  @Test
  void delegatesToFacadeToQuery()
      throws InternalAuthenticationException, AuthenticationNotSupportedException {
    when(facade.triggerVerify(any(), any(), any(), any())).thenReturn("transactionId");
    when(facade.isTransactionSuccess(any(), any(), any())).thenReturn(VerificationResult.SUCCESS);

    authenticator.authenticate(ProfileUrl.of("https://me/"), context);

    verify(facade).isTransactionSuccess("user", "factor", "transactionId");
  }

  @Test
  void itReturnsTrueWhenQueryReturnsSuccess()
      throws InternalAuthenticationException, AuthenticationNotSupportedException {
    when(facade.triggerVerify(any(), any(), any(), any())).thenReturn("transactionId");
    when(facade.isTransactionSuccess(any(), any(), any())).thenReturn(VerificationResult.SUCCESS);

    boolean actual = authenticator.authenticate(ProfileUrl.of("https://me/"), context);

    assertThat(actual).isTrue();
  }

  @Test
  void itReturnsFalseWhenQueryReturnsClientError()
      throws InternalAuthenticationException, AuthenticationNotSupportedException {
    when(facade.triggerVerify(any(), any(), any(), any())).thenReturn("transactionId");
    when(facade.isTransactionSuccess(any(), any(), any()))
        .thenReturn(VerificationResult.CLIENT_ERROR);

    boolean actual = authenticator.authenticate(ProfileUrl.of("https://me/"), context);

    assertThat(actual).isFalse();
  }

  @Test
  void itThrowsInternalAuthenticationErrorWhenInternalError() {
    when(facade.triggerVerify(any(), any(), any(), any())).thenReturn("transactionId");
    when(facade.isTransactionSuccess(any(), any(), any()))
        .thenReturn(VerificationResult.INTERNAL_ERROR);

    assertThatThrownBy(() -> authenticator.authenticate(ProfileUrl.of("https://me/"), context))
        .isInstanceOf(InternalAuthenticationException.class)
        .hasMessage(
            "Something went wrong while trying to authenticate. Check logs for more details");
  }

  @Test
  void itSleepsAndRetries()
      throws InternalAuthenticationException, InterruptedException,
          AuthenticationNotSupportedException {
    when(facade.triggerVerify(any(), any(), any(), any())).thenReturn("");
    when(facade.isTransactionSuccess(any(), any(), any()))
        .thenReturn(
            VerificationResult.WAITING, VerificationResult.WAITING, VerificationResult.SUCCESS);

    boolean actual = authenticator.authenticate(ProfileUrl.of("https://me/"), context);

    assertThat(actual).isTrue();
    verify(sleepFacade, times(2)).sleep(3000);
  }

  @Test
  void itReturnsSuccessIfASuccessIsFound()
      throws InternalAuthenticationException, AuthenticationNotSupportedException {
    when(facade.triggerVerify(any(), any(), any(), any())).thenReturn("");
    when(facade.isTransactionSuccess(any(), any(), any()))
        .thenReturn(VerificationResult.WAITING, VerificationResult.SUCCESS);

    boolean actual = authenticator.authenticate(ProfileUrl.of("https://me/"), context);

    assertThat(actual).isTrue();
  }

  @Test
  void itThrowsInternalAuthenticationErrorWhenExceedingMaximumRetries() {
    when(facade.isTransactionSuccess(any(), any(), any()))
        .thenReturn(
            VerificationResult.INTERNAL_ERROR,
            VerificationResult.INTERNAL_ERROR,
            VerificationResult.INTERNAL_ERROR,
            VerificationResult.INTERNAL_ERROR,
            VerificationResult.INTERNAL_ERROR,
            VerificationResult.INTERNAL_ERROR,
            VerificationResult.INTERNAL_ERROR,
            VerificationResult.INTERNAL_ERROR,
            VerificationResult.INTERNAL_ERROR,
            VerificationResult.INTERNAL_ERROR);

    assertThatThrownBy(() -> authenticator.authenticate(ProfileUrl.of("https://me/"), context))
        .isInstanceOf(InternalAuthenticationException.class)
        .hasMessage(
            "Something went wrong while trying to authenticate. Check logs for more details");
  }

  @Test
  void itThrowsInternalAuthenticationErrorWhenSleepIsInterrupted() throws InterruptedException {
    when(facade.isTransactionSuccess(any(), any(), any())).thenReturn(VerificationResult.WAITING);
    doThrow(InterruptedException.class).when(sleepFacade).sleep(anyLong());

    assertThatThrownBy(() -> authenticator.authenticate(ProfileUrl.of("https://me/"), context))
        .isInstanceOf(InternalAuthenticationException.class)
        .hasMessage(
            "Something went wrong while trying to authenticate. Check logs for more details");
  }
}
