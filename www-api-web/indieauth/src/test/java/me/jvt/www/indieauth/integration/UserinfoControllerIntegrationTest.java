package me.jvt.www.indieauth.integration;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;
import me.jvt.www.indieauth.controllers.UserinfoController;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.exception.InsufficientScopeException;
import me.jvt.www.indieauth.exception.InvalidTokenException;
import me.jvt.www.indieauth.token.model.UserinfoResponseDto;
import me.jvt.www.indieauth.userinfo.UserinfoService;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(UserinfoController.class)
class UserinfoControllerIntegrationTest {
  @Autowired private MockMvc mockMvc;

  @MockBean private UserinfoService service;

  @Nested
  class Retrieve {
    @Nested
    class OnSuccess {
      @Test
      void returns200() throws Exception {
        when(service.retrieve(any())).thenReturn(response());

        mockMvc
            .perform(get("/userinfo").header("authorization", "Bearer eyJ"))
            .andExpect(status().isOk());
      }

      @Test
      void delegatesToken() throws Exception {
        when(service.retrieve(any())).thenReturn(response());

        mockMvc.perform(get("/userinfo").header("authorization", "Bearer eyJ"));

        verify(service).retrieve("eyJ");
      }

      private static UserinfoResponseDto response() {
        return new UserinfoResponseDto("", ProfileUrl.of("https://foo"), "", Optional.empty());
      }
    }

    @Nested
    class OnError {
      @Test
      void onInvalidTokenException() throws Exception {
        when(service.retrieve(any())).thenThrow(new InvalidTokenException("Try a new one?"));

        mockMvc
            .perform(get("/userinfo").header("authorization", "Bearer eyJ"))
            .andExpect(status().isUnauthorized())
            .andExpect(
                header()
                    .string(
                        "www-authenticate",
                        "Bearer error=\"invalid_token\", error_description=\"Try a new one?\""));
      }

      @Test
      void onInsufficientScopeException() throws Exception {
        when(service.retrieve(any())).thenThrow(new InsufficientScopeException("foo"));

        mockMvc
            .perform(get("/userinfo").header("authorization", "Bearer eyJ"))
            .andExpect(status().isUnauthorized())
            .andExpect(
                header()
                    .string(
                        "www-authenticate",
                        "Bearer error=\"insufficient_scope\", error_description=\"foo\""));
      }

      @Test
      void onNoAuthenticationProvidedException() throws Exception {
        mockMvc
            .perform(get("/userinfo"))
            .andExpect(status().isUnauthorized())
            .andExpect(header().string("www-authenticate", "Bearer"));
      }
    }
  }
}
