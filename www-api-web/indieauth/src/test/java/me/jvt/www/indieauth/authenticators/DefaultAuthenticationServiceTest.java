package me.jvt.www.indieauth.authenticators;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import me.jvt.www.indieauth.authenticators.SessionCookieProvider.InvalidSessionCookieException;
import me.jvt.www.indieauth.authenticators.SessionCookieProvider.SessionCookie;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.template.AuthenticatorEntry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DefaultAuthenticationServiceTest {

  @Mock private SessionCookieProvider provider;
  @Mock private Authenticator authenticator0;
  @Mock private Authenticator authenticator1;
  @Mock private SessionCookie sessionCookie;

  private DefaultAuthenticationService service;

  @BeforeEach
  void setup() {
    service =
        new DefaultAuthenticationService(provider, Arrays.asList(authenticator0, authenticator1));
  }

  @Nested
  class Constructor {
    @Test
    void doesNotAllowEmptyAuthenticators() {
      assertThatThrownBy(() -> new DefaultAuthenticationService(null, Collections.emptyList()))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("DefaultAuthenticationService requires at least one Authenticator");
    }
  }

  @Nested
  class ProfileUrlFromCookie {

    @Test
    void itReturnsEmptyWhenNotProvided() {
      Optional<ProfileUrl> actual = service.profileUrlFromCookie(Optional.empty());

      assertThat(actual).isEmpty();
    }

    @Test
    void itDelegatesToProvider() throws InvalidSessionCookieException {
      validRequest();
      Optional<String> cookie = Optional.of("foo");

      service.profileUrlFromCookie(cookie);

      verify(provider).validate("foo");
    }

    @Test
    void itReturnsProfileUrlFromProvider() throws InvalidSessionCookieException {
      validRequest();
      Optional<String> cookie = Optional.of("foo");

      Optional<ProfileUrl> actual = service.profileUrlFromCookie(cookie);

      assertThat(actual).contains(ProfileUrl.of("https://profile/"));
    }

    @Test
    void itReturnsEmptyWhenInvalidCookie() throws InvalidSessionCookieException {
      when(provider.validate(anyString())).thenThrow(new InvalidSessionCookieException());

      Optional<ProfileUrl> actual = service.profileUrlFromCookie(Optional.of("bar"));

      assertThat(actual).isEmpty();
    }

    private void validRequest() throws InvalidSessionCookieException {
      when(provider.validate(anyString())).thenReturn(sessionCookie);
      when(sessionCookie.getProfileUrl()).thenReturn(ProfileUrl.of("https://profile/"));
    }
  }

  @Nested
  class Authenticate {

    @BeforeEach
    void setup() {
      lenient().when(authenticator0.id()).thenReturn("id-0");
      lenient().when(authenticator1.id()).thenReturn("id-1");
    }

    @Test
    void throwsAuthenticationNotSupportedExceptionWhenNoMatches() {
      assertThatThrownBy(() -> service.authenticate(null, null, null))
          .isInstanceOf(AuthenticationNotSupportedException.class);
    }

    @Test
    void bubblesUpAuthenticationNotSupportedException()
        throws InternalAuthenticationException, AuthenticationNotSupportedException {
      when(authenticator0.authenticate(any(), any()))
          .thenThrow(new AuthenticationNotSupportedException());

      assertThatThrownBy(() -> service.authenticate("id-0", ProfileUrl.of("https://fake"), null))
          .isInstanceOf(AuthenticationNotSupportedException.class);
    }

    @Test
    void bubblesUpInternalAuthenticationException()
        throws InternalAuthenticationException, AuthenticationNotSupportedException {
      when(authenticator0.authenticate(any(), any()))
          .thenThrow(new InternalAuthenticationException(""));

      assertThatThrownBy(() -> service.authenticate("id-0", ProfileUrl.of("https://fake"), null))
          .isInstanceOf(InternalAuthenticationException.class);
    }

    @Test
    void throwsAuthenticationFailedExceptionWhenAuthenticatorFails()
        throws InternalAuthenticationException, AuthenticationNotSupportedException {
      when(authenticator0.authenticate(any(), any())).thenReturn(false);

      assertThatThrownBy(() -> service.authenticate("id-0", ProfileUrl.of("https://fake"), null))
          .isInstanceOf(AuthenticationFailedException.class);
    }

    @Test
    void itDelegatesMe()
        throws AuthenticationFailedException, AuthenticationNotSupportedException,
            InternalAuthenticationException {
      happyPath();

      service.authenticate("id-1", ProfileUrl.of("https://me/"), null);

      verify(authenticator1).authenticate(eq(ProfileUrl.of("https://me/")), any());
    }

    @Test
    void itDelegatesContext(@Mock Context context)
        throws AuthenticationFailedException, AuthenticationNotSupportedException,
            InternalAuthenticationException {
      happyPath();

      service.authenticate("id-1", ProfileUrl.of("https://me"), context);

      verify(authenticator1).authenticate(any(), eq(context));
    }

    @Test
    void itDelegatesToProvider()
        throws InternalAuthenticationException, AuthenticationNotSupportedException,
            AuthenticationFailedException {
      happyPath();

      service.authenticate("id-1", ProfileUrl.of("https://me"), null);

      verify(provider).create(ProfileUrl.of("https://me"));
    }

    @Test
    void returnsSerializedSessionCookie()
        throws InternalAuthenticationException, AuthenticationNotSupportedException,
            AuthenticationFailedException {
      happyPath();

      String actual = service.authenticate("id-1", ProfileUrl.of("https://me"), null);

      assertThat(actual).isEqualTo("serialized");
    }

    private void happyPath()
        throws InternalAuthenticationException, AuthenticationNotSupportedException {
      when(authenticator1.authenticate(any(), any())).thenReturn(true);

      when(provider.create(any())).thenReturn(sessionCookie);
      when(sessionCookie.serialize()).thenReturn("serialized");
    }
  }

  @Nested
  class AuthenticatorsForMe {
    @Test
    void itReturnsEntriesFromAuthenticators(
        @Mock Authenticator authenticator0, @Mock Authenticator authenticator1) {
      when(authenticator0.id()).thenReturn("id0");
      when(authenticator0.name()).thenReturn("name0");
      when(authenticator1.id()).thenReturn("id1");
      when(authenticator1.name()).thenReturn("name1");
      service =
          new DefaultAuthenticationService(provider, Arrays.asList(authenticator0, authenticator1));

      List<AuthenticatorEntry> entries = service.authenticatorsForMe(null);

      assertThat(entries)
          .usingRecursiveFieldByFieldElementComparator()
          .containsExactly(
              new AuthenticatorEntry("id0", "name0"), new AuthenticatorEntry("id1", "name1"));
    }
  }
}
