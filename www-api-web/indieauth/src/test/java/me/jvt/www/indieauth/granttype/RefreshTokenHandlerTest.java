package me.jvt.www.indieauth.granttype;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import me.jvt.www.indieauth.exception.InvalidGrantException;
import me.jvt.www.indieauth.token.TokenCreator;
import me.jvt.www.indieauth.token.model.Token;
import me.jvt.www.indieauth.token.model.TokenExchangeDto;
import me.jvt.www.indieauth.token.model.TokenGrantRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RefreshTokenHandlerTest {
  private static final String GRANT_TYPE = "refresh_token";

  @Mock private TokenCreator creator;

  @InjectMocks private RefreshTokenHandler handler;

  @Nested
  class PreAuthorize {
    // has no implementation
  }

  @Nested
  class Authorize {
    // has no implementation
  }

  @Nested
  class GetGrantType {
    @Test
    void returnsRefreshToken() {
      assertThat(handler.getGrantType()).isEqualTo(GRANT_TYPE);
    }
  }

  @Nested
  class GetResponseType {
    @Test
    void returnsEmpty() {
      assertThat(handler.getResponseType()).isEmpty();
    }
  }

  @Nested
  class ProfileExchange {
    // has no implementaton
  }

  @Nested
  class TokenExchange {
    private TokenGrantRequest request = new TokenGrantRequest();

    @Nested
    class SadPath {
      @ParameterizedTest
      @NullAndEmptySource
      void itThrowsInvalidGrantExceptionWhenNoRefreshToken(String token) {
        request.setGrantType(GRANT_TYPE);
        request.setRefreshToken(token);

        assertThatThrownBy(() -> handler.tokenExchange(request))
            .isInstanceOf(InvalidGrantException.class);
      }

      @ParameterizedTest
      @NullAndEmptySource
      void itThrowsInvalidGrantExceptionWhenNoClientId(String clientId) {
        request.setGrantType(GRANT_TYPE);
        request.setRefreshToken("jwt");
        request.setClientId(clientId);

        assertThatThrownBy(() -> handler.tokenExchange(request))
            .isInstanceOf(InvalidGrantException.class);
      }
    }

    @Nested
    class HappyPath {
      private TokenExchangeDto actual;

      @BeforeEach
      void setup(@Mock Token token) {
        when(creator.refresh(any(), any())).thenReturn(token);
        when(token.getAccessToken()).thenReturn("at");
        when(token.getRefreshToken()).thenReturn("rt");
        when(token.getExpiresIn()).thenReturn(5L);

        request.setRefreshToken("token");
        request.setClientId("https://client");

        actual = handler.tokenExchange(request);
      }

      @Test
      void itDelegatesRefreshTokenToCreator() {
        verify(creator).refresh(eq("token"), any());
      }

      @Test
      void itDelegatesClientIdToCreator() {
        verify(creator).refresh(any(), eq("https://client"));
      }

      @Test
      void itReturnsAccessToken() {
        assertThat(actual.getAccessToken()).isEqualTo("at");
      }

      @Test
      void itReturnsRefreshToken() {
        assertThat(actual.getRefreshToken()).isEqualTo("rt");
      }

      @Test
      void itReturnsExpiresIn() {
        assertThat(actual.getExpiresIn()).isEqualTo(5);
      }
    }
  }
}
