package me.jvt.www.indieauth.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.Optional;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.exception.InsufficientScopeException;
import me.jvt.www.indieauth.exception.InvalidTokenException;
import me.jvt.www.indieauth.exception.NoAuthenticationProvidedException;
import me.jvt.www.indieauth.token.model.UserinfoResponseDto;
import me.jvt.www.indieauth.userinfo.UserinfoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UserinfoControllerTest {
  @Mock private UserinfoService service;

  @InjectMocks private UserinfoController controller;

  @Nested
  class Retrieve {

    private final UserinfoResponseDto response =
        new UserinfoResponseDto("", ProfileUrl.of("https://foo"), "", Optional.empty());
    private UserinfoResponseDto actual;

    @Nested
    class WhenNoToken {
      @Test
      void itThrowsNoAuthenticationProvidedExceptionWhenOptionalEmpty() {
        assertThatThrownBy(() -> controller.retrieve(Optional.empty()))
            .isInstanceOf(NoAuthenticationProvidedException.class)
            .hasMessage("No authentication was provided");
      }

      @Test
      void itThrowsNoAuthenticationProvidedExceptionWhenEmpty() {
        assertThatThrownBy(() -> controller.retrieve(Optional.of("")))
            .isInstanceOf(NoAuthenticationProvidedException.class)
            .hasMessage("No authentication was provided");
      }
    }

    @Nested
    class WhenNotABearerToken {
      @ParameterizedTest
      @ValueSource(strings = {"bearer foo", "Bearer", "Bearertoken", "eyJ"})
      void itThrowsNoAuthenticationProvidedException(String token) {
        assertThatThrownBy(() -> controller.retrieve(Optional.of(token)))
            .isInstanceOf(NoAuthenticationProvidedException.class)
            .hasMessage("No authentication was provided");
      }
    }

    @Nested
    class WhenRejectedByService {
      @Test
      void itThrowsNoAuthenticationProvidedException()
          throws InvalidTokenException, NoAuthenticationProvidedException,
              InsufficientScopeException {
        var cause = new NoAuthenticationProvidedException("");
        when(service.retrieve(any())).thenThrow(cause);

        assertThatThrownBy(() -> controller.retrieve(Optional.of("Bearer valid"))).isEqualTo(cause);
      }

      @Test
      void itThrowsInvalidTokenException()
          throws InvalidTokenException, NoAuthenticationProvidedException,
              InsufficientScopeException {
        var cause = new InvalidTokenException("");
        when(service.retrieve(any())).thenThrow(cause);

        assertThatThrownBy(() -> controller.retrieve(Optional.of("Bearer valid"))).isEqualTo(cause);
      }
    }

    @Nested
    class WhenValid {
      private final UserinfoResponseDto response =
          new UserinfoResponseDto("", ProfileUrl.of("https://foo"), "", Optional.empty());
      private UserinfoResponseDto actual;

      @BeforeEach
      void setup()
          throws InvalidTokenException, NoAuthenticationProvidedException,
              InsufficientScopeException {
        when(service.retrieve(any())).thenReturn(response);

        actual = controller.retrieve(Optional.of("Bearer eyJ"));
      }

      @Test
      void delegates()
          throws InvalidTokenException, NoAuthenticationProvidedException,
              InsufficientScopeException {
        verify(service).retrieve("eyJ");
      }

      @Test
      void returnsFromService() {
        assertThat(actual).isEqualTo(response);
      }
    }
  }
}
