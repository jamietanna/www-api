package me.jvt.www.indieauth.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.function.Supplier;
import me.jvt.www.indieauth.authorizationservermetadata.ServerConfigurationDto;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AuthorizationServerMetadataControllerTest {

  @InjectMocks private AuthorizationServerMetadataController controller;

  @Mock private Supplier<ServerConfigurationDto> supplier;

  @Nested
  class RetrieveConfiguration {
    @Test
    void itDelegatesToSupplier(@Mock ServerConfigurationDto mockResponse) {
      when(supplier.get()).thenReturn(mockResponse);

      ServerConfigurationDto actual = controller.retrieveConfiguration();

      assertThat(actual).isEqualTo(mockResponse);
    }
  }
}
