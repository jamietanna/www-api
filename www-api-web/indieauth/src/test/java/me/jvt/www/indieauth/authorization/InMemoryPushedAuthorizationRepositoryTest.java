package me.jvt.www.indieauth.authorization;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class InMemoryPushedAuthorizationRepositoryTest {
  @Mock private AuthorizationRequest request;

  private final InMemoryPushedAuthorizationRepository repository =
      new InMemoryPushedAuthorizationRepository(Clock.systemUTC());

  @Test
  void canBeRetrievedWithGet() throws AuthorizationException {
    String pushed = repository.put(request);

    assertThat(repository.get(pushed)).isSameAs(request);
  }

  @Test
  void canBeRetrievedMultipleTimesWithGet() {
    String pushed = repository.put(request);
    assertThatCode(
            () -> {
              repository.get(pushed);
              repository.get(pushed);
            })
        .doesNotThrowAnyException();
  }

  @Test
  void cannotBeRetrievedAfterPoppedMoreThanOnceAndThrowsAuthorizationException()
      throws AuthorizationException {
    String pushed = repository.put(request);
    repository.get(pushed);
    repository.pop(pushed);

    AuthorizationException exception =
        new AuthorizationException(AuthorizationException.INVALID_REQUEST);

    assertThatThrownBy(() -> repository.get(pushed)).isEqualTo(exception);
  }

  @Test
  void canBeRetrievedWithPop() throws AuthorizationException {
    String pushed = repository.put(request);

    assertThat(repository.pop(pushed)).isSameAs(request);
  }

  @Test
  void cannotBePoppedMoreThanOnceAndThrowsAuthorizationException() throws AuthorizationException {
    String pushed = repository.put(request);
    repository.pop(pushed);

    AuthorizationException exception =
        new AuthorizationException(AuthorizationException.INVALID_REQUEST);

    assertThatThrownBy(() -> repository.pop(pushed)).isEqualTo(exception);
  }

  @Test
  void requestUriFollowsRfc() {
    String pushed = repository.put(request);

    assertThat(pushed).startsWith("urn:ietf:params:oauth:request_uri:");
  }

  @Test
  void requestUriEndsWithRandomString() {
    String pushed = repository.put(request);

    assertThat(pushed).matches("^.*:[a-zA-Z0-9]+$");
  }

  @Test
  void addingSameItemIsAllowed() throws AuthorizationException {
    String first = repository.put(request);
    String second = repository.put(request);

    assertThat(repository.get(first)).isEqualTo(repository.get(second));
  }

  @ParameterizedTest
  @NullAndEmptySource
  void ifNotFoundGetThrowsAuthorizationException(String pushedRequest) {
    AuthorizationException exception =
        new AuthorizationException(AuthorizationException.INVALID_REQUEST);

    assertThatThrownBy(() -> repository.get(pushedRequest)).isEqualTo(exception);
  }

  @ParameterizedTest
  @NullAndEmptySource
  void ifNotFoundPopThrowsAuthorizationException(String pushedRequest) {
    AuthorizationException exception =
        new AuthorizationException(AuthorizationException.INVALID_REQUEST);

    assertThatThrownBy(() -> repository.pop(pushedRequest)).isEqualTo(exception);
  }

  @Test
  void cannotBeRetrievedAfterTheTimeLimitAndThrowsInvalidAuthorizationException(@Mock Clock clock) {
    Instant now = Instant.now().truncatedTo(ChronoUnit.SECONDS);
    Instant expired = now.plus(6, ChronoUnit.MINUTES);
    when(clock.instant()).thenReturn(now, expired);

    InMemoryPushedAuthorizationRepository repository =
        new InMemoryPushedAuthorizationRepository(clock);
    String pushed = repository.put(request);

    assertThatThrownBy(() -> repository.get(pushed)).isInstanceOf(AuthorizationException.class);
  }

  @Test
  void cannotBePoppedAfterTheTimeLimitAndThrowsInvalidAuthorizationException(@Mock Clock clock) {
    Instant now = Instant.now().truncatedTo(ChronoUnit.SECONDS);
    Instant expired = now.plus(61, ChronoUnit.SECONDS);
    when(clock.instant()).thenReturn(now, expired);

    InMemoryPushedAuthorizationRepository repository =
        new InMemoryPushedAuthorizationRepository(clock);
    String pushed = repository.put(request);

    assertThatThrownBy(() -> repository.pop(pushed)).isInstanceOf(AuthorizationException.class);
  }
}
