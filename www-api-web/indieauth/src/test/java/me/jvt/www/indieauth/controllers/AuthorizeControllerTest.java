package me.jvt.www.indieauth.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import me.jvt.www.indieauth.authenticators.Context;
import me.jvt.www.indieauth.authenticators.SessionCookieProvider;
import me.jvt.www.indieauth.authenticators.SessionCookieProvider.InvalidSessionCookieException;
import me.jvt.www.indieauth.authenticators.SessionCookieProvider.SessionCookie;
import me.jvt.www.indieauth.authorization.*;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.template.AuthenticatorEntry;
import me.jvt.www.indieauth.template.ScopeEntry;
import me.jvt.www.indieauth.token.TokenService;
import me.jvt.www.indieauth.token.model.ProfileExchangeDto;
import me.jvt.www.indieauth.token.model.Scope;
import me.jvt.www.indieauth.token.model.Scope.Group;
import me.jvt.www.indieauth.token.model.TokenGrantRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@ExtendWith(MockitoExtension.class)
class AuthorizeControllerTest {

  @Mock private Model model;
  @Mock private PushedAuthorizationRepository requestRepository;
  @Mock private TokenService service;
  @Mock private RedirectUriProvider redirectUriProvider;
  @Mock private SessionCookieProvider sessionCookieProvider;
  @Mock private SessionCookie cookie;
  @Captor private ArgumentCaptor<AuthorizationRequest> requestCaptor;
  @Captor private ArgumentCaptor<List<AuthenticatorEntry>> entryCaptor;

  private AuthorizeController endpoint;
  private String response;

  @BeforeEach
  void setup() {
    endpoint =
        new AuthorizeController(
            service, requestRepository, redirectUriProvider, 1, 2, sessionCookieProvider);
  }

  @Nested
  class Get {

    @Nested
    class HappyPath {
      private AuthorizationRequest request;

      @BeforeEach
      void setup() throws AuthorizationException {
        when(requestRepository.put(any())).thenReturn("urn:1234");

        authorize();

        verify(service, atLeastOnce()).preauthorize(requestCaptor.capture());
        request = requestCaptor.getValue();
      }

      @Test
      void responseTypeIsMapped() {
        assertThat(request.getResponseType()).isEqualTo("code");
      }

      @Test
      void clientIdIsMapped() {
        assertThat(request.getClientId()).isEqualTo("https://client");
      }

      @Test
      void redirectUriIsMapped() {
        assertThat(request.getRedirectUri()).isEqualTo("https://to-you");
      }

      @Test
      void stateIsMappedIfPresent() {
        assertThat(request.getState()).isEqualTo("the-state");
      }

      @Test
      void stateIsNotMappedIfNotPresent() throws AuthorizationException {
        response =
            endpoint.authorize(
                "",
                "",
                "",
                Optional.empty(),
                Optional.of(""),
                Optional.of(""),
                Optional.of("scope"),
                Optional.of("https://me/"));
        verify(service, atLeastOnce()).preauthorize(requestCaptor.capture());
        request = requestCaptor.getValue();

        assertThat(request.getState()).isNull();
      }

      @Test
      void codeChallengeIsMappedIfPresent() {
        assertThat(request.getPKCE().getCodeChallenge()).isEqualTo("challenge");
      }

      @Test
      void codeChallengeIsNotMappedIfNotPresent() throws AuthorizationException {
        response =
            endpoint.authorize(
                "",
                "",
                "",
                Optional.of(""),
                Optional.empty(),
                Optional.of(""),
                Optional.of("scope"),
                Optional.of("https://me/"));
        verify(service, atLeastOnce()).preauthorize(requestCaptor.capture());
        request = requestCaptor.getValue();

        assertThat(request.getPKCE().getCodeChallenge()).isNull();
      }

      @Test
      void codeChallengeMethodIsMappedIfPresent() {
        assertThat(request.getPKCE().getCodeChallengeMethod()).isEqualTo("S1234");
      }

      @Test
      void codeChallengeMethodIsNotMappedIfNotPresent() throws AuthorizationException {
        response =
            endpoint.authorize(
                "",
                "",
                "",
                Optional.of(""),
                Optional.of(""),
                Optional.empty(),
                Optional.of("scope"),
                Optional.of("https://me/"));
        verify(service, atLeastOnce()).preauthorize(requestCaptor.capture());
        request = requestCaptor.getValue();

        assertThat(request.getPKCE().getCodeChallengeMethod()).isNull();
      }

      @Test
      void pkceIsNotSetIfNoChallengeOrMethod() throws AuthorizationException {
        response =
            endpoint.authorize(
                "",
                "",
                "",
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.of("scope"),
                Optional.of("https://me/"));
        verify(service, atLeastOnce()).preauthorize(requestCaptor.capture());
        request = requestCaptor.getValue();

        assertThat(request.getPKCE()).isNull();
      }

      @Test
      void scopesAreParsedAndSetIfPresent() {
        assertThat(request.getScopes()).containsExactlyInAnyOrder(Scope.CREATE, Scope.UPDATE);
      }

      @Test
      void nonStandardScopeArePossible() throws AuthorizationException {
        response =
            endpoint.authorize(
                "",
                "",
                "",
                Optional.of(""),
                Optional.of(""),
                Optional.of(""),
                Optional.of("wibble"),
                Optional.of("https://me/"));
        verify(service, atLeastOnce()).preauthorize(requestCaptor.capture());
        request = requestCaptor.getValue();

        assertThat(request.getScopes()).containsExactly(new Scope("wibble"));
      }

      @Test
      void scopesAreEmptyIfNotPresent() throws AuthorizationException {
        response =
            endpoint.authorize(
                "",
                "",
                "",
                Optional.of(""),
                Optional.of(""),
                Optional.of(""),
                Optional.empty(),
                Optional.of("https://me/"));
        verify(service, atLeastOnce()).preauthorize(requestCaptor.capture());
        request = requestCaptor.getValue();

        assertThat(request.getScopes()).isEmpty();
      }

      @Test
      void meIsMappedIfPresent() {
        assertThat(request.getMe()).isEqualTo(ProfileUrl.of("https://me/"));
      }

      @Test
      void meIsDefaultedIfNotPresent() throws AuthorizationException {
        response =
            endpoint.authorize(
                "",
                "",
                "",
                Optional.of(""),
                Optional.of(""),
                Optional.of(""),
                Optional.of("scope"),
                Optional.empty());
        verify(service, atLeastOnce()).preauthorize(requestCaptor.capture());
        request = requestCaptor.getValue();

        assertThat(request.getMe()).isNull();
      }

      @Test
      void redirectsToConsentPage() {
        assertThat(response)
            .isEqualTo("redirect:/authorize/consent?client_id=https://client&request_uri=urn:1234");
      }

      @Test
      void storesInternalStateForRequest() {
        verify(requestRepository).put(request);
      }
    }

    @Nested
    class Errors {

      @Test
      void areMappedFromProvider() throws AuthorizationException {
        AuthorizationException exception = new AuthorizationException("something");
        doThrow(exception).when(service).preauthorize(any());
        when(redirectUriProvider.handleAuthorizationException(any(), any()))
            .thenReturn("the-redirect");

        authorize();

        assertThat(response).isEqualTo("redirect:the-redirect");
      }
    }

    private void authorize() {
      response =
          endpoint.authorize(
              "code",
              "https://client",
              "https://to-you",
              Optional.of("the-state"),
              Optional.of("challenge"),
              Optional.of("S1234"),
              Optional.of("create update"),
              Optional.of("https://me"));
    }
  }

  @Nested
  class ProfileExchange {

    @Mock private ProfileExchangeDto response;
    @Captor private ArgumentCaptor<TokenGrantRequest> requestArgumentCaptor;
    private TokenGrantRequest request;
    protected ResponseEntity<ProfileExchangeDto> actual;

    @BeforeEach
    void setup() {
      when(service.profileExchange(any())).thenReturn(response);

      executeAndCapture(Optional.of("verifier"));
    }

    void execute(Optional<String> codeVerifier) {
      MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
      params.add("grant_type", "grant");
      params.add("code", "code");
      params.add("refresh_token", "rt");
      params.add("client_id", "https://client_id");
      params.add("redirect_uri", "https://to-me");
      codeVerifier.ifPresent(s -> params.add("code_verifier", s));

      actual = endpoint.profileExchange(params);
    }

    @Test
    void mapsGrantType() {
      assertThat(request.getGrantType()).isEqualTo("grant");
    }

    @Test
    void mapsCode() {
      assertThat(request.getCode()).isEqualTo("code");
    }

    @Test
    void mapsRefreshToken() {
      assertThat(request.getRefreshToken()).isEqualTo("rt");
    }

    @Test
    void mapsClientId() {
      assertThat(request.getClientId()).isEqualTo("https://client_id");
    }

    @Test
    void mapsRedirectUri() {
      assertThat(request.getRedirectUri()).isEqualTo("https://to-me");
    }

    @Test
    void mapsCodeVerifierIfPresent() {
      assertThat(request.getCodeVerifier()).isEqualTo("verifier");
    }

    @Test
    void doesNotMapCodeVerifierIfNotPresent() {
      executeAndCapture(Optional.empty());

      assertThat(request.getCodeVerifier()).isNull();
    }

    @Test
    void returnsOnlyMe() {
      assertThat(actual.getBody()).isSameAs(response);
    }

    @Test
    void returnsOk() {
      assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private void executeAndCapture(Optional<String> codeVerifier) {
      execute(codeVerifier);
      verify(service, atLeastOnce()).profileExchange(requestArgumentCaptor.capture());
      request = requestArgumentCaptor.getValue();
    }
  }

  @Nested
  class Consent {

    @Mock private AuthorizationRequest request;

    @Nested
    class HappyPath {

      @Captor private ArgumentCaptor<Map<Scope.Group, List<ScopeEntry>>> scopeEntriesCaptor;
      private Map<Scope.Group, List<ScopeEntry>> scopeEntries;

      @BeforeEach
      void setup() throws AuthorizationException, InvalidSessionCookieException {
        when(requestRepository.get(any())).thenReturn(request);
        when(request.getScopes()).thenReturn(Collections.singleton(Scope.NOTIFY));
        when(sessionCookieProvider.validate(any())).thenReturn(cookie);
        when(cookie.getProfileUrl()).thenReturn(ProfileUrl.of("https://foo/"));
        response = endpoint.consent(model, "urn:1234", "valid", null, null);
        verify(model, atLeastOnce()).addAttribute(eq("scopes"), scopeEntriesCaptor.capture());
        scopeEntries = scopeEntriesCaptor.getValue();
      }

      @Test
      void addsRequestToModel() {
        verify(model, atLeastOnce()).addAttribute("request", request);
      }

      @Test
      void addsRequestedScopeToModelWithRequestedFlag() {
        List<ScopeEntry> entries = scopeEntries.get(Group.NOTIFICATION);

        assertThat(entries)
            .usingRecursiveFieldByFieldElementComparator()
            .contains(new ScopeEntry(Scope.NOTIFY, true));
      }

      @Test
      void addsNonRequestedScopeToModelWithoutRequestedFlag() {
        List<ScopeEntry> entries = scopeEntries.get(Group.PUBLISHING);

        assertThat(entries)
            .usingRecursiveFieldByFieldElementComparator()
            .contains(new ScopeEntry(Scope.CREATE, false));
      }

      @Test
      void addsRequestUriToModel() {
        verify(model, atLeastOnce()).addAttribute("request_uri", "urn:1234");
      }

      @Test
      void addsMeFromSessionCookie() {
        verify(model, atLeastOnce()).addAttribute("me", ProfileUrl.of("https://foo/"));
      }

      @Test
      void addsAccessTokenLifetimeToModel() {
        verify(model, atLeastOnce()).addAttribute("accessTokenDays", 1);
      }

      @Test
      void addsRefreshTokenLifetimeToModel() {
        verify(model, atLeastOnce()).addAttribute("refreshTokenDays", 2);
      }

      @Test
      void rendersConsentPage() {
        assertThat(response).isEqualTo("consent");
      }
    }

    @Nested
    class WhenNoOrInvalidCookie {
      @Mock private HttpServletRequest servletRequest;
      @Mock private HttpServletResponse servletResponse;
      @Captor private ArgumentCaptor<Cookie> cookieCaptor;
      private Cookie cookie;

      void process(String querystring) throws InvalidSessionCookieException {
        when(servletRequest.getRequestURI()).thenReturn("/this/url");
        when(servletRequest.getQueryString()).thenReturn(querystring);
        when(sessionCookieProvider.validate(any())).thenThrow(new InvalidSessionCookieException());
        response = endpoint.consent(model, null, null, servletRequest, servletResponse);
        verify(servletResponse).addCookie(cookieCaptor.capture());
        cookie = cookieCaptor.getValue();
      }

      @Nested
      class WithoutRequest {

        @BeforeEach
        void setup() throws AuthorizationException {
          when(requestRepository.get(any())).thenThrow(new AuthorizationException(""));
        }

        @Test
        void redirectsToAuthenticateRoot() throws InvalidSessionCookieException {
          process(null);
          assertThat(response).isEqualTo("redirect:/authenticate");
        }

        @Test
        void itSetsCookieName() throws InvalidSessionCookieException {
          process(null);

          assertThat(cookie.getName()).isEqualTo("redirect_uri");
        }

        @Test
        void itSetsCookieValueForRedirectUri() throws InvalidSessionCookieException {
          process("foo=bar&abc=def");

          assertThat(cookie.getValue()).isEqualTo("/this/url?foo=bar&abc=def");
        }

        @Test
        void cookieHasHttpOnly() throws InvalidSessionCookieException {
          process(null);

          assertThat(cookie.isHttpOnly()).isTrue();
        }

        @Test
        void cookieIsSecure() throws InvalidSessionCookieException {
          process(null);

          assertThat(cookie.getSecure()).isTrue();
        }

        @Test
        void cookieHasRootPath() throws InvalidSessionCookieException {
          process(null);

          assertThat(cookie.getPath()).isEqualTo("/");
        }
      }

      @Nested
      class WithRequest {
        @Mock private AuthorizationRequest request;

        @Test
        void meIsNotAddedWhenNullMe() throws AuthorizationException, InvalidSessionCookieException {
          when(requestRepository.get(any())).thenReturn(request);

          process(null);
          assertThat(response).isEqualTo("redirect:/authenticate");
        }

        @Test
        void meIsAddedWhenNotNull() throws AuthorizationException, InvalidSessionCookieException {
          when(requestRepository.get(any())).thenReturn(request);
          when(request.getMe()).thenReturn(ProfileUrl.of("https://me/"));

          process(null);
          assertThat(response).isEqualTo("redirect:/authenticate/start?me=https://me/");
        }
      }
    }

    @Nested
    class WhenNotFound {
      @BeforeEach
      void setup() throws AuthorizationException {
        AuthorizationException exception = new AuthorizationException("err");
        when(requestRepository.get(any())).thenThrow(exception);

        response = endpoint.consent(model, "", null, null, null);
      }

      @Test
      void returnsErrorPage() {
        assertThat(response).isEqualTo("error");
      }

      @Test
      void addsErrorToModel() {
        verify(model).addAttribute("error", "err");
      }
    }
  }

  private void setupCookie(ProfileUrl me) {
    try {
      when(sessionCookieProvider.validate(any())).thenReturn(cookie);
      when(cookie.getProfileUrl()).thenReturn(me);
    } catch (InvalidSessionCookieException e) {
      throw new IllegalStateException(e);
    }
  }

  @Nested
  class SubmitConsent {
    @Captor private ArgumentCaptor<Context> contextCaptor;
    @Mock private AuthorizationRequest request;

    @Nested
    class Allow {

      @Test
      void invalidRequestWhenRequestIdInvalidReturnsError() throws AuthorizationException {
        when(requestRepository.pop(any()))
            .thenThrow(new AuthorizationException(AuthorizationException.INVALID_REQUEST));

        String actual =
            endpoint.submitConsent(model, null, Collections.emptySet(), false, null, null, null);

        verify(model).addAttribute("error", "invalid_request");
        assertThat(actual).isEqualTo("error");
      }

      @Test
      void successRedirectsToAppWithCode() throws AuthorizationException {
        validRequest();

        AuthorizationResponseDto response = new AuthorizationResponseDto();
        response.setCode("1234");
        response.setRedirectUri("https://new-redirect");
        when(service.authorize(any())).thenReturn(response);
        when(redirectUriProvider.code(any(), any())).thenReturn("redirect-with-code");

        String actual =
            endpoint.submitConsent(
                model, "urn:...", Collections.emptySet(), false, null, null, null);

        assertThat(actual).isEqualTo("redirect:redirect-with-code");
      }

      @Test
      void successOverwritesMe() throws AuthorizationException {
        ProfileUrl me = ProfileUrl.of("https://foo.overwritten");
        validRequest();
        setupCookie(me);
        request.setMe(ProfileUrl.of("https://me"));

        AuthorizationResponseDto response = new AuthorizationResponseDto();
        response.setCode("1234");
        response.setRedirectUri("https://new-redirect");
        when(service.authorize(any())).thenReturn(response);

        endpoint.submitConsent(model, "urn:...", Collections.emptySet(), false, null, null, null);

        verify(request).setMe(me);
      }

      @Test
      void successOverwritesScopes(@Mock Set<Scope> scopes) throws AuthorizationException {
        validRequest();

        AuthorizationResponseDto response = new AuthorizationResponseDto();
        response.setCode("1234");
        response.setRedirectUri("https://new-redirect");
        when(service.authorize(any())).thenReturn(response);

        String actual = endpoint.submitConsent(model, "urn:...", scopes, false, null, null, null);

        verify(request).setScopes(scopes);
      }

      @ParameterizedTest
      @ValueSource(booleans = {true, false})
      void successSetsIssueRefreshToken(boolean issueRefreshToken) throws AuthorizationException {
        validRequest();

        AuthorizationResponseDto response = new AuthorizationResponseDto();
        response.setCode("1234");
        response.setRedirectUri("https://new-redirect");
        when(service.authorize(any())).thenReturn(response);

        endpoint.submitConsent(
            model, "urn:...", Collections.emptySet(), issueRefreshToken, null, null, null);

        verify(request).setIssueRefreshToken(issueRefreshToken);
      }

      @Test
      void successRedirectsToAppWithStateWhenPresent() throws AuthorizationException {
        validRequest();

        AuthorizationResponseDto response = new AuthorizationResponseDto();
        response.setCode("1234");
        response.setRedirectUri("https://new-redirect");
        response.setState("the-state");
        when(service.authorize(any())).thenReturn(response);
        when(redirectUriProvider.code(any(), any())).thenReturn("redirect-with-code-and-state");

        String actual =
            endpoint.submitConsent(
                model, "urn:...", Collections.emptySet(), false, null, null, null);

        assertThat(actual).isEqualTo("redirect:redirect-with-code-and-state");
      }
    }

    @Nested
    class WhenNoOrInvalidCookie {
      @Mock private HttpServletRequest servletRequest;
      @Mock private HttpServletResponse servletResponse;
      @Captor private ArgumentCaptor<Cookie> cookieCaptor;
      private Cookie cookie;

      void process(String querystring) throws InvalidSessionCookieException {
        when(servletRequest.getRequestURI()).thenReturn("/this/url");
        when(servletRequest.getQueryString()).thenReturn(querystring);
        when(sessionCookieProvider.validate(any())).thenThrow(new InvalidSessionCookieException());
        response =
            endpoint.submitConsent(
                model, null, Collections.emptySet(), false, null, servletRequest, servletResponse);
        verify(servletResponse).addCookie(cookieCaptor.capture());
        cookie = cookieCaptor.getValue();
      }

      @Nested
      class WithoutRequest {

        @BeforeEach
        void setup() throws AuthorizationException {
          when(requestRepository.get(any())).thenThrow(new AuthorizationException(""));
        }

        @Test
        void redirectsToAuthenticateRoot() throws InvalidSessionCookieException {
          process(null);
          assertThat(response).isEqualTo("redirect:/authenticate");
        }

        @Test
        void itSetsCookieName() throws InvalidSessionCookieException {
          process(null);

          assertThat(cookie.getName()).isEqualTo("redirect_uri");
        }

        @Test
        void itSetsCookieValueForRedirectUri() throws InvalidSessionCookieException {
          process("foo=bar&abc=def");

          assertThat(cookie.getValue()).isEqualTo("/this/url?foo=bar&abc=def");
        }

        @Test
        void cookieHasHttpOnly() throws InvalidSessionCookieException {
          process(null);

          assertThat(cookie.isHttpOnly()).isTrue();
        }

        @Test
        void cookieIsSecure() throws InvalidSessionCookieException {
          process(null);

          assertThat(cookie.getSecure()).isTrue();
        }

        @Test
        void cookieHasRootPath() throws InvalidSessionCookieException {
          process(null);

          assertThat(cookie.getPath()).isEqualTo("/");
        }
      }

      @Nested
      class WithRequest {
        @Mock private AuthorizationRequest request;

        @Test
        void meIsNotAddedWhenNullMe() throws AuthorizationException, InvalidSessionCookieException {
          when(requestRepository.get(any())).thenReturn(request);

          process(null);

          assertThat(response).isEqualTo("redirect:/authenticate");
        }

        @Test
        void meIsAddedWhenNotNull() throws AuthorizationException, InvalidSessionCookieException {
          when(requestRepository.get(any())).thenReturn(request);
          when(request.getMe()).thenReturn(ProfileUrl.of("https://me/"));

          process(null);

          assertThat(response).isEqualTo("redirect:/authenticate/start?me=https://me/");
        }
      }
    }

    private void validRequest() throws AuthorizationException {
      setupCookie(ProfileUrl.of("https://foo"));
      when(requestRepository.pop(any())).thenReturn(request);
    }
  }

  @Nested
  class Reject {
    @Mock private AuthorizationRequest request;

    @Test
    void returnsErrorWhenNoRequest() throws AuthorizationException {
      when(requestRepository.pop(any()))
          .thenThrow(new AuthorizationException(AuthorizationException.INVALID_REQUEST));

      String actual = endpoint.authorizeDeny(model, "uri");

      verify(model).addAttribute("error", "invalid_request");
      assertThat(actual).isEqualTo("error");
    }

    @Test
    void delegatesToRepositoryToGetRequest() throws AuthorizationException {
      when(requestRepository.pop(any())).thenReturn(request);

      endpoint.authorizeDeny(model, "uri");

      verify(requestRepository).pop("uri");
    }

    @Test
    void errorRedirectsToRedirectUriWithAccessDenied() throws AuthorizationException {
      when(requestRepository.pop(any())).thenReturn(request);
      when(redirectUriProvider.accessDenied(any())).thenReturn("denied");

      String actual = endpoint.authorizeDeny(model, "uri");

      assertThat(actual).isEqualTo("redirect:denied");
    }
  }
}
