package me.jvt.www.indieauth.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.okta.sdk.client.Client;
import java.util.Collections;
import java.util.Set;
import me.jvt.www.indieauth.authorization.AuthorizationException;
import me.jvt.www.indieauth.authorization.AuthorizationRequest;
import me.jvt.www.indieauth.authorization.AuthorizationResponseDto;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.exception.InvalidGrantException;
import me.jvt.www.indieauth.token.TokenService;
import me.jvt.www.indieauth.token.model.ProfileExchangeDto;
import me.jvt.www.indieauth.token.model.Scope;
import me.jvt.www.indieauth.token.model.TokenGrantDto;
import me.jvt.www.indieauth.token.model.TokenGrantRequest;
import me.jvt.www.indieauth.token.model.TokenVerificationDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("ephemeral")
class TokenServiceIntegrationTest {
  @Autowired private TokenService service;

  @SuppressWarnings("unused")
  @MockBean
  private Client client;

  @Test
  void grantedTokenCanBeVerified() throws AuthorizationException {
    TokenGrantDto tokenGrant = tokenExchange();

    TokenVerificationDto response = service.verifyToken(tokenGrant.getAccessToken());

    assertThat(response.isActive()).isTrue();
    // smoke test to validate that at least one of the fields are set on the response
    assertThat(response.getClientId()).isNotNull();
  }

  @Test
  void refreshTokensCannotBeVerified() throws AuthorizationException {
    TokenGrantDto tokenGrant = tokenExchange();

    TokenVerificationDto response = service.verifyToken(tokenGrant.getRefreshToken());

    assertThat(response.isActive()).isFalse();
  }

  @Test
  void refreshTokensCanBeGrantedAndReturnsDifferentTokens() throws AuthorizationException {
    TokenGrantDto tokenGrant = tokenExchange();

    String accessToken0 = tokenGrant.getAccessToken();
    String refreshToken0 = tokenGrant.getRefreshToken();

    TokenGrantRequest refreshRequest = new TokenGrantRequest();
    refreshRequest.setClientId("https://client");
    refreshRequest.setGrantType("refresh_token");
    refreshRequest.setRefreshToken(refreshToken0);
    TokenGrantDto refreshed = service.tokenExchange(refreshRequest);

    assertThat(accessToken0).isNotEqualTo(refreshed.getAccessToken());
    assertThat(refreshToken0).isNotEqualTo(refreshed.getRefreshToken());
  }

  @Test
  void accessTokensCannotBeUsedForRefreshTokenGrant() throws AuthorizationException {
    TokenGrantDto tokenGrant = tokenExchange();

    TokenGrantRequest refreshRequest = new TokenGrantRequest();
    refreshRequest.setGrantType("refresh_token");
    refreshRequest.setRefreshToken(tokenGrant.getAccessToken());
    assertThatThrownBy(() -> service.tokenExchange(refreshRequest))
        .isInstanceOf(InvalidGrantException.class);
  }

  @Test
  void authorizationCodeWithNoScopesCanReturnProfile() throws AuthorizationException {
    TokenGrantRequest request = authorize(Collections.emptySet());
    ProfileExchangeDto response = service.profileExchange(request);

    assertThat(response.getMe()).isEqualTo("https://me/");
  }

  @Test
  void authorizationCodeWithNoScopesCannotReturnToken() throws AuthorizationException {
    TokenGrantRequest request = authorize(Collections.emptySet());

    assertThatThrownBy(() -> service.tokenExchange(request))
        .isInstanceOf(InvalidGrantException.class);
  }

  private TokenGrantDto tokenExchange() throws AuthorizationException {
    TokenGrantRequest tokenGrantRequest = authorize(Collections.singleton(Scope.UNDELETE));
    return service.tokenExchange(tokenGrantRequest);
  }

  private TokenGrantRequest authorize(Set<Scope> scopes) throws AuthorizationException {
    AuthorizationRequest request = new AuthorizationRequest();
    request.setClientId("https://client");
    request.setMe(ProfileUrl.of("https://me/"));
    request.setScopes(scopes);
    request.setResponseType("code");
    request.setRedirectUri("https://to-me");
    request.setIssueRefreshToken(true);
    AuthorizationResponseDto authorizationResponse = service.authorize(request);

    TokenGrantRequest tokenGrantRequest = new TokenGrantRequest();
    tokenGrantRequest.setClientId("https://client");
    tokenGrantRequest.setGrantType("authorization_code");
    tokenGrantRequest.setRedirectUri(authorizationResponse.getRedirectUri());
    tokenGrantRequest.setCode(authorizationResponse.getCode());
    return tokenGrantRequest;
  }
}
