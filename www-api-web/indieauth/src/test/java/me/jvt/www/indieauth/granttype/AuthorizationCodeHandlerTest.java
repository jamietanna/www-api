package me.jvt.www.indieauth.granttype;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import me.jvt.www.indieauth.authorization.AuthorizationException;
import me.jvt.www.indieauth.authorization.AuthorizationRequest;
import me.jvt.www.indieauth.authorization.AuthorizationResponseDto;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.exception.InvalidGrantException;
import me.jvt.www.indieauth.granttype.authorizationcode.AuthorizationCodeStore;
import me.jvt.www.indieauth.granttype.authorizationcode.InvalidAuthorizationCodeException;
import me.jvt.www.indieauth.granttype.authorizationcode.pkce.PKCEVerifier;
import me.jvt.www.indieauth.token.TokenCreator;
import me.jvt.www.indieauth.token.model.ProfileExchangeDto;
import me.jvt.www.indieauth.token.model.Scope;
import me.jvt.www.indieauth.token.model.Token;
import me.jvt.www.indieauth.token.model.TokenExchangeDto;
import me.jvt.www.indieauth.token.model.TokenGrantRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AuthorizationCodeHandlerTest {
  @Mock private AuthorizationCodeStore store;
  @Mock private TokenCreator creator;
  @Mock private PKCEVerifier pkceVerifier;

  @InjectMocks private AuthorizationCodeHandler handler;

  @Nested
  class PreAuthorize {
    private final AuthorizationRequest request = new AuthorizationRequest();

    @Nested
    class PKCE {
      private final AuthorizationRequest.PKCE pkce = new AuthorizationRequest.PKCE();

      @Test
      void isOptional() {
        assertThatCode(() -> handler.preauthorize(request)).doesNotThrowAnyException();
      }

      @ParameterizedTest
      @NullAndEmptySource
      void throwsAuthorizationExceptionWhenPKCEMissingCodeChallenge(String challenge) {
        pkce.setCodeChallenge(challenge);
        pkce.setCodeChallengeMethod("S256");
        request.setPKCE(pkce);

        AuthorizationException expected =
            new AuthorizationException(
                AuthorizationException.INVALID_REQUEST, "PKCE requires code_challenge");

        assertThatThrownBy(() -> handler.preauthorize(request)).isEqualTo(expected);
      }

      @ParameterizedTest
      @NullAndEmptySource
      void throwsAuthorizationExceptionWhenPKCEMissingCodeMethod(String method) {
        pkce.setCodeChallenge("challenge");
        pkce.setCodeChallengeMethod(method);
        request.setPKCE(pkce);

        AuthorizationException expected =
            new AuthorizationException(
                AuthorizationException.INVALID_REQUEST, "PKCE requires code_challenge_method");

        assertThatThrownBy(() -> handler.preauthorize(request)).isEqualTo(expected);
      }

      @Test
      void throwsAuthorizationExceptionWhenMethodIsNotSupported() {
        when(pkceVerifier.supported(any())).thenReturn(false);
        pkce.setCodeChallenge("challenge");
        pkce.setCodeChallengeMethod("S123");
        request.setPKCE(pkce);

        AuthorizationException expected =
            new AuthorizationException(
                AuthorizationException.INVALID_REQUEST,
                "This server does not support S123 as a code_challenge_method");

        assertThatThrownBy(() -> handler.preauthorize(request)).isEqualTo(expected);
      }

      @Test
      void isHappyWhenBothSet() {
        when(pkceVerifier.supported(any())).thenReturn(true);
        pkce.setCodeChallenge("foo");
        pkce.setCodeChallengeMethod("S256");
        request.setPKCE(pkce);

        assertThatCode(() -> handler.preauthorize(request)).doesNotThrowAnyException();
      }
    }
  }

  @Nested
  class Authorize {
    @Captor private ArgumentCaptor<AuthorizationRequest> requestCaptor;
    private AuthorizationRequest request;

    private AuthorizationResponseDto response;
    private final AuthorizationRequest incomingRequest = new AuthorizationRequest();

    @BeforeEach
    void setup() throws AuthorizationException {
      when(store.put(any())).thenReturn("the-code");

      incomingRequest.setClientId("https://the-client");
      incomingRequest.setMe(ProfileUrl.of("https://me.me/"));
      incomingRequest.setRedirectUri("https://to-you");
      incomingRequest.setState("the-state");
      incomingRequest.setScopes(asSet(Scope.CREATE, Scope.MEDIA));

      response = handler.authorize(incomingRequest);

      verify(store).put(requestCaptor.capture());
      request = requestCaptor.getValue();
    }

    @Test
    void storesClientId() {
      assertThat(request.getClientId()).isEqualTo("https://the-client");
    }

    @Test
    void storesRedirectUri() {
      assertThat(request.getRedirectUri()).isEqualTo("https://to-you");
    }

    @Test
    void storesState() {
      assertThat(request.getState()).isEqualTo("the-state");
    }

    @Test
    void storesScopes() {
      assertThat(request.getScopes()).containsExactlyInAnyOrder(Scope.CREATE, Scope.MEDIA);
    }

    @Test
    void storesMe() {
      assertThat(request.getMe()).isEqualTo(ProfileUrl.of("https://me.me/"));
    }

    @Test
    void returnsGeneratedCode() {
      assertThat(response.getCode()).isEqualTo("the-code");
    }

    @Test
    void returnsRedirectUri() {
      assertThat(response.getRedirectUri()).isEqualTo("https://to-you");
    }

    @Test
    void returnsState() {
      assertThat(response.getState()).isEqualTo("the-state");
    }

    @Test
    void stateIsOptional() throws AuthorizationException {
      incomingRequest.setState(null);

      response = handler.authorize(incomingRequest);

      assertThat(response.getState()).isNull();
    }

    @Nested
    class PKCE {

      @BeforeEach
      void setup() {
        incomingRequest.setPKCE(new AuthorizationRequest.PKCE("challenge", "S1234"));
      }

      @Test
      void storesCodeChallenge() {
        assertThat(request.getPKCE().getCodeChallenge()).isEqualTo("challenge");
      }

      @Test
      void storesCodeMethod() {
        assertThat(request.getPKCE().getCodeChallengeMethod()).isEqualTo("S1234");
      }
    }
  }

  @Nested
  class GetGrantType {
    @Test
    void isAuthorizationCode() {
      assertThat(handler.getGrantType()).isEqualTo("authorization_code");
    }
  }

  @Nested
  class GetResponseType {
    @Test
    void isAuthorizationCode() {
      assertThat(handler.getResponseType()).contains("code");
    }
  }

  @Nested
  class ProfileExchange {

    private ProfileExchangeDto actual;
    @Mock private TokenGrantRequest request;

    @Nested
    class HappyPath {

      @BeforeEach
      void setup() throws InvalidAuthorizationCodeException {
        setUpStoreResponse();
        setUpRequest();
        when(pkceVerifier.verify(any(), any())).thenReturn(true);
        when(request.getCode()).thenReturn("code");

        actual = handler.profileExchange(request);
      }

      @Test
      void itDelegatesToStore() throws InvalidAuthorizationCodeException {
        verify(store).get("code");
      }

      @Test
      void returnsMe() {
        assertThat(actual.getMe()).isEqualTo("https://me/");
      }
    }

    @Nested
    class InvalidCode {
      @Test
      void throwsInvalidGrantException() throws InvalidAuthorizationCodeException {
        setUpStoreResponse();

        InvalidAuthorizationCodeException exception = new InvalidAuthorizationCodeException();
        when(store.get(any())).thenThrow(exception);

        assertThatThrownBy(() -> handler.profileExchange(request))
            .isInstanceOf(InvalidGrantException.class)
            .hasCause(exception);
      }
    }

    @Nested
    class IncorrectClientId {

      @ParameterizedTest
      @NullAndEmptySource
      @ValueSource(strings = {"https://client", "http://client"})
      void throwsInvalidGrantException(String clientId) throws InvalidAuthorizationCodeException {
        setUpStoreResponse();
        when(request.getClientId()).thenReturn(clientId);

        assertThatThrownBy(() -> handler.profileExchange(request))
            .isInstanceOf(InvalidGrantException.class);
      }
    }

    @Nested
    class IncorrectRedirectUri {
      @ParameterizedTest
      @NullAndEmptySource
      @ValueSource(
          strings = {"https://client", "http://to-me", "https://to-me", "https://to-me/to-you/"})
      void throwsInvalidGrantException(String redirectUri)
          throws InvalidAuthorizationCodeException {
        setUpStoreResponse();
        setUpRequest();
        when(request.getRedirectUri()).thenReturn(redirectUri);

        assertThatThrownBy(() -> handler.profileExchange(request))
            .isInstanceOf(InvalidGrantException.class);
      }
    }

    @Nested
    class PKCE {
      @Nested
      class WhenPresentInAuthorizationAndProfileRequests {

        @BeforeEach
        void setup() throws InvalidAuthorizationCodeException {
          AuthorizationRequest authzRequest = setUpStoreResponse();
          setUpRequest();
          setUpPkce(authzRequest);
        }

        @Test
        void delegatesToVerifier() {
          when(pkceVerifier.verify(any(), any())).thenReturn(true);

          handler.profileExchange(request);

          verify(pkceVerifier)
              .verify(
                  Optional.of(
                      new AuthorizationRequest.PKCE(
                          "OfYAxt8zU2dAPDWQxTAUIteRzMsoj9QBdMIVEDOErUo", "S256")),
                  Optional.of("a6128783714cfda1d388e2e98b6ae8221ac31aca31959e59512c59f5"));
        }

        @Test
        void throwsInvalidGrantExceptionWhenReturnsFalse() {
          when(pkceVerifier.verify(any(), any())).thenReturn(false);

          assertThatThrownBy(() -> handler.profileExchange(request))
              .isInstanceOf(InvalidGrantException.class);
        }
      }

      @Nested
      class WhenNotPresentInAuthorizationRequest {

        @Test
        void delegatesToVerifier() throws InvalidAuthorizationCodeException {
          setUpStoreResponse();
          setUpRequest();
          when(request.getCodeVerifier()).thenReturn("");
          when(pkceVerifier.verify(any(), any())).thenReturn(true);

          handler.profileExchange(request);

          verify(pkceVerifier).verify(Optional.empty(), Optional.of(""));
        }
      }

      @Nested
      class WhenNotPresentInProfileRequest {

        @Test
        void delegatesToVerifier() throws InvalidAuthorizationCodeException {
          AuthorizationRequest authzRequest = setUpStoreResponse();
          setUpRequest();
          authzRequest.setPKCE(new AuthorizationRequest.PKCE("challenge", "method"));
          when(pkceVerifier.verify(any(), any())).thenReturn(true);

          handler.profileExchange(request);

          verify(pkceVerifier)
              .verify(
                  Optional.of(new AuthorizationRequest.PKCE("challenge", "method")),
                  Optional.empty());
        }
      }

      private void setUpPkce(AuthorizationRequest authzRequest) {
        authzRequest.setPKCE(
            new AuthorizationRequest.PKCE("OfYAxt8zU2dAPDWQxTAUIteRzMsoj9QBdMIVEDOErUo", "S256"));
        when(request.getCodeVerifier())
            .thenReturn("a6128783714cfda1d388e2e98b6ae8221ac31aca31959e59512c59f5");
      }
    }

    private AuthorizationRequest setUpStoreResponse() throws InvalidAuthorizationCodeException {
      AuthorizationRequest authzRequest = new AuthorizationRequest();
      authzRequest.setClientId("https://client/");
      authzRequest.setMe(ProfileUrl.of("https://me"));
      authzRequest.setRedirectUri("https://to-me/to-you");
      authzRequest.setScopes(Collections.singleton(Scope.MEDIA));

      when(store.get(any())).thenReturn(authzRequest);

      return authzRequest;
    }

    private void setUpRequest() {
      when(request.getClientId()).thenReturn("https://client/");
      when(request.getRedirectUri()).thenReturn("https://to-me/to-you");
    }
  }

  @Nested
  class TokenExchange {

    private TokenExchangeDto actual;
    @Mock private TokenGrantRequest request;

    @Nested
    class HappyPath {

      @BeforeEach
      void setup() throws InvalidAuthorizationCodeException {
        setUpStoreResponse();
        setUpRequest();
        setUpToken();
        when(pkceVerifier.verify(any(), any())).thenReturn(true);
        when(request.getCode()).thenReturn("code");

        actual = handler.tokenExchange(request);
      }

      @Test
      void itDelegatesToStore() throws InvalidAuthorizationCodeException {
        verify(store).get("code");
      }

      @Test
      void itDelegatesToCreator() {
        verify(creator)
            .create(
                "https://client/",
                ProfileUrl.of("https://me/"),
                Collections.singleton(Scope.MEDIA),
                false);
      }

      @Test
      void returnsExpiresIn() {
        assertThat(actual.getExpiresIn()).isEqualTo(1234);
      }

      @Test
      void returnsMe() {
        assertThat(actual.getMe()).isEqualTo(ProfileUrl.of("https://me/"));
      }

      @Test
      void returnsScopes() {
        assertThat(actual.getScopes()).containsExactly(Scope.MEDIA);
      }

      @Test
      void returnsAccessToken() {
        assertThat(actual.getAccessToken()).isEqualTo("j.w.t");
      }
    }

    @Nested
    class RefreshToken {

      @BeforeEach
      void setup() throws InvalidAuthorizationCodeException {
        AuthorizationRequest storeRequest = setUpStoreResponse();
        storeRequest.setIssueRefreshToken(true);

        setUpRequest();

        Token token = setUpToken();
        token.setRefreshToken("j.w.t.2");
        when(pkceVerifier.verify(any(), any())).thenReturn(true);
        when(request.getCode()).thenReturn("code");

        actual = handler.tokenExchange(request);
      }

      @Test
      void itDelegatesToCreator() {
        verify(creator)
            .create(
                "https://client/",
                ProfileUrl.of("https://me"),
                Collections.singleton(Scope.MEDIA),
                true);
      }

      @Test
      void returnsRefreshToken() {
        assertThat(actual.getRefreshToken()).isEqualTo("j.w.t.2");
      }
    }

    @Nested
    class InvalidCode {
      @Test
      void throwsInvalidGrantException() throws InvalidAuthorizationCodeException {
        setUpStoreResponse();

        InvalidAuthorizationCodeException exception = new InvalidAuthorizationCodeException();
        when(store.get(any())).thenThrow(exception);

        assertThatThrownBy(() -> handler.tokenExchange(request))
            .isInstanceOf(InvalidGrantException.class)
            .hasCause(exception);
      }
    }

    @Nested
    class IncorrectClientId {

      @ParameterizedTest
      @NullAndEmptySource
      @ValueSource(strings = {"https://client", "http://client"})
      void throwsInvalidGrantException(String clientId) throws InvalidAuthorizationCodeException {
        setUpStoreResponse();
        when(request.getClientId()).thenReturn(clientId);

        assertThatThrownBy(() -> handler.tokenExchange(request))
            .isInstanceOf(InvalidGrantException.class);
      }
    }

    @Nested
    class IncorrectRedirectUri {
      @ParameterizedTest
      @NullAndEmptySource
      @ValueSource(
          strings = {"https://client", "http://to-me", "https://to-me", "https://to-me/to-you/"})
      void throwsInvalidGrantException(String redirectUri)
          throws InvalidAuthorizationCodeException {
        setUpStoreResponse();
        setUpRequest();
        when(request.getRedirectUri()).thenReturn(redirectUri);

        assertThatThrownBy(() -> handler.tokenExchange(request))
            .isInstanceOf(InvalidGrantException.class);
      }
    }

    @Nested
    class PKCE {
      @Nested
      class WhenPresentInAuthorizationAndTokenRequests {

        @BeforeEach
        void setup() throws InvalidAuthorizationCodeException {
          AuthorizationRequest authzRequest = setUpStoreResponse();
          setUpRequest();
          setUpPkce(authzRequest);
        }

        @Test
        void delegatesToVerifier() {
          setUpToken();
          when(pkceVerifier.verify(any(), any())).thenReturn(true);

          handler.tokenExchange(request);

          verify(pkceVerifier)
              .verify(
                  Optional.of(
                      new AuthorizationRequest.PKCE(
                          "OfYAxt8zU2dAPDWQxTAUIteRzMsoj9QBdMIVEDOErUo", "S256")),
                  Optional.of("a6128783714cfda1d388e2e98b6ae8221ac31aca31959e59512c59f5"));
        }

        @Test
        void throwsInvalidGrantExceptionWhenReturnsFalse() {
          when(pkceVerifier.verify(any(), any())).thenReturn(false);

          assertThatThrownBy(() -> handler.tokenExchange(request))
              .isInstanceOf(InvalidGrantException.class);
        }
      }

      @Nested
      class WhenNotPresentInAuthorizationRequest {

        @Test
        void delegatesToVerifier() throws InvalidAuthorizationCodeException {
          setUpStoreResponse();
          setUpRequest();
          when(request.getCodeVerifier()).thenReturn("");
          when(pkceVerifier.verify(any(), any())).thenReturn(true);
          setUpToken();

          handler.tokenExchange(request);

          verify(pkceVerifier).verify(Optional.empty(), Optional.of(""));
        }
      }

      @Nested
      class WhenNotPresentInTokenRequest {

        @Test
        void delegatesToVerifier() throws InvalidAuthorizationCodeException {
          AuthorizationRequest authzRequest = setUpStoreResponse();
          setUpRequest();
          authzRequest.setPKCE(new AuthorizationRequest.PKCE("challenge", "method"));
          when(pkceVerifier.verify(any(), any())).thenReturn(true);
          setUpToken();

          handler.tokenExchange(request);

          verify(pkceVerifier)
              .verify(
                  Optional.of(new AuthorizationRequest.PKCE("challenge", "method")),
                  Optional.empty());
        }
      }

      private void setUpPkce(AuthorizationRequest authzRequest) {
        authzRequest.setPKCE(
            new AuthorizationRequest.PKCE("OfYAxt8zU2dAPDWQxTAUIteRzMsoj9QBdMIVEDOErUo", "S256"));
        when(request.getCodeVerifier())
            .thenReturn("a6128783714cfda1d388e2e98b6ae8221ac31aca31959e59512c59f5");
      }
    }

    private AuthorizationRequest setUpStoreResponse() throws InvalidAuthorizationCodeException {
      AuthorizationRequest authzRequest = new AuthorizationRequest();
      authzRequest.setClientId("https://client/");
      authzRequest.setMe(ProfileUrl.of("https://me"));
      authzRequest.setRedirectUri("https://to-me/to-you");
      authzRequest.setScopes(Collections.singleton(Scope.MEDIA));

      when(store.get(any())).thenReturn(authzRequest);

      return authzRequest;
    }

    private void setUpRequest() {
      when(request.getClientId()).thenReturn("https://client/");
      when(request.getRedirectUri()).thenReturn("https://to-me/to-you");
    }

    private Token setUpToken() {
      Token token = new Token();
      token.setAccessToken("j.w.t");
      token.setRefreshToken("j.w.t.2");
      token.setExpiresIn(1234);

      when(creator.create(any(), any(), any(), anyBoolean())).thenReturn(token);

      return token;
    }
  }

  private Set<Scope> asSet(Scope... scopes) {
    return new HashSet<>(Arrays.asList(scopes));
  }
}
