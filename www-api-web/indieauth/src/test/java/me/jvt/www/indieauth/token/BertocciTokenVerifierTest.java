package me.jvt.www.indieauth.token;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import java.security.SecureRandom;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import me.jvt.www.indieauth.token.bertocci.BertocciTokenVerifier;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

class BertocciTokenVerifierTest {
  private static final JWSAlgorithm ALGORITHM = JWSAlgorithm.RS256;

  private static RSAKey key0;
  private static JWSSigner signer0;

  private static RSAKey key1;
  private static JWSSigner signer1;

  @BeforeAll
  static void setupClass() throws JOSEException {
    key0 = new RSAKeyGenerator(2048).algorithm(ALGORITHM).generate();
    signer0 = new RSASSASigner(key0);

    key1 = new RSAKeyGenerator(2048).algorithm(ALGORITHM).generate();
    signer1 = new RSASSASigner(key1);
  }

  private TokenVerifier verifier;

  @BeforeEach
  void setup() {
    JWKSet jwkSet = new JWKSet(Arrays.asList(key0, key1));
    verifier =
        new BertocciTokenVerifier(
            new ImmutableJWKSet<>(jwkSet), ALGORITHM, "https://iss", "wibble+jwt");
  }

  @ParameterizedTest
  @NullAndEmptySource
  void invalidTokenThrowsTokenVerificationException(String token) {
    assertThatThrownBy(() -> verifier.verify(token))
        .isInstanceOf(TokenVerificationException.class)
        .hasMessage("The token provided was not a valid JWT");
  }

  @Test
  void incorrectlySignedJwtThrowsTokenVerificationException() throws JOSEException {
    String token = macJwt().serialize();

    assertThatThrownBy(() -> verifier.verify(token))
        .isInstanceOf(TokenVerificationException.class)
        .hasMessage("The token provided was not correctly signed or formed");
  }

  @Test
  void returnsTokenAdapterWhenSignedJwt() throws TokenVerificationException {
    SignedJWT jwt = jwt();

    TokenAdapter actual = verifier.verify(jwt.serialize());

    assertThat(actual).isNotNull();
  }

  @Test
  void returnsTokenAdapterWhenSignedJwtOnDifferentKey() throws TokenVerificationException {
    SignedJWT jwt = jwt(signer1);

    TokenAdapter actual = verifier.verify(jwt.serialize());

    assertThat(actual).isNotNull();
  }

  @Test
  void invalidIssuerThrowsTokenVerificationException() {
    SignedJWT jwt = jwtWithIssuer();

    assertThatThrownBy(() -> verifier.verify(jwt.serialize()))
        .isInstanceOf(TokenVerificationException.class)
        .hasMessage("The token provided was not correctly signed or formed");
  }

  @ParameterizedTest
  @EmptySource
  @ValueSource(strings = "jwt")
  void invalidTypeThrowsTokenVerificationException(String type) {
    SignedJWT jwt = jwtWithType(type);

    assertThatThrownBy(() -> verifier.verify(jwt.serialize()))
        .isInstanceOf(TokenVerificationException.class)
        .hasMessage("The token provided was not correctly signed or formed");
  }

  private JWSObject macJwt() throws JOSEException {
    SecureRandom random = new SecureRandom();
    byte[] sharedSecret = new byte[32];
    random.nextBytes(sharedSecret);

    JWSSigner signer = new MACSigner(sharedSecret);

    JWSObject jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.HS256), new Payload(""));

    jwsObject.sign(signer);

    return jwsObject;
  }

  private SignedJWT jwtWithIssuer() {
    return jwt(signer0, "http://not-iss");
  }

  private SignedJWT jwtWithType(String jwt) {
    return jwt(signer0, "https://iss", jwt);
  }

  private SignedJWT jwt() {
    return jwt(signer0);
  }

  private SignedJWT jwt(JWSSigner signer) {
    return jwt(signer, "https://iss");
  }

  private SignedJWT jwt(JWSSigner signer, String issuer, String type) {
    SignedJWT jwt =
        new SignedJWT(
            new JWSHeader.Builder(JWSAlgorithm.RS256).type(new JOSEObjectType(type)).build(),
            new JWTClaimsSet.Builder()
                .claim("auth_time", Date.from(Instant.now()))
                .claim("token_type", "access_token")
                .issueTime(Date.from(Instant.now()))
                .expirationTime(Date.from(Instant.now()))
                .audience("aud")
                .subject("https://ignored/")
                .issuer(issuer)
                .build());
    try {
      jwt.sign(signer);
    } catch (JOSEException e) {
      throw new IllegalStateException(e);
    }

    return jwt;
  }

  private SignedJWT jwt(JWSSigner signer, String issuer) {
    return jwt(signer, issuer, "wibble+jwt");
  }
}
