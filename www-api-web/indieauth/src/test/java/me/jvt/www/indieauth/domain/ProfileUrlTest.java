package me.jvt.www.indieauth.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.MalformedURLException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

class ProfileUrlTest {
  @Nested
  class Of {
    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"tel:foo"})
    void throwsIllegalArgumentExceptionWhenInvalidUrl(String url) {
      assertThatThrownBy(() -> ProfileUrl.of(url))
          .isInstanceOf(IllegalArgumentException.class)
          .hasCauseInstanceOf(MalformedURLException.class);
    }

    @Test
    void itParsesWhenTrailingSlash() {
      var me = ProfileUrl.of("https://www.jvt.me/");

      assertThat(me.profileUrl()).hasToString("https://www.jvt.me/");
    }

    @Test
    void itAddsTrailingSlashWhenAbsent() {
      var me = ProfileUrl.of("https://www.staging.jvt.me");

      assertThat(me.profileUrl()).hasToString("https://www.staging.jvt.me/");
    }
  }
}
