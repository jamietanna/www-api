package me.jvt.www.indieauth.granttype.authorizationcode.pkce;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;
import me.jvt.www.indieauth.authorization.AuthorizationRequest;
import org.junit.jupiter.api.Test;

class PKCEVerifierTest {
  private static final String CHALLENGE = "OfYAxt8zU2dAPDWQxTAUIteRzMsoj9QBdMIVEDOErUo";
  private static final String METHOD = "S256";
  private static final String VERIFIER = "a6128783714cfda1d388e2e98b6ae8221ac31aca31959e59512c59f5";

  private final PKCEVerifier pkceVerifier = new PKCEVerifier();

  @Test
  void supportedMethods() {
    assertThat(pkceVerifier.getSupportedMethods()).containsExactly(METHOD);
  }

  @Test
  void supportsSomeMethods() {
    assertThat(pkceVerifier.supported(METHOD)).isTrue();
  }

  @Test
  void challengeAndMethodButNoVerifier() {
    boolean actual =
        pkceVerifier.verify(
            Optional.of(new AuthorizationRequest.PKCE(CHALLENGE, METHOD)), Optional.empty());

    assertThat(actual).isFalse();
  }

  @Test
  void noChallengeAndMethodButVerifier() {
    boolean actual = pkceVerifier.verify(Optional.empty(), Optional.of(VERIFIER));

    assertThat(actual).isFalse();
  }

  @Test
  void nonePresent() {
    boolean actual = pkceVerifier.verify(Optional.empty(), Optional.empty());

    assertThat(actual).isTrue();
  }

  @Test
  void otherMethodsNotSupported() {
    boolean actual =
        pkceVerifier.verify(
            Optional.of(new AuthorizationRequest.PKCE(CHALLENGE, "S512")), Optional.of(VERIFIER));

    assertThat(actual).isFalse();
  }

  @Test
  void paddingIsStripped() {
    boolean actual =
        pkceVerifier.verify(
            Optional.of(
                new AuthorizationRequest.PKCE(
                    "DwXhJ1p6ieibvO2BI_3Fu-FijjWwp5EKRBugL2oCw1w", "S256")),
            Optional.of("a4a03231cd563791a1862930fca82efa981159e2ea17300590a4d24757b4e77e"));
    assertThat(actual).isTrue();
  }
}
