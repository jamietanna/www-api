package me.jvt.www.indieauth.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import me.jvt.www.indieauth.domain.ProfileUrl;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;

@JsonTest
class ProfileUrlIntegrationTest {
  @Autowired private JacksonTester<ProfileUrl> jacksonTester;

  private JsonContent<ProfileUrl> asJson;

  @Nested
  class Serialises {
    @Test
    void asAString() throws IOException {
      asJson = jacksonTester.write(ProfileUrl.of("https://foo/"));

      assertThat(asJson).extractingJsonPathStringValue("$").isEqualTo("https://foo/");
    }
  }
}
