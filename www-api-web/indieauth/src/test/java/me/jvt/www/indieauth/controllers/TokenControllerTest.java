package me.jvt.www.indieauth.controllers;

import static com.github.valfirst.slf4jtest.LoggingEvent.info;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.github.valfirst.slf4jtest.*;
import java.util.Base64;
import java.util.Map;
import java.util.Optional;
import me.jvt.www.indieauth.token.TokenService;
import me.jvt.www.indieauth.token.model.TokenGrantDto;
import me.jvt.www.indieauth.token.model.TokenGrantRequest;
import me.jvt.www.indieauth.token.model.TokenVerificationDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@ExtendWith(MockitoExtension.class)
class TokenControllerTest {
  private final TestLogger logger = TestLoggerFactory.getTestLogger(TokenController.class);

  @Mock private TokenService service;

  @InjectMocks private TokenController controller;

  @Nested
  class Exchange {

    @Mock private TokenGrantDto response;
    @Captor private ArgumentCaptor<TokenGrantRequest> requestArgumentCaptor;
    private TokenGrantRequest request;
    protected ResponseEntity<TokenGrantDto> actual;

    @BeforeEach
    void setup() {
      when(service.tokenExchange(any())).thenReturn(response);

      executeAndCapture(Optional.of("verifier"), Optional.empty());
    }

    void execute(Optional<String> codeVerifier, Optional<String> basicAuth) {
      if (basicAuth.isPresent()) {
        String authorizationHeader =
            "Basic " + Base64.getEncoder().encodeToString(basicAuth.get().getBytes());
        basicAuth = Optional.of(authorizationHeader);
      }
      MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
      params.add("grant_type", "grant");
      params.add("code", "code");
      params.add("refresh_token", "rt");
      params.add("client_id", "https://client_id");
      params.add("redirect_uri", "https://to-me");
      codeVerifier.ifPresent(s -> params.add("code_verifier", s));

      actual = controller.exchange(params, basicAuth);
    }

    @Test
    void mapsGrantType() {
      assertThat(request.getGrantType()).isEqualTo("grant");
    }

    @Test
    void mapsCode() {
      assertThat(request.getCode()).isEqualTo("code");
    }

    @Test
    void mapsRefreshToken() {
      assertThat(request.getRefreshToken()).isEqualTo("rt");
    }

    @Test
    void mapsClientId() {
      assertThat(request.getClientId()).isEqualTo("https://client_id");
    }

    @ParameterizedTest
    @ValueSource(strings = {"client_id", "client_id:", "client_id:client_secret"})
    void prioritisesClientIdInAuthorizationHeaderWhenPresent(String plain) {
      executeAndCapture(Optional.of("verifier"), Optional.of(plain));

      verify(service, atLeastOnce()).tokenExchange(requestArgumentCaptor.capture());
      request = requestArgumentCaptor.getValue();
      assertThat(request.getClientId()).isEqualTo("client_id");
    }

    @Test
    void mapsRedirectUri() {
      assertThat(request.getRedirectUri()).isEqualTo("https://to-me");
    }

    @Test
    void mapsCodeVerifierIfPresent() {
      assertThat(request.getCodeVerifier()).isEqualTo("verifier");
    }

    @Test
    void doesNotMapCodeVerifierIfNotPresent() {
      executeAndCapture(Optional.empty(), Optional.empty());

      assertThat(request.getCodeVerifier()).isNull();
    }

    @Test
    void returnsEntityFromService() {
      assertThat(actual.getBody()).isSameAs(response);
    }

    @Test
    void returnsOk() {
      assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private void executeAndCapture(Optional<String> codeVerifier, Optional<String> basicAuth) {
      execute(codeVerifier, basicAuth);
      verify(service, atLeastOnce()).tokenExchange(requestArgumentCaptor.capture());
      request = requestArgumentCaptor.getValue();
    }
  }

  @Nested
  class Verify {
    private final TokenVerificationDto response = new TokenVerificationDto();
    private ResponseEntity<TokenVerificationDto> actual;

    @Nested
    class Get {
      @Nested
      class WhenActive {

        @BeforeEach
        void setup() {
          response.setActive(true);
          response.setClientId("https://client");
          when(service.verifyToken(any())).thenReturn(response);
          actual = controller.verifyGet("Bearer token");
        }

        @Test
        void itDelegatesToService() {
          verify(service).verifyToken("token");
        }

        @Test
        void returnsEntityFromService() {
          assertThat(actual.getBody()).isSameAs(response);
        }

        @Test
        void returnsOk() {
          assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.OK);
        }

        @Test
        void logsInformationAboutToken() {
          Assertions.assertThat(logger)
              .hasLogged(
                  info(
                      Map.of("introspect.clientId", "https://client"),
                      "Received a request to the IndieAuth token verify endpoint for client {}",
                      "https://client"));
        }
      }

      @Nested
      class WhenNotActive {

        @BeforeEach
        void setup() {
          response.setActive(false);
          when(service.verifyToken(any())).thenReturn(response);
          actual = controller.verifyGet("Bearer token");
        }

        @Test
        void itDelegatesToService() {
          verify(service).verifyToken("token");
        }

        @Test
        void hasNoBody() {
          assertThat(actual.getBody()).isNull();
        }

        @Test
        void returnsBadRequest() {
          assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        }
      }
    }

    @Nested
    class Post {
      @Nested
      class WhenActive {

        @BeforeEach
        void setup() {
          response.setActive(true);
          when(service.verifyToken(any())).thenReturn(response);
          actual = controller.verifyPost("token");
        }

        @Test
        void itDelegatesToService() {
          verify(service).verifyToken("token");
        }

        @Test
        void returnsEntityFromService() {
          assertThat(actual.getBody()).isSameAs(response);
        }

        @Test
        void returnsOk() {
          assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.OK);
        }
      }

      @Nested
      class WhenNotActive {

        @BeforeEach
        void setup() {
          response.setActive(false);
          when(service.verifyToken(any())).thenReturn(response);
          actual = controller.verifyPost("token");
        }

        @Test
        void itDelegatesToService() {
          verify(service).verifyToken("token");
        }

        @Test
        void returnsEntityFromService() {
          assertThat(actual.getBody()).isSameAs(response);
        }

        @Test
        void returnsOk() {
          assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.OK);
        }
      }
    }
  }
}
