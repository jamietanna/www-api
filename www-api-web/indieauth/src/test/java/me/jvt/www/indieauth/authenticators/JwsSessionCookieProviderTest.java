package me.jvt.www.indieauth.authenticators;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject.State;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.KeyLengthException;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import java.security.SecureRandom;
import java.text.ParseException;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import me.jvt.www.indieauth.authenticators.SessionCookieProvider.InvalidSessionCookieException;
import me.jvt.www.indieauth.authenticators.SessionCookieProvider.SessionCookie;
import me.jvt.www.indieauth.domain.ProfileUrl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class JwsSessionCookieProviderTest {
  private static final Instant NOW = Instant.now().truncatedTo(ChronoUnit.SECONDS);
  private static final Clock FIXED_CLOCK = Clock.fixed(NOW, ZoneId.of("UTC"));

  private JwsSessionCookieProvider provider;

  private static JWSSigner signer;
  private static byte[] secret;

  @BeforeAll
  static void setupClass() throws KeyLengthException {
    SecureRandom random = new SecureRandom();
    secret = new byte[32];
    random.nextBytes(secret);

    signer = new MACSigner(secret);
  }

  @BeforeEach
  void setup() {
    provider = new JwsSessionCookieProvider(secret, FIXED_CLOCK);
  }

  @Nested
  class Create {
    private SessionCookie actual;

    @BeforeEach
    void setup() {
      actual = provider.create(ProfileUrl.of("https://url"));
    }

    @Test
    void setsProfileUrl() {
      assertThat(actual.getProfileUrl()).isEqualTo(ProfileUrl.of("https://url"));
    }

    @Test
    void setsExpiryTo5Minutes() {
      assertThat(actual.getExpiry()).isEqualTo(Date.from(NOW.plus(5, ChronoUnit.MINUTES)));
    }

    @Test
    void isSignedJwt() throws ParseException {
      SignedJWT parsed = SignedJWT.parse(actual.serialize());

      assertThat(parsed.getState()).isEqualTo(State.SIGNED);
    }
  }

  @Nested
  class Validate {
    @ParameterizedTest
    @NullAndEmptySource
    void throwsInvalidSessionCookieExceptionWhenNullOrEmpty(String cookie) {
      assertThatThrownBy(() -> provider.validate(cookie))
          .isInstanceOf(InvalidSessionCookieException.class);
    }

    @Test
    void throwsInvalidSessionCookieExceptionWhenNotSignedCorrectly() throws JOSEException {
      SecureRandom random = new SecureRandom();
      byte[] sharedSecret = new byte[32];
      random.nextBytes(sharedSecret);
      JWSSigner wrongSigner = new MACSigner(sharedSecret);
      SignedJWT jwt =
          new SignedJWT(
              new JWSHeader(JWSAlgorithm.HS256),
              new JWTClaimsSet.Builder()
                  .expirationTime(Date.from(Instant.now(FIXED_CLOCK).plus(1, ChronoUnit.SECONDS)))
                  .build());
      jwt.sign(wrongSigner);

      assertThatThrownBy(() -> provider.validate(jwt.serialize()))
          .isInstanceOf(InvalidSessionCookieException.class);
    }

    @Test
    void throwsInvalidSessionCookieExceptionWhenCurrentDateIsAfterExpiry() throws JOSEException {
      SignedJWT jwt =
          new SignedJWT(
              new JWSHeader(JWSAlgorithm.HS256),
              new JWTClaimsSet.Builder()
                  .expirationTime(Date.from(Instant.now(FIXED_CLOCK).minus(1, ChronoUnit.SECONDS)))
                  .build());
      jwt.sign(signer);

      assertThatThrownBy(() -> provider.validate(jwt.serialize()))
          .isInstanceOf(InvalidSessionCookieException.class);
    }

    @Test
    void returnsSessionCookieIfValid() throws JOSEException, InvalidSessionCookieException {
      SignedJWT jwt =
          new SignedJWT(
              new JWSHeader(JWSAlgorithm.HS256),
              new JWTClaimsSet.Builder()
                  .expirationTime(Date.from(Instant.now(FIXED_CLOCK).plus(1, ChronoUnit.SECONDS)))
                  .claim("profile_url", "https://foo")
                  .build());
      jwt.sign(signer);

      SessionCookie actual = provider.validate(jwt.serialize());

      assertThat(actual.getProfileUrl()).isEqualTo(ProfileUrl.of("https://foo"));
    }
  }
}
