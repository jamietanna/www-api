package me.jvt.www.indieauth.authorization;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.function.Supplier;
import me.jvt.www.indieauth.authorizationservermetadata.ServerConfigurationDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RedirectUriProviderTest {
  private static final String REDIRECT = "https://them/foo";

  @Mock private AuthorizationRequest request;
  @Mock private Supplier<ServerConfigurationDto> configurationSupplier;
  @Mock private ServerConfigurationDto serverConfiguration;

  @InjectMocks private RedirectUriProvider provider;

  @BeforeEach
  void setup() {
    when(configurationSupplier.get()).thenReturn(serverConfiguration);
    when(serverConfiguration.getIssuer()).thenReturn("https://foo");
    when(request.getRedirectUri()).thenReturn(REDIRECT);
  }

  @Nested
  class AccessDenied {
    @Test
    void whenNoState() {
      String actual = provider.accessDenied(request);

      assertThat(actual).isEqualTo(REDIRECT + "?error=access_denied&iss=https://foo");
    }

    @Test
    void whenState() {
      when(request.getState()).thenReturn("the-state");

      String actual = provider.accessDenied(request);

      assertThat(actual)
          .isEqualTo(REDIRECT + "?error=access_denied&iss=https://foo&state=the-state");
    }
  }

  @Nested
  class Code {
    @Test
    void whenNoState() {
      String actual = provider.code(request, "code123");

      assertThat(actual).isEqualTo(REDIRECT + "?code=code123&iss=https://foo");
    }

    @Test
    void whenState() {
      when(request.getState()).thenReturn("the-state");
      String actual = provider.code(request, "code123");

      assertThat(actual).isEqualTo(REDIRECT + "?code=code123&iss=https://foo&state=the-state");
    }
  }

  @Nested
  class InvalidRequest {
    @Test
    void whenNoState() {
      String actual = provider.invalidRequest(request);

      assertThat(actual).isEqualTo(REDIRECT + "?error=invalid_request&iss=https://foo");
    }

    @Test
    void whenState() {
      when(request.getState()).thenReturn("the-state");

      String actual = provider.invalidRequest(request);

      assertThat(actual)
          .isEqualTo(REDIRECT + "?error=invalid_request&iss=https://foo&state=the-state");
    }
  }

  @Nested
  class HandleAuthorizationException {
    @Mock private AuthorizationException exception;

    @BeforeEach
    void setup() {
      when(exception.getError()).thenReturn("some_error");
      when(exception.getErrorDescription()).thenReturn("Error for developers");
    }

    @Test
    void returnsRedirectUri() {
      String actual = provider.handleAuthorizationException(request, exception);

      assertThat(actual).startsWith(REDIRECT + "?");
    }

    @Test
    void includesIssuer() {
      String actual = provider.handleAuthorizationException(request, exception);

      assertThat(actual).contains("&iss=https://foo");
    }

    @Test
    void returnsErrorFromException() {

      String actual = provider.handleAuthorizationException(request, exception);

      assertThat(actual).contains("error=some_error");
    }

    @Test
    void encodesErrorDescriptionFromException() {

      String actual = provider.handleAuthorizationException(request, exception);

      assertThat(actual).contains("error_description=Error+for+developers");
    }

    @Test
    void whenState() {
      when(request.getState()).thenReturn("the-state");

      String actual = provider.handleAuthorizationException(request, exception);

      assertThat(actual).contains("&state=the-state");
    }
  }

  @Nested
  class ServerError {
    @Test
    void whenNoState() {
      String actual = provider.serverError(request);

      assertThat(actual).isEqualTo(REDIRECT + "?error=server_error&iss=https://foo");
    }

    @Test
    void whenState() {
      when(request.getState()).thenReturn("the-state");

      String actual = provider.serverError(request);

      assertThat(actual)
          .isEqualTo(REDIRECT + "?error=server_error&iss=https://foo&state=the-state");
    }
  }
}
