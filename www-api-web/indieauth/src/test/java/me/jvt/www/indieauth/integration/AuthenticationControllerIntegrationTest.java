package me.jvt.www.indieauth.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.Optional;
import javax.servlet.http.Cookie;
import me.jvt.www.indieauth.authenticators.AuthenticationNotSupportedException;
import me.jvt.www.indieauth.authenticators.AuthenticationService;
import me.jvt.www.indieauth.authenticators.Context;
import me.jvt.www.indieauth.controllers.AuthenticationController;
import me.jvt.www.indieauth.domain.ProfileUrl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureMockMvc
@WebMvcTest(AuthenticationController.class)
@ExtendWith(SpringExtension.class)
class AuthenticationControllerIntegrationTest {
  @Autowired private MockMvc mvc;
  @MockBean private AuthenticationService service;

  @Nested
  class ShowAccounts {
    @Test
    void isMappedToRootUri() throws Exception {
      mvc.perform(get("/authenticate")).andExpect(status().isOk());
    }

    @Test
    void whenCookieIsNotSet() throws Exception {
      mvc.perform(get("/authenticate"));

      verify(service, atLeastOnce()).profileUrlFromCookie(Optional.empty());
    }

    @Test
    void whenCookieIsSet() throws Exception {
      mvc.perform(get("/authenticate").cookie(cookie()));

      verify(service, atLeastOnce()).profileUrlFromCookie(Optional.of("foo"));
    }
  }

  @Nested
  class ShowAuthenticationForUser {
    @BeforeEach
    void setup() throws AuthenticationNotSupportedException {
      when(service.authenticatorsForMe(any())).thenReturn(Collections.emptyList());
    }

    @Test
    void isMappedToPath() throws Exception {
      mvc.perform(builder()).andExpect(status().isOk());
    }

    @Test
    void resolvesMeFromQueryString() throws Exception {
      mvc.perform(builder());

      verify(service, atLeastOnce()).authenticatorsForMe(ProfileUrl.of("https://foo/"));
    }

    private MockHttpServletRequestBuilder builder() {
      return get("/authenticate/start").queryParam("me", "https://foo/");
    }
  }

  @Nested
  class AuthenticateAndSetCookie {
    @Captor private ArgumentCaptor<Context> contextCaptor;

    @Test
    void isMappedToPath() throws Exception {
      mvc.perform(builder()).andExpect(status().isFound());
    }

    @Test
    void resolvesMeFromParam() throws Exception {
      mvc.perform(builder());

      verify(service, atLeastOnce()).authenticate(any(), eq(ProfileUrl.of("https://foo")), any());
    }

    @Test
    void resolvesAuthenticatorIdFromParam() throws Exception {
      mvc.perform(builder());

      verify(service, atLeastOnce()).authenticate(eq("123"), any(), any());
    }

    @Test
    void resolvesXForwardedForIfSet() throws Exception {
      mvc.perform(builder().header("x-forwarded-for", "ip"));

      verify(service, atLeastOnce()).authenticate(any(), any(), contextCaptor.capture());

      assertThat(contextCaptor.getValue().getXForwardedFor()).isEqualTo("ip");
    }

    @Test
    void doesNotResolveXForwardedForIfNotSet() throws Exception {
      mvc.perform(builder());

      verify(service, atLeastOnce()).authenticate(any(), any(), contextCaptor.capture());

      assertThat(contextCaptor.getValue().getXForwardedFor()).isNull();
    }

    @Test
    void itResolvesUserAgent() throws Exception {
      mvc.perform(builder());

      verify(service, atLeastOnce()).authenticate(any(), any(), contextCaptor.capture());

      assertThat(contextCaptor.getValue().getUserAgent()).isEqualTo("The Agent");
    }

    @Test
    void resolvesRedirectUriIfPresent() throws Exception {
      mvc.perform(builder().cookie(new Cookie("redirect_uri", "https://away")))
          .andExpect(redirectedUrl("https://away"));
    }

    @Test
    void redirectsToRootIfRedirectUriNotPresent() throws Exception {
      mvc.perform(builder()).andExpect(redirectedUrl("/authenticate"));
    }

    @Test
    void setsCookie() throws Exception {
      mvc.perform(builder())
          .andExpect(MockMvcResultMatchers.cookie().exists("indieauth-authentication"));
    }

    private MockHttpServletRequestBuilder builder() {
      return post("/authenticate")
          .param("authenticator_id", "123")
          .param("me", "https://foo")
          .header("user-agent", "The Agent");
    }
  }

  private Cookie cookie() {
    return new Cookie("indieauth-authentication", "foo");
  }
}
