package me.jvt.www.indieauth.authenticators.oktaverify;

import static org.assertj.core.api.Assertions.assertThat;

import com.okta.sdk.resource.user.factor.VerifyUserFactorResponse;
import com.okta.sdk.resource.user.factor.VerifyUserFactorResponse.FactorResultEnum;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

class VerificationResultTest {
  @ParameterizedTest
  @EnumSource(VerifyUserFactorResponse.FactorResultEnum.class)
  void allHaveMappings(VerifyUserFactorResponse.FactorResultEnum factorResultEnum) {
    VerificationResult result = VerificationResult.fromFactorResultEnum(factorResultEnum);

    assertThat(result).isNotNull();
  }

  @Test
  void waiting() {
    VerificationResult result = VerificationResult.fromFactorResultEnum(FactorResultEnum.WAITING);

    assertThat(result).isEqualTo(VerificationResult.WAITING);
  }

  @Test
  void success() {
    VerificationResult result = VerificationResult.fromFactorResultEnum(FactorResultEnum.SUCCESS);

    assertThat(result).isEqualTo(VerificationResult.SUCCESS);
  }

  @ParameterizedTest
  @EnumSource(
      value = FactorResultEnum.class,
      names = {
        "EXPIRED",
        "CHALLENGE",
        "FAILED",
        "REJECTED",
        "TIMEOUT",
        "TIME_WINDOW_EXCEEDED",
        "PASSCODE_REPLAYED"
      })
  void clientError(FactorResultEnum factorResultEnum) {
    VerificationResult result = VerificationResult.fromFactorResultEnum(factorResultEnum);

    assertThat(result).isEqualTo(VerificationResult.CLIENT_ERROR);
  }

  @Test
  void internalError() {
    VerificationResult result = VerificationResult.fromFactorResultEnum(FactorResultEnum.ERROR);

    assertThat(result).isEqualTo(VerificationResult.INTERNAL_ERROR);
  }
}
