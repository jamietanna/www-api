package me.jvt.www.indieauth.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.nimbusds.jose.jwk.JWKSet;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class JwksControllerTest {
  @Mock private JWKSet jwks;
  @InjectMocks private JwksController controller;

  @Nested
  class RetrieveKeys {
    @Test
    void returnsKeysFromConstructor() {
      when(jwks.toString(anyBoolean())).thenReturn("the keys");

      assertThat(controller.retrieveKeys()).isEqualTo("the keys");
    }

    @Test
    void onlyReturnsPublicKeys() {
      when(jwks.toString(anyBoolean())).thenReturn("the keys");

      assertThat(controller.retrieveKeys()).isNotNull();

      verify(jwks).toString(true);
    }
  }
}
