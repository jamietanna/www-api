package me.jvt.www.indieauth;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import me.jvt.www.indieauth.exception.OAuthErrorDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class OAuthErrorDtoTest {

  private static final ObjectMapper mapper = new ObjectMapper();

  private ObjectNode node;

  @BeforeEach
  void setup() throws JsonProcessingException {
    OAuthErrorDto dto = new OAuthErrorDto();
    dto.setError("invalid_request");
    dto.setErrorDescription("You dun messed up, A. A. Ron");

    String actual = mapper.writeValueAsString(dto);
    node = mapper.readValue(actual, ObjectNode.class);
  }

  @Test
  void errorIsMapped() {
    assertThat(node.get("error").textValue()).isEqualTo("invalid_request");
  }

  @Test
  void errorDescriptionIsMapped() {
    assertThat(node.get("error_description").textValue()).isEqualTo("You dun messed up, A. A. Ron");
  }

  @Test
  void errorDescriptionIsNotMappedWhenNull() throws JsonProcessingException {
    OAuthErrorDto dto = new OAuthErrorDto();
    dto.setError("invalid_request");

    String actual = mapper.writeValueAsString(dto);
    node = mapper.readValue(actual, ObjectNode.class);

    assertThat(node.get("error_description")).isNull();
  }
}
