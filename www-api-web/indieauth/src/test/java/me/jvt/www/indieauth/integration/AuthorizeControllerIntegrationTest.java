package me.jvt.www.indieauth.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Set;
import javax.servlet.http.Cookie;
import me.jvt.www.indieauth.authenticators.AuthenticationNotSupportedException;
import me.jvt.www.indieauth.authenticators.Authenticator;
import me.jvt.www.indieauth.authenticators.Context;
import me.jvt.www.indieauth.authenticators.InternalAuthenticationException;
import me.jvt.www.indieauth.authenticators.SessionCookieProvider;
import me.jvt.www.indieauth.authenticators.SessionCookieProvider.InvalidSessionCookieException;
import me.jvt.www.indieauth.authenticators.SessionCookieProvider.SessionCookie;
import me.jvt.www.indieauth.authorization.AuthorizationException;
import me.jvt.www.indieauth.authorization.AuthorizationRequest;
import me.jvt.www.indieauth.authorization.AuthorizationResponseDto;
import me.jvt.www.indieauth.authorization.PushedAuthorizationRepository;
import me.jvt.www.indieauth.authorization.RedirectUriProvider;
import me.jvt.www.indieauth.controllers.AuthorizeController;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.exception.InvalidGrantException;
import me.jvt.www.indieauth.token.TokenService;
import me.jvt.www.indieauth.token.model.ProfileExchangeDto;
import me.jvt.www.indieauth.token.model.Scope;
import me.jvt.www.indieauth.token.model.TokenGrantRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@AutoConfigureMockMvc
@WebMvcTest(AuthorizeController.class)
@ExtendWith(SpringExtension.class)
class AuthorizeControllerIntegrationTest {

  private static final Cookie COOKIE = new Cookie("indieauth-authentication", "value");

  @Autowired private MockMvc mvc;

  @MockBean private Authenticator authenticator;
  @MockBean private PushedAuthorizationRepository requestRepository;
  @MockBean private TokenService service;
  @MockBean private RedirectUriProvider redirectUriProvider;
  @MockBean private SessionCookieProvider sessionCookieProvider;
  @Mock private SessionCookie cookie;
  @Captor private ArgumentCaptor<AuthorizationRequest> requestCaptor;

  private AuthorizationRequest request;
  private ResultActions response;

  @Nested
  class Get {

    @Nested
    class HappyPath {

      @BeforeEach
      void setup() throws Exception {
        when(requestRepository.put(any())).thenReturn("urn:1234");

        authorize();

        verify(service, atLeastOnce()).preauthorize(requestCaptor.capture());
        request = requestCaptor.getValue();
      }

      @Test
      void responseTypeIsMapped() {
        assertThat(request.getResponseType()).isEqualTo("code");
      }

      @Test
      void clientIdIsMapped() {
        assertThat(request.getClientId()).isEqualTo("https://client");
      }

      @Test
      void redirectUriIsMapped() {
        assertThat(request.getRedirectUri()).isEqualTo("https://to-you");
      }

      @Test
      void stateIsMappedIfPresent() {
        assertThat(request.getState()).isEqualTo("the-state");
      }

      @Test
      void codeChallengeIsMappedIfPresent() {
        assertThat(request.getPKCE().getCodeChallenge()).isEqualTo("challenge");
      }

      @Test
      void codeChallengeMethodIsMappedIfPresent() {
        assertThat(request.getPKCE().getCodeChallengeMethod()).isEqualTo("S1234");
      }

      @Test
      void scopesAreParsedAndSetIfPresent() {
        assertThat(request.getScopes()).containsExactlyInAnyOrder(Scope.CREATE, Scope.UPDATE);
      }

      @Test
      void meIsMappedIfPresent() {
        assertThat(request.getMe()).isEqualTo(ProfileUrl.of("https://me/"));
      }

      @Test
      void redirectsToConsentUsingRequestUri() throws Exception {
        response.andExpect(status().isFound());
      }
    }

    private void authorize() throws Exception {
      MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
      params.add("response_type", "code");
      params.add("client_id", "https://client");
      params.add("redirect_uri", "https://to-you");
      params.add("state", "the-state");
      params.add("code_challenge", "challenge");
      params.add("code_challenge_method", "S1234");
      params.add("scope", "create update");
      params.add("me", "https://me/");

      response = mvc.perform(get("/authorize").params(params));
    }
  }

  @Nested
  class ProfileExchange {

    @Captor private ArgumentCaptor<TokenGrantRequest> requestCaptor;

    @Nested
    class HappyPath {

      private ResultActions actions;
      private TokenGrantRequest request;

      @BeforeEach
      void setup() throws Exception {
        reset(service);
        when(service.profileExchange(any()))
            .thenReturn(new ProfileExchangeDto(ProfileUrl.of("https://bar/")));

        actions =
            mvc.perform(
                post("/authorize")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .param("other", "defined"));

        verify(service).profileExchange(requestCaptor.capture());
        request = requestCaptor.getValue();
      }

      @Test
      void itReturnsOk() throws Exception {
        actions.andExpect(status().isOk());
      }

      @Test
      void itReturnsMe() throws Exception {
        actions.andExpect(jsonPath("$.me").value("https://bar/"));
      }

      @Test
      void itMapsRequestBodyForDelegate() {
        assertThat(request).containsEntry("other", "defined");
      }
    }

    @Nested
    class InvalidGrant {

      private ResultActions actions;

      @BeforeEach
      void setup() throws Exception {
        reset(service);
        when(service.profileExchange(any())).thenThrow(new InvalidGrantException());

        actions =
            mvc.perform(
                post("/authorize")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .param("code", "foo"));
      }

      @Test
      void itReturnsBadRequestWhenInvalidGrant() throws Exception {
        actions.andExpect(status().isBadRequest());
      }

      @Test
      void itReturnsErrorWhenInvalidGrant() throws Exception {
        actions.andExpect(jsonPath("$.error").value("invalid_grant"));
      }
    }
  }

  @Nested
  class SubmitConsent {
    @Mock private AuthorizationRequest request;
    @Captor private ArgumentCaptor<Set<Scope>> scopeCaptor;

    @Nested
    class HappyPath {
      @Captor private ArgumentCaptor<Context> contextCaptor;

      @BeforeEach
      void setup() throws Exception {
        prepare();
        MockHttpServletRequestBuilder builder =
            submitConsent()
                .queryParam("issue_refresh_token", "true")
                .header("x-forwarded-for", "127.0.0.1")
                .cookie(COOKIE);

        perform(builder);
      }

      @Test
      void requestUriIsMapped() throws AuthorizationException {
        verify(requestRepository).pop("urn:abc:123");
      }

      @Test
      void scopesAreMapped() throws AuthorizationException {
        verify(request).setScopes(scopeCaptor.capture());

        Set<Scope> scopes = scopeCaptor.getValue();
        assertThat(scopes).containsExactlyInAnyOrder(new Scope("example"), new Scope("another"));
      }

      @Test
      void issueRefreshTokenIsMapped() throws AuthorizationException {
        verify(request).setIssueRefreshToken(true);
      }

      @Test
      void sessionCookieIsMapped() throws InvalidSessionCookieException {
        verify(sessionCookieProvider, atLeastOnce()).validate("value");
      }

      @Test
      void redirectsToAuthenticationUsingRequestUri() throws Exception {
        response.andExpect(redirectedUrl("code"));
      }
    }

    @Test
    void issueRefreshTokenIsMappedToFalseWhenAbsent() throws Exception {
      prepare();
      perform(submitConsent());

      verify(request).setIssueRefreshToken(false);
    }

    @Test
    void sessionCookieIsOptional() throws Exception {
      prepare();
      perform(submitConsent());

      response.andExpect(redirectedUrl("code"));
    }

    @Test
    void scopesAreOptional() throws Exception {
      prepare();
      MockHttpServletRequestBuilder builder =
          post("/authorize/consent")
              .queryParam("request_uri", "urn:abc:123")
              .header("user-agent", "browser 123");
      perform(builder);

      verify(request).setScopes(scopeCaptor.capture());
      Set<Scope> scopes = scopeCaptor.getValue();
      assertThat(scopes).isEmpty();
    }

    private void prepare()
        throws AuthorizationException, InternalAuthenticationException,
            AuthenticationNotSupportedException, InvalidSessionCookieException {
      Mockito.reset(authenticator);
      Mockito.reset(requestRepository);
      when(requestRepository.pop(any())).thenReturn(request);
      when(request.getMe()).thenReturn(ProfileUrl.of("https://me-hint/"));
      AuthorizationResponseDto response = new AuthorizationResponseDto();
      response.setRedirectUri("https://url/foo");
      response.setCode("the-code");
      when(service.authorize(any())).thenReturn(response);
      when(authenticator.authenticate(any(), any())).thenReturn(true);
      when(authenticator.id()).thenReturn("id");
      when(redirectUriProvider.code(any(), any())).thenReturn("code");
      when(sessionCookieProvider.validate(any())).thenReturn(cookie);
      when(cookie.getProfileUrl()).thenReturn(ProfileUrl.of("https://me/"));
    }

    private MockHttpServletRequestBuilder submitConsent() throws Exception {
      return post("/authorize/consent")
          .param("request_uri", "urn:abc:123")
          .param("scope", "another")
          .param("scope", "example")
          .header("user-agent", "browser 123");
    }

    private void perform(MockHttpServletRequestBuilder builder) throws Exception {
      response = mvc.perform(builder).andDo(MockMvcResultHandlers.print());
    }
  }

  @Nested
  class Consent {

    @Mock private AuthorizationRequest request;

    @BeforeEach
    void setup() {
      Mockito.reset(requestRepository);
      Mockito.reset(sessionCookieProvider);
    }

    @Test
    void pullsRequestUriFromParam() throws Exception {
      when(requestRepository.get(any())).thenReturn(request);
      when(sessionCookieProvider.validate(any())).thenReturn(cookie);
      when(cookie.getProfileUrl()).thenReturn(ProfileUrl.of("https://foo/"));

      response =
          mvc.perform(
              get("/authorize/consent").queryParam("request_uri", "urn:abc:123").cookie(COOKIE));

      verify(requestRepository).get("urn:abc:123");
    }

    @Test
    void sessionCookieIsOptionalAndRedirectsToAuthenticate() throws Exception {
      when(requestRepository.get(any())).thenReturn(request);
      when(sessionCookieProvider.validate(any())).thenThrow(new InvalidSessionCookieException());

      mvc.perform(get("/authorize/consent").queryParam("request_uri", "urn:abc:123"))
          .andExpect(redirectedUrl("/authenticate"));
    }

    @Test
    void sessionCookieIsOptionalAndAddsRedirectUriCookie() throws Exception {
      when(requestRepository.get(any())).thenReturn(request);
      when(sessionCookieProvider.validate(any())).thenThrow(new InvalidSessionCookieException());

      mvc.perform(get("/authorize/consent").queryParam("request_uri", "urn:abc:123"))
          .andExpect(
              MockMvcResultMatchers.cookie()
                  .value("redirect_uri", "/authorize/consent?request_uri=urn:abc:123"));
    }
  }

  @Nested
  class Reject {
    @Nested
    class HappyPath {
      @Mock private AuthorizationRequest request;

      @BeforeEach
      void setup() throws AuthorizationException {
        Mockito.reset(requestRepository);
        when(requestRepository.get(any())).thenReturn(request);
        when(request.getRedirectUri()).thenReturn("https://url/foo");
      }

      @Test
      void requestUriIsMapped() throws Exception {
        mvc.perform(post("/authorize/deny").queryParam("request_uri", "urn:abc:123"));

        verify(requestRepository).pop("urn:abc:123");
      }

      @Test
      void redirects() throws Exception {
        when(redirectUriProvider.accessDenied(any())).thenReturn("error");

        mvc.perform(post("/authorize/deny").queryParam("request_uri", "urn:abc:123"))
            .andExpect(redirectedUrl("error"));
      }
    }
  }
}
