package me.jvt.www.indieauth.token;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import me.jvt.www.indieauth.authorization.AuthorizationException;
import me.jvt.www.indieauth.authorization.AuthorizationRequest;
import me.jvt.www.indieauth.authorization.AuthorizationResponseDto;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.exception.UnsupportedGrantTypeException;
import me.jvt.www.indieauth.granttype.GrantTypeHandler;
import me.jvt.www.indieauth.token.model.ProfileExchangeDto;
import me.jvt.www.indieauth.token.model.Scope;
import me.jvt.www.indieauth.token.model.TokenExchangeDto;
import me.jvt.www.indieauth.token.model.TokenGrantDto;
import me.jvt.www.indieauth.token.model.TokenGrantRequest;
import me.jvt.www.indieauth.token.model.TokenVerificationDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TokenServiceTest {
  @Mock private TokenVerifier verifier;
  @Mock private GrantTypeHandler handler0;
  @Mock private GrantTypeHandler handler1;
  @Mock private TokenExchangeDto exchanged;

  private TokenService service;

  @BeforeEach
  void setup() {
    service = new TokenService(verifier, Arrays.asList(handler0, handler1));
  }

  @Test
  void cannotBeConstructedWithEmptyHandlers() {
    assertThatThrownBy(() -> new TokenService(verifier, Collections.emptyList()))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("TokenService requires at least one GrantTypeHandler");
  }

  @Nested
  class PreAuthorize {

    @Mock private AuthorizationRequest request;
    @Mock private AuthorizationResponseDto response;

    @Nested
    class HappyPath {

      @BeforeEach
      void setup() throws AuthorizationException {
        when(handler0.getResponseType()).thenReturn(Optional.of("code"));
        when(request.getResponseType()).thenReturn("code");

        service.preauthorize(request);
      }

      @Test
      void itDelegatesToHandler() throws AuthorizationException {
        verify(handler0).preauthorize(request);
      }
    }

    @Nested
    class AnotherGrantType {
      @Test
      void itDelegates() throws AuthorizationException {
        when(handler0.getResponseType()).thenReturn(Optional.of("code"));
        when(handler1.getResponseType()).thenReturn(Optional.of("profile"));

        when(request.getResponseType()).thenReturn("profile");

        service.preauthorize(request);

        verify(handler1).preauthorize(request);
      }
    }

    @Test
    void ignoresIfResponseTypeIsEmpty() throws AuthorizationException {
      when(handler0.getResponseType()).thenReturn(Optional.empty());
      when(handler1.getResponseType()).thenReturn(Optional.of("profile"));

      when(request.getResponseType()).thenReturn("profile");

      service.preauthorize(request);

      verify(handler1).preauthorize(request);
    }

    @Nested
    class UnsupportedGrantType {

      @Test
      void throwsAuthorizationExceptionWithUnsupportedResponseType() {
        when(handler0.getResponseType()).thenReturn(Optional.of("code"));
        when(handler1.getResponseType()).thenReturn(Optional.of("profile"));
        when(request.getResponseType()).thenReturn("foo");

        AuthorizationException expected = new AuthorizationException("unsupported_response_type");

        assertThatThrownBy(() -> service.preauthorize(request)).isEqualTo(expected);
      }
    }
  }

  @Nested
  class Authorize {

    private AuthorizationResponseDto actual;
    @Mock private AuthorizationRequest request;
    @Mock private AuthorizationResponseDto response;

    @Nested
    class HappyPath {

      @BeforeEach
      void setup() throws AuthorizationException {
        when(handler0.getResponseType()).thenReturn(Optional.of("code"));
        when(request.getResponseType()).thenReturn("code");
        when(handler0.authorize(any())).thenReturn(response);

        when(handler0.authorize(any())).thenReturn(response);

        actual = service.authorize(request);
      }

      @Test
      void itDelegatesToHandler() throws AuthorizationException {
        verify(handler0).authorize(request);
      }

      @Test
      void itReturnsFromDelegate() {
        assertThat(actual).isSameAs(response);
      }
    }

    @Nested
    class AnotherGrantType {
      @Test
      void itDelegates() throws AuthorizationException {
        when(handler0.getResponseType()).thenReturn(Optional.of("code"));
        when(handler1.getResponseType()).thenReturn(Optional.of("profile"));

        when(handler1.authorize(any())).thenReturn(response);
        when(request.getResponseType()).thenReturn("profile");

        service.authorize(request);

        verify(handler1).authorize(request);
      }
    }

    @Test
    void ignoresIfResponseTypeIsEmpty() throws AuthorizationException {
      when(handler0.getResponseType()).thenReturn(Optional.empty());
      when(handler1.getResponseType()).thenReturn(Optional.of("profile"));

      when(request.getResponseType()).thenReturn("profile");

      service.authorize(request);

      verify(handler1).authorize(request);
    }

    @Nested
    class UnsupportedGrantType {

      @Test
      void throwsAuthorizationExceptionWithUnsupportedResponseType() {
        when(handler0.getResponseType()).thenReturn(Optional.of("code"));
        when(handler1.getResponseType()).thenReturn(Optional.of("profile"));
        when(request.getResponseType()).thenReturn("foo");

        AuthorizationException expected = new AuthorizationException("unsupported_response_type");

        assertThatThrownBy(() -> service.authorize(request)).isEqualTo(expected);
      }
    }
  }

  @Nested
  class ProfileExchange {

    private ProfileExchangeDto actual;
    @Mock private TokenGrantRequest request;
    @Mock private ProfileExchangeDto exchanged;

    @Nested
    class HappyPath {

      @BeforeEach
      void setup() {
        when(handler0.getGrantType()).thenReturn("authorization_code");
        when(request.getGrantType()).thenReturn("authorization_code");
        when(handler0.profileExchange(any())).thenReturn(exchanged);

        actual = service.profileExchange(request);
      }

      @Test
      void itDelegatesToHandler() {
        verify(handler0).profileExchange(request);
      }

      @Test
      void returnsMe() {
        assertThat(actual).isEqualTo(exchanged);
      }
    }

    @Nested
    class AnotherGrantType {
      @Test
      void itDelegates() {
        when(handler0.getGrantType()).thenReturn("authorization_code");
        when(handler1.getGrantType()).thenReturn("client_credentials");

        when(handler1.profileExchange(any())).thenReturn(exchanged);
        when(request.getGrantType()).thenReturn("client_credentials");

        actual = service.profileExchange(request);

        verify(handler1).profileExchange(request);
      }
    }

    @Nested
    class UnsupportedGrant {
      @Test
      void throwsInvalidGrantException() {
        when(handler0.getGrantType()).thenReturn("authorization_code");
        when(handler1.getGrantType()).thenReturn("client_credentials");
        when(request.getGrantType()).thenReturn("foo");

        assertThatThrownBy(() -> service.profileExchange(request))
            .isInstanceOf(UnsupportedGrantTypeException.class);
      }
    }
  }

  @Nested
  class TokenExchange {

    private TokenGrantDto actual;
    @Mock private TokenGrantRequest request;

    @Nested
    class HappyPath {

      @BeforeEach
      void setup() {
        when(handler0.getGrantType()).thenReturn("authorization_code");
        when(request.getGrantType()).thenReturn("authorization_code");
        when(handler0.tokenExchange(any())).thenReturn(exchanged);
        when(exchanged.getAccessToken()).thenReturn("j.w.t");
        when(exchanged.getExpiresIn()).thenReturn(1234L);
        when(exchanged.getMe()).thenReturn(ProfileUrl.of("https://me/"));
        when(exchanged.getScopes()).thenReturn(Collections.singleton(Scope.MEDIA));

        actual = service.tokenExchange(request);
      }

      @Test
      void itDelegatesToHandler() {
        verify(handler0).tokenExchange(request);
      }

      @Test
      void returnsToken() {
        assertThat(actual.getAccessToken()).isEqualTo("j.w.t");
      }

      @Test
      void returnsExpiresIn() {
        assertThat(actual.getExpiresIn()).isEqualTo(1234);
      }

      @Test
      void returnsTokenTypeAsBearer() {
        assertThat(actual.getTokenType()).isEqualTo("Bearer");
      }

      @Test
      void returnsSpaceSeparatedScopes() {
        assertThat(actual.getScope()).isEqualTo("media");
      }

      @Test
      void returnsMe() {
        assertThat(actual.getMe()).isEqualTo("https://me/");
      }

      @Test
      void doesNotReturnRefreshTokenIfNotIssued() {
        assertThat(actual.getRefreshToken()).isNull();
      }

      @Test
      void returnsRefreshTokenIfIssued() {
        when(exchanged.getRefreshToken()).thenReturn("j.w.t.2");

        actual = service.tokenExchange(request);

        assertThat(actual.getRefreshToken()).isEqualTo("j.w.t.2");
      }
    }

    @Nested
    class AnotherGrantType {
      @Test
      void itDelegates() {
        when(handler0.getGrantType()).thenReturn("authorization_code");
        when(handler1.getGrantType()).thenReturn("client_credentials");

        when(handler1.tokenExchange(any())).thenReturn(exchanged);
        when(request.getGrantType()).thenReturn("client_credentials");

        actual = service.tokenExchange(request);

        verify(handler1).tokenExchange(request);
      }
    }

    @Nested
    class UnsupportedGrant {
      @Test
      void throwsInvalidGrantException() {
        when(handler0.getGrantType()).thenReturn("authorization_code");
        when(handler1.getGrantType()).thenReturn("client_credentials");
        when(request.getGrantType()).thenReturn("foo");

        assertThatThrownBy(() -> service.tokenExchange(request))
            .isInstanceOf(UnsupportedGrantTypeException.class);
      }
    }
  }

  @Nested
  class VerifyToken {
    @Test
    void delegatesToVerifier() throws TokenVerificationException {
      when(verifier.verify(anyString())).thenThrow(TokenVerificationException.class);

      service.verifyToken("");

      verify(verifier).verify("");
    }

    @Test
    void returnsActiveFalseWhenNotVerified() throws TokenVerificationException {
      when(verifier.verify(anyString())).thenThrow(TokenVerificationException.class);

      TokenVerificationDto actual = service.verifyToken("");

      assertThat(actual.isActive()).isFalse();
    }

    @Nested
    class VerifySucceeds {
      @Mock private TokenAdapter token;

      private TokenVerificationDto actual;

      @BeforeEach
      void setup() throws TokenVerificationException {
        when(token.getSubject()).thenReturn(ProfileUrl.of("https://me/"));
        when(verifier.verify(any())).thenReturn(token);
        actual = service.verifyToken("");
      }

      @Test
      void returnsActiveTrue() {
        assertThat(actual.isActive()).isTrue();
      }

      /* note that other coverage is performed as part of TokenVerificationDto itself, this is just to give us confidence we're calling the constructor */
      @Test
      void returnsSubject() {
        assertThat(actual.getMe()).isEqualTo("https://me/");
      }
    }
  }

  @Nested
  class VerifyAndUnpack {

    @Test
    void delegatesToVerifier() throws TokenVerificationException {
      when(verifier.verify(anyString())).thenThrow(TokenVerificationException.class);

      service.verifyAndUnpack("");

      verify(verifier).verify("");
    }

    @Nested
    class VerifyFails {

      @ParameterizedTest
      @NullAndEmptySource
      void returnsEmpty(String token) throws TokenVerificationException {
        when(verifier.verify(any())).thenThrow(TokenVerificationException.class);

        var actual = service.verifyAndUnpack(token);

        assertThat(actual).isEmpty();
      }
    }

    @Nested
    class VerifySucceeds {
      @Mock private TokenAdapter token;

      private Optional<TokenAdapter> actual;

      @BeforeEach
      void setup() throws TokenVerificationException {
        when(verifier.verify(any())).thenReturn(token);
        actual = service.verifyAndUnpack("");
      }

      @Test
      void returnsToken() {
        assertThat(actual).contains(token);
      }
    }
  }
}
