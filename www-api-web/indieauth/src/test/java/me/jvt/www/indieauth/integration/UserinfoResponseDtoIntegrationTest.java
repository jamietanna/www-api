package me.jvt.www.indieauth.integration;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.Optional;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.token.model.UserinfoResponseDto;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;

@JsonTest
class UserinfoResponseDtoIntegrationTest {
  @Autowired private JacksonTester<UserinfoResponseDto> jacksonTester;

  private JsonContent<UserinfoResponseDto> asJson;

  @Nested
  class Serialise {
    @Test
    void setsName() throws IOException {
      asJson = jacksonTester.write(dto());

      assertThat(asJson).extractingJsonPathStringValue("name").isEqualTo("Jamie");
    }

    @Test
    void setsSubAsUrl() throws IOException {
      asJson = jacksonTester.write(dto());

      assertThat(asJson).extractingJsonPathStringValue("url").isEqualTo("https://me/");
    }

    @Test
    void setsPhoto() throws IOException {
      asJson = jacksonTester.write(dto());

      assertThat(asJson).extractingJsonPathStringValue("photo").isEqualTo("https://photo");
    }

    @Nested
    class Email {
      @Test
      void isSetWhenPresent() throws IOException {
        asJson = jacksonTester.write(dto("me@you"));

        assertThat(asJson).extractingJsonPathStringValue("email").isEqualTo("me@you");
      }

      @Test
      void isNotSetWhenAbsent() throws IOException {
        asJson = jacksonTester.write(dto());

        assertThat(asJson).doesNotHaveJsonPath("email");
      }
    }
  }

  private static UserinfoResponseDto dto() {
    return new UserinfoResponseDto(
        "Jamie", ProfileUrl.of("https://me"), "https://photo", Optional.empty());
  }

  private static UserinfoResponseDto dto(String email) {
    return new UserinfoResponseDto("", ProfileUrl.of("https://me"), "", Optional.of(email));
  }
}
