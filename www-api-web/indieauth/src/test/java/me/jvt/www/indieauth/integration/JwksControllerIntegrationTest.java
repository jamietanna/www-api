package me.jvt.www.indieauth.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.*;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import com.nimbusds.jwt.proc.DefaultJWTClaimsVerifier;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;
import java.text.ParseException;
import java.util.Collections;
import java.util.Set;
import me.jvt.www.indieauth.authorization.AuthorizationException;
import me.jvt.www.indieauth.authorization.AuthorizationRequest;
import me.jvt.www.indieauth.authorization.AuthorizationResponseDto;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.token.TokenService;
import me.jvt.www.indieauth.token.model.Scope;
import me.jvt.www.indieauth.token.model.TokenGrantDto;
import me.jvt.www.indieauth.token.model.TokenGrantRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("ephemeral")
@AutoConfigureMockMvc
class JwksControllerIntegrationTest {
  @Autowired private TokenService service;
  @Autowired private JWSAlgorithm jwsAlgorithm;

  @Autowired private MockMvc mvc;

  @Test
  void respondsToUrl() throws Exception {
    mvc.perform(get("/jwks")).andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void hasCorrectContentType() throws Exception {
    mvc.perform(get("/jwks"))
        .andExpect(
            MockMvcResultMatchers.header()
                .stringValues("content-type", "application/jwk-set+json"));
  }

  @Test
  void canBeNegotiatedWithWildcardPlusJson() throws Exception {
    mvc.perform(get("/jwks").accept("application/*+json"))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void issuedTokenCanBeValidatedAgainstJwks() throws Exception {
    String rawJwkSet =
        mvc.perform(get("/jwks").accept("application/*+json"))
            .andReturn()
            .getResponse()
            .getContentAsString();

    TokenGrantDto exchanged = tokenExchange();

    JWTClaimsSet claimsSet = validate(exchanged.getAccessToken(), rawJwkSet);

    assertThat(claimsSet).isNotNull();
  }

  private TokenGrantDto tokenExchange() throws AuthorizationException {
    TokenGrantRequest tokenGrantRequest = authorize(Collections.singleton(Scope.UNDELETE));
    return service.tokenExchange(tokenGrantRequest);
  }

  private TokenGrantRequest authorize(Set<Scope> scopes) throws AuthorizationException {
    AuthorizationRequest request = new AuthorizationRequest();
    request.setClientId("https://client");
    request.setMe(ProfileUrl.of("https://me"));
    request.setScopes(scopes);
    request.setResponseType("code");
    request.setRedirectUri("https://to-me");
    request.setIssueRefreshToken(true);
    AuthorizationResponseDto authorizationResponse = service.authorize(request);

    TokenGrantRequest tokenGrantRequest = new TokenGrantRequest();
    tokenGrantRequest.setClientId("https://client");
    tokenGrantRequest.setGrantType("authorization_code");
    tokenGrantRequest.setRedirectUri(authorizationResponse.getRedirectUri());
    tokenGrantRequest.setCode(authorizationResponse.getCode());
    return tokenGrantRequest;
  }

  private JWTClaimsSet validate(String accessToken, String rawJwkSet)
      throws BadJOSEException, ParseException, JOSEException {
    ConfigurableJWTProcessor<SecurityContext> jwtProcessor = new DefaultJWTProcessor<>();
    JWKSource<SecurityContext> keySource = new ImmutableJWKSet<>(JWKSet.parse(rawJwkSet));
    JWSKeySelector<SecurityContext> keySelector =
        new JWSVerificationKeySelector<>(jwsAlgorithm, keySource);
    jwtProcessor.setJWSKeySelector(keySelector);
    jwtProcessor.setJWTClaimsSetVerifier(
        new DefaultJWTClaimsVerifier<>(new JWTClaimsSet.Builder().build(), Collections.emptySet()));
    jwtProcessor.setJWSTypeVerifier(
        new DefaultJOSEObjectTypeVerifier<>(new JOSEObjectType("at+jwt")));
    return jwtProcessor.process(accessToken, null);
  }
}
