package me.jvt.www.indieauth.authorizationservermetadata;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;
import me.jvt.www.indieauth.token.model.Scope;
import org.junit.jupiter.api.Test;

class DefaultServerConfigurationSupplierTest {

  private final DefaultServerConfigurationSupplier supplier =
      new DefaultServerConfigurationSupplier(
          "iss",
          "https://authz",
          "https://token",
          "https://user",
          Set.of(Scope.CREATE),
          Set.of("id_token"),
          Set.of("mode"),
          Set.of("grant_1", "grant_2"),
          "https://token",
          Set.of("HS123"),
          "https://jwks");

  private final ServerConfigurationDto actual = supplier.get();

  @Test
  void setsIssuer() {
    assertThat(actual.getIssuer()).isEqualTo("iss");
  }

  @Test
  void setsAuthorizationEndpoint() {
    assertThat(actual.getAuthorizationEndpoint()).isEqualTo("https://authz");
  }

  @Test
  void setsTokenEndpoint() {
    assertThat(actual.getTokenEndpoint()).isEqualTo("https://token");
  }

  @Test
  void setsUserinfoEndpoint() {
    assertThat(actual.getUserinfoEndpoint()).isEqualTo("https://user");
  }

  @Test
  void setsScopesSupported() {
    assertThat(actual.getScopesSupported()).containsExactly("create");
  }

  @Test
  void setsResponseTypesSupported() {
    assertThat(actual.getResponseTypesSupported()).containsExactly("id_token");
  }

  @Test
  void setsResponseModesSupported() {
    assertThat(actual.getResponseModesSupported()).containsExactly("mode");
  }

  @Test
  void setsGrantTypesSupported() {
    assertThat(actual.getGrantTypesSupported()).containsExactlyInAnyOrder("grant_1", "grant_2");
  }

  @Test
  void setsIntrospectionEndpoint() {
    assertThat(actual.getIntrospectionEndpoint()).isEqualTo("https://token");
  }

  @Test
  void setsCodeChallengeMethodsSupported() {
    assertThat(actual.getCodeChallengeMethodsSupported()).containsExactly("HS123");
  }

  @Test
  void setsJwksUri() {
    assertThat(actual.getJwksUri()).isEqualTo("https://jwks");
  }
}
