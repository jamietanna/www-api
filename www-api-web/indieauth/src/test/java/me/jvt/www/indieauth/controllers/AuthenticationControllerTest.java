package me.jvt.www.indieauth.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import me.jvt.www.indieauth.authenticators.AuthenticationFailedException;
import me.jvt.www.indieauth.authenticators.AuthenticationNotSupportedException;
import me.jvt.www.indieauth.authenticators.AuthenticationService;
import me.jvt.www.indieauth.authenticators.Context;
import me.jvt.www.indieauth.authenticators.InternalAuthenticationException;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.template.AuthenticatorEntry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;

@ExtendWith(MockitoExtension.class)
class AuthenticationControllerTest {
  @Mock private Model model;
  @Mock private AuthenticationService service;
  private AuthenticationController controller;

  @BeforeEach
  void setup() {
    controller =
        new AuthenticationController(service, List.of("https://foo", "https://bar/profile"));
  }

  @Nested
  class ShowAccounts {
    private final Optional<ProfileUrl> profileUrl = Optional.of(ProfileUrl.of("https://foo"));
    private String actual;

    @BeforeEach
    void setup() {
      when(service.profileUrlFromCookie(any())).thenReturn(profileUrl);
      actual = controller.showAccounts(model, Optional.of("cookie"));
    }

    @Test
    void itDelegatesSessionCookieToService() {
      verify(service).profileUrlFromCookie(Optional.of("cookie"));
    }

    @Test
    void itAddsUrls() {
      verify(model)
          .addAttribute("profileUrls", Arrays.asList("https://foo", "https://bar/profile"));
    }

    @Test
    void itReturnsView() {
      assertThat(actual).isEqualTo("showAccounts");
    }

    @Test
    void itSetsProfileUrl() {
      verify(model).addAttribute("currentProfileUrl", profileUrl);
    }
  }

  @Nested
  class ShowAuthenticationForUser {
    private String actual;

    @Nested
    class HappyPath {

      @Mock private List<AuthenticatorEntry> entries;
      @Captor private ArgumentCaptor<List<AuthenticatorEntry>> entryCaptor;

      @BeforeEach
      void setup() throws AuthenticationNotSupportedException {

        when(service.authenticatorsForMe(any())).thenReturn(entries);

        actual = controller.showAuthenticationForUser("https://me/", model);
      }

      @Test
      void itReturnsView() {
        assertThat(actual).isEqualTo("showAuth");
      }

      @Test
      void itAddsAuthenticatorsToModel() {
        verify(model).addAttribute("authenticators", entries);
      }

      @Test
      void itAddsProfileUrlToModel() {
        verify(model).addAttribute("me", ProfileUrl.of("https://me/"));
      }
    }

    @Nested
    class WhenNoAuthenticators {
      @BeforeEach
      void setup() throws AuthenticationNotSupportedException {
        when(service.authenticatorsForMe(any()))
            .thenThrow(new AuthenticationNotSupportedException());

        actual = controller.showAuthenticationForUser("https://me/", model);
      }

      @Test
      void itShowsErrorPage() {
        assertThat(actual).isEqualTo("error");
      }

      @Test
      void itShowsErrorAsInvalidRequest() {
        verify(model).addAttribute("error", "invalid_request");
      }
    }
  }

  @Nested
  class AuthenticateAndSetCookie {
    @Mock private HttpServletResponse response;
    @Captor private ArgumentCaptor<Cookie> cookieCaptor;

    private String actual;
    private Cookie sessionCookie;
    private Cookie redirectUriCookie;

    @Nested
    class HappyPath {

      @BeforeEach
      void setup()
          throws InternalAuthenticationException, AuthenticationNotSupportedException,
              AuthenticationFailedException {
        when(service.authenticate(any(), any(), any())).thenReturn("session-cookie-value");

        actual =
            controller.authenticateAndSetCookie(
                "id", "https://me/", Optional.of("https://redirect"), response, null, null, model);
        verify(response, atLeast(2)).addCookie(cookieCaptor.capture());
        for (Cookie cookie : cookieCaptor.getAllValues()) {
          if (cookie.getName().equals("indieauth-authentication")) {
            sessionCookie = cookie;
          }
          if (cookie.getName().equals("redirect_uri")) {
            redirectUriCookie = cookie;
          }
        }
      }

      @Test
      void delegatesAuthenticatorIdToService()
          throws InternalAuthenticationException, AuthenticationNotSupportedException,
              AuthenticationFailedException {
        verify(service).authenticate(eq("id"), any(), any());
      }

      @Test
      void delegatesMeToService()
          throws InternalAuthenticationException, AuthenticationNotSupportedException,
              AuthenticationFailedException {
        verify(service).authenticate(any(), eq(ProfileUrl.of("https://me/")), any());
      }

      @Test
      void delegatesContextToService()
          throws AuthenticationFailedException, AuthenticationNotSupportedException,
              InternalAuthenticationException {
        Context expected = new Context();
        expected.setXForwardedFor("ip");
        expected.setUserAgent("ua");

        verify(service).authenticate(any(), any(), refEq(expected));
      }

      @Test
      void itRedirectsIfRedirectUriCookie() {
        assertThat(actual).isEqualTo("redirect:https://redirect");
      }

      @Test
      void itRedirectsToAuthenticateRootWhenAuthenticated() {
        actual =
            controller.authenticateAndSetCookie(
                "id", "https://me/", Optional.empty(), response, null, null, model);

        assertThat(actual).isEqualTo("redirect:/authenticate");
      }

      @Test
      void itSetsSessionCookie() {
        assertThat(sessionCookie).isNotNull();
      }

      @Test
      void itSetsSessionCookieAsHttpOnly() {
        assertThat(sessionCookie.isHttpOnly()).isTrue();
      }

      @Test
      void itSetsSessionCookieAsSecure() {
        assertThat(sessionCookie.getSecure()).isTrue();
      }

      @Test
      void itSetsSessionCookieWithCorrectName() {
        assertThat(sessionCookie.getName()).isEqualTo("indieauth-authentication");
      }

      @Test
      void itSetsSessionCookieValueFromDelegate() {
        assertThat(sessionCookie.getValue()).isEqualTo("session-cookie-value");
      }

      @Test
      void itSetsRedirectUriCookie() {
        assertThat(redirectUriCookie).isNotNull();
      }

      @Test
      void itSetsRedirectUriCookieAsHttpOnly() {
        assertThat(redirectUriCookie.isHttpOnly()).isTrue();
      }

      @Test
      void itSetsRedirectUriCookieAsSecure() {
        assertThat(redirectUriCookie.getSecure()).isTrue();
      }

      @Test
      void itSetsRedirectUriCookieWithCorrectName() {
        assertThat(redirectUriCookie.getName()).isEqualTo("redirect_uri");
      }

      @Test
      void itSetsRedirectUriCookieValueAsNull() {
        assertThat(redirectUriCookie.getValue()).isNull();
      }

      @Test
      void itSetsRedirectUriCookieMaxAgeAsZero() {
        assertThat(redirectUriCookie.getMaxAge()).isZero();
      }
    }

    @Nested
    class OnAuthenticationFailedException {

      @BeforeEach
      void setup()
          throws AuthenticationFailedException, AuthenticationNotSupportedException,
              InternalAuthenticationException {
        when(service.authenticate(any(), any(), any()))
            .thenThrow(new AuthenticationFailedException());

        actual =
            controller.authenticateAndSetCookie(
                "id", "https://me/", Optional.empty(), response, null, null, model);
      }

      @Test
      void itShowsErrorPage() {
        assertThat(actual).isEqualTo("error");
      }

      @Test
      void itShowsErrorAsAccessDenied() {
        verify(model).addAttribute("error", "access_denied");
      }
    }

    @Nested
    class OnAuthenticationNotSupportedException {
      @BeforeEach
      void setup()
          throws AuthenticationFailedException, AuthenticationNotSupportedException,
              InternalAuthenticationException {
        when(service.authenticate(any(), any(), any()))
            .thenThrow(new AuthenticationNotSupportedException());

        actual =
            controller.authenticateAndSetCookie(
                "id", "https://me/", Optional.empty(), response, null, null, model);
      }

      @Test
      void itShowsErrorPage() {
        assertThat(actual).isEqualTo("error");
      }

      @Test
      void itShowsErrorAsInvalidRequest() {
        verify(model).addAttribute("error", "invalid_request");
      }
    }

    @Nested
    class OnInternalAuthenticationException {
      @BeforeEach
      void setup()
          throws AuthenticationFailedException, AuthenticationNotSupportedException,
              InternalAuthenticationException {
        when(service.authenticate(any(), any(), any()))
            .thenThrow(new InternalAuthenticationException("foo"));

        actual =
            controller.authenticateAndSetCookie(
                "id", "https://me/", Optional.empty(), response, null, null, model);
      }

      @Test
      void itShowsErrorPage() {
        assertThat(actual).isEqualTo("error");
      }

      @Test
      void itShowsErrorAsInternalError() {
        verify(model).addAttribute("error", "internal_error");
      }
    }
  }
}
