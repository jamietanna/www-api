package me.jvt.www.indieauth.token.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.token.TokenAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TokenVerificationDtoTest {

  private static final ObjectMapper mapper = new ObjectMapper();

  private ObjectNode node;

  @Nested
  class HappyPath {
    @BeforeEach
    void setup() throws JsonProcessingException {
      TokenVerificationDto dto = new TokenVerificationDto();
      dto.setActive(true);
      dto.setMe(ProfileUrl.of("https://me/"));
      dto.setClientId("https://client");
      dto.setExpiration(123);
      dto.setIssueTime(456);
      dto.setScope("draft update");
      dto.setIssuer("https://other.iss");
      dto.setAudience(Arrays.asList("foo", "bar"));

      String actual = mapper.writeValueAsString(dto);
      node = mapper.readValue(actual, ObjectNode.class);
    }

    @Test
    void meIsMapped() {
      assertThat(node.get("me").textValue()).isEqualTo("https://me/");
    }

    @Test
    void subjectIsMapped() {
      assertThat(node.get("sub").textValue()).isEqualTo("https://me/");
    }

    @Test
    void clientIdIsMapped() {
      assertThat(node.get("client_id").textValue()).isEqualTo("https://client");
    }

    @Test
    void scopeIsMapped() {
      assertThat(node.get("scope").textValue()).isEqualTo("draft update");
    }

    @Test
    void expirationIsMapped() {
      assertThat(node.get("exp").intValue()).isEqualTo(123);
    }

    @Test
    void issueTimeIsMapped() {
      assertThat(node.get("iat").intValue()).isEqualTo(456);
    }

    @Test
    void activeIsMapped() {
      assertThat(node.get("active").booleanValue()).isTrue();
    }

    @Test
    void issuerIsMapped() {
      assertThat(node.get("iss").textValue()).isEqualTo("https://other.iss");
    }

    @Test
    void audienceIsMapped() {
      List<String> elements = new ArrayList<>();
      node.get("aud").forEach(n -> elements.add(n.textValue()));

      assertThat(elements).containsExactly("foo", "bar");
    }

    @Test
    void tokenTypeIsMapped() {
      TokenVerificationDto dto = new TokenVerificationDto();
      dto.setActive(true);
      assertThat(dto.getTokenType()).isEqualTo("Bearer");
    }
  }

  @Nested
  class TokenType {

    private TokenVerificationDto dto;

    @BeforeEach
    void setup() {
      dto = new TokenVerificationDto();
      dto.setActive(true);
    }

    @Test
    void isBearerWhenActiveTrue() {
      assertThat(dto.getTokenType()).isEqualTo("Bearer");
    }

    @Test
    void mapsCorrectly() throws JsonProcessingException {
      String actual = mapper.writeValueAsString(dto);
      node = mapper.readValue(actual, ObjectNode.class);

      assertThat(node.get("token_type").textValue()).isEqualTo("Bearer");
    }
  }

  @Nested
  class WhenMissingProperties {
    @BeforeEach
    void setup() throws JsonProcessingException {
      TokenVerificationDto dto = new TokenVerificationDto();

      String actual = mapper.writeValueAsString(dto);
      node = mapper.readValue(actual, ObjectNode.class);
    }

    @Test
    void meIsNotMapped() {
      assertThat(node.get("me")).isNull();
    }

    @Test
    void clientIdIsNotMapped() {
      assertThat(node.get("client_id")).isNull();
    }

    @Test
    void scopeIsNotMapped() {
      assertThat(node.get("scope")).isNull();
    }

    @Test
    void activeIsFalse() {
      assertThat(node.get("active").booleanValue()).isFalse();
    }
  }

  @Nested
  class Constructor {
    @Test
    void canBeDefault() {
      assertThat(new TokenVerificationDto()).isNotNull();
    }

    @Test
    void doesNotSetActiveTrue() {
      assertThat(new TokenVerificationDto().isActive()).isFalse();
    }

    @Nested
    class CanTakeTokenAdapter {
      @Mock private TokenAdapter token;

      private TokenVerificationDto dto;

      @BeforeEach
      void setup() {
        when(token.getSubject()).thenReturn(ProfileUrl.of("https://me/"));
        when(token.getClientId()).thenReturn("https://client");
        when(token.getScope()).thenReturn(Scope.fromScopeString("wibble foo"));
        when(token.getIssueTime()).thenReturn(15L);
        when(token.getExpirationTime()).thenReturn(1234L);
        when(token.getIssuer()).thenReturn("https://iss");
        when(token.getAudience()).thenReturn(Arrays.asList("https://one", "https://two"));

        dto = new TokenVerificationDto(token);
      }

      @Test
      void returnsMe() {
        assertThat(dto.getMe()).isEqualTo("https://me/");
      }

      @Test
      void returnsClientId() {
        assertThat(dto.getClientId()).isEqualTo("https://client");
      }

      @Test
      void returnsScope() {
        assertThat(dto.getScope()).isEqualTo("foo wibble");
      }

      @Test
      void returnsTokenIssuedAtDatestamp() {
        assertThat(dto.getIssueTime()).isEqualTo(15L);
      }

      @Test
      void returnsTokenExpirationDatestamp() {
        assertThat(dto.getExpiration()).isEqualTo(1234L);
      }

      @Test
      void returnsAudiences() {
        assertThat(dto.getAudience()).containsExactly("https://one", "https://two");
      }

      @Test
      void returnsIssuer() {
        assertThat(dto.getIssuer()).isEqualTo("https://iss");
      }

      @Test
      void setsActiveTrue() {
        assertThat(dto.isActive()).isTrue();
      }
    }
  }
}
