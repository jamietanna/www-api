package me.jvt.www.indieauth.authorizationservermetadata;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.Set;
import me.jvt.www.indieauth.token.model.Scope;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;

@JsonTest
class ServerConfigurationDtoTest {

  @Autowired private JacksonTester<ServerConfigurationDto> jacksonTester;

  private JsonContent<ServerConfigurationDto> asJson;

  @BeforeEach
  void setup() throws IOException {
    ServerConfigurationDto dto =
        new ServerConfigurationDto(
            "iss",
            "https://authz",
            "https://token",
            "https://user",
            Set.of(Scope.CREATE),
            Set.of("id_token"),
            Set.of("mode"),
            Set.of("grant_1", "grant_2"),
            "https://token",
            Set.of("HS123"),
            "https://jwks");
    asJson = jacksonTester.write(dto);
  }

  @Test
  void setsIssuer() {
    assertThat(asJson).extractingJsonPathStringValue("issuer").isEqualTo("iss");
  }

  @Test
  void setsAuthorizationEndpoint() {
    assertThat(asJson)
        .extractingJsonPathStringValue("authorization_endpoint")
        .isEqualTo("https://authz");
  }

  @Test
  void setsTokenEndpoint() {
    assertThat(asJson).extractingJsonPathStringValue("token_endpoint").isEqualTo("https://token");
  }

  @Test
  void setsUserInfoEndpoint() {
    assertThat(asJson).extractingJsonPathStringValue("userinfo_endpoint").isEqualTo("https://user");
  }

  @Test
  void setsScopesSupported() {
    assertThat(asJson).extractingJsonPathArrayValue("scopes_supported").containsExactly("create");
  }

  @Test
  void setsResponseTypesSupported() {
    assertThat(asJson)
        .extractingJsonPathArrayValue("response_types_supported")
        .containsExactly("id_token");
  }

  @Test
  void setsResponseModesSupported() {
    assertThat(asJson)
        .extractingJsonPathArrayValue("response_modes_supported")
        .containsExactly("mode");
  }

  @Test
  void setsGrantTypesSupported() {
    assertThat(asJson)
        .extractingJsonPathArrayValue("grant_types_supported")
        .containsExactlyInAnyOrder("grant_1", "grant_2");
  }

  @Test
  void setsIntrospectionEndpoint() {
    assertThat(asJson)
        .extractingJsonPathStringValue("introspection_endpoint")
        .isEqualTo("https://token");
  }

  @Test
  void setsCodeChallengeMethodsSupported() {
    assertThat(asJson)
        .extractingJsonPathArrayValue("code_challenge_methods_supported")
        .containsExactly("HS123");
  }

  @Test
  void setsJwksUri() {
    assertThat(asJson).extractingJsonPathStringValue("jwks_uri").isEqualTo("https://jwks");
  }
}
