package me.jvt.www.indieauth.integration;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.function.Supplier;
import me.jvt.www.indieauth.authorizationservermetadata.ServerConfigurationDto;
import me.jvt.www.indieauth.controllers.AuthorizationServerMetadataController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureMockMvc
@WebMvcTest(AuthorizationServerMetadataController.class)
@ExtendWith(SpringExtension.class)
class AuthorizationServerMetadataControllerIntegrationTest {
  @Autowired private MockMvc mvc;

  @MockBean private Supplier<ServerConfigurationDto> serverConfigurationSupplier;

  @Test
  void respondsToUrl() throws Exception {
    mvc.perform(get("/.well-known/oauth-authorization-server"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andDo(MockMvcResultHandlers.print());
  }
}
