package me.jvt.www.indieauth.token.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

class ScopeTest {

  @Nested
  class ToScopeString {
    @Test
    void returnsNullWhenNull() {
      assertThat(Scope.toScopeString(null)).isNull();
    }

    @Test
    void whenEmpty() {
      assertThat(Scope.toScopeString(Collections.emptySet())).isEqualTo("");
    }

    @Test
    void whenSingleton() {
      assertThat(Scope.toScopeString(Collections.singleton(Scope.CREATE))).isEqualTo("create");
    }

    @Test
    void whenMultiple() {
      assertThat(Scope.toScopeString(asSet(Scope.CREATE, Scope.MEDIA))).isEqualTo("create media");
    }

    @Test
    void sorts() {
      assertThat(Scope.toScopeString(asSet(Scope.MEDIA, Scope.CREATE))).isEqualTo("create media");
    }

    private Set<Scope> asSet(Scope... scopes) {
      return new HashSet<>(Arrays.asList(scopes));
    }
  }

  @Nested
  class FromScopeString {
    @ParameterizedTest
    @NullAndEmptySource
    void returnsEmptyWhenNullOrEmpty(String scopeString) {
      assertThat(Scope.fromScopeString(scopeString)).isEmpty();
    }

    @Test
    void whenSingleton() {
      assertThat(Scope.fromScopeString("create")).containsExactly(Scope.CREATE);
    }

    @Test
    void whenMultiple() {
      assertThat(Scope.fromScopeString("create media"))
          .containsExactlyInAnyOrder(Scope.CREATE, Scope.MEDIA);
    }

    @Test
    void addsUnknownScopes() {
      assertThat(Scope.fromScopeString("private")).containsExactly(new Scope("private"));
    }
  }

  @Nested
  class FromString {
    @ParameterizedTest
    @NullAndEmptySource
    void whenNullOrEmptyThrowsIllegalArgumentException(String scope) {
      assertThatThrownBy(() -> Scope.fromString(scope))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Scope was invalid");
    }

    @ParameterizedTest
    @ValueSource(strings = {"create", "delete", "undelete", "update", "media"})
    void parsesKnownScopes(String scope) {
      Scope actual = Scope.fromString(scope);

      assertThat(actual.getDescription()).isNotNull();
    }

    @Test
    void lowercases() {
      Scope actual = Scope.fromString("SCOPE");

      assertThat(actual.getName()).isEqualTo("scope");
    }

    @ParameterizedTest
    @ValueSource(strings = {"example", "not-real-scope"})
    void parsesOtherScopes(String scope) {
      Scope actual = Scope.fromString(scope);

      assertThat(actual.getName()).isEqualTo(scope);
    }
  }
}
