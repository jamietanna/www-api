package me.jvt.www.indieauth.integration;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("ephemeral")
class ApplicationIntegrationTest {
  @Test
  void contextLoads(ApplicationContext context) {
    assertThat(context).isNotNull();
  }
}
