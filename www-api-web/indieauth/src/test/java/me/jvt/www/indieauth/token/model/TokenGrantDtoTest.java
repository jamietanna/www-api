package me.jvt.www.indieauth.token.model;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import me.jvt.www.indieauth.domain.ProfileUrl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class TokenGrantDtoTest {

  private static final ObjectMapper mapper = new ObjectMapper();

  private ObjectNode node;

  @Nested
  class HappyPath {
    @BeforeEach
    void setup() throws JsonProcessingException {
      TokenGrantDto dto = new TokenGrantDto();
      dto.setAccessToken("j.w.t");
      dto.setRefreshToken("j.w.t.2");
      dto.setExpiresIn(1234L);
      dto.setMe(ProfileUrl.of("https://me/"));
      dto.setScope("draft update");
      dto.setTokenType("Bearer");

      String actual = mapper.writeValueAsString(dto);
      node = mapper.readValue(actual, ObjectNode.class);
    }

    @Test
    void accessTokenIsMapped() {
      assertThat(node.get("access_token").textValue()).isEqualTo("j.w.t");
    }

    @Test
    void refreshTokenIsMapped() {
      assertThat(node.get("refresh_token").textValue()).isEqualTo("j.w.t.2");
    }

    @Test
    void expiresInIsMapped() {
      assertThat(node.get("expires_in").numberValue().longValue()).isEqualTo(1234L);
    }

    @Test
    void meIsMapped() {
      assertThat(node.get("me").textValue()).isEqualTo("https://me/");
    }

    @Test
    void scopeIsMapped() {
      assertThat(node.get("scope").textValue()).isEqualTo("draft update");
    }

    @Test
    void tokenTypeIsMapped() {
      assertThat(node.get("token_type").textValue()).isEqualTo("Bearer");
    }
  }

  @Test
  void whenExpiresInIs0ItDoesNotSerialise() throws JsonProcessingException {
    TokenGrantDto dto = new TokenGrantDto();
    dto.setExpiresIn(0);

    String actual = mapper.writeValueAsString(dto);
    node = mapper.readValue(actual, ObjectNode.class);

    assertThat(node.get("expires_in")).isNull();
  }

  @Test
  void whenNoRefreshTokenItDoesNotSerialise() throws JsonProcessingException {
    TokenGrantDto dto = new TokenGrantDto();
    dto.setRefreshToken(null);

    String actual = mapper.writeValueAsString(dto);
    node = mapper.readValue(actual, ObjectNode.class);

    assertThat(node.get("refresh_token")).isNull();
  }

  @Nested
  class WhenMissingProperties {
    @BeforeEach
    void setup() throws JsonProcessingException {
      TokenGrantDto dto = new TokenGrantDto();

      String actual = mapper.writeValueAsString(dto);
      node = mapper.readValue(actual, ObjectNode.class);
    }

    @ParameterizedTest
    @ValueSource(strings = {"access_token", "expires_in", "me", "scope", "token_type"})
    void propertiesAreNotMapped(String property) {
      assertThat(node.get(property)).isNull();
    }
  }
}
