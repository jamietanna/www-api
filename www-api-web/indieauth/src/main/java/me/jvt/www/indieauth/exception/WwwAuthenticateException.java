package me.jvt.www.indieauth.exception;

import java.util.Optional;

public class WwwAuthenticateException extends Exception {
  private final Optional<String> error;
  private final String errorDescription;

  public WwwAuthenticateException(String errorDescription) {
    this(Optional.empty(), errorDescription);
  }

  public WwwAuthenticateException(String error, String errorDescription) {
    this(Optional.ofNullable(error), errorDescription);
  }

  public WwwAuthenticateException(Optional<String> error, String errorDescription) {
    super(errorDescription);
    this.error = error;
    this.errorDescription = errorDescription;
  }

  public Optional<String> getError() {
    return error;
  }

  public String getErrorDescription() {
    return errorDescription;
  }
}
