package me.jvt.www.indieauth.token.bertocci;

import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import java.text.ParseException;
import java.util.List;
import java.util.Set;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.token.TokenAdapter;
import me.jvt.www.indieauth.token.model.Scope;

public class BertocciTokenAdapter implements TokenAdapter {
  private final ProfileUrl subject;
  private final String clientId;
  private final long expirationTime;
  private final long issueTime;
  private final Set<Scope> scope;
  private final List<String> audience;
  private final TokenType tokenType;
  private final String issuer;
  private final long authTime;

  public BertocciTokenAdapter(SignedJWT jwt) {
    try {
      JWTClaimsSet claims = jwt.getJWTClaimsSet();
      subject = ProfileUrl.of(claims.getSubject());
      clientId = claims.getStringClaim("client_id");
      expirationTime = claims.getExpirationTime().getTime() / 1000;
      issueTime = claims.getIssueTime().getTime() / 1000;
      scope = Scope.fromScopeString(claims.getStringClaim("scope"));
      audience = claims.getAudience();
      tokenType = TokenType.fromString(claims.getStringClaim("token_type"));
      issuer = claims.getIssuer();
      authTime = claims.getLongClaim("auth_time");
    } catch (ParseException e) {
      throw new IllegalArgumentException("An invalid SignedJWT was provided");
    }
  }

  @Override
  public ProfileUrl getSubject() {
    return subject;
  }

  @Override
  public String getClientId() {
    return clientId;
  }

  @Override
  public long getExpirationTime() {
    return expirationTime;
  }

  @Override
  public long getIssueTime() {
    return issueTime;
  }

  @Override
  public Set<Scope> getScope() {
    return scope;
  }

  @Override
  public List<String> getAudience() {
    return audience;
  }

  @Override
  public TokenType getTokenType() {
    return tokenType;
  }

  @Override
  public String getIssuer() {
    return issuer;
  }

  @Override
  public long getAuthTime() {
    return authTime;
  }
}
