package me.jvt.www.indieauth.authenticators;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import java.text.ParseException;
import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import me.jvt.www.indieauth.domain.ProfileUrl;

public class JwsSessionCookieProvider implements SessionCookieProvider {

  private final JWSSigner signer;
  private final JWSVerifier verifier;
  private final Clock clock;

  public JwsSessionCookieProvider(byte[] secret) {
    this(secret, Clock.systemUTC());
  }

  JwsSessionCookieProvider(byte[] secret, Clock clock) {
    try {
      this.signer = new MACSigner(secret);
      this.verifier = new MACVerifier(secret);
    } catch (JOSEException e) {
      throw new IllegalStateException(e);
    }
    this.clock = clock;
  }

  @Override
  public SessionCookie create(ProfileUrl profileUrl) {
    JWTClaimsSet claims =
        new JWTClaimsSet.Builder()
            .expirationTime(expiration())
            .claim("profile_url", profileUrl.profileUrl().toString())
            .build();
    SignedJWT jwt = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claims);
    try {
      jwt.sign(signer);
    } catch (JOSEException e) {
      throw new IllegalStateException(e);
    }
    return new JwsSessionCookie(jwt);
  }

  @Override
  public SessionCookie validate(String cookie) throws InvalidSessionCookieException {
    if (null == cookie) {
      throw new InvalidSessionCookieException();
    }
    SignedJWT jwt;
    try {
      jwt = SignedJWT.parse(cookie);
      if (!jwt.verify(verifier)) {
        throw new InvalidSessionCookieException();
      }
    } catch (ParseException | JOSEException e) {
      throw new InvalidSessionCookieException();
    }
    try {
      if (jwt.getJWTClaimsSet().getExpirationTime().before(Date.from(Instant.now(clock)))) {
        throw new InvalidSessionCookieException();
      }
    } catch (ParseException e) {
      throw new InvalidSessionCookieException();
    }

    return new JwsSessionCookie(jwt);
  }

  private Date expiration() {
    return Date.from(Instant.now(clock).plus(5, ChronoUnit.MINUTES));
  }

  private static class JwsSessionCookie implements SessionCookie {

    private final SignedJWT jwt;

    public JwsSessionCookie(SignedJWT jwt) {
      this.jwt = jwt;
    }

    @Override
    public ProfileUrl getProfileUrl() {
      try {
        return ProfileUrl.of(jwt.getJWTClaimsSet().getStringClaim("profile_url"));
      } catch (ParseException e) {
        throw new IllegalStateException(e);
      }
    }

    @Override
    public Date getExpiry() {
      try {
        return jwt.getJWTClaimsSet().getExpirationTime();
      } catch (ParseException e) {
        throw new IllegalStateException(e);
      }
    }

    @Override
    public String serialize() {
      return jwt.serialize();
    }
  }
}
