package me.jvt.www.indieauth.authorization;

import java.util.Objects;

public class AuthorizationException extends Exception {
  public static final String UNSUPPORTED_RESPONSE_TYPE = "unsupported_response_type";
  public static final String INVALID_REQUEST = "invalid_request";

  private final String error;
  private final String errorDescription;

  public AuthorizationException(String error) {
    this(error, null);
  }

  public AuthorizationException(String error, String errorDescription) {
    super(
        "Authorization failed with error=%s, and description=%s"
            .formatted(error, errorDescription));
    this.error = error;
    this.errorDescription = errorDescription;
  }

  public String getError() {
    return error;
  }

  public String getErrorDescription() {
    return errorDescription;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AuthorizationException exception = (AuthorizationException) o;
    return Objects.equals(error, exception.error)
        && Objects.equals(errorDescription, exception.errorDescription);
  }

  @Override
  public int hashCode() {
    return Objects.hash(error, errorDescription);
  }
}
