package me.jvt.www.indieauth.authorization;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.function.Supplier;
import me.jvt.www.indieauth.authorizationservermetadata.ServerConfigurationDto;
import org.springframework.stereotype.Component;

@Component
public class RedirectUriProvider {

  private final Supplier<ServerConfigurationDto> serverConfigurationSupplier;

  public RedirectUriProvider(Supplier<ServerConfigurationDto> serverConfigurationSupplier) {
    this.serverConfigurationSupplier = serverConfigurationSupplier;
  }

  public String accessDenied(AuthorizationRequest request) {
    return errorRedirect(request, "access_denied");
  }

  public String code(AuthorizationRequest request, String code) {
    String redirect = request.getRedirectUri() + "?code=" + code;
    redirect += "&iss=" + serverConfigurationSupplier.get().getIssuer();

    if (null != request.getState()) {
      redirect += "&state=" + request.getState();
    }

    return redirect;
  }

  public String handleAuthorizationException(
      AuthorizationRequest request, AuthorizationException exception) {
    String redirect = errorRedirect(request, exception.getError());
    if (null != exception.getErrorDescription()) {
      try {
        redirect +=
            "&error_description="
                + URLEncoder.encode(
                    exception.getErrorDescription(), StandardCharsets.UTF_8.toString());
      } catch (UnsupportedEncodingException e) {
        throw new IllegalStateException(e);
      }
    }

    return redirect;
  }

  public String invalidRequest(AuthorizationRequest request) {
    return errorRedirect(request, "invalid_request");
  }

  public String serverError(AuthorizationRequest request) {
    return errorRedirect(request, "server_error");
  }

  private String errorRedirect(AuthorizationRequest request, String error) {
    String redirect = request.getRedirectUri() + "?error=" + error;
    redirect += "&iss=" + serverConfigurationSupplier.get().getIssuer();

    if (null != request.getState()) {
      redirect += "&state=" + request.getState();
    }

    return redirect;
  }
}
