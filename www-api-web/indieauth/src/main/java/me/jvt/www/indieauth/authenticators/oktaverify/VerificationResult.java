package me.jvt.www.indieauth.authenticators.oktaverify;

import com.okta.sdk.resource.user.factor.VerifyUserFactorResponse;

public enum VerificationResult {
  WAITING,
  SUCCESS,
  CLIENT_ERROR,
  INTERNAL_ERROR;

  public static VerificationResult fromFactorResultEnum(
      VerifyUserFactorResponse.FactorResultEnum factorResultEnum) {
    switch (factorResultEnum) {
      case WAITING:
        return WAITING;
      case SUCCESS:
        return SUCCESS;
      case EXPIRED:
      case CHALLENGE:
      case FAILED:
      case REJECTED:
      case TIMEOUT:
      case TIME_WINDOW_EXCEEDED:
      case PASSCODE_REPLAYED:
        return CLIENT_ERROR;
      case ERROR:
        return INTERNAL_ERROR;
    }

    throw new IllegalArgumentException("Result " + factorResultEnum + " is not supported");
  }
}
