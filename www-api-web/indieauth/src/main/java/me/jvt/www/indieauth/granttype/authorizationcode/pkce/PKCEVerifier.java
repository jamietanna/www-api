package me.jvt.www.indieauth.granttype.authorizationcode.pkce;

import java.util.Base64;
import java.util.Optional;
import java.util.Set;
import me.jvt.www.indieauth.authorization.AuthorizationRequest;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

@Component
public class PKCEVerifier {

  private static final Set<String> SUPPORTED_METHODS = Set.of("S256");

  public Set<String> getSupportedMethods() {
    return SUPPORTED_METHODS;
  }

  public boolean supported(String method) {
    return getSupportedMethods().contains(method);
  }

  public boolean verify(
      Optional<AuthorizationRequest.PKCE> maybePkce, Optional<String> maybeVerifier) {
    if (maybePkce.isPresent() && maybeVerifier.isPresent()) {
      String challenge = maybePkce.get().getCodeChallenge();
      String method = maybePkce.get().getCodeChallengeMethod();
      String verifier = maybeVerifier.get();
      if (!supported(method)) {
        return false;
      }

      String computedChallenge =
          Base64.getUrlEncoder().encodeToString(DigestUtils.sha256(verifier));

      return computedChallenge.replace("=", "").equals(challenge);
    }
    return ((!maybePkce.isPresent() || maybeVerifier.isPresent())
        && (maybePkce.isPresent() || !maybeVerifier.isPresent()));
  }
}
