package me.jvt.www.indieauth.domain;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

@JsonSerialize(using = ProfileUrl.ProfileUrlSerializer.class)
public record ProfileUrl(String profileUrl) {
  public static ProfileUrl of(String me) {
    URL parsed;
    try {
      parsed = new URL(me);
      if (parsed.getPath().equals("")) {
        parsed = new URL(me + "/");
      }
    } catch (MalformedURLException e) {
      throw new IllegalArgumentException(e);
    }
    return new ProfileUrl(parsed.toString());
  }

  public static class ProfileUrlSerializer extends StdSerializer<ProfileUrl> {

    protected ProfileUrlSerializer() {
      this(null);
    }

    protected ProfileUrlSerializer(Class<ProfileUrl> t) {
      super(t);
    }

    @Override
    public void serialize(ProfileUrl value, JsonGenerator gen, SerializerProvider provider)
        throws IOException {
      gen.writeString(value.profileUrl);
    }
  }
}
