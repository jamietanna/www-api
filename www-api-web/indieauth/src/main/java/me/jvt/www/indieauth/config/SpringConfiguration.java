package me.jvt.www.indieauth.config;

import me.jvt.www.logging.LoggingConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import(LoggingConfiguration.class)
@Configuration
public class SpringConfiguration {}
