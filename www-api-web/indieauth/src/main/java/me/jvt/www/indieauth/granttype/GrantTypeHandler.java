package me.jvt.www.indieauth.granttype;

import java.util.Optional;
import me.jvt.www.indieauth.authorization.AuthorizationException;
import me.jvt.www.indieauth.authorization.AuthorizationRequest;
import me.jvt.www.indieauth.authorization.AuthorizationResponseDto;
import me.jvt.www.indieauth.token.model.ProfileExchangeDto;
import me.jvt.www.indieauth.token.model.TokenExchangeDto;
import me.jvt.www.indieauth.token.model.TokenGrantRequest;

public interface GrantTypeHandler {
  void preauthorize(AuthorizationRequest request) throws AuthorizationException;

  AuthorizationResponseDto authorize(AuthorizationRequest request) throws AuthorizationException;

  String getGrantType();

  Optional<String> getResponseType();

  ProfileExchangeDto profileExchange(TokenGrantRequest request);

  TokenExchangeDto tokenExchange(TokenGrantRequest request);
}
