package me.jvt.www.indieauth.config;

import java.time.Clock;
import me.jvt.www.indieauth.authorization.InMemoryPushedAuthorizationRepository;
import me.jvt.www.indieauth.authorization.PushedAuthorizationRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersistenceConfiguration {
  @Bean
  public PushedAuthorizationRepository pushedAuthorizationRepository(Clock clock) {
    return new InMemoryPushedAuthorizationRepository(clock);
  }
}
