package me.jvt.www.indieauth.authenticators.oktaverify;

import org.springframework.stereotype.Component;

@Component
public class SleepFacade {

  void sleep(long l) throws InterruptedException {
    Thread.sleep(l);
  }
}
