package me.jvt.www.indieauth.authorizationservermetadata;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.util.Set;
import java.util.stream.Collectors;
import me.jvt.www.indieauth.token.model.Scope;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ServerConfigurationDto {
  private final String issuer;
  private final String authorizationEndpoint;
  private final String tokenEndpoint;
  private final String userinfoEndpoint;
  private final Set<String> scopesSupported;
  private final Set<String> responseTypesSupported;
  private final Set<String> responseModesSupported;
  private final Set<String> grantTypesSupported;
  private final String introspectionEndpoint;
  private final Set<String> codeChallengeMethodsSupported;
  private final String jwksUri;

  public ServerConfigurationDto(
      String issuer,
      String authorizationEndpoint,
      String tokenEndpoint,
      String userinfoEndpoint,
      Set<Scope> scopesSupported,
      Set<String> responseTypesSupported,
      Set<String> responseModesSupported,
      Set<String> grantTypesSupported,
      String introspectionEndpoint,
      Set<String> codeChallengeMethodsSupported,
      String jwksUri) {
    this.issuer = issuer;
    this.authorizationEndpoint = authorizationEndpoint;
    this.tokenEndpoint = tokenEndpoint;
    this.userinfoEndpoint = userinfoEndpoint;
    this.scopesSupported = scopesSupported.stream().map(Scope::getName).collect(Collectors.toSet());
    this.responseTypesSupported = responseTypesSupported;
    this.responseModesSupported = responseModesSupported;
    this.grantTypesSupported = grantTypesSupported;
    this.introspectionEndpoint = introspectionEndpoint;
    this.codeChallengeMethodsSupported = codeChallengeMethodsSupported;
    this.jwksUri = jwksUri;
  }

  public String getIssuer() {
    return issuer;
  }

  public String getAuthorizationEndpoint() {
    return authorizationEndpoint;
  }

  public String getTokenEndpoint() {
    return tokenEndpoint;
  }

  public String getUserinfoEndpoint() {
    return userinfoEndpoint;
  }

  public Set<String> getScopesSupported() {
    return scopesSupported;
  }

  public Set<String> getResponseTypesSupported() {
    return responseTypesSupported;
  }

  public Set<String> getResponseModesSupported() {
    return responseModesSupported;
  }

  public Set<String> getGrantTypesSupported() {
    return grantTypesSupported;
  }

  public String getIntrospectionEndpoint() {
    return introspectionEndpoint;
  }

  public Set<String> getCodeChallengeMethodsSupported() {
    return codeChallengeMethodsSupported;
  }

  public String getJwksUri() {
    return jwksUri;
  }
}
