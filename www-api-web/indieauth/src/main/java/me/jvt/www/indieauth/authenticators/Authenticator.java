package me.jvt.www.indieauth.authenticators;

import me.jvt.www.indieauth.domain.ProfileUrl;

public interface Authenticator {

  /**
   * Get a unique identifier for the authenticator.
   *
   * @return unique identifier for the authenticator
   */
  String id();

  /**
   * Get the friendly name for the authenticator.
   *
   * @return the friendly name for the authenticator
   */
  String name();

  /**
   * Authenticate the user using some custom logic.
   *
   * @return whether the user has successfully authenticated
   * @throws AuthenticationNotSupportedException when the authentication was not possible for some
   *     reason
   * @throws InternalAuthenticationException when the authentication was not possible for some
   *     reason
   * @param me the me URL that should be authenticated
   * @param context the context for the request
   */
  boolean authenticate(ProfileUrl me, Context context)
      throws AuthenticationNotSupportedException, InternalAuthenticationException;
}
