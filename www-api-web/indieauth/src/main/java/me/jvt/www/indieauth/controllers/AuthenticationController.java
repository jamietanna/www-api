package me.jvt.www.indieauth.controllers;

import java.net.URLDecoder;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import me.jvt.www.indieauth.authenticators.AuthenticationFailedException;
import me.jvt.www.indieauth.authenticators.AuthenticationNotSupportedException;
import me.jvt.www.indieauth.authenticators.AuthenticationService;
import me.jvt.www.indieauth.authenticators.Context;
import me.jvt.www.indieauth.authenticators.InternalAuthenticationException;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.template.AuthenticatorEntry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/authenticate")
public class AuthenticationController {

  private final AuthenticationService service;
  private List<String> profileUrls;

  public AuthenticationController(
      AuthenticationService service,
      @Value("#{'${authenticator.okta.me}'.split(',')}") List<String> profileUrls) {
    this.service = service;
    this.profileUrls = profileUrls;
  }

  @GetMapping
  public String showAccounts(
      Model model, @CookieValue("indieauth-authentication") Optional<String> cookie) {
    model.addAttribute("currentProfileUrl", service.profileUrlFromCookie(cookie));
    model.addAttribute("profileUrls", profileUrls);
    return "showAccounts";
  }

  @GetMapping("/start")
  public String showAuthenticationForUser(@RequestParam("me") String encodedMe, Model model) {
    List<AuthenticatorEntry> authenticatorMappings;
    ProfileUrl me = ProfileUrl.of(URLDecoder.decode(encodedMe));
    try {
      authenticatorMappings = service.authenticatorsForMe(me);
    } catch (AuthenticationNotSupportedException e) {
      return error(model, "invalid_request");
    }
    model.addAttribute("me", me);
    model.addAttribute("authenticators", authenticatorMappings);
    return "showAuth";
  }

  @PostMapping
  public String authenticateAndSetCookie(
      @RequestParam("authenticator_id") String authenticatorId,
      @RequestParam("me") String me,
      @CookieValue("redirect_uri") Optional<String> redirectUri,
      HttpServletResponse response,
      @RequestHeader("User-Agent") String userAgent,
      @RequestHeader(value = "X-Forwarded-For", required = false) String xForwardedFor,
      Model model) {
    Context context = new Context();
    context.setUserAgent(userAgent);
    context.setXForwardedFor(xForwardedFor);
    String cookieValue;
    try {
      cookieValue = service.authenticate(authenticatorId, ProfileUrl.of(me), context);
    } catch (AuthenticationNotSupportedException e) {
      return error(model, "invalid_request");
    } catch (AuthenticationFailedException e) {
      return error(model, "access_denied");
    } catch (InternalAuthenticationException e) {
      return error(model, "internal_error");
    }

    Cookie sessionCookie = new Cookie("indieauth-authentication", cookieValue);
    sessionCookie.setHttpOnly(true);
    sessionCookie.setSecure(true);
    response.addCookie(sessionCookie);

    Cookie redirectUriCookie = new Cookie("redirect_uri", null);
    redirectUriCookie.setHttpOnly(true);
    redirectUriCookie.setSecure(true);
    redirectUriCookie.setMaxAge(0);
    response.addCookie(redirectUriCookie);

    String redirect = redirectUri.orElse("/authenticate");
    return "redirect:" + redirect;
  }

  private String error(Model model, String errorMessage) {
    model.addAttribute("error", errorMessage);
    return "error";
  }
}
