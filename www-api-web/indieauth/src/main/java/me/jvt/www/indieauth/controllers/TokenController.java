package me.jvt.www.indieauth.controllers;

import java.util.Base64;
import java.util.Optional;
import me.jvt.www.indieauth.token.TokenService;
import me.jvt.www.indieauth.token.model.TokenGrantDto;
import me.jvt.www.indieauth.token.model.TokenGrantRequest;
import me.jvt.www.indieauth.token.model.TokenVerificationDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TokenController {
  private static final Logger LOGGER = LoggerFactory.getLogger(TokenController.class);

  private final TokenService service;

  public TokenController(TokenService service) {
    this.service = service;
  }

  @PostMapping("/token")
  public ResponseEntity<TokenGrantDto> exchange(
      @RequestBody MultiValueMap<String, String> params,
      @RequestHeader("Authorization") Optional<String> authorizationHeader) {
    TokenGrantRequest grantRequest = new TokenGrantRequest();
    params.forEach(grantRequest::put);
    grantRequest.setClientId(getClientId(grantRequest.getClientId(), authorizationHeader));
    return new ResponseEntity<>(service.tokenExchange(grantRequest), HttpStatus.OK);
  }

  @GetMapping("/token")
  public ResponseEntity<TokenVerificationDto> verifyGet(
      @RequestHeader("Authorization") String authorizationHeader) {
    String accessToken = authorizationHeader.replace("Bearer ", "");
    TokenVerificationDto response = service.verifyToken(accessToken);
    if (response.isActive()) {
      try (MDC.MDCCloseable ignored =
          MDC.putCloseable("introspect.clientId", response.getClientId())) {
        LOGGER.info(
            "Received a request to the IndieAuth token verify endpoint for client {}",
            response.getClientId());
      }
      return new ResponseEntity<>(response, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
  }

  @PostMapping("/token_info")
  public ResponseEntity<TokenVerificationDto> verifyPost(
      @RequestParam("token") String accessToken) {
    TokenVerificationDto response = service.verifyToken(accessToken);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  private String getClientId(String clientIdParameter, Optional<String> authorizationHeader) {
    if (authorizationHeader.isPresent()) {
      String decoded =
          new String(Base64.getDecoder().decode(authorizationHeader.get().replace("Basic ", "")));
      String[] parts = decoded.split(":");
      return parts[0];
    } else {
      return clientIdParameter;
    }
  }
}
