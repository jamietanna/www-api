package me.jvt.www.indieauth.token;

public class TokenVerificationException extends Exception {

  public TokenVerificationException(String message) {
    super(message);
  }
}
