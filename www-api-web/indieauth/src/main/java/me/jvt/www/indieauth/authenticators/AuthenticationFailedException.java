package me.jvt.www.indieauth.authenticators;

/** An exception to note that the authentication failed. */
public class AuthenticationFailedException extends Exception {

  public AuthenticationFailedException() {
    super("The user could not successfully authenticate");
  }
}
