package me.jvt.www.indieauth.token;

import java.util.List;
import java.util.Optional;
import me.jvt.www.indieauth.authorization.AuthorizationException;
import me.jvt.www.indieauth.authorization.AuthorizationRequest;
import me.jvt.www.indieauth.authorization.AuthorizationResponseDto;
import me.jvt.www.indieauth.exception.UnsupportedGrantTypeException;
import me.jvt.www.indieauth.granttype.GrantTypeHandler;
import me.jvt.www.indieauth.token.model.ProfileExchangeDto;
import me.jvt.www.indieauth.token.model.Scope;
import me.jvt.www.indieauth.token.model.TokenExchangeDto;
import me.jvt.www.indieauth.token.model.TokenGrantDto;
import me.jvt.www.indieauth.token.model.TokenGrantRequest;
import me.jvt.www.indieauth.token.model.TokenVerificationDto;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class TokenService {

  private final TokenVerifier verifier;
  private final List<GrantTypeHandler> grantTypeHandlers;

  public TokenService(
      @Qualifier("accessTokenVerifier") TokenVerifier verifier,
      List<GrantTypeHandler> grantTypeHandlers) {
    if (grantTypeHandlers.isEmpty()) {
      throw new IllegalArgumentException("TokenService requires at least one GrantTypeHandler");
    }
    this.verifier = verifier;
    this.grantTypeHandlers = grantTypeHandlers;
  }

  public void preauthorize(AuthorizationRequest request) throws AuthorizationException {
    GrantTypeHandler handler =
        grantTypeHandlers.stream()
            .filter(h -> h.getResponseType().isPresent())
            .filter(h -> h.getResponseType().get().equals(request.getResponseType()))
            .findFirst()
            .orElseThrow(
                () -> new AuthorizationException(AuthorizationException.UNSUPPORTED_RESPONSE_TYPE));

    handler.preauthorize(request);
  }

  public AuthorizationResponseDto authorize(AuthorizationRequest request)
      throws AuthorizationException {
    GrantTypeHandler handler =
        grantTypeHandlers.stream()
            .filter(h -> h.getResponseType().isPresent())
            .filter(h -> h.getResponseType().get().equals(request.getResponseType()))
            .findFirst()
            .orElseThrow(
                () -> new AuthorizationException(AuthorizationException.UNSUPPORTED_RESPONSE_TYPE));

    return handler.authorize(request);
  }

  public ProfileExchangeDto profileExchange(TokenGrantRequest request) {
    GrantTypeHandler handler =
        grantTypeHandlers.stream()
            .filter(h -> h.getGrantType().equals(request.getGrantType()))
            .findFirst()
            .orElseThrow(UnsupportedGrantTypeException::new);

    return handler.profileExchange(request);
  }

  public TokenGrantDto tokenExchange(TokenGrantRequest grantRequest) {
    GrantTypeHandler handler =
        grantTypeHandlers.stream()
            .filter(h -> h.getGrantType().equals(grantRequest.getGrantType()))
            .findFirst()
            .orElseThrow(UnsupportedGrantTypeException::new);

    TokenExchangeDto exchanged = handler.tokenExchange(grantRequest);

    TokenGrantDto dto = new TokenGrantDto();
    dto.setAccessToken(exchanged.getAccessToken());
    dto.setRefreshToken(exchanged.getRefreshToken());
    dto.setExpiresIn(exchanged.getExpiresIn());
    dto.setMe(exchanged.getMe());
    dto.setTokenType("Bearer");
    dto.setScope(Scope.toScopeString(exchanged.getScopes()));

    return dto;
  }

  public TokenVerificationDto verifyToken(String token) {
    TokenVerificationDto dto;
    try {
      TokenAdapter verified = verifier.verify(token);
      dto = new TokenVerificationDto(verified);
      dto.setActive(true);
    } catch (TokenVerificationException e) {
      dto = new TokenVerificationDto();
      dto.setActive(false);
      return dto;
    }

    return dto;
  }

  public Optional<TokenAdapter> verifyAndUnpack(String token) {
    try {
      TokenAdapter verified = verifier.verify(token);
      return Optional.of(verified);
    } catch (TokenVerificationException e) {
      return Optional.empty();
    }
  }
}
