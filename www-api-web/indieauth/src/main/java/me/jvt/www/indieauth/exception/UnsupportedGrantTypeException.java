package me.jvt.www.indieauth.exception;

public class UnsupportedGrantTypeException extends OAuthException {

  public UnsupportedGrantTypeException() {
    super();
  }
}
