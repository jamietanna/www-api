package me.jvt.www.indieauth.token;

public interface TokenVerifier {

  TokenAdapter verify(String token) throws TokenVerificationException;
}
