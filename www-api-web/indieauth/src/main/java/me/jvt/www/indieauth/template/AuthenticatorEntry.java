package me.jvt.www.indieauth.template;

public class AuthenticatorEntry {
  private final String id;
  private final String name;

  public AuthenticatorEntry(String id, String name) {
    this.id = id;
    this.name = name;
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }
}
