package me.jvt.www.indieauth.authenticators.oktaverify;

import java.util.List;
import me.jvt.www.indieauth.authenticators.AuthenticationNotSupportedException;
import me.jvt.www.indieauth.authenticators.Authenticator;
import me.jvt.www.indieauth.authenticators.Context;
import me.jvt.www.indieauth.authenticators.InternalAuthenticationException;
import me.jvt.www.indieauth.domain.ProfileUrl;

public class OktaVerifyAuthenticator implements Authenticator {

  private static final int MAXIMUM_RETRIES = 10;
  private static final long RETRY_DELAY = 3000;

  private final List<ProfileUrl> validMeUrls;
  private final String userId;
  private final String factorId;
  private final OktaFacade facade;
  private final SleepFacade sleepFacade;

  public OktaVerifyAuthenticator(
      List<ProfileUrl> validMeUrls,
      String userId,
      String factorId,
      OktaFacade facade,
      SleepFacade sleepFacade) {
    this.validMeUrls = validMeUrls;
    this.userId = userId;
    this.factorId = factorId;
    this.facade = facade;
    this.sleepFacade = sleepFacade;
  }

  @Override
  public String id() {
    return "push";
  }

  @Override
  public String name() {
    return "Push Notification";
  }

  @Override
  public boolean authenticate(ProfileUrl me, Context context)
      throws InternalAuthenticationException, AuthenticationNotSupportedException {
    if (!validMeUrls.contains(me)) {
      throw new AuthenticationNotSupportedException();
    }

    String transactionId =
        facade.triggerVerify(userId, factorId, context.getXForwardedFor(), context.getUserAgent());
    for (int i = 0; i < MAXIMUM_RETRIES; i++) {
      VerificationResult result = facade.isTransactionSuccess(userId, factorId, transactionId);
      switch (result) {
        case SUCCESS:
          return true;
        case CLIENT_ERROR:
          return false;
        case INTERNAL_ERROR:
          throw new InternalAuthenticationException(
              "Something went wrong while trying to authenticate. Check logs for more details");
        case WAITING:
          try {
            sleepFacade.sleep(RETRY_DELAY);
          } catch (InterruptedException e) {
            throw new InternalAuthenticationException(
                "Something went wrong while trying to authenticate. Check logs for more details");
          }
      }
    }

    return false;
  }
}
