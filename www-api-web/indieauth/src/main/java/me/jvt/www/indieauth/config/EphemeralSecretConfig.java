package me.jvt.www.indieauth.config;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("ephemeral")
public class EphemeralSecretConfig {

  private static final Logger LOGGER = LoggerFactory.getLogger(EphemeralSecretConfig.class);

  @Bean
  public JWK jwk(JWSAlgorithm algorithm) throws JOSEException {
    return key(algorithm);
  }

  private RSAKey key(JWSAlgorithm algorithm) throws JOSEException {
    LOGGER.info("Generating key for {}", algorithm);
    return new RSAKeyGenerator(2048).algorithm(algorithm).generate();
  }
}
