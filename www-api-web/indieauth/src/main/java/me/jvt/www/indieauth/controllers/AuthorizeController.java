package me.jvt.www.indieauth.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import me.jvt.www.indieauth.authenticators.SessionCookieProvider;
import me.jvt.www.indieauth.authenticators.SessionCookieProvider.InvalidSessionCookieException;
import me.jvt.www.indieauth.authenticators.SessionCookieProvider.SessionCookie;
import me.jvt.www.indieauth.authorization.*;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.template.ScopeEntry;
import me.jvt.www.indieauth.token.TokenService;
import me.jvt.www.indieauth.token.model.ProfileExchangeDto;
import me.jvt.www.indieauth.token.model.Scope;
import me.jvt.www.indieauth.token.model.Scope.Group;
import me.jvt.www.indieauth.token.model.TokenGrantRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/authorize")
public class AuthorizeController {

  private final TokenService service;
  private final PushedAuthorizationRepository requestRepository;
  private final RedirectUriProvider redirectUriProvider;
  private final int accessTokenDays;
  private final int refreshTokenDays;
  private final SessionCookieProvider sessionCookieProvider;

  public AuthorizeController(
      TokenService service,
      PushedAuthorizationRepository requestRepository,
      RedirectUriProvider redirectUriProvider,
      @Value("${token.expiry.access-token:7}") int accessTokenDays,
      @Value("${token.expiry.refresh-token:90}") int refreshTokenDays,
      SessionCookieProvider sessionCookieProvider) {
    this.redirectUriProvider = redirectUriProvider;
    this.service = service;
    this.requestRepository = requestRepository;
    this.accessTokenDays = accessTokenDays;
    this.refreshTokenDays = refreshTokenDays;
    this.sessionCookieProvider = sessionCookieProvider;
  }

  @GetMapping
  public String authorize(
      @RequestParam("response_type") String responseType,
      @RequestParam("client_id") String clientId,
      @RequestParam("redirect_uri") String redirectUri,
      @RequestParam("state") Optional<String> state,
      @RequestParam("code_challenge") Optional<String> challenge,
      @RequestParam("code_challenge_method") Optional<String> challengeMethod,
      @RequestParam("scope") Optional<String> scopes,
      @RequestParam("me") Optional<String> me) {
    AuthorizationRequest request = new AuthorizationRequest();
    request.setResponseType(responseType);
    request.setClientId(clientId);
    request.setRedirectUri(redirectUri);
    state.ifPresent(request::setState);
    AuthorizationRequest.PKCE pkce = new AuthorizationRequest.PKCE();
    challenge.ifPresent(pkce::setCodeChallenge);
    challengeMethod.ifPresent(pkce::setCodeChallengeMethod);
    if (challenge.isPresent() || challengeMethod.isPresent()) {
      request.setPKCE(pkce);
    }
    if (scopes.isPresent()) {
      request.setScopes(scopes(scopes.get()));
    } else {
      request.setScopes(Collections.emptySet());
    }
    me.ifPresent(m -> request.setMe(ProfileUrl.of(m)));

    String requestUri = requestRepository.put(request);

    try {
      service.preauthorize(request);
    } catch (AuthorizationException e) {
      return redirect(redirectUriProvider.handleAuthorizationException(request, e));
    }

    return redirect("/authorize/consent?client_id=" + clientId + "&request_uri=" + requestUri);
  }

  @PostMapping
  public ResponseEntity<ProfileExchangeDto> profileExchange(
      @RequestBody MultiValueMap<String, String> params) {
    TokenGrantRequest grantRequest = new TokenGrantRequest();
    params.forEach(grantRequest::put);
    return new ResponseEntity<>(service.profileExchange(grantRequest), HttpStatus.OK);
  }

  @GetMapping("/consent")
  public String consent(
      Model model,
      @RequestParam("request_uri") String requestUri,
      @CookieValue(value = "indieauth-authentication", required = false) String sessionCookie,
      HttpServletRequest servletRequest,
      HttpServletResponse servletResponse) {
    SessionCookie cookie;
    try {
      cookie = sessionCookieProvider.validate(sessionCookie);
    } catch (InvalidSessionCookieException e) {
      return redirectToAuthWithRedirectCookie(servletRequest, servletResponse, requestUri);
    }

    AuthorizationRequest request;
    try {
      request = requestRepository.get(requestUri);
    } catch (AuthorizationException e) {
      model.addAttribute("error", e.getError());
      return "error";
    }

    Set<Scope> scopes = new HashSet<>(request.getScopes());
    EnumMap<Group, List<ScopeEntry>> scopeEntries = new EnumMap<>(Group.class);
    List<ScopeEntry> publishing = new ArrayList<>();
    for (Scope s : Scope.getKnownPublishingScopes()) {
      publishing.add(new ScopeEntry(s, scopes.contains(s)));
      scopes.remove(s);
    }
    scopeEntries.put(Group.PUBLISHING, publishing);

    List<ScopeEntry> reading = new ArrayList<>();
    for (Scope s : Scope.getKnownReadingScopes()) {
      reading.add(new ScopeEntry(s, scopes.contains(s)));
      scopes.remove(s);
    }
    scopeEntries.put(Group.READING, reading);

    List<ScopeEntry> notification = new ArrayList<>();
    for (Scope s : Scope.getKnownNotificationScopes()) {
      notification.add(new ScopeEntry(s, scopes.contains(s)));
      scopes.remove(s);
    }
    scopeEntries.put(Group.NOTIFICATION, notification);

    List<ScopeEntry> profile = new ArrayList<>();
    for (Scope s : Scope.getKnownProfileScopes()) {
      profile.add(new ScopeEntry(s, scopes.contains(s)));
      scopes.remove(s);
    }
    scopeEntries.put(Group.PROFILE, profile);

    List<ScopeEntry> unknown = new ArrayList<>();
    for (Scope s : scopes) {
      unknown.add(new ScopeEntry(s, true));
    }

    scopeEntries.put(Group.UNKNOWN, unknown);

    model.addAttribute("me", cookie.getProfileUrl());
    model.addAttribute("scopes", scopeEntries);
    model.addAttribute("request", request);
    model.addAttribute("request_uri", requestUri);
    model.addAttribute("accessTokenDays", accessTokenDays);
    model.addAttribute("refreshTokenDays", refreshTokenDays);

    return "consent";
  }

  @PostMapping("/consent")
  public String submitConsent(
      Model model,
      @RequestParam("request_uri") String requestUri,
      @RequestParam(value = "scope", required = false) Set<Scope> scopes,
      @RequestParam(value = "issue_refresh_token", required = false) boolean issueRefreshToken,
      @CookieValue(value = "indieauth-authentication", required = false) String sessionCookie,
      HttpServletRequest servletRequest,
      HttpServletResponse servletResponse) {
    SessionCookie cookie;
    try {
      cookie = sessionCookieProvider.validate(sessionCookie);
    } catch (InvalidSessionCookieException e) {
      return redirectToAuthWithRedirectCookie(servletRequest, servletResponse, requestUri);
    }

    AuthorizationRequest request;
    try {
      request = requestRepository.pop(requestUri);
    } catch (AuthorizationException e) {
      model.addAttribute("error", e.getError());
      return "error";
    }

    if (null == scopes) {
      scopes = Collections.emptySet();
    }

    request.setIssueRefreshToken(issueRefreshToken);
    request.setScopes(scopes);

    request.setMe(cookie.getProfileUrl());

    AuthorizationResponseDto response;
    try {
      response = service.authorize(request);
    } catch (AuthorizationException e) {
      throw new RuntimeException(e);
    }

    return redirect(redirectUriProvider.code(request, response.getCode()));
  }

  @PostMapping("/deny")
  public String authorizeDeny(Model model, @RequestParam("request_uri") String requestUri) {
    AuthorizationRequest request;
    try {
      request = requestRepository.pop(requestUri);
    } catch (AuthorizationException e) {
      model.addAttribute("error", e.getError());
      return "error";
    }

    return redirect(redirectUriProvider.accessDenied(request));
  }

  private Set<Scope> scopes(String scopeString) {
    return Arrays.stream(scopeString.split(" ")).map(Scope::fromString).collect(Collectors.toSet());
  }

  private String redirect(String redirect) {
    return "redirect:" + redirect;
  }

  private String redirectToAuthWithRedirectCookie(
      HttpServletRequest servletRequest, HttpServletResponse servletResponse, String requestUri) {
    String redirectUri = servletRequest.getRequestURI();
    if (null != servletRequest.getQueryString()) {
      redirectUri += "?" + servletRequest.getQueryString();
    }
    Cookie redirect = new Cookie("redirect_uri", redirectUri);
    redirect.setHttpOnly(true);
    redirect.setSecure(true);
    redirect.setPath("/");
    servletResponse.addCookie(redirect);

    String authenticatePath = "/authenticate";
    try {
      AuthorizationRequest request = requestRepository.get(requestUri);
      if (null != request.getMe()) {
        authenticatePath = "/authenticate/start?me=" + request.getMe().profileUrl().toString();
      }
    } catch (AuthorizationException e) {
      // ignore
    }
    return redirect(authenticatePath);
  }
}
