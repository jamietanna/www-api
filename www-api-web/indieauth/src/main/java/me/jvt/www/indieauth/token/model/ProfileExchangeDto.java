package me.jvt.www.indieauth.token.model;

import me.jvt.www.indieauth.domain.ProfileUrl;

public class ProfileExchangeDto {

  public ProfileExchangeDto() {}

  public ProfileExchangeDto(ProfileUrl me) {
    setMe(me);
  }

  private ProfileUrl me;

  public String getMe() {
    return me.profileUrl().toString();
  }

  public void setMe(ProfileUrl me) {
    this.me = me;
  }
}
