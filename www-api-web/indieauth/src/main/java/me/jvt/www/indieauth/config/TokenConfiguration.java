package me.jvt.www.indieauth.config;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import java.time.Clock;
import java.util.Arrays;
import me.jvt.www.indieauth.token.TokenCreator;
import me.jvt.www.indieauth.token.TokenVerifier;
import me.jvt.www.indieauth.token.bertocci.BertocciTokenCreator;
import me.jvt.www.indieauth.token.bertocci.BertocciTokenVerifier;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TokenConfiguration {
  @Bean
  public JWSAlgorithm jwsAlgorithm() {
    return JWSAlgorithm.RS256;
  }

  @Bean
  public JWKSet jwkSet(JWK... jwks) {
    return new JWKSet(Arrays.asList(jwks));
  }

  @Bean
  public JWKSource<SecurityContext> jwkSource(JWKSet jwkSet) {
    return new ImmutableJWKSet<>(jwkSet);
  }

  @Bean
  public Clock clock() {
    return Clock.systemUTC();
  }

  @Bean
  public TokenCreator tokenCreator(
      JWKSet jwks,
      @Value("${authorization-server.issuer}") String issuer,
      Clock clock,
      @Value("${authorization-server.default-resource}") String defaultResource,
      JWSAlgorithm jwsAlgorithm,
      @Qualifier("refreshTokenVerifier") TokenVerifier refreshTokenVerifier,
      @Value("${token.expiry.access-token:7}") int accessTokenDays,
      @Value("${token.expiry.refresh-token:90}") int refreshTokenDays) {
    if (1 != jwks.getKeys().size()) {
      throw new IllegalStateException("Only one JWK can be provided in the JWKS");
    }

    return new BertocciTokenCreator(
        jwks.getKeys().get(0).toRSAKey(),
        issuer,
        clock,
        defaultResource,
        jwsAlgorithm,
        refreshTokenVerifier,
        accessTokenDays,
        refreshTokenDays);
  }

  @Bean
  public TokenVerifier accessTokenVerifier(
      JWKSource<SecurityContext> jwkSource,
      JWSAlgorithm expectedJWSAlg,
      @Value("${authorization-server.issuer}") String issuer,
      @Value("at+jwt") String jwtContentType) {
    return new BertocciTokenVerifier(jwkSource, expectedJWSAlg, issuer, jwtContentType);
  }

  @Bean
  public TokenVerifier refreshTokenVerifier(
      JWKSource<SecurityContext> jwkSource,
      JWSAlgorithm expectedJWSAlg,
      @Value("${authorization-server.issuer}") String issuer,
      @Value("rt+jwt") String jwtContentType) {
    return new BertocciTokenVerifier(jwkSource, expectedJWSAlg, issuer, jwtContentType);
  }
}
