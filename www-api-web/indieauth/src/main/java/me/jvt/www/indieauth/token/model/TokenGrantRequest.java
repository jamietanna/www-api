package me.jvt.www.indieauth.token.model;

import java.util.HashMap;
import java.util.List;

public class TokenGrantRequest extends HashMap<String, String> {
  private static final String CLIENT_ID = "client_id";
  private static final String CODE = "code";
  private static final String CODE_VERIFIER = "code_verifier";
  private static final String GRANT_TYPE = "grant_type";
  private static final String REDIRECT_URI = "redirect_uri";
  private static final String REFRESH_TOKEN = "refresh_token";

  public String getClientId() {
    return get(CLIENT_ID);
  }

  public void setClientId(String clientId) {
    put(CLIENT_ID, clientId);
  }

  public String getCode() {
    return get(CODE);
  }

  public void setCode(String code) {
    put(CODE, code);
  }

  public String getCodeVerifier() {
    return get(CODE_VERIFIER);
  }

  public void setCodeVerifier(String codeVerifier) {
    put(CODE_VERIFIER, codeVerifier);
  }

  public String getGrantType() {
    return get(GRANT_TYPE);
  }

  public void setGrantType(String grantType) {
    put(GRANT_TYPE, grantType);
  }

  public String getRedirectUri() {
    return get(REDIRECT_URI);
  }

  public void setRedirectUri(String redirectUri) {
    put(REDIRECT_URI, redirectUri);
  }

  public String getRefreshToken() {
    return get(REFRESH_TOKEN);
  }

  public void setRefreshToken(String refreshToken) {
    put(REFRESH_TOKEN, refreshToken);
  }

  public void put(String key, List<String> value) {
    if (null != value) {
      super.put(key, value.get(0));
    }
  }
}
