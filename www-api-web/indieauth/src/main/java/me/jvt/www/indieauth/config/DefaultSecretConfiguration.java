package me.jvt.www.indieauth.config;

import com.nimbusds.jose.jwk.JWK;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;

@Configuration
@Profile("default")
public class DefaultSecretConfiguration {
  @Bean
  public JWK jwk(@Value("file:${indieauth.secrets.keys.path}") Resource resource) {
    try {
      File file = resource.getFile();
      String jwk = new String(Files.readAllBytes(file.toPath()));
      return JWK.parse(jwk);
    } catch (IOException e) {
      throw new IllegalStateException("Could not read JWK", e);
    } catch (ParseException e) {
      throw new IllegalStateException("Failed to parse JWK", e);
    }
  }
}
