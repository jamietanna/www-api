package me.jvt.www.indieauth.controllers;

import java.util.function.Supplier;
import me.jvt.www.indieauth.authorizationservermetadata.ServerConfigurationDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthorizationServerMetadataController {
  private final Supplier<ServerConfigurationDto> supplier;

  public AuthorizationServerMetadataController(Supplier<ServerConfigurationDto> provider) {
    this.supplier = provider;
  }

  /**
   * Produce an RFC8414-compliant OAuth Authorization Server Metadata response.
   *
   * @return the OAuth Server Metadata
   */
  @GetMapping("/.well-known/oauth-authorization-server")
  public ServerConfigurationDto retrieveConfiguration() {
    return supplier.get();
  }
}
