package me.jvt.www.indieauth.exception;

public class InvalidTokenException extends WwwAuthenticateException {

  public InvalidTokenException(String errorDescription) {
    super("invalid_token", errorDescription);
  }
}
