package me.jvt.www.indieauth.exception;

import java.util.Optional;

public class NoAuthenticationProvidedException extends WwwAuthenticateException {
  public NoAuthenticationProvidedException(String errorDescription) {
    super(Optional.empty(), errorDescription);
  }
}
