package me.jvt.www.indieauth.config;

import java.time.Clock;
import java.util.Arrays;
import java.util.List;
import me.jvt.www.indieauth.granttype.GrantTypeHandler;
import me.jvt.www.indieauth.granttype.authorizationcode.AuthorizationCodeStore;
import me.jvt.www.indieauth.granttype.authorizationcode.InMemoryAuthorizationCodeStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GrantTypeConfiguration {

  @Bean
  public List<GrantTypeHandler> handlers(GrantTypeHandler... handlers) {
    return Arrays.asList(handlers);
  }

  @Bean
  public AuthorizationCodeStore authorizationCodeStore(Clock clock) {
    return new InMemoryAuthorizationCodeStore(clock);
  }
}
