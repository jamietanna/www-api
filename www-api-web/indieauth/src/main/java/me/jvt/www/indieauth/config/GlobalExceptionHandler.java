package me.jvt.www.indieauth.config;

import me.jvt.www.indieauth.exception.InvalidGrantException;
import me.jvt.www.indieauth.exception.OAuthErrorDto;
import me.jvt.www.indieauth.exception.UnsupportedGrantTypeException;
import me.jvt.www.indieauth.exception.WwwAuthenticateException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(InvalidGrantException.class)
  public ResponseEntity<OAuthErrorDto> handleInvalidGrantException(InvalidGrantException e) {
    OAuthErrorDto dto = new OAuthErrorDto();
    dto.setError("invalid_grant");
    return new ResponseEntity<>(dto, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(UnsupportedGrantTypeException.class)
  public ResponseEntity<OAuthErrorDto> handleUnsupportedGrantType(UnsupportedGrantTypeException e) {
    OAuthErrorDto dto = new OAuthErrorDto();
    dto.setError("unsupported_grant_type");
    return new ResponseEntity<>(dto, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(WwwAuthenticateException.class)
  public ResponseEntity<Void> handleWwwAuthenticateException(WwwAuthenticateException e) {
    return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
        .header("Www-Authenticate", wwwAuthenticate(e))
        .build();
  }

  private static String wwwAuthenticate(WwwAuthenticateException e) {
    StringBuilder builder = new StringBuilder();
    builder.append("Bearer");
    if (e.getError().isPresent()) {
      builder.append(
          " error=\"%s\", error_description=\"%s\""
              .formatted(e.getError().get(), e.getErrorDescription()));
    }

    return builder.toString();
  }
}
