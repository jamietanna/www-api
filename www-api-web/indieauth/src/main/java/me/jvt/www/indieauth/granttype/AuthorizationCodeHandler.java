package me.jvt.www.indieauth.granttype;

import java.util.Optional;
import me.jvt.www.indieauth.authorization.AuthorizationException;
import me.jvt.www.indieauth.authorization.AuthorizationRequest;
import me.jvt.www.indieauth.authorization.AuthorizationRequest.PKCE;
import me.jvt.www.indieauth.authorization.AuthorizationResponseDto;
import me.jvt.www.indieauth.exception.InvalidGrantException;
import me.jvt.www.indieauth.granttype.authorizationcode.AuthorizationCodeStore;
import me.jvt.www.indieauth.granttype.authorizationcode.InvalidAuthorizationCodeException;
import me.jvt.www.indieauth.granttype.authorizationcode.pkce.PKCEVerifier;
import me.jvt.www.indieauth.token.TokenCreator;
import me.jvt.www.indieauth.token.model.ProfileExchangeDto;
import me.jvt.www.indieauth.token.model.Token;
import me.jvt.www.indieauth.token.model.TokenExchangeDto;
import me.jvt.www.indieauth.token.model.TokenGrantRequest;
import org.springframework.stereotype.Component;

@Component
public class AuthorizationCodeHandler implements GrantTypeHandler {

  private final AuthorizationCodeStore store;
  private final TokenCreator creator;
  private final PKCEVerifier pkceVerifier;

  public AuthorizationCodeHandler(
      AuthorizationCodeStore store, TokenCreator creator, PKCEVerifier pkceVerifier) {
    this.store = store;
    this.creator = creator;
    this.pkceVerifier = pkceVerifier;
  }

  @Override
  public void preauthorize(AuthorizationRequest request) throws AuthorizationException {
    if (null != request.getPKCE()) {
      PKCE pkce = request.getPKCE();
      if (!isNotEmpty(pkce.getCodeChallenge()) && isNotEmpty(pkce.getCodeChallengeMethod())) {
        throw new AuthorizationException(
            AuthorizationException.INVALID_REQUEST, "PKCE requires code_challenge");
      } else if (isNotEmpty(pkce.getCodeChallenge())
          && !isNotEmpty(pkce.getCodeChallengeMethod())) {
        throw new AuthorizationException(
            AuthorizationException.INVALID_REQUEST, "PKCE requires code_challenge_method");
      }
      if (!pkceVerifier.supported(pkce.getCodeChallengeMethod())) {
        throw new AuthorizationException(
            AuthorizationException.INVALID_REQUEST,
            String.format(
                "This server does not support %s as a code_challenge_method",
                pkce.getCodeChallengeMethod()));
      }
    }
  }

  @Override
  public AuthorizationResponseDto authorize(AuthorizationRequest request)
      throws AuthorizationException {
    String code = store.put(request);
    AuthorizationResponseDto response = new AuthorizationResponseDto();
    response.setCode(code);
    response.setRedirectUri(request.getRedirectUri());
    response.setState(request.getState());
    return response;
  }

  @Override
  public String getGrantType() {
    return "authorization_code";
  }

  @Override
  public Optional<String> getResponseType() {
    return Optional.of("code");
  }

  @Override
  public ProfileExchangeDto profileExchange(TokenGrantRequest request) {
    AuthorizationRequest authorizationRequest = getAndVerifyAuthorizationRequest(request);
    return new ProfileExchangeDto(authorizationRequest.getMe());
  }

  @Override
  public TokenExchangeDto tokenExchange(TokenGrantRequest request) {
    AuthorizationRequest authorizationRequest = getAndVerifyAuthorizationRequest(request);

    Token token =
        creator.create(
            authorizationRequest.getClientId(),
            authorizationRequest.getMe(),
            authorizationRequest.getScopes(),
            authorizationRequest.shouldIssueRefreshToken());

    return new TokenExchangeDto(
        authorizationRequest.getMe(),
        token.getAccessToken(),
        token.getRefreshToken(),
        authorizationRequest.getScopes(),
        token.getExpiresIn());
  }

  private AuthorizationRequest getAndVerifyAuthorizationRequest(TokenGrantRequest request) {
    String code = request.getCode();
    AuthorizationRequest authorizationRequest;
    try {
      authorizationRequest = store.get(code);
    } catch (InvalidAuthorizationCodeException e) {
      throw new InvalidGrantException(e);
    }

    if (!authorizationRequest.getClientId().equals(request.getClientId())) {
      throw new InvalidGrantException();
    }
    if (!authorizationRequest.getRedirectUri().equals(request.getRedirectUri())) {
      throw new InvalidGrantException();
    }
    boolean validPkce =
        pkceVerifier.verify(
            Optional.ofNullable(authorizationRequest.getPKCE()),
            Optional.ofNullable(request.getCodeVerifier()));
    if (!validPkce) {
      throw new InvalidGrantException();
    }
    return authorizationRequest;
  }

  private boolean isNotEmpty(String s) {
    return !(null == s || s.isEmpty());
  }
}
