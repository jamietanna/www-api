package me.jvt.www.indieauth.authenticators;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import me.jvt.www.indieauth.authenticators.SessionCookieProvider.InvalidSessionCookieException;
import me.jvt.www.indieauth.authenticators.SessionCookieProvider.SessionCookie;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.template.AuthenticatorEntry;

public class DefaultAuthenticationService implements AuthenticationService {

  private final SessionCookieProvider provider;
  private final List<Authenticator> authenticators;
  private final List<AuthenticatorEntry> authenticatorEntries = new ArrayList<>();

  public DefaultAuthenticationService(
      SessionCookieProvider provider, List<Authenticator> authenticators) {
    this.provider = provider;
    if (authenticators.isEmpty()) {
      throw new IllegalArgumentException(
          "DefaultAuthenticationService requires at least one Authenticator");
    }
    this.authenticators = authenticators;
    for (Authenticator authenticator : authenticators) {
      authenticatorEntries.add(new AuthenticatorEntry(authenticator.id(), authenticator.name()));
    }
  }

  @Override
  public Optional<ProfileUrl> profileUrlFromCookie(Optional<String> cookieValue) {
    if (!cookieValue.isPresent()) {
      return Optional.empty();
    }

    try {
      SessionCookie cookie = provider.validate(cookieValue.get());
      return Optional.of(cookie.getProfileUrl());
    } catch (InvalidSessionCookieException e) {
      return Optional.empty();
    }
  }

  @Override
  public String authenticate(String authenticatorId, ProfileUrl me, Context context)
      throws AuthenticationFailedException, AuthenticationNotSupportedException,
          InternalAuthenticationException {
    Optional<Authenticator> maybeAuthenticator =
        authenticators.stream().filter(a -> a.id().equals(authenticatorId)).findFirst();
    Authenticator authenticator =
        maybeAuthenticator.orElseThrow(AuthenticationNotSupportedException::new);

    if (!authenticator.authenticate(me, context)) {
      throw new AuthenticationFailedException();
    }

    return provider.create(me).serialize();
  }

  @Override
  public List<AuthenticatorEntry> authenticatorsForMe(ProfileUrl me) {
    return authenticatorEntries;
  }
}
