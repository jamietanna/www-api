package me.jvt.www.indieauth.authorization;

import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.RandomStringUtils;

public class InMemoryPushedAuthorizationRepository implements PushedAuthorizationRepository {
  private static final String PUSHED_REQUEST_FORMAT = "urn:ietf:params:oauth:request_uri:%s";

  private final Map<String, Entry> requests = new HashMap<>();
  private final Clock clock;

  public InMemoryPushedAuthorizationRepository(Clock clock) {
    this.clock = clock;
  }

  @Override
  public String put(AuthorizationRequest request) {
    String pushedRequest =
        String.format(PUSHED_REQUEST_FORMAT, RandomStringUtils.randomAlphanumeric(10));
    requests.put(pushedRequest, new Entry(expirationDate(), request));
    return pushedRequest;
  }

  @Override
  public AuthorizationRequest get(String pushedRequest) throws AuthorizationException {
    Entry entry = requests.get(pushedRequest);
    if (null == entry || Instant.now(clock).isAfter(entry.getExpiry())) {
      throw new AuthorizationException(AuthorizationException.INVALID_REQUEST);
    }
    return entry.getRequest();
  }

  @Override
  public AuthorizationRequest pop(String pushedRequest) throws AuthorizationException {
    Entry entry = requests.remove(pushedRequest);
    if (null == entry || Instant.now(clock).isAfter(entry.getExpiry())) {
      throw new AuthorizationException(AuthorizationException.INVALID_REQUEST);
    }
    return entry.getRequest();
  }

  private Instant expirationDate() {
    return Instant.now(clock).truncatedTo(ChronoUnit.SECONDS).plus(1, ChronoUnit.MINUTES);
  }

  private static class Entry {

    private final Instant expiry;
    private final AuthorizationRequest request;

    public Entry(Instant expiry, AuthorizationRequest request) {
      this.expiry = expiry;
      this.request = request;
    }

    public Instant getExpiry() {
      return expiry;
    }

    public AuthorizationRequest getRequest() {
      return request;
    }
  }
}
