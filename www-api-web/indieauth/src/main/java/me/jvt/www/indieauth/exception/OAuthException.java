package me.jvt.www.indieauth.exception;

class OAuthException extends RuntimeException {

  OAuthException() {
    super();
  }

  OAuthException(Throwable cause) {
    super(cause);
  }
}
