package me.jvt.www.indieauth.config;

import com.okta.sdk.authc.credentials.TokenClientCredentials;
import com.okta.sdk.client.Client;
import com.okta.sdk.client.Clients;
import java.util.List;
import me.jvt.www.indieauth.authenticators.oktaverify.OktaFacade;
import me.jvt.www.indieauth.authenticators.oktaverify.OktaVerifyAuthenticator;
import me.jvt.www.indieauth.authenticators.oktaverify.SleepFacade;
import me.jvt.www.indieauth.domain.ProfileUrl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty("authenticator.okta.okta-baseurl")
public class OktaAuthenticatorConfiguration {
  @Bean
  public OktaFacade oktaFacade(Client client) {
    return new OktaFacade(client);
  }

  @Bean
  public Client oktaClient(
      @Value("${authenticator.okta.okta-baseurl}") String baseUrl,
      @Value("${authenticator.okta.api-token}") String apiToken) {
    TokenClientCredentials clientCredentials = new TokenClientCredentials(apiToken);

    return Clients.builder().setClientCredentials(clientCredentials).setOrgUrl(baseUrl).build();
  }

  @Bean
  public List<ProfileUrl> meUrls(
      @Value("#{'${authenticator.okta.me}'.split(',')}") List<String> meUrls) {
    return meUrls.stream().map(ProfileUrl::of).toList();
  }

  @Bean
  public OktaVerifyAuthenticator oktaVerifyAuthenticator(
      @Value("${authenticator.okta.user-id}") String userId,
      @Value("${authenticator.okta.factor-id}") String factorId,
      OktaFacade facade,
      SleepFacade sleepFacade,
      List<ProfileUrl> meUrls) {
    return new OktaVerifyAuthenticator(meUrls, userId, factorId, facade, sleepFacade);
  }
}
