package me.jvt.www.indieauth.exception;

public class InvalidGrantException extends OAuthException {
  public InvalidGrantException() {
    super();
  }

  public InvalidGrantException(Throwable cause) {
    super(cause);
  }
}
