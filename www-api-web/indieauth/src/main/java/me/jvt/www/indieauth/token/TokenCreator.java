package me.jvt.www.indieauth.token;

import java.util.Set;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.exception.InvalidGrantException;
import me.jvt.www.indieauth.token.model.Scope;
import me.jvt.www.indieauth.token.model.Token;

public interface TokenCreator {
  /**
   * Create an access token for the given parameters.
   *
   * @param clientId the clientId (URL) that will be issued this token
   * @param profileUrl the profile URL to issue this token for
   * @param scopes the {@link Scope}s that this token will be issued for. Must not be empty
   * @return the {@link Token} representation of the token
   * @throws InvalidGrantException if request is malformed
   */
  Token create(String clientId, ProfileUrl profileUrl, Set<Scope> scopes);
  /**
   * Create an access, and optionally refresh token for the given parameters.
   *
   * @param clientId the clientId (URL) that will be issued this token
   * @param profileUrl the profile URL to issue this token for
   * @param scopes the {@link Scope}s that this token will be issued for. Must not be empty
   * @param issueRefresh whether we should issue a refresh token
   * @return the {@link Token} representation of the token
   * @throws InvalidGrantException if request is malformed
   */
  Token create(String clientId, ProfileUrl profileUrl, Set<Scope> scopes, boolean issueRefresh);

  /**
   * Create an access token for the given parameters, for a specific resource URL.
   *
   * @param clientId the clientId (URL) that will be issued this token
   * @param profileUrl the profile URL to issue this token for
   * @param scopes the {@link Scope}s that this token will be issued for. Must not be empty
   * @param resource the resource server URL that this token will be scoped to
   * @return the {@link Token} representation of the token
   * @throws InvalidGrantException if request is malformed
   */
  Token create(String clientId, ProfileUrl profileUrl, Set<Scope> scopes, String resource);

  /**
   * Create an access token, and optionally refresh token, for the given parameters, for a specific
   * resource URL.
   *
   * @param clientId the clientId (URL) that will be issued this token
   * @param profileUrl the profile URL to issue this token for
   * @param scopes the {@link Scope}s that this token will be issued for. Must not be empty
   * @param resource the resource server URL that this token will be scoped to
   * @param issueRefresh whether we should issue a refresh token
   * @return the {@link Token} representation of the token
   * @throws InvalidGrantException if request is malformed
   */
  Token create(
      String clientId,
      ProfileUrl profileUrl,
      Set<Scope> scopes,
      String resource,
      boolean issueRefresh);

  /**
   * Refresh a client's refresh token.
   *
   * <p>This may produce a new refresh token, or it may not.
   *
   * @param refreshToken the refresh token to be refreshed
   * @param clientId the client who is requesting a refreshed access token
   * @return the {@link Token} representation of the token
   * @throws InvalidGrantException if request is malformed
   */
  Token refresh(String refreshToken, String clientId);
}
