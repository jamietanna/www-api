package me.jvt.www.indieauth.config;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import me.jvt.www.indieauth.authorizationservermetadata.DefaultServerConfigurationSupplier;
import me.jvt.www.indieauth.granttype.GrantTypeHandler;
import me.jvt.www.indieauth.granttype.authorizationcode.pkce.PKCEVerifier;
import me.jvt.www.indieauth.token.model.Scope;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AuthorizationServerConfiguration {
  @Bean
  public DefaultServerConfigurationSupplier supplier(
      @Value("${authorization-server.issuer}") String issuer,
      List<GrantTypeHandler> grantTypeHandlers,
      PKCEVerifier verifier) {
    return new DefaultServerConfigurationSupplier(
        issuer,
        String.format("%s/authorize", issuer),
        String.format("%s/token", issuer),
        String.format("%s/userinfo", issuer),
        Scope.getKnownScopes(),
        grantTypeHandlers.stream()
            .map(GrantTypeHandler::getResponseType)
            .flatMap(Optional::stream)
            .collect(Collectors.toSet()),
        Set.of("query"),
        grantTypeHandlers.stream().map(GrantTypeHandler::getGrantType).collect(Collectors.toSet()),
        String.format("%s/token_info", issuer),
        verifier.getSupportedMethods(),
        String.format("%s/jwks", issuer));
  }
}
