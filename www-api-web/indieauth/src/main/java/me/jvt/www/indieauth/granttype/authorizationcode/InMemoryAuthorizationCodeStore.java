package me.jvt.www.indieauth.granttype.authorizationcode;

import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import me.jvt.www.indieauth.authorization.AuthorizationRequest;
import org.apache.commons.lang3.RandomStringUtils;

public class InMemoryAuthorizationCodeStore implements AuthorizationCodeStore {

  private final Map<String, Entry> requests = new HashMap<>();
  private final Clock clock;

  public InMemoryAuthorizationCodeStore(Clock clock) {
    this.clock = clock;
  }

  @Override
  public AuthorizationRequest get(String code) throws InvalidAuthorizationCodeException {
    Entry entry = requests.remove(code);
    if (null == entry || Instant.now(clock).isAfter(entry.getExpiry())) {
      throw new InvalidAuthorizationCodeException();
    }
    return entry.getRequest();
  }

  @Override
  public String put(AuthorizationRequest request) {
    String code = RandomStringUtils.randomAlphanumeric(20);
    requests.put(code, new Entry(expirationDate(), request));
    return code;
  }

  private Instant expirationDate() {
    return Instant.now(clock).truncatedTo(ChronoUnit.SECONDS).plus(5, ChronoUnit.MINUTES);
  }

  private static class Entry {

    private final Instant expiry;
    private final AuthorizationRequest request;

    public Entry(Instant expiry, AuthorizationRequest request) {
      this.expiry = expiry;
      this.request = request;
    }

    public Instant getExpiry() {
      return expiry;
    }

    public AuthorizationRequest getRequest() {
      return request;
    }
  }
}
