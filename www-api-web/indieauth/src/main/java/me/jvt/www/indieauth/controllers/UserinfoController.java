package me.jvt.www.indieauth.controllers;

import java.util.Optional;
import me.jvt.www.indieauth.exception.InsufficientScopeException;
import me.jvt.www.indieauth.exception.InvalidTokenException;
import me.jvt.www.indieauth.exception.NoAuthenticationProvidedException;
import me.jvt.www.indieauth.token.model.UserinfoResponseDto;
import me.jvt.www.indieauth.userinfo.UserinfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/userinfo")
public class UserinfoController {
  private final UserinfoService userinfoService;

  public UserinfoController(UserinfoService userinfoService) {
    this.userinfoService = userinfoService;
  }

  @GetMapping
  public UserinfoResponseDto retrieve(@RequestHeader("Authorization") Optional<String> maybeToken)
      throws InvalidTokenException, NoAuthenticationProvidedException, InsufficientScopeException {
    if (maybeToken.isEmpty()) {
      throw new NoAuthenticationProvidedException("No authentication was provided");
    }
    var token = maybeToken.get();
    if (!token.startsWith("Bearer ")) {
      throw new NoAuthenticationProvidedException("No authentication was provided");
    }

    return userinfoService.retrieve(token.replace("Bearer ", ""));
  }
}
