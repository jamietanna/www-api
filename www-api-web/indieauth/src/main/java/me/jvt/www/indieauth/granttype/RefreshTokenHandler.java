package me.jvt.www.indieauth.granttype;

import java.util.Optional;
import me.jvt.www.indieauth.authorization.AuthorizationException;
import me.jvt.www.indieauth.authorization.AuthorizationRequest;
import me.jvt.www.indieauth.authorization.AuthorizationResponseDto;
import me.jvt.www.indieauth.exception.InvalidGrantException;
import me.jvt.www.indieauth.token.TokenCreator;
import me.jvt.www.indieauth.token.model.ProfileExchangeDto;
import me.jvt.www.indieauth.token.model.Token;
import me.jvt.www.indieauth.token.model.TokenExchangeDto;
import me.jvt.www.indieauth.token.model.TokenGrantRequest;
import org.springframework.stereotype.Component;

@Component
public class RefreshTokenHandler implements GrantTypeHandler {

  private final TokenCreator creator;

  public RefreshTokenHandler(TokenCreator creator) {
    this.creator = creator;
  }

  @Override
  public void preauthorize(AuthorizationRequest request) throws AuthorizationException {
    // not implemented
    throw new UnsupportedOperationException();
  }

  @Override
  public AuthorizationResponseDto authorize(AuthorizationRequest request)
      throws AuthorizationException {
    throw new UnsupportedOperationException();
  }

  @Override
  public String getGrantType() {
    return "refresh_token";
  }

  @Override
  public Optional<String> getResponseType() {
    return Optional.empty();
  }

  @Override
  public ProfileExchangeDto profileExchange(TokenGrantRequest request) {
    // not implemented
    throw new UnsupportedOperationException();
  }

  @Override
  public TokenExchangeDto tokenExchange(TokenGrantRequest request) {
    String refreshToken = request.getRefreshToken();
    if (null == refreshToken || refreshToken.isEmpty()) {
      throw new InvalidGrantException();
    }
    String clientId = request.getClientId();
    if (null == clientId || clientId.isEmpty()) {
      throw new InvalidGrantException();
    }

    Token refreshed = creator.refresh(refreshToken, clientId);
    return new TokenExchangeDto(
        refreshed.getAccessToken(), refreshed.getRefreshToken(), refreshed.getExpiresIn());
  }
}
