package me.jvt.www.indieauth.authenticators;

import java.util.Date;
import me.jvt.www.indieauth.domain.ProfileUrl;

public interface SessionCookieProvider {

  /**
   * Provide a {@link SessionCookie} for the given profile URL.
   *
   * @param profileUrl the profile URL to create a cookie value for
   * @return the {@link SessionCookie} value for the given profile URL
   */
  SessionCookie create(ProfileUrl profileUrl);

  /**
   * Validate the serialized value, and if so, return a parsed representation.
   *
   * @param cookie the serialized cookie
   * @throws InvalidSessionCookieException when invalid
   * @return the parsed {@link SessionCookie} from the serialized form
   */
  SessionCookie validate(String cookie) throws InvalidSessionCookieException;

  class InvalidSessionCookieException extends Exception {}

  interface SessionCookie {

    /**
     * The profile URL authenticated by this cookie.
     *
     * @return the profile URL
     */
    ProfileUrl getProfileUrl();
    /**
     * The last possible date that this cookie is valid for
     *
     * @return the expiry
     */
    Date getExpiry();

    /**
     * Serialize the value of this cookie to a cookie-safe String.
     *
     * @return String representation of the cookie
     */
    String serialize();
  }
}
