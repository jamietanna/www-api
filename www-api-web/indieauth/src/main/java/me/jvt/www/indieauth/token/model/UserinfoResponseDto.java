package me.jvt.www.indieauth.token.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import me.jvt.www.indieauth.domain.ProfileUrl;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
public record UserinfoResponseDto(
    String name, ProfileUrl url, String photo, java.util.Optional<String> email) {}
