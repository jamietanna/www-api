package me.jvt.www.indieauth.authenticators;

import java.util.HashMap;

/** Contextual information about the request for authentication. */
public class Context extends HashMap<String, String> {

  private static final String USER_AGENT = "user-agent";
  private static final String X_FORWARDED_FOR = "x-forwarded-for";

  public String getUserAgent() {
    return get(USER_AGENT);
  }

  public void setUserAgent(String userAgent) {
    put(USER_AGENT, userAgent);
  }

  public String getXForwardedFor() {
    return get(X_FORWARDED_FOR);
  }

  public void setXForwardedFor(String xForwardedFor) {
    put(X_FORWARDED_FOR, xForwardedFor);
  }
}
