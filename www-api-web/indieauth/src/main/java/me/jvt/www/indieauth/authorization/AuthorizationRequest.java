package me.jvt.www.indieauth.authorization;

import java.util.Objects;
import java.util.Set;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.token.model.Scope;

public class AuthorizationRequest {

  private String responseType;
  private String clientId;
  private String redirectUri;
  private PKCE pkce;
  private String state;
  private Set<Scope> scopes;
  private ProfileUrl me;
  private boolean issueRefreshToken;

  public String getResponseType() {
    return responseType;
  }

  public void setResponseType(String responseType) {
    this.responseType = responseType;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getRedirectUri() {
    return redirectUri;
  }

  public void setRedirectUri(String redirectUri) {
    this.redirectUri = redirectUri;
  }

  public PKCE getPKCE() {
    return pkce;
  }

  public void setPKCE(PKCE pkce) {
    this.pkce = pkce;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public Set<Scope> getScopes() {
    return scopes;
  }

  public void setScopes(Set<Scope> scopes) {
    this.scopes = scopes;
  }

  public ProfileUrl getMe() {
    return me;
  }

  public void setMe(ProfileUrl me) {
    this.me = me;
  }

  public boolean shouldIssueRefreshToken() {
    return issueRefreshToken;
  }

  public void setIssueRefreshToken(boolean issueRefreshToken) {
    this.issueRefreshToken = issueRefreshToken;
  }

  public static class PKCE {
    private String codeChallenge;
    private String codeChallengeMethod;

    public PKCE() {}

    public PKCE(String codeChallenge, String codeChallengeMethod) {
      this.codeChallenge = codeChallenge;
      this.codeChallengeMethod = codeChallengeMethod;
    }

    public String getCodeChallenge() {
      return codeChallenge;
    }

    public void setCodeChallenge(String codeChallenge) {
      this.codeChallenge = codeChallenge;
    }

    public String getCodeChallengeMethod() {
      return codeChallengeMethod;
    }

    public void setCodeChallengeMethod(String codeChallengeMethod) {
      this.codeChallengeMethod = codeChallengeMethod;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      PKCE pkce = (PKCE) o;
      return Objects.equals(codeChallenge, pkce.codeChallenge)
          && Objects.equals(codeChallengeMethod, pkce.codeChallengeMethod);
    }

    @Override
    public int hashCode() {
      return Objects.hash(codeChallenge, codeChallengeMethod);
    }
  }
}
