package me.jvt.www.indieauth.token.bertocci;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.BadJOSEException;
import com.nimbusds.jose.proc.DefaultJOSEObjectTypeVerifier;
import com.nimbusds.jose.proc.JWSKeySelector;
import com.nimbusds.jose.proc.JWSVerificationKeySelector;
import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import com.nimbusds.jwt.proc.DefaultJWTClaimsVerifier;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;
import java.text.ParseException;
import java.util.Collections;
import me.jvt.www.indieauth.token.TokenAdapter;
import me.jvt.www.indieauth.token.TokenVerificationException;
import me.jvt.www.indieauth.token.TokenVerifier;

/**
 * Implementation of {@link TokenVerifier}, based on
 * https://tools.ietf.org/html/draft-ietf-oauth-access-token-jwt-10#section-3.
 */
public class BertocciTokenVerifier implements TokenVerifier {

  private static final String NOT_A_VALID_JWT = "The token provided was not a valid JWT";
  private final ConfigurableJWTProcessor<SecurityContext> jwtProcessor;

  public BertocciTokenVerifier(
      JWKSource<SecurityContext> jwkSource,
      JWSAlgorithm expectedJWSAlg,
      String issuer,
      String jwtContentType) {
    jwtProcessor = new DefaultJWTProcessor<>();
    JWSKeySelector<SecurityContext> keySelector =
        new JWSVerificationKeySelector<>(expectedJWSAlg, jwkSource);
    jwtProcessor.setJWTClaimsSetVerifier(
        new DefaultJWTClaimsVerifier<>(
            new JWTClaimsSet.Builder().issuer(issuer).build(), Collections.emptySet()));

    jwtProcessor.setJWSKeySelector(keySelector);
    jwtProcessor.setJWSTypeVerifier(
        new DefaultJOSEObjectTypeVerifier<>(new JOSEObjectType(jwtContentType)));
  }

  @Override
  public TokenAdapter verify(String token) throws TokenVerificationException {
    SignedJWT jwt;
    try {
      if (null == token || token.isEmpty()) {
        throw new TokenVerificationException(NOT_A_VALID_JWT);
      }
      jwt = SignedJWT.parse(token);
    } catch (ParseException e) {
      throw new TokenVerificationException(NOT_A_VALID_JWT);
    }

    try {
      jwtProcessor.process(token, null);
    } catch (ParseException | JOSEException e) {
      throw new TokenVerificationException(NOT_A_VALID_JWT);
    } catch (BadJOSEException e) {
      throw new TokenVerificationException("The token provided was not correctly signed or formed");
    }

    return new BertocciTokenAdapter(jwt);
  }
}
