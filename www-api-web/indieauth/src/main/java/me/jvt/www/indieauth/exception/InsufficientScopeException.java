package me.jvt.www.indieauth.exception;

public class InsufficientScopeException extends WwwAuthenticateException {
  public InsufficientScopeException(String errorDescription) {
    super("insufficient_scope", errorDescription);
  }
}
