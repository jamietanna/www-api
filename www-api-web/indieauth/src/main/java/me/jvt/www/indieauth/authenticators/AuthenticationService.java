package me.jvt.www.indieauth.authenticators;

import java.util.List;
import java.util.Optional;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.template.AuthenticatorEntry;

public interface AuthenticationService {

  /**
   * For a given session cookie, determine its validity, and if valid, return the profile URL it
   * contains.
   *
   * @param cookieValue the <code>indieauth-authentication</code> session cookie
   * @return the profile URL, or <code>Optional.empty()</code> if not valid
   */
  Optional<ProfileUrl> profileUrlFromCookie(Optional<String> cookieValue);

  /**
   * Authenticate the user in <code>me</code> with a given authenticator, and provide the session
   * cookie value for that user.
   *
   * @param authenticatorId the authenticator ID that maps to a single {@link Authenticator}
   * @param me the profile URL of the user authenticating
   * @param context the context for the request
   * @return the session cookie value
   * @throws AuthenticationFailedException when the authentication fails
   * @throws AuthenticationNotSupportedException when the authentication was not possible for some
   *     reason
   * @throws InternalAuthenticationException when the authentication was not possible for some
   *     reason
   */
  String authenticate(String authenticatorId, ProfileUrl me, Context context)
      throws AuthenticationFailedException, AuthenticationNotSupportedException,
          InternalAuthenticationException;

  /**
   * List the {@link Authenticator}s that can be used for the given <code>me</code>.
   *
   * @param me the profile URL to authenticate
   * @return the authenticators that are configured. Will never be empty
   * @throws AuthenticationNotSupportedException if none supported
   */
  List<AuthenticatorEntry> authenticatorsForMe(ProfileUrl me)
      throws AuthenticationNotSupportedException;
}
