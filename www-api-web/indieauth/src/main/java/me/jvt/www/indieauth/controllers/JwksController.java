package me.jvt.www.indieauth.controllers;

import com.nimbusds.jose.jwk.JWKSet;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JwksController {
  private final JWKSet jwks;

  public JwksController(JWKSet jwks) {
    this.jwks = jwks;
  }

  @GetMapping(value = "/jwks", produces = "application/jwk-set+json")
  public String retrieveKeys() {
    return jwks.toString(true);
  }
}
