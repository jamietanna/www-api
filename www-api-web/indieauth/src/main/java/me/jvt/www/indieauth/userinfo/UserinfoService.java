package me.jvt.www.indieauth.userinfo;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Optional;
import me.jvt.www.indieauth.exception.InsufficientScopeException;
import me.jvt.www.indieauth.exception.InvalidTokenException;
import me.jvt.www.indieauth.exception.NoAuthenticationProvidedException;
import me.jvt.www.indieauth.token.TokenAdapter;
import me.jvt.www.indieauth.token.TokenService;
import me.jvt.www.indieauth.token.model.Scope;
import me.jvt.www.indieauth.token.model.UserinfoResponseDto;
import org.springframework.stereotype.Component;

@Component
public class UserinfoService {

  private final TokenService tokenService;

  public UserinfoService(TokenService tokenService) {
    this.tokenService = tokenService;
  }

  public UserinfoResponseDto retrieve(String token)
      throws NoAuthenticationProvidedException, InvalidTokenException, InsufficientScopeException {
    if (null == token || token.isEmpty()) {
      throw new NoAuthenticationProvidedException("Token was null/empty");
    }
    var maybeTokenAdapter = tokenService.verifyAndUnpack(token);
    if (maybeTokenAdapter.isEmpty()) {
      throw new InvalidTokenException("The token was not deemed valid");
    }
    var tokenAdapter = maybeTokenAdapter.get();
    if (!tokenAdapter.getScope().contains(Scope.PROFILE)) {
      throw new InsufficientScopeException("`profile` scope was not present");
    }

    tokenAdapter.getScope();
    tokenAdapter.getClientId();
    tokenAdapter.getSubject();
    return new UserinfoResponseDto(
        "Jamie Tanna",
        maybeTokenAdapter.get().getSubject(),
        "https://www.jvt.me/img/profile.jpg",
        mapEmail(tokenAdapter));
  }

  private static Optional<String> mapEmail(TokenAdapter tokenAdapter) {
    if (!tokenAdapter.getScope().contains(Scope.EMAIL)) {
      return Optional.empty();
    }

    String encoded =
        Base64.getUrlEncoder()
            .encodeToString(tokenAdapter.getClientId().getBytes(StandardCharsets.UTF_8));

    return Optional.of("indieauth+" + encoded + "@jamietanna.co.uk");
  }
}
