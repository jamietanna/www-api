package me.jvt.www.indieauth.token.bertocci;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.exception.InvalidGrantException;
import me.jvt.www.indieauth.token.TokenAdapter;
import me.jvt.www.indieauth.token.TokenCreator;
import me.jvt.www.indieauth.token.TokenVerificationException;
import me.jvt.www.indieauth.token.TokenVerifier;
import me.jvt.www.indieauth.token.model.Scope;
import me.jvt.www.indieauth.token.model.Token;

/**
 * Implementation of {@link TokenCreator}, based on
 * https://tools.ietf.org/html/draft-ietf-oauth-access-token-jwt-10#section-3.
 */
public class BertocciTokenCreator implements TokenCreator {

  private static final long SECONDS_IN_DAY = 60 * 60 * 24L;
  private static final String ACCESS_TOKEN_CONTENT_TYPE = "at+jwt";
  private static final String REFRESH_TOKEN_CONTENT_TYPE = "rt+jwt"; // not standard, but aligns

  private final JWSSigner signer;
  private final String issuer;
  private final Clock clock;
  private final String defaultResource;
  private final JWSAlgorithm algorithm;
  private final TokenVerifier refreshTokenVerifier;
  private final int accessTokenDays;
  private final int refreshTokenDays;

  public BertocciTokenCreator(
      RSAKey key,
      String issuer,
      Clock clock,
      String defaultResource,
      JWSAlgorithm algorithm,
      TokenVerifier refreshTokenVerifier,
      int accessTokenDays,
      int refreshTokenDays) {
    this.algorithm = algorithm;
    this.refreshTokenVerifier = refreshTokenVerifier;
    try {
      this.signer = new RSASSASigner(key);
    } catch (JOSEException e) {
      throw new IllegalArgumentException("Invalid key provided", e);
    }
    this.issuer = issuer;
    this.clock = clock;
    this.defaultResource = defaultResource;
    this.accessTokenDays = accessTokenDays;
    this.refreshTokenDays = refreshTokenDays;
  }

  @Override
  public Token create(String clientId, ProfileUrl profileUrl, Set<Scope> scopes) {
    return create(clientId, profileUrl, scopes, defaultResource);
  }

  @Override
  public Token create(
      String clientId, ProfileUrl profileUrl, Set<Scope> scopes, boolean issueRefresh) {
    return create(clientId, profileUrl, scopes, defaultResource, issueRefresh);
  }

  @Override
  public Token create(String clientId, ProfileUrl profileUrl, Set<Scope> scopes, String resource) {
    return create(clientId, profileUrl, scopes, resource, false);
  }

  @Override
  public Token create(
      String clientId,
      ProfileUrl profileUrl,
      Set<Scope> scopes,
      String resource,
      boolean issueRefresh) {
    if (scopes.isEmpty()) {
      throw new InvalidGrantException();
    }
    Instant now = now();
    Token theToken = new Token();
    theToken.setAccessToken(accessToken(clientId, profileUrl, scopes, resource, now));

    if (issueRefresh) {
      theToken.setRefreshToken(refreshToken(clientId, profileUrl, scopes, resource, now));
    }

    theToken.setExpiresIn(SECONDS_IN_DAY * accessTokenDays);
    return theToken;
  }

  @Override
  public Token refresh(String refreshToken, String clientId) {
    if (null == refreshToken || refreshToken.isEmpty()) {
      throw new InvalidGrantException();
    }
    TokenAdapter parsed;
    try {
      parsed = refreshTokenVerifier.verify(refreshToken);
    } catch (TokenVerificationException e) {
      throw new InvalidGrantException(e);
    }

    Instant now = now();
    Token token = new Token();
    if (!TokenAdapter.TokenType.REFRESH_TOKEN.equals(parsed.getTokenType())) {
      throw new InvalidGrantException();
    }
    String tokenClientId = parsed.getClientId();
    if (!tokenClientId.equals(clientId)) {
      throw new InvalidGrantException();
    }
    ProfileUrl profileUrl = parsed.getSubject();
    Set<Scope> scopes = parsed.getScope();
    List<String> resource = parsed.getAudience();
    token.setAccessToken(accessToken(clientId, profileUrl, scopes, resource.get(0), now));
    token.setRefreshToken(refreshToken(clientId, profileUrl, scopes, resource.get(0), now));
    token.setExpiresIn(SECONDS_IN_DAY * accessTokenDays);

    return token;
  }

  private String accessToken(
      String clientId, ProfileUrl profileUrl, Set<Scope> scopes, String resource, Instant now) {
    return accessToken(clientId, profileUrl, Scope.toScopeString(scopes), resource, now);
  }

  private String accessToken(
      String clientId, ProfileUrl profileUrl, String scopes, String resource, Instant now) {
    JWTClaimsSet claims =
        new JWTClaimsSet.Builder()
            .issuer(issuer)
            .expirationTime(accessTokenExpirationTime())
            .audience(resource)
            .subject(profileUrl.profileUrl().toString())
            .claim("client_id", clientId)
            .issueTime(issueTime(now))
            .claim("auth_time", now.getEpochSecond())
            .claim("scope", scopes)
            .claim("token_type", "access_token")
            .jwtID(UUID.randomUUID().toString())
            .build();
    JWSHeader header =
        new JWSHeader.Builder(algorithm)
            .type(new JOSEObjectType(ACCESS_TOKEN_CONTENT_TYPE))
            .build();
    SignedJWT accessToken = new SignedJWT(header, claims);
    try {
      accessToken.sign(signer);
    } catch (JOSEException e) {
      throw new IllegalStateException(e);
    }
    return accessToken.serialize();
  }

  private String refreshToken(
      String clientId, ProfileUrl profileUrl, Set<Scope> scopes, String resource, Instant now) {
    return refreshToken(clientId, profileUrl, Scope.toScopeString(scopes), resource, now);
  }

  private String refreshToken(
      String clientId, ProfileUrl profileUrl, String scopes, String resource, Instant now) {
    JWSHeader header =
        new JWSHeader.Builder(algorithm)
            .type(new JOSEObjectType(REFRESH_TOKEN_CONTENT_TYPE))
            .build();
    JWTClaimsSet claims =
        new JWTClaimsSet.Builder()
            .issuer(issuer)
            .expirationTime(refreshTokenExpirationTime())
            .audience(resource)
            .subject(profileUrl.profileUrl().toString())
            .claim("client_id", clientId)
            .issueTime(issueTime(now))
            .claim("auth_time", now.getEpochSecond())
            .claim("scope", scopes)
            .claim("token_type", "refresh_token")
            .jwtID(UUID.randomUUID().toString())
            .build();
    SignedJWT refreshToken = new SignedJWT(header, claims);
    try {
      refreshToken.sign(signer);
    } catch (JOSEException e) {
      throw new IllegalStateException(e);
    }
    return refreshToken.serialize();
  }

  private Instant now() {
    return Instant.now(clock);
  }

  private Date issueTime(Instant now) {
    return Date.from(now);
  }

  private Date accessTokenExpirationTime() {
    Instant now = Instant.now(clock);

    return Date.from(now.plus(accessTokenDays, ChronoUnit.DAYS));
  }

  private Date refreshTokenExpirationTime() {
    Instant now = Instant.now(clock);

    return Date.from(now.plus(refreshTokenDays, ChronoUnit.DAYS));
  }
}
