package me.jvt.www.indieauth.token.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.token.TokenAdapter;

@JsonInclude(Include.NON_NULL)
public class TokenVerificationDto {

  private boolean active;

  @JsonProperty("token_type")
  private String tokenType;

  @JsonProperty("client_id")
  private String clientId;

  @JsonProperty("exp")
  private long expiration;

  @JsonProperty("iat")
  private long issueTime;

  private ProfileUrl me;

  private String scope;

  @JsonProperty("iss")
  private String issuer;

  @JsonProperty("aud")
  private List<String> audience;

  public TokenVerificationDto() {
    // empty constructor for i.e. Jackson
  }

  public TokenVerificationDto(TokenAdapter token) {
    setActive(true);
    setClientId(token.getClientId());
    setExpiration(token.getExpirationTime());
    setIssueTime(token.getIssueTime());
    setMe(token.getSubject());
    setScope(Scope.toScopeString(token.getScope()));
    setIssuer(token.getIssuer());
    setAudience(token.getAudience());
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
    if (this.active) {
      this.tokenType = "Bearer";
    }
  }

  public String getTokenType() {
    return tokenType;
  }

  public void setTokenType(String tokenType) {
    this.tokenType = tokenType;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public long getExpiration() {
    return expiration;
  }

  public void setExpiration(long expiration) {
    this.expiration = expiration;
  }

  public long getIssueTime() {
    return issueTime;
  }

  public void setIssueTime(long issueTime) {
    this.issueTime = issueTime;
  }

  @JsonProperty("sub")
  public String getSubject() {
    if (null == me) {
      return null;
    }

    return me.profileUrl().toString();
  }

  public String getMe() {
    return getSubject();
  }

  public void setMe(ProfileUrl me) {
    this.me = me;
  }

  public String getScope() {
    return scope;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }

  public String getIssuer() {
    return issuer;
  }

  public void setIssuer(String issuer) {
    this.issuer = issuer;
  }

  public List<String> getAudience() {
    return audience;
  }

  public void setAudience(List<String> audience) {
    this.audience = audience;
  }
}
