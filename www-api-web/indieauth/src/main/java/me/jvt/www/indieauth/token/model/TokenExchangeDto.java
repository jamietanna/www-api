package me.jvt.www.indieauth.token.model;

import java.util.Set;
import me.jvt.www.indieauth.domain.ProfileUrl;

public class TokenExchangeDto {
  private long expiresIn;
  private ProfileUrl me;
  private String accessToken;
  private String refreshToken;
  private Set<Scope> scopes;

  public TokenExchangeDto(String accessToken, String refreshToken, long expiresIn) {
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
    this.expiresIn = expiresIn;
  }

  public TokenExchangeDto(
      ProfileUrl me, String accessToken, String refreshToken, Set<Scope> scopes, long expiresIn) {
    this.me = me;
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
    this.scopes = scopes;
    this.expiresIn = expiresIn;
  }

  public long getExpiresIn() {
    return expiresIn;
  }

  public ProfileUrl getMe() {
    return me;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public Set<Scope> getScopes() {
    return scopes;
  }

  public String getRefreshToken() {
    return refreshToken;
  }
}
