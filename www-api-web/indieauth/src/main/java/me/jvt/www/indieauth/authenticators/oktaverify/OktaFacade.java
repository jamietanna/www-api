package me.jvt.www.indieauth.authenticators.oktaverify;

import com.okta.sdk.client.Client;
import com.okta.sdk.error.authc.InvalidAuthenticationException;
import com.okta.sdk.resource.user.User;
import com.okta.sdk.resource.user.factor.UserFactor;
import com.okta.sdk.resource.user.factor.VerifyUserFactorResponse;
import java.util.Map;

public class OktaFacade {

  private final Client client;

  public OktaFacade(Client client) {
    this.client = client;
  }

  /**
   * Trigger the Okta Verify flow for the configured user and factor.
   *
   * @param userId the userId to verify
   * @param factorId the factorId for the device to verify
   * @param xForwardedFor the value of the <code>X-Forwarded-For</code> header
   * @param userAgent the value of the <code>User-Agent</code> header
   * @return the transaction ID for the verify request
   * @throws InvalidAuthenticationException if something goes wrong while trying to trigger the
   *     verification
   */
  public String triggerVerify(
      String userId, String factorId, String xForwardedFor, String userAgent) {
    User me = client.getUser(userId);
    UserFactor factor = me.getFactor(factorId);
    VerifyUserFactorResponse response =
        factor.verify(null, null, null, xForwardedFor, userAgent, null);
    Map<String, String> poll = ((Map<String, String>) response.getLinks().get("poll"));
    return poll.get("href").replaceAll(".*/transactions/", "");
  }

  /**
   * Poll the Okta API for the specified <code>transactionId</code>.
   *
   * @param userId the userId to verify
   * @param factorId the factorId for the device to verify
   * @param transactionId the transactionId provided by <code>triggerVerify</code>
   * @return the response from the Okta API
   */
  public VerificationResult isTransactionSuccess(
      String userId, String factorId, String transactionId) {
    return VerificationResult.fromFactorResultEnum(
        client.getFactorTransactionStatus(userId, factorId, transactionId).getFactorResult());
  }
}
