package me.jvt.www.indieauth.token;

import java.util.List;
import java.util.Set;
import me.jvt.www.indieauth.domain.ProfileUrl;
import me.jvt.www.indieauth.token.model.Scope;

public interface TokenAdapter {
  ProfileUrl getSubject();

  String getClientId();

  long getExpirationTime();

  long getIssueTime();

  Set<Scope> getScope();

  List<String> getAudience();

  TokenType getTokenType();

  String getIssuer();

  long getAuthTime();

  enum TokenType {
    ACCESS_TOKEN("access_token"),
    REFRESH_TOKEN("refresh_token");

    private final String name;

    TokenType(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

    public static TokenType fromString(String stringValue) {
      for (TokenType value : values()) {
        if (value.getName().equals(stringValue)) {
          return value;
        }
      }
      throw new IllegalArgumentException(
          String.format("The token_type=%s is not supported at this time", stringValue));
    }
  }
}
