package me.jvt.www.indieauth.token.model;

import java.util.*;
import java.util.stream.Collectors;

public class Scope {
  public static final Scope CREATE =
      new Scope("create", "Allows the application to publish new posts", Group.PUBLISHING);
  public static final Scope DRAFT =
      new Scope(
          "draft",
          "Posts created by this application will only be created as a draft, and will not be able to modify published posts",
          Group.PUBLISHING);
  public static final Scope DELETE =
      new Scope("delete", "Allows the application to delete posts", Group.PUBLISHING);
  public static final Scope UNDELETE =
      new Scope(
          "undelete",
          "Allows the application to undelete previously deleted posts",
          Group.PUBLISHING);
  public static final Scope UPDATE =
      new Scope("update", "Allows the application to update existing posts", Group.PUBLISHING);
  public static final Scope MEDIA =
      new Scope(
          "media",
          "Allows the application to upload media files to the media endpoint",
          Group.PUBLISHING);
  public static final Scope READ =
      new Scope("read", "Allows the application to read content from channels", Group.READING);
  public static final Scope FOLLOW =
      new Scope("follow", "Allows the application to follow and unfollow feeds", Group.READING);
  public static final Scope MUTE =
      new Scope("mute", "Allows the application to mute and unmute users", Group.READING);
  public static final Scope BLOCK =
      new Scope("block", "Allows the application to block and unblock users", Group.READING);
  public static final Scope CHANNELS =
      new Scope("channels", "Allows the application to manage your channels", Group.READING);

  public static final Scope NOTIFY =
      new Scope(
          "notify", "Allows the application to send you a push notification", Group.NOTIFICATION);

  public static final Scope PROFILE =
      new Scope(
          "profile",
          "Allows the application to retrieve information about who you are, such as your name, and photo",
          Group.PROFILE);
  public static final Scope EMAIL =
      new Scope(
          "email",
          "Allows the application to retrieve the email address (with a unique identifier for the client_id)",
          Group.PROFILE);

  private static final Map<String, Scope> SCOPES;
  private static final List<Scope> PUBLISHING_SCOPES;
  private static final List<Scope> READING_SCOPES;
  private static final List<Scope> NOTIFICATION_SCOPES;
  private static final List<Scope> PROFILE_SCOPES;

  static {
    List<Scope> publishing = new ArrayList<>();
    publishing.add(CREATE);
    publishing.add(DRAFT);
    publishing.add(DELETE);
    publishing.add(UNDELETE);
    publishing.add(UPDATE);
    publishing.add(MEDIA);
    PUBLISHING_SCOPES = Collections.unmodifiableList(publishing);

    List<Scope> reading = new ArrayList<>();
    reading.add(READ);
    reading.add(FOLLOW);
    reading.add(MUTE);
    reading.add(BLOCK);
    reading.add(CHANNELS);
    READING_SCOPES = Collections.unmodifiableList(reading);

    List<Scope> notifications = new ArrayList<>();
    notifications.add(NOTIFY);
    NOTIFICATION_SCOPES = Collections.unmodifiableList(notifications);

    List<Scope> profile = new ArrayList<>();
    profile.add(PROFILE);
    profile.add(EMAIL);
    PROFILE_SCOPES = Collections.unmodifiableList(profile);

    Map<String, Scope> scopes = new HashMap<>();
    for (Scope s : PUBLISHING_SCOPES) {
      scopes.put(s.getName(), s);
    }
    for (Scope s : READING_SCOPES) {
      scopes.put(s.getName(), s);
    }
    for (Scope s : NOTIFICATION_SCOPES) {
      scopes.put(s.getName(), s);
    }
    for (Scope s : PROFILE_SCOPES) {
      scopes.put(s.getName(), s);
    }

    SCOPES = Collections.unmodifiableMap(scopes);
  }

  public static List<Scope> getKnownPublishingScopes() {
    return PUBLISHING_SCOPES;
  }

  public static List<Scope> getKnownReadingScopes() {
    return READING_SCOPES;
  }

  public static List<Scope> getKnownNotificationScopes() {
    return NOTIFICATION_SCOPES;
  }

  public static List<Scope> getKnownProfileScopes() {
    return PROFILE_SCOPES;
  }

  public static Set<Scope> getKnownScopes() {
    return new HashSet<>(SCOPES.values());
  }

  private final Group group;

  public enum Group {
    PUBLISHING("Publishing"),
    READING("Reading"),
    NOTIFICATION("Notifications"),
    PROFILE("Profile"),
    UNKNOWN("Unknown");

    private final String name;

    Group(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }
  }

  private final String name;

  private final String description;

  public Scope(String name) {
    this.name = name;
    this.description = "This scope is not known to this server";
    this.group = Group.UNKNOWN;
  }

  public Scope(String name, String description, Group group) {
    this.name = name;
    this.description = description;
    this.group = group;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public Group getGroup() {
    return group;
  }

  public static String toScopeString(Set<Scope> scopes) {
    if (null == scopes) {
      return null;
    }

    return scopes.stream()
        .map(Scope::getName)
        .sorted(String::compareTo)
        .collect(Collectors.joining(" "));
  }

  public static Set<Scope> fromScopeString(String scopeString) {
    if (null == scopeString || scopeString.isEmpty()) {
      return Collections.emptySet();
    }

    return Arrays.stream(scopeString.split(" ")).map(Scope::fromString).collect(Collectors.toSet());
  }

  public static Scope fromString(String scope) {
    if (null == scope || scope.isEmpty()) {
      throw new IllegalArgumentException("Scope was invalid");
    }
    String lowercase = scope.toLowerCase();
    Scope found = SCOPES.get(lowercase);
    if (null != found) {
      return found;
    }
    return new Scope(lowercase);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Scope scope = (Scope) o;
    return Objects.equals(name, scope.name) && Objects.equals(description, scope.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, description);
  }
}
