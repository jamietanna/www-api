package me.jvt.www.indieauth.authenticators;

/** An exception to note that the user trying to authenticate is not supported. */
public class AuthenticationNotSupportedException extends Exception {

  public AuthenticationNotSupportedException() {
    super("The me used to authenticate was not supported by this authenticator.");
  }
}
