package me.jvt.www.indieauth.authenticators;

/**
 * An exception to note that something went wrong while trying to determine if the user could
 * authenticate.
 */
public class InternalAuthenticationException extends Exception {

  public InternalAuthenticationException(String message) {
    super(message);
  }
}
