package me.jvt.www.indieauth.authorization;

/** https://tools.ietf.org/html/draft-ietf-oauth-par-04 */
public interface PushedAuthorizationRepository {
  String put(AuthorizationRequest request);

  AuthorizationRequest get(String pushedRequest) throws AuthorizationException;

  AuthorizationRequest pop(String pushedRequest) throws AuthorizationException;
}
