package me.jvt.www.indieauth.authorizationservermetadata;

import java.util.Set;
import java.util.function.Supplier;
import me.jvt.www.indieauth.token.model.Scope;

/**
 * Default implementation for a {@link Supplier<ServerConfigurationDto>}, which caches the {@link
 * ServerConfigurationDto} on construction of the object.
 */
public class DefaultServerConfigurationSupplier implements Supplier<ServerConfigurationDto> {

  private final ServerConfigurationDto dto;

  public DefaultServerConfigurationSupplier(
      String issuer,
      String authorizationEndpoint,
      String tokenEndpoint,
      String userinfoEndpoint,
      Set<Scope> scopesSupported,
      Set<String> responseTypesSupported,
      Set<String> responseModesSupported,
      Set<String> grantTypesSupported,
      String introspectionEndpoint,
      Set<String> codeChallengeMethodsSupported,
      String jwksUri) {
    dto =
        new ServerConfigurationDto(
            issuer,
            authorizationEndpoint,
            tokenEndpoint,
            userinfoEndpoint,
            scopesSupported,
            responseTypesSupported,
            responseModesSupported,
            grantTypesSupported,
            introspectionEndpoint,
            codeChallengeMethodsSupported,
            jwksUri);
  }

  @Override
  public ServerConfigurationDto get() {
    return dto;
  }
}
