package me.jvt.www.indieauth.granttype.authorizationcode;

import me.jvt.www.indieauth.authorization.AuthorizationRequest;

public interface AuthorizationCodeStore {
  AuthorizationRequest get(String code) throws InvalidAuthorizationCodeException;

  String put(AuthorizationRequest request);
}
