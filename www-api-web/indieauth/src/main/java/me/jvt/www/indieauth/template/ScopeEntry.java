package me.jvt.www.indieauth.template;

import me.jvt.www.indieauth.token.model.Scope;

public class ScopeEntry {
  private final String name;
  private final String description;
  private final boolean requested;

  public ScopeEntry(Scope scope, boolean requested) {
    this.name = scope.getName();
    this.description = scope.getDescription();
    this.requested = requested;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public boolean isRequested() {
    return requested;
  }
}
