package me.jvt.www.indieauth.config;

import java.security.SecureRandom;
import java.util.List;
import me.jvt.www.indieauth.authenticators.AuthenticationService;
import me.jvt.www.indieauth.authenticators.Authenticator;
import me.jvt.www.indieauth.authenticators.Context;
import me.jvt.www.indieauth.authenticators.DefaultAuthenticationService;
import me.jvt.www.indieauth.authenticators.JwsSessionCookieProvider;
import me.jvt.www.indieauth.authenticators.SessionCookieProvider;
import me.jvt.www.indieauth.domain.ProfileUrl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class AuthenticatorConfiguration {

  @Bean
  @Profile("local")
  public Authenticator noopAuthenticator() {
    return new Authenticator() {
      @Override
      public String id() {
        return "noop";
      }

      @Override
      public String name() {
        return "No-Op";
      }

      @Override
      public boolean authenticate(ProfileUrl me, Context context) {
        return true;
      }
    };
  }

  @Bean
  public AuthenticationService authenticationService(
      SessionCookieProvider provider, List<Authenticator> authenticators) {
    return new DefaultAuthenticationService(provider, authenticators);
  }

  @Bean
  public SessionCookieProvider provider(SecureRandom random) {
    byte[] sharedSecret = new byte[32];
    random.nextBytes(sharedSecret);
    return new JwsSessionCookieProvider(sharedSecret);
  }

  @Bean
  public SecureRandom random() {
    return new SecureRandom();
  }
}
