package me.jvt.www.editor.config;

import java.util.Base64;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import me.jvt.www.editor.security.AESJWETokenService;
import me.jvt.www.editor.security.TokenService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SecretConfiguration {
  @Bean
  public SecretKey accessTokenEncryptionKey(
      @Value("${access-token-encryption.key.base64}") String accessTokenEncryptionKeyBase64) {
    byte[] encoded = Base64.getDecoder().decode(accessTokenEncryptionKeyBase64);

    return new SecretKeySpec(encoded, "AES");
  }

  @Bean
  public TokenService tokenEncrypter(SecretKey secretKey) {
    return new AESJWETokenService(secretKey);
  }
}
