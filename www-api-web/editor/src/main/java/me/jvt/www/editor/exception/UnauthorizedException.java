package me.jvt.www.editor.exception;

public class UnauthorizedException extends RuntimeException {
  public UnauthorizedException(String description) {
    super(description);
  }
}
