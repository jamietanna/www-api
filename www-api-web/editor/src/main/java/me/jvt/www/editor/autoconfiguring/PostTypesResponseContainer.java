package me.jvt.www.editor.autoconfiguring;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class PostTypesResponseContainer {

  @JsonProperty("post-types")
  private List<MicropubPostType> postTypes;

  @SuppressWarnings("unsued")
  PostTypesResponseContainer() {}

  public List<MicropubPostType> getPostTypes() {
    return postTypes;
  }

  public void setPostTypes(List<MicropubPostType> postTypes) {
    this.postTypes = postTypes;
  }

  public static class MicropubPostType {
    private String type;
    private String name;

    private String h;
    private List<String> properties;

    @JsonProperty("required-properties")
    private List<String> requiredProperties;

    public String getType() {
      return type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getH() {
      return h;
    }

    public void setH(String h) {
      this.h = h;
    }

    public List<String> getProperties() {
      return properties;
    }

    public void setProperties(List<String> properties) {
      this.properties = properties;
    }

    public List<String> getRequiredProperties() {
      return requiredProperties;
    }

    public void setRequiredProperties(List<String> requiredProperties) {
      this.requiredProperties = requiredProperties;
    }
  }
}
