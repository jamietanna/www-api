package me.jvt.www.editor.security;

public interface TokenService {
  /**
   * Encrypt the given opaque access token, returning the encrypted opaque value.
   *
   * @param accessToken the decrypted token to encrypt
   * @return the encrypted token
   */
  String encrypt(String accessToken);

  /**
   * Decrypt the provided opaque access token, returning the encrypted opaque value.
   *
   * @param encryptedAccessToken the encrypted token to decrypt
   * @throws TokenDecryptionException if the token is not valid, or anything goes wrong while
   *     decrypting
   * @return the decrypted token
   */
  String decrypt(String encryptedAccessToken) throws TokenDecryptionException;
}
