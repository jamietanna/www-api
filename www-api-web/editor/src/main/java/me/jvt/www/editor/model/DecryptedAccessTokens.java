package me.jvt.www.editor.model;

public class DecryptedAccessTokens {
  private DecryptedAccessToken production;
  private DecryptedAccessToken staging;

  public DecryptedAccessToken getProduction() {
    return production;
  }

  public void setProduction(DecryptedAccessToken production) {
    this.production = production;
  }

  public DecryptedAccessToken getStaging() {
    return staging;
  }

  public void setStaging(DecryptedAccessToken staging) {
    this.staging = staging;
  }
}
