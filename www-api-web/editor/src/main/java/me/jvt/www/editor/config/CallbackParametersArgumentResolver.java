package me.jvt.www.editor.config;

import me.jvt.www.editor.model.CallbackParameters;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class CallbackParametersArgumentResolver implements HandlerMethodArgumentResolver {

  @Override
  public boolean supportsParameter(MethodParameter parameter) {
    return parameter.getParameter().getType() == CallbackParameters.class;
  }

  @Override
  public Object resolveArgument(
      MethodParameter parameter,
      ModelAndViewContainer mavContainer,
      NativeWebRequest webRequest,
      WebDataBinderFactory binderFactory)
      throws Exception {
    String code = validateParam(webRequest, "code");
    String state = validateParam(webRequest, "state");

    return new CallbackParameters(code, state);
  }

  private String validateParam(NativeWebRequest request, String param) {
    String paramValue = ((ServletWebRequest) request).getRequest().getParameter(param);
    if (null == paramValue) {
      throw new IllegalArgumentException(String.format("%s parameter was not provided", param));
    }

    if (paramValue.isEmpty()) {
      throw new IllegalArgumentException(String.format("%s parameter was empty", param));
    }

    return paramValue;
  }
}
