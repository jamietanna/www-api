package me.jvt.www.editor.config;

import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.indieauthclient.InMemoryIndieAuthService;
import me.jvt.www.indieauthclient.IndieAuthClient;
import me.jvt.www.indieauthclient.StaticIndieAuthConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IndieAuthConfiguration {

  @Bean
  public IndieAuthClient indieAuthClient(
      RequestSpecificationFactory requestSpecificationFactory,
      @Value("${indieauth.authorization-endpoint}") String authorizationEndpoint,
      @Value("${indieauth.token-endpoint}") String tokenEndpoint,
      @Value("${indieauth.client-id}") String clientId,
      @Value("${indieauth.redirect-uri}") String redirectUri) {
    return new IndieAuthClient(
        new StaticIndieAuthConfiguration(authorizationEndpoint, tokenEndpoint),
        clientId,
        redirectUri,
        requestSpecificationFactory);
  }

  @Bean
  public InMemoryIndieAuthService inMemoryIndieAuthService(IndieAuthClient client) {
    return new InMemoryIndieAuthService(client);
  }
}
