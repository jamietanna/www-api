package me.jvt.www.editor.model;

import java.util.Objects;

public class AccountInformation {

  private String accessToken;
  private String micropubEndpoint;
  private String micropubMediaEndpoint;

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getMicropubEndpoint() {
    return micropubEndpoint;
  }

  public void setMicropubEndpoint(String micropubEndpoint) {
    this.micropubEndpoint = micropubEndpoint;
  }

  public String getMicropubMediaEndpoint() {
    return micropubMediaEndpoint;
  }

  public void setMicropubMediaEndpoint(String micropubMediaEndpoint) {
    this.micropubMediaEndpoint = micropubMediaEndpoint;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccountInformation that = (AccountInformation) o;
    return Objects.equals(accessToken, that.accessToken)
        && Objects.equals(micropubEndpoint, that.micropubEndpoint)
        && Objects.equals(micropubMediaEndpoint, that.micropubMediaEndpoint);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accessToken, micropubEndpoint);
  }
}
