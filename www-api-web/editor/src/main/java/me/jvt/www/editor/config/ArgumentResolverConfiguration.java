package me.jvt.www.editor.config;

import java.util.List;
import me.jvt.www.editor.security.TokenService;
import org.springframework.stereotype.Component;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class ArgumentResolverConfiguration implements WebMvcConfigurer {

  private final TokenService tokenService;

  public ArgumentResolverConfiguration(TokenService tokenService) {
    this.tokenService = tokenService;
  }

  @Override
  public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
    resolvers.add(new CallbackParametersArgumentResolver());
    resolvers.add(new DecryptedAccessTokensArgumentResolver(tokenService));
  }
}
