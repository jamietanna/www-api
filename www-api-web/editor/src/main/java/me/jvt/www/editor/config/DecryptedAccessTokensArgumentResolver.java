package me.jvt.www.editor.config;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import me.jvt.www.editor.model.DecryptedAccessToken;
import me.jvt.www.editor.model.DecryptedAccessTokens;
import me.jvt.www.editor.security.TokenDecryptionException;
import me.jvt.www.editor.security.TokenService;
import org.springframework.core.MethodParameter;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class DecryptedAccessTokensArgumentResolver implements HandlerMethodArgumentResolver {

  private final TokenService service;

  public DecryptedAccessTokensArgumentResolver(TokenService service) {
    this.service = service;
  }

  @Override
  public boolean supportsParameter(MethodParameter parameter) {
    return parameter.getParameter().getType() == DecryptedAccessTokens.class;
  }

  @Override
  public Object resolveArgument(
      MethodParameter parameter,
      @Nullable ModelAndViewContainer mavContainer,
      NativeWebRequest webRequest,
      @Nullable WebDataBinderFactory binderFactory)
      throws Exception {
    DecryptedAccessTokens tokens = new DecryptedAccessTokens();
    Cookie[] cookies = ((ServletWebRequest) webRequest).getRequest().getCookies();

    if (null == cookies) {
      return tokens;
    }

    for (Cookie cookie : cookies) {
      try {
        if (cookie.getName().equals("www.jvt.me")) {
          tokens.setProduction(decrypt(cookie));
        } else if (cookie.getName().equals("www.staging.jvt.me")) {
          tokens.setStaging(decrypt(cookie));
        }
      } catch (IllegalArgumentException e) {
        HttpServletResponse response = ((ServletWebRequest) webRequest).getResponse();
        if (null != response) {
          response.addCookie(deleteCookie(cookie));
        }
        throw e;
      }
    }

    return tokens;
  }

  private DecryptedAccessToken decrypt(Cookie cookie) {
    try {
      return new DecryptedAccessToken(service.decrypt(cookie.getValue()));
    } catch (TokenDecryptionException e) {
      throw new IllegalArgumentException(
          String.format("Provided cookie, %s, was invalid", cookie.getName()));
    }
  }

  private Cookie deleteCookie(Cookie cookie) {
    Cookie delete = new Cookie(cookie.getName(), null);
    delete.setHttpOnly(cookie.isHttpOnly());
    delete.setMaxAge(0);
    delete.setSecure(cookie.getSecure());
    return delete;
  }
}
