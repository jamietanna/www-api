package me.jvt.www.editor.security;

public class TokenDecryptionException extends Exception {
  public TokenDecryptionException(String message) {
    super(message);
  }
}
