package me.jvt.www.editor.config;

import java.time.Clock;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.logging.LoggingConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(LoggingConfiguration.class)
public class SpringConfiguration {

  @Bean
  public Clock clock() {
    return Clock.systemUTC();
  }

  @Bean
  public RequestSpecificationFactory requestSpecificationFactory() {
    return new RequestSpecificationFactory();
  }
}
