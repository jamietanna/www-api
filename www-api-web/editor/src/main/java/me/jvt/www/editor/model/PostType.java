package me.jvt.www.editor.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import me.jvt.www.editor.autoconfiguring.PostTypesResponseContainer.MicropubPostType;
import me.jvt.www.editor.autoconfiguring.PropertiesContainer;
import me.jvt.www.editor.model.Property.PropertyBuilder;

public class PostType {
  private String type;
  private String name;

  private String h;
  private List<Property> properties;

  public PostType() {}

  public PostType(
      MicropubPostType micropubPostType, List<PropertiesContainer.Property> supportedProperties) {
    setH(micropubPostType.getH());
    setType(micropubPostType.getType());
    setName(micropubPostType.getName());
    List<Property> properties = new ArrayList<>();
    for (String property : micropubPostType.getProperties()) {
      PropertyBuilder builder = PropertyBuilder.builder();
      Optional<PropertiesContainer.Property> maybeProperty =
          supportedProperties.stream().filter(p -> p.getName().equals(property)).findFirst();
      if (maybeProperty.isPresent()) {
        builder.withDisplayName(maybeProperty.get().getDisplayName());
        builder.withHints(maybeProperty.get().getHints());
        builder.withOptions(maybeProperty.get().getOptions());
        builder.withDefaultValue(maybeProperty.get().getDefaultValue());
      }
      builder
          .withName(property)
          .withIsRequired(micropubPostType.getRequiredProperties().contains(property));
      properties.add(builder.build());
    }
    setProperties(properties);
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getH() {
    return h;
  }

  public void setH(String h) {
    this.h = h;
  }

  public List<Property> getProperties() {
    return properties;
  }

  public void setProperties(List<Property> properties) {
    this.properties = properties;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PostType postType = (PostType) o;
    return Objects.equals(type, postType.type)
        && Objects.equals(name, postType.name)
        && Objects.equals(h, postType.h)
        && Objects.equals(properties, postType.properties);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, name, h, properties);
  }
}
