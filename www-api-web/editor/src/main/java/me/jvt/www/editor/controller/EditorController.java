package me.jvt.www.editor.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.time.temporal.IsoFields;
import java.time.temporal.TemporalAdjusters;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import me.jvt.www.editor.autoconfiguring.AutoconfiguringService;
import me.jvt.www.editor.exception.UnauthorizedException;
import me.jvt.www.editor.model.AccountInformation;
import me.jvt.www.editor.model.CallbackParameters;
import me.jvt.www.editor.model.DecryptedAccessTokens;
import me.jvt.www.editor.model.PostType;
import me.jvt.www.editor.security.CodeVerifierGenerator;
import me.jvt.www.editor.security.TokenService;
import me.jvt.www.indieauthclient.InMemoryIndieAuthService;
import me.jvt.www.indieauthclient.IndieAuthService.TokenEndpointResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class EditorController {

  private static final List<String> SCOPES =
      Arrays.asList("create", "update", "delete", "undelete", "media");
  private static final String CREATE_THIS_WEEK_URL = "/autoconfiguring/article";
  private static final String EDIT_URL = "https://micropublish.net/edit?url=%s";
  private static final String WEEK_NOTES_FORMAT = "https://www.jvt.me/week-notes/%d/%02d/";

  private final Clock clock;
  private final InMemoryIndieAuthService service;
  private final TokenService tokenService;
  private final AutoconfiguringService autoconfiguringService;
  private final CodeVerifierGenerator generator;

  public EditorController(
      Clock clock,
      InMemoryIndieAuthService service,
      TokenService tokenService,
      AutoconfiguringService autoconfiguringService,
      CodeVerifierGenerator generator) {
    this.clock = clock;
    this.service = service;
    this.tokenService = tokenService;
    this.autoconfiguringService = autoconfiguringService;
    this.generator = generator;
  }

  @GetMapping("/")
  public String root(Model model, DecryptedAccessTokens decryptedAccessTokens) {
    model.addAttribute("accountInformation", accountInformation(decryptedAccessTokens));

    model.addAttribute(
        "authorization_staging",
        service.constructAuthorizationUrl(
            generateState("staging"), generator.generate(), "https://www.staging.jvt.me/", SCOPES));
    model.addAttribute(
        "authorization_production",
        service.constructAuthorizationUrl(
            generateState("production"), generator.generate(), "https://www.jvt.me/", SCOPES));

    LocalDate now =
        clock
            .instant()
            .atZone(ZoneId.of("UTC"))
            .toLocalDate()
            .with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
    model.addAttribute("this_week_notes_create", createThisWeekNotesUrl(now));
    model.addAttribute("this_week_notes_update", updateThisWeekNotesUrl(now));
    model.addAttribute("last_week_notes_update", updateLastWeekNotesUrl(now));

    return "index";
  }

  @GetMapping("/autoconfiguring")
  public String autoconfiguring(Model model, DecryptedAccessTokens decryptedAccessTokens) {
    Map<String, AccountInformation> accountInformation = accountInformation(decryptedAccessTokens);
    model.addAttribute("accountInformation", accountInformation);
    Map<String, List<PostType>> postTypes = new HashMap<>();

    if (accountInformation.containsKey("www.jvt.me")) {
      postTypes.put(
          "production",
          autoconfiguringService.retrievePostTypes(
              accountInformation.get("www.jvt.me").getMicropubEndpoint()));
    }

    if (accountInformation.containsKey("www.staging.jvt.me")) {
      postTypes.put(
          "staging",
          autoconfiguringService.retrievePostTypes(
              accountInformation.get("www.staging.jvt.me").getMicropubEndpoint()));
    }

    model.addAttribute("post_types", postTypes);

    return "autoconfiguring-root";
  }

  @GetMapping("/autoconfiguring/{type}")
  public String autoconfiguringType(
      Model model, DecryptedAccessTokens decryptedAccessTokens, @PathVariable String type) {
    Map<String, AccountInformation> accountInformation = accountInformation(decryptedAccessTokens);
    model.addAttribute("accountInformation", accountInformation);
    Map<String, PostType> postType = new HashMap<>();

    if (accountInformation.containsKey("www.jvt.me")) {
      List<PostType> postTypes =
          autoconfiguringService.retrievePostTypes(
              accountInformation.get("www.jvt.me").getMicropubEndpoint());

      postType.put("www.jvt.me", getOrThrow(postTypes, type));
    }

    if (accountInformation.containsKey("www.staging.jvt.me")) {
      List<PostType> postTypes =
          autoconfiguringService.retrievePostTypes(
              accountInformation.get("www.staging.jvt.me").getMicropubEndpoint());

      postType.put("www.staging.jvt.me", getOrThrow(postTypes, type));
    }

    if (postType.isEmpty()) {
      throw new IllegalArgumentException(
          String.format("Provided post type, `%s`, is not supported", type));
    }

    model.addAttribute("post_type", postType);

    return "autoconfiguring-type";
  }

  @GetMapping("/callback")
  public ResponseEntity<Object> callback(
      CallbackParameters callbackParameters, HttpServletResponse response) {
    String code = callbackParameters.getCode();
    String state = callbackParameters.getState();

    // then, get the access token
    TokenEndpointResponse tokenEndpointResponse =
        service.exchangeAuthorizationCode(code, state, null, service.retrieveMeForState(state));
    String me = tokenEndpointResponse.getMe();
    if (!isValidMeUrl(me)) {
      throw new UnauthorizedException("Unexpected me value for this authorization code");
    }

    // set the token
    Cookie tokenCookie =
        new Cookie(getCookieName(me), tokenService.encrypt(tokenEndpointResponse.getAccessToken()));
    tokenCookie.setHttpOnly(true);
    tokenCookie.setSecure(true);
    response.addCookie(tokenCookie);

    return ResponseEntity.status(HttpStatus.FOUND).header(HttpHeaders.LOCATION, "/").build();
  }

  @GetMapping("/custom")
  public String custom(Model model, DecryptedAccessTokens decryptedAccessTokens) {
    model.addAttribute("accountInformation", accountInformation(decryptedAccessTokens));
    return "custom";
  }

  private Map<String, AccountInformation> accountInformation(
      DecryptedAccessTokens decryptedAccessTokens) {
    Map<String, AccountInformation> accountInformation = new HashMap<>();

    if (null != decryptedAccessTokens.getProduction()) {
      AccountInformation production = new AccountInformation();
      production.setAccessToken(decryptedAccessTokens.getProduction().getAccessToken());
      production.setMicropubEndpoint("https://www-api.jvt.me/micropub");
      production.setMicropubMediaEndpoint("https://www-api.jvt.me/micropub/media");
      accountInformation.put("www.jvt.me", production);
    }

    if (null != decryptedAccessTokens.getStaging()) {
      AccountInformation staging = new AccountInformation();
      staging.setAccessToken(decryptedAccessTokens.getStaging().getAccessToken());
      staging.setMicropubEndpoint("https://www-api.staging.jvt.me/micropub");
      staging.setMicropubMediaEndpoint("https://www-api.staging.jvt.me/micropub/media");
      accountInformation.put("www.staging.jvt.me", staging);
    }

    return accountInformation;
  }

  private boolean isValidMeUrl(String me) {
    return me.equals("https://www.jvt.me")
        || me.equals("https://www.jvt.me/")
        || me.equals("https://www.staging.jvt.me")
        || me.equals("https://www.staging.jvt.me/");
  }

  private PostType getOrThrow(List<PostType> postTypes, String type) {
    Optional<PostType> found = postTypes.stream().filter(p -> p.getType().equals(type)).findFirst();

    if (!found.isPresent()) {
      throw new IllegalArgumentException(
          String.format("Provided post type, `%s`, is not supported", type));
    }
    return found.get();
  }

  private String generateState(String state) {
    return String.format("%s-%d", state, clock.millis());
  }

  private String getCookieName(String me) {
    if (me.equals("https://www.jvt.me") || me.equals("https://www.jvt.me/")) {
      return "www.jvt.me";
    } else if (me.equals("https://www.staging.jvt.me")
        || me.equals("https://www.staging.jvt.me/")) {
      return "www.staging.jvt.me";
    } else {
      throw new RuntimeException("Cookie provided was not valid");
    }
  }

  private static String urlEncode(String s) {
    try {
      return URLEncoder.encode(s, StandardCharsets.UTF_8.toString());
    } catch (UnsupportedEncodingException e) {
      throw new IllegalStateException(e);
    }
  }

  private static String createThisWeekNotesUrl(LocalDate now) {
    String title =
        String.format(
            "Week Notes %02d#%02d",
            now.getYear() % 2000, now.get(IsoFields.WEEK_OF_WEEK_BASED_YEAR));
    return CREATE_THIS_WEEK_URL + "?name=" + urlEncode(title);
  }

  private static String updateThisWeekNotesUrl(LocalDate now) {
    return String.format(
        EDIT_URL,
        String.format(
            WEEK_NOTES_FORMAT, now.getYear(), now.get(IsoFields.WEEK_OF_WEEK_BASED_YEAR)));
  }

  private static String updateLastWeekNotesUrl(LocalDate now) {
    LocalDate lastWeek = now.minus(1, ChronoUnit.WEEKS);
    return String.format(
        EDIT_URL,
        String.format(
            WEEK_NOTES_FORMAT,
            lastWeek.getYear(),
            lastWeek.get(IsoFields.WEEK_OF_WEEK_BASED_YEAR)));
  }
}
