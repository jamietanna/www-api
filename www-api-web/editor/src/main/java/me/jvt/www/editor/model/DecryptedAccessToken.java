package me.jvt.www.editor.model;

public class DecryptedAccessToken {

  private final String accessToken;

  public DecryptedAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getAccessToken() {
    return accessToken;
  }
}
