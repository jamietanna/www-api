package me.jvt.www.editor.autoconfiguring;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class PropertiesContainer {

  private List<Property> properties;

  public List<Property> getProperties() {
    return properties;
  }

  public void setProperties(List<Property> properties) {
    this.properties = properties;
  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Property {
    private String name;

    @JsonProperty("display-name")
    private String displayName;

    @JsonProperty("default")
    private String defaultValue;

    private List<Hint> hints;

    private List<String> options;

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getDisplayName() {
      return displayName;
    }

    public void setDisplayName(String displayName) {
      this.displayName = displayName;
    }

    public String getDefaultValue() {
      return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
      this.defaultValue = defaultValue;
    }

    public List<Hint> getHints() {
      return hints;
    }

    public void setHints(List<Hint> hints) {
      this.hints = hints;
    }

    public List<String> getOptions() {
      return options;
    }

    public void setOptions(List<String> options) {
      this.options = options;
    }
  }

  public enum Hint {
    defaultType,
    date,
    multiline,
    multivalue,
    url;
  }
}
