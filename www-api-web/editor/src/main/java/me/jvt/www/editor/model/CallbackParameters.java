package me.jvt.www.editor.model;

public class CallbackParameters {

  private final String code;
  private final String state;

  public CallbackParameters(String code, String state) {
    this.code = code;
    this.state = state;
  }

  public String getCode() {
    return code;
  }

  public String getState() {
    return state;
  }
}
