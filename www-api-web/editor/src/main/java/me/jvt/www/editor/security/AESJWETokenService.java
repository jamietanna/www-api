package me.jvt.www.editor.security;

import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jose.crypto.DirectEncrypter;
import java.text.ParseException;
import javax.crypto.SecretKey;

public class AESJWETokenService implements TokenService {

  private final SecretKey secretKey;

  public AESJWETokenService(SecretKey secretKey) {
    this.secretKey = secretKey;
  }

  @Override
  public String encrypt(String accessToken) {
    JWEHeader header = new JWEHeader(JWEAlgorithm.DIR, EncryptionMethod.A256GCM);
    Payload payload = new Payload(accessToken);
    JWEObject jweObject = new JWEObject(header, payload);
    try {
      jweObject.encrypt(new DirectEncrypter(secretKey));
    } catch (JOSEException e) {
      throw new RuntimeException(e);
    }

    return jweObject.serialize();
  }

  @Override
  public String decrypt(String encryptedAccessToken) throws TokenDecryptionException {
    JWEObject jweObject;
    try {
      jweObject = JWEObject.parse(encryptedAccessToken);
      jweObject.decrypt(new DirectDecrypter(secretKey));
    } catch (ParseException | JOSEException e) {
      throw new TokenDecryptionException("Token provided was not valid");
    }

    return jweObject.getPayload().toString();
  }
}
