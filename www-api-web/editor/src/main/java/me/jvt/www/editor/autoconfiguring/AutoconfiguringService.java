package me.jvt.www.editor.autoconfiguring;

import io.restassured.response.Response;
import java.util.ArrayList;
import java.util.List;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.editor.model.PostType;
import org.springframework.stereotype.Service;

@Service
public class AutoconfiguringService {

  private final RequestSpecificationFactory requestSpecificationFactory;

  public AutoconfiguringService(RequestSpecificationFactory requestSpecificationFactory) {
    this.requestSpecificationFactory = requestSpecificationFactory;
  }

  public List<PostType> retrievePostTypes(String micropubBaseUrl) {
    Response postTypes =
        requestSpecificationFactory
            .newRequestSpecification()
            .queryParam("q", "post-types")
            .get(micropubBaseUrl);
    PostTypesResponseContainer postTypesContainer = postTypes.as(PostTypesResponseContainer.class);
    Response properties =
        requestSpecificationFactory
            .newRequestSpecification()
            .queryParam("q", "properties")
            .get(micropubBaseUrl);
    PropertiesContainer propertiesContainer = properties.as(PropertiesContainer.class);
    return convert(postTypesContainer, propertiesContainer);
  }

  private List<PostType> convert(
      PostTypesResponseContainer postTypesContainer, PropertiesContainer properties) {
    List<PostType> postTypes = new ArrayList<>();
    for (PostTypesResponseContainer.MicropubPostType micropubPostType :
        postTypesContainer.getPostTypes()) {
      postTypes.add(new PostType(micropubPostType, properties.getProperties()));
    }
    return postTypes;
  }
}
