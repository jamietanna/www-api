package me.jvt.www.editor.security;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

@Component
public class CodeVerifierGenerator {
  public String generate() {
    return RandomStringUtils.randomAlphanumeric(64);
  }
}
