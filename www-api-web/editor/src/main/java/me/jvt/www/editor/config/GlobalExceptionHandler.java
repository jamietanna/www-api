package me.jvt.www.editor.config;

import me.jvt.www.editor.exception.UnauthorizedException;
import me.jvt.www.editor.model.ErrorResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {
  @ExceptionHandler(IllegalArgumentException.class)
  protected ResponseEntity<Object> handleInvalidRequestException(
      IllegalArgumentException ex, WebRequest request) {
    return ResponseEntity.badRequest()
        .body(new ErrorResponseDto("invalid_request", ex.getMessage()));
  }

  @ExceptionHandler(UnauthorizedException.class)
  protected ResponseEntity<Object> handleUnauthorizedException(
      UnauthorizedException ex, WebRequest request) {
    return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
        .body(new ErrorResponseDto("unauthorized", ex.getMessage()));
  }
}
