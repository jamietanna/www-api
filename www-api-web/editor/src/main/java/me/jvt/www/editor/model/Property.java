package me.jvt.www.editor.model;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import me.jvt.www.editor.autoconfiguring.PropertiesContainer.Hint;

public class Property {

  private final String name;

  private final String displayName;

  private final boolean isRequired;

  private final Hint hint;
  private final boolean isMultiValue;
  private final List<String> options;
  private final String defaultValue;

  private Property(PropertyBuilder builder) {
    this.name = builder.name;
    if (null != builder.displayName) {
      this.displayName = builder.displayName;
    } else {
      this.displayName = builder.name;
    }
    this.isRequired = builder.isRequired;

    if (null == builder.hints) {
      builder.hints = Collections.emptyList();
    }
    this.isMultiValue = builder.hints.contains(Hint.multivalue);
    this.hint =
        builder.hints.stream()
            .filter(h -> !h.equals(Hint.multivalue))
            .findFirst()
            .orElse(Hint.defaultType);

    if (null == builder.options) {
      builder.options = Collections.emptyList();
    }
    this.options = builder.options;

    this.defaultValue = builder.defaultValue;
  }

  public String getName() {
    return name;
  }

  public String getDisplayName() {
    return displayName;
  }

  public boolean isRequired() {
    return isRequired;
  }

  public boolean isMultivalue() {
    return isMultiValue;
  }

  public List<String> getOptions() {
    return options;
  }

  public String getDefaultValue() {
    return defaultValue;
  }

  public Hint getHint() {
    return hint;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Property property = (Property) o;
    return isRequired == property.isRequired
        && isMultiValue == property.isMultiValue
        && Objects.equals(name, property.name)
        && Objects.equals(displayName, property.displayName)
        && hint == property.hint
        && Objects.equals(options, property.options)
        && Objects.equals(defaultValue, property.defaultValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, displayName, isRequired, hint, isMultiValue, options, defaultValue);
  }

  public static final class PropertyBuilder {

    private String name;
    private String displayName;
    private boolean isRequired = false;
    private List<Hint> hints = Collections.emptyList();
    private List<String> options = Collections.emptyList();
    private String defaultValue;

    private PropertyBuilder() {}

    public static PropertyBuilder builder() {
      return new PropertyBuilder();
    }

    public PropertyBuilder withName(String name) {
      this.name = name;
      return this;
    }

    public PropertyBuilder withDisplayName(String displayName) {
      this.displayName = displayName;
      return this;
    }

    public PropertyBuilder withIsRequired(boolean isRequired) {
      this.isRequired = isRequired;
      return this;
    }

    public PropertyBuilder withHints(List<Hint> hints) {
      this.hints = hints;
      return this;
    }

    public PropertyBuilder withOptions(List<String> options) {
      this.options = options;
      return this;
    }

    public PropertyBuilder withDefaultValue(String defaultValue) {
      this.defaultValue = defaultValue;
      return this;
    }

    public Property build() {
      return new Property(this);
    }
  }
}
