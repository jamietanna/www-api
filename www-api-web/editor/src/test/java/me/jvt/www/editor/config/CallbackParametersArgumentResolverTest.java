package me.jvt.www.editor.config;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import me.jvt.www.editor.model.CallbackParameters;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;

@ExtendWith(MockitoExtension.class)
class CallbackParametersArgumentResolverTest {

  @Mock private MethodParameter parameter;
  @Mock private ModelAndViewContainer mavContainer;
  @Mock private ServletWebRequest webRequest;
  @Mock private HttpServletRequest httpServletRequest;
  @Mock private WebDataBinderFactory binderFactory;

  @InjectMocks private CallbackParametersArgumentResolver sut;

  private CallbackParameters actual;

  @BeforeEach
  void setup() {
    when(webRequest.getRequest()).thenReturn(httpServletRequest);
  }

  @Test
  void itThrowsIllegalArgumentExceptionWhenNoCode() {
    assertThatThrownBy(
            () -> sut.resolveArgument(parameter, mavContainer, webRequest, binderFactory))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("code parameter was not provided");
  }

  @Test
  void itThrowsIllegalArgumentExceptionWhenNoState() {
    setCode("valid");

    assertThatThrownBy(
            () -> sut.resolveArgument(parameter, mavContainer, webRequest, binderFactory))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("state parameter was not provided");
  }

  @Test
  void itThrowsIllegalArgumentExceptionWhenCodeIsEmpty() {
    setCode("");

    assertThatThrownBy(
            () -> sut.resolveArgument(parameter, mavContainer, webRequest, binderFactory))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("code parameter was empty");
  }

  @Test
  void itThrowsIllegalArgumentExceptionWhenStateIsEmpty() {
    setCode("valid");
    setState("");

    assertThatThrownBy(
            () -> sut.resolveArgument(parameter, mavContainer, webRequest, binderFactory))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("state parameter was empty");
  }

  @Test
  void itReturnsCallbackParametersWhenParsed() throws Exception {
    setCode("the-code");
    setState("random-state");

    actual =
        (CallbackParameters)
            sut.resolveArgument(parameter, mavContainer, webRequest, binderFactory);

    assertThat(actual).isNotNull();
    assertThat(actual.getCode()).isEqualTo("the-code");
    assertThat(actual.getState()).isEqualTo("random-state");
  }

  private void setQueryParam(String param, String value) {
    given(httpServletRequest.getParameter(param)).willReturn(value);
  }

  private void setCode(String code) {
    setQueryParam("code", code);
  }

  private void setState(String state) {
    setQueryParam("state", state);
  }
}
