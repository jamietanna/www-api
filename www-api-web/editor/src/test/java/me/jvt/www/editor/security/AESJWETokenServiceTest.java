package me.jvt.www.editor.security;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jose.crypto.DirectEncrypter;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AESJWETokenServiceTest {
  private static SecretKey secretKey;

  static SecretKey generateSecretKey() throws NoSuchAlgorithmException {
    KeyGenerator keyGen = KeyGenerator.getInstance("AES");
    keyGen.init(256);
    return keyGen.generateKey();
  }

  @BeforeAll
  static void setupClass() throws NoSuchAlgorithmException {
    secretKey = generateSecretKey();
  }

  private AESJWETokenService service;

  @BeforeEach
  void setup() {
    service = new AESJWETokenService(secretKey);
  }

  @Test
  void itEncrypts() throws ParseException, JOSEException {
    String accessToken = "j.w.t";

    String actual = service.encrypt(accessToken);

    JWEObject jwe = JWEObject.parse(actual);
    jwe.decrypt(new DirectDecrypter(secretKey));

    assertThat(jwe.getPayload().toString()).isEqualTo(accessToken);
  }

  @Test
  void itDecrypts() throws TokenDecryptionException, JOSEException {
    String actual = service.decrypt(encrypt("j.w.t"));
    assertThat(actual).isEqualTo("j.w.t");
  }

  @Test
  void decryptThrowsTokenDecryptionExceptionWhenNotEncrypted() {
    assertThatThrownBy(() -> service.decrypt("j.w.t"))
        .isInstanceOf(TokenDecryptionException.class)
        .hasMessage("Token provided was not valid");
  }

  @Test
  void decryptThrowsTokenDecryptionExceptionWhenNotEncryptedWithCorrectKey() {
    assertThatThrownBy(() -> service.decrypt(encrypt("j.w.t", generateSecretKey())))
        .isInstanceOf(TokenDecryptionException.class)
        .hasMessage("Token provided was not valid");
  }

  @Test
  void decryptThrowsTokenDecryptionExceptionWhenEncryptedWithCorrectKeyButInvalidPrefix() {
    assertThatThrownBy(() -> service.decrypt(" not valid " + encrypt("j.w.t")))
        .isInstanceOf(TokenDecryptionException.class)
        .hasMessage("Token provided was not valid");
  }

  @Test
  void decryptThrowsTokenDecryptionExceptionWhenEncryptedWithCorrectKeyButInvalidSuffix() {
    assertThatThrownBy(() -> service.decrypt(encrypt("j.w.t") + " not valid "))
        .isInstanceOf(TokenDecryptionException.class)
        .hasMessage("Token provided was not valid");
  }

  private String encrypt(String accessToken) throws JOSEException {
    return encrypt(accessToken, secretKey);
  }

  private String encrypt(String accessToken, SecretKey key) throws JOSEException {
    JWEHeader header = new JWEHeader(JWEAlgorithm.DIR, EncryptionMethod.A256GCM);
    Payload payload = new Payload(accessToken);
    JWEObject jweObject = new JWEObject(header, payload);
    jweObject.encrypt(new DirectEncrypter(key));

    return jweObject.serialize();
  }
}
