package me.jvt.www.editor.config;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import me.jvt.www.editor.model.DecryptedAccessTokens;
import me.jvt.www.editor.security.TokenDecryptionException;
import me.jvt.www.editor.security.TokenService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;

@ExtendWith(MockitoExtension.class)
class DecryptedAccessTokensArgumentResolverTest {

  @Captor private ArgumentCaptor<Cookie> cookieCaptor;

  @Mock private TokenService service;

  @Mock private MethodParameter parameter;
  @Mock private ModelAndViewContainer mavContainer;
  @Mock private ServletWebRequest webRequest;
  @Mock private HttpServletRequest httpServletRequest;
  @Mock private HttpServletResponse httpServletResponse;
  @Mock private WebDataBinderFactory binderFactory;

  @InjectMocks private DecryptedAccessTokensArgumentResolver sut;

  private DecryptedAccessTokens actual;

  @BeforeEach
  void setup() {
    when(webRequest.getRequest()).thenReturn(httpServletRequest);
  }

  @Test
  void itReturnsEmptyObjectIfNoTokens() throws Exception {
    actual =
        (DecryptedAccessTokens)
            sut.resolveArgument(parameter, mavContainer, webRequest, binderFactory);

    assertThat(actual).isNotNull();
    assertThat(actual.getProduction()).isNull();
    assertThat(actual.getStaging()).isNull();
  }

  @Test
  void itParsesValidProductionCookieIfPresent() throws Exception {
    setupCookie("cookie-value", true);

    when(service.decrypt(any())).thenReturn("decrypted");

    actual =
        (DecryptedAccessTokens)
            sut.resolveArgument(parameter, mavContainer, webRequest, binderFactory);

    assertThat(actual).isNotNull();
    assertThat(actual.getProduction().getAccessToken()).isEqualTo("decrypted");
    assertThat(actual.getStaging()).isNull();
  }

  @Test
  void itParsesValidStagingCookieIfPresent() throws Exception {
    setupCookie("cookie-value", false);

    when(service.decrypt(any())).thenReturn("decrypted");

    actual =
        (DecryptedAccessTokens)
            sut.resolveArgument(parameter, mavContainer, webRequest, binderFactory);

    assertThat(actual).isNotNull();
    assertThat(actual.getProduction()).isNull();
    assertThat(actual.getStaging().getAccessToken()).isEqualTo("decrypted");
  }

  @Test
  void invalidProductionCookieThrowsIllegalArgumentException() throws Exception {
    setupInvalidCookie(true);

    assertThatThrownBy(
            () -> sut.resolveArgument(parameter, mavContainer, webRequest, binderFactory))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Provided cookie, www.jvt.me, was invalid");
  }

  @Test
  void invalidStagingCookieThrowsIllegalArgumentException() throws Exception {
    setupInvalidCookie(false);

    assertThatThrownBy(
            () -> sut.resolveArgument(parameter, mavContainer, webRequest, binderFactory))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Provided cookie, www.staging.jvt.me, was invalid");
  }

  @ParameterizedTest
  @ValueSource(booleans = {true, false})
  void invalidCookieSetsCookieAsDeleted(boolean isProd) throws Exception {
    setupInvalidCookie(isProd);

    assertThatThrownBy(
            () -> sut.resolveArgument(parameter, mavContainer, webRequest, binderFactory))
        .isInstanceOf(IllegalArgumentException.class);

    verify(httpServletResponse).addCookie(cookieCaptor.capture());
    Cookie cookie = getCookie(cookieCaptor.getAllValues(), cookieName(isProd));

    assertThat(cookie.getMaxAge()).isEqualTo(0);
  }

  @ParameterizedTest
  @ValueSource(booleans = {true, false})
  void invalidCookieReplaysCookieValuesOnResponse(boolean isProd) throws Exception {
    setupInvalidCookie(isProd);

    assertThatThrownBy(
            () -> sut.resolveArgument(parameter, mavContainer, webRequest, binderFactory))
        .isInstanceOf(IllegalArgumentException.class);

    verify(httpServletResponse).addCookie(cookieCaptor.capture());
    Cookie cookie = getCookie(cookieCaptor.getAllValues(), cookieName(isProd));

    assertThat(cookie.isHttpOnly()).isTrue();
    assertThat(cookie.isHttpOnly()).isTrue();
  }

  @Test
  void invalidCookieHandlesNullResponse() throws Exception {
    setupCookie("invalid", false);
    when(service.decrypt(any())).thenThrow(new TokenDecryptionException("Wibble"));
    when(webRequest.getResponse()).thenReturn(null);

    assertThatThrownBy(
            () -> sut.resolveArgument(parameter, mavContainer, webRequest, binderFactory))
        .isInstanceOf(IllegalArgumentException.class);
  }

  private Cookie getCookie(List<Cookie> cookies, String cookieName) {
    Optional<Cookie> cookieOptional =
        cookieCaptor.getAllValues().stream()
            .filter(c -> c.getName().equals(cookieName))
            .findFirst();
    if (!cookieOptional.isPresent()) {
      throw new RuntimeException("Could not find cookie");
    }

    return cookieOptional.get();
  }

  private void setupCookie(String value, boolean isProd) {
    Cookie[] cookies = new Cookie[1];
    String cookieName = cookieName(isProd);
    Cookie cookie = new Cookie(cookieName, value);
    cookie.setHttpOnly(true);
    cookie.setSecure(true);
    cookies[0] = cookie;
    when(httpServletRequest.getCookies()).thenReturn(cookies);
  }

  private void setupInvalidCookie(boolean isProd) throws TokenDecryptionException {
    setupCookie("invalid", isProd);
    when(service.decrypt(any())).thenThrow(new TokenDecryptionException("Wibble"));
    when(webRequest.getResponse()).thenReturn(httpServletResponse);
  }

  private String cookieName(boolean isProd) {
    if (isProd) {
      return "www.jvt.me";
    } else {
      return "www.staging.jvt.me";
    }
  }
}
