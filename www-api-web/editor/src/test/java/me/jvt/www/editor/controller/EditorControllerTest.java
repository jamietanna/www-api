package me.jvt.www.editor.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.startsWith;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Stream;
import javax.servlet.http.Cookie;
import me.jvt.www.editor.autoconfiguring.AutoconfiguringService;
import me.jvt.www.editor.model.AccountInformation;
import me.jvt.www.editor.model.PostType;
import me.jvt.www.editor.security.CodeVerifierGenerator;
import me.jvt.www.editor.security.TokenDecryptionException;
import me.jvt.www.editor.security.TokenService;
import me.jvt.www.indieauthclient.InMemoryIndieAuthService;
import me.jvt.www.indieauthclient.IndieAuthService.TokenEndpointResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@WebMvcTest(EditorController.class)
@Import(EditorControllerTest.TestConfig.class)
class EditorControllerTest {

  @TestConfiguration
  public static class TestConfig {
    @Bean
    public Clock clock() {
      return Clock.fixed(Instant.parse("2021-01-04T00:00:00Z"), ZoneId.of("UTC"));
    }
  }

  private static Stream<Arguments> meToCookieMapping() {
    return Stream.of(
        Arguments.of("https://www.jvt.me/", "www.jvt.me"),
        Arguments.of("https://www.staging.jvt.me/", "www.staging.jvt.me"));
  }

  @Captor private ArgumentCaptor<Map<String, AccountInformation>> accountInformationArgumentCaptor;
  @MockBean private AutoconfiguringService autoconfiguringService;
  @MockBean private InMemoryIndieAuthService service;
  @MockBean private TokenService tokenService;
  @MockBean private CodeVerifierGenerator generator;

  @Mock private TokenEndpointResponse tokenEndpointResponse;
  @Autowired private Clock clock;

  @Autowired MockMvc mockMvc;

  @BeforeEach
  void setup() {
    when(generator.generate()).thenReturn("first", "second");
    when(service.exchangeAuthorizationCode(any(), any(), any(), any()))
        .thenReturn(tokenEndpointResponse);
    when(tokenService.encrypt(any())).thenReturn("encrypted-token");
  }

  @Test
  void callbackReturnsBadRequestIfMissingCode() throws Exception {
    this.mockMvc
        .perform(get("/callback").queryParam("state", "abc"))
        .andExpect(MockMvcResultMatchers.status().isBadRequest())
        .andExpect(jsonPath("$.error").value("invalid_request"))
        .andExpect(jsonPath("$.error_description").value("code parameter was not provided"));
  }

  @Test
  void callbackReturnsBadRequestIfMissingState() throws Exception {
    this.mockMvc
        .perform(get("/callback").queryParam("code", "c.o.d.e"))
        .andExpect(MockMvcResultMatchers.status().isBadRequest())
        .andExpect(jsonPath("$.error").value("invalid_request"))
        .andExpect(jsonPath("$.error_description").value("state parameter was not provided"));
  }

  @Test
  void callbackReturnsBadRequestIfStateIsEmpty() throws Exception {
    this.mockMvc
        .perform(get("/callback").queryParam("code", "c.o.d.e").queryParam("state", ""))
        .andExpect(MockMvcResultMatchers.status().isBadRequest())
        .andExpect(jsonPath("$.error").value("invalid_request"))
        .andExpect(jsonPath("$.error_description").value("state parameter was empty"));
  }

  @Test
  void callbackDelegatesToIndieAuthServiceToExchangeAuthorizationCode() throws Exception {
    when(service.retrieveMeForState(any())).thenReturn("https://me");
    when(service.exchangeAuthorizationCode(any(), any(), any(), any()))
        .thenReturn(tokenEndpointResponse);
    when(tokenEndpointResponse.getMe()).thenReturn("https://me");

    this.mockMvc.perform(
        get("/callback").queryParam("code", "c.o.d.e").queryParam("state", "state"));

    verify(service).exchangeAuthorizationCode("c.o.d.e", "state", null, "https://me");
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        "https://www.jvt.me",
        "https://www.jvt.me/",
        "https://www.staging.jvt.me",
        "https://www.staging.jvt.me/"
      })
  void callbackDoesNotRejectMeIfProdOrStagingIdentity(String me) throws Exception {
    when(tokenEndpointResponse.getMe()).thenReturn(me);
    when(service.retrieveMeForState(any())).thenReturn(me);
    when(service.exchangeAuthorizationCode(any(), any(), any(), any()))
        .thenReturn(tokenEndpointResponse);

    this.mockMvc
        .perform(get("/callback").queryParam("code", "c.o.d.e").queryParam("state", "state"))
        .andExpect(MockMvcResultMatchers.redirectedUrl("/"));
    verify(service).exchangeAuthorizationCode("c.o.d.e", "state", null, me);
  }

  @Test
  void callbackRejectsMeIfNotMyProdOrStagingIdentity() throws Exception {
    when(tokenEndpointResponse.getMe()).thenReturn("https://www.jvt.me/profile/");

    this.mockMvc
        .perform(get("/callback").queryParam("code", "c.o.d.e").queryParam("state", "state"))
        .andExpect(MockMvcResultMatchers.status().isUnauthorized())
        .andExpect(jsonPath("$.error").value("unauthorized"))
        .andExpect(
            jsonPath("$.error_description")
                .value("Unexpected me value for this authorization code"));
  }

  @ParameterizedTest
  @MethodSource("meToCookieMapping")
  void callbackSetsCookieWithEncryptedAccessToken(String me, String cookieName) throws Exception {
    when(tokenEndpointResponse.getAccessToken()).thenReturn("https://www.jvt.me");
    when(tokenEndpointResponse.getAccessToken()).thenReturn("token");
    when(tokenEndpointResponse.getMe()).thenReturn(me);

    this.mockMvc
        .perform(
            get("/callback")
                .queryParam("code", "c.o.d.e")
                .queryParam("me", me)
                .queryParam("state", "state"))
        .andExpect(MockMvcResultMatchers.redirectedUrl("/"))
        .andExpect(MockMvcResultMatchers.cookie().value(cookieName, "encrypted-token"))
        .andExpect(MockMvcResultMatchers.cookie().httpOnly(cookieName, true))
        .andExpect(MockMvcResultMatchers.cookie().secure(cookieName, true));
  }

  @Nested
  class RootTest extends EndpointTest {

    @Override
    String getEndpoint() {
      return "/";
    }

    @Override
    String getTemplate() {
      return "index";
    }

    @Test
    void addsStagingAuthorizationEndpoint() throws Exception {
      when(service.constructAuthorizationUrl(startsWith("staging"), any(), any(), any()))
          .thenReturn("https://authz/staging");

      mockMvc
          .perform(get(getEndpoint()))
          .andExpect(model().attribute("authorization_staging", "https://authz/staging"));

      verify(service)
          .constructAuthorizationUrl(
              eq("staging-" + clock.millis()),
              eq("first"),
              eq("https://www.staging.jvt.me/"),
              eq(Arrays.asList("create", "update", "delete", "undelete", "media")));
    }

    @Test
    void addsProductionAuthorizationEndpoint() throws Exception {
      when(service.constructAuthorizationUrl(startsWith("production"), any(), any(), any()))
          .thenReturn("https://authz/prod");

      mockMvc
          .perform(get(getEndpoint()))
          .andExpect(model().attribute("authorization_production", "https://authz/prod"));

      verify(service)
          .constructAuthorizationUrl(
              eq("production-" + clock.millis()),
              eq("second"),
              eq("https://www.jvt.me/"),
              eq(Arrays.asList("create", "update", "delete", "undelete", "media")));
    }

    @Test
    void addsLinkToCreateThisWeeksNotes() throws Exception {
      String name = "Week Notes 21#01";
      String encoded;
      try {
        encoded = URLEncoder.encode(name, StandardCharsets.UTF_8.toString());
      } catch (UnsupportedEncodingException e) {
        throw new IllegalStateException(e);
      }

      mockMvc
          .perform(get(getEndpoint()))
          .andExpect(
              model()
                  .attribute("this_week_notes_create", "/autoconfiguring/article?name=" + encoded));
    }

    @Test
    void addsLinkToCurrentWeeksNotes() throws Exception {
      mockMvc
          .perform(get(getEndpoint()))
          .andExpect(
              model()
                  .attribute(
                      "this_week_notes_update",
                      "https://micropublish.net/edit?url=https://www.jvt.me/week-notes/2021/01/"));
    }

    @Test
    void addsLinkToLastWeeksNotes() throws Exception {
      mockMvc
          .perform(get(getEndpoint()))
          .andExpect(
              model()
                  .attribute(
                      "last_week_notes_update",
                      "https://micropublish.net/edit?url=https://www.jvt.me/week-notes/2020/53/"));
    }
  }

  @Nested
  class CustomTest extends EndpointTest {

    @Override
    String getEndpoint() {
      return "/custom";
    }

    @Override
    String getTemplate() {
      return "custom";
    }
  }

  @Nested
  class AutoconfiguringTest extends EndpointTest {

    @BeforeEach
    void setup() {
      when(autoconfiguringService.retrievePostTypes(any())).thenReturn(Collections.emptyList());
    }

    @Override
    String getEndpoint() {
      return "/autoconfiguring";
    }

    @Override
    String getTemplate() {
      return "autoconfiguring-root";
    }

    @Test
    void delegatesToServiceWithStagingEndpointWhenValidCookie() throws Exception {
      withStagingCookie();

      verify(autoconfiguringService).retrievePostTypes("https://www-api.staging.jvt.me/micropub");
    }

    @Test
    void delegatesToServiceWithProductionEndpointWhenValidCookie() throws Exception {
      withProductionCookie();

      verify(autoconfiguringService).retrievePostTypes("https://www-api.jvt.me/micropub");
    }

    @Test
    void addsPostTypesForStaging() throws Exception {
      PostType postType = new PostType();
      postType.setType("example");
      when(autoconfiguringService.retrievePostTypes(any()))
          .thenReturn(Collections.singletonList(postType));

      withStagingCookie()
          .andExpect(
              model()
                  .attribute(
                      "post_types",
                      Collections.singletonMap("staging", Collections.singletonList(postType))));
    }

    @Test
    void addsPostTypesForProduction() throws Exception {
      PostType postType = new PostType();
      postType.setType("example");
      when(autoconfiguringService.retrievePostTypes(any()))
          .thenReturn(Collections.singletonList(postType));

      withProductionCookie()
          .andExpect(
              model()
                  .attribute(
                      "post_types",
                      Collections.singletonMap("production", Collections.singletonList(postType))));
    }
  }

  @Nested
  class AutoconfiguringNoteTest extends EndpointTest {

    private final PostType postType = new PostType();

    @Override
    String getEndpoint() {
      return "/autoconfiguring/note";
    }

    @Override
    String getTemplate() {
      return "autoconfiguring-type";
    }

    @BeforeEach
    void setup() {
      postType.setType("note");
      when(autoconfiguringService.retrievePostTypes(any()))
          .thenReturn(Collections.singletonList(postType));
    }

    @Override
    void renders() throws Exception {
      withStagingCookie().andExpect(status().isOk()).andExpect(view().name(getTemplate()));
    }

    @Test
    void delegatesToServiceWithStagingEndpointWhenValidCookie() throws Exception {
      withStagingCookie();

      verify(autoconfiguringService).retrievePostTypes("https://www-api.staging.jvt.me/micropub");
    }

    @Test
    void delegatesToServiceWithProductionEndpointWhenValidCookie() throws Exception {
      withProductionCookie();

      verify(autoconfiguringService).retrievePostTypes("https://www-api.jvt.me/micropub");
    }

    @Test
    void addsPostTypeIfMatchesForStaging() throws Exception {
      withStagingCookie()
          .andExpect(
              model()
                  .attribute(
                      "post_type", Collections.singletonMap("www.staging.jvt.me", postType)));
    }

    @Test
    void addsPostTypeIfMatchesForProduction() throws Exception {
      withProductionCookie()
          .andExpect(
              model().attribute("post_type", Collections.singletonMap("www.jvt.me", postType)));
    }

    @Test
    void returnsBadRequestWhenTypeNotFound() throws Exception {
      postType.setType("not-note");

      withProductionCookie()
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(jsonPath("$.error").value("invalid_request"))
          .andExpect(
              jsonPath("$.error_description")
                  .value("Provided post type, `note`, is not supported"));
    }

    @Test
    void returnsBadRequestWhenTypeNotFoundAndNoMapping() throws Exception {
      postType.setType("not-note");

      mockMvc
          .perform(get(getEndpoint()))
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(jsonPath("$.error").value("invalid_request"))
          .andExpect(
              jsonPath("$.error_description")
                  .value("Provided post type, `note`, is not supported"));
    }
  }

  abstract class EndpointTest {
    abstract String getEndpoint();

    abstract String getTemplate();

    @AfterEach
    void setUp() {
      reset(autoconfiguringService);
      reset(service);
      reset(tokenService);
    }

    @Test
    void renders() throws Exception {
      mockMvc
          .perform(get(getEndpoint()))
          .andExpect(status().isOk())
          .andExpect(view().name(getTemplate()));
    }

    @Test
    void addsStagingAccountInformationIfValidCookie() throws Exception {
      withStagingCookie()
          .andExpect(
              model()
                  .attribute(
                      "accountInformation",
                      Collections.singletonMap("www.staging.jvt.me", staging())));
    }

    @Test
    void addsProductionAccountInformationIfValidCookie() throws Exception {
      withProductionCookie()
          .andExpect(
              model()
                  .attribute(
                      "accountInformation", Collections.singletonMap("www.jvt.me", production())));
    }

    @ParameterizedTest
    @ValueSource(strings = {"www.jvt.me", "www.staging.jvt.me"})
    void returns400WhenInvalidCookies(String cookieName) throws Exception {
      withInvalidCookie(cookieName)
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(jsonPath("$.error").value("invalid_request"))
          .andExpect(
              jsonPath("$.error_description")
                  .value(String.format("Provided cookie, %s, was invalid", cookieName)));
    }

    @ParameterizedTest
    @ValueSource(strings = {"www.jvt.me", "www.staging.jvt.me"})
    void deletesInvalidCookie(String cookieName) throws Exception {
      withInvalidCookie(cookieName)
          .andExpect(
              MockMvcResultMatchers.cookie().value(cookieName, org.hamcrest.Matchers.nullValue()))
          .andExpect(MockMvcResultMatchers.cookie().maxAge(cookieName, 0));
    }

    protected ResultActions withStagingCookie() throws Exception {
      when(tokenService.decrypt("staging-cookie")).thenReturn("staging.token");

      return mockMvc.perform(
          get(getEndpoint()).cookie(new Cookie("www.staging.jvt.me", "staging-cookie")));
    }

    protected ResultActions withProductionCookie() throws Exception {
      when(tokenService.decrypt("production-cookie")).thenReturn("production.token");

      return mockMvc.perform(
          get(getEndpoint()).cookie(new Cookie("www.jvt.me", "production-cookie")));
    }

    protected ResultActions withInvalidCookie(String cookieName) throws Exception {
      when(tokenService.decrypt(any())).thenThrow(new TokenDecryptionException("Bad token"));

      return mockMvc.perform(get(getEndpoint()).cookie(new Cookie(cookieName, "invalid")));
    }
  }

  private AccountInformation staging() {
    AccountInformation staging = new AccountInformation();
    staging.setAccessToken("staging.token");
    staging.setMicropubEndpoint("https://www-api.staging.jvt.me/micropub");
    staging.setMicropubMediaEndpoint("https://www-api.staging.jvt.me/micropub/media");

    return staging;
  }

  private AccountInformation production() {
    AccountInformation production = new AccountInformation();
    production.setAccessToken("production.token");
    production.setMicropubEndpoint("https://www-api.jvt.me/micropub");
    production.setMicropubMediaEndpoint("https://www-api.jvt.me/micropub/media");
    return production;
  }
}
