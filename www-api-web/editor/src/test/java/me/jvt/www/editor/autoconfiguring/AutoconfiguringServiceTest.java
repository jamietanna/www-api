package me.jvt.www.editor.autoconfiguring;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import me.jvt.www.api.core.RequestSpecificationFactory;
import me.jvt.www.editor.autoconfiguring.PostTypesResponseContainer.MicropubPostType;
import me.jvt.www.editor.model.PostType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AutoconfiguringServiceTest {

  @Mock private RequestSpecificationFactory factory;

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification requestSpecification0;

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification requestSpecification1;

  @Mock private Response response0;
  @Mock private Response response1;
  private final PostTypesResponseContainer container = new PostTypesResponseContainer();

  @InjectMocks private AutoconfiguringService service;

  private List<PostType> actual;

  @BeforeEach
  void setup() {
    when(factory.newRequestSpecification())
        .thenReturn(requestSpecification0, requestSpecification1);
    setupPostTypes();
    setupProperties();

    actual = service.retrievePostTypes("https://micropub/path");
  }

  @Test
  void itAddsQueryParamForPostTypes() {
    verify(requestSpecification0).queryParam("q", "post-types");
  }

  @Test
  void itPerformsGETForPostTypes() {
    verify(requestSpecification0).get("https://micropub/path");
  }

  @Test
  void itAddsQueryParamForProperties() {
    verify(requestSpecification1).queryParam("q", "properties");
  }

  @Test
  void itPerformsGETForProperties() {
    verify(requestSpecification1).get("https://micropub/path");
  }

  @Test
  void itReturnsPostTypes() {
    assertThat(actual).hasSize(2);
  }

  private void setupPostTypes() {
    when(requestSpecification0.get(anyString())).thenReturn(response0);
    when(response0.as(PostTypesResponseContainer.class)).thenReturn(container);
    MicropubPostType postType0 = new MicropubPostType();
    postType0.setProperties(Arrays.asList("not-required", "required"));
    postType0.setRequiredProperties(Collections.singletonList("required"));
    MicropubPostType postType1 = new MicropubPostType();
    postType1.setProperties(Collections.singletonList("not-required"));
    postType1.setRequiredProperties(Collections.emptyList());
    container.setPostTypes(Arrays.asList(postType0, postType1));
  }

  private void setupProperties() {
    when(requestSpecification1.get(anyString())).thenReturn(response1);
    PropertiesContainer container = new PropertiesContainer();
    when(response1.as(PropertiesContainer.class)).thenReturn(container);

    PropertiesContainer.Property property0 = new PropertiesContainer.Property();
    property0.setName("required");
    property0.setDisplayName("Required Field");

    PropertiesContainer.Property property1 = new PropertiesContainer.Property();
    property1.setName("not-required");
    property1.setDisplayName("Optional Field");

    container.setProperties(Arrays.asList(property0, property1));
  }
}
