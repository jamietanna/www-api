package me.jvt.www.editor.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import me.jvt.www.editor.autoconfiguring.PostTypesResponseContainer;
import me.jvt.www.editor.autoconfiguring.PropertiesContainer;
import me.jvt.www.editor.autoconfiguring.PropertiesContainer.Hint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PostTypeTest {

  @Mock private PostTypesResponseContainer.MicropubPostType micropubPostType;
  @Mock private PropertiesContainer.Property property0;
  @Mock private PropertiesContainer.Property property1;

  private PostType actual;

  @BeforeEach
  private void setup() {
    when(micropubPostType.getH()).thenReturn("h-entry");
    when(micropubPostType.getName()).thenReturn("Bookmark");
    when(micropubPostType.getType()).thenReturn("bookmark");
    when(micropubPostType.getProperties()).thenReturn(Arrays.asList("foo", "bar"));
    when(micropubPostType.getRequiredProperties()).thenReturn(Collections.singletonList("bar"));

    when(property0.getName()).thenReturn("bar");
    when(property0.getDisplayName()).thenReturn("Bar Stool");
    when(property0.getHints()).thenReturn(Collections.emptyList());
    when(property1.getName()).thenReturn("foo");
    when(property1.getDisplayName()).thenReturn("Fool");
    when(property1.getHints()).thenReturn(Collections.emptyList());
    actual = new PostType(micropubPostType, Arrays.asList(property0, property1));
  }

  @Test
  void setsH() {
    assertThat(actual.getH()).isEqualTo("h-entry");
  }

  @Test
  void setsName() {
    assertThat(actual.getName()).isEqualTo("Bookmark");
  }

  @Test
  void setsType() {
    assertThat(actual.getType()).isEqualTo("bookmark");
  }

  @Test
  void itSetsRequiredProperties() {
    Property property = get(actual, "bar");
    assertThat(property.isRequired()).isTrue();
  }

  @Test
  void itSetsProperties() {
    Property property = get(actual, "foo");
    assertThat(property.isRequired()).isFalse();
  }

  @Test
  void itSetsHints() {
    when(property1.getHints()).thenReturn(Collections.singletonList(Hint.date));

    actual = new PostType(micropubPostType, Arrays.asList(property0, property1));

    Property property = get(actual, "foo");
    assertThat(property.getHint()).isEqualTo(Hint.date);
  }

  @Test
  void itHandlesNoPropertyMapping() {
    actual = new PostType(micropubPostType, Collections.singletonList(property0));

    Property property = get(actual, "foo");
    assertThat(property.getDisplayName()).isEqualTo("foo");
  }

  @Test
  void itHandlesNoHintMapping() {
    Property expected =
        Property.PropertyBuilder.builder()
            .withName("bar")
            .withDisplayName("Bar Stool")
            .withIsRequired(true)
            .withHints(Collections.emptyList())
            .build();

    assertThat(actual.getProperties()).contains(expected);
  }

  @Test
  void itSetsOptionsWhenNotNull() {
    when(property0.getOptions()).thenReturn(Collections.singletonList("only-value"));

    actual = new PostType(micropubPostType, Collections.singletonList(property0));

    Property property = get(actual, "bar");
    assertThat(property.getOptions()).containsExactly("only-value");
  }

  @Test
  void itSetsDefaultWhenNotNull() {
    when(property0.getDefaultValue()).thenReturn("sample");

    actual = new PostType(micropubPostType, Collections.singletonList(property0));

    Property property = get(actual, "bar");
    assertThat(property.getDefaultValue()).isEqualTo("sample");
  }

  private Property get(PostType actual, String name) {
    return actual.getProperties().stream().filter(p -> name.equals(p.getName())).findFirst().get();
  }
}
