package me.jvt.www.editor.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collections;
import me.jvt.www.editor.autoconfiguring.PropertiesContainer.Hint;
import org.junit.jupiter.api.Test;

class PropertyTest {

  private Property property;

  @Test
  void onlyFirstNonMultivalueHintIsUsed() {
    property = withHints(Hint.date, Hint.url);

    assertThat(property.getHint()).isEqualTo(Hint.date);
  }

  @Test
  void defaultHintIsProvidedIfNoHints() {
    property = withHints(Hint.defaultType);

    assertThat(property.getHint()).isEqualTo(Hint.defaultType);
  }

  @Test
  void multivalueIsSetWhenContainsHint() {
    property = withHints(Hint.multivalue);

    assertThat(property.isMultivalue()).isTrue();
  }

  @Test
  void multivalueIsNotExclusiveWithOtherHints() {
    property = withHints(Hint.multivalue, Hint.url);

    assertThat(property.isMultivalue()).isTrue();
    assertThat(property.getHint()).isEqualTo(Hint.url);
  }

  private Property withHints(Hint hint) {
    return Property.PropertyBuilder.builder().withHints(Collections.singletonList(hint)).build();
  }

  private Property withHints(Hint... hints) {
    return Property.PropertyBuilder.builder().withHints(Arrays.asList(hints)).build();
  }
}
