package me.jvt.www.servlet;

import java.io.IOException;
import javax.servlet.ServletInputStream;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

@Component
public class ServletInputStreamReader {
  public String toString(ServletInputStream inputStream) throws IOException {
    return IOUtils.toString(inputStream);
  }
}
