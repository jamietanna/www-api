package me.jvt.www.servlet;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;

@Component
public class MultiReadHttpServletRequestSupplier {
  public MultiReadHttpServletRequest supply(HttpServletRequest httpServletRequest) {
    return new MultiReadHttpServletRequest(httpServletRequest);
  }
}
