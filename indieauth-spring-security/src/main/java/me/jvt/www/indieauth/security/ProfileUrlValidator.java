package me.jvt.www.indieauth.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ProfileUrlValidator {
  private final String indieAuthProfileUrl;

  public ProfileUrlValidator(@Value("${indieauth.profile.url}") String indieAuthProfileUrl) {
    this.indieAuthProfileUrl = indieAuthProfileUrl;
  }

  public boolean isValidProfileUrl(String profileUrl) {
    return indieAuthProfileUrl.equals(profileUrl);
  }
}
