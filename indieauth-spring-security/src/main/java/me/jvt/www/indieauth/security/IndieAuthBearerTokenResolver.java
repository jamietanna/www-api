package me.jvt.www.indieauth.security;

import javax.servlet.http.HttpServletRequest;
import org.springframework.security.oauth2.server.resource.web.BearerTokenResolver;
import org.springframework.security.oauth2.server.resource.web.DefaultBearerTokenResolver;

public class IndieAuthBearerTokenResolver implements BearerTokenResolver {

  private final DefaultBearerTokenResolver delegate;

  public IndieAuthBearerTokenResolver() {
    this.delegate = new DefaultBearerTokenResolver();
    delegate.setAllowFormEncodedBodyParameter(true);
  }

  @Override
  public String resolve(HttpServletRequest request) {
    return delegate.resolve(request);
  }
}
