package me.jvt.www.indieauth.security;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;

@ExtendWith(MockitoExtension.class)
class IndieAuthBearerTokenResolverTest {

  @Mock private HttpServletRequest mockHttpServletRequest;

  private String[] parameterValues;

  private IndieAuthBearerTokenResolver sut;

  @BeforeEach
  void setup() {
    sut = new IndieAuthBearerTokenResolver();

    Mockito.lenient().when(mockHttpServletRequest.getMethod()).thenReturn("POST");
    Mockito.lenient()
        .when(mockHttpServletRequest.getContentType())
        .thenReturn("application/x-www-form-urlencoded");
    parameterValues = new String[0];
    Mockito.lenient()
        .when(mockHttpServletRequest.getParameterValues(eq("access_token")))
        .thenReturn(parameterValues);
  }

  @Test
  void itResolvesTheTokenFromTheAuthorizationHeader() {
    // given
    when(mockHttpServletRequest.getHeader("Authorization")).thenReturn("Bearer ey123");

    // when
    String actual = sut.resolve(mockHttpServletRequest);

    // then
    assertThat(actual).isEqualTo("ey123");
  }

  @Test
  void itResolvesTheTokenFromTheFormBodyParameter() {
    // given
    setParameterValues("token-for-ma-bruddah");

    // when
    String actual = sut.resolve(mockHttpServletRequest);

    // then
    assertThat(actual).isEqualTo("token-for-ma-bruddah");
  }

  @Test
  void itResolvesMultipleTokensFromFormParametersAndRaisesExceptionInDelegate() {
    // given
    setParameterValues("token-for-ma-bruddah", "another-token-yo");

    // when
    assertThatThrownBy(
            () -> {
              sut.resolve(mockHttpServletRequest);
            })
        // then
        .isInstanceOf(OAuth2AuthenticationException.class);
  }

  @Test
  void itResolvesTokenFromMultiPartFormData() {
    // given
    setParameterValues("j.w.t");

    // when
    String actual = sut.resolve(mockHttpServletRequest);

    // then
    assertThat(actual).isEqualTo("j.w.t");
  }

  @Test
  void itResolvesMultipleTokensFromMultiPartFormParametersAndRaisesExceptionInDelegate() {
    // given
    setParameterValues("j.w.t", "a.b.c");

    // when
    assertThatThrownBy(
            () -> {
              sut.resolve(mockHttpServletRequest);
            })
        // then
        .isInstanceOf(OAuth2AuthenticationException.class);
  }

  @Test
  void itReturnsNullIfNoTokenFound() {
    // given
    setParameterValues();

    // when
    String actual = sut.resolve(mockHttpServletRequest);

    // then
    assertThat(actual).isNull();
  }

  private void setParameterValues(String... values) {
    parameterValues = values;
    when(mockHttpServletRequest.getParameterValues(eq("access_token"))).thenReturn(parameterValues);
  }
}
