package me.jvt.www.indieauth.security;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

class ProfileUrlValidatorTest {
  private final ProfileUrlValidator validator = new ProfileUrlValidator("https://correct");

  @Nested
  class IsValidProfileUrl {

    @Test
    void allowsValueFromConstructor() {
      assertThat(validator.isValidProfileUrl("https://correct")).isTrue();
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"https://correctly"})
    void doesNotAllowOtherValues(String value) {
      assertThat(validator.isValidProfileUrl(value)).isFalse();
    }
  }
}
