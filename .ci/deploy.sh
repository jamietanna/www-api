#!/usr/bin/env sh
set -eou pipefail
set -x

rsync --info=progress2 -e "ssh -o StrictHostKeyChecking=no" www-api-web/postdeploy/target/postdeploy*.jar www-data@www-api.jvt.me:/etc/www-api/post-deploy.jar
ssh -o StrictHostKeyChecking=no www-data@www-api.jvt.me sudo /bin/systemctl reload post-deploy.service
rsync --info=progress2 -e "ssh -o StrictHostKeyChecking=no" www-api-web/micropub/target/micropub*.jar www-data@www-api.jvt.me:/etc/www-api/micropub.jar
ssh -o StrictHostKeyChecking=no www-data@www-api.jvt.me sudo /bin/systemctl reload micropub-staging.service
ssh -o StrictHostKeyChecking=no www-data@www-api.jvt.me sudo /bin/systemctl reload micropub.service
rsync --info=progress2 -e "ssh -o StrictHostKeyChecking=no" www-api-web/google-fit/target/google-fit*.jar www-data@www-api.jvt.me:/etc/www-api/google-fit.jar
ssh -o StrictHostKeyChecking=no www-data@www-api.jvt.me sudo /bin/systemctl reload google-fit.service
rsync --info=progress2 -e "ssh -o StrictHostKeyChecking=no" www-api-web/notifications/target/notifications*.jar www-data@www-api.jvt.me:/etc/www-api/notifications.jar
ssh -o StrictHostKeyChecking=no www-data@www-api.jvt.me sudo /bin/systemctl reload notifications.service
rsync --info=progress2 -e "ssh -o StrictHostKeyChecking=no" www-api-web/analytics/target/analytics*.jar www-data@www-api.jvt.me:/etc/www-api/analytics.jar
ssh -o StrictHostKeyChecking=no www-data@www-api.jvt.me sudo /bin/systemctl reload analytics.service
rsync --info=progress2 -e "ssh -o StrictHostKeyChecking=no" www-api-web/editor/target/editor*.jar www-data@www-api.jvt.me:/etc/www-api/editor.jar
ssh -o StrictHostKeyChecking=no www-data@www-api.jvt.me sudo /bin/systemctl reload editor.service

rsync --info=progress2 -e "ssh -o StrictHostKeyChecking=no" www-api-web/indieauth/target/indieauth*.jar www-data@indieauth.jvt.me:/etc/www-api/indieauth.jar
ssh -o StrictHostKeyChecking=no www-data@indieauth.jvt.me sudo /bin/systemctl reload indieauth.service
