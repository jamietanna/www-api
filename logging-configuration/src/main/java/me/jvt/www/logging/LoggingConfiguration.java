package me.jvt.www.logging;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("me.jvt.www.logging")
public class LoggingConfiguration {}
