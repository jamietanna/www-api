package me.jvt.www.logging;

import java.io.IOException;
import java.util.UUID;
import java.util.regex.Pattern;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class CorrelationIdFilter extends OncePerRequestFilter {
  private static final Pattern UUID_PATTERN =
      Pattern.compile(
          "([a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12})",
          Pattern.CASE_INSENSITIVE);

  private final MDCFacade mdc;

  public CorrelationIdFilter(MDCFacade mdc) {
    this.mdc = mdc;
  }

  @Override
  protected void doFilterInternal(
      @NonNull HttpServletRequest request,
      @NonNull HttpServletResponse response,
      @NonNull FilterChain filterChain)
      throws ServletException, IOException {
    String correlationId = request.getHeader("correlation-id");
    if (null == correlationId || !UUID_PATTERN.matcher(correlationId).matches()) {
      correlationId = UUID.randomUUID().toString();
    }
    mdc.put("correlationId", correlationId);

    try {
      filterChain.doFilter(request, response);
    } finally {
      response.addHeader("correlation-id", correlationId);
      mdc.clear();
    }
  }
}
