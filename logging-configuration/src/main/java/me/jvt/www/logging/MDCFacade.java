package me.jvt.www.logging;

import java.util.Map;
import org.slf4j.MDC;
import org.slf4j.spi.MDCAdapter;
import org.springframework.stereotype.Component;

@Component
public class MDCFacade implements MDCAdapter {
  @Override
  public void put(String key, String val) {
    MDC.getMDCAdapter().put(key, val);
  }

  @Override
  public String get(String key) {
    return MDC.getMDCAdapter().get(key);
  }

  @Override
  public void remove(String key) {
    MDC.getMDCAdapter().remove(key);
  }

  @Override
  public void clear() {
    MDC.getMDCAdapter().clear();
  }

  @Override
  public Map<String, String> getCopyOfContextMap() {
    return MDC.getMDCAdapter().getCopyOfContextMap();
  }

  @Override
  public void setContextMap(Map<String, String> contextMap) {
    MDC.getMDCAdapter().setContextMap(contextMap);
  }
}
