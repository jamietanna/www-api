package me.jvt.www.logging;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.AdditionalMatchers.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.matches;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.util.UUID;
import java.util.regex.Pattern;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CorrelationIdFilterTest {

  private static final Pattern UUID_PATTERN =
      Pattern.compile("([a-fA-F0-9]{8}(-[a-fA-F0-9]{4}){4}[a-fA-F0-9]{8})");

  @Mock private HttpServletRequest request;
  @Mock private HttpServletResponse response;
  @Mock private FilterChain filterChain;

  @Mock private MDCFacade mdc;

  @InjectMocks private CorrelationIdFilter filter;

  @Test
  void addsCorrelationIdHeaderIfPresent() throws ServletException, IOException {
    String uuid = UUID.randomUUID().toString();
    when(request.getHeader("correlation-id")).thenReturn(uuid);

    filter.doFilterInternal(request, response, filterChain);

    verify(mdc).put("correlationId", uuid);
  }

  @Test
  void addsCorrelationIdWhenLowercaseHeaderIfPresent() throws ServletException, IOException {
    String uuid = UUID.randomUUID().toString().toLowerCase();
    when(request.getHeader("correlation-id")).thenReturn(uuid);

    filter.doFilterInternal(request, response, filterChain);

    verify(mdc).put("correlationId", uuid);
  }

  @Test
  void addsCorrelationIdWhenUppercaseHeaderIfPresent() throws ServletException, IOException {
    String uuid = UUID.randomUUID().toString().toUpperCase();
    when(request.getHeader("correlation-id")).thenReturn(uuid);

    filter.doFilterInternal(request, response, filterChain);

    verify(mdc).put("correlationId", uuid);
  }

  @Test
  void generatesCorrelationIdHeaderIfAbsent() throws ServletException, IOException {
    filter.doFilterInternal(request, response, filterChain);

    verify(mdc).put(eq("correlationId"), matches(UUID_PATTERN));
  }

  @Test
  void generatesCorrelationIdHeaderIfNotUUIDv4() throws ServletException, IOException {
    String uuidv1 = "4c1fa866-65e5-1867-b079-fd7f598a8cc6";
    when(request.getHeader("correlation-id")).thenReturn(uuidv1);

    filter.doFilterInternal(request, response, filterChain);

    verify(mdc).put(eq("correlationId"), not(eq(uuidv1)));
  }

  @Test
  void delegatesToChain() throws ServletException, IOException {
    filter.doFilterInternal(request, response, filterChain);

    verify(filterChain).doFilter(request, response);
  }

  @Test
  void addsCorrelationIdToResponse() throws ServletException, IOException {
    filter.doFilterInternal(request, response, filterChain);

    verify(response).addHeader(eq("correlation-id"), matches(UUID_PATTERN));
  }

  @Test
  void clearsMdcAfterRequest() throws ServletException, IOException {
    filter.doFilterInternal(request, response, filterChain);
    InOrder inOrder = inOrder(mdc, filterChain);

    inOrder.verify(filterChain).doFilter(any(), any());
    inOrder.verify(mdc).clear();
  }

  @Test
  void alwaysClearsMdc() throws ServletException, IOException {
    doThrow(new IOException("This should")).when(filterChain).doFilter(any(), any());

    assertThatThrownBy(() -> filter.doFilterInternal(request, response, filterChain))
        .isInstanceOf(IOException.class);

    verify(mdc).clear();
  }

  @Test
  void alwaysAddsToResponse() throws ServletException, IOException {
    doThrow(new IOException("This should")).when(filterChain).doFilter(any(), any());

    assertThatThrownBy(() -> filter.doFilterInternal(request, response, filterChain))
        .isInstanceOf(IOException.class);

    verify(response).addHeader(eq("correlation-id"), matches(UUID_PATTERN));
  }
}
