package me.jvt.www.api.core;

import static io.restassured.RestAssured.given;

import io.restassured.specification.RequestSpecification;

public class RequestSpecificationFactory {

  public RequestSpecification newRequestSpecification() {
    return given();
  }
}
